<style>
   
</style>
<div class="login_wrapper">
   <div class="container">
      <div class="col-md-12 no_padng">
         <div class="col-md-4"></div>
         <div class="col-md-4 no_padng">
            <form method="POST" action="" id="forgotanfp" data-parsley-validate="true" class="validate">
               <div class="login_box">
                  <div class="padng_outer">
                     <div class="login_to_zyva">
                        <h3>Forgot password?</h3><hr class="log_hr">
                     </div>

                     <div class="height_90">
                        <input type="email" id="femail" name="email" class="text_box_input"
                        placeholder="Email" tabindex="4" required="" 
                        data-parsley-required-message="Email is required."                                  
                        data-parsley-errors-container="#forgot_email">                              
                        <div class="pass_error" id="forgot_email"></div>
                        <div class="renew_pass pass_error align_center"></div>
                     </div>
                     <div class="view_loader"></div>
                     <div class="log_fogot">
                        <button  type="button" class="log_btn" id="land_logforget" >Submit</button>
                     </div>
                     <div class="fb_google">
                           <div class="fb">
                              <a href="<?php echo $authUrl;?>"><button type="button"><p>Facebook</p></button></a>
                           </div>
                           <div class="google">
                              <a href="<?php echo $googleUrl;?>"><button type="button"><p>Google</p></button></a>
                           </div>
                           <div class="clear"></div>
                     </div>

                  </div>
               </div>
            </form>   
         </div>
         <div class="col-md-4"></div>
      </div>
      <div class="col-md-12 no_padng">
         <div class="col-md-4"></div>
         <div class="col-md-4 no_padng">
            <div class="reg_now">
               <div class="cabin_padding">
                  <div class="login_new_user">
                     <p>New to Zyva?</p>
                     <a href="<?php echo base_url('home/signup');?>">register now</a>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4"></div>
      </div>
   </div>
</div>