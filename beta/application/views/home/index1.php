<div class="banner">
    <div class="starter-template">
        <div class="home_first_cabin">
            <!-- slider  -->
            <div class="container">
                <!-- <div class="col-md-5">
                    <div class="slide_section">
                        <div id="mySlider" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators slider_carousel">
        						<?php 
        						$i=0;
        						foreach($banner as $banners) { ?>
        						
        							<li data-target="#mySlider" data-slide-to="<?php echo $i; ?>" <?php if($i==0){?> class="active" <?php } ?>></li>
        						<?php 
                                ++$i;
                                } ?>
                            </ol>
        					
        					<div class="carousel-inner" role="listbox">
        						<?php 
        						  $i=0;    						
            						foreach($banner as $banners) { 
            						
            						if($i==0){
            						?>
                                    <div class="item active">
                                        <a href="#"><img class="bg" src="<?php echo $banners->banner_image;?>" /></a>
                                    </div>
            						<?php } else { ?>
                                    <div class="item">
                                        <a href="#"><img class="bg" src="<?php echo  $banners->banner_image;?>"/></a>
                                    </div>
        						<?php }
        						++$i;
        						}
        						?>
                                
                            </div>
        					
        					
        					<?php /*foreach($banner as $banner) { ?>
        					
        					<div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <a href="#"><img class="bg frst_img" src="<?php echo $banner->banner_image;?>" /></a>
                                </div>
                              
                                
                            </div>
        					<?php
        					}*/
        					?>
        					
        					
        					
        					
        					
                        </div>
                    </div>
                </div> -->


                <div class="col-md-7 col-sm-4 col-xs-12 remove_pad">
                    <div class="slide_section">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators slider_carousel">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <!-- <a href="#"><img class="bg frst_img" src="<?php echo base_url();?>assets/images/grl-one.png" /></a> -->
                                    <a href="#"><img class="bg frst_img" src="<?php echo base_url();?>assets/images/model_1.png" /></a>
                                </div>
                                <div class="item">
                                    <a href="#"><img class="bg sec_img" src="<?php echo base_url();?>assets/images/modalss_b.png"/></a>
                                </div>
                                <div class="item">
                                    <a href="#"><img class="bg" src="<?php echo base_url();?>assets/images/modalss_c.png" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-8 col-xs-12 remove_left_pad">
                    <div class="col-md-12 remove_left_pad">
                    <div class="outer_des">
                        <div class="text">
                            <h1>FOR WOMEN</h1>
                            <h5><span class="bord_span">ALL THE TRENDS </span> YOU NEED THE SEASON</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has</p>
                            <a href="#"> <button class="zyva_but box foo">read more</button></a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <!-- slider  end-->
        </div>


        <!-- fashion_block  -->
        <div class="banner_scnd">
            <div class="container">
                <div class="col-lg-12 pd-cust-1">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pd-zero">
                        <div class="fshion_store_out">
                            <div class="fshion_store">
                                <div class="fasion_head">
                                    <div class="circle_out_full">
                                        <div class="circle_out">
                                            <div class="circle">20% </div>
                                        </div>
                                    </div>
                                    <div class="circle_scnd">
                                        <h1>FASHIONS</h1>
                                        <H3>For Women</H3>
                                    </div>
                                </div>
                                <div class="fasion_descript">
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                    </p>
                                </div>
                                <div class="fasion_button">
                                    <a href="#"  <button class="blck_read red_mre foot">read more</button></a>
                                </div>
                            </div>
                            <div class="dress_material">
                                <img  class=" derss_a img-responsive" src="<?php echo base_url();?>assets/images/dres_pat.png">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pd-zero">
                        <div class="fshion_stre">
                            <div class="women_acesseries">
                                <h4>Women accessories</h4>
                                <hr class="bord_bot">
                                </hr>
                                <p>100 modal items</p>
                            </div>
                            <div class="dress_b">
                                <img src="<?php echo base_url();?>assets/images/dress_pat_a.png" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- fashion_block end -->



        <!-- sale_block  -->
        <div class="banner_thrd" id="myDiv">
            <div class="container">
                <div class="row">
                    <div class=" col-lg-4 col-md-4 col-sm-12 col-xs-12 pad_rht" >
                        <div class="dress_materials">
                            <img class="img-responsive " src="<?php echo base_url();?>assets/images/dress_c.png">
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="super_sale">
                                <div class="super_sale_inner">
                                    <h3>SUPER<br>
                                        SALE
                                    </h3>
                                    <h4>enjoy 20% off</h4>
                                    <p>with code # sample</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="sale_img">
                                <div class="hover " style="padding:0px;">
                                    <div class="hovereffect">
                                        <img class="img-responsive"src="<?php echo base_url();?>assets/images/dress_d.png" alt="">
                                        <div class="overlay">
                                            <a class="info" href="<?php echo base_url('Women/index/Women');?>">link here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="arrivals">
                                <div class="hover">
                                    <div class="hovereffect">
                                        <img class="img-responsive"src="<?php echo base_url();?>assets/images/dress_e.png" alt="">
                                        <div class="overlay">
                                            <a class="info" href="<?php echo base_url('Women/index/Women');?>">link here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 padding_none">
                            <div class="arival_new ">
                                <div class="arival_new_inner">
                                    <h5>NEW ARRIVALS</h5>
                                    <h3>#outfit</h3>
                                    <h4><span class="gray_of">of </span>today</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- sale_block end -->




        <!-- derss_block  -->
        <div class="banner_frth">
            <div class="container">
                <div class="row">
                    <div id="owl-demo">
                        <div class="item">
                            <div class="model_a">
                                <div class="wrapper">
                                    <div class="plus_add"> +</div>
                                    <div class="tooltip">
                                        <h3>Kurthis</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                        <p class="rd_mre">Read More</p>
                                    </div>
                                </div>
                                <img src="<?php echo base_url();?>assets/images/model_a.png">
                            </div>
                        </div>
                        <div class="item">
                            <div class="model_a">
                                <div class="wrapper chge_loc">
                                    <div class="plus_add"> +</div>
                                    <div class="tooltip">
                                        <h3>Kurthis</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                        <p class="rd_mre">Read More</p>
                                    </div>
                                </div>
                                <img src="<?php echo base_url();?>assets/images/model_b.png">
                            </div>
                        </div>
                        <div class="item">
                            <div class="model_a">
                                <div class="wrapper chge_lft">
                                    <div class="plus_add"> +</div>
                                    <div class="tooltip">
                                        <h3>Kurthis</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                        <p class="rd_mre">Read More</p>
                                    </div>
                                </div>
                                <img src="<?php echo base_url();?>assets/images/model_c.png">
                            </div>
                        </div>
                        <div class="item">
                            <div class="model_a">
                                <div class="wrapper">
                                    <div class="plus_add"> +</div>
                                    <div class="tooltip">
                                        <h3>Kurthis</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                        <p class="rd_mre">Read More</p>
                                    </div>
                                </div>
                                <img src="<?php echo base_url();?>assets/images/model_a.png">
                            </div>
                        </div>
                        <div class="item">
                            <div class="model_a">
                                <div class="wrapper chge_loc">
                                    <div class="plus_add"> +</div>
                                    <div class="tooltip">
                                        <h3>Kurthis</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                        <p class="rd_mre">Read More</p>
                                    </div>
                                </div>
                                <img src="<?php echo base_url();?>assets/images/model_b.png">
                            </div>
                        </div>
                        <div class="item">
                            <div class="model_a">
                                <div class="wrapper chge_lft">
                                    <div class="plus_add"> +</div>
                                    <div class="tooltip">
                                        <h3>Kurthis</h3>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                        <p class="rd_mre">Read More</p>
                                    </div>
                                </div>
                                <img src="<?php echo base_url();?>assets/images/model_c.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- derss_block end -->



            <!-- footer_block -->

<!-- footer_block end -->
							

            
			
			
			
			

            <!-- Modal register end -->

        </div>
        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->


       

    </body>
</html>
