<div class="login_wrapper">
   <div class="container">
      <div class="col-md-12 no_padng">
         <div class="col-md-4"></div>
         <div class="col-md-4 no_padng">
            <form method="POST" id="signup_form"  autocomplete="off">
               <div class="login_box">
                  <div class="padng_outer">
                     <div class="login_to_zyva">
                        <h3>Signup to zyva</h3><hr class="log_hr">
                     </div>
                     <div class="register_height">
                        <input type="text" class="text_box_input" name="first_name" id="first_name"                                  
                           data-parsley-pattern="^[a-zA-Z\  \/]+$" required=""                                  
                           placeholder="Name" tabindex="1"                                 
                           data-parsley-required-message="Name is required."                                  
                           data-parsley-errors-container="#reg_fname">                                 
                        <div class="reg_error" id="reg_fname"></div>
                     </div>

                     <div class="register_height">
                         <input type="text" class="text_box_input" name="telephone" id="mobile"                                  
                           data-parsley-trigger="keyup" data-parsley-type="digits"                                  
                           data-parsley-minlength="8" data-parsley-maxlength="15"                                   
                           data-parsley-pattern="^[0-9]+$" required="" placeholder="Telephone"                                  
                           tabindex="2" data-parsley-required-message="Phone number is required."                                  
                           data-parsley-errors-container="#reg_phone">                                 
                        <div class="reg_error" id="reg_phone"></div>
                     </div>

                     <div class="register_height">
                        <input type="email"  name="email" required="" tabindex="3" class="text_box_input" 
                           data-parsley-trigger="change" autocomplete="off" 
                           data-parsley-required-message="Email is required." 
                           data-parsley-errors-container="#uname"  placeholder="Email">
                        <div id="uname" class="uname_pass"></div>
                     </div>

                     <div class="height_90">
                        <input type="password" name="password" required="" tabindex="4" class="text_box_input"
                        data-parsley-trigger="change" data-parsley-minlength="6" 
                        autocomplete="off" data-parsley-required-message="Password is required." 
                        data-parsley-errors-container="#pass" placeholder="Password">
                        <div id="pass" class="pass_error"></div>
                        <div class="error_div pass_error signup_error"></div>
                     </div>

                     <div class="log_fogot">
                        <button  type="button" class="log_btn" id="signup_button" tabindex="5">Signup</button>
                        <p class="forgot_pass"> 
                           <a href="<?php echo base_url('home/forgot_password');?>" tabindex="6">Forgot password ?</a>
                        </p>
                     </div>
                     <div class="fb_google">
                           <div class="fb">
                              <a href="<?php echo $authUrl;?>"><button type="button" tabindex="7"><p>Facebook</p></button></a>
                           </div>
                           <div class="google">
                              <a href="<?php echo $googleUrl;?>"><button type="button" tabindex="8"><p>Google</p></button></a>
                           </div>
                           <div class="clear"></div>
                     </div>

                  </div>
               </div>
            </form>   
         </div>
         <div class="col-md-4"></div>
      </div>
      <div class="col-md-12 no_padng">
         <div class="col-md-4"></div>
         <div class="col-md-4 no_padng">
            <div class="reg_now">
               <div class="cabin_padding">
                  <div class="login_new_user">
                     <p>Already in Zyva?</p>
                     <a href="<?php echo base_url('home/signin');?>" tabindex="9">login now</a>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4"></div>
      </div>
   </div>
</div>