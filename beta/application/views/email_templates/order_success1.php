<html>
	<head>
		<!-- <link href="http://192.168.138.31/TRAINEES/Nikhila/zyva_latest/assets/css/email_style.css" rel="stylesheet"> -->
	</head>
	<style>
		
	</style>
	<body>
		<div style="width:90%; margin:0  auto;">
			<div style=" width:100%; float:left; background-image: url('<?php echo base_url();?>assets/images/bg_image.png ?>'); background-repeat: no-repeat; background-position: top;">
				<div class="width_80" style="width:80%; margin: 0 auto;">
					<div class="logo" style="text-align:center">
						<img src="http://192.168.138.31/TRAINEES/Nikhila/zyva_latest/assets/images/logo.png">
					</div>
					<div class="email_container">
						<div class="menu_container" style="margin-top: 20px; width: 100%; border-top: 1px solid #dcdcdc; border-bottom: 1px solid #dcdcdc;">
							<!-- =================INCLUDE HEADER======================= -->
							<?php include 'header.php'; ?>
							<!-- =================INCLUDE HEADER======================= -->
						</div>

						<h2 style="color: #c25ba5; font-size: 32px; font-family: 'Roboto', sans-serif;font-weight: 400;">Order Booked Successfully</h2>
						<h4 style="color: #5b4180; font-size: 20px; font-family: 'Roboto', sans-serif;
						    font-weight: 100; font-style: italic; width: 100%;">
						    Your order has been placed successfully. Your order details 
						    <br>are as follows.
						</h4>
						<h3 style="color: #777; font-size: 19px; font-family: 'Roboto', sans-serif; 
						font-weight: 600; width: 100%;">
    						Order Id : ZVA123456
    					</h3>
						<div class="detail_cabin" style="width: 100%;float: left; 
							background-color: #f1f1f1; margin-bottom: 20px;">
							<div class="" style="padding:25px;">
								<h4 style="color: #5b4180; font-size: 20px;margin: 0px !important;
    								font-family: 'Roboto', sans-serif;font-weight: 100;width: 50% !important;
    								float: left; text-align: left;">
    								Total Items : <span style="font-weight:bold">2</span></h4>
								<h4 style="color: #5b4180; font-size: 20px;margin: 0px !important;
    								font-family: 'Roboto', sans-serif;font-weight: 100;width: 50% !important;
    								float: left; text-align: right;">
									Total  :<span style="font-weight:bold"> &#8377; 1000.00</span></h4>
							</div>	
							<div class="det_inner" style="width: 100%; padding: 35px 0px;float: left;">
								<div class="first_section" style="width: 33.33%; float: left; border-right: 1px solid #d6d6d6;">
									<div class="img_section" style="width: 30%; float: left; padding-left: 25px;">
										<img src="http://192.168.138.31/TRAINEES/Nikhila/zyva_latest/assets/img/images/dress_c.png"
										style="width: 100px; height: 120px; border: 1px solid #e9e9e9;">
									</div>
									<div class="det_section" style="width: 50%; float: left; padding-left: 25px;">
										<h5 style="margin-top: 0px; padding-bottom: 6px; margin-bottom: 0px;
										    color: #808080; font-size: 18px; font-weight: 300;
										    font-family: 'Roboto', sans-serif; text-transform: inherit;">
										    Designer Saree
										</h5>
										<h6 style="font-size: 16px; font-weight: 600; color: #5a5a5a;
											font-family: 'Roboto', sans-serif;text-transform: uppercase; 
											margin-top: 30px;margin-bottom: 10px;">
											AALIYAH
										</h6>
										<h5 style="margin-top: 0px; padding-bottom: 6px; margin-bottom: 0px;
										    color: #808080; font-size: 18px; font-weight: 300;
										    font-family: 'Roboto', sans-serif; text-transform: inherit;">
										    LKMAA303
										</h5>
									</div>
								</div>
								<div class="first_section" style="width: 33.33%; float: left; border-right: 1px solid #d6d6d6;">
									<div class="qty_section" style="width: 35%; float: left; padding-left: 40px;">
										<p style="margin-top:0px !important; font-size: 14px; font-weight: 300; 
											color: #5a5a5a; font-family: 'Roboto', sans-serif;margin-bottom: 25px;">
											Color
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px;">
											Size
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px;">
											Qty
										</p>
									</div>
									<div class="qty_section" style="width: 35%; float: left; padding-left: 40px;">
										<p style="margin-top:0px !important">
											<img src="<?php echo base_url();?>assets//images/g.png"
											style="width: 30px; height: 30px; object-fit: contain; border-radius: 50%;">
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px;padding-left: 10px;">
											M
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px; padding-left: 10px;">
											2
										</p>
									</div>
								</div>
								<div class="second_section" style="width: 33%; float: left">
									<div class="price_section" style="    font-size: 18px;
									    font-weight: 600; color: #5a5a5a; font-family: 'Roboto', sans-serif;
									    text-transform: uppercase; text-align: left; padding-left: 50px;">
									    &#8377; 1000.00
									</div>
								</div>
							</div>

							<hr style="width: 96%; border-bottom: 1px solid #d6d6d6;border-top: none;">

							<div class="det_inner" style="width: 100%; padding: 35px 0px;float: left;">
								<div class="first_section" style="width: 33.33%; float: left; border-right: 1px solid #d6d6d6;">
									<div class="img_section" style="width: 30%; float: left; padding-left: 25px;">
										<img src="<?php echo base_url()?>assets/img/images/dress_b.png"
										style="width: 100px; height: 120px; border: 1px solid #e9e9e9;">
									</div>
									<div class="det_section" style="width: 50%; float: left; padding-left: 25px;">
										<h5 style="margin-top: 0px; padding-bottom: 6px; margin-bottom: 0px;
										    color: #808080; font-size: 18px; font-weight: 300;
										    font-family: 'Roboto', sans-serif; text-transform: inherit;">
										    Designer Saree
										</h5>
										<h6 style="font-size: 16px; font-weight: 600; color: #5a5a5a;
											font-family: 'Roboto', sans-serif;text-transform: uppercase; 
											margin-top: 30px;margin-bottom: 10px;">
											AALIYAH
										</h6>
										<h5 style="margin-top: 0px; padding-bottom: 6px; margin-bottom: 0px;
										    color: #808080; font-size: 18px; font-weight: 300;
										    font-family: 'Roboto', sans-serif; text-transform: inherit;">
										    LKMAA303
										</h5>
									</div>
								</div>
								<div class="first_section" style="width: 33.33%; float: left; border-right: 1px solid #d6d6d6;">
									<div class="qty_section" style="width: 35%; float: left; padding-left: 40px;">
										<p style="margin-top:0px !important; font-size: 14px; font-weight: 300; 
											color: #5a5a5a; font-family: 'Roboto', sans-serif;margin-bottom: 25px;">
											Color
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px;">
											Size
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px;">
											Qty
										</p>
									</div>
									<div class="qty_section" style="width: 35%; float: left; padding-left: 40px;">
										<p style="margin-top:0px !important">
											<img src="<?php echo base_url();?>assets//images/g.png"
											style="width: 30px; height: 30px; object-fit: contain; border-radius: 50%;">
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px;padding-left: 10px;">
											M
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px; padding-left: 10px;">
											2
										</p>
									</div>
								</div>
								<div class="second_section" style="width: 33%; float: left">
									<div class="price_section" style="    font-size: 18px;
									    font-weight: 600; color: #5a5a5a; font-family: 'Roboto', sans-serif;
									    text-transform: uppercase; text-align: left; padding-left: 50px;">
									    &#8377; 1000.00
									</div>
								</div>
							</div>

						</div>

						<div class="detail_cabin" style="width: 100%;float: left; 
							background-color: #f1f1f1; margin-bottom: 20px;">
							<div class="" style="padding:25px;">
								<h4  style="color: #5b4180;font-size: 22px; 
							    	font-family: 'Roboto', sans-serif;font-weight: 100; width:100%; float:left">
							    	Delivery Details
								</h4>
							</div>

							<h3 style="padding-left:25px; padding-top:25px;margin-bottom:0px; color: #777; font-size: 19px;
								font-family: 'Roboto', sans-serif; font-weight: 600;width: 100%;">
								Tessa
							</h3>
							<div class="address" style="width: 100%; padding: 25px;">
								<p style="margin-top: 0px; padding-bottom: 6px;margin-bottom: 0px;
								    color: #808080; font-size: 17px; font-weight: 100;line-height: 28px;
								    font-family: 'Roboto', sans-serif; text-transform: inherit;">
	    							Techware Solutions ,Second Floor Carnival Infopark Phase 1 <br>
									Kusumagiri P O , Kakanad EKM, Kakkanad <br>
									Ernakulam - 682030, KL</p>
									<p style="margin-top:15px; margin-bottom: 10px;
								    color: #808080; font-size: 17px; font-weight: 100;
								    font-family: 'Roboto', sans-serif; text-transform: inherit;">
								    Mobile: 9562340213</p>
								</p>
							</div>
						</div>

						<div style="width:100%; float:left" >
							<h4 style="margin-bottom:10px; color: #5b4180; font-size: 20px; font-family: 'Roboto', sans-serif;font-weight: 100;font-style: italic;width: 100%;">Thank You!</h4>
							<h4 style="margin-top:0px; margin-bottom:50px; color: #5b4180; font-size:20px; font-family: 'Roboto', sans-serif;font-weight: 100;font-style: italic;width: 100%;">Zyva Team</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer_width" style="width:90%; margin:0  auto;">	
			<?php include 'footer.php'; ?>
		</div>
			
	</body>

<html>
