<html>
	<head>
		<!-- <link href="http://192.168.138.31/TRAINEES/Nikhila/zyva_latest/assets/css/email_style.css" rel="stylesheet"> -->
	</head>
	<body>
		<div style="width:800px; margin:0  auto;">
			<div style=" width:100%; float:left; background-image: url('https://zyva.in/assets/images/bg_image.png'); background-repeat: no-repeat; background-position: top;">
				<div class="width_80" style="width:80%; margin: 0 auto;">
					<div class="logo" style="text-align:center">
						<img src="https://zyva.in/assets/images/logo.png">
					</div>
					<div class="email_container">
						<div class="menu_container" style="margin-top: 20px; width: 100%; border-top: 1px solid #dcdcdc; border-bottom: 1px solid #dcdcdc;">
							<!-- =================INCLUDE HEADER======================= -->
							<?php include 'header.php'; ?>
							<!-- =================INCLUDE HEADER======================= -->
						</div>
						
						<h4 style="margin-top:100px; color: #5b4180; font-size: 20px; font-family: 'Roboto', sans-serif; font-weight: 100;font-style: italic;width: 100%;">Hi Team,</h4>
						<p style="color: #5b4180;font-size: 20px;font-family: 'Roboto', sans-serif; font-weight: 100; font-style: italic; width: 100%;">
						<?php echo $name;?> has raised a contact request. The following are</br> 
						the customer's details ,</p>
						<div style="width:100%; float:left" >
							<p style="margin-bottom: 10px !important; color: #8c8c8c;font-size: 18px;font-family: 'Roboto', sans-serif; font-weight: 100;width: 100%;">
								Name
								<span style="padding-left:50px">:</span>
								<span style="padding-left:50px"><?php echo $name;?></span>
							</p>
							<!-- <p style="margin-bottom: 10px !important; margin-top:10px; color: #8c8c8c;font-size: 18px;font-family: 'Roboto', sans-serif; font-weight: 100;width: 100%;">
								Mob
								<span style="padding-left:65px">:</span>
								<span style="padding-left:50px">9847698254</span>
							</p> -->
							<p style="margin-bottom: 10px !important; margin-top:10px; color: #8c8c8c;font-size: 18px;font-family: 'Roboto', sans-serif; font-weight: 100;width: 100%;">
								Email
								<span style="padding-left:54px">:</span>
								<span style="padding-left:50px"><?php echo $email;?></span>
							</p>
							<p style="margin-bottom: 10px !important; margin-top:10px; color: #8c8c8c;font-size: 18px;font-family: 'Roboto', sans-serif; font-weight: 100;width: 100%;">
								Subject
								<span style="padding-left:38px">:</span>
								<span style="padding-left:50px"><?php echo $subject;?></span>
							</p>
							<p style="margin-top: 0px !important; color: #8c8c8c;font-size: 18px;
								font-family: 'Roboto', sans-serif; font-weight: 100; width: 100%;">
								Message
								<span style="padding-left:24px">:</span>
								<span style="padding-left:50px"><?php echo $msg;?></span>
							</p>
						</div>

						<div style="width:100%; float:left" >
							<h4 style="margin-bottom:60px; color: #5b4180; font-size: 20px; font-family: 'Roboto', sans-serif;font-weight: 100;font-style: italic;width: 100%;">Thank You!</h4>
						</div>
					</div>
				</div>
			</div>
			<!-- =================INCLUDE FOOTER======================= -->
			<?php include 'footer.php'; ?>
			<!-- =================INCLUDE FOOTER======================= -->
		</div>

			
	</body>

<html>
