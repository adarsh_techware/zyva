<html>
	<head>
	</head>
	<body>
		<div style="width:800px; margin:0  auto;">
			<div style=" width:100%; float:left; background-image: url('https://zyva.in/assets/images/bg_image.png'); background-repeat: no-repeat; background-position: top;">
				<div class="width_80" style="width:80%; margin: 0 auto;">
					<div class="logo" style="text-align:center">
						<img src="https://zyva.in/assets/images/logo.png">
					</div>
					<div class="email_container">
						<div class="menu_container" style="margin-top: 20px; width: 100%; border-top: 1px solid #dcdcdc; border-bottom: 1px solid #dcdcdc;">
							<!-- =================INCLUDE HEADER======================= -->
							<?php include 'header.php'; ?>
							<!-- =================INCLUDE HEADER======================= -->
						</div>
						
						<h4 style="margin-top:100px; color: #5b4180; font-size: 20px; font-family: 'Roboto', sans-serif; font-weight: 100;font-style: italic;width: 100%;">
							Hi <?php echo $reg_name; ?>,
						</h4>
						<p style="color: #5b4180;font-size: 20px;font-family: 'Roboto', sans-serif; font-weight: 100; font-style: italic; width: 100%;">
						Your account has been successfully created. Please use below<br> 
						Credentials for further use,</p>
						<div style="width:100%; float:left" >
							<p style="margin-bottom: 10px !important; line-height:26px; color: #8c8c8c;font-size: 20px;font-family: 'Roboto', sans-serif; font-weight: 100; font-style: italic; width: 100%;">
							Username   : 
								<span style="color: #5b4180 !important;">
								<?php echo $email; ?>
								</span>
							</p>
							<p style="margin-top: 0px !important; color: #8c8c8c;font-size: 20px;font-family: 'Roboto', sans-serif; font-weight: 100; font-style: italic; width: 100%;">
							Password   : 
							<span style="color: #5b4180 !important;">
								<?php echo $password; ?>
							</span>
							</p>
						</div>

						<div style="width:100%; float:left" >
							<h4 style="margin-bottom:10px; color: #5b4180; font-size: 20px; font-family: 'Roboto', sans-serif;font-weight: 100;font-style: italic;width: 100%;">Thank You!</h4>
							<h4 style="margin-top:0px; margin-bottom:100px; color: #5b4180; font-size:20px; font-family: 'Roboto', sans-serif;font-weight: 100;font-style: italic;width: 100%;">Zyva Team</h4>
						</div>
					</div>
				</div>
			</div>
			<!-- =================INCLUDE FOOTER======================= -->	
			<?php include 'footer.php'; ?>
			<!-- =================INCLUDE FOOTER======================= -->
		</div>

			
	</body>

<html>
