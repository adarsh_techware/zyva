<html>
<head>
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>404 Page no found</title>
	<meta name="viewport" content="width=device-width">
</head>
<body>
	<div class="container">
		<div class="col-md-4"></div>
		<div class="col-md-4 com-sm-12 col-xs-12 remove_pad">
			<div class="error_page_wrap">
				<div class="404_img">
					<img src="<?php echo base_url();?>assets/images/404.png">
				</div>
				<div class="error_para">
					<h4>Sorry! Page not found.</h4>
					<p>Unfortunately the page you are looking for has been moved or deleted.</p>
					<a href="<?php echo base_url();?>" title="Back to site" class="error_back"><button>Go to Home</button></a>
				</div>
			</div>
		</div>
		<div class="col-md-4"></div>	
	</div>
</body>
</html>