<div class="starter-template">

    <div class="my_acount_tabs top_inr">

        <div class="terms_condition">

            <div class="container">

                <div class="col-md-12">   

                    <div class="terms_cabin">
                        <!-- <h2>Terms And  Conditions</h2> -->

                            <div class="zyva_terms">
                                <h3>Payment and Logistics</h3>

                                  <p>We’ve got a very convenient option where you can pay at your doorstep before receiving the package

                                    and signing the delivery sheet. As part of our policy to protect against the fraudulent, we conduct

                                    security checks on all orders received. These checks can take various forms and may involve

                                    contacting you by telephone before we process your order.</p>

                                    <p>Process flow to purchase the product & service - end to end</p>

                                    <p>Step 1: Select Design</p>
                                    <p>Step 2: Customization</p>
                                    <p>Step 3: Stitching</p>
                                    <p>Step 4: Packing</p>
                                    <p>Step 5: Delivery</p>
                                    <p>Maryam Apparels has a huge collection of designs. You can choose your design from a variety set of designs

                                    and patterns from Maryam Apparels. The material is stitched according to your requests and

                                    measurements provided to us by the sample outfit. After the customization, stitching process will

                                    start. After a quality check from our side, the product is delivered to you.</p>
                            </div>
                        </div>
                    </div> 
                </div>

            </div>

        </div>   

    </div>

</div>



