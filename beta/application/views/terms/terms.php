<div class="starter-template">

    <div class="my_acount_tabs top_inr">

        <div class="terms_condition">

            <div class="container">

                <div class="col-md-12">   

                    <div class="terms_cabin">
                        <h2>Terms And  Conditions</h2>

                            <div class="zyva_terms">
                                <h3>Welcome to Maryam Apparels</h3>

                                  <p>Welcome to Maryam Apparels. Thank you for visiting www.zyva.in. Your use of Maryam Apparels and services

                                    are governed by the following terms and conditions. You are required to read and accept all the

                                    terms and conditions laid down. Please read them carefully. Maryam Apparels owns, operates and

                                    controls the Maryam Apparels website (www.zyva.in) Maryam Apparels provides end users with free information

                                    relating to Maryam Apparels offers for Kurtis and services on the Site. Maryam Apparels is not responsible for the

                                    websites of any other Offers.</p>

                                    <h3>Responsibility for content</h3>
                                    <p>Maryam Apparels reserves the right to change all or parts of the Offers as they appear on the Site and to add

                                        to, delete or cease publication as well as to the Content (as defined hereafter),

                                        recommendations and services provided to you on or through the Site temporarily or

                                        permanently without prior notice. Maryam Apparels authorizes you to view and access the Maryam Apparels Content

                                        solely for identifying Products, carrying out purchases of Products and processing returns and

                                        refunds, in accordance with Return and Refund Policy, if any. Maryam Apparels, therefore, grants you a

                                        limited, revocable permission to access and use the Services. Maryam Apparels does not moderate or pre-

                                        screen any Content uploaded or submitted by Members and is not responsible for any such

                                        Content, including the accuracy or legality of the Content.
                                    </p>

                                    <p>Maryam Apparels reserves the right, at its discretion, to change, modify, add, or remove portions of these

                                    Terms and Conditions at any time. Please check these Terms and Conditions periodically for

                                    changes. Your continued use of the Site following the posting of changes to these terms will

                                    mean you accept those changes.</p>
                                    

                                    <p>Unless Maryam Apparels has otherwise agreed in writing, nothing in the terms gives you a right to use any of

                                    Maryam Apparels Brand names, trademarks, service marks, logos, domain names, and other distinctive

                                    brand features. You additionally agree that in using the offering, you will not use any trade

                                    mark, service mark, trade name, logo of any company or organization in a way that is likely or

                                    intended to cause confusion about the owner or authorized user of such marks, names or logos.</p>
                                    

                                    <p>Maryam Apparels may make changes to the terms from time to time. The same will be communicated to

                                        you, on the website, without prior intimation to you. Your continued use of the offering will

                                        indicate your acceptance of the modified terms. When these changes are made, Maryam Apparels shall

                                        make the amended copy of the terms available in an accessible location.</p>
                                            

                                    <h3>Copyright</h3>

                                    <p>All documents, designs, photographs, illustrations, audio clips, or the like, as well as the

                                        software of zyva.in and other websites operated by Maryam Apparels under the copyright laws. The

                                        websites of zyva.in are patent fair and equitable competition and protected by other laws,

                                        they may therefore be either in whole or in part, be copied or imitated. No logo, graphic or

                                        image from any Microsoft Web sites may not be copied or retransmitted unless it has the

                                        express written consent of Maryam Apparels before. The documents, the structure and the software are

                                        protected by copyright. The logos, trade name, or some other words are authenticated

                                        trademarks of Maryam Apparels.</p>

                                    <h3>Terms of Sale</h3>

                                    <p>By placing an order you are offering to purchase a kurtis on and subject to the following terms

                                        and conditions. All orders are subject to availability and confirmation of the order price.</p>

                                    <div class="terms_sub">
                                        
                                        <h3>a.Our Contract</h3>
                                        <p>When you place an order, you will receive an acknowledgement e-mail confirming receipt of

                                            your order. This email will only be an acknowledgement and will not constitute acceptance of

                                            your order. A contract between us for the purchase of the goods will not be formed until your

                                            payment has been completed.</p>

                                        <h3>b.Pricing and Availability</h3>
                                        <p>Whilst we try and ensure that all details, descriptions and prices which appear on this Website

                                            are accurate, errors may occur. If we discover an error in the price of materials which you have

                                            ordered we will inform you of this as soon as possible and give you the option of reconfirming

                                            your order at the correct price or cancelling it. If we are unable to contact you we will treat the

                                            order as cancelled..</p>

                                            <p>The Service may contain typographical errors or other errors or inaccuracies and may not be

                                             complete or current. We therefore reserve the right to correct any errors, inaccuracies or 

                                             omissions and to change or update information at any time without prior notice. We reserve

                                            the right to refuse to fill any orders that you may place based on information on the Service

                                            that may contain errors or inaccuracies, including, without limitation, errors, inaccuracies or

                                            out-of- date information regarding pricing, shipping, payment terms, or return policies.</p>

                                        <h3>c. Payment</h3>
                                        <p>Payment on Delivery wherein payment shall be made only by cash on delivery. For the purposes 

                                            of buying Kurtis on Maryam Apparels, you agree and undertake not to make payments in any way other than

                                            as provided, without the prior consent of the Maryam Apparels.

                                            <b><a href="<?php echo base_url('terms/payment_logistics');?>" style="text-decoration: none;">
                                                Click here for</a> Payment and logistics.</b></p>
                                    </div>
                                    
                                    <h3>Governing Law:</h3>
                                    <p>The Agreement and any contractual obligation between Maryam Apparels and you under the Agreement shall be governed by the laws of India. </p>
                            </div>
                        </div>
                    </div> 
                </div>

            </div>

        </div>   

    </div>

</div>



