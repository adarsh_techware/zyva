<div class="starter-template">

    <div class="my_acount_tabs top_inr">

        <div class="about_page policy_min_height">

            <div class="container">

                <div class="col-md-12">   

                    <div class="terms_cabin">
                        <h2>Policy</h2>

                            <div class="zyva_terms">
                                <h3>Privacy Policy</h3>
                                <p>Please read our privacy policy to have a clear understanding of how we collect/use your personal
                                 information.</p>
                                <p>We collect the information from you when you place an order. This personal information collected

                                    from you is used to process your delivery and to respond to your customer service requests. This will

                                    be under our safe custody and we do not sell or trade or share your personal information to third

                                    parties.</p>
                                <p>Please note that we do not allow third party tracking of personal data.</p>

                                <h3>Return, Refund and Cancellation Policy</h3>
                                <p>You have the option to return or exchange items purchased on Maryam Apparels. in. If you wish to return an item

                                    returns must reach us no later than 7 working days from the date you receive the item, we will not

                                    accept returns after this time has passed. Return/Exchange acceptance varies from 7 days to 30 days

                                    of the receipt of item and depends on our sellers and Affiliate partners. If you choose to exchange the

                                    item for reason of mismatch of size or receipt of a defective item, you will be provided with a

                                    replacement of the item, free of cost.</p>
                                <p>Please note before making a purchase that any item returned will take up to 7 working days to be

                                    refunded back to you. If there is any fault with your item please notify us within 24hrs of receiving the

                                    material so we can look into this.</p>

                                <h3>Shipping Policy</h3>
                                <p>Some items may have shipping depending on the type of the material. For International orders, the

                                    shipping charges are different for different materials. Also Prices may be different for different

                                    countries to account for shipping costs as sellers are free to set their prices and prices listed in INR are

                                    only for India.</p>
                            </div>
                        </div>
                    </div> 
                </div>

            </div>

        </div>   

    </div>

</div>



