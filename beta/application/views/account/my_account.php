
         <div class="container">
            <div class="col-md-2"></div>
            <div class="col-md-8">
               <div class="row top_inr">
                  <div class="board">
                     <h4>My account</h4>
                     <div class="board-inner acnt_tab">
                        <ul class="nav nav-tabs" id="myTab">
                           <li class="active">
                              <a href="#home" data-toggle="tab" title="welcome">
                              <span class="round-tabs one">
                              <img src="<?php echo base_url();?>assets/images/edit_acnt.png">
                              </span> 
                              </a>
                              Edit Account
                           </li>
                           <li><a href="#profile" data-toggle="tab" title="profile">
                              <span class="round-tabs two">
                              <img src="<?php echo base_url();?>assets/images/addr_book.png">
                              </span> 
                              </a>
                              Address Book
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-2"></div>
         </div>
         <div class="tab_comt_out">
            <div class="container">
               <div class="col-md-1"></div>
			   
                <div class="col-md-10" style="margin-bottom: 50px;">
			   	     
                  <div class="tab-content">
				 
				            
                    <!--//////////////////////////////////////////////// TAB1 START -->
				 
                     <div class="tab-pane fade in active less_pad txt_highlt" id="home">
                     <form method="POST" id="accountdetails" data-parsley-validate="true" class="validate" name="accountdetails">
                        <div class="col-md-12">

                						<div class="errormsg"></div>
                						
                             
                						
                              <div class="col-md-6">
                                <h4>Account Information</h4>
                                <div class="acnt_inform wdth_bord mar_inr">
                                    
                                   <input type="text" class="my_acunt_dtls" name="first_name" value="<?php echo $account->first_name; ?>"
                                    placeholder="First Name"
                                    data-parsley-trigger="change" required=""
                                    data-parsley-required-message="Please insert first name." 
                                    data-parsley-errors-container="#acc_first_name">
                                    <div class="acc_errors" id="acc_first_name"></div>

                                   <input type="text" class="my_acunt_dtls" name="last_name"  value="<?php echo $account->last_name; ?>"  
                                   placeholder="Last Name"
                                   data-parsley-trigger="change" required=""
                                   data-parsley-required-message="Please insert last name." 
                                   data-parsley-errors-container="#acc_last_name" >
                                   <div class="acc_errors" id="acc_last_name"></div>

                                   <input type="email"class="my_acunt_dtls" name="email" value="<?php echo $account->email; ?>"      
                                   placeholder="Email Address" data-parsley-trigger="change" required=""
                                   data-parsley-required-message="Please insert email." 
                                   data-parsley-errors-container="#acc_email" readonly="true">
                                  <div class="acc_errors" id="acc_email"></div>

                                </div>
                             </div>
                             <div class="col-md-6">
                                     <h4>Change Password</h4>
                                <div class="acnt_inform mar_inr">
                                   
                                   <input type="password" class="my_acunt_dtls" name="password"   
                                   placeholder="Current Password" id="current_pass"
                                   data-parsley-trigger="change" 
                                   data-parsley-required-message="Please insert current password." 
                                   data-parsley-errors-container="#acc_cur_pass">
                                  <div class="acc_errors" id="acc_cur_pass"></div>

                                   <input type="password" class="my_acunt_dtls" name="c_password" 
                                    placeholder="New Password" id="new_pass"
                                    data-parsley-trigger="change" 
                                    data-parsley-required-message="Please insert new password." 
                                    data-parsley-errors-container="#acc_new_pass">
                                   <div class="acc_errors" id="acc_new_pass"></div>

                                   <input type="password" class="my_acunt_dtls" name="n_password" 
                                   placeholder=" Confirm New Password" id="con_pass"
                                   data-parsley-trigger="change" 
                                   data-parsley-required-message="Please insert confirm password." 
                                   data-parsley-errors-container="#acc_con_pass">
                                  <div class="acc_errors" id="acc_con_pass"></div>

                                </div>
                             </div>
						                
						   
						              
						   
                        </div>

        								<div class="row">
        								 
        								   <div class="col-md-2"></div>
        									 <div class="col-md-8">
                                                 
                             <div class="col-md-12">
                                 <div class="col-md-3"></div>
                             
                                  <div class="col-md-6 my_acct_margin">
                                    <div class="savsd_edit">
                									    <button class="sav_acnt chg_pwd"  type="button">Save</button>
                										 
                									    <button id="can1" type="submit" class="sav_acnt cacce_but">Cancel</button>
                             
                                    </div>
                                  </div>

                                <div class="col-md-3"></div>
        									    </div>
                              
        								    </div>
                            <div class="col-md-2"></div>
                        </div>
                        </form>

					           </div>
                     <!--//////////////////////////////////////////////// TAB1 END -->




                     <!--//////////////////////////////////////////////// TAB2 START -->
                     <div class="tab-pane fade txt_highlt less_pad" id="profile">
                         <div class="col-md-12">
						 
                						 
                						  <div class="errormsg3"></div>
                						  <form method="POST" id="addressbook" name="addressbook" data-parsley-validate="true" class="validate">

                                <div class="col-md-6">
                                   <h4>Contact Information</h4>
                                  <div class="acnt_inform wdth_bord_hight mar_inr">
                                      
                                     <input type="text" class="my_acunt_dtls" name="firstname" value="" 
                                        placeholder="First Name" data-parsley-trigger="change" required=""
                                        data-parsley-required-message="Please insert first name." 
                                        data-parsley-errors-container="#con_first_name">
                                      <div class="acc_errors" id="con_first_name"></div>

                                     <input type="text" class="my_acunt_dtls"  name="lastname"   value=""  
                                        placeholder="Last Name"  data-parsley-trigger="change" required=""
                                        data-parsley-required-message="Please insert last name." 
                                        data-parsley-errors-container="#con_last_name">
                                        <div class="acc_errors" id="con_last_name"></div>

                                     <input type="text" data-parsley-trigger="change"  class="my_acunt_dtls" name="company"  value=""
                                        placeholder="Company" data-parsley-trigger="change" 
                                        data-parsley-required-message="Please insert company name." 
                                        data-parsley-errors-container="#con_company">
                                        <div class="acc_errors" id="con_company"></div>

                                     <input type="email" class="my_acunt_dtls" name="email1" value=""  
                                        placeholder="Email" data-parsley-trigger="change" required=""
                                        data-parsley-required-message="Please insert email." 
                                        data-parsley-errors-container="#con_email">
                                        <div class="acc_errors" id="con_email"></div>

                                     <input type="text" class="my_acunt_dtls" name="telephone" value=""
                                        placeholder="Telephone" data-parsley-trigger="change"  data-parsley-type="digits" 
                                        data-parsley-minlength="10" data-parsley-maxlength="15"  
                                        data-parsley-pattern="^[0-9]+$" required=""
                                        data-parsley-required-message="Please insert phone number." 
                                        data-parsley-errors-container="#con_telephone">
                                        <div class="acc_errors" id="con_telephone"></div>
                                  </div>
                                </div>

                                <div class="col-md-6">
                                  <h4>Address </h4>
                                  <div class="acnt_inform mar_inr">
                                     
                                     <input type="text" class="my_acunt_dtls" name="address_1"   value="" 
                                        placeholder="Address Line 1"data-parsley-trigger="change" required=""
                                        data-parsley-required-message="Please insert address line 1." 
                                        data-parsley-errors-container="#con_add1">
                                        <div class="acc_errors" id="con_add1"></div>

                                     <input type="text" class="my_acunt_dtls" name="address_2"   value=""  
                                        placeholder="Address Line 2" data-parsley-trigger="change" required=""
                                        data-parsley-required-message="Please insert address line 2." 
                                        data-parsley-errors-container="#con_add2">
                                        <div class="acc_errors" id="con_add2"></div>
                                      

                                      <!-- <div class="place-select"> -->
    								  
                          				      <?php if($statedetails > 0){ ?>
                          							
                          							  <div class="place-select">
                          								 <!-- <select name="state" id="fxselect">-->
                          								   <select class="sel_width" onchange="selectCity(this.options[this.selectedIndex].value)"  name="state_id" id="state_id"
                                                data-parsley-trigger="change" required=""
                                                data-parsley-required-message="Please select state." 
                                                data-parsley-errors-container="#con_state">

                                                <option selected="" value="">Select State</option>
                                                               
                          											 <?php foreach($statedetails as $statedetails) {	?>

                          						            <option value="<?php echo $statedetails->id;?>"><?php echo $statedetails->state_name;?></option>
                          								       
                                                 <?php  } ?>
                          									 
                                                </select>
                                                 <div class="acc_errors" id="con_state"></div>
                                          </div>
                          							
                          							  <div class="place-select">
                          								  <select class="sel_width"  id="city_dropdown" name="city_id"  onchange=""
                                              data-parsley-trigger="change" required="" 
                                              data-parsley-required-message="Please select city." 
                                              data-parsley-errors-container="#con_city">
                                              <option selected="" value="">Select City</option>

                          											<?php foreach($citydetails as $citydetails){	?>

                          						          <option value="<?php echo $citydetails->id;?>"><?php echo $citydetails->city_name;?></option>
                          								       
                                                <?php }  ?>
                                            </select>
                                            <div class="acc_errors" id="con_city"></div>

                                          </div>
                          					
                          	            <?php } ?>
                          		
                                        <input type="text" class="my_acunt_dtls" name="Zip"  value="" placeholder="Zip Code"
                                        data-parsley-type="digits" data-parsley-minlength="2" 
                                        data-parsley-maxlength="10" data-parsley-pattern="^[0-9]+$"  
                                        data-parsley-trigger="change" required=""
                                        data-parsley-required-message="Please insert zip code." 
                                        data-parsley-errors-container="#con_zip">
                                        <div class="acc_errors" id="con_zip"></div>
                                      <!-- </div> -->
                                  </div>
    						  
                                </div>
    						 
              								  <div class="row">
              								 
                									<div class="col-md-4"></div>

													       
                                   
                									<div class="col-md-4 my_acct_margin">
                									 
                  									 <button class="sav_acnt" id="edit1" type="submit">Save</button>
                  										 
                  									 <button id="can" class="sav_acnt cacce_but" type="button">Cancel</button>
                									</div>
                									<div class="col-md-4"></div>

              								 
              								  </div>
                              </form>


                                <div class="row">
                                   <h3>Address List</h3>
                                  <div class="tab-pane active" id="already">
                                      <?php
                                      $i = 0;
                                      foreach ($address_list as $rs) {
                                      // if($rs=="") {
                                      //   echo "<p>No Address Found!<p>"; 
                                      // }
                                      // else {
                                      $class = $i%2==1?'no_margins_right':'';
                                      ++$i;
                                      ?>
                                       <div class="address_exist <?php echo $class; ?>">

                                          <div class="address_exist_check">
                                              <input type="radio" name="address_id" 
                                                id="address_id_<?php echo $rs->address_id; ?>" 
                                                value="<?php echo $rs->address_id; ?>" 
                                                class="address_class" <?php if($rs->default_id==1){?> checked="checked" <?php }?>/>
                                              <label for="address_id_<?php echo $rs->address_id; ?>"></label>
                                          </div>

                                          

                                          <div class="exist_pad">
                                            <p><?php echo ucfirst($rs->firstname).' '.$rs->lastname; ?></p>
                                            <?php if($rs->company!=''){?>
                                              <p><?php echo $rs->company; ?></p>
                                            <?php } ?>
                                            <p><?php echo $rs->address_1; ?></p>
                                            <?php if($rs->address_2!=''){?>
                                              <p><?php echo $rs->address_2; ?></p>
                                            <?php } ?>
                                            <p><?php echo $rs->city_name; ?></p>
                                            <p><?php echo $rs->state_name; ?>-<?php echo $rs->Zip; ?></p>
                                          </div>
                                        </div> 

                                      <?php  } ?>
                                          
                                     </div>
                                   </div>

                              

                          </div>
    					
                        <div class="clearfix"></div>
    					 
                      </div>
                      <!--//////////////////////////////////////////////// TAB2 END -->


                  </div> <!-- Tab content -->

                </div> <!-- col-md-10 -->

             <div class="col-md-1"></div>

          </div> <!-- Container end -->

    	  </div> <!-- tab_comt_out end -->





      <!-- footer_block -->
    
      <!-- footer_block end -->
      <!-- Modal login -->
      
      <!-- Modal login end -->
      <!-- Modal register -->
 
      <!-- Modal register end -->
      <!-- Bootstrap core JavaScript
         ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      
  
    
     
     <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
      <a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;"></a>
    
    
     

