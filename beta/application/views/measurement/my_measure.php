<div class="starter-template">
  <div class="my_acount_tabs top_inr">

      <!-- Starting my order Page here -->


    <div class="col-md-12  col-sm-12 col-xs-12 pad_lft">
      <div class="container">
        <form method="post"  data-parsley-validate=""  class="measurement validate">
          





          <div class="col-md-12 col-sm-12 col-xs-12 order_padbots remove_pad">
            <div class="my_ordr">
              <span class="mywish_img"><img src="<?php echo base_url();?>assets/images/measurement.png"></span>
              My Measurement
            </div>

                <!--  Sectio1 srarts here  -->
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 bord_rhts">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="women_sizes">
                            <img src="<?php echo base_url();?>assets/images/women_top.png" class="top_img">
                            </div>

                        </div>
                         <div class="col-md-9  col-sm-12 col-xs-12">
                            <div class="women_sizes_detail">
                              <h5>Women’s sizes</h5>
                              <h5 class="top_wear">Top wear</h5>
                              <input type="hidden"  name="customer_id" value="<?php echo $this->session->userdata('user_id')?>" >
                              <ol class="custom-counter">

                                <li>
                                  <div class="prev_size"><p>1</p></div>
                                  <div class="size_name">Bust Size</div> 
                                  <div class="size_input">
                                      <input type="text" class="measure_input"  name="bustsize" placeholder="eg:30 cm"  
                                       data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$" data-parsley-type="digits"  
                                       required="" data-parsley-required-message="Please insert bust size."
                                       data-parsley-errors-container="#bust_size1" value="<?php echo $measurement->bustsize; ?>"/>
                                  </div>
                                  <div id="bust_size1" class="custom_measure_parse_error"></div>
                                </li>

                                <li>
                                  <div class="prev_size"><p>2</p></div>
                                  <div class="size_name">Waist Size</div>
                                  <div class="size_input">
                                    <input type="text" class="measure_input"  name="waistsize"   placeholder="eg:30 cm"  
                                    data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"  data-parsley-type="digits"
                                    required="" data-parsley-required-message="Please insert waist size."
                                    data-parsley-errors-container="#waist_size1" value="<?php echo $measurement->waistsize; ?>"/>
                                  </div>
                                  <div id="waist_size1" class="custom_measure_parse_error"></div>
                                </li>

                                <li>
                                  <div class="prev_size"><p>3</p></div>
                                  <div class="size_name">Hip Size</div>
                                  <div class="size_input">
                                    <input type="text" class="measure_input"  name="hipsize"   placeholder="eg:30 cm"  
                                    data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$" data-parsley-type="digits" 
                                    required="" data-parsley-required-message="Please insert hip size."
                                       data-parsley-errors-container="#hip_size1" value="<?php echo $measurement->hipsize; ?>"/>
                                  </div>
                                  <div id="hip_size1" class="custom_measure_parse_error"></div>
                                </li> 

                              </ol>

                            </div>

                        </div>


                    </div>
                </div>

                <div class="col-md-6 col-sm-3 col-sm-12 details_pad">
                     <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="women_sizes">
                            <img src="<?php echo base_url();?>assets/images/women_bot.png">

                            </div>

                        </div>

                        <div class="col-md-9  col-sm-12 col-xs-12">
                          <div class="women_sizes_detail">
                            <h5>Women’s sizes</h5>
                            <h5 class="top_wear">bottom wear</h5>

                            <ol class="custom-counter">
                            <li>
                              <div class="prev_size"><p>1</p></div>
                              <div class="size_name">Pant Waist</div> 
                              <div class="size_input">
                                <input type="text" class="measure_input"  name="pantwaist"   placeholder="eg:30 cm" 
                                data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$" data-parsley-type="digits"
                                required="" data-parsley-required-message="Please insert pant waist size."
                                data-parsley-errors-container="#pant_waist1" value="<?php echo $measurement->pantwaist; ?>"/>
                              </div>
                              <div id="pant_waist1" class="custom_measure_parse_error"></div>
                            </li>  

                            <li>
                              <div class="prev_size"><p>2</p></div>
                              <div class="size_name">Hip</div>
                              <div class="size_input">
                                <input type="text" class="measure_input"   name="hip"  placeholder="eg:30 cm"   
                                data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$" data-parsley-type="digits"
                                required="" data-parsley-required-message="Please insert hip size."
                                data-parsley-errors-container="#pant_hip1" value="<?php echo $measurement->hip; ?>"/>
                              </div>
                              <div id="pant_hip1" class="custom_measure_parse_error"></div>
                            </li> 

                            <li>
                              <div class="prev_size"><p>3</p></div>
                              <div class="size_name">Inseam</div>
                              <div class="size_input">
                                <input type="text" class="measure_input"  name="inseam"  placeholder="eg:30 cm" 
                                data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$" data-parsley-type="digits"
                                required="" data-parsley-required-message="Please insert inseam size." 
                                data-parsley-errors-container="#inseam1" value="<?php echo $measurement->inseam; ?>"/>
                              </div>
                              <div id="inseam1" class="custom_measure_parse_error"></div>
                            </li>
                            </ol>
                          </div>

                        </div>
                    </div>
                </div>

            </div>
                 

                <div class="save_measure_outer">
                  <div class="measure_status"></div>
                  <button class="save measure" id="savemesh">Save my measurement</button>
                </div>

        </div>
                <!--  Sectio1 End here  -->

              </form>

              </div>
            </div>
         
      </div>
    </div>
  
           <div class="how_to_measure">
               <div class="container">
                   <div class="col-md-2"></div>
                   <div class="col-md-8"> 
                       <div class="measure_tips">
                       
                          <h5>How to Measure</h5>
                           
                          <div class="measre_sect">
                            <p><b>Bust:</b>Bust taken from underarm to underarm. Measure under your arms at the fuller part of your Bust, keep tape parallel to the shoulder blades.</p>
                          </div>
                           
                           
                          <div class="measre_sect">
                            <p><b>Waist: :</b>Measure around your natural Waistline, Generally 10” from  underarm, keep the tape comfortably loose.</p>
                          </div>
                           
                           
                          <div class="measre_sect">
                            <p><b>Pant Waist:</b>Measurement taken a few inches below your natural waist. The waist band will rest at about1-2. </p>                           
                          </div>

                       </div>
                   </div>
                   <div class="col-md-2"></div>
                </div>
            </div> 


  </div>
</div>

      <!-- footer_block -->



<div class="banner_fth">
         <div class="container">
            <div class="row">
               <div class="col-md-2 col-sm-12 col-xs-12">
               </div>
               <div class="col-md-8 col-sm-12 col-xs-12">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="foot_img_outer">
                           <div class="foot_img">
                              <img src="<?php echo base_url();?>assets/images/phones.png">
                           </div>
                        </div>
                        <div class="foot_b contnt">
                           <h4>Contact Us</h4>
                           <ul class="ft_link">
                              <li>Tel:+91 985 9548</li>
                              <li>Tel:+91 985 9548</li>
                              <li>support@zyva.com</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="foot_img_outer">
                           <div class="foot_img">
                              <img src="<?php echo base_url();?>assets/images/location.png">
                           </div>
                        </div>
                        <div class="foot_b">
                           <h4>Visit Us</h4>
                           <ul class="ft_link">
                              <li>Zyva Fashions</li>
                              <li> Market Road,Kakanad</li>
                              <li>Kochi </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="foot_img_outer">
                           <div class="foot_img">
                              <img class="acnt" src="<?php echo base_url();?>assets/images/acount.png">
                           </div>
                        </div>
                        <div class="foot_b">
                           <h4> Follow Us</h4>
                           <ul class="ft_link">
                              <li>www.zyva.co.in</li>
                              <li>
                                 <div class="social_icon">
                                    <ul class="social">
                                       <li><i class="fa fa-facebook fa-lg lght_rse" aria-hidden="true"></i></li>
                                       <li><i class="fa fa-twitter fa-lg lght_rse" aria-hidden="true"></i>
                                       </li>
                                       <li><i class="fa fa-linkedin fa-lg lght_rse" aria-hidden="true"></i>
                                       </li>
                                    </ul>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-2 col-sm-12 col-xs-12">
               </div>
            </div>
            <div class="col-md-12 col-sm-6 col-xs-12">
               <div class="col-md-2 col-sm-6 col-xs-12"></div>
               <div class="col-md-8 col-sm-6 col-xs-12 no_padng">
                  <div class="footer_navs">
                      <ul class="nav navbar-nav navbar-right nav_font foot_rht">
                        <li><a class="cool-link" href="<?php echo base_url('About/about') ?>">About Us</a></li>
                        <li><a class="cool-link" href="<?php echo base_url('About/contact'); ?>">Contact Us</a></li>
                        <li><a class="cool-link" href="#">Terms </a></li>
                        <li><a class="cool-link" href="#"> Policies</a></li>
                        <li><a class="cool-link" href="#">Download Measurement Form</a></li>
                     </ul>
                  </div>
                  <div class="col-md-12 col-sm-6 col-xs-12">
                     <div class="col-md-4 col-sm-6 col-xs-12"></div>
                     <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="footer_navs_cards">
                           <ul>
                              <li><img src="<?php echo base_url();?>assets/images/payment_a.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/payment_b.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/payment_c.png"></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-6 col-xs-12"></div>
                  </div>
                  <div class="col-md-12 col-sm-6 col-xs-12">
                     <div class="col-md-3 col-sm-6 col-xs-12"></div>
                     <div class="col-md-6">
                        <div class="footer_navs_cards">
                           <p>zyva.co.in 2016.All Rights Reserved</p>
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12"></div>
                  </div>
               </div>
               <div class="col-md-2"></div>
            </div>
         </div>
      </div>

      
    
      <!-- footer_block end -->
      <!-- Modal login -->
     
      <!-- Modal login end -->
          
          
          
           <!-- popover -->
      
<!-- Modal -->
<div class="modal fade" id="myModal_pop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Modal</h4>
      </div>
      <div id="calendar" class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->!--  popover end -->   
          
          
          
      <!-- Modal register -->
     

      <!-- Modal register end -->
      <!-- Bootstrap core JavaScript
         ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
      
     
      <a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;"></a>
      



      <script>


 //   function post_ajax(url, data) {
 //  var result = '';
 //  $.ajax({
 //         type: "POST",
 //         url: url,
 //    data: data,
 //    success: function(response) {
 //      result = response;

 //    },
 //    error: function(response) {
 //      result = 'error';
 //    },
 //    async: false
 //    });

 //    return result;
 // }


 //   ///add to measurement///


 //   $('#savemesh').on('click', function (e) {
 //     e.preventDefault();
 //     alert("hi");


 //     var value=$('#measurement').serialize();

 //       alert(value);

 //       var data = {value:value};
 //       var url = base_url+'Measurement/add_measurement';
 //       var result = post_ajax(url, data);
 //       obj = JSON.parse(result);

 //       console.log(obj.status);
 //       if(obj.status=="success"){
 //         $(".errormsg").html('');
 //         $(".errormsg").html('<p class="error">'+obj.message+'</p>');
 //         setTimeout(function(){$(".error_div").hide();}, 3000);
 //       }
 //       else{

 //         $(".errormsg").html('');
 //         $(".errormsg").html('<p class="error">'+obj.message+'</p>');
 //         setTimeout(function(){$(".error_div").hide();}, 3000);
 //       }



 //     });


 //   </script>
  

