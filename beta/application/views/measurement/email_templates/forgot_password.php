<html>
	<head>

		<!-- <link href="http://192.168.138.31/TRAINEES/Nikhila/zyva_latest/assets/css/email_style.css" rel="stylesheet"> -->
	</head>

	<body>
		<div style="width:90%; margin:0  auto;">
			<div style=" width:100%; float:left; background-image: url('<?php echo base_url();?>assets/images/bg_image.png ?>'); background-repeat: no-repeat; background-position: top;">
				<div class="width_80" style="width:80%; margin: 0 auto;">
					<div class="logo" style="text-align:center">
						<img src="http://192.168.138.31/TRAINEES/Nikhila/zyva_latest/assets/images/logo.png">
					</div>
					<div class="email_container">
						<div class="menu_container" style="margin-top: 20px; width: 100%; border-top: 1px solid #dcdcdc; border-bottom: 1px solid #dcdcdc;">
							<!-- =================INCLUDE HEADER======================= -->
							<?php include 'header.php'; ?>
							<!-- =================INCLUDE HEADER======================= -->
						</div>
						
						<h4 style="margin-top:100px; color: #5b4180; font-size: 20px; font-family: 'Roboto', sans-serif; font-weight: 100;font-style: italic;width: 100%;">Hi <?php echo $reg_name; ?>,</h4>
						<p style="color: #5b4180;font-size: 20px;font-family: 'Roboto', sans-serif; font-weight: 100; font-style: italic; width: 100%;">
						Your password change request has been received. Please click link to reset your password: <?php echo $link; ?><br> 
						

						<div style="width:100%; float:left" >
							<h4 style="margin-bottom:10px; color: #5b4180; font-size: 20px; font-family: 'Roboto', sans-serif;font-weight: 100;font-style: italic;width: 100%;">Thank You!</h4>
							<h4 style="margin-top:0px; margin-bottom:100px; color: #5b4180; font-size:20px; font-family: 'Roboto', sans-serif;font-weight: 100;font-style: italic;width: 100%;">Zyva Team</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer_width" style="width:90%; margin:0  auto;">	
			<?php include 'footer.php'; ?>
		</div>

			
	</body>

<html>
