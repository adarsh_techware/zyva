<!--================== Modal login satrt ===================== -->
<div class="modal fade " id="myModal" role="dialog">
   <div class="modal-dialog mod_width nw-mdl-k">
      <!-- Modal content-->      
      <div class="modal-content">
         <div class="modal-body login_modal ">
            <div class="col-md-12 no_padng">
               <form method="POST" id="login_form">
                  <div class="col-md-6 no_padng">
                     <div class="login_section1">
                        <div class="padng_outer">
                           <img class="log_zyva" src="<?php echo base_url();?>assets/images/zya_logo_blck.png">                           
                           <button type="button" class="close hidden-lg hidden-md hidden-sm" data-dismiss="modal"> 
                           	<img src="<?php echo base_url();?>assets/images/modal_close.png"></button>                                                      
                           	<input type="text"  name="email" required="" class="text_box_input"                               
                           	data-parsley-trigger="change" autocomplete="off"                               
                           	data-parsley-required-message="Email is required."                               
                           	data-parsley-errors-container="#uname"  placeholder="Email">                              
                           <div id="uname" class="uname_pass"></div>
                           <div class="pass_height">
                              <input type="password" name="password" required="" class="text_box_input"                                 
                              data-parsley-trigger="change"                                 
                              autocomplete="off" data-parsley-required-message="Password is required."                                  
                              data-parsley-errors-container="#pass" placeholder="Password">                                                               
                              <div id="pass" class="pass_error"></div>
                              <div class="error_div pass_error"></div>
                           </div>
                           <button  type="button" class="log_but" id="login_button" >Login</button>                           
                           <p class="forgot_pass" id="forgot_password"> <a data-toggle="modal" data-dismiss="modal" data-target="#forgot" >Forgot password ?</a></p>
                        </div>
                     </div>
                  </div>
               </form>
               <div class="col-md-6 no_padng">
                  <div class="login_section2">
                     <div class="reg_outer">
                        <button type="button" class="close hidden-xs" data-dismiss="modal"> <img src="<?php echo base_url();?>assets/images/modal_close.png"></button>                        
                        <div class="new_user">
                           <p>New User?</p>
                           <!--<h4 id="reg" data-toggle="modal" data-target="#myModal_reg">register now</h4>-->                           
                           <h4 id="reg" data-toggle="modal" data-dissmiss="modal" data-target="#myModal_reg">register now</h4>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Modal login end -->

<!-- Modal forgot -->
<div class="modal fade" id="forgot" role="dialog">
   <div class="modal-dialog forgot_modal nw-mdl-k">
      <!-- Modal content-->      
      <div class="modal-content">
         <div class="modal-body login_modal ">
            <div class="col-md-12 no_padng">
               <div class="col-md-6 no_padng">
                  <div class="login_section1">
                     <div class="padng_outer">
                        <img class="log_zyva" src="<?php echo base_url();?>assets/images/zya_logo_blck.png">                        
                        <button type="button" class="close hidden-lg hidden-md hidden-sm" data-dismiss="modal"> <img src="<?php echo base_url();?>assets/images/modal_close.png"></button>                        
                        <form method="POST" action="" id="forgotanfp" data-parsley-validate="true" class="validate">
                           <div class="pass_height">
                              <input type="email" id="femail" name="email" class="text_box_input" placeholder="Email"                                 
                              tabindex="4" required="" data-parsley-required-message="Please insert email."                                  
                              data-parsley-errors-container="#forgot_email">                              
                              <div class="pass_error" id="forgot_email"></div>
                              <div class="renew_pass pass_error"></div>
                           </div>
                           <div class="view_loader"></div>
                           <button type="button" class="log_but" id="land_logforget" >Submit</button>                          
                        </form>
                        <p class="forgot_pass"> <a data-toggle="modal"  data-dismiss="modal" data-target="#forgot"></a></p>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 no_padng">
                  <div class="login_section2">
                     <div class="padng_outer">
                        <button type="button" class="close hidden-xs" data-dismiss="modal"> <img src="<?php echo base_url();?>assets/images/modal_close.png"></button>                        
                        <div class="new_user">
                           <p>New User?</p>
                           <h4 id="from_forgot_register" data-toggle="modal"  data-dissmiss="modal" data-target="#myModal_reg">register now</h4>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--==================== Modal forgot end ====================->

<!--==================== Modal register ====================-->
<div class="modal fade" id="myModal_reg" role="dialog">
   <div class="modal-dialog reg_mod_width">
      <!-- Modal content-->      
      <div class="modal-content">
         <div class="modal-body login_modal">
            <div class="reg_modals">
            	<form method="POST" id="signup_form"  autocomplete="off">
                   <div class="lft_reg">
                        <div class="register_padding">
                          <img class="log_zyva" src="<?php echo base_url();?>assets/images/zya_logo_blck.png">                              
                          <button type="button" class="close" style="float:right; padding-right:30px" data-dismiss="modal"> 
                          <img src="<?php echo base_url();?>assets/images/modal_close.png"></button>                              
                          <div class="row">
                            <div class="col-md-6">
                              <div class="reg_input">
                                 <input type="text" class="text_box_input reg_input_width " name="first_name" id="first_name"                                  
                                 data-parsley-pattern="^[a-zA-Z\  \/]+$" required=""                                  
                                 placeholder="First Name" tabindex="1"                                 
                                 data-parsley-required-message="Please insert first name."                                  
                                 data-parsley-errors-container="#reg_fname">                                 
                                 <div class="reg_error" id="reg_fname"></div>
                              </div>
                       		</div>
                       		<div class="col-md-6">
                              <div class="reg_input">
                                <input type="text" class="text_box_input reg_input_width " id="last_name" name="last_name"                                     
                                data-parsley-pattern="^[a-zA-Z\  \/]+$" required=""                                     
                                placeholder="Last Name" tabindex="2"                                    
                                data-parsley-required-message="Please insert last name."                                     
                                data-parsley-errors-container="#reg_lname">                                    
                                <div class="reg_error" id="reg_lname"></div>
                               </div>
                           	</div>
                           </div>

                           <div class="row">
	                           	<div class="col-md-6">
	                              <div class="reg_input">
	                                 <input type="text" class="text_box_input reg_input_width " name="telephone" id="mobile"                                  
	                                 data-parsley-trigger="keyup" data-parsley-type="digits"                                  
	                                 data-parsley-minlength="8" data-parsley-maxlength="15"                                   
	                                 data-parsley-pattern="^[0-9]+$" required="" placeholder="Telephone"                                  
	                                 tabindex="3" data-parsley-required-message="Please insert phone number."                                  
	                                 data-parsley-errors-container="#reg_phone">                                 
	                                 <div class="reg_error" id="reg_phone"></div>
	                              </div>
	                            </div>
	                            <div class="col-md-6">
	                              <div class="reg_input">
	                                <input type="email" class="text_box_input reg_input_width " id="email" name="email"                                     
	                                required="" placeholder="Email" tabindex="4"                                    
	                                data-parsley-required-message="Please insert email."                                     
	                                data-parsley-errors-container="#reg_email">                                    
	                                <div class="reg_error" id="reg_email"></div>
	                                <div class="error_div1"  style="color:red"></div>
	                             </div>
	                            </div>
                           </div>

                           <div class="row">
                           		<div class="col-md-6">  
	                              	<div class="reg_input">
	                                 <input type="password" class="text_box_input reg_input_width " id="password"                                  
	                                 data-parsley-trigger="change"  data-parsley-minlength="2"                                  
	                                 data-parsley-maxlength="15" required=""                                 
	                                  name="password" placeholder="Password" tabindex="5"                                 
	                                  data-parsley-required-message="Please insert password."                                  
	                                  data-parsley-errors-container="#reg_pass">                                 
	                                 <div class="reg_error" id="reg_pass"></div>
	                              	</div>
                           		</div>
	                           	<div class="col-md-6">  
	                              <div class="reg_input">
	                                <input type="password" class="text_box_input reg_input_width " id="c_password" name="c_password"                                     
	                                data-parsley-trigger="change" data-parsley-minlength="2" data-parsley-maxlength="15"                                     
	                                placeholder="Confirm Password" tabindex="6"  required=""                                  
	                                data-parsley-required-message="Please insert confirm password."                                     
	                                data-parsley-errors-container="#reg_cpass">                                    
	                                <div class="reg_error" id="reg_cpass"></div>
	                                <div class="error_div2"  style="color:red"></div>
	                             </div>
	                           	</div>
                           </div>
                        </div>

                        <div class="col-md-12 no_padng">
                           <div class="col-md-6 reg_pad_right">
                              <div class="margn_out">
                                 <div class="check_outer">
                                    <div class="reg_checks">                                       
                                    	<input id="checkbox-1" class="checkbox-custom" value="agree" required="" name="terms" type="checkbox" checked tabindex="7">                                       
                                    	<label for="checkbox-1"   class="checkbox-custom-label"> I Accept the Terms and Conditions </label>                                    
                                    </div>
                                    <div>

                                     <input id="checkbox-2" class="checkbox-custom"   value="on"  name="Subscribe" type="checkbox" tabindex="8">                                       
                                     <label for="checkbox-2"  class="checkbox-custom-label"> Subscribe to Newsletter</label>                                    
                                 </div>
                                 </div>
                                 <div class="check_inner login_button_space">                                    
                                 	<button class="log_but signup_button" type="button"  id="signup_button"> Sign Up</button>                                 
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="acount">
                                 <p>Already have an Account</p>
                                 <h4 id="already_acc_signin"><a data-toggle="modal" data-dismiss="modal" data-target="#myModal">Log In here  </a></h4>
                              </div>
                           </div>
                        </div>
                     
                   </div>
                </form>
                <!-- <form method="POST" id="signup_form"  autocomplete="off">
                   <div class="lft_reg">
                   	  <div class="col-md-12 no_padng">
                        <div class="col-md-6 no_padng">
                           <div class="padng_outer_reg pad_bot">
                              <img class="log_zyva" src="<?php echo base_url();?>assets/images/zya_logo_blck.png">                              
                              <button type="button" class="close hidden-lg hidden-md hidden-sm" data-dismiss="modal"> 
                              	<img src="<?php echo base_url();?>assets/images/modal_close.png"></button>                              
                              <div class="reg_input">
                                 <input type="text" class="text_box_input" name="first_name" id="first_name"                                  
                                 data-parsley-pattern="^[a-zA-Z\  \/]+$" required=""                                  
                                 placeholder="First Name" tabindex="1"                                 
                                 data-parsley-required-message="Please insert first name."                                  
                                 data-parsley-errors-container="#reg_fname">                                 
                                 <div class="reg_error" id="reg_fname"></div>
                              </div>
                              <div class="reg_input">
                                 <input type="text" class="text_box_input" name="telephone" id="mobile"                                  
                                 data-parsley-trigger="keyup" data-parsley-type="digits"                                  
                                 data-parsley-minlength="8" data-parsley-maxlength="15"                                   
                                 data-parsley-pattern="^[0-9]+$" required="" placeholder="Telephone"                                  
                                 tabindex="3" data-parsley-required-message="Please insert phone number."                                  
                                 data-parsley-errors-container="#reg_phone">                                 
                                 <div class="reg_error" id="reg_phone"></div>
                              </div>
                              <div class="reg_input">
                                 <input type="password" class="text_box_input no_margn" id="password"                                  
                                 data-parsley-trigger="change"  data-parsley-minlength="2"                                  
                                 data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z0-9\  \/]+$" required=""                                 
                                  name="password" placeholder="Password" tabindex="5"                                 
                                  data-parsley-required-message="Please insert password."                                  
                                  data-parsley-errors-container="#reg_pass">                                 
                                 <div class="reg_error" id="reg_pass"></div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 no_padng">
                           <div class="padng_outer_reg">
                              <button type="button" class="close hidden-xs" data-dismiss="modal"> <img src="<?php echo base_url();?>assets/images/modal_close.png"></button>                              
                              <div class="reg_form">
                                 <div class="reg_input">
                                    <input type="text" class="text_box_input" id="last_name" name="last_name"                                     
                                    data-parsley-pattern="^[a-zA-Z\  \/]+$" required=""                                     
                                    placeholder="Last Name" tabindex="2"                                    
                                    data-parsley-required-message="Please insert last name."                                     
                                    data-parsley-errors-container="#reg_lname">                                    
                                    <div class="reg_error" id="reg_lname"></div>
                                 </div>
                                 <div class="reg_input">
                                    <input type="email" class="text_box_input" id="email" name="email"                                     
                                    required="" placeholder="Email" tabindex="4"                                    
                                    data-parsley-required-message="Please insert email."                                     
                                    data-parsley-errors-container="#reg_email">                                    
                                    <div class="reg_error" id="reg_email"></div>
                                    <div class="error_div1"  style="color:red"></div>
                                 </div>
                                 <div class="reg_input">
                                    <input type="password" class="text_box_input no_margn" id="c_password" name="c_password"                                     
                                    data-parsley-trigger="change" data-parsley-minlength="2" data-parsley-maxlength="15"                                     
                                    data-parsley-pattern="^[a-zA-Z0-9\  \/]+$" required=""                                     
                                    placeholder="Confirm Password" tabindex="6"                                    
                                    data-parsley-required-message="Please insert confirm password."                                     
                                    data-parsley-errors-container="#reg_cpass">                                    
                                    <div class="reg_error" id="reg_cpass"></div>
                                    <div class="error_div2"  style="color:red"></div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="col-md-12 no_padng">
                           <div class="col-md-6 reg_pad_right">
                              <div class="margn_out">
                                 <div class="check_outer">
                                    <div class="reg_checks">                                       
                                    	<input id="checkbox-1" class="checkbox-custom" value="agree" required="" name="terms" type="checkbox" checked tabindex="7">                                       
                                    	<label for="checkbox-1"   class="checkbox-custom-label"> I Accept the Terms and Conditions </label>                                    
                                    </div>
                                    <div>

                                     <input id="checkbox-2" class="checkbox-custom"   value="on"  name="Subscribe" type="checkbox" tabindex="8">                                       
                                     <label for="checkbox-2"  class="checkbox-custom-label"> Subscribe to Newsletter</label>                                    
                                 </div>
                                 </div>
                                 <div class="check_inner login_button_space">                                    
                                 	<button class="log_but" type="button"  id="signup_button"> Sign in</button>                                 
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="acount">
                                 <p>Already have an Account</p>
                                 <h4 id="already_acc_signin"><a data-toggle="modal" data-dismiss="modal" data-target="#myModal">Log In here  </a></h4>
                              </div>
                           </div>
                        </div>
                     </div>
                   </div>
                </form> -->
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Modal register  end-->

<!-- MODAL FOR SIZE GUIDE START -->
<div class="modal fade" id="size_guide_modal" role="dialog">
   <div class="modal-dialog size_guide_width">
      <!-- Modal content-->      
      <div class="modal-content">
         <div class="modal-body login_modal">
            <div class="reg_modals">
               <div class="col-md-12 no_padng">
                  <form method="POST"  autocomplete="off">
                     <div class="lft_reg">
                        <div class="col-md-6 col-sm-6 col-xs-6 no_padng">
                           <div class="padng_outer_reg pad_bot">                              
                           	<img class="log_zyva" src="<?php echo base_url();?>assets/images/zya_logo_blck.png">                           
                           </div>
                        </div>
                        <div class="col-md-6  col-sm-6 col-xs-6 no_padng">
                           <div class="padng_outer_reg pad_bot">                               
                           	<button type="button" class="close" data-dismiss="modal"> 
                           		<img src="<?php echo base_url();?>assets/images/modal_close.png"></button>                           
                           	</div>
                        </div>
                        <div class="col-md-12">
                           <div class="size_guides">                               
                           	<img src="<?php echo base_url('assets/images/size_guide.jpg'); ?>" />                           
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- MODAL FOR SIZE GUIDE END -->

<!-- MODAL FOR CUSTOM SIZE DESCRIPTION START -->
<div class="modal fade" id="custom_size" role="dialog">
   <div class="modal-dialog size_guide_width">
      <!-- Modal content-->      
      <div class="modal-content">
         <div class="modal-body login_modal">
            <div class="reg_modals">
               <div class="col-md-12 no_padng">
                  <form method="POST" autocomplete="off">
                     <div class="lft_reg">
                        <div class="col-md-6 col-sm-6 col-xs-6 no_padng">
                           <div class="padng_outer_reg pad_bot">                              
                           	<img class="log_zyva" src="<?php echo base_url();?>assets/images/zya_logo_blck.png">                           
                           </div>
                        </div>
                        <div class="col-md-6  col-sm-6 col-xs-6 no_padng">
                           <div class="padng_outer_reg pad_bot">                               
                           	<button type="button" class="close" data-dismiss="modal"> 
                           		<img src="<?php echo base_url();?>assets/images/modal_close.png"></button>                           
                           	</div>
                        </div>
                        <div class="col-md-12">
                           <div class="size_guides" style="text-align:center;margin-bottom:100px!important;">
                              <h4>CUSTOM SIZE DESCRIPTION</h4>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- MODAL FOR CUSTOM SIZE END -->
<div class="banner_fth">
   <div class="container">
      <div class="row">
         <div class="download_android">            
         	<button> Download Android App </button>         
         </div>
         <div class="col-md-2 col-sm-12 col-xs-12">         
         </div>
         <div class="col-md-8 col-sm-12 col-xs-12 remove_media_pad">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="foot_img_outer">
                     <div class="foot_img">
                        <img src="<?php echo base_url();?>assets/images/phones.png">                        
                        <h4>  Contact Us</h4>
                     </div>
                  </div>
                  <div class="foot_b contnt">
                     <ul class="ft_link">
                        <li><a href="tel:+919526039555">Tel:+91 952 6039555</a></li>
                        <li><a href="tel:+919562481145">Tel:+91 956 2481145</a></li>
                        <li><a href="mailto:name@email.com">sales@zyva.in</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="foot_img_outer">
                     <div class="foot_img">
                        <img src="<?php echo base_url();?>assets/images/location.png">                         
                        <h4>Visit Us</h4>
                     </div>
                  </div>
                  <div class="foot_b">
                     <ul class="ft_link">
                        <!-- <li>Maryam Apparels</li>
                        <li>Kadungalloor Road</li>
                        <li>Muppathadam, Edayar</li>
                        <li>Kerala 683110</li> -->
                        <li>DOOR NO. 66/6500,</li>
                        <li>2nd Floor, Matsun Towers,</li>
                        <li>A K Seshadn Road,</li>
                        <li>Ernakulam, Kochi - 682 011</li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="foot_img_outer">
                     <div class="foot_img">
                        <img class="acnt" src="<?php echo base_url();?>assets/images/account.png">                        
                        <h4> Follow Us</h4>
                     </div>
                  </div>
                  <div class="foot_b">
                     <ul class="ft_link">
                        <li>www.zyva.in</li>
                        <li>
                           <div class="social_icon">
                              <ul class="social">
                                 <li><a href="https://www.facebook.com/zyvafashions/"><i class="fa fa-facebook fa-lg lght_rse" aria-hidden="true"></i></a></li>
                                 <li><i class="fa fa-twitter fa-lg lght_rse" aria-hidden="true"></i>                                 
                                 </li>
                                 <li><i class="fa fa-linkedin fa-lg lght_rse" aria-hidden="true"></i>                                 
                                 </li>
                              </ul>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-2 col-sm-12 col-xs-12">         </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="col-md-2 col-sm-12 col-xs-12"></div>
         <div class="col-lg-8 col-md-10 col-sm-10 col-xs-12 no_padng">
            <div class="footer_navs">
               <ul class="nav navbar-nav navbar-right nav_font foot_rht">
                  <li class="foot_about"><a class="cool-link" href="<?php echo base_url('About/about') ?>">About Us</a></li>
                  <li class="foot_contact"><a class="cool-link" href="<?php echo base_url('About/contact'); ?>">Contact Us</a></li>
                  <li class="foot_terms"><a class="cool-link" href="<?php echo base_url('terms'); ?>">Terms </a></li>
                  <li class="foot_policy"><a class="cool-link" href="<?php echo base_url('terms/policy'); ?>"> Policies</a></li>
                  <li class="foot_contact foot_disc"><a class="cool-link" href="<?php echo base_url('terms/disclaimer'); ?>">Disclaimer</a></li>
                  <li class="foot_form"><a class="cool-link" href="<?php echo base_url('assets/images/size_guide.jpg'); ?>" download>Measurement Form</a></li>
               </ul>
            </div>
            <div class="col-md-12 col-sm-6 col-xs-12 remove_media_pad">
               <div class="col-md-4 col-sm-6 col-xs-12"></div>
               <div class="col-md-4 col-sm-6 col-xs-12 remove_media_pad">
                  <div class="footer_navs_cards">
                     <ul>
                        <li><img src="<?php echo base_url();?>assets/images/payment_a.png"></li>
                        <li><img src="<?php echo base_url();?>assets/images/payment_b.png"></li>
                        <li><img src="<?php echo base_url();?>assets/images/payment_c.png"></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-4 col-sm-6 col-xs-12"></div>
            </div>
            <div class="col-md-12 col-sm-6 col-xs-12 remove_media_pad">
               <div class="col-md-3 col-sm-6 col-xs-12"></div>
               <div class="col-md-6  remove_media_pad">
                  <div class="footer_navs_cards">
                     <p>zyva.in 2016.All Rights Reserved</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6 col-xs-12"></div>
            </div>
         </div>
         <div class="col-md-2"></div>
      </div>
   </div>
</div>