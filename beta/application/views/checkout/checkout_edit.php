<form method="POST" id="payment">
      <div class="check_out_outer">
         <div class="container">
            <div class="row checkout_page_start">
               <div class="col-md-4 col-sm-12 col-xs-12">
			   <form method="POST" id="myorder">
			   
                  <div class="order_summary_outer checkout_order">
                     <div class="order_summary_inner">
                        <h4 class="order_pad">order summary</h4>
						<?php
						$i=0;
						$total=0;
						
						  foreach ($cartlist as $cart) {?>
                        <div class="orders" id="order">
						
                           <div class="row">
                              <div class="col-md-4 col-sm-6 col-xs-12">
                                 <div class="order_dtl">
                                    <img src="<?php echo $cart->product_image;?>">
                                 </div>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-12" style="padding-left:0px;">
                                 <div class="order_inf">
                                    <h4><?php echo $cart->product_name;?></h4>
									                  <input type="hidden"  name="product_id[]" value="<?php echo $cart->product_id;;?>" >
                                    <h4><?php echo $cart->quantity;?> x &#8377;<?php echo $cart->price;?></h4>
									                  
                                    <div class="dres_inf">
                                       <p class="cart_color">Color</p>
                                       <p class="clr_dre" style="background-color:<?php echo $cart->color_name;?> "></p>
									                   
                                    </div>
									
									
									
                                    <div class="dres_inf">
                                       <p>Size</p>
                                       <p class="cart_size"><?php echo $cart->size_type;?></p>
									                     
                                    </div>
									
									
                									 <!-- <p>QTY</p>
                                      <p class=""><?php echo $cart->quantity;?></p> -->
					                         
					  
                                 </div>
                              </div>
                           </div>
                           <div class="remove_but">
                              <div class="row">
                                 <div class="col-md-3 col-sm-6 col-xs-12 pad_rht">
								                     <div class="edit">
                                       <i class="fa fa-pencil gray_clr" aria-hidden="true"></i>
                           
                                				 <p>
                                				    <a href="<?php echo base_url('product_info/'.$cart->prod_display);?>"><button  class="remove_btn1" type="button"> edit</button></a>
                                				 </p>
									
                                    </div>
								 
								 
                                 
                                     
									
									
									
									
                                 </div>
                                 <div class="col-md-3 col-sm-6 col-xs-12 pad_rht pad_lft">
                                    <div class="remove">
                                       <i class="fa fa-times gray_clr" aria-hidden="true"></i>
                                          <p class="remove_pdt" data-id="<?php echo $cart->id;?>"> &nbsp;remove</p>
                                    </div>
                                 </div>
                                 <div class="col-md-6 col-sm-6 col-xs-12"></div>
                              </div>
                           </div>
						 
                        </div>
						
                      
                          <?php 
						    $q=$cart->quantity;
						  
							    $total+= $cart->price*$q;
							    $i++; 
								}
								?>
                         <div class="row">
                         <div class="col-md-6">
                             <div class="sub_tot order_pad">Sub Total</div>
                             
                             
                             </div>
                          <div class="col-md-6">
                               <div class="sub_tot total_rup">
                              &#8377; <?php echo $total;?>
                              </div>
                             
                             </div>
                         </div>
                         
                         
                     </div>
                  </div>
				  </form>
               </div>

               <div class="row">
                <div class="col-md-4 bill">Billing Address</div>
                <div class="col-md-4 ship">Shipping and Payment</div>
                <div class="col-md-4 ord_inf">Order Information</div>
               </div>

               <div class="row">
                <div class="col-md-12">
                  <div class="checkout_billing">
                    <div class="text_boxs">
                              <input type="text" name="firstname"  value="<?php echo $address->firstname; ?>" placeholder="First name" 
                              data-parsley-trigger="change" 
                              data-parsley-pattern="^[a-zA-Z\  \/]+$" 
                              required="" data-parsley-required-message="Please insert your first name."
                              data-parsley-errors-container="#first_name"/>

                              <div id="first_name" class="custom_parse_error"></div>
                            </div>

                            <div class="text_boxs">
                              <input type="text" name="lastname" value="<?php echo $address->lastname; ?>"placeholder="Last name" 
                              data-parsley-trigger="change" 
                              data-parsley-pattern="^[a-zA-Z\  \/]+$"
                              required="" data-parsley-required-message="Please insert your last name."
                              data-parsley-errors-container="#last_name"/>

                              <div id="last_name" class="custom_parse_error"></div>  
                            </div>
                              
                              <div class="text_boxs">
                              <input type="text" name="address_1"  value="<?php echo $address->address_1; ?>"placeholder="Address Line 1" 
                              data-parsley-trigger="change" 
                              data-parsley-minlength="2"  data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"  
                              required="" data-parsley-required-message="Please insert address line 1."
                              data-parsley-errors-container="#add_line1"/>

                              <div id="add_line1" class="custom_parse_error"></div> 
                            </div>
                                 
                              
                            <div class="text_boxs"> 
                              <input type="text" name="address_2"  value="<?php echo $address->address_2; ?>"placeholder="Address Line 2" 
                              data-parsley-trigger="change" 
                              data-parsley-minlength="2"  data-parsley-pattern="^[a-zA-Z\ 0-9 \/]+$" 
                              required="" data-parsley-required-message="Please insert address line 2."
                              data-parsley-errors-container="#add_line2"/>

                              <div id="add_line2" class="custom_parse_error"></div>
                            </div> 


<!-- next column starts -->
                            </div>
                            <div class="checkout_billing">
                            <?php if($statedetails > 0) { ?>
                              
                                <div class="text_boxs">
                                <div class="place-select slct_drop ">
                              
                                   <select class="sscct" onchange="selectCity(this.options[this.selectedIndex].value)"  name="state_id" id="state_id" required="" data-parsley-required-message="Please select state."
                                    data-parsley-errors-container="#states"/>
                                      <option selected="" value="">State</option>

                                       <?php foreach($statedetails as $statedetails){ ?>

                                        <option value="<?php echo $statedetails->id;?>"<?php if ($statedetails->id == $data1->state_id ){ ?>
                                        selected = "selected" <?php } ?>><?php echo $statedetails->state_name;?></option>
                                      <?php } ?>
                                   
                                    </select>
                                </div>
                                <div id="states" class="custom_parse_error"></div>

                              </div>
                                            
                                            
                                            
                              <div class="text_boxs">              
                               <div class="place-select slct_drop">
                                  <select class="sscct"   id="city_dropdown" name="city_id"  onchange="" required="" data-parsley-required-message="Please select city."
                                    data-parsley-errors-container="#city"/>
                                    <option selected="" value="-1">City</option>

                                    <?php foreach($citydetails as $citydetails) { ?>

                                      <option value="<?php echo $citydetails->id;?>"<?php if ($citydetails->id == $data1->city_id){ ?>
                                      selected = "selected" <?php } ?>><?php echo $citydetails->city_name;?></option>
                                   
                                    <?php } ?>
                                  </select>
                                </div>
                                <div id="city" class="custom_parse_error"></div>
                              </div>                
                          
                             <?php } ?>

                                     
                            <div class="text_boxs">        
                              <input type="text" name="Zip"  value="<?php echo $address->Zip; ?>"placeholder="Zip Code" 
                              data-parsley-trigger="change" 
                              data-parsley-minlength="2" data-parsley-maxlength="5" data-parsley-pattern="^[0-9\  \/]+$"  
                              required="" data-parsley-required-message="Please insert zip code."
                              data-parsley-errors-container="#zip"/>

                              <div id="zip" class="custom_parse_error"></div>
                            </div>
               
                            <div class="text_boxs">   
                              <input type="text" name="telephone"  value="<?php echo $address->telephone; ?>"placeholder=" Telephone" 
                              data-parsley-trigger="change" 
                              data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$"
                              required="" data-parsley-required-message="Please insert phone number."
                              data-parsley-errors-container="#phone"/>

                              <div id="phone" class="custom_parse_error"></div>
                            </div>
                  </div>
                </div>
               </div>







               
            </div>
         </div>
      </div>
	  </form>
      <!-- footer_block -->
     
      <!-- footer_block end -->
      <!-- Modal login -->
 
      <!-- Modal login end -->
      <!-- Modal register -->

      <!-- Modal register end -->
      <!-- Bootstrap core JavaScript
         ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
     
          
          
          <script>
          $('.shipping').on('click',function(){
            var total = '<?php echo $total; ?>';
            var charge = $(this).val();
            $('#shipping_charge').val(charge);
            if($("#checkbox-5").is(':checked')){
              var gift = $("#checkbox-5").val();
              var sum = parseInt(total)+parseInt(charge)+parseInt(gift);
              $("#abc").html(sum);
              $('#abc1').val(sum);
            }

          })

  $('#checkbox-5').on('click',function(){
    if($("#checkbox-5").is(':checked')){
      var total = '<?php echo $total; ?>';
      var shipping = $('#shipping_charge').val();
      var gift = $(this).val();
      var sum = parseInt(total)+parseInt(shipping)+parseInt(gift);
      $("#abc").html(sum);
      $('#abc1').val(sum);
    } else {
      var total = '<?php echo $total; ?>';
      var shipping = $('#shipping_charge').val();
      var gift = $(this).val();
      var sum = parseInt(total)+parseInt(shipping);
      $("#abc").html(sum);
      $('#abc1').val(sum);
    }

  })

  $('#checkbox-6').on('click',function(){
    $(this).attr('checked','checked');
  })
          /* function add_number() {
            var t = <?php echo $total; ?>;
            var first_number = parseInt(document.getElementById("check1").value);
            var second_number = parseInt(document.getElementById("check2").value);
         result = first_number + second_number + t ;
			
          $("#abc").html(result);
       document.getElementById('abc1').value=result;
	 //  alert(result);
	 
		 
		 
		 
		 
        }*/
          
          
          </script>

