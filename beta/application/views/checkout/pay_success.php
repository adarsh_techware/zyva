<div class="success_container">
  <div class="container">
    <div class="col-md-3"></div>
    <div class="col-md-6 col-sm-12 col-xs-12">

      <div class="success_content">
        <img src="<?php echo base_url();?>assets/images/success.png">
        <h3>Order Successfully Placed</h3>
        <p>Order number: <?php echo $booked->booking_no; ?></p>
      </div>

      <div class="order_summary">
        <p>Order Summary</p><hr class="summary_hr">
        <div class="order_table">
          <div class="order_tab_head">
            <p><?php echo $booked->total_item; ?> Item(s) :<span>&#8377;<?php echo number_format($booked->total_rate, 2, '.', ''); ?></span></p>
          </div>
          <div class="order_table_cont">
              <h4 class="order_subhead">DELIVER TO</h4>
              <p class="order_address">
                <?php echo $booked->delivery_name; ?>,<br>
                <?php echo $booked->address_1.' '.$booked->address_2; ?>,<br/>
                <?php echo get_city_name($booked->city_id).' '.get_state_name($booked->state_id).' '.$booked->Zip; ?><br>
                Mobile : <?php echo $booked->telephone; ?>
              </p>
          </div>
          <div class="order_table_cont no_right_border">
              <h4 class="order_subhead">PAYMENT MODE</h4>
              <p class="order_address">
                <?php echo $booked->payment_type; ?>
              </p>
          </div>

        </div>
      </div>  

    </div>
    <div class="col-md-3"></div>
  </div>
</div>


