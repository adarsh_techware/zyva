<div class="starter-template">
   <!-- wrapper -->
   <div class="full_wdth_wrapper"></div>
   <!-- wrapper end -->
   <!-- tab bar  -->
   <div class="inner_tab ">
      <div class="container">
         <div class="row">
            <div class="navs_tabs">
               <ul>
                  <li> <a>Home</a></li>
                  <li>/</li>
                  <li class="women_clr"><a href="<?php echo base_url('product/'.$cat_id); ?>"><?php echo ucfirst($cat_id); ?></a></li>
                  <?php if($sub_id!=''){?><li>/</li>
                  <li class="women_clr"><a href="<?php echo base_url('product/'.$cat_id.'/'.$sub_id); ?>"><?php echo ucfirst($sub_id); ?></a></li>
                  <?php } ?>
               </ul>
            </div>
            <div class="col-md-3">
               <div class="tab_out">
                  <p><span>category</span></p>

                  <div class="fancy-collapse-panel">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php
                        foreach ($Category as $rs) { ?>
                        

                            <div class="panel panel-default  women_panel">
                                <div class="panel-heading no_head" role="tab" id="heading_<?php echo $rs->id; ?>">
                                  <div <?php if ( $this->uri->uri_string() == 'product/'.$rs->display_name ) : ?> class="active_menu" <?php endif; ?> >
                                    <h4 class="panel-title fnt_sze">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $rs->id; ?>" <?php if($cat_id==$rs->display_name){?> aria-expanded="true" <?php } else {?> aria-expanded="true" <?php } ?> aria-controls="collapse_<?php echo $rs->id; ?>"><?php echo $rs->category_name; ?>
                                        </a>
                                    </h4>
                                  </div>
                                </div>
                                <div id="collapse_<?php echo $rs->id; ?>" class="panel-collapse collapse <?php if($cat_id==$rs->display_name){?> in <?php } ?>" role="tabpanel" aria-labelledby="heading<?php echo $rs->id; ?>">
                                    <div class="panel-body no_bord">
                                        <ul class="tab_list">
                                        <?php foreach ($rs->sub as $row) { ?>
                                           
                                            <li><a href="<?php echo base_url('product/'.$rs->display_name.'/'.$row->sub_display); ?>"><?php echo $row->sub_category; ?></a></li>
                                            <?php } ?>
                                           
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            
                        </div>
                    </div>


                  
                  <p><span>filter</span></p>
                  <div class="layout">
                     <p>Price</p>
                    <div class="price_range">
                      <div id="slider"  class="ranger"></div>
                      <!-- <select name="input-with-keypress-0" id="input-with-keypress-0">
                        <option value="">Min</option>
                        <option value="500">500</option>
                        <option value="1000">1000</option>
                        <option value="2000">2000</option>
                        <option value="3000">3000</option>
                        <option value="4000">4000</option>
                        <option value="5000">5000</option>
                        <option value="6000">6000</option>
                        <option value="7000">7000</option>
                        <option value="8000">8000</option>
                        <option value="9000">9000</option>
                        <option value="10000">10000</option>
                      </select> -->

                      <input type="text" class="keypress_0" name="input-with-keypress-0" id="input-with-keypress-0">
                      <input type="text" class="keypress_1" name="input-with-keypress-1" id="input-with-keypress-1">
                    </div>


                     <!-- <input type="range" multiple="" value="0,100" class="multirange original"> -->

                     <!-- <div class="layout-slider" style="width: 100%;padding-left:20px;">
                        <span style="display: inline-block; width: 220px; padding: 0 5px;">
                            <input id="Slider1" type="slider" name="price" class="ranger" value="10.5;60000" />
                        </span>  
                    </div> -->


                  </div>
                  <form action="" method="POST">
                     <div class="price_lst">
                        <p>Size</p>
                        <div class="sizes_outs" id="btn">
                           <?php
                              foreach ($size as $rs) {
                              ?>
                           <div class="sizes_out">
                              <div class="sizes size_opt" data-id="<?php echo $rs->id;?>"><?php echo $rs->size_type; ?></div>
                              <input type="hidden"  id="size"  name="size" value="<?php echo $rs->size_type; ?>" >
                           </div>
                           <?php } ?>
                           
                        </div>
                        
                     </div>
                  </form>
               </div>
            </div>
            <!--  <div id="search_content"> -->
            <div class="col-md-9">
               <div class="sort_by">
                  <p>Sort by</p>
                  <div class="select-style">
                     <select name="sort" id="filter_search">
                        <option value="product">Newest Product</option>
                        <option value="high">High Price</option>
                        <option value="low">Low Price</option>
                     </select>
                  </div>

                  <!-- <ul class="pagination">
                     <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw1.png"></a></li>
                      <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw2.png"></a></li>
                     
                      <li><a class="active" href="<?php echo site_url('Women/index') ?>"><?php echo $links; ?></a></li>
                      
                      <li><a class="active" href="<?php echo site_url('Women/index') ?>"><?php echo $links; ?></a></li>
                       <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw3.png"></a></li>
                       <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw4.png"></a></li> 
                     </ul> -->

                  <!-- <ul class="pagination">
                     <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw1.png"></a></li>
                     <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw2.png"></a></li>
                     <li><a class="active" href="#">1</a></li>
                     <li><a href="#">2</a></li>
                     <li><a href="#">3</a></li>
                     <li><a href="#">4</a></li>
                     <li><a href="#">5</a></li>
                     <li><a href="#">6</a></li>
                     <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw3.png"></a></li>
                     <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw4.png"></a></li>
                  </ul> -->
               </div>
               <div class="tab-content contnt_tabs">
                  <div id="search_content">
                     <div id="price_content1">
                        <?php
                           if($product) {   ?>
                        <div id="home" class="tab-pane fade in active box_shdw pad_nill">
                        <ul id="port" class="clearfix">
                           <?php

                              //echo '<pre>';background-image: url("../img/z_loader.gif")
                                             //print_r($catimg);die;
                              $i = 1;
                              foreach ($product as $rs) {
                              $class = $i%4==0?'prod_border':'';
                              ++$i;
                              ?>
                           
                              <li class="<?php echo $class; ?>">
                                 <div class="prod_image" style="background-image: url('http://beta.zyva.in/assets/img/z_loader.gif') no-repeat !important;background-size: 80px 60px;">
                                  <a href="<?php echo base_url('product_info/'.$rs->prod_display);?>">
                                    <img data-original="<?php echo base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);?>">
                                    <!--<img src="<?php echo base_url();?>assets/images/404.png">-->
                                  </a>
                                </div>
                                 <h4><?php echo $rs->product_name;?></h4>
                                 <p> &#8377; <?php echo $rs->price;?></p>
                                 <?php
                                    if($rs->quantity==0) { ?>
                                 <div class="stock">Out of stock</div>
                                 <?php }  ?>
                                 <!-- <div class="zyva_lg"><img class="zyva_blck" src="<?php echo base_url();?>assets/images/zyva_lg.png"></div> -->
                              </li>

                           
                           <?php } ?>
                           </ul>
                           <div class="view_more_prod" id="view_more_product">SHOW MORE PRODUCTS</div>
                           <div class="result_div"></div>
                        </div>
                        <?php } 
                           else { ?>
                           <div class="noresult">
                              <h1> No Results Found</h1>
                              <img src="<?php echo base_url();?>assets/images/404.png">
                              <h4>Please checkout our new arrivals <a href="<?php echo base_url('product/women'); ?>">Click here</a></h4>
                            </div>
                          <?php } ?>
                     </div>
                  </div>
                  <div id="menu1" class="tab-pane fade box_shdw">
                     <h3>Menu 1</h3>
                     <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                  </div>
                  <div id="menu2" class="tab-pane fade box_shdw">
                     <h3>Menu 2</h3>
                     <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                  </div>
                  <div id="menu3" class="tab-pane fade box_shdw">
                     <h3>Menu 3</h3>
                     <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                  </div>
               </div>
            </div>
            <!-- </div> -->
         </div>
      </div>
   </div>
   <input type="hidden" name="total_products" id="total_products" value="<?php echo $total_products; ?>">
   <input type="hidden" name="last_limit" id="last_limit" value="<?php echo $last_limit; ?>">
   <input type="hidden" name="cat_id" id="cat_id" value="<?php echo $cat_id; ?>">
   <input type="hidden" name="size_id" id="size_id" value="">
   <input type="hidden" name="sub_id" id="sub_id" value="<?php echo $sub_id; ?>">
   <input type="hidden" name="min_range" id="min_range">
   <input type="hidden" name="max_range" id="max_range">
   </form>
</div>

<script type="text/javascript">
var processing;

$(document).ready(function(){

    $("#view_more_product").click(function(e){

        if (processing)
            return false;

        var last_limit = $('#last_limit').val();
        var total_products = $('#total_products').val();
        var order_by = $('#filter_search').val();
        var size_id = $('#size_id').val();
        var min_range = $('#min_range').val();
        var max_range = $('#max_range').val();



        if(total_products>last_limit){


        if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
           //alert(last_limit+' - '+total_products);
            processing = true;
            var cat_id = '<?php echo $cat_id; ?>';
            var sub_id = '<?php echo $sub_id; ?>';
            var data = 'cat_id='+cat_id+'&last_limit='+last_limit+'&order_by='+order_by+'&size_id='+size_id+'&sub_id='+sub_id+'&min_range='+min_range+'&max_range='+max_range;

            $.ajax({
            type: "POST",
            url: base_url+'ajax/scroll_product',
            data: data,
            success: function(response) {
              console.log(response);
                var obj = JSON.parse(response);
                $('#port').append(obj.data);
                $('#last_limit').val(obj.last_limit);
                $('#total_products').val(obj.total_products);
                if(obj.total_products==0){
                   $('.result_div').html('<p>No Result Found....</p>'); 
                }
                processing = false;

            },
            error: function(response) {
                result = 'error';
            },
            async: false
            });
            

            //var result = call_scroll_products();
            //console.log(result);
            //processing = false;
            /*$.post('/echo/html/', 'html=<div class="loadedcontent">new div</div>', function(data){
                $('#container').append(data);
                processing = false;
            });
            alert('sdfsdfsd');*/
        }
    }
    });
});

//  var slider = document.getElementById('slider');

// noUiSlider.create(slider, {
//   start: [0, 100],
//   connect: true,
//   range: {
//     'min': 0,
//     'max': 100
//   }
// });


var keypressSlider = document.getElementById('slider');
var input1 = document.getElementById('input-with-keypress-1');
var input0 = document.getElementById('input-with-keypress-0');
var inputs = [input0, input1];

noUiSlider.create(keypressSlider, {
  start: [500, 10000],
  connect: true,
   direction: 'ltr',
   tooltips: [true, wNumb({ decimals: 0 })],
  range: {
    'min': [500],
    'max': [10000]
  }
});

keypressSlider.noUiSlider.on('end', function(values, handle){
  //addClassFor(lEnd, 'tShow', 450);

  
  var array = values.toString().split(",");
  $('#min_range').val(array[0]);
  $('#max_range').val(array[1]);
  change_range();
});

keypressSlider.noUiSlider.on('update', function( values, handle, unencoded) {
  
  inputs[handle].value = values[handle];
});

function setSliderHandle(i, value) {
  var r = [null,null];
  r[i] = value;
  keypressSlider.noUiSlider.set(r);
}

// Listen to keydown events on the input field.
inputs.forEach(function(input, handle) {

  input.addEventListener('change', function(){
    setSliderHandle(handle, this.value);
  });

  input.addEventListener('keydown', function( e ) {

    var values = keypressSlider.noUiSlider.get();
    var value = Number(values[handle]);

    // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
    var steps = keypressSlider.noUiSlider.steps();

    // [down, up]
    var step = steps[handle];

    var position;

    // 13 is enter,
    // 38 is key up,
    // 40 is key down.
    switch ( e.which ) {

      case 13:
        setSliderHandle(handle, this.value);
        break;

      case 38:

        // Get step to go increase slider value (up)
        position = step[1];

        // false = no step is set
        if ( position === false ) {
          position = 1;
        }

        // null = edge of slider
        if ( position !== null ) {
          setSliderHandle(handle, value + position);
        }

        break;

      case 40:

        position = step[0];

        if ( position === false ) {
          position = 1;
        }

        if ( position !== null ) {
          setSliderHandle(handle, value - position);
        }

        break;
    }
  });
});
     
</script>

