 <div class="tab_cabin">
                                   <ul class="nav nav-tabs ">
                                   <li class="delivery_address_list first_list">
                                     <a href="#already" data-toggle="tab">Already Exist</a>
                                   </li>
                                   <li class="delivery_address_list">
                                     <a href="#add_new" data-toggle="tab">Add New</a>
                                   </li>
                                 </ul> 

                                  <div class="tab-content  address_tab_content">
                                   <div class="tab-pane active" id="already">
                                    <?php
                                    $i = 0;
                                    foreach ($address_list as $rs) {
                                    $class = $i%2==1?'no_margin_right':'';
                                    ++$i;
                                    ?>
                                    <div class="address_exists <?php echo $class; ?>">

                                      <div class="address_exist_check">
                                        <input type="radio" name="address_id" 
                                        id="address_id_<?php echo $rs->address_id; ?>" 
                                        value="<?php echo $rs->address_id; ?>" 
                                        class="address_class" />
                                        <label for="address_id_<?php echo $rs->address_id; ?>"></label>
                                      </div>

                                      <div class="exist_pad">

                                       <p><?php echo ucfirst($rs->firstname).' '.$rs->lastname; ?></p>
                                       <?php if($rs->company!=''){?><p><?php echo $rs->company; ?></p><?php } ?>
                                       <p><?php echo $rs->address_1; ?></p>
                                       <?php if($rs->address_2!=''){?><p><?php echo $rs->address_2; ?></p><?php } ?>
                                       <p><?php echo $rs->city_name; ?></p>
                                       <p><?php echo $rs->state_name; ?>-<?php echo $rs->Zip; ?></p>
                                      </div>

                                    </div> 
                                    <?php } ?>
                                        
                                   </div>
                                   
                                   <div class="tab-pane" id="add_new">
                                    <form name="address_new" id="address_new" method="post">
                                        <div class="rows">
                                          <div class="checkout_billing">
                                            <div class="text_boxs">
                                              <input type="text" name="firstname" placeholder="First name" 
                                              data-parsley-trigger="change" 
                                              data-parsley-pattern="^[a-zA-Z\  \/]+$" 
                                              required="" data-parsley-required-message="Please insert your first name."
                                              data-parsley-errors-container="#first_name"/>

                                              <div id="first_name" class="custom_parse_error"></div>
                                            </div>

                                            <div class="text_boxs">
                                              <input type="text" name="lastname" placeholder="Last name" 
                                              data-parsley-trigger="change" 
                                              data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                              required="" data-parsley-required-message="Please insert your last name."
                                              data-parsley-errors-container="#last_name"/>

                                              <div id="last_name" class="custom_parse_error"></div>  
                                            </div>

                                            <div class="text_boxs">
                                              <input type="text" name="address_1" placeholder="Address Line 1" 
                                              data-parsley-trigger="change" 
                                              data-parsley-minlength="2"  data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"  
                                              required="" data-parsley-required-message="Please insert address line 1."
                                              data-parsley-errors-container="#add_line1"/>

                                              <div id="add_line1" class="custom_parse_error"></div> 
                                            </div>


                                            <div class="text_boxs"> 
                                              <input type="text" name="address_2"  value="<?php echo $address->address_2; ?>" placeholder="Address Line 2" 
                                              data-parsley-trigger="change" 
                                              data-parsley-minlength="2"  data-parsley-pattern="^[a-zA-Z\ 0-9 \/]+$" 
                                              required="" data-parsley-required-message="Please insert address line 2."
                                              data-parsley-errors-container="#add_line2"/>

                                              <div id="add_line2" class="custom_parse_error"></div>
                                            </div> 
                                          </div>

                                          <div class="checkout_billing">
                                          <?php if($statedetails > 0) { ?>

                                            <div class="text_boxs">
                                              <div class="place-select slct_drop ">
                                                <select class="sscct state_id" name="state_id" id="state_id" required="" 
                                                data-parsley-required-message="Please select state."
                                                data-parsley-errors-container="#states">
                                                  <option selected="" value="">State</option>

                                                  <?php foreach($statedetails as $state) { ?>

                                                  <option value="<?php echo $state->id;?>"> 
                                                    <?php echo $state->state_name;?>
                                                  </option>
                                                <?php } ?>

                                                </select>
                                              </div>
                                              <div id="states" class="custom_parse_error"></div>

                                            </div>

                                            <div class="text_boxs">              
                                              <div class="place-select slct_drop">
                                                <select class="sscct" id="addr_city_dropdown" name="city_id" onchange="" 
                                                  required="" data-parsley-required-message="Please select city."
                                                  data-parsley-errors-container="#city">
                                                  <option selected="" value="-1">City</option>

                                                  <?php foreach($citydetails as $city) { ?>

                                                  <option value="<?php echo $city->id;?>"
                                                    <?php if ($city->id == $data1->city_id){ ?>
                                                    selected = "selected" <?php } ?>><?php echo $city->city_name;?>
                                                  </option>

                                                  <?php } ?>
                                                </select>
                                              </div>
                                              <div id="city" class="custom_parse_error"></div>

                                            </div>                

                                          <?php } ?>


                                            <div class="text_boxs">        
                                              <input type="text" name="Zip" placeholder="Zip Code" 
                                              data-parsley-trigger="change" 
                                              data-parsley-minlength="2" data-parsley-maxlength="5" data-parsley-pattern="^[0-9\  \/]+$"  
                                              required="" data-parsley-required-message="Please insert zip code."
                                              data-parsley-errors-container="#zip"/>

                                              <div id="zip" class="custom_parse_error"></div>
                                            </div>

                                            <div class="text_boxs">   
                                              <input type="text" name="telephone" placeholder=" Telephone" 
                                              data-parsley-trigger="change" 
                                              data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$"
                                              required="" data-parsley-required-message="Please insert phone number."
                                              data-parsley-errors-container="#phone"/>

                                              <div id="phone" class="custom_parse_error"></div>
                                            </div>
                                          </div>
                                        </div>
                                       <div class="delivery_sub">
                                          <input type="button" class="delivery_btn" value="Submit" id="new_address" />
                                       </div>
                                    </form>
                                   </div>

                                 </div>
                              </div> 