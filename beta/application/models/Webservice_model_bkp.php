<?php

class Webservice_model extends CI_Model {

	public function _consruct(){
		parent::_construct();

 	}


 	function viewproduct($id){


       $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product.feature,product.mrp,product_gallery.product_image");
        $this->db->from("category");
        $this->db->join("product",'product.category_id = category.id','left');
        $this->db->join("product_gallery",'product_gallery.product_id= product.id');
       
         $query=$this->db->where('product.id',$id);
        $query=$this->db->get();
        return $query->row();

}




function viewproduct1($id){


       $this->db->select('product.id as id,product.*,
                       product_gallery.product_image,
                        product_gallery.product_id
               
               ');
        $this->db->from('product');
       
        $this->db->join('product_gallery','product_gallery.product_id= product.id');
         
         $query=$this->db->where('product.id',$id);
        $query=$this->db->get();
        return $query->row();

}

  function viewcolor($id){

	  $this->db->select('color.id as id,color.color_name,color.image,product_color.product_id,product_color.color_id,product.product_name');
      $this->db->from('color');
      
     
	   $this->db->join('product_color', 'FIND_IN_SET(color.id, product_color.color_id) > 0');
      $this->db->join('product','product.id=product_color.product_id');
      $this->db->where('product.id',$id);
      $query=$this->db->get();
       return $query->result();
}


function get_filter_color(){

 // $this->db->select('product_color.id as id,product_color.color_id,color.id,color.image');
 //  $this->db->from('product_color');
 //  $this->db->join('color','color.id=product_color.color_id');
  $this->db->select('*');
  $this->db->from('color');
  $query=$this->db->get();
  return $query->result();

}



function viewproductsize($id){
      $this->db->select('size.id as id,size.size_type,product_size.product_id,
        product_size.size_id,product.product_name
                                    ');
      $this->db->from('size');
      
	  
	  $this->db->join('product_size', 'FIND_IN_SET(size.id, product_size.size_id) > 0');
    //  $this->db->join('product_size','size.id=product_size.size_id');
      $this->db->join('product','product.id=product_size.product_id');
      $this->db->where('product.id',$id);
      //$this->db->group_by("product_size.size_id");   
      $query=$this->db->get();
	 
     
        return $query->result();
    }


    function viewitem_imgslider(){
         $this->db->select('product_gallery.id as id,product_gallery.product_id,product_gallery.product_image,product.product_name,product.price');
           $this->db->from('product_gallery');
           $this->db->join('product','product.id=product_gallery.product_id');
          $this->db->limit(8,0);
          $query=$this->db->get();
          //echo $this->db->last_query();
          return $query->result();
    }



    function viewsubimg($id){
            $this->db->select('product_gallery.id as id,product_gallery.product_id,product_gallery.product_image,product.product_name');
           $this->db->from('product_gallery');
           $this->db->join('product','product.id=product_gallery.product_id');
          $this->db->where('product.id',$id);
          $query=$this->db->get();
            return $query->result();
    }


  ///stitching neck//

        function front_neck(){

            $this->db->select("stich_type.id as id,stich_type.image,stich_type.discription");
            $this->db->from("stich_type");
            $this->db->join("stichtype",'stichtype.id =stich_type.type_id');
            $this->db->where("stichtype.stichtype",'FRONT');
            $query = $this->db->get();
           // echo $this->db->last_query();
        // print_r($query);die;
            return $query->result();
            
            }

             function back_neck(){

            $this->db->select("stich_type.id as id,stichtype.stichtype,stich_type.type_id,stich_type.image,stich_type.discription");
            $this->db->from("stich_type");
            $this->db->join("stichtype",'stichtype.id =stich_type.type_id');
            $this->db->where("stichtype.stichtype",'BACK');
            $query = $this->db->get();
        // print_r($query);die;
             return $query->result();
            
            }

             function sleeve_neck(){

            $this->db->select("stich_type.id as id,stichtype.stichtype,stich_type.type_id,stich_type.image,stich_type.discription");
            $this->db->from("stich_type");
            $this->db->join("stichtype",'stichtype.id =stich_type.type_id');
            $this->db->where("stichtype.stichtype",'SLEEVE');
            $query = $this->db->get();
        // print_r($query);die;
            return $query->result();
            
            }

            function hemline_neck(){

            $this->db->select("stich_type.id as id,stichtype.stichtype,stich_type.type_id,stich_type.image,stich_type.discription");
            $this->db->from("stich_type");
            $this->db->join("stichtype",'stichtype.id =stich_type.type_id');
            $this->db->where("stichtype.stichtype",'HEMLINE');
            $query = $this->db->get();
        // print_r($query);die;
            return $query->result();
            
            }



    function addons1(){


    $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
    $this->db->from("adons_type");
    $this->db->join("add_ons",'add_ons.type =adons_type.id');
    $this->db->where('add_ons.type', 1);
  
     $result = $this->db->get()->result();
  
      return $result;

                      }



        function addons2(){


    $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
    $this->db->from("adons_type");
    $this->db->join("add_ons",'add_ons.type =adons_type.id');
    $this->db->where('add_ons.type', 2);
  
     $result = $this->db->get()->result();
  
      return $result;

                      }
              


 function addons3(){


    $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
    $this->db->from("adons_type");
    $this->db->join("add_ons",'add_ons.type =adons_type.id');
    $this->db->where('add_ons.type', 3);
  
     $result = $this->db->get()->result();
  
      return $result;

                      }

                      function addons4(){


    $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
    $this->db->from("adons_type");
    $this->db->join("add_ons",'add_ons.type =adons_type.id');
    $this->db->where('add_ons.type', 4);
    $this->db->limit(2,0);
  
     $result = $this->db->get()->result();
  
      return $result;

                      }



    function addons5(){


    $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
    $this->db->from("adons_type");
    $this->db->join("add_ons",'add_ons.type =adons_type.id');
    $this->db->where('add_ons.type', 4);
    $this->db->order_by('id',"desc");
    $this->db->limit(2,0);
    
  
     $result = $this->db->get()->result();
  
      return $result;

                      }
 
 
       
    //view measurement

        function view($id)
   {


  //$id=$this->session->userdata('user_id');

    // $this->db->select('*');
    $this->db->where('customer_id',$id);

    $query = $this->db->get('measurement');
    // $this->db->from('measurement');

    $result = $query->row();
    return $result;

   }

   //get shipping address
   function get_addrs($id){
     $this->db->select('address');
     $this->db->from('add_address1');

    $this->db->where('customer_id',$id);

    $query = $this->db->get();
    // $this->db->from('measurement');

    $result = $query->row();
    return $result;

   }


   ///get search item

  function  get_search_item($id){

     $this->db->select('*');
     $this->db->from('recent_search');

    $this->db->where('customer_id',$id);

    $query = $this->db->get();
    // $this->db->from('measurement');

    $result = $query->result();
    return $result;


   }



   function shoppingbag_count($id)
  {
    
    //$id = $this->session->userdata('user_id');
    
        $this->db->select('*'); 
              $this->db->from('cart');
        $this->db->where('customer_id',$id);
        $query = $this->db->get();      
         $result = $query->num_rows();
          return $result;
        
        

  }


  function notification_count($id)
  {
    
    //$id = $this->session->userdata('user_id');
    
        $this->db->select('*'); 
              $this->db->from('notification');
        $this->db->where('customer_id',$id);
        $query = $this->db->get();      
         $result = $query->num_rows();
          return $result;
        
        

  }

  function get_address_count($id){

      $this->db->select('*'); 
              $this->db->from('add_address1');
        $this->db->where('customer_id',$id);
        $query = $this->db->get();      
         $result = $query->num_rows();
          return $result;
       

  }


 function get_profilepic($id){

  $this->db->select('photo');
  $this->db->from('customer');
   $this->db->where('id',$id);
  $query = $this->db->get();      
         $result = $query->row();
          return $result;
  }

 function  get_detail2($id){

  $this->db->select('*');
  $this->db->from('customer');
   $this->db->where('id',$id);
  $query = $this->db->get();      
         $result = $query->row();
          return $result;

 }



 	
}
?>