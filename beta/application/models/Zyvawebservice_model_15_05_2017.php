<?php

class Zyvawebservice_model extends CI_Model {

	public function _consruct(){
		parent::_construct();

 	}


 	function viewproduct($id){


       $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product.feature,product.mrp,product_gallery.product_image");
        $this->db->from("category");
        $this->db->join("product",'product.category_id = category.id','left');
        $this->db->join("product_gallery",'product_gallery.product_id= product.id');
       
         $query=$this->db->where('product.id',$id);
        $query=$this->db->get();
        return $query->row();

}




function viewproduct1($id){


       $this->db->select('product.id as id,product.*,
                       product_gallery.product_image,
                        product_gallery.product_id
               
               ');
        $this->db->from('product');
       
        $this->db->join('product_gallery','product_gallery.product_id= product.id','left');
         
         $query=$this->db->where('product.id',$id);
        $query=$this->db->get();
       // echo $this->db->last_query();
        return $query->row();

}






function viewcolor($id){
  $this->db->distinct();
    $this->db->select('color.id as id,color.color_name,color.image,product_quantity.product_id,product_quantity.color_id,product.product_name');
      $this->db->from('color');
      $this->db->join('product_quantity', 'FIND_IN_SET(color.id, product_quantity.color_id) > 0');
      $this->db->join('product','product.id=product_quantity.product_id');
      $this->db->where('product.id',$id);
      $query=$this->db->get();
       return $query->result();
}



function get_filter_color(){

 $this->db->select('*');
  $this->db->from('color');
  $query=$this->db->get();
  return $query->result();

}




function get_color_id($img){

  $this->db->select('color.id');
  $this->db->from('color');
  $this->db->where('color.image',$img);
  $query=$this->db->get();
  return $query->row()->id;

}


function get_size_id($type){

$this->db->select('size.id');
  $this->db->from('size');
  $this->db->where('size.size_type',$type);
  $query=$this->db->get();
  return $query->row()->id;
}







function stock_qty1($id){

       $this->db->select("product_quantity.id,product_quantity.product_id,product_quantity.quantity");
        $this->db->from("product_quantity");
        $query=$this->db->where('product_quantity.product_id',$id);
        $query=$this->db->get();
        return $query->row();


}


function viewproductsize($id){
      /*$this->db->distinct();

      $this->db->select('size.id as id,size.size_type,product_size.product_id,
        product_size.size_id,product.product_name,GROUP_CONCAT(distinct product_size.size_id) as size
');
      $this->db->from('size');
      
	  
	  $this->db->join('product_size', 'FIND_IN_SET(size.id, product_size.size_id) > 0');
    //  $this->db->join('product_size','size.id=product_size.size_id');
      $this->db->join('product','product.id=product_size.product_id');
      $this->db->where('product.id',$id);
      $query=$this->db->get();
	 
     
        return $query->result();*/
        return $size = $this->db->distinct()->select('size_id,size.size_type')->from('product_quantity')->join('size','product_quantity.size_id=size.id')->join('product','product_quantity.product_id=product.id')->where('product.id',$id)->where('size.size_type !=','Custom')->get()->result();
    }


    function viewitem_imgslider(){
         $this->db->select('product_gallery.id as id,product_gallery.product_id,product_gallery.product_image,product.product_name,product.price');
           $this->db->from('product_gallery');
           $this->db->join('product','product.id=product_gallery.product_id');
          $this->db->limit(8,0);
          $query=$this->db->get();
          //echo $this->db->last_query();
          return $query->result();
    }



    function viewsubimg($id){
            $this->db->select('product_gallery.id as id,product_gallery.product_id,product_gallery.product_image,product.product_name');
           $this->db->from('product_gallery');
           $this->db->join('product','product.id=product_gallery.product_id');
          $this->db->where('product.id',$id);
          $query=$this->db->get();
            return $query->result();
    }


  ///stitching neck//

        function front_neck(){

            $this->db->select("stich_type.id as id,stich_type.image,stich_type.discription");
            $this->db->from("stich_type");
            $this->db->join("stichtype",'stichtype.id =stich_type.type_id');
            $this->db->where("stichtype.stichtype",'FRONT');
            $query = $this->db->get();
           // echo $this->db->last_query();
        // print_r($query);die;
            return $query->result();
            
            }

             function back_neck(){

            $this->db->select("stich_type.id as id,stichtype.stichtype,stich_type.type_id,stich_type.image,stich_type.discription");
            $this->db->from("stich_type");
            $this->db->join("stichtype",'stichtype.id =stich_type.type_id');
            $this->db->where("stichtype.stichtype",'BACK');
            $query = $this->db->get();
        // print_r($query);die;
             return $query->result();
            
            }

             function sleeve_neck(){

            $this->db->select("stich_type.id as id,stichtype.stichtype,stich_type.type_id,stich_type.image,stich_type.discription");
            $this->db->from("stich_type");
            $this->db->join("stichtype",'stichtype.id =stich_type.type_id');
            $this->db->where("stichtype.stichtype",'SLEEVE');
            $query = $this->db->get();
        // print_r($query);die;
            return $query->result();
            
            }

            function hemline_neck(){

            $this->db->select("stich_type.id as id,stichtype.stichtype,stich_type.type_id,stich_type.image,stich_type.discription");
            $this->db->from("stich_type");
            $this->db->join("stichtype",'stichtype.id =stich_type.type_id');
            $this->db->where("stichtype.stichtype",'HEMLINE');
            $query = $this->db->get();
        // print_r($query);die;
            return $query->result();
            
            }



    function addons1(){


    $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
    $this->db->from("adons_type");
    $this->db->join("add_ons",'add_ons.type =adons_type.id');
    $this->db->where('add_ons.type', 1);
  
     $result = $this->db->get()->result();
  
      return $result;

                      }



        function addons2(){


    $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
    $this->db->from("adons_type");
    $this->db->join("add_ons",'add_ons.type =adons_type.id');
    $this->db->where('add_ons.type', 2);
  
     $result = $this->db->get()->result();
  
      return $result;

                      }
              


 function addons3(){


    $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
    $this->db->from("adons_type");
    $this->db->join("add_ons",'add_ons.type =adons_type.id');
    $this->db->where('add_ons.type', 3);
  
     $result = $this->db->get()->result();
  
      return $result;

                      }

                      function addons4(){


    $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
    $this->db->from("adons_type");
    $this->db->join("add_ons",'add_ons.type =adons_type.id');
    $this->db->where('add_ons.type', 4);
    $this->db->limit(2,0);
  
     $result = $this->db->get()->result();
  
      return $result;

                      }



    

  function addons5(){
    $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
    $this->db->from("adons_type");
    $this->db->join("add_ons",'add_ons.type =adons_type.id');
    $this->db->where('add_ons.type', 4);
    $this->db->order_by('id',"desc");
    $this->db->limit(2,0);
    
  
     $result = $this->db->get()->result();
  
      return $result;

                      }
 
 
       
    //view measurement

        function view($id)
   {


    $this->db->where('customer_id',$id);

    $query = $this->db->get('measurement');
    
    $result = $query->row();
    return $result;

   }

   //get shipping address
   function get_addrs($id){
     $this->db->select('address');
     $this->db->from('add_address1');

    $this->db->where('customer_id',$id);

    $query = $this->db->get();
    

    $result = $query->row();
    return $result;

   }


   ///get search item

  function  get_search_item($id){
    $result = $this->db->where('reseller_id',$id)->get('recent_search')->result();
    return $result;
  }



   function shoppingbag_count($id)
  {
    
    //$id = $this->session->userdata('user_id');
    
        $this->db->select('*'); 
              $this->db->from('cart');
        $this->db->where('customer_id',$id);
        $query = $this->db->get();      
         $result = $query->num_rows();
          return $result;
        
        

  }


  function notification_count($id)
  {
    
    //$id = $this->session->userdata('user_id');
    
        $this->db->select('*'); 
              $this->db->from('notification');
        $this->db->where('customer_id',$id);
        $query = $this->db->get();      
         $result = $query->num_rows();
          return $result;
        
        

  }


  function get_maxcount(){

  $this->db->select('product.id as id,product.*,
                       product_gallery.product_image,
                        product_gallery.product_id,
               product_quantity.quantity,
               product.status
               ');
        $this->db->from('product');
       
        $this->db->join('product_gallery','product_gallery.product_id= product.id');
        $this->db->join("product_quantity",'product_quantity.product_id=product.id');

        $this->db->group_by('product.id');
        $this->db->where('product.status',1);
        $this->db->order_by('product.id','DESC');
        //$this->db->limit(6);
        $query = $this->db->get();
        //$result = $query->result();
        $result=$query->num_rows();
        
         return $result;
       

  }

  function get_address_count($id){

      $this->db->select('*'); 
              $this->db->from('add_address1');
        $this->db->where('customer_id',$id);
        $query = $this->db->get();      
         $result = $query->num_rows();
          return $result;
       

  }


 function get_profilepic($id){

  $this->db->select('image');
  $this->db->from('reseller');
   $this->db->where('id',$id);
  $query = $this->db->get();      
         $result = $query->row();
          return $result;
  }

 function  get_detail2($id){

  $this->db->select('*');
  $this->db->from('reseller');
   $this->db->where('id',$id);
  $query = $this->db->get();      
         $result = $query->row();
          return $result;

 }

 function stock_qty($id){

       $this->db->select("product_quantity.id,product_quantity.product_id,product_quantity.quantity");
        $this->db->from("product_quantity");
        $query=$this->db->where('product_quantity.product_id',$id);
        $query=$this->db->get();
        return $query->row();


}



function items(){

    $this->db->select('product_name');
    $this->db->from('product');
    $res=$this->db->get();
    $result=$res->result();
    return $result;


   }



function cart_update($id){
  

    $this->db->where('reseller_id',$id);
    $query = $this->db->get("cart")->result();
    //print_r($query);exit;
    
    foreach ($query as $rs) {
      $qty_array = $this->db->where('product_id',$rs->product_id)->where('size_id',$rs->size_id)->where('color_id',$rs->color_id)->get('product_quantity')->row();
       //print_r($qty_array);
      if($rs->quantity > $qty_array->quantity){
        if($qty_array->quantity==0){
          $this->db->where('id',$rs->id);
          $this->db->update("cart",array('stock_status'=>'1'));
        } else {
          $this->db->where('id',$rs->id);
          $this->db->update("cart",array('quantity'=>$qty_array->quantity,'stock_status'=>'2'));
        }
         
      }
      
    }
  }

      
 ///


  /*public function get_cart($id)
    {
        
   $rs = $this->db->query("SELECT cart.id,cart.quantity,cart.price,cart.stock_status,product.id AS product_id,product.product_name,
    product.product_code,product.actual_price,product.product_offer,product.price as pprice,
    product.price,color.image,size.size_type FROM `cart` LEFT JOIN 
    product ON product.id = cart.product_id LEFT JOIN
     color ON color.id = cart.color_id  LEFT JOIN
     
     size ON size.id = cart.size_id WHERE cart.reseller_id =" . $id)->result();
        //echo $this->db->last_query();exit;
        return $rs;
    }*/
    
 	
}
?>