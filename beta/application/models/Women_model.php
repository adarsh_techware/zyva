<?php
class Women_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function view_subcat() {
        $query = $this->db->select('*');
        $query = $this->db->from('product');
        $query = $this->db->join('subcategory', 'product.subcategory_id=subcategory.id');
        $result = $query->result();
        return $result;
    }
    function category() {
        $this->db->where('status', 1);
        $this->db->order_by('order_by', 'ASC');
        $query = $this->db->get('category');
        $result = $query->result();
        return $result;
    }
    function sub_category_own() {
        $this->db->select('subcategory.id as id,category.category_name,subcategory.sub_category');
        $this->db->from('subcategory');
        $this->db->join('category', 'category.id = subcategory.category_id', 'left');
        $query = $this->db->get();
        return $query->result();
    }
    function sub_category($name) {
        $this->db->select("subcategory.id as id,subcategory.sub_category,subcategory.sub_image,product.product_name,product.product_code,product.description,product.price");
        $this->db->from("subcategory");
        $this->db->join("product", 'subcategory.id =product.subcategory_id');
        $this->db->where("subcategory.sub_category", $name);
        $query = $this->db->get();
        // print_r($query);die;
        return $query->result();
        //$this->db->join('subcategory','subcategory.category_id=category.id');
        
    }
    function categoryview($name) {
        /* $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id ,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
        
        $this->db->from("category");
        
        $this->db->join("product",'product.category_id = category.id');
        
        $this->db->join("product_gallery",'product_gallery.product_id= product.id');
        
        $this->db->where("category.category_name",$name);
        
        $query = $this->db->get();
        
        $final = $query->result();
        
        //var_dump($final);exit;
        
        if($final){
        
        return $final;
        
        
        
        }
        
        else{
        
        $this->db->select("subcategory.id as id,subcategory.sub_category,subcategory.sub_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
        
        $this->db->from("subcategory");
        
        $this->db->join("product",'product.subcategory_id =subcategory.id');
        
         $this->db->join("product_gallery",'product.id =product_gallery.product_id');
        
        $this->db->where("subcategory.sub_category",$name);
        
        $query = $this->db->get();
        
        // print_r($query);die;
        
        // print_r($query);die;
        
        return $query->result();
        
        
        
        }*/
        $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
        $this->db->from("category");
        $this->db->join("product", 'product.category_id = category.id');
        $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
        $this->db->where("category.category_name", $name);
        // $this->db->where('status',1);
        $query = $this->db->get();
        $final = $query->result();
        //var_dump($final);exit;
        if ($final) {
            return $final;
        } else {
            $this->db->select("subcategory.id as id,subcategory.sub_category,subcategory.sub_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
            $this->db->from("subcategory");
            $this->db->join("product", 'product.subcategory_id =subcategory.id');
            $this->db->join("product_gallery", 'product.id =product_gallery.product_id');
            $this->db->where("subcategory.sub_category", $name);
            $query = $this->db->get();
            // print_r($query);die;
            // print_r($query);die;
            return $query->result();
        }
    }
    public function fetch_category($limit, $start, $name) {
        $this->db->limit($limit, $start);
        $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
        $this->db->from("category");
        $this->db->join("product", 'product.category_id = category.id');
        $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
        $this->db->where("category.category_name", $name);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $final = $query->result();
            return $final;
        } else {
            $this->db->limit($limit, $start);
            $this->db->select("subcategory.id as id,subcategory.sub_category,subcategory.sub_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
            $this->db->from("subcategory");
            $this->db->join("product", 'product.subcategory_id =subcategory.id');
            $this->db->join("product_gallery", 'product.id =product_gallery.product_id');
            $this->db->where("subcategory.sub_category", $name);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $final = $query->result();
                return $final;
            }
            // print_r($query);die;
            // print_r($query);die;
            return false;
        }
    }
    function view_subcategory() {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->join('Subcategory', 'subcategory.category_id=category.id');
        $query = $this->db->get();
        // var_dump($query->result);
        // exit;
        return $query->result();
    }
    function viewimg1($id) {
        $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product.feature,product_gallery.product_image");
        $this->db->from("category");
        $this->db->join("product", 'product.category_id = category.id', 'left');
        $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
        $query = $this->db->where('product.product_name', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    function product1($id) {
        // echo $id;die;
        $query = $this->db->where('subcategory_id', $id);
        // $query = $this->db->select('*');
        // $query = $this->db->from('product');
        $query = $this->db->get('product');
        $result = $query->row();
        return $result;
    }
    /////user infor///////////
    public function userinfo() {
        $id = $this->session->userdata('user_id');
        $this->db->select('first_name');
        $this->db->from('customer');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        //echo $this->db->last_query();
        //die;
        return $result;
    }
    function num_rows($name) {
        $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
        $this->db->from("category");
        $this->db->join("product", 'product.category_id = category.id');
        $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
        $this->db->where("category.category_name", $name);
        $query = $this->db->get();
        $final = $query->num_rows();
        //var_dump($final);exit;
        if ($final) {
            return $final;
        } else {
            $this->db->select("subcategory.id as id,subcategory.sub_category,subcategory.sub_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
            $this->db->from("subcategory");
            $this->db->join("product", 'product.subcategory_id =subcategory.id');
            $this->db->join("product_gallery", 'product.id =product_gallery.product_id');
            $this->db->where("subcategory.sub_category", $name);
            $query = $this->db->get();
            // print_r($query);die;
            // print_r($query);die;
            return $query->num_rows();
        }
    }
    function detail($perpage, $page, $name) {
        $count = $perpage * $page;
        $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
        $this->db->from("category", $perpage, $page);
        $this->db->join("product", 'product.category_id = category.id');
        $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
        $this->db->where("category.category_name", $name);
        $this->db->limit($perpage, $count);
        $query = $this->db->get();
        //echo $this->db->last_query();
        $final = $query->result();
        //var_dump($final);exit;
        if ($final) {
            return $final;
        } else {
            $this->db->select("subcategory.id as id,subcategory.sub_category,subcategory.sub_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
            $this->db->from("subcategory", $perpage, $page);
            $this->db->join("product", 'product.subcategory_id =subcategory.id');
            $this->db->join("product_gallery", 'product.id =product_gallery.product_id');
            $this->db->where("subcategory.sub_category", $name);
            $query = $this->db->get();
            // print_r($query);die;
            // print_r($query);die;
            return $query->result();
        }
    }
    function getsize() {
        $query1 = $this->db->get('size');
        // var_dump($query1);die;
        return $query1->result();
    }
    function viewproductsize($id) {
        $this->db->select('size.id as id,size.size_type,product_size.product_id,product_size.size_id,product.product_name');
        $this->db->from('size');
        $this->db->join('product_size', 'FIND_IN_SET(size.id, product_size.size_id) > 0');
        //  $this->db->join('product_size','size.id=product_size.size_id');
        $this->db->join('product', 'product.id=product_size.product_id');
        $this->db->where('product.product_name', $id);
        $query = $this->db->get();
        // echo  $this->db->last_query();
        //die;
        // var_dump($query);die;
        return $query->result();
    }
    function viewsubimg($id) {
        $this->db->select('product_gallery.id as id,product_gallery.product_id,product_gallery.product_image,product.product_name');
        $this->db->from('product_gallery');
        $this->db->join('product', 'product.id=product_gallery.product_id');
        $this->db->where('product.product_name', $id);
        $query = $this->db->get();
        return $query->result();
    }
    function viewitem_img() {
        $this->db->select('product_gallery.id as id,product_gallery.product_id,product_gallery.product_image,product.product_name,product.price');
        $this->db->from('product_gallery');
        $this->db->join('product', 'product.id=product_gallery.product_id');
        $this->db->limit(8, 0);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }
    function viewproductcolor($id) {
        $this->db->select('color.id as id,color.color_name,product_color.product_id,product_color.color_id,product.product_name');
        $this->db->from('color');
        $this->db->join('product_color', 'FIND_IN_SET(color.id, product_color.color_id) > 0');
        $this->db->join('product', 'product.id=product_color.product_id');
        $this->db->where('product.product_name', $id);
        $query = $this->db->get();
        //  echo  $this->db->last_query();
        // die;
        // var_dump($query);die;
        return $query->result();
    }
    function getstock($tem) {
        /*$result = array();
        
          foreach($tem as $product)
        
          {
        
            $result[] = array('product_id' => $product->product_id,
        
                     
        
                      );
        
        
        
        
        
        print_r($result);
        
                              $this->db->select_max("quantity");
        
          $this->db->from("product_quantity");
        
          $this->db->where("product_id",$result->product_id);
        
        
        
        $query = $this->db->get();
        
        echo $this->db->last_query();
        
        die;
        
        return $query->result();
        
        }*/
    }
    public function get_cart_details() {
        $id = $this->session->userdata('user_id');
        $this->db->select("cart.product_id,product.product_name,

                            product.prod_display,cart.price as price, 

                            color.color_name as color_name, size.size_type, cart.quantity");
        $this->db->from("cart");
        $this->db->join("color", 'color.id = cart.color_id');
        $this->db->join("size", 'size.id = cart.size_id');
        $this->db->join("product", 'product.id = cart.product_id');
        $this->db->where("cart.customer_id", $id);
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return $query->result();
    }
    // SELECT  FROM `cart` JOIN `color` ON `color`.`id` = `cart`.`color_id` JOIN `size` ON `size`.`id` = `cart`.`size_id` JOIN `product` ON `product`.`id` = `cart`.`product_id`  WHERE `cart`.`customer_id` = '3'
    
}
