<?php
class Cart_model extends CI_Model {
    public function _consruct() {
        parent::_construct();
    }
    public function addtocart1($data) {
        $id = $this->session->userdata('user_id');
        $sess_id = $this->session->userdata('session_id');
        if ($id) {
            $query = $this->db->where('customer_id', $id)->where('product_id', $data['product_id'])->where('color_id', $data['color_id'])->where('size_id', $data['size_id'])->get('cart');
            if ($query->num_rows() > 0) {
                $rs = $query->row();
                $quantity = $rs->quantity + $data['quantity'];
                $this->db->where('id', $rs->id)->update('cart', array('quantity' => $quantity));
                return 'success';
            } else {
                $data = array('customer_id' => $id, 'product_id' => $data['product_id'], 'color_id' => $data['color_id'], 'size_id' => $data['size_id'], 'quantity' => $data['quantity'], 'stitching_on' => 0);
                $this->db->insert('cart', $data);
                return 'success';
            }
        } else {
            $query = $this->db->where('session_id', $id)->where('product_id', $data['product_id'])->where('color_id', $data['color_id'])->where('size_id', $data['size_id'])->get('temporary_cart');
            if ($query->num_rows() > 0) {
                $rs = $query->row();
                $quantity = $rs->quantity + $data['quantity'];
                $this->db->where('id', $rs->id)->update('temporary_cart', array('quantity' => $quantity));
                return 'success';
            } else {
                $data = array('session_id' => $sess_id, 'product_id' => $data['product_id'], 'color_id' => $data['color_id'], 'size_id' => $data['size_id'], 'quantity' => $data['quantity'], 'stitching_on' => 0);
                $this->db->insert('temporary_cart', $data);
                return 'success';
            }
        }
    }
    public function addtocart($data) {
        $id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('customer_id', $id);
        $this->db->where('product_id', $data['product_id']);
        $this->db->where('color_id', $data['color_id']);
        $this->db->where('size_id', $data['size_id']);
        $query1 = $this->db->get()->result();
        if (count($query1) > 0) {
            $result = array();
            foreach ($query1 as $product) {
                $result[] = array('product_id' => $product->product_id, 'color_id' => $product->color_id, 'price' => $product->price, 'size_id' => $product->size_id, 'quantity' => $product->quantity, 'customer_id' => $product->customer_id);
                //$q=$product->quantity+1;
                $a = $data['quantity'];
                $q = $product->quantity + $a;
                $a = array('quantity' => $q);
                $this->db->where('customer_id', $id);
                $this->db->where('product_id', $data['product_id']);
                $this->db->where('color_id', $data['color_id']);
                $this->db->where('size_id', $data['size_id']);
                if ($this->db->update('cart', $a)) {
                    $this->db->select('*');
                    $this->db->from('product_quantity');
                    $this->db->where('product_id', $data['product_id']);
                    $this->db->where('color_id', $data['color_id']);
                    $this->db->where('size_id', $data['size_id']);
                    $query1 = $this->db->get()->row();
                    if (count($query1) > 0) {
                        $result[] = array('quantity' => $query1->quantity);
                        //$q=$query1->quantity-1;
                        $a = $data['quantity'];
                        $q = $query1->quantity - $a;
                        $this->db->where('id', $query1->id);
                        $this->db->set('quantity', $q);
                        $this->db->update('product_quantity');
                        return "success";
                    }
                    return "success";
                }
            }
        } else {
            //********** measurementtemp//
            $data1 = array('customer_id' => $data['customer_id'], 'product_id' => $data['product_id'], 'price' => $data['price'], 'color_id' => $data['color_id'], 'size_id' => $data['size_id'], 'quantity' => $data['quantity'], 'stitching_on' => $data['stitching_on']
            // 'stitch_id'=>0
            );
            if ($this->db->insert('cart', $data1)) {
                $last_id = $this->db->insert_id();
                $data2 = array('cart_id' => $last_id, 'front_id' => $data['front_id'], 'back_id' => $data['back_id'], 'sleeve_id' => $data['sleeve_id'], 'neck_id' => $data['neck_id'], 'add_ons' => $data['add_ons'], 'add_ons1' => $data['add_ons1'], 'add_ons2' => $data['add_ons2'], 'add_ons3' => $data['add_ons3'], 'specialcase' => $data['specialcase']);
                $this->db->insert('stitching', $data2);
                $this->db->select('*');
                $this->db->from('product_quantity');
                $this->db->where('product_id', $data['product_id']);
                $this->db->where('color_id', $data['color_id']);
                $this->db->where('size_id', $data['size_id']);
                $query1 = $this->db->get()->row();
                if (count($query1) > 0) {
                    $result[] = array('quantity' => $query1->quantity);
                    //$q=$query1->quantity-1;
                    $a = $data1['quantity'];
                    $q = $query1->quantity - $a;
                    $this->db->where('id', $query1->id);
                    $this->db->set('quantity', $q);
                    $this->db->update('product_quantity');
                    return "success";
                }
                //echo  $this->db->last_query();exit;
                
            } else {
                return "error";
            }
        }
    }
    //// add to temp////
    function addtotemp($data) {
        // $array = $data['add_ons'];
        // $comma_separated = implode(",", $array);
        // $data['add_ons']=$comma_separated;
        // $dataad = json_encode($data['add_ons']);
        // if(isset($data)){
        $data1 = array('customer_id' => $data['customer_id'], 'product_id' => $data['product_id'], 'price' => $data['price'], 'color_id' => $data['color_id'], 'size_id' => $data['size_id'], 'quantity' => $data['quantity'], 'stitching_on' => $data['stitching_on'], 'front_id' => $data['front_id'], 'back_id' => $data['back_id'], 'sleeve_id' => $data['sleeve_id'], 'neck_id' => $data['neck_id'], 'add_ons' => $data['add_ons'], 'add_ons1' => $data['add_ons1'], 'add_ons2' => $data['add_ons2'], 'add_ons3' => $data['add_ons3'], 'specialcase' => $data['specialcase']
        // 'stitch_id'=>$data['stitch_id']
        );
        //   $this->db->insert('temp_cart',$data1);
        //   $last_id=$this->db->insert_id();
        // $data['front']=$this->db->get('stich_type');
        if ($this->db->insert('temp_cart', $data1)) {
            //echo $this->db->last_query();
            return $last_id = $this->db->insert_id();
        } else {
            return false;
        }
    }
    function addon_change($add_ons, $add_ons1, $add_ons2, $add_ons3) {
        $add_array = array();
        if ($add_ons != '') {
            $add_array[] = $add_ons;
        }
        if ($add_ons1 != '') {
            $add_array[] = $add_ons1;
        }
        if ($add_ons2 != '') {
            $add_array[] = $add_ons2;
        }
        if ($add_ons3 != '') {
            $add_array[] = $add_ons3;
        }
        $add_list = implode(',', $add_array);
        if ($add_list != '') {
            $rs = $this->db->query("SELECT SUM(charge) AS addon_charge FROM add_ons WHERE id IN($add_list)")->row();
            $addon_charge = $rs->addon_charge;
        } else {
            $addon_charge = 0;
        }
        return $addon_charge;
    }
    function view_front($id) {
        $this->db->select('stich_type.id as id,stich_type.image,stich_type.discription,temp_cart.front_id');
        $this->db->from('stich_type');
        $this->db->join('temp_cart', 'stich_type.id=temp_cart.front_id');
        $this->db->where('stich_type.id', $id);
        $qry = $this->db->get();
        return $qry->row();
    }
    function view_back($id) {
        $this->db->select('stich_type.id as id,stich_type.image,stich_type.discription,temp_cart.back_id');
        $this->db->from('stich_type');
        $this->db->join('temp_cart', 'stich_type.id=temp_cart.back_id');
        $this->db->where('stich_type.id', $id);
        $qry = $this->db->get();
        return $qry->row();
    }
    function view_sleeve($id) {
        $this->db->select('stich_type.id as id,stich_type.image,stich_type.discription,temp_cart.sleeve_id');
        $this->db->from('stich_type');
        $this->db->join('temp_cart', 'stich_type.id=temp_cart.sleeve_id');
        $this->db->where('stich_type.id', $id);
        $qry = $this->db->get();
        return $qry->row();
    }
    function view_hemline($id) {
        $this->db->select('stich_type.id as id,stich_type.image,stich_type.discription,temp_cart.neck_id');
        $this->db->from('stich_type');
        $this->db->join('temp_cart', 'stich_type.id=temp_cart.neck_id');
        $this->db->where('stich_type.id', $id);
        $qry = $this->db->get();
        return $qry->row();
    }
    function view_addons($id) {
        $this->db->select('add_ons.id as id,add_ons.type,add_ons.image,add_ons.discription,add_ons.charge,adons_type.id,adons_type.adons_type,temp_cart.add_ons');
        $this->db->from('add_ons');
        $this->db->join('adons_type', 'adons_type.id=add_ons.type');
        $this->db->join('temp_cart', 'add_ons.id=temp_cart.add_ons');
        $this->db->where('add_ons.id', $id);
        $qry = $this->db->get();
        return $qry->row();
    }
    function view_addons1($id) {
        $this->db->select('add_ons.id as id,add_ons.type,add_ons.image,add_ons.discription,add_ons.charge,adons_type.id,adons_type.adons_type,temp_cart.add_ons1');
        $this->db->from('add_ons');
        $this->db->join('adons_type', 'adons_type.id=add_ons.type');
        $this->db->join('temp_cart', 'add_ons.id=temp_cart.add_ons1');
        $this->db->where('add_ons.id', $id);
        $qry = $this->db->get();
        return $qry->row();
    }
    function view_addons2($id) {
        $this->db->select('add_ons.id as id,add_ons.type,add_ons.image,add_ons.discription,add_ons.charge,adons_type.id,adons_type.adons_type,temp_cart.add_ons2');
        $this->db->from('add_ons');
        $this->db->join('adons_type', 'adons_type.id=add_ons.type');
        $this->db->join('temp_cart', 'add_ons.id=temp_cart.add_ons2');
        $this->db->where('add_ons.id', $id);
        $qry = $this->db->get();
        return $qry->row();
    }
    function view_addons3($id) {
        $this->db->select('add_ons.id as id,add_ons.type,add_ons.image,add_ons.discription,add_ons.charge,adons_type.id,adons_type.adons_type,temp_cart.add_ons3');
        $this->db->from('add_ons');
        $this->db->join('adons_type', 'adons_type.id=add_ons.type');
        $this->db->join('temp_cart', 'add_ons.id=temp_cart.add_ons3');
        $this->db->where('add_ons.id', $id);
        $qry = $this->db->get();
        return $qry->row();
    }
    function stich_charge() {
        $this->db->select('charge');
        $this->db->from('shipping_charge');
        $this->db->where('label', 'stiching_charge');
        $qr = $this->db->get();
        return $qr->row();
    }
    public function checkcart($data) {
        $id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('customer_id', $id);
        $this->db->where('product_id', $data['product_id']);
        $this->db->where('color_id', $data['color_id']);
        $this->db->where('size_id', $data['size_id']);
        $query1 = $this->db->get()->result();
        if (count($query1) > 0) {
            $result = array();
            foreach ($query1 as $product) {
                $result[] = array('product_id' => $product->product_id, 'color_id' => $product->color_id, 'price' => $product->price, 'size_id' => $product->size_id, 'quantity' => $product->quantity, 'customer_id' => $product->customer_id);
                //  $q=$product->quantity+1;
                $a = $data['quantity'];
                $q = $product->quantity + $a;
                $a = array('quantity' => $q);
                $this->db->where('customer_id', $id);
                $this->db->where('product_id', $data['product_id']);
                $this->db->where('color_id', $data['color_id']);
                $this->db->where('size_id', $data['size_id']);
                if ($this->db->update('cart', $a)) {
                    $this->db->where('customer_id', $id);
                    $result = $this->db->delete('wishlist');
                }
                return "exist";
            }
        } else {
            return "noexist";
        }
    }
    public function addalltocart() {
        $id = $this->session->userdata('user_id');
        $this->db->select('customer_id,product_id,color_id,size_id,quantity,price');
        $this->db->from('wishlist');
        $this->db->where('customer_id', $id);
        $query1 = $this->db->get()->result();
        if (count($query1) > 0) {
            $result = array();
            foreach ($query1 as $product) {
                $result[] = array('product_id' => $product->product_id, 'color_id' => $product->color_id, 'price' => $product->price, 'size_id' => $product->size_id, 'quantity' => $product->quantity, 'customer_id' => $product->customer_id);
            }
        }
        if ($this->db->insert_batch('cart', $result)) {
            $this->db->where('customer_id', $id);
            $result = $this->db->delete('wishlist');
            return "success";
        } else {
            return "error";
        }
    }
    public function cartdetails() {
        $id = $this->session->userdata('user_id');
        $result = $this->db->query('SELECT cart.id as id,cart.product_id,product.product_name,product.product_code,product.prod_display,product.price,color.image,size.size_type,cart.quantity FROM cart INNER JOIN product ON product.id = cart.product_id LEFT JOIN color ON color.id = cart.color_id LEFT JOIN size ON size.id = cart.size_id WHERE cart.customer_id = ' . $id)->result();
        $new_result = array();
        $grand = 0;
        $total = 0;
        foreach ($result as $rs) {
            $total = $rs->quantity * $rs->price;
            $rs->total = $total;
            $grand+= $total;
            $new_result[] = $rs;
        }
        return array('result' => $new_result, 'total' => $grand);
    }
    ////////////add to wish list////
    public function addtowishlist($data) {
        if ($this->db->insert('wishlist', $data)) {
            return "success";
        } else {
            return "error";
        }
    }
    public function remove_cart($data) {
        $this->db->where('id', $data['id']);
        $result = $this->db->delete('cart');
        if ($result) {
            //echo $this->db->last_query();
            //exit;
            return "success";
        } else {
            return "error";
        }
    }
    function add_stitching() {
        if ($this->db->insert('stitching', $data)) {
            return "success";
        } else {
            return "error";
        }
    }
    function payment($data) {
        $id = $this->session->userdata('user_id');
        $unique_id = rand(111111111, 999999999);
        $booking_code = 'ZYA' . $unique_id;
        $book = array('booking_no' => $booking_code, 'address_id' => $data['address_id'], 'total_rate' => $data['total_rate'], 'customer_id' => $id, 'payment_type' => $data['payment_type'], 'booked_date' => date('Y-m-d H:i:s'));
        $this->db->insert('booking', $book);
        $booking_id = $this->db->insert_id();
        $bill_address = array('booking_id' => $booking_id, 'firstname' => $data['firstname'], 'lastname' => $data['lastname'], 'address_1' => $data['address_1'], 'address_2' => $data['address_2'], 'state_id' => $data['state_id'], 'city_id' => $data['city_id'], 'Zip' => $data['Zip'], 'telephone' => $data['telephone']);
        $this->db->insert('billing_address', $bill_address);
        $customer_name = $data['firstname'] . ' ' . $data['lastname'];
        $customer_no = $data['telephone'];
        $cart = $this->db->where('customer_id', $id)->get('cart')->result();
        foreach ($cart as $rs) {
            $this->db->set('quantity', "quantity - $rs->quantity", FALSE);
            $this->db->where('product_id', $rs->product_id);
            $this->db->where('color_id', $rs->color_id);
            $this->db->where('size_id', $rs->size_id);
            $this->db->update('product_quantity');
            //echo $this->db->last_query();
            $data = array('booking_id' => $booking_id, 'product_id' => $rs->product_id, 'color_id' => $rs->color_id, 'size_id' => $rs->size_id, 'stitch_id' => $rs->stitch_id, 'quantity' => $rs->quantity);
            $new_result[] = $data;
        }
        $this->db->insert_batch('order_details', $new_result);
        $message = "You have received a new order from the customer " . $customer_name;
        $mobile = "9526039555";
        $this->send_otp($message, $mobile);
        $message = "An order has been Successfully placed in Zyva. Order number: " . $booking_code;
        //$mobile = "9526039555";
        $this->send_otp($message, $customer_no);
        $this->db->where('customer_id', $id)->delete('cart');
        return $booking_code;
    }
    function send_otp($msg, $mob_no) {
        $sender_id = "TWSMSG"; // sender id
        $pwd = "968808"; //your SMS gatewayhub account password
        $str = trim(str_replace(" ", "%20", $msg));
        // to replace the space in message with  ‘%20’
        $url = "http://api.smsgatewayhub.com/smsapi/pushsms.aspx?user=nixon&pwd=" . $pwd . "&to=91" . $mob_no . "&sid=" . $sender_id . "&msg=" . $str . "&fl=0&gwid=2";
        // create a new cURL resource
        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // grab URL and pass it to the browser
        $result = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);
    }
    public function count_cart() {
        $id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where("customer_id", $id);
        $query = $this->db->get();
        //return $query->result();
        //$qry=$query->count->num_rows();
        $s = $query->num_rows();
        return $s;
    }
    // public function delete_tempcart() {
    //     $sess_id = $this->session->userdata('session_id');
    //     $this->db->where("session_id", $sess_id);
    //     $result = $this->db->delete('temporary_cart');
    //     // echo $this->db->last_query();die;
    //     return $result;
    // }
}
?>