<?php

$ci =& get_instance();

function getSettings(){

	$ci = & get_instance();
	$ci->db->where('id',1);
	$s = $ci->db->get('settings');
	$s = $s->row();
	return $s;
}

// function get_size($id){
// 	$ci = & get_instance();
// 	$ci->db->distinct();
// 	$ci->db->select('size.size_type');
//     $ci->db->from('product_quantity');
//     $ci->db->join('size', 'product_quantity.size_id=size.id');
//     $ci->db->where('product_quantity.product_id', $id);
//     $ci->db->where('product_quantity.quantity >', 0);
//     $ci->db->where('size.id !=',7);
//     $qry = $ci->db->get()->result();
//     $rs= array();
//     foreach ($qry as $result) {
//     	$rs[] = $result->size_type;
// 	}
// 	return $size = implode(', ',$rs);
// }


function get_user_data(){
	$ci =& get_instance();
	$sessionData = $ci->session->userdata('user_value');
    $session_id = $sessionData['id'];

	$ci->db->select('*');
    $ci->db->where('id', $session_id);
    $query = $ci->db->get('user');
    $result = $query->row();

	if($result){
		$result->status = 'success';
		return $result;
	} else {
		return false;
	}
}

function get_icon(){

	$CI = & get_instance();
	$CI->db->select('*');
	$CI->db->where('id', 1);
	$CI->db->from('settings');
	$result = $CI->db->get()->row();
	return $result;

}

function get_reseller($id){
	$CI = & get_instance();
	$CI->db->select('reseller_id');
	$CI->db->where('id', $id);
	$result = $CI->db->get('customer')->row();
	return $result->reseller_id;
}





function get_addons_list($id){


	$ci = & get_instance();
	return $ci->db->query("SELECT `add_ons`.id,`add_ons`.type,`add_ons`.discription,`add_ons`.image,`add_ons`.charge,`adons_type`.adons_type FROM `add_ons` INNER JOIN `adons_type` ON `add_ons`.type = `adons_type`.id WHERE `add_ons`.type =".$id)->result();
/*	$CI->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
	  $CI->db->from("adons_type");
      $CI->db->join("add_ons",'add_ons.type =adons_type.id');
	$CI->db->where('add_ons.type', $id);
	// $CI->db->get();
	$result = $CI->db->get();//->result();

	echo $this->db->last_query();*/
	// print_r($result);
	//return $result;*/

}



function set_upload_logo($path) {   
	//upload an image options
	$config = array();
	$config['upload_path'] = $path;
	$config['allowed_types'] = 'gif|jpg|png';
	$config['width'] = 60;
    $config['height'] = 80;
	$config['overwrite']     = FALSE;

	return $config;
}





function set_upload_optionsgallery($path) {   
	//upload an image options
	$config = array();
	$config['upload_path'] = $path;
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']      = '0';
	$config['overwrite']     = FALSE;

	return $config;
}


function set_upload_profile($path) {   
	//upload an image options
	$config = array();
	$config['upload_path'] = $path;
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']      = '0';
	$config['overwrite']     = FALSE;

	return $config;
}

function set_upload_profilepic($path) {   
	//upload an image options
	$config = array();
	$config['upload_path'] = $path;
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']      = '0';
	$config['overwrite']     = FALSE;

	return $config;
}

function set_upload_banner($path) {   
	//upload an image options
	$config = array();
	$config['upload_path'] = $path;
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']      = '900';
	$config['overwrite']     = FALSE;

	return $config;
}

function set_banner_image($path) {   
	//upload an image options
	$config = array();
	$config['upload_path'] = $path;
	$config['allowed_types'] = 'gif|jpg|png';
	$config['max_size']      = '900';
	$config['overwrite']     = FALSE;

	return $config;
}

function get_image_path($id){
	$ci = & get_instance();
	$rs = $ci->db->select('product_image,format')->where('product_id',$id)->get('product_gallery')->row();
	if(count($rs)>0)
	return base_url('admin/'.$rs->product_image.$rs->format);
}

function get_city_name($id){
	$ci = & get_instance();
	$city = $ci->db->where('id',$id)->get('city')->row();
	if(count($city)>0)
	return $city->city_name;
}

function get_state_name($id){
	$ci = & get_instance();
	$city = $ci->db->where('id',$id)->get('state')->row();
	if(count($city)>0)
	return $city->state_name;
}

function get_status($status){
	if($status=='1'){
		return 'Booked';
	} else if($status=='2'){
		return 'Processing';
	} else if($status=='3'){
		return 'Delivered';
	} else {
		return 'Cancelled';
	}
}

function set_upload_options()
{   
    //upload an image options
    $config = array();
    $config['upload_path'] = 'assets/uploads';
    $config['allowed_types'] = '*';
    $config['max_size']      = '0';
    $config['overwrite']     = FALSE;

    return $config;
}

function get_size($id){
		$ci = & get_instance();
		$ci->db->distinct();
		$ci->db->select('size.size_type');
	    $ci->db->from('product_quantity');
	    $ci->db->join('size', 'product_quantity.size_id=size.id');
	    $ci->db->where('product_quantity.product_id', $id);
	    $ci->db->where('product_quantity.quantity >', 0);
	    $ci->db->where('size.id !=',7);
	    $qry = $ci->db->get()->result();
	    $rs= array();
	    foreach ($qry as $result) {
	    	$rs[] = $result->size_type;
		}
		return $size = implode(', ',$rs);
	}

function upload_image(){
	include('class.upload.php');
}

function get_actual_qty($product_id,$color_id,$size_id,$qty){
	$ci = & get_instance();
	
    $rs = $ci->db->query("SELECT quantity FROM product_quantity WHERE color_id='$color_id' AND size_id ='$size_id' AND product_id ='$product_id'")->row();
    return $quantity = $rs->quantity>15?15:$rs->quantity;
    
}


function max_quantity($id){
	$ci = & get_instance();
	$row = $ci->db->query('SELECT MAX(quantity) as quantity FROM `product_quantity` WHERE product_id ='.$id)->row();
    return $row->quantity;
}

function  get_stitching_price($stitch_id){
	$ci = & get_instance();
	$ci->db->where('id', $stitch_id);
	$ci->db->where('status', '1');
	$ci->db->select('*');
	$result = $ci->db->get('stitching')->row();
	return $result->stitch_price;
}

function  get_reseller_selling_price($reseller_id, $product_id){
	$ci = & get_instance();
	$ci->db->where('reseller_id', $reseller_id);
	$ci->db->where('product_id', $product_id);
	$ci->db->select('price');
	$result = $ci->db->get('reseller_price')->row();
	return $result->price;
}

function get_dealer_price($product_id) {
	$ci = & get_instance();
	$ci->db->where('id', $product_id);
	$ci->db->select('dp');
	$result = $ci->db->get('product')->row();
	return $result->dp;
}

?>
