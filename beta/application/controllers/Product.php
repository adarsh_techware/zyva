<?php
class Product extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Product_model');
        $this->load->model('Home_model');
        $this->load->model('Women_neck_model');
        $this->load->helper('general');
    }
    public function index($cat_id) {
        //error_reporting(0);
        // if ($cat_id == null) {
        //     redirect(base_url());
        // } else {
            $data['start'] = 0;
            $data['end'] = 24;
            $data['cat_id'] = $cat_id;
            $data['order_by'] = 'product';
            $data['size_id'] = '';
            $template['cat_id'] = $cat_id;
            $template['sub_id'] = '';
            $template['total_products'] = $this->Product_model->prod_by_cat_all($cat_id);
            //echo $this->db->last_query();
            $template['last_limit'] = $data['end'];
            $template['product'] = $this->Product_model->prod_by_cat($data);
            $template['data'] = $this->Home_model->userinfo();
            if ($this->session->userdata('user_id')) {
                $user = $template['data'];
                $new_res = array();
                if ($user->reseller_id > 0) {
                    foreach ($template['product'] as $rs) {
                        $rs->price = $this->check_reseller($user->reseller_id, $rs->id, $rs->price);
                        $new_res[] = $rs;
                    }
                    $template['product'] = $new_res;
                }
            }
            $template['Category'] = $this->Product_model->category_list();
            $template['sub_category'] = $this->Product_model->sub_category_list($cat_id);
            //print_r($template['sub_category']);die;
            $template['color'] = $this->Product_model->color_list();
            //print_r($template['color']);die;
            $template['result'] = $this->Home_model->shoppingbag();
            $template['cartlist'] = $this->Home_model->cartlist();
            $template['size'] = $this->Product_model->size_list();
            $template['page'] = 'product/product_page';
        // }
        $this->load->view('template', $template);
    }
    public function check_reseller($reseller_id, $prod_id, $price) {
        $row = $this->db->where('product_id', $prod_id)->where('reseller_id', $reseller_id)->get('reseller_price')->row();
        if (count($row) > 0) {
            return $row->price;
        } else {
            return $price;
        }
    }
    public function product_sub($cat_id = null, $sub_id = null) {
        if ($cat_id == null || $sub_id == null) {
            redirect(base_url());
        } else {
            $data['start'] = 0;
            $data['end'] = 24;
            $data['cat_id'] = $cat_id;
            $data['sub_id'] = $sub_id;
            $data['order_by'] = 'product';
            $data['size_id'] = '';
            $template['cat_id'] = $cat_id;
            $template['sub_id'] = $sub_id;
            $template['total_products'] = $this->Product_model->prod_by_cat_all($cat_id, $sub_id);
            $template['last_limit'] = $data['end'];
            $template['product'] = $this->Product_model->prod_by_cat($data);
            $template['data'] = $this->Home_model->userinfo();
            if ($this->session->userdata('user_id')) {
                $user = $template['data'];
                $new_res = array();
                if ($user->reseller_id > 0) {
                    foreach ($template['product'] as $rs) {
                        $rs->price = $this->check_reseller($user->reseller_id, $rs->id, $rs->price);
                        $new_res[] = $rs;
                    }
                    $template['product'] = $new_res;
                }
            }
            $template['Category'] = $this->Product_model->category_list();
            $template['result'] = $this->Home_model->shoppingbag();
            $template['cartlist'] = $this->Home_model->cartlist();
            $template['size'] = $this->Product_model->size_list();
            $template['page'] = 'product/product_page';
        }
        $this->load->view('template', $template);
    }
    public function product_info($pdt_id = null) {
        error_reporting(0);
        if ($pdt_id == null) {
            redirect(base_url());
        }
        $user_id= $this->session->userdata('user_id');
        if (!isset($user_id)){
			$this->session->set_userdata('session_id',session_id());
		}
		$template['$sess_id'] = $this->session->userdata('session_id');
        $product_data = $this->db->where('prod_display', $pdt_id)->get('product')->row();
        if (count($product_data) == 0) {
            $template['Category'] = $this->Product_model->category_list();
            $template['data'] = $this->Home_model->userinfo();
            $template['result'] = $this->Home_model->shoppingbag();
            $template['cartlist'] = $this->Home_model->cartlist();
            $template['page'] = 'my404';
            $this->load->view('template', $template);
        } else {
            $template['Category'] = $this->Product_model->category_list();
            $template['data'] = $this->Home_model->userinfo();
            $template['result'] = $this->Home_model->shoppingbag();
            $template['cartlist'] = $this->Home_model->cartlist();
            $template['prod_info'] = $this->Product_model->product_info($pdt_id);
            if ($this->session->userdata('user_id')) {
                $user = $template['data'];
                $new_res = array();
                if ($user->reseller_id > 0) {
                    $template['prod_info']->price = $this->check_reseller($user->reseller_id, $template['prod_info']->id, $template['prod_info']->price);
                }
            }
            $product_name = $template['prod_info']->product_name;
            $Prod_display = "- Zyva Fashions";
            $template['title'] = $product_name . ' ' . $Prod_display;
            $template['description'] = $template['prod_info']->description;
            $template['cate_gallery'] = $this->Product_model->same_category($pdt_id);
            //echo '<pre>';
            //print_r($template['cate_gallery']);
            if ($this->session->userdata('user_id')) {
                $user = $template['data'];
                $new_res = array();
                if ($user->reseller_id > 0) {
                    foreach ($template['cate_gallery'] as $rs) {
                        $rs->price = $this->check_reseller($user->reseller_id, $rs->id, $rs->price);
                        $new_res[] = $rs;
                    }
                    $template['cate_gallery'] = $new_res;
                }
            }
            //print_r($template['cate_gallery']);
            $template['front'] = $this->Women_neck_model->front_neck();
            $template['back'] = $this->Women_neck_model->back_neck();
            $template['sleeve'] = $this->Women_neck_model->sleeve_neck();
            $template['hemline'] = $this->Women_neck_model->hemline_neck();
            $template['adons'] = $this->Women_neck_model->viewadons();
            $template['adons_sub'] = $this->Women_neck_model->viewsub_adons();
            $template['measurement'] = $this->Product_model->get_measurement();
            $template['page'] = 'product/product_info';
            $this->load->view('template', $template);
        }
    }
}
?>