<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('Home_model');

		$this->load->model('Women_model');

		$this->load->helper('general_helper');
	}

	function index() {
		$template['title'] = 'Maryam Apparels | Terms';
		$template['page'] = 'terms/terms';

		$template['data'] = $this->Women_model->userinfo();

		$template['result']=$this->Home_model->shoppingbag();

		$template['cartlist']=$this->Home_model->cartlist();

		$template['Category'] = $this->Women_model->category();

		$template['banner'] = $this->Home_model->getbanners();

		$template['result']=$this->Home_model->shoppingbag();
		$this->load->view('template',$template);
	}


	function policy() {
		$template['title'] = 'Maryam Apparels | Policy';
		$template['page'] = 'terms/policy';
		$template['data'] = $this->Women_model->userinfo();

		$template['result']=$this->Home_model->shoppingbag();

		$template['cartlist']=$this->Home_model->cartlist();

		$template['Category'] = $this->Women_model->category();

		$template['banner'] = $this->Home_model->getbanners();

		$template['result']=$this->Home_model->shoppingbag();
		$this->load->view('template',$template);
	}


	function disclaimer() {
		$template['title'] = 'Maryam Apparels | Disclaimer and limitation of liability';
		$template['page'] = 'terms/disclaimer';

		$template['data'] = $this->Women_model->userinfo();

		$template['result']=$this->Home_model->shoppingbag();

		$template['cartlist']=$this->Home_model->cartlist();

		$template['Category'] = $this->Women_model->category();

		$template['banner'] = $this->Home_model->getbanners();

		$template['result']=$this->Home_model->shoppingbag();
		$this->load->view('template',$template);
	}

	function payment_logistics() {
		$template['title'] = 'Maryam Apparels | Policy';
		$template['page'] = 'terms/payment';
		$template['data'] = $this->Women_model->userinfo();

		$template['result']=$this->Home_model->shoppingbag();

		$template['cartlist']=$this->Home_model->cartlist();

		$template['Category'] = $this->Women_model->category();

		$template['banner'] = $this->Home_model->getbanners();

		$template['result']=$this->Home_model->shoppingbag();
		$this->load->view('template',$template);
	}

}