<?php
class Transaction extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Transaction_model');
    }

    public function index() {
    	echo "Hai";
    	$template['page'] = 'Transaction/list_transaction';
    	$template['page_title'] = "Transaction";
		$template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
		$template['transaction_det'] = $this->Transaction_model->get_transaction();
		$this->load->view('template',$template);
    }
}
?>