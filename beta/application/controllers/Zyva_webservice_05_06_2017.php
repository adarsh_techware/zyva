<?php
defined('BASEPATH') OR exit('No direct script access allowed');


if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

	// Access-Control headers are received during OPTIONS requests
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
			header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
			header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		exit(0);
	}

class Zyva_webservice extends CI_Controller {

 public function __construct() {
 parent::__construct();
 $this->load->model('Webservice_model');
 $this->load->model('Zyvawebservice_model');
 $this->load->library('upload');
 		}



 		public function signup_data() {
		$data = json_decode(file_get_contents("php://input"));
		unset($data->gender);
		//echo $data;die;
		$status = "error";
		$msg = "Username or Email or Phone already exist";
		$result = false;
		if($data) {
			$this->db->where("email", $data->email);
			
			$this->db->or_where("telephone", $data->telephone);
			$count = $this->db->count_all_results("customer");
			
			if($count == 0){
				$data->email = $data->email;
				$data->password = md5($data->password);
				$data->photo="http://192.168.138.31/TRAINEES/Asha/zyva_project/assets/uploads/aaaa.png";
				$result = $this->db->insert("customer", $data);
				if($result) {
					$status = "success";
					$msg = "Registered successfully";
				}
			    $result = $data;
			}
		}
		
		echo json_encode(array("status" => $status, "message" => $msg, "data" => $result));
		
	}



	public function signin() {
		$data = json_decode(file_get_contents("php://input"));
		$result = false;
		if(isset($data)) {

			$where = array('username'=>$data->username,
						   'password'=> md5($data->password),
						   'status'=>1);
			$query = $this->db->where($where)->get('reseller');
		  
		  if($query->num_rows() == 1) {
			  $result = $query->row();
			  $status = "success";
			  $msg = "Login successfully";
		  } else{
			$status = "error";
		    $msg = "Invalid Login";
		  }

		} else {
			$status = "error";
		    $msg = "Invalid arguments";
		}  
		echo json_encode(array("status" => $status, "message" => $msg, "data" => $result));
		
	}




	////get_user


	public function get_seller() {
		$data = json_decode(file_get_contents("php://input"));
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		if(!empty($data->id)) {
			$this->db->select('*');
			$this->db->from('reseller');
	    	$this->db->where('id',$data->id);
			$result = $this->db->get()->row();		
			$status = "success";
			$msg = "profile";
		} else {
			$msg = "Invalid arguments. Please try again";
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result));
	}


	///get_badge
	public function get_badge() {
		$data = json_decode(file_get_contents("php://input"));
		$result1 = 0;
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;		
		if(!empty($data->id)) {
			$this->db->select('id');
			$this->db->from('booking');
		    $this->db->where('reseller_id',$data->id);
			$query= $this->db->get();
			$result1 = $query->num_rows();
	
			if(!empty($result1)) {

				if($result1>=20){
					$result = base_url("assets/badge/z-badge1.png");
				} else if ($result1>=10) {
					$result = base_url("assets/badge/z-badge2.png");
				} else if ($result1>=5) {
					$result = base_url("assets/badge/z-badge3.png");
				} else if ($result1>=4) {
					$result = base_url("assets/badge/z-badge4.png");
				} else{
				 	$result = base_url("assets/badge/z-badge5.png");
				}
		
            	$status = "success";
				$msg = "profile";
			} else{
					$status = "error";
					$msg = "Sorry, error occured. Please try again";
					$result = false;
			}
		} else {
			$status = "error";
			$msg = "Invalid arguments. Please try again";
			$result = false;
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'count'=>$result1));
		
	}



	// Get Home page content
	public function get_homepage() {

		
		 $this->db->select('product.id as id,product.*,
		                   product_gallery.product_image,
		                   product_gallery.format,
		                    product_gallery.product_id,
							 product_quantity.quantity,
							 product.status
							 ');
        $this->db->from('product');
       
        $this->db->join('product_gallery','product_gallery.product_id= product.id','left');
        $this->db->join("product_quantity",'product_quantity.product_id=product.id','left');

        $this->db->group_by('product.id');
        $this->db->where('product.status',1);
        $this->db->order_by('product.id','DESC');
        //$this->db->limit(6);
        $query = $this->db->get();
        $result = $query->result();
        $row_count=$query->num_rows();
        $new_result = array();
        foreach ($result as $rs) {
        	$rs->share_image = base_url('admin/'.$rs->product_image.$rs->format);
        	$rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
        	
        	$rs->quantity = $this->Zyvawebservice_model->stock_qty($rs->id)->quantity;
        	$new_result[] = $rs;
        }
         //print_r($row_count);
        
        $max_count = $this->Zyvawebservice_model->get_maxcount();
        //print_r($max_count);
        if($result) {
		 	$status = "success";
			$msg = "Homedata";
		}else{
		 	$status = "error";
	       	$msg = "Unable to fetch data";
	       	$result = false;
		}

	    
			 

			
	echo json_encode(array("status" => $status, "message" => $msg, "data" => $new_result,'row_count'=>$row_count,'max_count'=>$max_count));
	}



	///get product detail

	function product_detail(){
        $data = json_decode(file_get_contents("php://input"));
        $id=$data->product_id;
		$uid=$data->user_id;

		$result['main_image']=$this->Zyvawebservice_model->viewmainimage($id);
		   //print_r($result['main_image']->product_image);
        $result['prod'] = $this->Zyvawebservice_model->viewproduct1($id);
 	    $result['color'] = $this->Zyvawebservice_model->viewcolor($id);
        $result['size'] = $this->Zyvawebservice_model->viewproductsize($id);
        $result['images'] = $this->Zyvawebservice_model->viewitem_imgslider();
        $result['subimage']=$this->Zyvawebservice_model->viewsubimg($id);
        $qty =  $this->Zyvawebservice_model->stock_qty($id);
        $result['custom'] = $this->Zyvawebservice_model->custom_check($id);

             
           $this->db->select('*');
           $this->db->where('reseller_id', $uid);
           $this->db->where('product_id', $id);
           $query = $this->db->get('reseller_price');
           $result1 = $query->row();
           
           if($result1){

             $original_price=$result1->price;

           }

           else{

           	 $original_price=$result['prod']->price;
           }

       


	if($result)
			 {
			 	$status = "success";
			    $msg = "select items";
			//$result=$result['categories'];
			 }else{
			 	$status = "error";
		       $msg = "Unable to fetch data";
		       $result = false;
			 }
			
        

       if(isset($qty->quantity)){
       	$quantity = $qty->quantity;
       } else {
       	$quantity = 0;
       }
        
        
    
    echo json_encode(array("status" => $status, "message" => $msg, "data" => $result,'price'=>$original_price,"qty"=>$quantity,"custom"=>$result['custom']));
  }


////get_color size

  
  public function get_color_size(){
  	$data = json_decode(file_get_contents("php://input"));
        //print_r($data);die;
		
		$cid=$data->cid;
		$pid=$data->pid;
		$uid=$data->user_id;

 $result['size']=$this->db->select('size.id,size.size_type,product_quantity.quantity')->where('product_id',$pid)->where('color_id',$cid)->where('size_type!=','custom')->where('product_quantity.quantity >','0')->from('product_quantity')->join('size','size.id=product_quantity.size_id')->get()->result();

 $result['custom'] = $this->Zyvawebservice_model->custom_color_check($pid,$cid);
       
               $result['prod'] = $this->Zyvawebservice_model->viewproduct1($pid);
 	    	$result['color'] = $this->Zyvawebservice_model->viewcolor($pid);
            //$result['size'] = $this->Zyvawebservice_model->viewproductsize($pid);
             $result['images'] = $this->Zyvawebservice_model->viewitem_imgslider();
             $result['subimage']=$this->Zyvawebservice_model->viewsubimg($pid);
             
             
               

           $this->db->select('*');
           $this->db->where('reseller_id', $uid);
           $this->db->where('product_id', $pid);
           $query = $this->db->get('reseller_price');
           $result1 = $query->row();
           
           if($result1){

             $original_price=$result1->price;

           }

           else{

           	 $original_price=$result['prod']->mrp;
           }

       


	if($result)
			 {
			 	$status = "success";
			    $msg = "select items";
			//$result=$result['categories'];
			 }else{
			 	$status = "error";
		       $msg = "Unable to fetch data";
		       $result = false;
			 }
			
        

       
        
        
   



 echo json_encode(array("status" => $status, "message" => $msg, "data" => $result,'price'=>$original_price));


    }





		//get_cart


		public function get_cart() {

			$stitching_charge = 0;
			$addon_charge = 0;
			$selling_price = 0;
			$delivery_charge = 0;

			$data = json_decode(file_get_contents("php://input"));

			$status = "error";
			$msg = "Sorry, error occured. Please try again";
			$result = false;

			$delevery = $this->Zyvawebservice_model->delivery_charge();

			$delevery_charge = $delevery->charge;

			if(isset($data->addons1)){
				$addons1=$data->addons1;
				$addons2=$data->addons2;
				$addons3=$data->addons3;
				$addons4=$data->addons4;
				$addon_charge=$addons1+$addons2+$addons3+$addons4;
			}

			$this->db->where('cart.reseller_id',$data->user_id);
	    	$this->db->where('cart.product_id',$data->product_id);
	    	$rs = $this->db->get('cart')->row();

	    	if($rs->size_id==7){
	    		$stitch_charge=$this->Zyvawebservice_model->stitch_charge();           
            	$stitching_charge = $stitch_charge->charge;
	    	}


	    	$selling_price = ($rs->quantity*$rs->price);
	    	$addon_charge = ($rs->quantity*$addon_charge);
	    	$stitching_charge = ($rs->quantity*$stitching_charge);

	    	$total_paid = $delevery_charge + $selling_price + $addon_charge + $stitching_charge;

	    	$status = 'success';
	    	$msg = "Order placed in cart successfully";
	    	
							

	    	$this->db->select('cart.id as cid,cart.quantity,cart.product_id,product.product_name,size.size_type,color.image,product_gallery.product_image,product_gallery.format,product.price as pprice')->from('cart')->join('product','product.id = cart.product_id')->join('size','size.id = cart.size_id','left')->join('color','color.id = cart.color_id')->join('product_gallery', 'product_gallery.product_id = cart.product_id','left')->where('cart.reseller_id',$data->user_id)->where('cart.product_id',$data->product_id);

	    	$query = $this->db->get();

        	$result['prod'] = $query->row();

        	$result['prod']->product_image = base_url('admin/'.$result['prod']->product_image.'_201x302'.$result['prod']->format);

	    	$res = array('status' => $status,
	    				 'message' => $msg,
	    				 'selling_price'=>$selling_price,
	    				 'addon_charge'=>$addon_charge,
	    				 'stitching_charge'=>$stitching_charge,
	    				 'delevery_charge'=>$delevery_charge,
	    				 'total'=>$total_paid,
	    				 'quantity'=>$rs->quantity,
	    				 'data' => $result);

	    	print json_encode($res);

	    	/*echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'total'=>$total,'discount'=>$discount,'subtotal'=>$subtotal,'actualprice'=>$actualprice,'reseller_price'=>$reseller_price,'pay_amount'=>$paid_price,'stitch_charge'=>$stitch,'addons_charge'=>$addons_charge,'delevery_charge'=>$delevery_charge));



			$actualprice = 0;
			$reseller_price = 0;
			$paid_price = 0;
			//$stitch_charge=0;
             $stitch=0;
			$addons_charge=0;
		$data = json_decode(file_get_contents("php://input"));

		//print_r($data->free_stitch);exit;

		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;

         
		if($data->user_id) {
			
          $delevery=$this->Zyvawebservice_model->delivery_charge();
           //print_r($delevery->charge);exit;
           $delevery_charge=$delevery->charge;

             if(!empty($data->addons1)){

             	$addons1=$data->addons1;
             }
             else{

             	$addons1=0;
             }

             if(!empty($data->addons2)){

             	$addons2=$data->addons2;
             }
             else{

             	$addons2=0;
             }

             if(!empty($data->addons3)){

             	$addons3=$data->addons3;
             }
             else{

             	$addons3=0;
             }

             if(!empty($data->addons4)){

             	$addons4=$data->addons4;
             }
             else{

             	$addons4=0;
             }

           $addons_charge=$addons1+$addons2+$addons3+$addons4;


   $this->db->select('cart.id as cid,cart.stock_status,
		                    cart.price,cart.quantity,
		                    cart.product_id,
		                    
		                    product.product_name,
		                    product.dp,
		                    product.mrp,

							product.actual_price,
							product.product_offer,
							product.price as pprice,
							size.size_type,
							color.image,
						
							product_gallery.product_image,
							product_gallery.format
                               
		                  
							');
							
							
		$this->db->from('cart');
		$this->db->join('size', 'size.id = cart.size_id','left');
		$this->db->join('color', 'color.id = cart.color_id','left');
	  
	     $this->db->join('product', 'product.id = cart.product_id','left');
		 $this->db->join('product_gallery', 'product_gallery.product_id = cart.product_id','left');
		
	    $this->db->where('reseller_id',$data->user_id);
	    $this->db->where('cart.product_id',$data->product_id);
        //$this->db->group_by("cart.product_id"); 		
		$query = $this->db->get();

        $result['prod'] = $query->row();

        //print_r($result['prod']);exit;


        /*if($result['prod']->size_type=='Custom'){

           $stitch_charge=$this->Zyvawebservice_model->stitch_charge();
           
            $stitch=$stitch_charge->charge;

        }

        else{

        	  $stitch=0;
        }*//*
		
		$result['prod']->product_image = base_url('admin/'.$result['prod']->product_image.'_201x302'.$result['prod']->format);
		$total=0;
		$discount=0;
		$vat=0;
		$subtotal=0;


                    //print_r($result['prod']->size_type);exit;
		if($result['prod']->size_type=='Custom'){

           $stitch_charge=$this->Zyvawebservice_model->stitch_charge();
           
            $stitch=$stitch_charge->charge;

        } else{

        	  $stitch=0;
        }
		
        $mrp=$result['prod']->mrp;
        $reseller_price=$result['prod']->price;
        $qty=$result['prod']->quantity;
            //print_r($qty);exit;
            //$stitch=$stitch_charge->charge;
              $charges=$addons_charge + $stitch;
              //print_r($charges);exit;
             
            $paid_price=($reseller_price + $charges)*$qty;
           //print_r($paid_price);exit;

            $product_price=$qty*$mrp;
            $dp=$result['prod']->dp;
            
             $vatt=$result['prod']->product_offer;
             $total=$total+$product_price;
             $discount= $discount+$dp;
             $subtotal= $total-$discount;
             $actualprice= $subtotal;
             $total=$total;
		     
		
		      $actualprice=$actualprice;
		      $status = "success";
			  $msg = "cart";
		    
		 
		  

		
		
	}	
	 
	 
	 
	echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'total'=>$total,'discount'=>$discount,'subtotal'=>$subtotal,'actualprice'=>$actualprice,'reseller_price'=>$reseller_price,'pay_amount'=>$paid_price,'stitch_charge'=>$stitch,'addons_charge'=>$addons_charge,'delevery_charge'=>$delevery_charge));*/
		
		
}	









		//remove cart
	 public function remove_cart() {
		$data = json_decode(file_get_contents("php://input"));
		
		$cid=$data->id->prod->cid;
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		if($data->id) {
			 
			
			$this->db->where("id", $cid);
			$result=$this->db->delete("cart");

			$this->db->select('*');
			$this->db->from('cart');
			$this->db->where('id',$cid);
			$rr=$this->db->get();
			$res=$rr->num_rows();
			
			if($result){

            $status = "success";
			$msg = "Product removed from cart";
		      }
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'res'=>$res));
		
	}




///checkout1 for addto cart



	public function checkout1 () {
		$data = json_decode(file_get_contents("php://input"));
		//print_r($data);exit;
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		//print_r($data->stitch);exit;
		$unique_id    = rand(111111111, 999999999);
        $user_code = $unique_id;


        if(!empty($data->reprice)){

             //$original_price=$result1->price;
             $original_price=$data->reprice;


           }

           else{

           	 $original_price=$data->price;
           }



		//if(!empty($data->size_id)){

		if(isset($data->user_id)) {

			$this->db->where('reseller_id',$data->user_id)->delete('cart');

			   if($data->stitch==true){
                    $cart['size_id']=7;
           	
                   }
                   else{
                    $cart['size_id']=$data->size_id;

                   }

			
			$cart['reseller_id'] = $data->user_id;
			$cart['random_userid']=$user_code;
			$cart['product_id'] = $data->id;

			$cart['price'] = $original_price;
			//print_r($cart['price']);exit;
			$cart['stitching_on']=$data->stitch;
			$cart['color_id'] = $data->color_id;
			//$cart['size_id'] = $data->size_id;
            
            $cart['quantity']=1;



			$return = $this->db->insert('cart', $cart);
			$insert_id=$this->db->insert_id();

             
             $result=$return;
             $insert=$insert_id;
			if($result) {
				//$result['insert_id'] = $this->db->insert_id();
				//$result['status'] = 1;
				$status = "success";
				$msg = "Product added to cart";
				
				$notifi['customer_id']=$user_code;
				$notifi['reseller_id'] = $data->user_id;
			     $notifi['product_id'] = $data->id;
			     $notifi['message']=$msg;
			     $date = date('Y-m-d H:i:s');
			     $notifi['date']=$date;
				 $this->db->insert('notification',$notifi);
				 
				 //$count1=$this->Webservice_model->notification_count($data->user_id);
				 
			}

			
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'insert_id'=>$insert));
		
	}



 public function addaddress()
	 {
		 
		$data = json_decode(file_get_contents("php://input"));
		//print_r($data);exit;
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		 
				 

				   if($data) {
                $datas = new stdClass();

				 $datas->reseller_id =$data->user_id;
				$datas->name = $data->data1->name;
				$datas->mobile = $data->data1->mobile;
				$datas->pincode = $data->data1->pincode;
				$datas->address = $data->data1->address;
				$datas->town = $data->data1->town;
				$datas->city = $data->data1->city;
				



    $this->db->select('*');
           $this->db->where('reseller_id', $data->user_id);
           $query = $this->db->get('add_address1');
           $result = $query->row();

     if($result)
        {

              $this->db->where('reseller_id', $data->user_id);
             $result=$this->db->update('add_address1',$datas);
              //$status = "success";
			 //$msg = "success";
         }
         else{ 
  
      
            $result=$this->db->insert('add_address1',$datas);

	  
			 	
			
			}

			$status = "success";
			$msg = "successfully saved your address";
        

							 }	
			  
				 
				 echo json_encode(array('status' => $status, 'message' => $msg));
				 
				
		        
	 }



///add to addressbook


	 public function customer_address(){
	 	$data = json_decode(file_get_contents("php://input"));
	 	$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		$this->db->select('id');
		$this->db->from('customer');
		$this->db->where('telephone',$data->data1->mobile);
		$qry=$this->db->get()->row();
		//list($first_name,$last_name) = explode(' ',"$data->data1->name");
		list($first_name, $last_name) = array_pad(explode(' ', $data->data1->name, 2), 2, null);
		$last_name = $last_name!=null?$last_name:'';
		if(count($qry)==0){
			$res = array('reseller_id'=>$data->user_id,
						 'first_name'=>$first_name,
						 'last_name'=>$last_name,
						 'telephone'=>$data->data1->mobile,
						 'email'=>$data->data1->email,
						 'password'=>md5($data->data1->mobile),
						 'reseller_code'=>$data->reseller_code
						 );
			$rs_result = $this->db->insert('customer',$res);
			$customer = $this->db->insert_id();
			if($rs_result && $data->data1->email!=''){
				$this->Zyvawebservice_model->send_mail($res);
			}
		} else {
			$customer = $qry->id;
		}

		$datas = new stdClass();
		$datas->customer_id = $customer;
		$datas->firstname = $first_name;
		$datas->lastname = $last_name;
		$datas->email1 = $data->data1->email;
		$datas->telephone = $data->data1->mobile;
		$datas->address_1 = $data->data1->address;
		$datas->Zip = $data->data1->pincode;
		$datas->address_2 = $data->data1->city.', '.$data->data1->town;
		
		$this->db->insert('address_book',$datas);

		$address_id = $this->db->insert_id();
		$status = "success";
		$msg = "successfully saved your address";

		echo json_encode(array('status' => $status,'message' => $msg,'result'=>"$address_id",'customer_id'=>$customer));

	 }

	 public function addaddress2()
	 {
		 
		/*$data = json_decode(file_get_contents("php://input"));
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		$this->db->select('id');
		$this->db->from('customer');
		$this->db->where('telephone',$data->data1->mobile);
		$qry=$this->db->get();
		$datas = new stdClass();
		$datas->reseller_id =$data->user_id;
		$datas->name = $data->data1->name;
		$datas->mobile = $data->data1->mobile;
		$datas->pincode = $data->data1->pincode;
		$datas->address = $data->data1->address;
		$datas->town = $data->data1->town;
		$datas->city = $data->data1->city;

        $res=$qry->row();
		if($res){		
            
            $datas->customer_id=$res->id;
			
				


				   


    $this->db->select('*');
           $this->db->where('reseller_id', $data->user_id);
           $query = $this->db->get('add_address1');
           $result = $query->row();

     if($result)
        {

              $this->db->where('reseller_id', $data->user_id);
             $result=$this->db->update('add_address1',$datas);
              
         }
         else{ 
  
      
            $result=$this->db->insert('add_address1',$datas);

	  
			 	
			
			}

			$status = "success";
			$msg = "successfully saved your address";
        

							 }	
}
else{


          if($data) {
                $datas = new stdClass();
                 //$datas->customer_id=$res->id;
				 $datas->reseller_id =$data->user_id;
				$datas->name = $data->data1->name;
				$datas->mobile = $data->data1->mobile;
				$datas->pincode = $data->data1->pincode;
				$datas->address = $data->data1->address;
				$datas->town = $data->data1->town;
				$datas->city = $data->data1->city;
				


				   


    $this->db->select('*');
           $this->db->where('reseller_id', $data->user_id);
           $query = $this->db->get('add_address1');
           $result = $query->row();

     if($result)
        {

              $this->db->where('reseller_id', $data->user_id);
             $result=$this->db->update('add_address1',$datas);
              
         }
         else{ 
  
      
            $result=$this->db->insert('add_address1',$datas);

	  
			 
			
			}
               $datass = new stdClass();
			  $datass->reseller_id =$data->user_id;
				$datass->first_name = $data->data1->name;
				$datass->telephone = $data->data1->mobile;
				


			$result=$this->db->insert('customer',$datass);


			$status = "success";
			$msg = "successfully saved your address";
        

							 }	

}

			  //  $result = $datas;
				 
				 echo json_encode(array('status' => $status, 'message' => $msg));
				 
		*/		
		        
	 }



	 //add to order


	  function addto_order() {
        $data = json_decode(file_get_contents("php://input"));		
	    $id = $data->user_id->id;
	    $address_id = $data->address_id;		
        $unique_id    = rand(111111111, 999999999);
        
        $booking_code = 'ZYA' . $unique_id;

 		$date=date('Y-m-d H:i:s');
        
        $book = array(
            'booking_no' => $booking_code,
            'address_id' => $address_id,
            'total_rate' => $data->price,
            'reseller_id' => $id,
            'customer_id' => $data->customer_id,
            'payment_type' => "Cash on delevery",
            'booked_date' => $date,
            'status'=>1
        );
        $this->db->insert('booking', $book);

        $booking_id = $this->db->insert_id();
        
        
        $new_result = array();
        $cart = $this->db->where('reseller_id', $id)->get('cart')->result();
        
        foreach ($cart as $rs) {
            $this->db->set('quantity', "quantity - $rs->quantity", FALSE);
            $this->db->where('product_id', $rs->product_id);
            $this->db->where('color_id', $rs->color_id);
            $this->db->where('size_id', $rs->size_id);
            $this->db->update('product_quantity');

           
            $datas  = array(
                'booking_id' => $booking_id,
                'product_id' => $rs->product_id,
                'color_id' => $rs->color_id,
                'size_id' => $rs->size_id,
                'stitch_id' => $rs->stitch_id,
                'quantity' => $rs->quantity,
                'status'=>1
            );
            $new_result[] = $datas;
        }

        if(!empty($new_result)){
        	$this->db->insert_batch('order_details', $new_result);
        }
        
        


        
      	$result=  $this->db->where('reseller_id', $id)->delete('cart');
        
        //return $booking_code;



        if($result) {
			$status = "success";
			$msg = "successfully";
		}

		$resller_name = $this->Zyvawebservice_model->get_resller($id);

		$customer_no = $this->Zyvawebservice_model->get_customer($data->address_id);


		$message = "You have received a new order from the reseller ".$resller_name;
		$mobile = "9526039555";


		$this->send_otp($message,$mobile);

		$message = "The order has been placed by the reseller ".$resller_name.". Order number: ".$booking_code;
		//$mobile = "9526039555";

		$this->send_otp($message,$customer_no);




	
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'booking_id'=>$booking_id));							
		 
        }



    function send_otp($msg, $mob_no) {
		$sender_id="TWSMSG"; // sender id	
		$pwd="968808";   //your SMS gatewayhub account password	
		$str = trim(str_replace(" ", "%20", $msg));
		// to replace the space in message with  ‘%20’
		$url="http://api.smsgatewayhub.com/smsapi/pushsms.aspx?user=nixon&pwd=".$pwd."&to=91".$mob_no."&sid=".$sender_id."&msg=".$str."&fl=0&gwid=2";
		// create a new cURL resource
		$ch = curl_init();
		
		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// grab URL and pass it to the browser
		$result = curl_exec($ch);
		// close cURL resource, and free up system resources
		curl_close($ch);
	}





public function get_order() {
		$data = json_decode(file_get_contents("php://input"));

		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		if($data->id) {
			
		
			$this->db->select('order_details.id as id,order_details.*,
		                   
		                    product.product_name,
							 product.mrp,
							 
							 product.price,
							 booking.reseller_id,
							 booking.total_rate,
							 booking.booked_date,
					
							product_gallery.product_image,
							product_gallery.format
		                  
							');
							
							
		$this->db->from('order_details');
		$this->db->join('booking', 'booking.id = order_details.booking_id','left');
		$this->db->join('color', 'color.id = order_details.color_id','left');
	    $this->db->join('size', 'size.id = order_details.size_id','left');

	   $this->db->join('product', 'product.id = order_details.product_id','left');
		 $this->db->join('product_gallery', 'product_gallery.product_id = order_details.product_id','left');
		
	    $this->db->where('booking.reseller_id',$data->id);

        $this->db->group_by("order_details.booking_id"); 
        $this->db->order_by('id','desc');
        //$this->db->limit(5);		

			$result = $this->db->get()->result();
			$new_result = array();

			foreach ($result as $rs) {
				$rs->share_image = base_url('admin/'.$rs->product_image.$rs->format);
				$rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
				
        		$new_result[] = $rs; 
			}
			//print_r($result);
			
			$status = "success";
			$msg = "order";
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $new_result));
		
	}



	//Ger parent categories
	public function get_categories() {
		$data = json_decode(file_get_contents("php://input"));
		$result = array();
		
		$status = "success";
		$msg = "Categories";
		$id=$data->id;
		$result1=$this->Zyvawebservice_model->get_search_item($id);
		$rs = $this->db->where('category.status',1)->get('category')->result();
		foreach ($rs as $row) {
			$ros = $this->db->select('product_gallery.product_image,product_gallery.format')->where('product.category_id',$row->id)->from('product')->join('product_gallery','product_gallery.product_id= product.id')->order_by('product.id','DESC')->limit(1)->get()->row();
			if(count($ros)>0){
				$row->product_image = base_url('admin/'.$ros->product_image.'_201x302'.$ros->format);
				$result[] = array('id'=>$row->id,
							  'category_name'=>$row->category_name,
							  'product_image'=>$row->product_image);
			} 
			
			
		}
		
        $items=$this->Zyvawebservice_model->items();
       
     
echo json_encode(array("status" => $status, "message" => $msg, "data" => $result,"data1"=>$result1,"sugg"=>$items));
	}



///add_reseller_price

function add_reseller_price()
   {
      
        $data = json_decode(file_get_contents("php://input"));
        //print_r($data);die;
        $status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		$id=$data->user_id;


   if($data) {
                $datas = new stdClass();

				$datas->product_id =$data->product_id;
				$datas->reseller_id = $data->user_id;
				$datas->price = $data->reprice;
				
			
           $this->db->select('*');
           $this->db->where('product_id',$datas->product_id);
           $this->db->where('reseller_id', $id);

           $query = $this->db->get('reseller_price');
           $result = $query->row();

     if($result)
        {

              $this->db->where('reseller_id', $id);
              $this->db->set('price',$datas->price);
             $result=$this->db->update('reseller_price',$datas);
              //$status = "success";
			 //$msg = "success";
         }
         else{ 
  
      
            $result=$this->db->insert('reseller_price',$datas);

	  
			 	}

			$status = "success";
			$msg = "success";
        
}
       
        
        
    
    echo json_encode(array("status" => $status, "message" => $msg, "data" => $result));




     }




     //get_product_search


	public function get_product_search(){

      
        $data = json_decode(file_get_contents("php://input"));
		$status = "success";
		$msg = "Categories search items";
		$result = false;
		$id=$data->user_id;
		//print_r($data);
		$keyword = $data->item;


		if(trim($keyword)){
			 $this->db->select("product.id as pid,product.product_name,product.price,product.status,product_gallery.product_id,product_gallery.product_image,product_gallery.format,product_quantity.quantity");
        $this->db->from("product");
       
        $this->db->join("product_gallery",'product_gallery.product_id= product.id');
		
        $this->db->join("product_quantity",'product_quantity.product_id=product.id');

        $this->db->like("product.product_name",$keyword);
        $this->db->group_by('product.id');
        $this->db->where('product.status',1);
        $query = $this->db->get();
        $result=$query->result();

        $new_result = array();

        if($result){

        	foreach ($result as $rs) {
        		$rs->share_image = base_url('admin/'.$rs->product_image.$rs->format);
				$rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
				
        		$new_result[] = $rs; 
			}

             $search['reseller_id'] = $id;
			 $search['category_id'] = $keyword;
			     //$notifi['message']=$msg;
			     $date = date('Y-m-d H:i:s');
			     $search['date']=$date;
				 $this->db->insert('recent_search',$search);

        
         }

    
        
}



 echo json_encode(array("status" => $status, "message" => $msg, "data" => $new_result));

	}




// Categories - Autocomplete
	public function search_category() {
		$data = json_decode(file_get_contents("php://input"));
		$status = "error";
		$msg = "No category found";
		$result = false;
		
		$keyword = $data->item;
		
		if(trim($keyword)) {
		  $this->db->select("id as id, product_name as name");
		  $this->db->like("product_name", $keyword);
		  //$this->db->where("parent_category = 0");
		  $result = $this->db->get("product")->result();
		  if($result) {
			  $status = "success";
		  }
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result));
	}

	//remove remove_search

	public function remove_search() {
		$data = json_decode(file_get_contents("php://input"));
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		if($data->id) {
			
			$this->db->where("recent_search.id", $data->id);
			$this->db->delete("recent_search");
			
			$result['id'] = 0;
			$result['status'] = 0;
			
			$status = "success";
			$msg = "successfully deleted";
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result));
		
	}



//get resent search result

public function get_recent_search1(){

      
     $data = json_decode(file_get_contents("php://input"));
		$status = "success";
		$msg = "Categories search items";
		$result = false;
		$id=$data->customer_id;
		$item=$data->cat;
	   $this->db->select("product.id as pid,product.product_name,product.price,product.status,product_gallery.product_id,product_gallery.product_image,product_gallery.format,product_quantity.quantity");
       $this->db->from("product");

        $this->db->join("product_gallery",'product_gallery.product_id= product.id');
		$this->db->join("product_quantity",'product_quantity.product_id=product.id');

        $this->db->where("product.product_name",$item);
        $this->db->group_by('product.id');
        $this->db->where('product.status',1);
        $query = $this->db->get();
        
        $result=$query->result();

        $new_result = array();

        foreach ($result as $rs) {
        	$rs->share_image = base_url('admin/'.$rs->product_image.$rs->format);
			$rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
			
    		$new_result[] = $rs; 
		}



        
        


 echo json_encode(array("status" => $status, "message" => $msg, "data" => $new_result));

	}




	////get profile 


public function get_profile() {
		$data = json_decode(file_get_contents("php://input"));

		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		
		if(!empty($data->id)) {
			
		$this->db->select('*');
		$this->db->from('reseller');
	    $this->db->where('id',$data->id);
         		
        $result = $this->db->get()->row();
		

		$photo=$this->Zyvawebservice_model->get_profilepic($data->id);

		$result1=$this->Zyvawebservice_model->get_detail2($data->id);
			$status = "success";
			$msg = "profile";
		}
		
	
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'photo'=>$photo,'data1'=>$result1));
		
	}


public function edit_detail(){

       $data = json_decode(file_get_contents("php://input"));
		//print_r($data->profiles);exit;
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		 
				 $id=$data->user_id;
				 //print_r($data->data1->name);exit;

				   if($data) {
                $datas = new stdClass();
                $datas->first_name = $data->profiles->first_name;
                $datas->telephone= $data->profiles->telephone;
                $datas->birthday = $data->profile->birthday;
				$datas->gender = $data->profile->gender;
				 $datas->location =$data->profile->location;
				
				$this->db->where('id',$id);


				$result = $this->db->update("reseller", $datas);
				if($result) {
					$status = "success";
					$msg = "Profile  updated successfully";
				}
			 }	
			  
				 
				 echo json_encode(array('status' => $status, 'message' => $msg));
		

	}


//get color

	public function get_color_list(){

       $result['colo'] = $this->Zyvawebservice_model->get_filter_color();
       //print_r($result['colo']);exit;
      if($result)
			 {
			 	$status = "success";
			$msg = "Homedata";
			//$result=$result['colo'];
			 }else{
			 	$status = "error";
		       $msg = "Unable to fetch data";
		       $result = false;
			 }
			
		 
		
		echo json_encode(array("status" => $status, "message" => $msg, "data" => $result['colo']));
	}



///get filter 


	public function get_filter_list(){
		$data = json_decode(file_get_contents("php://input"));
        $rs = (array) $data;
        $status = "success";
		$msg = "Categories items";
		$result = false;
		if(empty($rs)){
			$this->db->select('product.id as id,product.*,
		                   product_gallery.product_image,
		                   product_gallery.format,
		                    product_gallery.product_id,
							 product_quantity.quantity,
							 product.status
							 ');
	        $this->db->from('product');
	       
	        $this->db->join('product_gallery','product_gallery.product_id= product.id');
	        $this->db->join("product_quantity",'product_quantity.product_id=product.id');

	        $this->db->group_by('product.id');
	        $this->db->where('product.status',1);
	        $this->db->order_by('product.id','DESC');
	        $result = $this->db->get()->result();
		} else {



			$this->db->select('product.id as id,product.*,
		                   product_gallery.product_image,
		                   product_gallery.format,
		                    product_gallery.product_id,
							 product_quantity.quantity,
							 product.status,(product.price-product.dp) AS discount
							 ');
	        $this->db->from('product');	       
	        $this->db->join('product_gallery','product_gallery.product_id= product.id');
	        $this->db->join("product_quantity",'product_quantity.product_id=product.id');
	        $this->db->group_by('product.id');

			if(isset($data->prices)){
				if($data->prices==1){
					$this->db->order_by("product.price","DESC");
				} else if($data->prices==2){
					$this->db->order_by("product.price","ASC");
				} else if($data->prices==3){
					$this->db->select('SUM(order_details.quantity) AS max_pdt');
					$this->db->join("order_details",'product.id = order_details.product_id','left');
					$this->db->order_by("max_pdt","desc");
				} else if($data->prices==4){
					$this->db->order_by("discount","desc");
				} else {
					$this->db->order_by("product.id","desc");
				}				
			}

			if(isset($data->colors) && !empty($data->colors)){
				$this->db->where_in('product_quantity.color_id',$data->colors);
			}

			if(isset($data->ranges)){
				list($start,$end) = explode('-', $data->ranges);
				$this->db->where('product.price >=', $start);
				$this->db->where('product.price <=', $end);
			}
			if(isset($data->discounts)){
				$this->db->select('((product.price-product.dp)/product.price)*100 AS new_discount');
				if($data->discounts==0){
					$this->db->having('new_discount >', '50',false);
				} else {
					$this->db->having('new_discount <=', $data->discounts,false);					
				}
				$this->db->order_by("new_discount","desc");
			}
			$this->db->where('product.status',1);
			$result = $this->db->get()->result();
			//echo $this->db->last_query();
			
		}

		$new_result = array();

        foreach ($result as $rs) {
        	$rs->share_image = base_url('admin/'.$rs->product_image.$rs->format);
			$rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
			
    		$new_result[] = $rs; 
		}
		echo json_encode(array("status" => $status, "message" => $msg, "data" => $new_result));
	}

	 
	

//change password
	public function changepass()
 {

 	   $data = json_decode(file_get_contents("php://input"));
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;

	 
		  $id=$data->user_id;
		  
                    $psw= md5($data->data1->cpassword);
					$npsw= md5($data->data1->npassword);
					 $cpsw= md5($data->data1->cnpassword);
		         
					 $datas=array('password'=>$npsw);
					 
					 $this->db->select('*');
		             $this->db->where('id', $id);
					 $query = $this->db->get('reseller');
					 $results = $query->row();
					 
		 if($results)
				{
					 if ($psw == $results->password) 
					{

							if($npsw == $cpsw) {
							$this->db->where('id', $id);
							if($this->db->update('reseller',$datas))
							{
							 
							  $msg= "success";
							  $status="success";
							} 
							else 
							{ 
								 $msg= "error";
								 $status="error";
							}
						    } 
							else 
							{ 
								$msg= 'change password missmatch';
								$status="error";
							}
				    } 
					else 
					{ 
					   $msg= 'password mismatch'; 
					   $status="error";
					}
				} 

				echo json_encode(array('status' => $status, 'message' => $msg));

		 
		 
   }




         //get address
	 
	public function get_address() {
		$data = json_decode(file_get_contents("php://input"));

		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		if($data->id) {
			
		$this->db->select('*');
		$this->db->from('add_address1');
	    $this->db->where('reseller_id',$data->id);
        $result = $this->db->get()->row();
		//print_r($result);exit;

			
			$status = "success";
			$msg = "address";
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result));
		
	}

///remove address

	
	 public function remove_address() {
		$data = json_decode(file_get_contents("php://input"));
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		if($data->id) {
			
			$this->db->where("id", $data->id);
			$this->db->delete("add_address1");
			
			$result['id'] = 0;
			$result['status'] = 0;
			
			$status = "success";
			$msg = "address removed successfully";
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result));
		
	}





///get address


	public function get_address1(){
     $data = json_decode(file_get_contents("php://input"));
		//print_r($data);exit;
		$id=$data->customer_id;
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;

		$this->db->select('*');
		$this->db->from('add_address1');
		$this->db->where('reseller_id',$id);
		$result= $this->db->get()->row();
		if($result){

        $status = "success";
	    $msg = "fetch address succesfully";

		}





		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result));

		


	}


	 public function edit_address(){

       $data = json_decode(file_get_contents("php://input"));
		//print_r($data);exit;
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		 
				 $id=$data->user_id;
				 //print_r($data->data1->name);exit;

				   if($data) {
                $datas = new stdClass();

				 //$datas->customer_id =$data->user_id;
				$datas->name = $data->data1->name;
				$datas->mobile = $data->data1->mobile;
				$datas->pincode = $data->data1->pincode;
				$datas->address = $data->data1->addres;
				$datas->town = $data->data1->town;
				$datas->city = $data->data1->city;
				$this->db->where('reseller_id',$id);


				$result = $this->db->update("add_address1", $datas);
				if($result) {
					$status = "success";
					$msg = "successfully updated your address ";
				}
			 }	
			  
				 
				 echo json_encode(array('status' => $status, 'message' => $msg));
		

	}



	////// Update Profile Picture
	public function update_profile_picture() {
		$data = $_POST;

	    $id = $data['id'];
        $status = "error";
		$msg = "Error occured. Please try again";
		$result = false;
		
		if(isset($_FILES['file'])){

			$upload_conf = set_upload_options();
			$this->upload->initialize( $upload_conf );
			
			if (!$this->upload->do_upload('file')){
				$error = 0;
				$msg = $this->upload->display_errors();
			}
			else {
				$upload_data = $this->upload->data();
				$data_new['image']=base_url().$upload_conf['upload_path'].'/'.$upload_data['file_name'];
				
				$this->db->where("id", $id);
				$this->db->set('image',$data_new['image']);
				$result = $this->db->update("reseller");
				if($result) {
				 	$status = "success";
					$msg = "Profile picture updated";

                }
					
				

			}

			echo json_encode(array("status" => $status, "message" => $msg));
		}
	}


	function add_measurement(){
		$data = json_decode(file_get_contents("php://input"));
        $status = "Success";
		$msg = "Measurement updated successfully.";
		$result = false;

		$datas = new stdClass();

		//$datas->customer_id = $res->cid;
		$datas->bustsize = $data->data1->bustsize;
		$datas->waistsize = $data->data1->waistsize;
		$datas->hipsize = $data->data1->hipsize;
		$datas->pantwaist = $data->data1->pantwaist;
		$datas->hip = $data->data1->hip;
		$datas->inseam = $data->data1->inseam;

		
        $this->db->insert('new_measurement',$datas);

        $measure_id = $this->db->insert_id();
        $this->db->where('id',$data->insert_id)->update('stitching',array('measure_id'=>$measure_id));

        echo json_encode(array("status" => $status, "message" => $msg));
		

	}



  //add measurement
	function add_measurement_bkp()
   {
      
        $data = json_decode(file_get_contents("php://input"));
        //print_r($data);die;
        $status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		$id=$data->user_id;

		$this->db->select('customer.id as cid,customer.telephone,add_address1.id,add_address1.mobile,add_address1.customer_id');
		$this->db->from('customer');
		$this->db->join('add_address1','customer.telephone=add_address1.mobile');
        $qry=$this->db->get();
        $res=$qry->row();

        if(!empty($res)){



       if($data) {
                $datas = new stdClass();

				$datas->customer_id =$res->cid;
				$datas->bustsize = $data->data1->bustsize;
				$datas->waistsize = $data->data1->waistsize;
				$datas->hipsize = $data->data1->hipsize;
				$datas->pantwaist = $data->data1->pantwaist;
				$datas->hip = $data->data1->hip;
				$datas->inseam = $data->data1->inseam;

			
           $this->db->select('*');
           $this->db->where('customer_id', $res->cid);
           $query = $this->db->get('measurement');
           $result = $query->row();
           $id1=$result->id;

     if($id1)
        {

              $this->db->where('customer_id', $res->cid);
             $result=$this->db->update('measurement',$datas);

             $this->db->set('measure_id',$id1);
             $this->db->where('id',$data->insert_id);
             $rr=$this->db->update('stitching');

              
         }
         else{ 
  
      
            $result=$this->db->insert('measurement',$datas);
            $insert_id = $this->db->insert_id();

            
            $this->db->set('measure_id',$insert_id);
            $this->db->where('id',$data->insert_id);
             $rr=$this->db->update('stitching');
            
			
			}

			$status = "success";
			$msg = "successfully saved";
        
}

}
else{


       $status = "success";
	   $msg="enter your delevery address";
}
       
        
        
    
    echo json_encode(array("status" => $status, "message" => $msg, "data" => $result));




     }





//get_measurement


public function get_measurement() {

	$data = json_decode(file_get_contents("php://input"));
      
		$result = false;
		
		$id=$data->user_id;

		  $this->db->select('*');

          $this->db->from('customer');
          $this->db->where('reseller_id',$id);
          $qry=$this->db->get();
          $res=$qry->row();
          
             if(!empty($res)){

           $this->db->select('*');
           $this->db->where('customer_id', $res->id);
           $query = $this->db->get('measurement');
           $result = $query->row();
           

    
      if($result)
			 {
			 	$status = "success";
			$msg = "Homedata";
			//$result=$result['categories'];
			 }else{
			 	$status = "error";
		       $msg = "Unable to fetch data";
		       $result = false;
			 }
			
			
		 }
		
		echo json_encode(array("status" => $status, "message" => $msg, "data" => $result));
	}




//getstitching
     
	public function get_stitching() {

    $result['front'] = $this->Zyvawebservice_model->front_neck();

         
   $result['back'] = $this->Zyvawebservice_model->back_neck();
   $result['sleeve'] = $this->Zyvawebservice_model->sleeve_neck();
    $result['hemline'] = $this->Zyvawebservice_model->hemline_neck();
    $result['addons1'] = $this->Zyvawebservice_model->addons1();
    $result['addons2'] = $this->Zyvawebservice_model->addons2();
   $result['addons3'] = $this->Zyvawebservice_model->addons3();
    $result['addons4'] = $this->Zyvawebservice_model->addons4();
    $result['addons5'] = $this->Zyvawebservice_model->addons5();
      if($result)
			 {
			 	$status = "success";
			$msg = "Homedata";
			
			 }else{
			 	$status = "error";
		       $msg = "Unable to fetch data";
		       $result = false;
			 }
			
		 
		
		echo json_encode(array("status" => $status, "message" => $msg, "data" => $result));
	}



	////addto stitch

	public function addto_stitch() {
		$data = json_decode(file_get_contents("php://input"));
		//print_r($data);exit;
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		
		if($data) {
			
			$stitch['front_id'] = $data->front_id;
			$stitch['back_id'] = $data->back_id;

			$stitch['sleeve_id'] = $data->sleeve;
			$stitch['neck_id']=$data->hemline_id;
			//$wishlist['color_id'] = $data->color;
			$stitch['add_ons'] = $data->topline_id;
			$stitch['add_ons1'] = $data->closing_id;
			$stitch['add_ons2'] = $data->placket_id;
			$stitch['add_ons3'] = $data->other;
			$stitch['cart_id'] = $data->insert_id;
            $stitch['specialcase'] = $data->special->ramu;
			$result = $this->db->insert('stitching', $stitch);
			 
             
             $insert_id = $this->db->insert_id();

             $this->db->where('id',$data->insert_id)->update('cart',array('stitch_id'=>$insert_id,'stitching_on'=>'1'));
             
			if($result) {
				
				$status = "success";
				$msg = "Product added to stitching";
			}
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'insert_id'=>$insert_id));
		
	}





     ///get notification


	public function get_notification() {
		$data = json_decode(file_get_contents("php://input"));

		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		if($data->user_id) {
		
			$this->db->select('notification.id as nid,notification.*,
		                   notification.product_id,
		                    product.product_name,
							
							
					
							product_gallery.product_image,
							product_gallery.format,
		                  
							');
							
							
		$this->db->from('notification');
		
	  
	   $this->db->join('product', 'product.id = notification.product_id','left');
		 $this->db->join('product_gallery', 'product_gallery.product_id = notification.product_id','left');
		
	    $this->db->where('reseller_id',$data->user_id);
        //$this->db->group_by("notification.product_id"); 		
       // $this->db->limit(5);
        $this->db->order_by("id", "desc");
			$result = $this->db->get()->result();
			
			$status = "success";
			$msg = "notification";
		}

		$new_result = array();

        foreach ($result as $rs) {
        	$rs->share_image = base_url('admin/'.$rs->product_image.$rs->format);
			$rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
			
    		$new_result[] = $rs; 
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $new_result));
		
	}


   //remove wishlist

	public function remove_notification() {
		$data = json_decode(file_get_contents("php://input"));
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		if($data->id) {

			$rs = $this->db->where('id',$data->id)->delete('notification');
			
			/*$this->db->where("id", $data->id);
			$rs = $this->db->delete("notification");*/
			if($rs){
				$status = "success";
				$msg = "Notification is removed";
			}
			//echo $this->db->last_query();
			
			//$result['id'] = 0;
			//$result['status'] = 0;
			
			
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result));
		
	}



	//remove all notification


	 public function allremove_notification() {
		$data = json_decode(file_get_contents("php://input"));
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;
		
		if($data->id) {
			
			$this->db->where("notification.reseller_id", $data->id);
			$this->db->delete("notification");
			
			$result['id'] = 0;
			$result['status'] = 0;
			
			$status = "success";
			$msg = "Notifications removed  ";
		}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result));
		
	}




   //get order summary


	public function get_orders() {
	$data = json_decode(file_get_contents("php://input"));

		
      if($data->book_id) {
	
     $this->db->select("booking.id as id,booking.*,COUNT(order_details.id) AS total_item,
		                   CONCAT(address_book.firstname,' ',address_book.lastname) AS name,address_book.address_1,address_book.telephone AS mobile,address_book.address_2
					");
							
							
		$this->db->from('booking');
		$this->db->join('address_book', 'address_book.id = booking.address_id','left');
		
	  	$this->db->join('order_details', 'order_details.booking_id = booking.id','left');
	     //$this->db->join('booking','booking.id=billing_address.booking_id');
		
	    $this->db->where('booking.id',$data->book_id);
	    $this->db->group_by('booking.id');	
        //$this->db->group_by("cart.product_id"); 		
		$query = $this->db->get();
		
		$result = $query->row();
				
	       $status = "success";
			$msg = "order";

           

}	
	 
	echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result));
		
		
}	


//get_earnings
public function get_earnings(){

$data = json_decode(file_get_contents("php://input"));

  if(empty($data->month)){
  $first_day_this_month = date('Y-m-01'); 
  $last_day_this_month  = date('Y-m-t');
} else {
	$m=$data->month;
	if($m<10){
		$m = "0".$m;
	}
	$date  = date("Y-$m-d");
    $first_day_this_month = date("Y-$m-01");
    $last_day_this_month  = date('Y-m-t',strtotime($date));
    $first_day_this_month.'-'.$last_day_this_month;

}








   $this->db->select('booking.id as id,booking.*,order_details.id,product.id,product.mrp,product.dp,
		                  order_details.product_id,order_details.booking_id');
        $this->db->from('booking');
        $this->db->join('order_details','order_details.booking_id= booking.id');

        $this->db->join('product','order_details.product_id= product.id');
        //$this->db->where("booking.booked_date",$date1);
        $this->db->where('booking.booked_date >=', $first_day_this_month);
        $this->db->where('booking.booked_date <=', $last_day_this_month);
        $this->db->where('booking.reseller_id',$data->userid);
        $this->db->group_by('booking.id');
        $query = $this->db->get();
        //print_r($query);
         
       $this->db->last_query();
       
        $result = $query->result();
        
         //print_r($result);
         $result1 = array(); 
       	
      
		foreach($result as $product) {

	       $product->booked_date = date('Y-m-d',strtotime($product->booked_date));
	       $result1[] = $product;
         }

         $unique_types = array_unique(array_map(function($elem){return $elem->booked_date;}, $result1));
         
         $return_array = array();
         $total_earning = 0;
         $scount=0;
         foreach ($unique_types as $key => $value) {
         	$new_result = array('date'=>$value,
         						'earning'=>0,
         						'sale_count'=>0);
         	foreach ($result1 as $rs) {
         		if($rs->booked_date==$value){
         			$new_result['sale_count'] = $new_result['sale_count'] + 1;
         			//$scount+=$new_result['sale_count'];
         			//print_r($scount);
         			//$earning = $rs->mrp - $rs->dp;
         			$earning = $rs->total_rate - $rs->dp;
         			$total_earning += $earning;
         			//$scount+=$sale_count;
         			$new_result['earning'] = $new_result['earning'] + $earning;
         		}
         	}
         	$return_array[] = $new_result;

         }

 
            $cur_month=date("F");
			$cur_year=date("Y");


if($result)
			 {
			 	$status = "success";
			$msg = "view data";
			
	
			

			 }

			 else{
			 	$status = "error";
		       $msg = "No earnings of this month";
		       $result = false;
			 }

			
 echo json_encode(array("status" => $status, "message" => $msg, "data" => $return_array,"total_earnings"=>$total_earning,"cur_month"=>$cur_month,"cur_year"=>$cur_year));
 //echo json_encode(array("status" => $status, "message" => $msg, "data" => $return_array,"total_earnings"=>$total_earning));

	}



	


///get checkin

	public function get_checkin1(){

    $data = json_decode(file_get_contents("php://input"));

       
       $this->db->select('*');
       $this->db->from('checkin');
       $this->db->where('zip_code',$data->zip);
       $qry=$this->db->get();
       $res=$qry->row();
       if($res){
          $status = "success";
			$msg = "This location is  available";

       }
       else{

       	   $status = "error";
			$msg = "This location is  not available";

       }

       	echo json_encode(array("status" => $status, "message" => $msg));

	}



	public function get_checkin(){

		$data = json_decode(file_get_contents("php://input"));

		$zip = $data->zip;

		$result = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$zip&sensor=true&key=AIzaSyB1br9lwKFyEpCnS5elLan_90CCsYeak6I"));

		if(!empty($result)){
			$rs = $result->results[0]->formatted_address;
			$list = explode(', ', $rs);
			$country = 'India';
			if(in_array($country, $list)){
				$status = "success";
				$msg = "This location is available";
			} else {
				$status = "error";
				$msg = "This location is not available";
			}
		} else {
			$status = "error";
			$msg = "This location is not available";
		}

       	echo json_encode(array("status" => $status, "message" => $msg));

	}




	public function  update_qty() {
		$data = json_decode(file_get_contents("php://input"));
		//print_r($data);exit;
		
		$status = "error";
		$msg = "Sorry, error occured. Please try again";
		$result = false;

		
		if(isset($data->user_id)) {
            $qty = $data->qty;
            $this->db->select('*');
            $this->db->from('cart');
            $this->db->where('id',$data->pid);
            $res=$this->db->get();
            $val=$res->row();


            $this->db->select('quantity');
            $this->db->from('product_quantity');
            $this->db->where('color_id',$val->color_id);
            $this->db->where('size_id',$val->size_id);
            $this->db->where('product_id',$val->product_id);
            $rr=$this->db->get();
            $val1 = $rr->row();

            if(!empty($val1)){
            //print_r($val1);exit;
          
	            if($val1->quantity >= $qty){          
	            	$this->db->where('id',$data->pid);
	            	$this->db->set('quantity',$qty);
	            	$result = $this->db->update('cart');
	                $status = "success";
					$msg = "Product quantity updated";

	            } else {
	           	 	$qty=$val1->quantity;
	            	$this->db->where('id',$data->pid);
	            	$this->db->set('quantity',$qty);
	            	$result = $this->db->update('cart');
	                $status = "success";
					$msg = "Product quantity updated with limited quantity"; 
	            }                
        	}
    	}
		
		echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'quantity'=>$qty));
	}


public function update_stock()
 {

 	 /*$data = json_decode(file_get_contents("php://input"));
 	 
 	 $cart=$data->cart;
 	 foreach ($cart as $rs) {
 	 	 
 	 	//print_r($rs->product_id);
 	   $color_id=$this->Zyvawebservice_model->get_color_id($rs->image);
       $size_id=$this->Zyvawebservice_model->get_size_id($rs->size_type);

        $this->db->set('quantity', "quantity - $rs->quantity", FALSE);
        $this->db->where('product_id', $rs->product_id);
        $this->db->where('color_id', $color_id);
        $this->db->where('size_id', $size_id);
       $res= $this->db->update('product_quantity');
       

 	 }


if($res){
        $status="success";
        $msg="successfully";

 	 }
 	 else{
 	 	  $status="error";
        $msg="error";

 	 }*/

 	$status="success";
    $msg="successfully";

 	echo json_encode(array('status' => $status, 'message' => $msg));
 

}

public function pdt_list(){
	$data = json_decode(file_get_contents("php://input"));
	$this->db->select('product.id as id,product.*,
		                   product_gallery.product_image,
		                   product_gallery.format,
		                    product_gallery.product_id,
							 product_quantity.quantity,
							 product.status
							 ');
        $this->db->from('product');
       
        $this->db->join('product_gallery','product_gallery.product_id= product.id');
        $this->db->join("product_quantity",'product_quantity.product_id=product.id');

        $this->db->group_by('product.id');
        $this->db->where('product.category_id',$data->cat_id);
        $this->db->order_by('product.id','DESC');
        $query = $this->db->get();
        $result = $query->result();
        
        if($result) {
		 	$status = "success";
			$msg = "Homedata";
		}else{
		 	$status = "error";
	       	$msg = "Unable to fetch data";
	       	$result = false;
		}

		$new_result = array();

        foreach ($result as $rs) {
        	$rs->share_image = base_url('admin/'.$rs->product_image.$rs->format);
			$rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
			
    		$new_result[] = $rs; 
		}

	    
			 

			
	echo json_encode(array("status" => $status, "message" => $msg, "data" => $new_result));
}

public function check_version(){
	$data = json_decode(file_get_contents("php://input"));
	$version = $data->version;
	$rs = $this->db->select('reseller_app')->get('settings')->row();
	//echo $rs->reseller_app;
	if($rs->reseller_app > $version){
		echo json_encode(array('status'=>'error'));
	}  else {
		echo json_encode(array('status'=>'success'));
	}
}




	           
 }		
