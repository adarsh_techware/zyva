<?php
defined('BASEPATH') OR exit('No direct script access allowed');


if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400'); // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    
    exit(0);
}

class Webservice extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Webservice_model');
        $this->load->library('upload');
    }
    
    
    // Signup
    public function signup_data()
    {
        $data = json_decode(file_get_contents("php://input"));
        unset($data->gender);
        
        if ($data) {
            $this->db->where("email", $data->email);
            
            $this->db->or_where("telephone", $data->telephone);
            $count = $this->db->count_all_results("customer");
            
            if ($count == 0) {
                $data->email = $data->email;
                
                $data->password = md5($data->password);
                $data->photo    = base_url("assets/uploads/aaaa.png");
                //list($first_name, $last_name) = explode(' ', $data->name);
                $data->first_name = $data->name;
                $data->last_name = '';
                unset($data->name);
                $query = $this->db->select('id')->where('reseller_code',$data->reseller_code)->get('reseller');
                if($query->num_rows()>0){
                    $rs = $query->row();
                    $data->reseller_id = $rs->id;

                } else {
                    $error_msg = 1;
                    unset($data->reseller_code);
                }





                $result         = $this->db->insert("customer", $data);
                if ($result) {
                    $status = "success";
                    $msg    = "Registered successfully";
                } else {
                    
                    $status = "error";
                    $msg    = "Email or Phone already exist";
                    $result = false;
                }
                
                $result = $data;
            }
        }
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result
        ));
        
    }
    
    
    // Signin
    public function signin()
    {
        $data   = json_decode(file_get_contents("php://input"));
        $status = "error";
        $msg    = "Invalid Login";
        $result = false;
        if (isset($data)) {
            $this->db->select('*');
            $this->db->from('customer');
            // $this->db->where('status', '1');
            $this->db->where('email', $data->username);
            $this->db->where('password', md5($data->password));
            //$this->db->limit(1);
            
            $query = $this->db->get();
            
            
            
            if ($query->num_rows() == 1) {
                $result = $query->row();
                $status = "success";
                $msg    = "Login successfully";
            }
        }
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result
        ));
        
    }
    
    //Ger parent categories
    public function get_categories()
    {
        $data    = json_decode(file_get_contents("php://input"));
        $status  = "success";
        $msg     = "Categories";
        $result1 = array();
        $new_result = array();
        
        if (!empty($data->id)) {
            $result1 = $this->Webservice_model->get_search_item($data->id);
            
        }


        $rs = $this->db->where('category.status',1)->get('category')->result();
        foreach ($rs as $row) {
            $ros = $this->db->select('product_gallery.product_image,product_gallery.format')->where('product.category_id',$row->id)->from('product')->join('product_gallery','product_gallery.product_id= product.id')->order_by('product.id','DESC')->limit(1)->get()->row();
            if(count($ros)>0){
                $row->product_image = base_url('admin/'.$ros->product_image.'_201x302'.$ros->format);
                $new_result[] = array('id'=>$row->id,
                              'category_name'=>$row->category_name,
                              'product_image'=>$row->product_image);
            } 
            
            
        }
        
        
        
        
        
        
        
        
        
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $new_result,
            "data1" => $result1
        ));
    }


    public function get_homepage(){
        $data = json_decode(file_get_contents("php://input"));
        if(!empty($data)){
           $id = $data->id; 
           $bag_count = $this->Webservice_model->shoppingbag_count($id);           
           $notify_count = $this->Webservice_model->notification_count($id);
        } else {
            $bag_count = 0;          
            $notify_count = 0;
        }

        $status = 'error';
        $msg = 'Unable to fetch data';
        
        $category = array();
        $rs = $this->db->where('category.status',1)->get('category')->result();
        foreach ($rs as $row) {
            $ros = $this->db->select('product_gallery.product_image,product_gallery.format')->where('product.category_id',$row->id)->from('product')->join('product_gallery','product_gallery.product_id= product.id')->order_by('product.id','DESC')->limit(1)->get()->row();
            if(count($ros)>0){
                $row->product_image = base_url('admin/'.$ros->product_image.'_201x302'.$ros->format);
                $category[] = array('id'=>$row->id,
                              'category_name'=>$row->category_name,
                              'product_image'=>$row->product_image);
            } 
            $status = 'success';
            $msg = 'Homedata';
        } 

        $result['gallery'] = $this->Webservice_model->get_gallery();
        $result['categories'] = $category;
        $result['bag_count'] = $bag_count;
        $result['notify_count'] = $notify_count;

        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result
        ));



    }
    
    
    
    // Get Home page content
    public function get_homepage_dup()
    {

    	$rs = $this->db->where('status','1')->order_by('order_by','ASC')->get('category')->result();

    	

    	$result = array();

    	if(count($rs)>0){
    		foreach ($rs as $row) {
    			$ros = $this->db->query("SELECT product_gallery.product_image,product_gallery.format FROM product_gallery JOIN product ON product.id = product_gallery.product_id WHERE product.category_id = $row->id ORDER BY product.id,product_gallery.default DESC LIMIT  0,1")->row();

    			if(count($ros)>0){
    				$row->product_image = base_url('admin/'.$ros->product_image.$ros->format);    				
    			} else {
    				$row->product_image = base_url('assets/images/dress_c.png'); 
    			}

    			$result['categories'][] = $row;

    			
    		}
    	}

    	

    	$result['gallery'] = $this->Webservice_model->get_gallery();
        //print_r($result['gallery']);exit;

    	if ($result) {
            $status = "success";
            $msg    = "Homedata";
        } else {
            $status = "error";
            $msg    = "Unable to fetch data";
            $result = false;
        }

      

        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result
        ));
        
       
        
    }
    
    
    
    
    
    
    public function get_count()
    {
        
        
        $data = json_decode(file_get_contents("php://input"));
        
        
        $status = "error";
        $msg    = "Unable to fetch data";
        $result = false;
        // print_r($data);exit;
        $count  = 0;
        $count1 = 0;
        if (!empty($data)) {
            $id = $data->id;
            
            
            
            $count = $this->Webservice_model->shoppingbag_count($id);
            
            $count1 = $this->Webservice_model->notification_count($id);
            
            $status = "success";
            $msg    = "successfully";
            // $result = false;
            
        }
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            'count' => $count,
            "count1" => $count1
        ));
        
        
        
    }
    
    
    
    
    
    
    
    ////get productlist
    
    public function get_product_list()
    {
        
        
        $data = json_decode(file_get_contents("php://input"));
       
        $status  = "success";
        $msg     = "Categories items";
        $result  = false;
        $move_id = false;
        $reseller  = null;
        

        $id   = $data->category_id;
        $qtyy = 0;

        if(!empty($data->userid)){
            $reseller = $data->userid->reseller_id;
        }

        if($reseller){
            $this->db->select('product.product_name,product.product_code,product.description,product.price,product.mrp,product.id as pid,product_gallery.product_image,product_gallery.format,product_quantity.id,product_quantity.product_id,product_quantity.quantity,IFNULL(reseller_price.price,product.price) AS reseller_amount');
            $this->db->from("category");
            $this->db->join("product", 'product.category_id = category.id');
            $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
            $this->db->join("reseller_price", "reseller_price.product_id = product.id AND reseller_id = '$reseller'",'LEFT');
            $this->db->join("product_quantity", 'product_quantity.product_id=product.id');
            $this->db->where("category.id", $id);
            $this->db->where('product.status', 1);
            $this->db->group_by('product.id');
            $query = $this->db->get()->result();
        } else {
            $this->db->select('product.product_name,product.product_code,product.description,product.price,product.mrp,product.id as pid,product_gallery.product_image,product_gallery.format,product_quantity.id,product_quantity.product_id,product_quantity.quantity,product.price AS reseller_amount');
            $this->db->from("category");
            $this->db->join("product", 'product.category_id = category.id');
            $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
            $this->db->join("product_quantity", 'product_quantity.product_id=product.id');
            $this->db->where("category.id", $id);
            $this->db->where('product.status', 1);
            $this->db->group_by('product.id');
            $query = $this->db->get()->result();
        }
        
        
        /*$this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product.mrp,product.id as pid,product_gallery.product_image,product_gallery.format,product_quantity.id,product_quantity.product_id,product_quantity.quantity");
        $this->db->from("category");
        $this->db->join("product", 'product.category_id = category.id');
        $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
        $this->db->join("product_quantity", 'product_quantity.product_id=product.id');
        $this->db->where("category.id", $id);
        $this->db->where('product.status', 1);
        $this->db->group_by('product.id');
        $query = $this->db->get()->result();*/
        
        $result = array();

        foreach ($query as $rs) {
            $rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
            $rs->quantity = $this->Webservice_model->stock_qty_org($rs->pid)->quantity;
             $result[] = $rs; 
                    
          
            
        }

        $category = $this->db->where('id',$id)->get('category')->row();


        /*if (!empty($data->userid)) {
            $price = $this->Webservice_model->view_price($data->userid->id,$query);
             //print_r($price);exit;

            if($price){
                 foreach ($price as $rs1) {

                    $rs->price=$rs1->price;
                    $result[]=$rs;
                    //print_r($rs->price);exit;
                    
                    
                    }


            }
            
        }*/
        
        

         

        
        $move_id = array();
        
        if (!empty($data->userid)) {
            $move = $this->Webservice_model->view_move($data->userid->id);
            foreach ($move as $mov) {
                $move_id[] = $mov->product_id;
                
            }
        }
        
        $new_result = array();
        
        foreach ($result as $rs) {
            if (in_array($rs->pid, $move_id)) {
                $rs->fav = false;
            } else {
                $rs->fav = true;
            }
            $new_result[] = $rs;
            
        }
        
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $new_result,
            "category"=>$category,
            "move" => $move_id
        ));
        
    }
    
    
    ///get product detail
    
    function product_detail()
    {
        $data = json_decode(file_get_contents("php://input"));
                
        $id = $data->product_id;

        $count  = 0;
        $count1 = 0;
        $reseller = null;

        if(!empty($data->user_id)){
           $uid = $data->user_id->reseller_id;
           $this->db->select('*');
           $this->db->where('reseller_id', $uid);
           $this->db->where('product_id', $id);
           $query = $this->db->get('reseller_price');
           $reseller = $query->row();
           $count  = $this->Webservice_model->shoppingbag_count($data->user_id->id);
           $count1 = $this->Webservice_model->notification_count($data->user_id->id);
        }
        
        
        
        //$qty=0;
        
        $result['prod']     = $this->Webservice_model->viewproduct($id);
        $prod_id = $id;

        $cat_id = $result['prod']->id;
        $result['main_image']=$this->Webservice_model->viewmainimage($id);
        $result['color']    = $this->Webservice_model->viewcolor($id);
        $result['size']     = $this->Webservice_model->viewproductsize($id);
        $result['images']   = $this->Webservice_model->viewitem_imgslider($cat_id,$prod_id);
        $result['subimage'] = $this->Webservice_model->viewsubimg($id);
        $qty                = $this->Webservice_model->stock_qty_org($id);
        $result['custom']   = $this->Webservice_model->custom_check($id);
        if($reseller){
            $result['prod']->price = $reseller->price;
        }
        
        if ($result) {
            $status = "success";
            $msg    = "select items";
            //$result=$result['categories'];
        } else {
            $status = "error";
            $msg    = "Unable to fetch data";
            $result = false;
        }
        
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result,
            "qty" => $qty->quantity,
            "count" => $count,
            "count1" => $count1
        ));
    }
    
    
    
    
    ///get_color_size
    
    
    public function get_color_size()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        
        $cid    = $data->cid;
        $pid    = $data->pid;
       
        $count  = 0;
        $count1 = 0;

        $reseller = null;

        if(!empty($data->user_id)){
           $uid = $data->user_id->reseller_id;
           $this->db->select('*');
           $this->db->where('reseller_id', $uid);
           $this->db->where('product_id', $pid);
           $query = $this->db->get('reseller_price');
           $reseller = $query->row();
           $count  = $this->Webservice_model->shoppingbag_count($data->user_id->id);
           $count1 = $this->Webservice_model->notification_count($data->user_id->id);
        }

        
        $result['size'] = $this->db->select('size.id,size.size_type,product_quantity.quantity')->where('product_id', $pid)->where('color_id', $cid)->where('size_type!=', 'custom')->from('product_quantity')->join('size', 'size.id=product_quantity.size_id')->get()->result();
        $result['prod'] = $this->Webservice_model->viewproduct($pid);
        $result['custom'] = $this->Webservice_model->custom_color_check($pid,$cid);
        
        $result['color']    = $this->Webservice_model->viewcolor($pid);
        $result['images']   = $this->Webservice_model->viewitem_imgslider($cid,$pid);
        $result['subimage'] = $this->Webservice_model->viewsubimg($pid);

        if($reseller){
            $result['prod']->price = $reseller->price;
        }
        
        //$count=$this->Webservice_model->shoppingbag_count($uid);
        //$count1=$this->Webservice_model->notification_count($uid);
        
        if ($result) {
            $status = "success";
            $msg    = "select items";
            
        } else {
            $status = "error";
            $msg    = "Unable to fetch data";
            $result = false;
        }
        
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result,
            "count" => $count,
            "count1" => $count1
        ));
        
        
    }
    
    
    //addtowishlist
    public function add_wishlist()
    {
        $data   = json_decode(file_get_contents("php://input"));
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        $count1 = 0;
        
        if (isset($data->user_id->id)) {
            
            $this->db->select('*');
            $this->db->from('wishlist');
            $this->db->where('customer_id', $data->user_id->id);
            $this->db->where('product_id', $data->id);
            $this->db->where('color_id', $data->color_id);
            $this->db->where('size_id', $data->size_id);
            $query1 = $this->db->get()->row();
            if (count($query1) > 0) {
                $status = "success";
                $msg = "Product already exist in wishlist";
                // $q=$query1->quantity+1;
                /*$a = 1;
                $q = $query1->quantity + $a;
                $a = array(
                    'quantity' => $q
                );
                $this->db->where('customer_id', $data->user_id->id);
                $this->db->where('product_id', $data->id);
                $this->db->where('color_id', $data->color_id);
                $this->db->where('size_id', $data->size_id);
                
                
                $result = $this->db->update('wishlist', $a);*/
                
            } else {
                
                
                
                
                $data1  = array(
                    'customer_id' => $data->user_id->id,
                    'product_id' => $data->id,
                    'price' => $data->price,
                    'color_id' => $data->color_id,
                    'size_id' => $data->size_id,
                    'quantity' => 1
                );
                $result = $this->db->insert('wishlist', $data1);
                $status = "success";
                $msg    = "Product added to wishlist";
            }
            if ($result) {
                
                
                $notifi['customer_id'] = $data->user_id->id;
                $notifi['product_id']  = $data->id;
                $notifi['color_id']    = $data->color_id;                
                $notifi['size_id'] = $data->size_id;                
                $notifi['message'] = $msg;
                $date              = date('Y-m-d H:i:s');
                $notifi['date']    = $date;
                $this->db->insert('notification', $notifi);
                
                /*$this->db->select('*');
                $this->db->where('product_id', $data->id);
                $this->db->where('color_id', $data->color_id);
                $this->db->where('size_id', $data->size_id);
                $this->db->where('customer_id', $data->user_id->id);
                $this->db->where('message', $msg);
                $query   = $this->db->get('notification');
                $result1 = $query->row();
                
                if ($result1) {
                    
                    $this->db->where('customer_id', $data->user_id->id);
                    $this->db->update('notification', $notifi);
                    //$status = "success";
                    //$msg = "success";
                } else {*/
                    
                    
                    
                    
                    
                    
                    
                //}
                
                
                
                
            }
            $count1 = $this->Webservice_model->notification_count($data->user_id->id);
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result,
            'count1' => $count1
        ));
        
    }
    
    
    
    
    
    
    
    
    
    
    public function add_tobag()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        
        
        if (isset($data->user_id->id)) {

                // if($data->stitch==true){
                //     $data->size_id;=7;
            
                //    }
                   //else{
                    //$cart['size_id']=$data->size_id;

                   //}

            
            $this->db->select('*');
            $this->db->from('cart');
            $this->db->where('customer_id', $data->user_id->id);
            $this->db->where('product_id', $data->id);
            $this->db->where('color_id', $data->color_id);
            $this->db->where('size_id', $data->size_id);
            $query1 = $this->db->get()->row();
            if (count($query1) > 0 && $data->size_id!=7) {
                // $q=$query1->quantity+1;
                $a = 1;
                $q = $query1->quantity + $a;
                $a = array(
                    'quantity' => $q
                );
                $this->db->where('customer_id', $data->user_id->id);
                $this->db->where('product_id', $data->id);
                $this->db->where('color_id', $data->color_id);
                $this->db->where('size_id', $data->size_id);
                
                
                $result    = $this->db->update('cart', $a);
                // $count1=$this->Webservice_model->notification_count($data->user_id);
                $insert_id = $query1->id;
                $status    = "success";
                $msg       = "Product updated to cart";
                
                
                
                
            }
            
            else {
                $data1     = array(
                    'customer_id' => $data->user_id->id,
                    'reseller_id'=> $data->user_id->reseller_id,
                    'product_id' => $data->id,
                    'price' => $data->price,
                    'color_id' => $data->color_id,
                    'size_id' => $data->size_id,
                    'quantity' => 1,
                    'stitching_on'=>$data->stitch
                );
                $result    = $this->db->insert('cart', $data1);
                $insert_id = $this->db->insert_id();
                
                
            }
            
            
            if ($result) {
                $status = "success";
                $msg    = "Product added to cart";
            } else {
                
                $status = "error";
                $msg    = "Sorry, error occured. Please try again";
                $result = false;
                
            }
            
            
            
            
            $notifi['customer_id'] = $data->user_id->id;
            $notifi['product_id']  = $data->id;
            $notifi['color_id']    = $data->color_id;
            
            $notifi['size_id'] = $data->size_id;
            
            $notifi['message'] = $msg;
            $date              = date('Y-m-d H:i:s');
            $notifi['date']    = $date;
            
            
            $this->db->select('*');
            $this->db->where('product_id', $data->id);
            $this->db->where('color_id', $data->color_id);
            $this->db->where('size_id', $data->size_id);
            $this->db->where('message', $msg);
            $this->db->where('customer_id', $data->user_id->id);
            $query   = $this->db->get('notification');
            $result1 = $query->row();
            
            if ($result1) {
                
                $this->db->where('customer_id', $data->user_id->id);
                $this->db->update('notification', $notifi);
                //$status = "success";
                //$msg = "success";
            } else {
                
                
                $this->db->insert('notification', $notifi);
                
                
                
                
            }
            
            
            $count1 = $this->Webservice_model->notification_count($data->user_id->id);
            
            $count = $this->Webservice_model->shoppingbag_count($data->user_id->id);
            
            
        }
        
        //echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'count1'=>$count1));
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result,
            'insert_id' => $insert_id,
            'count1' => $count1,
            'count' => $count
        ));
        
        
    }
    
    
    
    
    
    
    public function update_qty()
    {
        $data = json_decode(file_get_contents("php://input"));
        //print_r($data);exit;
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        
        if (isset($data->user_id)) {
            
            
            $qty = $data->qty;
            $this->db->select('*');
            $this->db->from('cart');
            $this->db->where('id', $data->pid);
            $res = $this->db->get();
            $val = $res->row();
            
            //print_r($val);
            
            $this->db->select('*');
            $this->db->from('product_quantity');
            $this->db->where('color_id', $val->color_id);
            $this->db->where('size_id', $val->size_id);
            $this->db->where('product_id', $val->product_id);
            $rr   = $this->db->get();
            $val1 = $rr->row();
             if(!empty($val1)){
            
            if ($val1->quantity >= $qty) {
                
                
                $this->db->where('id', $data->pid);
                $this->db->set('quantity', $qty);
                $result = $this->db->update('cart');
                $status = "success";
                $msg    = "Product quantity updated";
                
            } else if($val1->quantity==0) {
                $this->db->where('id', $data->pid);
                $stock_status = '1';
                $this->db->set('stock_status',$stock_status);
                $result = $this->db->update('cart');                
                $status = "success";
                $msg    = "Product out of stock";

            } else {
                
                $qty = $val1->quantity;
                $this->db->where('id', $data->pid);
                $this->db->set('quantity', $qty);
                $stock_status = '2';
                $this->db->set('stock_status',$stock_status);
                $result = $this->db->update('cart');                
                $status = "success";
                $msg    = "Product quantity updated with limited quantity";
            }
            
            
            
        }
        
        /*else{

                $status = "success";
                $msg    = "successfully";
        }*/

    }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
        
    }
    
    
    
    
    
    //Get users wishlist
    public function get_wishlist()
    {
        $data   = json_decode(file_get_contents("php://input"));
        $count  = $this->Webservice_model->shoppingbag_count($data->id);
        $count1 = $this->Webservice_model->notification_count($data->id);
        
        $wishlist = $this->Webservice_model->get_wishlist($data->id);
        
        
        $new_result = array();
        foreach ($wishlist as $rs) {
            
            $image = $this->Webservice_model->get_product_image($rs->product_id);
            
            $rs->product_image = $image;
            $new_result[]      = $rs;
            
            
        }
        
        $result_count = count($new_result);
        
        
        if ($result_count > 0) {
            $status = "success";
            $msg    = "Wishlist";
            
            
        } else {
            $status       = "error";
            $msg          = "Sorry, error occured. Please try again";
            $result_count = 0;
        }
        
        
        
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $new_result,
            'result_count' => $result_count,
            "count" => $count,
            "count1" => $count1
        ));
        
        
        
        
    }
    
    //remove wishlist
    
    public function remove_wishlist()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if ($data->id) {
            
            $this->db->where("wishlist.id", $data->id);
            $this->db->delete("wishlist");
            
            $result['id']     = 0;
            $result['status'] = 0;
            
            $status = "success";
            $msg    = "Product removed from wishlist";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    //addalltocart


    public function add_alltocart(){
        $data = json_decode(file_get_contents("php://input"));        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        $rs = $this->db->where('id',$data->w_id)->get('wishlist')->row();
        $w_id = $data->w_id;
        
        $count1 = $this->Webservice_model->notification_count($data->user_id);
        $count = $this->Webservice_model->shoppingbag_count($data->user_id);

        if(count($rs)>0){


        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('customer_id', $rs->customer_id);
        $this->db->where('product_id', $rs->product_id);
        $this->db->where('color_id', $rs->color_id);
        $this->db->where('size_id', $rs->size_id);
        $query1 = $this->db->get()->row();
        if (count($query1) > 0 && $rs->size_id!=7) {
            // $q=$query1->quantity+1;
            $a = 1;
            $q = $query1->quantity + $a;
            $a = array(
                'quantity' => $q
            );
            $this->db->where('customer_id', $rs->customer_id);
            $this->db->where('product_id', $rs->product_id);
            $this->db->where('color_id', $rs->color_id);
            $this->db->where('size_id', $rs->size_id);
            
            
            $result    = $this->db->update('cart', $a);
            // $count1=$this->Webservice_model->notification_count($data->user_id);
            $insert_id = $query1->id;
            $status    = "success";
            $msg       = "Product updated to cart";
            
            
            
            
        }
            
        else {
            $data1     = array(
                'customer_id' => $rs->customer_id,
                'reseller_id'=> $data->reseller_id,
                'product_id' => $rs->product_id,
                'price' => $rs->price,
                'color_id' => $rs->color_id,
                'size_id' => $rs->size_id,
                'quantity' => 1,
                'stitching_on'=>$rs->size_id==7?1:0
            );
            $result    = $this->db->insert('cart', $data1);
            $insert_id = $this->db->insert_id();
            
            
        }
            
            
        if ($result) {
            $status = "success";
            $msg    = "Product added to cart";
        } else {
            
            $status = "error";
            $msg    = "Sorry, error occured. Please try again";
            $result = false;
            
        }
            
            
            
            
        $notifi['customer_id'] = $rs->customer_id;
        $notifi['product_id']  = $rs->product_id;
        $notifi['color_id']    = $rs->color_id;
        
        $notifi['size_id'] = $rs->size_id;
        
        $notifi['message'] = $msg;
        $date              = date('Y-m-d H:i:s');
        $notifi['date']    = $date;
        
        
        $this->db->select('*');
        $this->db->where('product_id', $rs->product_id);
        $this->db->where('color_id', $rs->color_id);
        $this->db->where('size_id', $rs->size_id);
        $this->db->where('message', $msg);
        $this->db->where('customer_id', $rs->customer_id);
        $query   = $this->db->get('notification');
        $result1 = $query->row();
        
        if ($result1) {
            
            $this->db->where('customer_id', $rs->customer_id);
            $this->db->update('notification', $notifi);
            //$status = "success";
            //$msg = "success";
        } else {
            
            
            $this->db->insert('notification', $notifi);
            
            
            
            
        }
        
         $this->db->where('wishlist.id', $w_id);
         $result = $this->db->delete('wishlist');
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result,
            'insert_id' => $insert_id,
            'count1' => $count1,
            'count' => $count
        )); 
     } else {
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result,
            'count1' => $count1,
            'count' => $count
        ));     
     }
            
            
    }
        
        //echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'count1'=>$count1));
          
    
    
    /*public function add_alltocart()
    {
        
        
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        //print_r($data);exit;
        
        
        $id  = $data->user_id;
        $pid = $data->pid;
        //$res=$this->Webservice_model->cart_update($id);
        
        
        
        $this->db->select('customer_id,product_id,price,size_id,quantity,color_id');
        $this->db->from('wishlist');
        $this->db->where('customer_id', $id);
        //$this->db->where('wishlist.id',$pid);
        $query1 = $this->db->get()->row();
        
        
        $data1 = array(
            'customer_id' => $query1->customer_id,
            
            'product_id' => $query1->product_id,
            'price' => $query1->price,
            
            'size_id' => $query1->size_id,
            'color_id'=>$query1->color_id,
            'quantity' => $query1->quantity
            
        );
        
        
        
        
        
        
        if ($this->db->insert('cart', $data1)) {
            $this->db->where('wishlist.id', $pid);
            $result = $this->db->delete('wishlist');
            
            $status = "success";
            $msg    = "Product removed from wishlist";
        }
        
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
        
    }*/
    
    
    ///getcart details


   public function get_cart(){
        $data = json_decode(file_get_contents("php://input"));
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        $result = $this->Webservice_model->get_cart($data->id);
        $delevery = $this->Webservice_model->delivery_charge();
        $stitch_charge=$this->Webservice_model->stitch_charge();
        $count1      = $this->Webservice_model->notification_count($data->id);
        $delevery_charge = $delevery->charge;

        $stitching_charge = 0;
        $addon_charge = 0;
        $selling_price = 0;
        $subtotal = 0;

        $total_stitching_charge = 0;
        $total_addon_charge = 0;
        $total_selling_price = 0;
        $grand_total = 0;

        //print_r($result);

        if(count($result)>0){
            
            foreach ($result as $rs) {

                if($rs->size_id==7){
                    
                    $stitching_charge = $stitch_charge->charge;
                    $addon_charge = $this->Webservice_model->addon_cart($rs->stitch_id);
                    $rs->size_type = 'C';
                } else {
                    $stitching_charge = 0;
                    $addon_charge = 0;
                }


                $selling_price = ($rs->quantity*$rs->price);
                $addon_charge = ($rs->quantity*$addon_charge);
                $stitching_charge = ($rs->quantity*$stitching_charge);

                $total_paid = $selling_price + $addon_charge + $stitching_charge;

                $rs->price = $total_paid;
                $rs->product_image = $this->Webservice_model->get_product_image($rs->product_id);

                if($rs->stock_status!=1){
                    $total_selling_price += $selling_price;
                    $total_addon_charge += $addon_charge;
                    $total_stitching_charge += $stitching_charge;
                }

                

                $new_result[] = $rs;
                
            }

            $grand_total = $delevery_charge + $total_selling_price + $total_addon_charge + $total_stitching_charge;

            $subtotal = $total_selling_price + $total_addon_charge + $total_stitching_charge;

            $result = array('data'=>$new_result,
                            'delivery_charge'=>$delevery_charge,
                            'selling_price'=>$total_selling_price,
                            'addon_charge' =>$total_addon_charge,
                            'stitching_charge'=>$total_stitching_charge,
                            'grand_total'=>$grand_total,
                            'subtotal'=>$subtotal,
                            'status'=>"success",
                            "message"=>"cart successfully updated",
                            "count1" => $count1
                            );

            print json_encode($result);


        } else {
             $result = array('status'=>"success",
                "data"=>array(),
                "message"=>"Cart is empty",
                'delivery_charge'=>$delevery_charge,
                'selling_price'=>$total_selling_price,
                'addon_charge' =>$total_addon_charge,
                'stitching_charge'=>$total_stitching_charge,
                'grand_total'=>$grand_total,
                'subtotal'=>$subtotal,"count1" => $count1);
            print json_encode($result);
        }

    }
    
    
   /*public function get_cart()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        $addonss=0;
        $discounts = 0;
        $discount = 0;
        $total_rate = 0;
        $subtotal = 0;
        $actualprice = 0;
           $stitch=0;
           $stitchs=0;
            $addonss1=0;
            $total = 0;
        if (!empty($data->id)) {
            
            //$res    = $this->Webservice_model->cart_update($data->id);
            $result = $this->Webservice_model->get_cart($data->id);

            if(count($result)){
                $delivery_charge    = $this->Webservice_model->delivery_charge();

            
            $new_result = array();

           
            foreach ($result as $rs) {
                
                $image = $this->Webservice_model->get_product_image($rs->product_id);
                
                
                $rs->product_image = $image;
                $new_result[]      = $rs;
                
                 
                }
                foreach ($result as $row) {
                
                $stock_qty = $this->Webservice_model->stock_qty1($row->product_id);
                $stockqty  = $row->stock_status;

                if ($row->stock_status != 1) {
                    if($rs->size_type=='Custom'){

                        $this->db->select('cart.id as cid,size.size_type');
                        $this->db->from('cart');
                        $this->db->join('size','size.id=cart.size_id');
                        $this->db->where('size_type','Custom');
                        $this->db->where('customer_id',$data->id);
                        $rr=$this->db->get()->num_rows();
                        $stitch_charge=$this->Webservice_model->stitch_charge();
                        $stitchs=0;
                        $stitchs=$stitch_charge->charge * $rs->quantity;
                        $stitch=$stitch+$stitchs;
                    //print_r($stitch);

                        $addonss1=$this->Webservice_model->get_addonscharge($rs->id);

                        $addonss=$addonss + $addonss1;
                    //print_r($addonss);exit;
                        $addonss=$addonss * $rs->quantity;
                    //print_r($addonss);exit;
                    

                    }
                    
                    
                    //$stitch=$stitch+$stitch;
                    //$addonss=$addonss+$addonss;
                $price     = $row->pprice;
            
                $discounts = $row->product_offer;
                $qty       = $row->quantity;
                
                $total      = $total + $qty * $price;
                
                $discount    = $discount + $qty * $discounts;
                $total_rate=$total+$addonss+$stitch;
                $subtotal    = $total_rate - $discount;
                
                $actualprice = $subtotal;                    
                    
                    
                }
                
            } 
            } else {

            }
    
            
            
            
            
            $total       = 0;
            $discount    = 0;
            $vat         = 0;
            $subtotal    = 0;
            $actualprice = 0;
            $stockqty    = 0;
            //$stitchs=0;
            //$addonss1=0;
            
            
            
            
            $total       = $total;
            $discount    = $discount;
            $subtotal    = $subtotal;
            
            $actualprice = $actualprice;
            $addonss=$addonss;
            $stitch=$stitch;
            
            $status      = "success";
            $msg         = "cart";
            
            $count1      = $this->Webservice_model->notification_count($data->id);
            $addrs_count = $this->Webservice_model->get_address_count($data->id);
            
             
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $new_result,
            'quantity' => $stockqty,
            'total' => $total,
            'discount' => $discount,
            'subtotal' => $subtotal,
            'actualprice' => $actualprice,
            'count1' => $count1,
            'addrs_count' => $addrs_count,
            'addonss'=>$addonss,
            'stitch'=>$stitch,
            'delivery_charge'=>$delivery_charge
        ));
        
        
    }*/
    
    
    ///update stock
    
    public function update_stock()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        
        $cart = $data->cart;
        foreach ($cart as $rs) {
            
            
            $color_id = $this->Webservice_model->get_color_id($rs->image);
            $size_id  = $this->Webservice_model->get_size_id($rs->size_type);
            
            $this->db->set('quantity', "quantity - $rs->quantity", FALSE);
            $this->db->where('product_id', $rs->product_id);
            $this->db->where('color_id', $color_id);
            $this->db->where('size_id', $size_id);
            $res = $this->db->update('product_quantity');
            
            
        }
        
        
        if ($res) {
            $status = "success";
            $msg    = "successfully";
            
        } else {
            $status = "error";
            $msg    = "error";
            
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg
        ));
        
        
    }
    
    ///addto move
    
    public function add_alltomove()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        //print_r($data->user_id);exit;
        
        
        $id = $data->user_id;
        
        
        
        
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('id', $data->cid);
        $query1 = $this->db->get()->row();
        //print_r( $query1);exit;
        
        $data1  = array(
            'customer_id' => $query1->customer_id,
            
            'product_id' => $query1->product_id,
            'price' => $query1->price,
            
            'size_id' => $query1->size_id,
            'color_id'=>$query1->color_id,
            'quantity'=>$query1->quantity
        );
        
        if ($this->db->insert('wishlist', $data1)) {
            $this->db->where('id', $data->cid);
            $result = $this->db->delete('cart');
            $status = "success";
            $msg    = "Product added to wishlist";
            
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    
    //remove cart
    public function remove_cart()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if ($data->id) {
            
            $this->db->where("cart.id", $data->id);
            $this->db->delete("cart");
            
            $result['id']     = 0;
            $result['status'] = 0;
            
            $status = "success";
            $msg    = "Product removed from cart";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    //change password
    public function changepass()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        
        $id = $data->user_id;
        //print_r($data->data1->cpassword);exit;
        // if(isset($data)) {
        
        $psw  = md5($data->data1->cpassword);
        $npsw = md5($data->data1->npassword);
        $cpsw = md5($data->data1->cnpassword);
        
        $datas = array(
            'password' => $npsw
        );
        
        $this->db->select('*');
        $this->db->where('id', $id);
        $query   = $this->db->get('customer');
        $results = $query->row();
        
        if ($results) {
            if ($psw == $results->password) {
                
                if ($npsw == $cpsw) {
                    $this->db->where('id', $id);
                    if ($this->db->update('customer', $datas)) {
                        
                        $msg    = "password changed successfully";
                        $status = "success";
                    } else {
                        $msg    = "error";
                        $status = "error";
                    }
                } else {
                    $msg    = 'change password missmatch';
                    $status = "error";
                }
            } else {
                $msg    = 'password mismatch';
                $status = "error";
            }
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg
        ));
        
        
        
    }
    
    
    ///add adress
    public function addaddress()
    {
        
        $data = json_decode(file_get_contents("php://input"));

        $name = $data->data1->name;

        list($firstname,$lastname) = explode(' ',"$name ");


        $rs = array('customer_id'=>$data->user_id,
                    'firstname'=>$firstname,
                    'lastname'=>$lastname,
                    'telephone'=>$data->data1->mobile,
                    'address_1'=>$data->data1->addres,
                    'address_2'=>$data->data1->town.', '.$data->data1->city,
                    'Zip'=>$data->data1->pincode,
                    'default_id'=>1);
        $this->db->where('customer_id',$data->user_id)->update('address_book',array('default_id'=>'0'));
        $this->db->insert('address_book',$rs);

        $status = "success";
        $msg    = "successfully saved your address";
        //print_r($data);exit;

       /* print_r($data);
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if ($data) {
            $datas = new stdClass();
            
            $datas->customer_id = $data->user_id;
            $datas->name        = $data->data1->name;
            $datas->mobile      = $data->data1->mobile;
            $datas->pincode     = $data->data1->pincode;
            $datas->address     = $data->data1->addres;
            $datas->town        = $data->data1->town;
            $datas->city        = $data->data1->city;
            
            $this->db->select('*');
            $this->db->where('customer_id', $data->user_id);
            $query  = $this->db->get('add_address1');
            $result = $query->row();
            
            if ($result) {
                
                $this->db->where('customer_id', $data->user_id);
                $result = $this->db->update('add_address1', $datas);
                
            } else {
                
                
                $result = $this->db->insert('add_address1', $datas);
                
            }
            
            
            $status = "success";
            $msg    = "successfully saved your address";
            
            
        }*/
        
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg
        ));
        
        
        
    }
    
    
    //get address
    
    public function get_address()
    {

        $data = json_decode(file_get_contents("php://input"));
        
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;

        $address = $this->db->where('customer_id', $data->id)->where('rem_status','0')->get('address_book');

        $count = $address->num_rows();

        if($count>0){
            $address_list = $address->result();
            $address = $this->db->where('customer_id', $data->id)->order_by('default_id','DESC')->LIMIT(1)->get('address_book')->row();
        } else {
            $address_list = array();
        }

        $status = "success";
        $msg    = "address";

        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $address_list,
            'address'=>$address,
            'count' => $count
        ));


        
       /* if (!empty($data->id)) {
            $count = $this->Webservice_model->shoppingbag_count($data->id);
            
            //print_r($data);exit;
            $this->db->select('*');
            $this->db->from('add_address1');
            $this->db->where('customer_id', $data->id);
            $result = $this->db->get()->row();
            //print_r($result);exit;
            
            
            $status = "success";
            $msg    = "address";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result,
            'count' => $count
        ));*/
        
    }
    
    
    ////get profile 
    
    
    public function get_profile()
    {
        $data    = json_decode(file_get_contents("php://input"));
        $status  = "error";
        $msg     = "Sorry, error occured. Please try again";
        $result  = false;
        $result1 = '';
        $photo   = '';
        if (!empty($data->id)) {
            $this->db->select('*');
            $this->db->from('customer');
            $this->db->where('id', $data->id);
            $result = $this->db->get()->row();
            $photo  = $this->Webservice_model->get_profilepic($data->id);
            
            $result1 = $this->Webservice_model->get_detail2($data->id);
            $status  = "success";
            $msg     = "profile";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result,
            'photo' => $photo,
            'data1' => $result1
        ));
    }

    public function set_default(){
        $post    = json_decode(file_get_contents("php://input"));
        $address_id = $post->data;
        $user_id = $post->user_id;
        $this->db->where('customer_id',$user_id)->update('address_book',array('status'=>1));
        $this->db->where('id',$address_id)->update('address_book',array('status'=>1));
        print json_encode(array('status'=>'success','message'=>'addresss set successfully'));

    }
    
    
    
    ////get_user
    
    
    public function get_user()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if (!empty($data->id)) {
            
            $this->db->select('*');
            $this->db->from('customer');
            $this->db->where('id', $data->id);
            $result = $this->db->get()->row();
            //print_r($result);exit;
            
            
            $status = "success";
            $msg    = "profile";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    
    ///remove address
    
    
    public function remove_address()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if ($data->id) {
            
            $this->db->where("id", $data->id);
            $this->db->update("address_book",array('rem_status'=>'0'));
            
            $result['id']     = 0;
            $result['status'] = 0;
            
            $status = "success";
            $msg    = "address removed successfully";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    //add measurement
    function add_measurement()
    {
        
        $data   = json_decode(file_get_contents("php://input"));
        //print_r($data);die;
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if (!empty($data->user_id->id)) {
            
            if ($data) {
                $datas = new stdClass();
                
                $datas->customer_id = $data->user_id->id;
                $datas->bustsize    = $data->data1->bustsize;
                $datas->waistsize   = $data->data1->waistsize;
                $datas->hipsize     = $data->data1->hipsize;
                $datas->pantwaist   = $data->data1->pantwaist;
                $datas->hip         = $data->data1->hip;
                $datas->inseam      = $data->data1->inseam;
                
                
                $this->db->select('*');
                $this->db->where('customer_id', $data->user_id->id);
                $query   = $this->db->get('measurement');
                $result1 = $query->row();
                
                if ($result1) {
                    
                    $this->db->where('customer_id', $data->user_id->id);
                    $result    = $this->db->update('measurement', $datas);
                    $insert_id = $result1->id;
                    //$status = "success";
                    //$msg = "success";
                } else {
                    
                    
                    $result    = $this->db->insert('measurement', $datas);
                    $insert_id = $this->db->insert_id();
                    
                    
                    
                    
                }
                
                //print_r($insert_id);exit;
                
                
                
                $status = "success";
                $msg    = "successfully saved";
            }
            
        }
        
        
        
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result,
            'insert_id' => $insert_id
        ));
        
        
        
        
    }
    
    
    
    //get_measurement
    
    
    public function get_measurement()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        
        $result = false;
        
        if (!empty($data->user_id->id)) {
            
            $this->db->select('*');
            $this->db->where('customer_id', $data->user_id->id);
            $query  = $this->db->get('measurement');
            $result = $query->row();
        }
        
        if ($result) {
            $status = "success";
            $msg    = "Homedata";
            
        } else {
            $status = "error";
            $msg    = "Unable to fetch data";
            $result = false;
        }
        
        
        
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result
        ));
    }
    
    
    //getstitching
    
    public function get_stitching()
    {
        
        $result['front'] = $this->Webservice_model->front_neck();
        
        
        $result['back']    = $this->Webservice_model->back_neck();
        $result['sleeve']  = $this->Webservice_model->sleeve_neck();
        $result['hemline'] = $this->Webservice_model->hemline_neck();
        $result['addons1'] = $this->Webservice_model->addons1();
        $result['addons2'] = $this->Webservice_model->addons2();
        $result['addons3'] = $this->Webservice_model->addons3();
        $result['addons4'] = $this->Webservice_model->addons4();
        $result['addons5'] = $this->Webservice_model->addons5();
        if ($result) {
            $status = "success";
            $msg    = "Homedata";
            //$result=$result['categories'];
        } else {
            $status = "error";
            $msg    = "Unable to fetch data";
            $result = false;
        }
        
        
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result
        ));
    }
    
    
    ////addto stitch
    
    public function addto_stitch()
    {
        $data = json_decode(file_get_contents("php://input"));
        //print_r($data);exit;
        
        
        
        if (!empty($data)) {
            
            $insert_id = 0;
            
            $this->db->select('*');
            $this->db->from('new_measurement');
            $this->db->where('customer_id', $data->userdata);
            $rr    = $this->db->get();
            $query = $rr->row();
            if ($query) {
                $stitch['measure_id'] = $query->id;
                
            } else {
                $stitch['measure_id'] = 0;
            }
            
            //$insert_id=$query->id;
            
            $stitch['front_id'] = $data->front_id;
            $stitch['back_id']  = $data->back_id;
            
            $stitch['sleeve_id']   = $data->sleeve;
            $stitch['neck_id']     = $data->hemline_id;
            //$wishlist['color_id'] = $data->color;
            $stitch['add_ons']     = $data->topline_id;
            $stitch['add_ons1']    = $data->closing_id;
            $stitch['add_ons2']    = $data->placket;
            $stitch['add_ons3']    = $data->other;

            $stitch['stitch_price'] =  $this->Webservice_model->get_charge($data->topline_id,$data->closing_id,$data->placket,$data->other);


            $stitch['cart_id']     = $data->insert_id;
            $stitch['specialcase'] = $data->special->ramu;
            
            //$stitch['measure_id']=$insert_id;
            $return = $this->db->insert('stitching', $stitch);
            $stitch_id = $this->db->insert_id();

            $this->db->set('stitch_id',$stitch_id);
            $this->db->where('id',$data->insert_id);
            $this->db->update('cart');
        
            $result = $return;
            if ($result) {
                
                $status = "success";
                $msg    = "successfully added";
            } else {
                
                $status = "error";
                $msg    = "Sorry, error occured. Please try again";
                $result = false;
            }
            
            echo json_encode(array(
                'status' => $status,
                'message' => $msg,
                'data' => $result
            ));
            
        }
        
        
    }
    
    
    
    ///get address
    
    
    public function get_address1()
    {
        $data = json_decode(file_get_contents("php://input"));
        //print_r($data);exit;
        $id   = $data->address_id;
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        if (!empty($id)) {
            
            $this->db->select('*');
            $this->db->from('address_book');
            $this->db->where('id', $id);
            $this->db->where('rem_status','0');
            $result = $this->db->get()->row();
            
        }
        if ($result) {
            
            $status = "success";
            $msg    = "fetch address succesfully";
            
        }
        
        
        
        
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
        
        
        
    }
    
    public function edit_address()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        //print_r($data);exit;
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        $id = $data->address_id;
        //print_r($data->data1->name);exit;
        
        if ($data) {
            $datas = new stdClass();

            $name = $data->data1->name;

            list($firstname,$lastname) = explode(' ',"$name ");


        $rs = array(
                    'firstname'=>$firstname,
                    'lastname'=>$lastname,
                    'telephone'=>$data->data1->mobile,
                    'address_1'=>$data->data1->addres,
                    'address_2'=>$data->data1->town.', '.$data->data1->city,
                    'Zip'=>$data->data1->pincode);

            
           
           
            $this->db->where('id', $id);
            
            
            $result = $this->db->update("address_book", $rs);
            if ($result) {
                $status = "success";
                $msg    = "successfully saving address";
            }
        }
        //  $result = $datas;
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg
        ));
        
        
    }
    
    
    
    
    ///edit profile
    
    
    public function edit_detail()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        //print_r($data->profiles);exit;
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        $id = $data->user_id;
        //print_r($data->data1->name);exit;
        
        if ($data) {
            $datas             = new stdClass();
            $error_msg = 0;
            $datas->first_name = $data->profiles->first_name;
            
            $datas->telephone = $data->profiles->telephone;
            
            $datas->birthday = $data->profile->birthday;
            
            $datas->gender   = $data->profile->gender;
            $datas->location = $data->profile->location;
            $datas->reseller_code=$data->profile->reseller_code;
            $query = $this->db->select('id')->where('reseller_code',$datas->reseller_code)->get('reseller');
            if($query->num_rows()>0){
                $rs = $query->row();
                $datas->reseller_id = $rs->id;

            } else {
                $error_msg = 1;
                unset($datas->reseller_code);
            }
            $this->db->where('id', $id);
            
            
            $result = $this->db->update("customer", $datas);
            if ($result) {
                $status = "success";
                if($error_msg==1){
                    $msg    = "profile updated but invalid reseller code";
                } else {
                    $msg    = "profile updated successfully";
                }
                
            }

             

        }
        //  $result = $datas;
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg
        ));
        
        
    }
    
    
    
    
    ///get notification
    
    
    public function get_notification()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if (!empty($data->user_id->id)) {
            //echo "ll";
            //print_r($data);exit;
            $this->db->select('notification.id as id,notification.*,
		                   notification.product_id,
		                    product.product_name
							');
            
            
            $this->db->from('notification');
            
            
            $this->db->join('product', 'product.id = notification.product_id', 'left');
            // $this->db->join('color','color.id=notification.color_id');
            // $this->db->join('size','size.id=notification.size_id');
            $this->db->where('customer_id', $data->user_id->id);
            // $this->db->group_by("notification.product_id");
            // $this->db->group_by("notification.color_id");
            // $this->db->group_by("notification.size_id");
            // $this->db->group_by("notification.message");
            
            //$this->db->limit(5);
            $this->db->order_by("id", "desc");
            $result = $this->db->get()->result();

             $new_result = array();

        foreach ($result as $rs) {
            $row = $this->db->where('product_id',$rs->product_id)->limit(1)->get('product_gallery')->row();
            $rs->product_image = base_url('admin/'.$row->product_image.'_201x302'.$row->format);
            $new_result[] = $rs; 
        }
            
            $status = "success";
            $msg    = "notification";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $new_result
        ));
        
    }
    
    
    //remove wishlist
    
    public function remove_notification()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if ($data->id) {
            
            $this->db->where("notification.id", $data->id);
            $this->db->delete("notification");
            
            $result['id'] = 0;
            //$result['status'] = 0;
            
            $status = "success";
            $msg    = "Notification is removed";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    ///addtoorder
    
    function dummy()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        
        
        print_r($data);
        
        $id = $data->user_id->id;
        
        $date = date(" jS \of F Y");
        
        
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('customer_id', $id);
        //$this->db->where_in('id',$data['id']);
        $query1 = $this->db->get()->result();
        //echo $this->db->last_query();
        //die;
        if (count($query1) > 0) {
            $result = array();
            foreach ($query1 as $product) {
                $result[] = array(
                    'product_id' => $product->product_id,
                    'color_id' => $product->color_id,
                    'price' => $product->price,
                    'size_id' => $product->size_id,
                    'status' => '1',
                    'date' => $date,
                    'customer_id' => $id
                );
            }
            
        }
        
        $this->db->insert_batch('order', $result);
        
        
        
        
        $this->db->where_in('customer_id', $id);
        $result = $this->db->delete('cart');
        if ($result) {
            $status = "success";
            $msg    = "successfully";
            
        } else {
            
            
            $status = "error";
            $msg    = "Sorry, Enter your delevery address";
            $result = false;
            
        }
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
        
        // }
        
        
        
        
        
    }
    
    
    ////

    function addto_order() {
        $data = json_decode(file_get_contents("php://input"));      
        $id = $data->user_id->id;
        $address_id = $data->address_id; 
        $reseller_id =   $data->user_id->reseller_id;     
        $unique_id    = rand(111111111, 999999999);
        
        $booking_code = 'ZYA' . $unique_id;

        $date=date('Y-m-d H:i:s');
        
        $book = array(
            'booking_no' => $booking_code,
            'address_id' => $address_id,
            'total_rate' => $data->price,
            'reseller_id' => $reseller_id,
            'customer_id' => $id,
            'payment_type' => "Cash on delevery",
            'medium'=>'UserApp',
            'booked_date' => $date,
            'status'=>0
        );
        $this->db->insert('booking', $book);

        $booking_id = $this->db->insert_id();
        
        
        $new_result = array();
        $cart = $this->db->where('customer_id', $id)->where('stock_status!=',1)->get('cart')->result();
       // print_r($cart);
        
        foreach ($cart as $rs) {
           // print_r($rs);
            $this->db->set('quantity', "quantity - $rs->quantity", FALSE);
            $this->db->where('product_id', $rs->product_id);
            $this->db->where('color_id', $rs->color_id);
            $this->db->where('size_id', $rs->size_id);
            $this->db->update('product_quantity');

           
            $datas  = array(
                'booking_id' => $booking_id,
                'product_id' => $rs->product_id,
                'color_id' => $rs->color_id,
                'size_id' => $rs->size_id,
                'stitch_id' => $rs->stitch_id,
                'quantity' => $rs->quantity,
                'price' => $rs->price,
                'status'=>1
            );
            $new_result[] = $datas;
        }

        if(!empty($new_result)){
            $this->db->insert_batch('order_details', $new_result);
        }
        
        


        
        $result=  $this->db->where('customer_id', $id)->delete('cart');
        
        //return $booking_code;



        if($result) {
            $status = "success";
            $msg = "successfully";
        }

        //$resller_name = $this->Zyvawebservice_model->get_resller($id);

        $customer = $this->Webservice_model->get_customer($data->address_id);
        

        $message = "You have received a new order from the customer ".$customer->customer_name;
        $mobile = "9526039555";


        //$this->send_otp($message,$mobile);

        $message = "The order has been successfully placed. Order number: ".$booking_code;
        //$mobile = "9526039555";

        $this->send_otp($message,$customer->customer_no);




    
        echo json_encode(array('status' => $status, 'message' => $msg, 'data' => $result,'booking_id'=>$booking_id,'bookig_no'=>$booking_code));                           
         
    }
    
    
    
    
    function addto_order_dup()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        
        //print_r($data);
        
        $id = $data->user_id->id;
        
        
        $this->db->select('*');
        $this->db->from('add_address1');
        $this->db->where('customer_id', $id);
        $query = $this->db->get();
        
        $result1 = $query->row();
        
        
        
        $unique_id    = rand(111111111, 999999999);
        $booking_code = 'ZYA' . $unique_id;
        
        
        
        $date = date(" jS \of F Y");
        
        $book = array(
            'booking_no' => $booking_code,
            'address_id' => $result1->id,
            'total_rate' => $data->price,
            'customer_id' => $id,
            'payment_type' => "cash on delevery",
            'booked_date' => $date
        );
        $this->db->insert('booking', $book);
        $booking_id = $this->db->insert_id();
        
        $bill_address = array(
            'booking_id' => $booking_id,
            'firstname' => $result1->name,
            
            //'lastname' => $data['lastname'],
            
            'address_1' => $result1->address,
            
            'telephone' => $result1->mobile
        );
        $this->db->insert('billing_address', $bill_address);
        
        $cart = $this->db->where('customer_id', $id)->get('cart')->result();
        
        foreach ($cart as $rs) {
            $this->db->set('quantity', 'quantity' + $rs->quantity, FALSE);
            $this->db->where('product_id', $rs->product_id);
            $this->db->where('color_id', $rs->color_id);
            $this->db->where('size_id', $rs->size_id);
            $this->db->update('product_quantity');
            
            $datas        = array(
                'booking_id' => $booking_id,
                'product_id' => $rs->product_id,
                'color_id' => $rs->color_id,
                'size_id' => $rs->size_id,
                'stitch_id' => $rs->stitching_on,
                'quantity' => $rs->quantity
            );
            $new_result[] = $datas;
        }
        
        $this->db->insert_batch('order_details', $new_result);
        
        $result = $this->db->where('customer_id', $id)->delete('cart');
        
        //return $booking_code;
        
        
        
        if ($result) {
            $status = "success";
            $msg    = "successfully";
            
        }
        
        
        
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result,
            'booking_id' => $booking_id
        ));
        
    }



    function send_otp($msg, $mob_no) {
        $sender_id="TWSMSG"; // sender id   
        $pwd="968808";   //your SMS gatewayhub account password 
        $str = trim(str_replace(" ", "%20", $msg));
        // to replace the space in message with  ‘%20’
        $url="http://api.smsgatewayhub.com/smsapi/pushsms.aspx?user=nixon&pwd=".$pwd."&to=91".$mob_no."&sid=".$sender_id."&msg=".$str."&fl=0&gwid=2";
        // create a new cURL resource
        $ch = curl_init();
        
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // grab URL and pass it to the browser
        $result = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);
    }
    
    
    
    
    //}
    
    
    
    
    //get order
    
    
    
    public function get_order()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if (!empty($data->id)) {
            //echo "ll";
            //print_r($data);exit;

            $result = $this->db->query("SELECT order_details.id,order_details.price,product.product_name,product.product_code,booking.booking_no,order_details.product_id,booking.booked_date,order_details.quantity FROM `order_details` LEFT JOIN booking ON booking.id = order_details.booking_id LEFT JOIN product ON product.id = order_details.product_id WHERE booking.customer_id = '$data->id' ORDER BY order_details.`id` DESC")->result();

            

            $new_result = array();

        foreach ($result as $rs) {
            $row = $this->db->where('product_id',$rs->product_id)->limit(1)->get('product_gallery')->row();
            $rs->product_image = base_url('admin/'.$row->product_image.'_201x302'.$row->format);
            $new_result[] = $rs;
        }
            
            $status = "success";
            $msg    = "order";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $new_result
        ));
        
    }
    
    ///get orderdetail
    
    public function get_orderdetail()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        $id     = $data->user_id;
        $oid    = $data->pid;
        $result = $this->Webservice_model->get_order_info($oid);
        $result->product_image = $this->Webservice_model->get_product_image($result->product_id);
        $result->reseller = $this->Webservice_model->get_reseller($result->reseller_id);
        $result->address = $this->Webservice_model->get_address($result->address_id);
        if($result->stitch_id!=0){
            $result->stich =  $this->Webservice_model->get_stitch_info($result->stitch_id);
        }
            //print_r($result);
            $status = "success";
            $msg    = "order";
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    
    
    
    
    
    
    //add_previously
    public function add_previously()
    {
        $data = json_decode(file_get_contents("php://input"));
        //print_r($data);exit;
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        //print_r($data->color);exit;
        
        if (isset($data->user_id->id)) {
            
            $prev['customer_id'] = $data->user_id->id;
            $prev['product_id']  = $data->product_id;
            
            
            $date         = date('Y-m-d H:i:s');
            $prev['date'] = $date;
            $return       = $this->db->insert('previously_viewed', $prev);
            $result       = $return;
            if ($result) {
                //$result['id'] = $this->db->insert_id();
                //$result['status'] = 1;
                $status = "success";
                $msg    = "Product added to previous";
                
                
                
            }
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    
    //get_prevoiusview
    
    
    public function get_prevoiusview()
    {
        
        
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        
        if (!empty($data->id)) {
            
            $this->db->select("previously_viewed.id as id,previously_viewed.product_id,previously_viewed.date,product_gallery.product_id,product.product_name,product.price,product.mrp,product.id as pid,product_gallery.product_image,product_gallery.format");
            $this->db->from("previously_viewed");
            $this->db->join("product", 'product.id = previously_viewed.product_id');
            $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
            
            $this->db->where("previously_viewed.customer_id", $data->id);
            $this->db->limit(8);
            $this->db->order_by('id', "desc");
            $this->db->group_by("previously_viewed.product_id");
            
            $query  = $this->db->get()->result();
            //$final = $query->result();
            //$result = $query->result();

            $new_result = array();

        foreach ($query as $rs) {
            if($data->reseller_id!=''){
                $rows =  $this->db->where('reseller_id',$data->reseller_id)->where('product_id',$rs->product_id)->get('reseller_price')->row();
                    if(count($rows)>0){
                        $rs->price = $rows->price;
                    }
            }
            $rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
            $new_result[] = $rs; 
        }
            $status = "success";
            $msg    = "previously viewed items";
            
            
            
        }
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $new_result
        ));
        
    }
    
    
    //remove_previous
    
    
    public function remove_previous()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if ($data->id) {
            
            $this->db->where("previously_viewed.id", $data->id);
            $this->db->delete("previously_viewed");
            
            $result['id']     = 0;
            $result['status'] = 0;
            
            $status = "success";
            $msg    = "Product removed from previous view";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    //get_product_search
    
    
    public function get_product_search()
    {
        
        
        $data   = json_decode(file_get_contents("php://input"));
        $status = "success";
        $msg    = "Categories search items";
        $result = false;
        
        $keyword = $data->item;
        
        
        // if(!empty($data->item)){
        //      //$item=$data->item;
        if (trim($keyword)) {
            $this->db->select("product.id as pid,product.product_name,product.price,product.mrp,product.status,product_gallery.product_id,product_gallery.product_image,product_gallery.format,product_quantity.quantity");
            $this->db->from("product");
            
            $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
            
            $this->db->join("product_quantity", 'product_quantity.product_id=product.id');
            $this->db->like("product.product_name", $keyword);
            $this->db->group_by("product.id");
            $this->db->where('product.status', 1);
            $query = $this->db->get();
            
            $result = $query->result();
            
            // foreach ($result as $qty) {
            
            //  $qtyy=$qty->quantity;
            
            //               }
            $move_id = array();
            $new_result1 = array();
            
            if ($result) {


                  foreach ($result as $rs) {
                $rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
                $new_result1[] = $rs; 
            }
                
                if (!empty($data->customer_id->id)) {
                    
                    
                    
                    $move = $this->Webservice_model->view_move($data->customer_id->id);
                    foreach ($move as $mov) {
                        $move_id[] = $mov->product_id;
                        
                    }
                    
                }
            }
            
            
            
            $new_result = array();
            
            
            
            
            
            
            foreach ($new_result1 as $rs) {
                if (in_array($rs->pid, $move_id)) {
                    $rs->fav = false;
                } else {
                    $rs->fav = true;
                }
                $new_result[] = $rs;
                
            }
            
            
            
            
        }
        
        
        
        //echo json_encode(array("status" => $status, "message" => $msg, "data" => $result));
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $new_result,
            "move" => $move_id
        ));
        
        
    }
    
    
    ///get search category
    
    // Categories - Autocomplete
    public function search_category()
    {
        $data   = json_decode(file_get_contents("php://input"));
        //print_r($data);exit;
        $status = "error";
        $msg    = "No category found";
        $result = false;
        
        $keyword = $data->item;
        
        if (trim($keyword)) {
            $this->db->select("id as id, CONCAT(product_name,' - ',product_code) AS name");
            $this->db->or_like("product_name", $keyword);
            $this->db->or_like("product_code", $keyword);
            //$this->db->where("parent_category = 0");
            $result = $this->db->get("product")->result();
            if ($result) {



                $status = "success";
            }
        }

        //echo $this->db->last_query();
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
    }
    
    ////


    ///add add_recentsearch
       public function add_recentsearch() {
        $data   = json_decode(file_get_contents("php://input"));
        //print_r($data);exit;
        $status = "error";
        $msg    = "No category found";
        $result = false;
        
        $keyword = $data->item;
        $pid = $data->pid;
        $userid=$data->userid->id;
        if(!empty($data->userid)){
            $search = array('customer_id'=>$data->userid->id,
                  'category_id'=>$pid,
                  'search_name'=>$keyword,
                  'date'=>date('Y-m-d H:i:s'));
            $result= $this->db->insert('recent_search', $search);
         
        }
        if ($result) {
            $status = "success";
            $msg    = "data added";
        }
                
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
    }
    
    
    //get_recent_search
    
    
    public function get_recent_search1()
    {
        
        
        $data   = json_decode(file_get_contents("php://input"));
        $status = "success";
        $msg    = "Categories search items";
        $result = false;
        $id     = $data->customer_id;
        $item   = $data->cat;
        
        
        // $this->db->select("*");
        $this->db->select("product.id as pid,product.product_name,product.price,product.mrp,product.status,product_gallery.product_id,product_gallery.product_image,product_quantity.quantity,product_gallery.format");
        $this->db->from("product");
        
        $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
        $this->db->join("product_quantity", 'product_quantity.product_id=product.id');
        
        $this->db->where("product.product_name", $item);
        $this->db->group_by("product.id");
        $this->db->where('product.status', 1);
        $result = $this->db->get()->result();

        
        //$result = $query->result();
          $new_result1 = array();
        foreach ($result as $rs) {
                $rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
                $new_result1[] = $rs; 
            }
        
        $move_id = array();
        
        if (!empty($data->customer_id)) {
            $move = $this->Webservice_model->view_move($data->customer_id);
            foreach ($move as $mov) {
                $move_id[] = $mov->product_id;
                
            }
        }
        
        $new_result = array();
        
        
        
        
        
        
        foreach ($new_result1 as $rs) {
            if (in_array($rs->pid, $move_id)) {
                $rs->fav = false;
            } else {
                $rs->fav = true;
            }
            $new_result[] = $rs;
            
        }
        
        
        
        
        
        
        
        //echo json_encode(array("status" => $status, "message" => $msg, "data" => $result));
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $new_result,
            "move" => $move_id
        ));
        
        
    }
    
    
    ////// Update Profile Picture
    public function update_profile_picture()
    {
        $this->load->helper('general');
        $data = $_POST;
        
        //print_r($_FILES['file']);
        $id     = $data['id'];
        $status = "error";
        $msg    = "Error occured. Please try again";
        $result = false;
        
        if (isset($_FILES['file'])) {
            upload_image();
            $filename =  time()."_".$_FILES['file']['name'];
             $_FILES['file']['name']  = $filename;
             $uploadPath              = 'assets/uploads/';
             $config['upload_path']   = $uploadPath;
             $config['allowed_types'] = 'gif|jpg|png|jpeg';
             $this->upload->initialize($config);
             $this->load->library('upload', $config);
             $this->upload->initialize($config);
             if($this->upload->do_upload('file')) {
                $fileData   = $this->upload->data();
                $image_name = $this->image_manipulate($filename,$uploadPath.$fileData['file_name']);

                $data_new['image']=base_url().$config['upload_path'].'/'.$image_name;
                
                $this->db->where("id", $id);
                $this->db->set('photo',$data_new['image']);
                $result = $this->db->update("customer");
                if ($result) {
                    $status = "success";
                    $msg    = "Profile picture updated";
                    
                }
                
                echo json_encode(array(
                        "status" => $status,
                        "message" => $msg
                    ));
                
            }
            
            
        }
    }

    function image_manipulate($image,$img_name){

        ini_set("max_execution_time",0);
        
        $dir_dest = 'assets/uploads/';
        $dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $dir_dest);
        
        
        
        
        
            
            
            
            ini_set("max_execution_time",0);
            $dir_dest = 'assets/uploads/';
            $dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $dir_dest);
            list($main_filename,$image) = explode('.', $image);
            $handle = new Upload($img_name);
            if($handle->uploaded) {
               // $handle->Process($dir_dest,$main_filename);
                //if($handle->processed) {
                   
                    $handle->image_resize          = true;
                    $handle->image_y               = 75;
                    $handle->image_x               = 50;
                    $handle->Process($dir_dest,$main_filename);

                   
                    return $main_filename.'.'.$image;
                    
                
            } else {
               return $main_filename.'.'.$image;
            }
  
        


    }

    public function check_version(){
        $data = json_decode(file_get_contents("php://input"));
        $version = $data->version;
        $rs = $this->db->select('user_app')->get('settings')->row();
        //echo $rs->reseller_app;
        if($rs->user_app > $version){
            echo json_encode(array('status'=>'error'));
        }  else {
            echo json_encode(array('status'=>'success'));
        }
    }
    
    
    //remove all notification
    
    
    public function allremove_notification()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if ($data->id) {
            
            $this->db->where("notification.customer_id", $data->id);
            $this->db->delete("notification");
            
            $result['id']     = 0;
            $result['status'] = 0;
            
            $status = "success";
            $msg    = "Notifications removed  ";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    ///remove search
    
    //remove remove_search
    
    public function remove_search()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        
        if ($data->id) {
            
            $this->db->where("recent_search.id", $data->id);
            $this->db->delete("recent_search");
            
            $result['id']     = 0;
            $result['status'] = 0;
            
            $status = "success";
            $msg    = "Data deleted successfully";
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
    }
    
    
    ///get filter 
    
     /* public function get_filter_list1()
    
    
    */

    /////


    public function get_filter_list(){
        $data = json_decode(file_get_contents("php://input"));
        $rs = (array) $data;
        $reseller = null;
        if($data->user_id){
            $reseller = $data->user_id->reseller_id;
        }
        $status = "success";
        $msg = "Categories items";
        $result = false;
        if(empty($rs)){
            $this->db->select('product.id as pid,product.*,
                           product_gallery.product_image,
                           product_gallery.format,
                            product_gallery.product_id,
                             product_quantity.quantity,
                             product.status
                             ');
            
            $this->db->from('product');
           
            $this->db->join('product_gallery','product_gallery.product_id= product.id');
            $this->db->join("product_quantity",'product_quantity.product_id=product.id');
            if($reseller){
                $this->db->select('IFNULL(reseller_price.price,product.price) AS reseller_amount');
                $this->db->join("reseller_price", "reseller_price.product_id = product.id AND reseller_id = '$reseller'",'LEFT');
            } else {
                $this->db->select('product.price AS reseller_amount');
            }
            $this->db->group_by('product.id');
            $this->db->where('product.status',1);
            $this->db->order_by('product.id','DESC');
            $result = $this->db->get()->result();
        } else {



            $this->db->select('product.id as pid,product.*,
                           product_gallery.product_image,
                           product_gallery.format,
                            product_gallery.product_id,
                             product_quantity.quantity,
                             product.status,(product.price-product.dp) AS discount
                             ');
            $this->db->from('product');        
            $this->db->join('product_gallery','product_gallery.product_id= product.id');
            $this->db->join("product_quantity",'product_quantity.product_id=product.id');
            if($reseller){
                $this->db->select('IFNULL(reseller_price.price,product.price) AS reseller_amount');
                $this->db->join("reseller_price", "reseller_price.product_id = product.id AND reseller_id = '$reseller'",'LEFT');
            } else {
                $this->db->select('product.price AS reseller_amount');
            }
            $this->db->group_by('product.id');

            if(isset($data->prices)){
                if($data->prices==1){
                    $this->db->order_by("reseller_amount","DESC");
                } else if($data->prices==2){
                    $this->db->order_by("reseller_amount","ASC");
                } else if($data->prices==3){
                    $this->db->select('SUM(order_details.quantity) AS max_pdt');
                    $this->db->join("order_details",'product.id = order_details.product_id','left');
                    $this->db->order_by("max_pdt","desc");
                } else if($data->prices==4){
                    $this->db->order_by("discount","desc");
                } else {
                    $this->db->order_by("product.id","desc");
                }               
            }

            if(isset($data->colors) && !empty($data->colors)){
                $this->db->where_in('product_quantity.color_id',$data->colors);
            }

            if(isset($data->ranges)){
                list($start,$end) = explode('-', $data->ranges);
                //$this->db->where('reseller_amount >=', $start);
                //$this->db->where('reseller_amount <=', $end);
                $this->db->having("reseller_amount >=$start AND reseller_amount <=$end");
            }
            if(isset($data->discounts)){
                //$this->db->select('((product.mrp-reseller_amount)/product.mrp)*100 AS new_discount');
                if($data->discounts==0){
                    //$this->db->having("reseller_amount >=$start AND reseller_amount <=$end");
                    $this->db->having('((product.mrp-reseller_amount)/product.mrp)*100 >', '50',false);
                } else {
                    $this->db->having('((product.mrp-reseller_amount)/product.mrp)*100 <=', $data->discounts,false);                   
                }
                $this->db->order_by('((product.mrp-reseller_amount)/product.mrp)*100',"desc");
            }
            $this->db->where('product.status',1);
            $result = $this->db->get()->result();
            //echo $this->db->last_query();
            
        }

        $new_result = array();

        foreach ($result as $rs) {
            $rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
            $rs->quantity = $this->Webservice_model->stock_qty_org($rs->pid)->quantity;
            $new_result[] = $rs; 
        }
        echo json_encode(array("status" => $status, "message" => $msg, "data" => $new_result));
    }

    
    
    
    //get color
    
    public function get_color_list()
    {
        
        $result['colo'] = $this->Webservice_model->get_filter_color();
        //print_r($result['colo']);exit;
        if ($result) {
            $status = "success";
            $msg    = "Homedata";
            
        } else {
            $status = "error";
            $msg    = "Unable to fetch data";
            $result = false;
        }
        
        
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result['colo']
        ));
    }
    
    
    
    //get order summary
    
    
    public function get_orders()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        //book_idprint_r((expression))
        
        
        
        if ($data->book_id) {
            
            $this->db->select('booking.id as id,booking.*,COUNT(order_details.id) AS total_item,
		                   address_book.id,CONCAT(address_book.firstname," ",address_book.lastname) AS name,address_book.address_1,address_book.address_2,address_book.telephone AS mobile,address_book.Zip AS pincode
		                    
		                   
					');
            
            
            $this->db->from('booking');
            $this->db->join('address_book', 'address_book.id = booking.address_id', 'left');
            $this->db->join('order_details', 'order_details.booking_id = booking.id', 'left');
            //$this->db->join('booking','booking.id=billing_address.booking_id');
            
            $this->db->where('booking.id', $data->book_id);
            $this->db->group_by('booking.id');
            //$this->db->group_by("cart.product_id"); 		
            $query = $this->db->get();
            //->result();
            
            
            $result = $query->row();
            
            
            
            $status = "success";
            $msg    = "order";
            
            
            
            
            
            
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
        
    }


    public function get_failed_order()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        //book_idprint_r((expression))
        
        
        
        if ($data->book_id) {

            $result = $this->db->query("SELECT booking.booking_no,transaction.message FROM booking LEFT JOIN transaction ON booking.booking_no = transaction.booking_no WHERE booking.id = $data->book_id")->row();

            $result->message = $result->message==null?"Transaction Incompleted":$result->message;
            
            /*$this->db->select('booking.id as id,booking.*,COUNT(order_details.id) AS total_item,
                           address_book.id,CONCAT(address_book.firstname," ",address_book.lastname) AS name,address_book.address_1,address_book.address_2,address_book.telephone AS mobile,address_book.Zip AS pincode
                            
                           
                    ');
            
            
            $this->db->from('booking');
            $this->db->join('address_book', 'address_book.id = booking.address_id', 'left');
            $this->db->join('order_details', 'order_details.booking_id = booking.id', 'left');
            //$this->db->join('booking','booking.id=billing_address.booking_id');
            
            $this->db->where('booking.id', $data->book_id);
            $this->db->group_by('booking.id');
            //$this->db->group_by("cart.product_id");       
            $query = $this->db->get();
            //->result();
            
            
            $result = $query->row();*/
            
            
            
            $status = "success";
            $msg    = "order Failed";
            
            
            
            
            
            
        }
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $result
        ));
        
        
    }
    
    
    
    
    public function get_shoppingbag()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        
        $count = $this->Webservice_model->shoppingbag_count($data);
        
        if ($count) {
            
            
            $status = "success";
            $msg    = "bag count";
            
        }
        
        else {
            
            $status = "error";
            $msg    = "count empty";
        }
        
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $count
        ));
        
        
        
        
    }
    
    public function forget_password()
    {
        $data = json_decode(file_get_contents("php://input"));
        
        $res = $this->Webservice_model->forgetpassword($data);
        if ($res = "EmailSend") {
            
            $status = 'loggedIn';
            $msg    = 'Successfully Sent. Please Check Your Email.';
        } else {
            $status = 'No';
            $msg    = 'Sorry. Please Enter Your Correct Email.';
        }
        
        
        
        echo json_encode(array(
            'status' => $status,
            'message' => $msg,
            'data' => $res
        ));
        
    }
    
    
    
    ///new measurement
    
    function add_measurement1()
    {
        
        $data   = json_decode(file_get_contents("php://input"));
        //print_r($data);die;
        $status = "error";
        $msg    = "Sorry, error occured. Please try again";
        $result = false;
        $insert_id = 0;
        
        if (!empty($data->user_id->id)) {
            
            if ($data) {
                $datas = new stdClass();
                
                $datas->customer_id = $data->user_id->id;
                $datas->bustsize    = $data->data1->bustsize;
                $datas->waistsize   = $data->data1->waistsize;
                $datas->hipsize     = $data->data1->hipsize;
                $datas->pantwaist   = $data->data1->pantwaist;
                $datas->hip         = $data->data1->hip;
                $datas->inseam      = $data->data1->inseam;
                
                $result    = $this->db->insert('new_measurement', $datas);
                $insert_id = $this->db->insert_id();    
                
                
                if ($data->type=="new") {
                    
                    $this->db->where('customer_id', $data->user_id->id);
                    $result    = $this->db->update('measurement', $datas);
                } 
                
                //print_r($insert_id);exit;
                
                
                
                $status = "success";
                $msg    = "successfully saved";
            }
            
        }
        
        
        
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result,
            'insert_id' => $insert_id
        ));
        
        
        
        
    }
    
    
    
    //get_measurement
    
    
    public function get_measurement1()
    {
        
        $data = json_decode(file_get_contents("php://input"));
        
        $result = false;
        
        if (!empty($data->user_id->id)) {
            
            $this->db->select('*');
            $this->db->where('customer_id', $data->user_id->id);
            $query  = $this->db->get('new_measurement');
            $result = $query->row();
        }
        
        if ($result) {
            $status = "success";
            $msg    = "Homedata";
            
        } else {
            $status = "error";
            $msg    = "Unable to fetch data";
            $result = false;
        }
        
        
        
        
        echo json_encode(array(
            "status" => $status,
            "message" => $msg,
            "data" => $result
        ));
    }
    
    



    //facebook login


    /*public function fb_login1() {
        $data = json_decode(file_get_contents("php://input"));
        if($data!=null)
        {   
            $identifier = $data->userdata->id;
            $displayName = $data->userdata->name;
            $pic = $data->userdata->profile_pic;

            print_r($data);exit;
            
            $cndn_arr = array(
                              'social_provider' => 'Facebook',
                              'identifier'  =>  $identifier
                              );
            $usr = $this->db->where($cndn_arr)->from('users')->get()->row();
            if(empty($usr))
            {
                $arr = array(
                              'social_provider' => 'Facebook',
                              'identifier'  =>  $identifier,
                              'name'    =>  $displayName,
                              'profile_picture' => $pic,
                              'status' => 1
                              );
                $this->db->insert('users',$arr);
                $id = $this->db->insert_id();
                $usrname = url_title($displayName, 'underscore', true).$id;
                $this->db->where('id',$id)->update('users',array('username'=>$usrname));
                $usr = $this->db->where('id',$id)->from('users')->get()->row();
            }
            else if($pic != $usr->profile_picture)
            {
                $data = array('profile_picture' => $pic);
                $this->db->where($cndn_arr)->update('users',$data);
                $usr = $this->db->where('id',$id)->from('users')->get()->row();
            }
            echo json_encode(array("status" => "success", "message" => "Login successfull", "data" => $usr));
        }
        else
        {
            echo json_encode(array("status" => "error", "message" => "Oops! Something went wrong", "data" => ''));
        }
    }}*/




    public function fb_login() {
        $data = json_decode(file_get_contents("php://input"));
        if($data!=null)
        {   
            $identifier = $data->userdata->id;
            $displayName = $data->userdata->name;
            $pic = $data->userdata->profile_pic;

            print_r($data);exit;
            
            $cndn_arr = array(
                              'social_provider' => 'Facebook',
                              'identifier'  =>  $identifier
                              );
            $usr = $this->db->where($cndn_arr)->from('customer')->get()->row();
            if(empty($usr))
            {
                $arr = array(
                              'social_provider' => 'Facebook',
                              'identifier'  =>  $identifier,
                              'name'    =>  $displayName,
                              'profile_picture' => $pic,
                              'status' => 1
                              );
                $this->db->insert('customer',$arr);
                $id = $this->db->insert_id();
                $usrname = url_title($displayName, 'underscore', true).$id;
                $this->db->where('id',$id)->update('customer',array('username'=>$usrname));
                $usr = $this->db->where('id',$id)->from('customer')->get()->row();
            }
            else if($pic != $usr->profile_picture)
            {
                $data = array('profile_picture' => $pic);
                $this->db->where($cndn_arr)->update('customer',$data);
                $usr = $this->db->where('id',$id)->from('customer')->get()->row();
            }
            echo json_encode(array("status" => "success", "message" => "Login successfull", "data" => $usr));
        }
        else
        {
            echo json_encode(array("status" => "error", "message" => "Oops! Something went wrong", "data" => ''));
        }
    }
    
    


    
    
}



