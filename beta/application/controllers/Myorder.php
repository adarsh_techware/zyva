<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myorder extends CI_Controller {

 public function __construct() {
 parent::__construct();

$this->load->model('Home_model');
$this->load->model('Checkout_model');
$this->load->model('Women_model');
$this->load->model('Cart_model');
$this->load->model('Account_model');
$this->load->model('Myorder_model');

if(!$this->session->userdata('user_id')) { 
			redirect(base_url());
		}
   }
   function index(){
	   $template['page'] = 'myorder/my_order';
	    $id = $this->uri->segment(3);
	  
	   
	   $template['result']=$this->Home_model->shoppingbag();
	  $template['cartlist']=$this->Home_model->cartlist();
	    $template['data'] = $this->Women_model->userinfo();
	    $template['Category'] = $this->Women_model->category();
     // $template['image'] = $this->Women_model->viewwomen();
     $template['sub_Category'] = $this->Women_model->sub_category_own();
     $template['size'] = $this->Women_model->getsize();
	  $template['psize'] = $this->Women_model->viewproductsize($id);
	    $template['address'] = $this->Checkout_model->view_address();
		$template['shipping'] = $this->Checkout_model->view_shipping();
		$template['shipping1'] = $this->Checkout_model->view_shipping1();
		
		$template['shipping2'] = $this->Checkout_model->view_shipping2();
		$template['order'] = $this->Myorder_model->myorder();
		
		
		
       $this->load->view('template',$template);
   }
   
     function removeorder(){
	  
	  
	   if($_POST)
		{
			$data = $_POST;
			//print_r($data);
			//die;
			
			$res=$this->Myorder_model->myorder_delete($data);
            $res1=$this->Myorder_model->count_cart(); 
			  
			   if($res1 > 1)
			{
	           //redirect(base_url().'Women/');	
				$result = array('status'  => 'success','message'  => 'Sucessfully  Removed from order','count'=>$res1);
            }
			 
			 else{
			 
			
			   
						  if($res=="success")
						{
						 
						   $result = array('status'  => 'success','message'  => 'Sucessfully  Removed from order','count'=>$res1);
						  
							
						}
						 else
						{
						
						   $result = array('status'  => 'error','message'  => 'Eroor');
						}
		
			 }
	
		
		print json_encode( $result );
			  
			  
		}
		  
		  
	     // redirect(base_url().'Myorder/');
   }

   public function remove_order(){
   	$data = $_POST;
   	$rs = $this->Myorder_model->remove_order($data);
   	print json_encode(array('status'=>'success'));
   }
   
  
   
   
   
   
   
   
   
   
}
?>