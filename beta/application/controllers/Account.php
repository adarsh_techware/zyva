<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Account extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Home_model');
        $this->load->model('Account_model');
        $this->load->model('Women_model');
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
    }
    function index() {
        $template['page'] = 'account/my_account';
        $template['data1'] = $this->Account_model->view_addressbook();
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        $template['account'] = $this->Account_model->view();
        $template['address'] = $this->Account_model->view_address();
        $template['data'] = $this->Women_model->userinfo();
        $template['Category'] = $this->Women_model->category();
        $template['statedetails'] = $this->Account_model->get_state();
        $template['address_list'] = $this->Home_model->view_address_list();
        //var_dump($template['statedetails']);
        //die;
        $template['citydetails'] = $this->Account_model->get_city();
        $this->load->view('template', $template);
    }
    function edit_account() {
        $template['account'] = $this->Account_model->view();
        if ($_POST) {
            //if($('#current_pass').val()=='' && ('#new_pass').val()=='' && ('#con_pass').val()=='') {
            // 	alert("Hai");
            // 	$(".errormsg").html('');
            // 	$(".errormsg").html('<p class="alert alert-success">'+obj.message+'</p>');
            // 	setTimeout(function(){$(".errormsg").hide();}, 3000);
            // }
            $data = $_POST;
            $myArray = array();
            parse_str($_POST['value'], $myArray);
            //print_r($myArray);
            $res = $this->Account_model->edit($myArray);
            if ($res == "error") {
                $result = array('status' => 'error', 'message' => 'Error');
            } elseif ($res == "cpasswordmissmatch") {
                $result = array('status' => 'cpasswordmissmatch', 'message' => 'Confirm Password Mismatch.', 'class' => 'error');
            } elseif ($res == "passwordmismatch") {
                $result = array('status' => 'passwordmismatch', 'message' => 'Current Password is Incorrect.', 'class' => 'error');
            } elseif ($res == "required") {
                $result = array('status' => 'Password Required', 'message' => 'New Password is empty.', 'class' => 'error');
            } else {
                $result = array('status' => 'success', 'message' => 'Your Data Updated Successfully .', 'class' => 'success', 'result' => $res);
            }
            print json_encode($result);
        }
    }
    ///////add address book/////
    function add_addressbook() {
        if ($_POST) {
            $data = $_POST;
            $myArray = array();
            parse_str($_POST['value'], $myArray);
            $res = $this->Account_model->addaddress($myArray);
            if ($res == "success") {
                $result = array('status' => 'success', 'message' => 'New Address Added Sucessfully');
            } else {
                $result = array('status' => 'error', 'message' => 'Some errors occured in Address creation');
            }
            print json_encode($result);
            //$this->load->view('template',$template);
            
        }
    }
    //////select state city////////////
    public function loadData($loadId = '', $loadType = '') {
        $loadType = $_POST['loadType'];
        $loadId = $_POST['loadId'];
        $result = $this->Account_model->getData($loadType, $loadId);
        //var_dump($result);
        //die;
        $HTML = "";
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $list) {
                $HTML.= "<option value='" . $list->id . "'>" . $list->name . "</option>";
            }
        }
        echo $HTML;
    }
    function address() {
        if ($_POST) {
            $data = $_POST;
            $myArray = array();
            parse_str($_POST['value'], $myArray);
            unset($myArray['shipping_charge']);
            unset($myArray['enable_gift']);
            $res = $this->Account_model->update_addressbook($myArray);
            //var_dump($res); die;
            if ($res > 0) {
                $result = array('status' => 'success', 'result' => $res);
            } else {
                $result = array('status' => 'error', 'message' => 'Error');
            }
            print json_encode($result);
        }
        // else {
        //  	$result = array('status'  => 'info','message' => 'No Change Done !','result'  => $res);
        //  }
        
    }
    public function change_default() {
        $data = $_POST;
        $rs = $this->Account_model->update_default($data['address_id']);
        if ($rs) {
            echo 'success';
        } else {
            echo 'error';
        }
    }
    public function add_new_default() {
        $data = $_POST;
        $rs = $this->Account_model->add_new_address($data);
        // print_r($rs);die;
        if ($rs) {
            echo 'success';
        } else {
            echo 'error';
        }
    }
}
?>
