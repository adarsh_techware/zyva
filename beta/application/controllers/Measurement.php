<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Measurement extends CI_Controller {

 public function __construct() {
 parent::__construct();

$this->load->model('measurement_model');
$this->load->model('Women_model');
$this->load->model('Home_model');

if(!$this->session->userdata('user_id')) {
			redirect(base_url());
		}
   }
   function index(){

     $template['page'] = 'measurement/my_measure';

      $template['data'] = $this->Women_model->userinfo();
	    $template['Category'] = $this->Women_model->category();
      $template['cartlist']     = $this->Home_model->cartlist();
      $template['measurement'] = $this->measurement_model->view();
      if(!$template['measurement']){
        $template['measurement'] = (object) array('waistsize'=>'','bustsize'=>'','waistsize'=>'','hipsize'=>'','pantwaist'=>'','hip'=>'','inseam'=>'');
      }
      
      $template['result']=$this->Home_model->shoppingbag();
		  $this->load->view('template',$template);

   }



 //   ///////add measurement/////

 function add_measurement()
   {
      $template['measher'] = $this->measurement_model->view();

       if($_POST)
     {
       $data = $_POST;
       $myArray=array();
             parse_str($_POST['value'],$myArray);
        $res=$this->measurement_model->addmesh($myArray);


         if($res=="success_update" || $res=="success_insert")
       {
        if($res=='success_insert') $msg = 'Measurement Added Sucessfully';
        else $msg = 'Measurement Updated Sucessfully';

          $result = array('status'  => 'success','message'  => $msg);
       }
        else
       {

          $result = array('status'  => 'error','message'  => 'Error');
       }

          print json_encode( $result );



     }
   }

///*************Add new measurement*********/


   function add_newmeasurement(){

    $myArray=array();
    parse_str($_POST['value'],$myArray);
    $res=$this->measurement_model->addmeshnew($myArray);
    if($res) {
      $result = array('status'=>'success','message'=>'Measurement Added Sucessfully','stitch_id'=>$res);
    } else {
      $result = array('status'=> 'error','message'=>'Error');
    }

     print json_encode( $result );
    }
   



function add_newmeasurement1(){

    

      $template['measher'] = $this->measurement_model->view();

       if($_POST)
     {
       $data = $_POST;
       $myArray=array();
             parse_str($_POST['value'],$myArray);
       $res=$this->measurement_model->addmeshnew1($myArray);


         if($res=="success")
       {

          $result = array('status'  => 'success','message'  => 'Sucessfully Added to measurement');
       }
        else
       {

          $result = array('status'  => 'error','message'  => 'Error');
       }

     print json_encode( $result );



     }
  }
}
?>
