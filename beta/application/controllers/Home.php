<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Home_model');
        $this->load->model('Women_model');
        $this->load->model('Cart_model');
        $this->load->helper('general_helper');
        $this->load->library('facebook'); 
        $method = $this->router->fetch_method();
        if ($this->session->userdata('user_id') && $method!='index') {
            redirect(base_url());
        }       
    }
    function index() {
        $template['title'] = 'Zyva Fashions Womens Kurtis : Buy Designer Ladies Kurtis Online';
        $template['description'] = 'Online stylish Kurtis Shopping Store for Ladies and Girls. Cash on Delivery, Finest Collection, Best Prices';
        $template['page'] = 'home/index';
        $template['data'] = $this->Women_model->userinfo();
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        $template['Category'] = $this->Women_model->category();
        $template['banner'] = $this->Home_model->getbanners();
        $template['result'] = $this->Home_model->shoppingbag();
        $this->load->view('template', $template);
    }
    /******************NEW SIGNUP STATR **********************/
    public function signup() {
        include_once APPPATH . "libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH . "libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
        $clientId = '39980478287-m7jnkfsidgfbpr2g2jr8mqer2ocs5al9.apps.googleusercontent.com';
        $clientSecret = 'z_vOFQc9Wh8kkCoOIJ81evCc';
        $redirectUrl = base_url('home/google_login');
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to Zyva');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        $template['googleUrl'] = $gClient->createAuthUrl();
        $template['authUrl'] = $this->facebook->login_url();

        $template['page'] = 'home/signup';
        $template['data'] = $this->Women_model->userinfo();
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        $template['Category'] = $this->Women_model->category();
        $template['banner'] = $this->Home_model->getbanners();
        $template['result'] = $this->Home_model->shoppingbag();
        $this->load->view('template', $template);
    }
    /******************SignUp Start**********************/
    public function sign_up() {
        // echo "hi";
        // die;
        $myArray = array();
        parse_str($_POST['value'], $myArray);
        $res = $this->Home_model->sign_up($myArray);
        if ($res == "password") {
            $result = array('status' => 'password', 'message' => 'Password And Confirm Password Do Not Match.');
        } 
        if ($res == "email") {
            $result = array('status' => 'email', 'message' => 'Email Already Exist.');
        } else {
            $rs = $this->Home_model->send_notification($myArray);
            $result = array('status' => 'success', 'result' => 'Sucessfully Registered.. Please Login');
        }
        print json_encode($result);
    }

    /****************** SIGNIN START **********************/
    function signin() {

        $back_url = $_SERVER['HTTP_REFERER'];
        $this->session->set_userdata('back_url',$back_url);

        include_once APPPATH . "libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH . "libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
        $clientId = '39980478287-m7jnkfsidgfbpr2g2jr8mqer2ocs5al9.apps.googleusercontent.com';
        $clientSecret = 'z_vOFQc9Wh8kkCoOIJ81evCc';
        $redirectUrl = base_url('home/google_login');
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to Zyva');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        $template['googleUrl'] = $gClient->createAuthUrl();
        $template['authUrl'] = $this->facebook->login_url();

        $template['page'] = 'home/login';
        $template['data'] = $this->Women_model->userinfo();
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        $template['Category'] = $this->Women_model->category();
        $template['banner'] = $this->Home_model->getbanners();
        $template['result'] = $this->Home_model->shoppingbag();
        $this->load->view('template', $template);
    }
    /******************Login Start**********************/
    public function login() {
        // $myArray = array();
        // parse_str($_POST['value'], $myArray);
        // $res = $this->Home_model->login($myArray);
        // if ($res == "incorrect") {
        //     $result = array('status' => 'incorrect', 'message' => 'Invalid username or password.');
        // } else {
        //     $result = array('status' => 'success', 'result' => $res);
        // }
        // print json_encode($result);


        $myArray = array();
        parse_str($_POST['value'], $myArray);
        $res = $this->Home_model->login($myArray);
        if ($res == "incorrect") {
            $result = array('status' => 'incorrect', 'message' => 'Invalid username or password.');
        } else {
            $result = array('status' => 'success', 'result' => $res);
            $temp_cart_dat = $this->Home_model->get_temp_cart();
            if (count($temp_cart_dat) > 0) {
                foreach ($temp_cart_dat as $data) {
                    $data1 = (array)$data;
                    $results = $this->Cart_model->addtocart1($data1);
                }
                $this->Cart_model->delete_tempcart();
                $this->session->unset_userdata('session_id');
            }
        } 
        print json_encode($result);

    }
    /******************FORGOT PASSWORD START**********************/
    public function forgot_password() {
        include_once APPPATH . "libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH . "libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
        $clientId = '39980478287-m7jnkfsidgfbpr2g2jr8mqer2ocs5al9.apps.googleusercontent.com';
        $clientSecret = 'z_vOFQc9Wh8kkCoOIJ81evCc';
        $redirectUrl = base_url('home/google_login');
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to Zyva');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        $template['googleUrl'] = $gClient->createAuthUrl();
        $template['authUrl'] = $this->facebook->login_url();

        
        $template['page'] = 'home/forgot';
        $template['data'] = $this->Women_model->userinfo();
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        $template['Category'] = $this->Women_model->category();
        $template['banner'] = $this->Home_model->getbanners();
        $template['result'] = $this->Home_model->shoppingbag();
        $this->load->view('template', $template);
    }
    /******************Check login Or Not**********************/
    public function check_logged() {
        $user = $this->session->userdata('user_value');
        if ($user) {
            echo true;
        } else {
            echo false;
        }
    }
    /******************Forgot Password**********************/
    public function Forget_Password() {
        if (isset($_POST)) {
            $data = $_POST;
            $email = $this->input->post('email');
            $res = $this->Home_model->forgetpassword($email);
            if ($res == "EmailNotExist") {
                $result = array('status' => 'No', 'message' => 'Sorry. Please Enter Your Correct Email.');
            } else {
                $result = array('status' => 'loggedIn', 'message' => 'Successfully Sent. Please Check Your Email.');
            }
            print json_encode($result);
        }
    }
    public function test_email() {
        $myArray = (object)array('firstname' => 'asdasdasd', 'lastname' => 'asdasdasd', 'email' => 'adarsh.techware@gmail.com', 'password' => '123456');
        $rs = $this->Home_model->send_notification($myArray);
    }
    public function reset($reset_key = null) {
        if ($reset_key == null) {
            redirect(base_url());
        } else {
            $template['reset_key'] = $reset_key;
            $template['page'] = 'home/reset_pass';
            $template['data'] = $this->Women_model->userinfo();
            $template['result'] = $this->Home_model->shoppingbag();
            $template['cartlist'] = $this->Home_model->cartlist();
            $template['Category'] = $this->Women_model->category();
            $template['banner'] = $this->Home_model->getbanners();
            $template['result'] = $this->Home_model->shoppingbag();
            $this->load->view('template', $template);
        }
    }
    public function reset_password() {
        $data = $_POST;
        $res = $this->Home_model->reset_pass($data);
        if ($res > 0) {
            $result = array('status' => 'success', 'message' => 'Updated Successfully');
        } else {
            $result = array('status' => 'error', 'message' => 'Invalid Key. Please use correct key');
        }
        print json_encode($result);
    }
    public function social_function() {
        include_once APPPATH . "libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH . "libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
        $clientId = '39980478287-m7jnkfsidgfbpr2g2jr8mqer2ocs5al9.apps.googleusercontent.com';
        $clientSecret = 'z_vOFQc9Wh8kkCoOIJ81evCc';
        $redirectUrl = base_url('home/google_login');
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to Zyva');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        $template['googleUrl'] = $gClient->createAuthUrl();
        $template['authUrl'] = $this->facebook->login_url();
        $this->load->view('home/social_login', $template);
    }
    public function google_login() {
        include_once APPPATH . "libraries/google-api-php-client/Google_Client.php";
        include_once APPPATH . "libraries/google-api-php-client/contrib/Google_Oauth2Service.php";
        $setting = getSettings();
        $clientId = '39980478287-m7jnkfsidgfbpr2g2jr8mqer2ocs5al9.apps.googleusercontent.com';
        $clientSecret = 'z_vOFQc9Wh8kkCoOIJ81evCc';
        $redirectUrl = base_url('home/google_login'); //base_url() . 'user_authentication/';
        // Google Client Configuration
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to Zyva');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        if (isset($_REQUEST['code'])) {
            $gClient->authenticate();
            $this->session->set_userdata('token', $gClient->getAccessToken());
            redirect($redirectUrl);
        }
        $token = $this->session->userdata('token');
        if (!empty($token)) {
            $gClient->setAccessToken($token);
        }
        if ($gClient->getAccessToken()) {
            $userProfile = $google_oauthV2->userinfo->get();
            //print_r($userProfile);
            $query = $this->db->where('social_id', $userProfile['id'])->or_where('email',$userProfile['email'])->get('customer');
            if ($query->num_rows() > 0) {
                $res = $query->row();
                $this->session->set_userdata('user_value', $res);
                $this->session->set_userdata('user_id', $res->id);
                $user = $this->session->userdata('user_value');
                $id = $this->session->userdata('user_id');
                $result = array('status' => 'success', 'result' => $res);
            } else {
                list($first_name, $last_name) = explode(' ', $userProfile['name']);
                $email = $userProfile['email'];
                $social_id = $userProfile['id'];
                $status = 1;
                $terms = 'agree';
                $created_date = date('Y-m-d');
                $photo = $userProfile['picture'];
                $data = array('first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'social_id' => $social_id, 'terms' => $terms, 'status' => $status, 'created_date' => $created_date, 'photo' => $photo);
                $this->db->insert('customer', $data);
                $id = $this->db->insert_id();
                $res = $this->db->where('id', $id)->get('customer')->row();
                $this->session->set_userdata('user_value', $res);
                $this->session->set_userdata('user_id', $res->id);
                $user = $this->session->userdata('user_value');
                $id = $this->session->userdata('user_id');
            }
            // redirect(base_url());
            $temp_cart_dat = $this->Home_model->get_temp_cart();
            if (count($temp_cart_dat) > 0) {
                foreach ($temp_cart_dat as $data) {
                    $data = (array)$data;
                    $res = $this->Cart_model->addtocart1($data);
                }
                //print_r($res);die;
                $this->Cart_model->delete_tempcart();
                $this->session->unset_userdata('session_id');
            } 
            
            if($this->session->userdata('back_url')){
                $this->session->unset_userdata('back_url');
                redirect($this->session->userdata('back_url'));
            } else {
                redirect(base_url());
            }
        }
    }
    public function facebook_login() {
        $userData = array();
        if ($this->facebook->is_authenticated()) {
            // Get user facebook profile details
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
            // Preparing data for database insertion
            // $userData['oauth_provider'] = 'facebook';
            $userData['social_id'] = $userProfile['id'];
            $userData['first_name'] = $userProfile['first_name'];
            $userData['last_name'] = $userProfile['last_name'];
            $userData['email'] = $userProfile['email'];
            $userData['gender'] = $userProfile['gender'];
            $userData['location'] = $userProfile['locale'];
            $userdata['status'] = 1;
            $userdata['terms'] = 'agree';
            $userData['created_date'] = date('Y-m-d H:i:s');
            $userData['photo'] = $userProfile['picture']['data']['url'];
            $res = $this->Home_model->fb_login($userData);
            $result = array('status' => 'success', 'result' => $res);
            // redirect(base_url());
            //print json_encode($result);

            $temp_cart_dat = $this->Home_model->get_temp_cart();
            if (count($temp_cart_dat) > 0) {
                foreach ($temp_cart_dat as $data) {
                    $data = (array)$data;
                    $res = $this->Cart_model->addtocart1($data);
                }
                //print_r($res);die;
                $this->Cart_model->delete_tempcart();
                $this->session->unset_userdata('session_id');
            }
            
            if($this->session->userdata('back_url')){
                $this->session->unset_userdata('back_url');
                redirect($this->session->userdata('back_url'));
            }
            //print_r($userData);
            
        }
    }
    public function check_mail() {
        $data = '{"name":"Adarsh","email":"adarsh.techware@gmail.com","message" :"Hi Team"}';
        $data = json_decode($data);
        $this->load->library('email');
        $config = Array('protocol' => 'smtp', 'smtp_host' => 'mail.zyva.in', 'smtp_port' => 587, 'smtp_user' => 'info@zyva.in', // change it to yours
        'smtp_pass' => '&1M5ZI&xLO@y', // change it to yours
        'smtp_timeout' => 20, 'mailtype' => 'html', 'charset' => 'iso-8859-1', 'wordwrap' => TRUE);
        $this->email->initialize($config); // add this line
        $subject = 'New Mail';
        $name = $data->name;
        $mailTemplate = $data->message;
        //$this->email->set_newline("\r\n");
        $this->email->from('info@zyva.in', $name);
        $this->email->to($data->email);
        $this->email->subject($subject);
        $this->email->message($mailTemplate);
        echo $this->email->send();
    }

    public function price_adjust(){
        $res = $this->db->select('id,price')->get('product')->result();
        $result = array();

        foreach ($res as $row) {
            $rs = (object) array();
            $discount = (($row->price*30)/100);
            $rs->price = $row->price - $discount;
            $rs->reseller_id = 23;
            $rs->product_id = $row->id;
            $result[] = $rs;
        }
        $this->db->insert_batch('reseller_price',$result);
    }
}
?>

