<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class About extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Home_model');
        $this->load->model('Women_model');
        $this->load->helper('general_helper');
    }
    function contact() {
        $template['page'] = 'about/contact';
        $template['data'] = $this->Women_model->userinfo();
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        //var_dump($template['cartlist']);
        //die;
        $template['Category'] = $this->Women_model->category();
        $template['banner'] = $this->Home_model->getbanners();
        $template['result'] = $this->Home_model->shoppingbag();
        $this->load->view('template', $template);
    }
    function about() {
        $template['title'] = 'Custom Kurti Designers Kochi | Ladies Kurti Manufacturer';
        $template['description'] = 'Maryam Apparels is the most reliable wholesale women kurti suppliers. For ladies kurti online at affordable prices visit our website. Shop Now!';
        $template['page'] = 'about/about';
        $template['data'] = $this->Women_model->userinfo();
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        //var_dump($template['cartlist']);
        //die;
        $template['Category'] = $this->Women_model->category();
        $template['banner'] = $this->Home_model->getbanners();
        $template['result'] = $this->Home_model->shoppingbag();
        $this->load->view('template', $template);
    }
    public function send_mail() {
    	$template['Category'] = $this->Women_model->category();
        $settings = get_icon();
        $data = $_POST;
        parse_str($data['value'], $myArray);

        $template['name'] 		= $myArray['name'];
        $template['email'] 		= $myArray['email'];
        $template['subject']	= $myArray['subject'];
        $template['msg'] 		= $myArray['msg'];

        $this->load->library('email');
        $config = Array(
		        	'protocol' 	=> 'smtp', 
		        	'smtp_host' => $settings->smtp_host, //'smtp.techlabz.in',
		        	'smtp_port' => 587, 
		        	'smtp_user' => $settings->smtp_username, //'no-reply@techlabz.in', // change it to yours
		        	'smtp_pass' => $settings->smtp_password, //'baAEanx7', // change it to yours
		        	'smtp_timeout' => 20, 
		        	'mailtype' 	=> 'html', 
		        	'charset' 	=> 'iso-8859-1', 
		        	'wordwrap' 	=> TRUE
		        	);
        $this->email->initialize($config); // add this line
        $subject 		= $settings->sms_username.' Contact US';
        $name 			= $settings->sms_username;
        $to 			= $settings->smtp_username;
		$mailTemplate 	= $this->load->view('email_templates/contactus_success',$template, true);

        //$this->email->set_newline("\r\n");
        $this->email->from($settings->admin_email, $name);
        $this->email->to($to);
        // $this->email->to('nikhila.techware@gmail.com');
        $this->email->subject($subject);
        $this->email->message($mailTemplate);
        $var = $this->email->send();
        if ($var == 1) {
            $result = array('status' => 'success', 'message' => 'Mail sent successfully');
        } else {
            $result = array('status' => 'error', 'message' => '');
        }
        print json_encode($result);
    }

    function contactus() {
		$template['Category'] = $this->Women_model->category();
		$template['page'] = 'email_templates/contactus_success';
		$this->load->view('email_templates/contactus_success', $template);
	}
}