<?php
class Ajax extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Product_model');
    }
    public function scroll_product() {
        $cat = $_POST;
        $cat_id = $cat['cat_id'];
        $sub_id = $cat['sub_id'];
        $data['start'] = $cat['last_limit'];
        $data['end'] = 24;
        $data['order_by'] = $cat['order_by'];
        $data['size_id'] = $cat['size_id'];
        $data['cat_id'] = $cat_id;
        $data['sub_id'] = $sub_id;
        $data['min_range'] = $cat['min_range'];
        $data['max_range'] = $cat['max_range'];
        $data['product'] = $this->Product_model->prod_by_cat($data);
        if ($this->session->userdata('user_id')) {
            $data['data'] = $this->userinfo();
            $user = $data['data'];
            $new_res = array();
            if ($user->reseller_id > 0) {
                foreach ($data['product'] as $rs) {
                    $rs->price = $this->check_reseller($user->reseller_id, $rs->id, $rs->price);
                    $new_res[] = $rs;
                }
                $data['product'] = $new_res;
            }
            //$user_id = $this->session->userdata('user_id');
            //$user = $this->db->where('id',$user_id)->get('customer')->row();
            //echo '<pre>';
            //print_r($template['product']);
            
        }
        //$template['last_query'] = $this->db->last_query();
        $template['data'] = $this->load->view('product/product_scroll', $data, true);
        $template['total_products'] = $this->Product_model->prod_by_filter_all($cat_id, $data['size_id'], $sub_id, $data['min_range'], $data['max_range']);
        $template['last_limit'] = $cat['last_limit'] + $data['end'];
        print json_encode($template);
    }
    public function check_reseller($reseller_id, $prod_id, $price) {
        $row = $this->db->where('product_id', $prod_id)->where('reseller_id', $reseller_id)->get('reseller_price')->row();
        if (count($row) > 0) {
            return $row->price;
        } else {
            return $price;
        }
    }
    public function userinfo() {
        $id = $this->session->userdata('user_id');
        $this->db->select("CONCAT(first_name,' ',last_name) AS first_name,email,first_name as name,last_name,reseller_id");
        $this->db->from('customer');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    public function filter_product() {
        $cat = $_POST;
        $cat_id = $cat['cat_id'];
        $sub_id = $cat['sub_id'];
        $data['start'] = 0;
        $data['end'] = 24;
        $data['order_by'] = $cat['order_by'];
        $data['size_id'] = $cat['size_id'];
        $data['cat_id'] = $cat_id;
        $data['sub_id'] = $cat['sub_id'];
        $data['min_range'] = $cat['min_range'];
        $data['max_range'] = $cat['max_range'];
        // print_r($data);die;
        $data['product'] = $this->Product_model->prod_by_cat($data);
        if ($this->session->userdata('user_id')) {
            $data['data'] = $this->userinfo();
            $user = $data['data'];
            $new_res = array();
            if ($user->reseller_id > 0) {
                foreach ($data['product'] as $rs) {
                    $rs->price = $this->check_reseller($user->reseller_id, $rs->id, $rs->price);
                    $new_res[] = $rs;
                }
                $data['product'] = $new_res;
            }
            //$user_id = $this->session->userdata('user_id');
            //$user = $this->db->where('id',$user_id)->get('customer')->row();
            //echo '<pre>';
            //print_r($template['product']);
            
        }
        $template['total_products'] = $this->Product_model->prod_by_filter_all($cat_id, $data['size_id'], $sub_id, $data['min_range'], $data['max_range']);
        $template['data'] = $this->load->view('product/product_scroll', $data, true);
        $template['last_limit'] = $data['end'];
        print json_encode($template);
    }
    public function size_list() {
        $data = $_POST;
        $data['result'] = $this->Product_model->get_size_list($data);
        $data['type'] = 'size';
        $temp = $this->load->view('product/product_ajax', $data);
        //$result = array('data'=>$temp);
        
    }
    public function size_custom_list() {
        $data = $_POST;
        $rs = $this->Product_model->get_custom_size_list($data);
        echo $rs;
    }
    public function get_keyword() {
        $key = $_GET['key'];
        $rs = $this->Product_model->get_search_product($key);
        print json_encode($rs);
    }
    public function search_item() {
        $data = $_POST;
        $rs = $this->Product_model->search_item($data['search']);
        print json_encode($rs);
    }
    public function update_cart() {
        $data = $_POST;
        $user_id = $this->session->userdata('user_id');
        $rs = $this->Product_model->update_cart($data);
        $rs = $this->db->query("SELECT SUM(price) AS price FROM cart WHERE customer_id =" . $user_id)->row();
        print json_encode($rs->price);
    }

    public function filter_search_pdt(){
        $data = $_POST;
        $rs['product'] = $this->Product_model->search_filter($data);
        //echo '<pre>';
        //print_r($rs);
        $template['data'] = $this->load->view('product/product_filter', $rs, true);
        print json_encode($template);
    }
}