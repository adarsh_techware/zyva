/* checkout-wizard */
    


    /*var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    $(".next").click(function() {
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50) + "%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale(' + scale + ')'
                });
                next_fs.css({
                    'left': left,
                    'opacity': opacity
                });
            },
            duration: 800,
            complete: function() {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });*/

    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches



$('#next1').on('click', function (e) {
    var url = base_url+'Checkout/countcart';
    /*var result = post_ajax(url);
    obj = JSON.parse(result);   
    if(obj.count=='0')
    {
        $(".errormsg2").html('Your cart is empty');
        setTimeout(function(){$(".errormsg2").hide();}, 2000);
    }
    else
    {*/
        var value=$('.checkout_tab').serialize();

        var data = {value:value};
        var url = base_url+'Account/address';
        var result = post_ajax(url, data);
        obj = JSON.parse(result);
        
        console.log(obj.status);
        if(obj.status=="success"){
            $(".errormsg").html('');
            $(".errormsg").html('<p class="error">'+obj.message+'</p>');
            setTimeout(function(){$(".errormsg").hide();}, 3000);
        }
        else{
            
            $(".errormsg").html('');
            $(".errormsg").html('<p class="error">'+obj.message+'</p>');
            setTimeout(function(){$(".errormsg").hide();}, 3000);
        }
    //}
    
           
});

    $(".next").click(function() {
		
		
		  if ($('.checkout_tab').parsley().validate())
	{
		
		
		
		//alert("hi");
		
		
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50) + "%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale(' + scale + ')'
                });
                next_fs.css({
                    'left': left,
                    'opacity': opacity
                });
            },
            duration: 800,
            complete: function() {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    }
	
	
	}
	
	
	
	
	);


    $(".previous").click(function() {
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1 - now) * 50) + "%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'left': left
                });
                previous_fs.css({
                    'transform': 'scale(' + scale + ')',
                    'opacity': opacity
                });
            },
            duration: 800,
            complete: function() {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".submit").click(function() {
        return false;
    })

/*  checkout tab end*/







<div class="col-md-8 col-sm-12 col-xs-12">
                  <div class="multi_wizard">
                   
                     <!-- multistep form -->
                     <form id="msform"  data-parsley-validate="" class="validate adredd_val">
                        <!-- progressbar -->
                        <ul id="progressbar">
                           <li class="active">
                              <p class="bill">Billing Address</p>
                           </li>
                           <li>
                              <p class="ship">Shipping and Payment</p>
                           </li>
                           <li>
                              <p class="ord_inf">Order Information</p>
                           </li>
                        </ul>
                        <!-- fieldsets -->
                        
                        <fieldset>

                           <h2 class="fs-title">Billing Address</h2>
                          <div class="rows">
                            <div class="checkout_billing">
                                        <?php //foreach($address as $address_info) { ?>
                            
                            <div class="text_boxs">
                              <input type="text" name="firstname"  value="<?php echo $address->firstname; ?>" placeholder="First name" 
                                          data-parsley-trigger="change" 
                              data-parsley-pattern="^[a-zA-Z\  \/]+$" 
                              required="" data-parsley-required-message="Please insert your first name."
                              data-parsley-errors-container="#first_name"/>

                              <div id="first_name" class="custom_parse_error"></div>
                            </div>

                            <div class="text_boxs">
                              <input type="text" name="lastname" value="<?php echo $address->lastname; ?>"placeholder="Last name" 
                                          data-parsley-trigger="change" 
                              data-parsley-pattern="^[a-zA-Z\  \/]+$"
                              required="" data-parsley-required-message="Please insert your last name."
                              data-parsley-errors-container="#last_name"/>

                              <div id="last_name" class="custom_parse_error"></div>  
                            </div>
                                          
                              <div class="text_boxs">
                              <input type="text" name="address_1"  value="<?php echo $address->address_1; ?>"placeholder="Address Line 1" 
                              data-parsley-trigger="change" 
                              data-parsley-minlength="2"  data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"  
                              required="" data-parsley-required-message="Please insert address line 1."
                              data-parsley-errors-container="#add_line1"/>

                              <div id="add_line1" class="custom_parse_error"></div> 
                            </div>
                                 
                              
                            <div class="text_boxs"> 
                              <input type="text" name="address_2"  value="<?php echo $address->address_2; ?>"placeholder="Address Line 2" 
                              data-parsley-trigger="change" 
                              data-parsley-minlength="2"  data-parsley-pattern="^[a-zA-Z\ 0-9 \/]+$" 
                              required="" data-parsley-required-message="Please insert address line 2."
                              data-parsley-errors-container="#add_line2"/>

                              <div id="add_line2" class="custom_parse_error"></div>
                            </div> 

                            </div>

                            <div class="checkout_billing">
                            <?php if($statedetails > 0) { ?>
                              
                              <div class="text_boxs">
                                <div class="place-select slct_drop ">
                              
                                   <select class="sscct" onchange="selectCity(this.options[this.selectedIndex].value)"  name="state_id" id="state_id" required="" data-parsley-required-message="Please select state."
                                    data-parsley-errors-container="#states"/>
                                      <option selected="" value="">State</option>

                                       <?php foreach($statedetails as $statedetails){ ?>

                                        <option value="<?php echo $statedetails->id;?>"
                                            <?php if ($statedetails->id == $data1->state_id ){ ?>
                                            selected = "selected" <?php } ?> > 
                                            <?php echo $statedetails->state_name;?>
                                          </option>
                                      <?php } ?>
                                   
                                    </select>
                                </div>
                                <div id="states" class="custom_parse_error"></div>

                              </div>
                                            
                                            
                                            
                              <div class="text_boxs">              
                               <div class="place-select slct_drop">
                                  <select class="sscct"   id="city_dropdown" name="city_id"  onchange="" required="" data-parsley-required-message="Please select city."
                                    data-parsley-errors-container="#city"/>
                                    <option selected="" value="-1">City</option>

                                    <?php foreach($citydetails as $citydetails) { ?>

                                      <option value="<?php echo $citydetails->id;?>"<?php if ($citydetails->id == $data1->city_id){ ?>
                                      selected = "selected" <?php } ?>><?php echo $citydetails->city_name;?></option>
                                   
                                    <?php } ?>
                                  </select>
                                </div>
                                <div id="city" class="custom_parse_error"></div>
                              </div>                
                          
                             <?php } ?>

                                     
                            <div class="text_boxs">        
                              <input type="text" name="Zip"  value="<?php echo $address->Zip; ?>"placeholder="Zip Code" 
                                          data-parsley-trigger="change" 
                              data-parsley-minlength="2" data-parsley-maxlength="5" data-parsley-pattern="^[0-9\  \/]+$"  
                              required="" data-parsley-required-message="Please insert zip code."
                              data-parsley-errors-container="#zip"/>

                              <div id="zip" class="custom_parse_error"></div>
                            </div>
                           
                            <div class="text_boxs">   
                              <input type="text" name="telephone"  value="<?php echo $address->telephone; ?>"placeholder=" Telephone" 
                                            data-parsley-trigger="change"   
                              data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$"
                              required="" data-parsley-required-message="Please insert phone number."
                              data-parsley-errors-container="#phone"/>

                              <div id="phone" class="custom_parse_error"></div>
                            </div>
                            
                            
                            <?php //} ?>
                          </div>
                           </div>
                          <!-- <div class="checkout_billing_btn"> -->

                           <h2 class="fs-title pad_tops">Delivery Address</h2>
                           <div class="check_box">
                              <div class="check_lft">
                                 <input id="checkbox-8" class="checkbox-custom" name="" type="checkbox" 
                                     />
                                 <label for="checkbox-8" class="checkbox-custom-label"> Subscribe to Newsletter</label>
                              </div>
                           </div>
                           <div id="newsletter" class="custom_check_parse_error"></div>
                           <input type="button" id="next1" name="next" class="next action-button next_action" value="Next"/>
                                      <!-- </div> -->
                       
                                    
                        </fieldset>

                      
                        
                        <fieldset>
                              <h2 class="fs-title">Shipping Method</h2>
                              <div class=" check_box" id="check1">
                                <div class="check_lft" >
                                                  <input id="checkbox-3" class="checkbox-custom shipping" required="" name="checkbox-2" type="radio" value="<?php echo $shipping->charge; ?>" >
                                  <label for="checkbox-3" class="checkbox-custom-label"><?php echo $shipping->label; ?></label>
                                </div>
                                <div class="price" id="price"> &#8377; <?php echo $shipping->charge; ?></div>
                              </div>

                              <div class="check_box" >
                                <div class="check_lft">
                                               <input id="checkbox-4" class="checkbox-custom shipping" name="checkbox-2" type="radio" value="<?php echo $shipping1->charge; ?>" >
                                   <label for="checkbox-4" class="checkbox-custom-label"><?php echo $shipping1->label; ?></label>
                                </div>
                                <div class="price"> &#8377; <?php echo $shipping1->charge; ?></div>
                              </div>
                            
                            <h2 class="fs-title pad_tops">Is it a Gift ?</h2>
                            <div class="check_box">
                              <div class="check_lft"> 
                                 <input id="checkbox-5" class="checkbox-custom gift" name="checkbox-2"  type="checkbox" value="<?php echo $shipping2->charge; ?>"  >
                                 <label for="checkbox-5" class="checkbox-custom-label"> <?php echo $shipping2->label; ?></label>
                              </div>
                                 <div class="price"> &#8377; <?php echo $shipping2->charge; ?></div>
                           </div>
                            
                            <h2 class="fs-title pad_tops">Payment Method</h2>
                            <div class="check_box">
                              <div class="check_lft">
                                 <input id="checkbox-6" class="checkbox-custom" required="" name="checkbox-2" type="checkbox" checked="checked" readonly >
                                 <label for="checkbox-6" class="checkbox-custom-label"> Cash on Delivery</label>
                              </div>
                           </div>
                            
                            <input type="hidden" id="shipping_charge" name="shipping_charge" />
                            <input type="hidden" id="enable_gift" name="enable_gift" />

                           <input type="button" name="previous" class="previous action-button" value="Previous" />
                           
                          
                           <input id="next" type="button" name="next" class="action-button next" value="Next"/>
                          
                        </fieldset>
                        
                        <fieldset>

                                    <input type="hidden" id="check1" name="check1" />
                                    <input type="hidden" id="check2" name="check2" />
                    
                        
                        
                        
                        <h2 class="fs-title">Order Information</h2>
                        <div class="row">
                            <div class="col-md-2">
                              <div class="sub_tot">Sub Total</div>
                            </div>

                            <div class="col-md-3">
                               <div class="sub_tot total_rup">
                                              &#8377; <?php echo $total;?>.00    
                              </div>
                            </div>

                            <div class="col-md-7"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                              <div class="sub_tot">Grand Total</div>
                            </div>

                              <div class="col-md-3">
                                <div class="sub_tot total_rup">
                                   <span>&#8377;</span><span id="abc"> 0</span>.00
                                </div>
                             </div>

                            <div class="col-md-7"></div>
                         </div>

                            <div class="check_box">
                              <div class="check_lft full_check">
                                 <input id="checkbox-7" class="checkbox-custom" name="checkbox-2" type="checkbox" >
                                 <label for="checkbox-7"  class="checkbox-custom-label"> By placing an order you agree our Terms & Conditions & Privacy Policy </label>
                              </div>
                           </div>
                            
                           <input type="button" name="previous" class="previous action-button" value="Previous" />
                           
                           <input id="pay" type="submit" name="submit" class="submit action-button" value="Place Order" />
                           
                        </fieldset>
                     </form>
                  </div>
                    </div>

