$(document).ready(function() {

// show forgot password modal from login modal
    $('#forgot_password').click(function(e){
        e.preventDefault();

        $('#myModal').modal('hide') .on('hidden.bs.modal', function (e) {
                $('#forgot').modal('show');
                $('body').css('overflow-x', 'hidden');

                $('body').addClass('modal-open');
                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });

    });

// show register modal from login modal

    $('#reg').click(function(e){
        e.preventDefault();

        $('#myModal').modal('hide').on('hidden.bs.modal', function (e) {
                $('#myModal_reg').modal('show');
                $('body').css('overflow-x', 'hidden');
                $('body').addClass('modal-open');
                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });

    })

// show login modal from register modal
    $('#already_acc_signin').click(function(e){
        e.preventDefault();

        $('#myModal_reg').modal('hide').on('hidden.bs.modal', function (e) {
                $('#myModal').modal('show');
                $('body').css('overflow-x', 'hidden');
                $('body').addClass('modal-open');

                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });

    });

// show register modal from login modal

    $('#from_forgot_register').click(function(e){
        e.preventDefault();

        $('#forgot').modal('hide').on('hidden.bs.modal', function (e) {
                $('#myModal_reg').modal('show');
                $('body').css('overflow-x', 'hidden');
                $('body').addClass('modal-open');
                $(this).off('hidden.bs.modal'); // Remove the 'on' event binding
            });

    })

    
// Zoom image in product info page
 
    $('.small-thumbnail img').click(function () {
        $('#DataDisplay').attr("src", $(this).attr("data-image"));
        $('.small-thumbnail img').removeClass('highlight_zoom');
        $(this).addClass('highlight_zoom');
    });
 

    // $('.color_pick_outer').on('click', function () {
    //     $('.color_pick_outer ').removeClass('highlight');
    //     $(this).addClass('highlight');
    // });

$(window).load(function() {
  var width = $(window).width();
  if(width > 1200) {
    $('.slider').slick({
      slidesToShow: 5,
      centerMode: false,
      autoPlay: true,
      centerPadding: "0px",
      speed: 500
    })
  }
});
 
$(window).load(function() {
  var width = $(window).width();
    if(width > 700) {
      $('.slider').slick({
          slidesToShow: 2,
          centerMode: false,
          autoPlay: true,
          centerPadding: "0px",
          speed: 500
        })
    }
});


$(window).load(function() {
  var width = $(window).width();
  if(width < 700) {
    $('.slider').slick({
      slidesToShow: 1,
      centerMode: false,
      autoPlay: true,
      centerPadding: "0px",
      speed: 500
    })
  }
});





$(document).ready(function () {
    $('.color_pick_outer').on('click', function () {
        var product_id = $('#product_id').val();
        var outer = $(this).data('id');
        $('.color_pick_outer ').removeClass('highlight');        
        $(this).addClass('highlight');
        get_size_list(product_id,outer);
        //get_custom(product_id,outer);
        $('#color_id').val(outer);
        $('#size_id').val("");
        
    });

    $('.size_new_opt').on('click',function(){
        $(".sizes").removeClass('act_size');
        $('#chkPassport').prop('checked', false);
        $('#chkPassport').attr('checked', false); 
        $(this).toggleClass('act_size');
        var size_id = $(this).data('id');
        $('#size_id').val(size_id);
    })
});


 $('.color_pick_outer').on('click', function () {
    var product_id = $('#product_id').val();
    var color_id = $(this).data('id');
    var data = {'product_id':product_id,'color_id':color_id};
    var url = base_url+'ajax/size_custom_list';
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(response) {
           
          if(response==0){
            $('.stiching_box').css('display','none');
          } else {
            $('.stiching_box').css('display','block');
          }
        }
    });
})

    


/*carosel slider product*/
    $('.carousel[data-type="multi"] .item').each(function() {
        // alert("Hai....");
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 4; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });
   

//////checkbox checked////

    $('#chkPassport').on('click',function () {
        if($(this).is(':checked')==true){
            $("#addtocart1").hide();
            $("#addtocart").show();
            $(".sizes").removeClass('act_size'); 
            if($('#color_id').val()!=''){
                $('.add_bag').removeAttr("disabled");
            }          
            
            var size_id = $(this).data('id');
            $('#size_id').val(size_id);
        } else {
            $('.add_bag').attr('disabled','disabled');
            $("#addtocart1").show();
            $("#addtocart").hide();
            $('#size_id').val('');
        }
  
  });
    
 
    
    



$('.my_class').on('click',function(){
    $(".stich_images_front").hide();
    var id = $(this).data('id');
    $('#round_front_'+id).css('display','block');
    $('#front_id').val(id);
})
	


    $('.my_class1').on('click',function(){
    $(".stich_images_back").hide();
    var id = $(this).data('id');
    $('#round_back_'+id).css('display','block');
    $('#back_id').val(id);
})


     $('.my_class2').on('click',function(){
    $(".stich_images_sleeve").hide();
    var id = $(this).data('id');
    $('#round_sleeve_'+id).css('display','block');
    $('#sleeve_id').val(id);
})


     $('.my_class3').on('click',function(){
    $(".stich_images_hemline").hide();
    var id = $(this).data('id');
    $('#round_hemline_'+id).css('display','block');
    $('#hemline_id').val(id);
})
    
    
     $('.add_a').on('click',function(){
    $(".addon_class_one").hide();
    var id = $(this).data('id');
    $('#round_addon_'+id).css('display','block');
    $('#addon_id').val(id);
    // $('#')

  
    
})


     $('.add_a1').on('click',function(){
    $(".addon_class_two").hide();
    var id = $(this).data('id');
    $('#round_addon1_'+id).css('display','block');


    //$('#round_addon1_'+id).css('display','block');
    $('#addon1_id').val(id);
})



     $('.add_a2').on('click',function(){
    $(".addon_class_three").hide();
    var id = $(this).data('id');

    $('#round_addon2_'+id).css('display','block');
    $('#addon2_id').val(id);
})
	

     $('.add_a3').on('click',function(){
    $(".addon_class_four").hide();
    var id = $(this).data('id');
    $('#round_addon3_'+id).css('display','block');
    $('#addon3_id').val(id);
})
	//////////////
	function myFunction($id1) {
	$(".stich_images").hide();

		//e.preventDefault();
		//alert($id1);
	
}




    ////checkbox clicked///


    

//  $(document).ready(function() {  
// //$('#addtocart').hide();
//    $('#addtocart').css('visibility','hidden'); 
      
// $('#chkPassport').click(function () {
 
//   var chkstich = document.getElementById("chkPassport");

// if (chkstich.checked) {

//    $("#chkPassport").val("checked");
//             $('#addtocart').css('visibility','visible');  
//           // $('#addtocart').Show();
//              // $('#addtocart1').hide();
//               $('#addtocart1').css('visibility','hidden');  
//                       } 

//     else {
//             // $('#addtocart').hide();
//             //  $('#addtocart1').show();
//              $('#addtocart').css('visibility','hidden'); 
//                $('#addtocart1').css('visibility','visible');  
//          }
// });
// });



    
    
    
/* pop-over*/
    
    

    // var searchBox = $('#search .search-box');
    // var searchTrigger = $('#search-trigger');

    // searchTrigger.on("click", function(e) {
    //     e.preventDefault();
    //     e.stopPropagation();
    //     searchBox.toggle();
    // });
    // $(document).click(function(event) {
    //     if (!(searchBox.is(event.target)) && (searchBox.has(event.target).length === 0)) {
    //         searchBox.hide();
    //     };
    // });



    
    

    
    

/*  Wizard for stiching starts*/
    
    

    $(document).ready(function() {
        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function(e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function() {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
   




/* change dress color*/

    
  
 


/*about-page -tabs*/



    $('.buttons').click(function(e) {
        e.preventDefault();
        setContent($(this));
    });

    $('.buttons.active').length && setContent($('.buttons.active'));

    function setContent($el) {
        $('.buttons').removeClass('active');
        $('.cont').hide();

        $el.addClass('active');
        $($el.data('rel')).show();
    }





 /*shrink add background-color*/
    

    $(window)
        .on("scroll", function() {
            if ($(window)
                .scrollTop() > 50) {
                $(".custom_nav")
                    .addClass("actives");

            } else {
                //remove the background property so it comes transparent again (defined in your css)
                $(".custom_nav")
                    .removeClass("actives");

            }
        })



    $(window)
        .on("scroll", function() {
            if ($(window)
                .scrollTop() > 50) {
                $(".inner_nav")
                    .addClass("actives");

            } else {
                //remove the background property so it comes transparent again (defined in your css)
                $(".inner_nav")
                    .removeClass("actives");

            }
        })
    
    
    
    

 /*  Shrink and AutoHide navbar*/


    $(window)
        .scroll(function() {
            if ($(document)
                .scrollTop() > 100) {
                $('nav')
                    .addClass('shrink')
                    .addClass('navbar-inverse');
            } else {
                $('nav')
                    .removeClass('shrink')
                    .removeClass('navbar-inverse');;
            }
            if ($(document)
                .scrollTop() >= 200) {
                //$("nav").autoHidingNavbar();
            }
        });




/*share social icons*/

    $(".share_icons").hide();
    $(".close_share").hide();
    $(".close_share_outedr").hide();

    $(".share").click(function() {
        $(".share").hide();
        $(".share_icons").show();
        $(".close_share").show();
        $(".close_share_outedr").show();

    });
    $(".close_share").click(function() {
        $(".share_icons").hide();
        $(".close_share").hide();
        $(".share").show();

    });




 /* owl demo*/

    $("#owl-demo")
        .owlCarousel({

            autoPlay: 30000, //Set AutoPlay to 3 seconds

            items: 3,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3]

        });




/* price ranger*/
    
    //var min_range = $('#min_range').val();
    //var max_range = $('#max_range').val();
    
    jQuery("#Slider1").slider({
        from: 100,
        to: 10000,
        step: 100,
        smooth: true,
        round: 0,
        skin: "plastic",
        /*onstatechange: function( myvalue ){ 
               console.log(myvalue);               
               call_me(myvalue);
			
        }*/
    });

    /*jQuery("#Slider1").on('change',function{
        alert('sdfdfsdf');
    }) */
	
	function call_me(myvalue){
		
		
		var data = {'range':myvalue};
		var url = base_url+'Women/price_range';
		var result = post_ajax(url, data);
		obj = JSON.parse(result);
			 
			if(obj){
				
				$('#price_content1').html(obj.template);
			}
			else{
				
				$(".errormsg1").html('');
				$(".errormsg1").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg1").hide();}, 3000);
			}

	}

    function call_scroll_products(){
        var result = '';
        var data = '';
        $.ajax({
            type: "POST",
            url: base_url+'ajax/scroll_product',
            data: data,
            success: function(response) {
                result = response;

            },
            error: function(response) {
                result = 'error';
            },
            async: false
        });
        return result;
    }



/*  scrol_bottom*/
    
    

    $("button_scroll")
        .click(function() {
            $("html, body")
                .animate({
                    scrollTop: $('#myDiv')
                        .offset()
                        .top - 90
                }, 1500);
        });




 /*back to top*/
 
    

    $(window)
        .scroll(function() {
            if ($(this)
                .scrollTop() > 100) {
                $('#scroll')
                    .fadeIn();
            } else {
                $('#scroll')
                    .fadeOut();
            }
        });
    $('#scroll')
        .click(function() {
            $("html, body")
                .animate({
                    scrollTop: 0
                }, 600);
            return false;
        });



});

$('#addtocart').on('click',function(){
   
    if($('#size_id').val()!='' && $('#color_id').val()!=''){
        $('#stichingModal').modal('show');
    } else {
        $(".front_style").html('select size and color');
    }
})
// $('.first_list').addClass('delivery_address_active');
// $('.delivery_address_list').on('click',function(){
//     $('.delivery_address_list').removeClass('delivery_address_active');
//     $(this).addClass('delivery_address_active');
// });