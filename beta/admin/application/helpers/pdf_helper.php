<?php
function tcpdf() {
    require_once ('tcpdf/examples/lang/eng.php');
    require_once ('tcpdf/tcpdf.php');
}
function get_icon() {
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->where('id', 1);
    $CI->db->from('settings');
    $result = $CI->db->get()->row();
    return $result;
}
function set_upload_option() {
    //upload an image options
    $config = array();
    $config['upload_path'] = 'assets/uploads';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '';
    $config['min_size'] = '';
    $config['overwrite'] = FALSE;
    return $config;
}
function set_upload_logo($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png';
    $config['width'] = 60;
    $config['height'] = 80;
    $config['overwrite'] = FALSE;
    return $config;
}
function set_upload_optionsgallery($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '0';
    $config['overwrite'] = FALSE;
    return $config;
}
function set_upload_profile($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '0';
    $config['overwrite'] = FALSE;
    return $config;
}
function set_upload_profilepic($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '0';
    $config['overwrite'] = FALSE;
    return $config;
}
function set_upload_banner($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '900';
    $config['overwrite'] = FALSE;
    return $config;
}
function set_banner_image($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '900';
    $config['overwrite'] = FALSE;
    return $config;
}
function set_upload_product($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '0';
    $config['overwrite'] = FALSE;
    return $config;
}
function set_product_image($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '0';
    $config['overwrite'] = FALSE;
    return $config;
}
function set_upload_subcategory($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '0';
    $config['overwrite'] = FALSE;
    return $config;
}
function set_upload_category($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '0';
    $config['overwrite'] = FALSE;
    return $config;
}
function set_upload_color($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '0';
    $config['overwrite'] = FALSE;
    return $config;
}
function edit_upload_color($path) {
    //upload an image options
    $config = array();
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '0';
    $config['overwrite'] = FALSE;
    return $config;
}
function check_login() {
    $CI = & get_instance();
    if ($CI->session->userdata('logged_in') == false) {
        redirect(base_url());
    }
}
function get_reseller($id) {
    if ($id != null) {
        $CI = & get_instance();
        $CI->db->select("CONCAT(first_name,' ',last_name) AS name");
        $CI->db->where('id', $id);
        $CI->db->from('reseller');
        $result = $CI->db->get()->row();
        if ($result) return $result->name;
        else return '';
    } else {
        return '';
    }
}
// function get_address($id){
// 	$CI = & get_instance();
// 	$rs = $CI->db->where('id',$id)->get('address_book')->row();
// 	$string = $rs->firstname.' '.$rs->last_name.'<br/>'.$rs->address_1.'<br/>'.$rs->address_2.'<br/>'.$rs->telephone;
// 	return $string;
// }
function get_address($id) {
    $CI = & get_instance();
    $rs = $CI->db->where('id', $id)->get('address_book')->row();
    $string = $rs->firstname . '<br/>' . $rs->address_1 . '<br/>' . $rs->address_2 . '<br/>' . $rs->telephone;
    return $string;
}
function get_stitch_info($id) {
    $CI = & get_instance();
    $res = $CI->db->where('id', $id)->get('stitching')->row();
    //print_r($res);
    $front = get_stitch($res->front_id);
    //print_r($front);
    $res->back_id;
    $back = get_stitch($res->back_id);
    //print_r($back);
    $sleeve = get_stitch($res->sleeve_id);
    $hemline = get_stitch($res->neck_id);
    $add_ons = get_addons($res->add_ons);
    $add_ons1 = get_addons($res->add_ons1);
    $add_ons2 = get_addons($res->add_ons2);
    $add_ons3 = get_addons($res->add_ons3);
    $meas_type = $res->measure_id == 0 ? 'Free Size' : 'Use Measurment';
    $measurement = get_measurement($res->measure_id);
    $stich_array = array('front' => $front, 'back' => $back, 'sleeve' => $sleeve, 'hemline' => $hemline, 'add_ons' => $add_ons, 'add_ons1' => $add_ons1, 'add_ons2' => $add_ons2, 'add_ons3' => $add_ons3, 'meas_type' => $meas_type, 'measure_id' => $res->measure_id, 'specialcase' => $res->specialcase, 'measurement' => $measurement);
    //print_r($stich_array);
    return $stich_array;
}
function get_stitch($id) {
    $CI = & get_instance();
    $new_rs = $CI->db->where('id', $id)->get('stich_type')->row();
    return $new_rs;
}
function get_addons($id) {
    $CI = & get_instance();
    $new_rs = $CI->db->where('id', $id)->get('add_ons')->row();
    return $new_rs;
}
function get_measurement($id) {
    $CI = & get_instance();
    $new_rs = $CI->db->where('id', $id)->get('new_measurement')->row();
    return $new_rs;
}
function get_product_image($id) {
    $ci = & get_instance();
    $rs = $ci->db->select('product_image,format')->where('product_id', $id)->get('product_gallery')->row();
    if (count($rs) > 0) return base_url($rs->product_image . $rs->format);
}
/*********************************/
function side_nav($tag, $assigned) {
    if ($tag == $assigned) {
        return 'active';
    }
}
function sub_nav($sub, $assigned) {
    if ($sub == $assigned) {
        return 'active';
    }
}
function get_function($class, $function) {
    $ci = & get_instance();
    $ci->load->database();
    $result = $ci->db->query("SELECT function_menu.fun_name,function_menu.fun_path,function_menu.id,function_menu.main_id,function_menu.fun_menu,main_menu.menu_name,main_menu.main_name FROM function_menu INNER JOIN main_menu ON function_menu.main_id = main_menu.id WHERE main_menu.main_control = '$class' AND function_menu.fun_path = '$function'");
    if ($result->num_rows() > 0) {
        return $result->row();
    } else {
        //return null;
        return (object)array('fun_menu' => '', 'menu_name' => '');
    }
}
function get_permit($role) {
    $ci = & get_instance();
    $ci->load->database();
    $result = $ci->db->query('SELECT permissions.main_id,permissions.function_id,main_menu.main_control FROM permissions INNER JOIN main_menu ON permissions.main_id = main_menu.id WHERE permissions.role_id =' . $role . ' ORDER BY main_menu.main_priority ASC');
    $sub = array();
    $menu = array();
    if ($result->num_rows() > 0) {
        $row = $result->result();
        foreach ($row as $rs) {
            $function_ids = explode(',', $rs->function_id);
            $module = $ci->db->where('id', $rs->main_id)->get('main_menu')->row();
            $sub = array();
            foreach ($function_ids as $key) {
                $data = $ci->db->where('id', $key)->get('function_menu')->row();
                array_push($sub, $data);
            }
            $module->sub = $sub;
            array_push($menu, $module);
        }
        return $menu;
    } else {
        return array();
    }
}
function privillage($class, $method, $role_id) {
    $ci = & get_instance();
    $array = array();
    $rs = $ci->db->query("SELECT function_menu.id,function_menu.main_id FROM function_menu INNER JOIN main_menu ON function_menu.main_id = main_menu.id WHERE function_menu.fun_path = '$method' AND main_menu.main_control = '$class'");
    if ($rs->num_rows() > 0) {
        $rs = $rs->row();
        $row = $ci->db->query("SELECT permissions.function_id FROM permissions WHERE permissions.role_id = $role_id AND permissions.main_id =" . $rs->main_id);
        if ($row->num_rows() > 0) {
            $result = $row->row();
            $array = explode(',', $result->function_id);
            if (in_array($rs->id, $array)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return true;
    }
}
function get_reseller_sale($rid) {
    $CI = & get_instance();
    $CI->db->where('reseller_id', $rid);
    $query = $CI->db->get('booking');
    $count = $query->num_rows();
    return $count;
}
function get_reseller_earnings($rid) {
    $ci = & get_instance();
    $ci->db->select("sum(order_details.price - product.dp) as earning");
    $ci->db->from('order_details');
    $ci->db->join('product', 'product.id = order_details.product_id');
    $ci->db->join('booking', 'booking.id = order_details.booking_id');
    $ci->db->where('reseller_id', $rid);
    $ci->db->where('order_details.status !=', '4');
    $ci->db->where('order_details.size_id !=', '7');
    $ci->db->where('booking.status', '1');
    //$ci->db->group_by('reseller_id');
    $query = $ci->db->get()->row();
    $earning = $query->earning;
    //echo $ci->db->last_query();die;
    return $earning;
}


function get_reseller_earning($rid) {
	$ci = & get_instance();
    $ci->db->select_sum('total_rate');
	$ci->db->from('booking');
	$ci->db->where('status', '1');
	$ci->db->where('reseller_id', $rid);
	$query = $ci->db->get();
	$total = $query->row()->total_rate;
	//echo $ci->db->last_query();die;
	return $total;
}

function get_total_paid($rid) {
	$ci = & get_instance();
    $ci->db->select_sum('amount');
	$ci->db->from('reseller_payments');
	$ci->db->where('reseller_id', $rid);
	$query = $ci->db->get();
	$total = $query->row()->amount;
	return $total;
}

