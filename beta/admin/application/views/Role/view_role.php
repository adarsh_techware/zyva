	

	

	<div class="content-wrapper" >

   <!-- Content Header (Page header) -->

   <section class="content-header">

      <h1>

        Manage Role

      </h1>

      <ol class="breadcrumb">

         <li><a href="<?php echo base_url();?>"><i class="fa fa-user-md"></i></i>Home</a></li>

         <li><a href="<?php echo base_url();?>user">role</a></li>

         <li  class="active"><a href="<?php echo base_url(); ?>user">View </a></li>

      </ol>

   </section>

   <!-- Main content -->

   <section class="content">

      <div class="row">

         <div class="col-xs-12">

            <?php

               if($this->session->flashdata('message')) {

                        $message = $this->session->flashdata('message');

               

                     ?>

            <div class="alert alert-<?php echo $message['class']; ?>">

               <button class="close" data-dismiss="alert" type="button">×</button>

               <?php echo $message['message']; ?>

            </div>

            <?php

               }

               ?>

         </div>

         <div class="col-xs-12">

            <!-- /.box -->

            <div class="box">

               <div class="box-header">

                  <h3 class="box-title">View Role</h3>

               </div>

               <!-- /.box-header -->

               <div class="box-body">

                  <table id="" class="table table-bordered table-striped datatable">

                     <thead>

                        <tr>

                           <th class="hidden">ID</th>

						         <th>Role</th> 						   

                           <th width="">Action</th>

                        </tr>

                     </thead> 

                     <tbody>

                        <?php

                        $i = 0;

                           foreach($data as $role) {	

//var_dump($product);

//die;						   

                           ?>

                        <tr>

                     <td class="hidden"><?php echo $role->id; ?></td>                 

                     <td class="center"><?php echo $role->rolename; ?></td>

                     <td class="center"><a class="btn btn-sm btn-primary" href="<?php echo base_url('role/edit_role/'.$role->id); ?>">

                            <i class="fa fa-fw fa-edit"></i>Edit</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-sm btn-success" href="<?php echo base_url('permission/index/'.$role->id); ?>">

                            <i class="fa fa-fw fa-check-square"></i>Permission</a></td>

                        </tr>

                        <?php

                           }

                           ?>

                     </tbody>

                     <tfoot>

                        <tr>


                            <th class="hidden">ID</th>  

                           <th>Role</th>

                           <th>Action</th>

                        </tr>

                     </tfoot>

                  </table>

               </div>

               <!-- /.box-body -->

            </div>

            <!-- /.box -->

         </div>

         <!-- /.col -->

      </div>

      <!-- /.row -->

   </section>

   <!-- /.content -->

</div>

















 