<table class="table table-bordered">
<tbody>
	<tr>
          <td>Name</td>
          <td><?php echo $reseller->first_name.' '.$reseller->last_name; ?></td>
        </tr>
        
        <tr>
          <td>Mobile</td>
          <td><?php echo $reseller->telephone; ?></td>
        </tr>
        <tr>
          <td>Email Id</td>
          <td><?php echo $reseller->email; ?></td>
        </tr>
        <tr>
          <td>Address</td>
          <td><?php echo $reseller->address; ?></td>
        </tr>
        
        <tr>
          <td>Account No</td>
          <td><?php echo $reseller->account_no; ?></td>
        </tr>
        <tr>
          <td>Account Holder</td>
          <td><?php echo $reseller->account_holder; ?></td>
        </tr>
        <tr>
          <td>IFSC Code</td>
          <td><?php echo $reseller->ifcs_code; ?></td>
        </tr>
        <tr>
          <td>Bank name</td>
          <td><?php echo $reseller->bank_name; ?></td>
        </tr>
        <!-- <tr>
          <td>Bank name</td>
          <td><?php echo $reseller->bank_name; ?></td>
        </tr> -->
        
        </tbody>
</table>