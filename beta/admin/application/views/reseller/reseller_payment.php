<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Reseller Payment
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>
        <li><a href="<?php echo base_url();?>Reseller">Reseller</a></li>
        <li class="active"><a href="<?php echo base_url();?>reseller/payment">Reseller Payment</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Add Payment</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start --> 
               <form role="form" action="" method="post"  data-parsley-validate="" class="validate">
                  <div class="box-body">
                   <div class="col-md-6">
                       <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">Reference Number</label>
                          <textarea class="form-control required" required="" 
                          name="ref_id"  placeholder="Reference Number"></textarea>
                        </div>

                        <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">Paid Amount</label>
                          <input type="text" class="form-control required"  required="" 
                          name="amount"  placeholder="Paid Amount">
                        </div>
                    </div>
                    <div class="col-md-12">
                      <div class="box-footer">
                         <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
                        <button class="btn btn-info pay_submit" id="" type="">Submit</button>
                      </div>
                    </div>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
