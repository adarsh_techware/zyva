<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Reseller
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>
        <li><a href="<?php echo base_url();?>Reseller">Reseller</a></li>
        <li class="active"><a href="<?php echo base_url();?>Reseller/edit">Edit Reseller</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Edit Reseller</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="" method="post"  data-parsley-validate="" class="validate" enctype="multipart/form-data">
                  <div class="box-body">


              <div class="col-md-6">
           
                         <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">First Name</label>
                            <input type="text"  value="<?php echo $reseller1->first_name; ?>" class="form-control required input_length" data-parsley-trigger="change" 
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="first_name"  placeholder="First name">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div> 
              
                          <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Last Name</label>
                            <input type="text" value="<?php echo $reseller1->last_name; ?>" class="form-control required input_length"  data-parsley-trigger="change"  
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="last_name"  placeholder="Last name">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div> 
               
                          <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Address</label>
                            <textarea class="form-control required input_length" name="address" data-parsley-trigger="change" required="" placeholder="Address" rows="5"><?php echo $reseller1->address; ?></textarea>
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>
              
             
                         <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">Email</label>
                          <input type="email"  value="<?php echo $reseller1->email; ?>" class="form-control required input_length" name="email" data-parsley-trigger="keyup"   required="" placeholder="Email">
                          <span class="glyphicon  form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">Mobile</label>
                          <input type="text"  value="<?php echo $reseller1->telephone; ?>" class="form-control required input_length" name="telephone" data-parsley-trigger="keyup" data-parsley-type="digits" data-parsley-minlength="10" data-parsley-maxlength="15" data-parsley-pattern="^[0-9]+$" required="" placeholder="Mobile">
                          <span class="glyphicon  form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">Reseller code</label>
                          <input type="text" value="<?php echo $reseller1->reseller_code; ?>"  class="form-control required input_length" name="reseller_code" data-parsley-trigger="keyup" required="" placeholder="reseller code">
                          <span class="glyphicon  form-control-feedback"></span>
                          </div>
                    
                    
                     </div>
                     <div class="col-md-6">

                       
                          <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Username</label>
                            <input type="text" value="<?php echo $reseller1->username; ?>"  class="form-control required input_length" name="username" data-parsley-trigger="change" required="" placeholder="username" readonly>
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Password</label>
                            <input type="password"  class="form-control input_length" name="password" data-parsley-trigger="change" placeholder="password">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">Account No</label>
                          <input type="text" class="form-control input_length" name="account_no" data-parsley-trigger="keyup" data-parsley-pattern="^[0-9]+$" placeholder="Bank Account No" value="<?php echo $reseller1->account_no; ?>">
                          <span class="glyphicon  form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">IFSC</label>
                          <input type="text" class="form-control input_length" name="ifcs_code" data-parsley-trigger="keyup"  placeholder="IFSC Code" value="<?php echo $reseller1->ifcs_code; ?>">
                          <span class="glyphicon  form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">Account Holder</label>
                          <input type="text" class="form-control input_length" name="account_holder" data-parsley-trigger="keyup"  placeholder="Holder Name" value="<?php echo $reseller1->account_holder; ?>">
                          <span class="glyphicon  form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">Bank name</label>
                          <input type="text" class="form-control input_length" name="bank_name" data-parsley-trigger="keyup"  placeholder="Bank name" value="<?php echo $reseller1->bank_name; ?>">
                          <span class="glyphicon  form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">Phone verified</label>
                          <select class="form-control" name="phone_verified" id="phone_verified">
                            <option value="0" <?php if($reseller1->phone_verified==0) echo 'selected="SELECTED"'; ?>>No</option>
                            <option value="1" <?php if($reseller1->phone_verified==1) echo 'selected="SELECTED"'; ?>>Yes</option>
                          </select>
                          <span class="glyphicon  form-control-feedback"></span>
                          </div>

                         
                      

                     


                          


                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">update</button>
                  </div>
           </div>
           <div class="col-md-6">



</div>

          </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
