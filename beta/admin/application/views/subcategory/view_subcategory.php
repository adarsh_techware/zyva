<div class="content-wrapper">

 <!-- Content Header (Page header) -->

 <section class="content-header">

    <h1>

      Subcategory

    </h1>

        <br><div>

<a href="<?php echo base_url(); ?>sub_category/create"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a>

  </div>



    <ol class="breadcrumb">

      <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>

      <li><a href="<?php echo base_url();?>sub_category">Subcategory</a></li>

      <li  class="active"><a href="<?php echo base_url(); ?>sub_category">View Subcategory</a></li>

      

    </ol>

 </section>

 <!-- Main content -->

 <section class="content">

    <div class="row">

       <div class="col-xs-12">

          <?php

             if($this->session->flashdata('message')) {

                      $message = $this->session->flashdata('message');



                   ?>

          <div class="alert alert-<?php echo $message['class']; ?>">

             <button class="close" data-dismiss="alert" type="button">×</button>

             <?php echo $message['message']; ?>

          </div>

          <?php

             }

             ?>

       </div>

       <div class="col-xs-12">

          <!-- /.box -->

          <div class="box">

             <div class="box-header">

                <h3 class="box-title">View Subcategory</h3>

             </div>

             <!-- /.box-header -->

             <div class="box-body">

                <table id="" class="table table-bordered table-striped datatable">

                   <thead>

                      <tr>

                          <th class="hidden">ID</th>

                          <th>Sub Category Name</th>

                          <th>Category Name</th>

                          <!-- <th>Image</th> -->

                          <th>Status</th>

                          <th>Actions</th>

                      </tr>

                   </thead>

                   <tbody>

                     <?php

                         foreach($sub_category as $sub) {

                          //  print_r($sub_category);die;

            //  $image=$sub->image;

                         ?>

                      <tr>

                        <td class="hidden"><?php echo $sub->id; ?></td>

                         <td class="center"><?php echo $sub->sub_category; ?></td>

                   <td class="center"><?php echo $sub->category_name; ?></td>

                   <!-- <td class="center" ><img width="100px" height="100px" src="<?php echo $sub->sub_image; ?>"></td> -->





                                                        <td><span class="center label  <?php if($sub->status == '1')

                                            			{

                                            			echo "label-success";

                                            			}

                                            			else

                                            			{

                                            			echo "label-warning";

                                            			}

                                            			?>"><?php if($sub->status == '1')

                                            			{

                                            			echo "enable";

                                            			}

                                            			else

                                            			{

                                            			echo "disable";

                                            			}

                                            			?></span>

                         <td class="center">

                           <!-- <a class="btn btn-sm bg-olive show-subcatgetdetails" href="javascript:void(0);" data-id="<?php //echo $sub->id; ?>">

                            <i class="fa fa-fw fa-eye"></i> View </a> -->

                            <a class="btn btn-sm btn-primary" href="<?php echo site_url('sub_category/edit_subcategory/'.$sub->id); ?>">

                            <i class="fa fa-fw fa-edit"></i>Edit</a>

                            <!-- <a class="btn btn-sm btn-danger" href="<?php echo site_url('Category/delete_subcat/'.$sub->id); ?>" onClick="return doconfirm()">

                            <i class="fa fa-fw fa-trash"></i>Delete</a> -->



                            <?php if( $sub->status){?>

                                           <a class="btn btn-sm label-warning" href="<?php echo base_url();?>sub_category/subcategory_status/<?php echo $sub->id; ?>">

                                           <i class="fa fa-folder-open"></i> Disable </a>

                                           <?php

                                              }



                                              else

                                              {

                                              ?>

                                           <a class="btn btn-sm label-success" href="<?php echo base_url();?>sub_category/subCategory_active/<?php echo $sub->id; ?>">

                                           <i class="fa fa-folder-o"></i> Enable </a>

                                           <?php

                                              }

                                              ?>

                         </td>

                      </tr>

                      <?php

                         }

                         ?>

                   </tbody>

                   <!-- <tfoot>

                      <tr>

                         <th class="hidden">ID</th>

                         <th>Sub Category Name</th>

             <th>Category Name</th>



             <th width="200px;">Action</th>

                      </tr>

                   </tfoot> -->

                </table>

             </div>

             <!-- /.box-body -->

          </div>

          <!-- /.box -->

       </div>

       <!-- /.col -->

    </div>

    <!-- /.row -->

 </section>

 <!-- /.content -->

</div>

<div class="modal fade modal-wide" id="popup-subcatgetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"

 aria-hidden="true">

 <div class="modal-dialog">

    <div class="modal-content">

       <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

          <h4 class="modal-title">View Subcategory Details</h4>

       </div>

       <div class="modal-subcatbody">

       </div>

       <div class="business_info">

       </div>

       <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

       </div>

    </div>

    <!-- /.modal-content -->

 </div>

 <!-- /.modal-dialog -->

</div>

