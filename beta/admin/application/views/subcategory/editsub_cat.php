<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Subcategory
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>
        <li><a href="<?php echo base_url();?>category/view_subcategory">Subcategory</a></li>
        <li  class="active"><a href="<?php echo base_url(); ?>category/edit_subcategory">Edit Subcategory</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-warning">
               <div class="box-header with-border">
                  <h3 class="box-title">Edit Subcategory</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
			    <form role="form" action="" method="post"  class="validate" enctype="multipart/form-data">

                  <div class="box-body">
                     <div class="col-md-6">

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">SubCategory Name</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="sub_category"  value="<?php echo $subcat->sub_category; ?>">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

					 <div class="form-group">
                          <label>Select Category</label>
							<select class="form-control select2 required"  style="width: 100%;" id="bus_name" name="category_id">
								   <?php
								   if($Category) {
									  foreach($Category as $categorydetails){
										  $s = ($categorydetails->id == $subcat->category_id) ? "selected" : "";

								   ?>
            <option <?php echo $s; ?> value="<?php echo $categorydetails->id;?>"><?php echo $categorydetails->category_name; ?></option>
								   <?php
								   }
								   }
								   ?>
                            </select>
                        </div>

                        <!-- <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Type</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="type"  value="<?php echo $subcat->type; ?>">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div> -->
                        <!-- <img src="<?php echo $subcat->sub_image; ?>" width="200px"></a>
             					 <div class="form-group ">
             									<label class="control-label" for="shopimage">Select Images</label>
             									<input type="file"  name="sub_image" size="20" />
                        </div> -->


<script type="text/javascript">
function showDiv(select){
   if(select.value=='child'){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
}
</script>







					    <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                        </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
