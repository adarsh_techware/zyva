<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Customers
      </h1>
      <br>
      <div>
         <!-- <a href="<?php// echo base_url(); ?>user/add_sub_category"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a> -->
      </div>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url(); ?>"><i class=""></i>Home</a></li>
         <li><a href="<?php echo base_url(); ?>customer">Customers</a></li>
         <li class="active">View Customers</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <?php
               if($this->session->flashdata('message')) {
                        $message = $this->session->flashdata('message');
                     ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               
               ?>
         </div>
         <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">View Customer</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <table id="" class="table table-bordered table-striped datatable">
                     <thead>
                        <tr>
                           <th class="hidden">ID</th>
                           <th>First Name</th>
                           <th>Last Name</th>
                           <th>Telephone</th>
                           <th>Email</th>
                           <th>Orders</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           foreach($customer as $userdetail) {
                           
                            //  print_r($user);die;
                           
                           
                           
                           ?>
                        <tr>
                           <td class="hidden"><?php echo $userdetail->id; ?></td>
                           <td class="center"><?php echo $userdetail->first_name; ?></td>
                           <td class="center"><?php echo $userdetail->last_name; ?></td>
                           <td class="center"><?php echo $userdetail->telephone; ?></td>
                           <!-- <td class="center" ><img width="100px" height="100px" src="<?php //echo $sub->sub_image; ?>"></td> -->
                           <td class="center"><?php echo $userdetail->email; ?></td>
                           <td class="center"><a class="btn btn-sm bg-olive show-catgetdetails"  href="<?php echo base_url('booking/index/'.$userdetail->id); ?>">
                              <i class="fa fa-fw fa-eye"></i> View </a> 
                        </tr>
                        <?php
                           }
                           
                           ?>
                     </tbody>
                     <tfoot>
                        <tr>
                           <th class="hidden">ID</th>
                           <th>First Name</th>
                           <th>Last Name</th>
                           <th>Telephone</th>
                           <th>Email</th>
                           <th>Orders</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<div class="modal fade modal-wide" id="popup-customergetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
   aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">View customer Details</h4>
         </div>
         <div class="modal-customerbody">
         </div>
         <div class="business_info">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>