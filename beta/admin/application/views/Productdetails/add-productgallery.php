<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <section class="content-header">
      <h1>
        Product
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url();?>"><i class="fa fa-user-md"></i></i>Home</a></li>
         <li><a href="<?php echo base_url();?>product/view_product">Product</a></li>
         <li  class="active"><a href="<?php echo base_url(); ?>product/add_productgallery">Create Gallery</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Create Gallery</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="" method="post"  data-parsley-validate="" class="validate" enctype="multipart/form-data">
                  <div class="box-body">
                     <div class="col-md-12">

				            <div class="form-group" id="" >
                            <label>Select Product</label>
                  <select class="form-control input_width"   name="product_id" id="product_id">

                   <?php
				  // print_r($product);
				  // die;
                  foreach($product as $pro){

                   ?>
                <option value="<?php echo $pro->id;?>"><?php echo $pro->product_name;?></option>
                   <?php
                  }
                   ?>
                   </select>
            </div>

			 <div class="form-group ">
				<label class="control-label" for="shopimage">Upload Images</label>
				<input type="file"  name="product_image" size="20" />
             </div>

						 


			 

              
				 
				
				
                  <!-- /.box-body -->
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
				   </div>


				  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
