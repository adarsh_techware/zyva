

	<div class="content-wrapper" >
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        View Product color Details
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-user-md"></i>Home</a></li>
         <li><a href="#">Product color Details</a></li>
         <li class="active">View Product color Details</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
        <div class="row">
         <div class="col-xs-12">
            <?php
               if($this->session->flashdata('message')) {
                        $message = $this->session->flashdata('message');

                     ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-xs-12">

            <!-- /.box -->
            <div class="box">
	<div>
	<a href="<?php echo base_url(); ?>Product_ctrl/add_productcolor"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a>
  </div>
               <div class="box-header">
                  <h3 class="box-title">View Product Details</h3>
               </div>

               <!-- /.box-header -->
               <div class="box-body">
                  <table id="" class="table table-bordered table-striped datatable">
                     <thead>
                        <tr>
                           <th class="hidden">ID</th>
						               <th>Product name</th>   
                           <th>Product Color</th>

                           <th>Status</th>
                           <th width="">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
						
                           foreach($data as $productcolor) {

                           ?>
                        <tr>
                           <td class="hidden"><?php echo $productcolor->id; ?></td>
                          <!--<td class="center" ><img width="100px" height="100px" src="<?php echo $product->product_image; ?>"></td> -->
                           <td class="center"><?php echo $productcolor->product_name; ?></td>
                           <td class="center"><?php echo $productcolor->color_name; ?></td>


            <td><span class="center label  <?php if($productcolor->status == '1')
			{
			echo "label-success";
			}
			else
			{
			echo "label-warning";
			}
			?>"><?php if($productcolor->status == '1')
			{
			echo "enable";
			}
			else
			{
			echo "disable";
			}
			?></span>
                           <td class="center">





                              <a class="btn btn-sm btn-primary" href="<?php echo base_url();?>Product_ctrl/edit_productcolor/<?php echo $productcolor->id; ?>">
                              <i class="fa fa-fw fa-edit"></i>Edit</a>



							 <?php if( $productcolor->status){?>
                              <a class="btn btn-sm label-warning" href="<?php echo base_url();?>Product_ctrl/productcolor_status/<?php echo $productcolor->id; ?>">
                              <i class="fa fa-folder-open"></i> Disable </a>
                              <?php
                                 }

                                 else
                                 {
                                 ?>
                              <a class="btn btn-sm label-success" href="<?php echo base_url();?>Product_ctrl/productcolor_active/<?php echo $productcolor->id; ?>">
                              <i class="fa fa-folder-o"></i> Enable </a>
                              <?php
                                 }
                                 ?>

                           </td>
                        </tr>
                        <?php
                           }
                           ?>
                     </tbody>
                     <tfoot>
                        <tr>
                            <th class="hidden">ID</th>
							 <th>Product name</th>
                           <th>Product Color</th>



                           <th>Status</th>
                           <th width="">Action</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>

<div class="modal fade modal-wide" id="popup-productModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
   aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">View Banner Details</h4>
         </div>
         <div class="modal-productbody">
         </div>
         <div class="business_info">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
