<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        Product
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url();?>"><i class="fa fa-user-md"></i></i>Home</a></li>
         <li><a href="<?php echo base_url();?>product/view_product">Product</a></li>
         <li  class="active"><a href="<?php echo base_url(); ?>product/edit_product">Edit Product</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Edit Product</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="" method="post"  data-parsley-validate="" class="validate" enctype="multipart/form-data">
                  <div class="box-body">
                     <div class="col-md-6">



						              <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Name</label>
                            <input type="text" class="form-control required"  required="" name="product_name" value="<?php echo $data->product_name; ?>" data-id="<?php echo $data->id; ?>" id="pdt_name">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>

						              <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Code</label>
                            <input type="text" class="form-control required"   required="" name="product_code"  value="<?php echo $data->product_code; ?>">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>
						  


			                   <div class="form-group" id="" >
                            <label>Category</label>
                            <select class="form-control "  style="width: 100%;" name="category_id" id="category_id">

                               <?php
            				            // print_r($category);
                              foreach($category as $categorys){
                              ?>
                                <option value="<?php echo $categorys->id;?>"<?php if ($categorys->id == $data->category_id){ ?>
            							       selected = "selected" <?php } ?>><?php echo $categorys->category_name;?></option>
                              <?php
                              }
                              ?>
                            </select>
                        </div>

			
			                   <div class="form-group" id="subcat" >
                            <label>SubCategory</label>
                            <select class="form-control"  style="width: 100%;" name="subcategory_id" id="subcategory_id">

                            <?php foreach($sub_cat as $subcategory){ ?>
  				                      <option value="<?php echo $subcategory->id;?>"<?php if ($subcategory->id == $data->subcategory_id){ ?>
  							                 selected = "selected" <?php } ?> > <?php echo $subcategory->sub_category;?></option> 
                            <?php } ?>
                           </select>
                          </div>
			
			
			

                        <!-- <div class="form-group"  id="subcategorydiv">
                          <label>Select Subcategory</label>
                            <select class="form-control get_cat_sub"  style="width: 100%;" name="subcategory_id" >
                              <option value="">Select</option>
                            </select>
                        </div>
 -->

                          <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Short Description</label>
                            <textarea class="form-control required" value="<?php echo $data->short_description; ?>"
                            required="" name="short_description"  placeholder="Short Description"><?php echo $data->short_description; ?></textarea>
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>

                          <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Description</label>
                            <textarea class="form-control required"  required="" name="description" value="<?php echo $data->description; ?>"  placeholder="Product Description"><?php echo $data->description; ?></textarea>
                            <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                              <label for="exampleInputEmail1">Product features</label>
                              <textarea class="form-control required" value="<?php echo $data->feature; ?>"  required="" name="feature"  placeholder="Product Features"><?php echo $data->feature; ?></textarea>
                              <span class="glyphicon  form-control-feedback"></span>
                          </div>

			                   <!-- 	<div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Short Description</label>
                            <input type="text" class="form-control required" 
                            required="" name="short_description"  value="<?php echo $data->short_description; ?>" placeholder="Short Description">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div> -->

				

				                  <!-- <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Description</label>
                            <input type="text" class="form-control required"  required="" name="description"  value="<?php echo $data->description; ?>">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div> -->

				                          <!-- <div class="form-group has-feedback">
                                    <label for="exampleInputEmail1">Product Features</label>
                                    <input type="text" class="form-control required"  required="" 
									                   name="feature" value="<?php echo $data->feature; ?>" placeholder="Product Features">
                                    <span class="glyphicon  form-control-feedback"></span>

                                    </div> -->
						</div>
            <div class="col-md-6">
		
				<div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Selling Price</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                             required="" name="price"  value="<?php echo $data->price; ?>">
                            <span class="glyphicon  form-control-feedback"></span>
                </div>


                
				<!-- <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Stitching Charge</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="0" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" 
							required="" name="stitching_charge" value="<?php echo $data->stitching_charge; ?>" placeholder="Stitching Charge">
                            <span class="glyphicon  form-control-feedback"></span>
                </div> -->
				<div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Market Price</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" 
							required="" name="mrp" value="<?php echo $data->mrp; ?>"  placeholder="Market Price">
                            <span class="glyphicon  form-control-feedback"></span>
                </div>
				<div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Dealer Price</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" 
							required="" name="dp"  value="<?php echo $data->dp; ?>" placeholder="Dealer Price">
                            <span class="glyphicon  form-control-feedback"></span>
                </div>
				<div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Actual Price</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" 
							required="" name="actual_price" value="<?php echo $data->actual_price; ?>" placeholder="Actual Price">
                            <span class="glyphicon  form-control-feedback"></span>
                </div>
              <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Offer</label>
                            <input type="text" class="form-control" data-parsley-trigger="change"
                            data-parsley-minlength="1" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" 
							required="" name="product_offer" value="<?php echo $data->product_offer; ?>" placeholder="Product Offer">
                            <span class="glyphicon  form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Gallery</label>
                            <input type="file" class="form-control" name="product_image[]" multiple/>
                        </div>


                <!-- <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Stitching Enable</label>
                             <input type="checkbox" id="myCheck" name="stitching_charge" value="1" onclick="enable()">
                            
                            <span class="glyphicon  form-control-feedback"></span>
                </div> -->

                          <div class="form-group has-feedback">
                             <div class="check_lft">
                                <input type="checkbox" id="myCheck"  name="stitching_charge" value="1" <?php if( $data->stitching_charge==1) echo 'checked="checked"'; ?> class="checkbox-custom">
                                <label for="myCheck" class="checkbox-custom-label">Stitching Enable</label>
                             </div>
                          </div>
                        </div>

              



              <div class="col-md-12">
              <div class="message_info" style="color: red"></div>
               <!-- /.box-body -->
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary" id="pdt_btn">Submit</button>
                  </div>
				      </div>
				   <div class="col-md-6">
				   </div>

				  </div>
               </form>
            </div>


            <script>

            function enable() {
              document.getElementById("myCheck").enabled = true;
                              }
            </script>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
