<div class="content-wrapper">

   <!-- Content Header (Page header) -->

   <section class="content-header">

      <h1>

          Manage Users

      </h1>

      <ol class="breadcrumb">

         <li><a href="<?php echo base_url();?>"><i class="fa fa-male"></i>Home</a></li>

         <li><a href="<?php echo base_url();?>user/create">Users</a></li>

         <li class="active"><a href="#">Create</a></li>

      </ol>

   </section>

   <!-- Main content -->

   <section class="content">

      <div class="row">

         <!-- left column -->

         <div class="col-md-12">

            <?php

               if($this->session->flashdata('message')) {

               $message = $this->session->flashdata('message');

               ?>

            <div class="alert alert-<?php echo $message['class']; ?>">

               <button class="close" data-dismiss="alert" type="button">×</button>

               <?php echo $message['message']; ?>

            </div>

            <?php

               }

               ?>

         </div>

         <div class="col-md-12">

            <!-- general form elements -->

            <div class="box">

               <div class="box-header with-border">

                  <h3 class="box-title">Create</h3>

               </div>

               <!-- /.box-header -->

               <!-- form start -->

               <form role="form" action="" method="post"  data-parsley-validate="" class="validate" enctype="multipart/form-data">		

                  <div class="box-body">



                     <div class="col-md-6">

					 

					               <div class="form-group has-feedback">

                            <label for="exampleInputEmail1">First Name</label>

                            <input type="text" class="form-control required input_length" data-parsley-trigger="change"	

                            data-parsley-minlength="2" data-parsley-maxlength="15"  required="" name="first_name"  placeholder="First name">

                            <span class="glyphicon  form-control-feedback"></span>

                          </div> 

						  

						              <div class="form-group has-feedback">

                            <label for="exampleInputEmail1">Last Name</label>

                            <input type="text" class="form-control required input_length"  data-parsley-trigger="change"	

                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="last_name"  placeholder="Last name">

                            <span class="glyphicon  form-control-feedback"></span>

                          </div> 

						   

						              <div class="form-group has-feedback">

                            <label for="exampleInputEmail1">Address</label>

                            <textarea class="form-control required input_length" name="address" data-parsley-trigger="change" required="" placeholder="Address"></textarea>

                            <span class="glyphicon  form-control-feedback"></span>

                          </div>

						  

						 

            						 <div class="form-group has-feedback">

            					    <label for="exampleInputEmail1">Email</label>

            					    <input type="email" class="form-control required input_length" name="email" data-parsley-trigger="keyup"   required="" placeholder="Email">

            					    <span class="glyphicon  form-control-feedback"></span>

            					    </div>



                          <div class="form-group has-feedback">

                          <label for="exampleInputEmail1">Mobile</label>

                          <input type="text" class="form-control required input_length" name="telephone" data-parsley-trigger="keyup" data-parsley-type="digits" data-parsley-minlength="10" data-parsley-maxlength="15" data-parsley-pattern="^[0-9]+$" required="" placeholder="Mobile">

                          <span class="glyphicon  form-control-feedback"></span>

                          </div>

            				

        						 </div>

                     <div class="col-md-6">



            						<div class="form-group has-feedback">

                              <label> Role</label>

                              <select class="form-control input_length"  style="width: 100%;" name="role_id" id="">

                              <option value="">Select role</option>

                               <?php

                              foreach($role as $role){                  

                               ?>

                                <option value="<?php echo $role->id;?>"><?php echo $role->rolename;?></option>

                               <?php

                              }

                               ?>

                               </select>

                          </div>

            						   

            						  <div class="form-group has-feedback">

                            <label for="exampleInputEmail1">Username</label>

                            <input type="text" class="form-control required input_length" name="username" data-parsley-trigger="change" required="" placeholder="username">

                            <span class="glyphicon  form-control-feedback"></span>

                          </div>



                          <div class="form-group has-feedback">

                            <label for="exampleInputEmail1">Password</label>

                            <input type="password" class="form-control required input_length" name="password" data-parsley-trigger="change" required="" placeholder="password">

                            <span class="glyphicon  form-control-feedback"></span>

                          </div>



                          <div class="form-group has-feedback">

                              <label> Status</label>

                              <select class="form-control input_length"  name="status" id="">

                              <option value="">Select status</option>

                              <option value="">Active</option>

                              <option value="">Inactive</option>

                               <?php

                              // foreach($role as $role){                  

                               ?>

                                <!-- <option value="<?php echo $role->id;?>"><?php echo $role->rolename;?></option> -->

                               <?php

                              // }

                               ?>

                               </select>

                          </div>

            						  

            						

            						  <!-- <div class="form-group ">

                            <label class="control-label" for="shopimage">Images</label>

                            <input type="file"  name="profile_picture" size="20" class="pic_file" />

                          </div> -->

            					</div>



            					 <div class="col-md-12">

                          <div class="box-footer">

                             <button type="submit" class="btn btn-primary">Submit</button>

                          </div>

                        </div>

            				   

            				  

            				   

				            </div><!-- Box body -->



               </form>

            </div>

            <!-- /.box -->

         </div>

      </div>

      <!-- /.row -->

   </section>

   <!-- /.content -->

</div>







  



		



