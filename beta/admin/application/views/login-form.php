<!DOCTYPE html>
<html>
   <head>

      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Admin | Log in</title>
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/AdminLTE.min.css">
      <link href="<?php echo base_url();?>assets/css/parsley/parsley.css" rel="stylesheet">
      <script src="<?php echo base_url();?>assets/js/parsley.min.js"></script>
      <style>
      input.parsley-error, select.parsley-error, textarea.parsley-error {
         width: 100%;
      }

      </style>
   </head>



   <body class="hold-transition login-page">
      <div class="login-box">
      <div class="login-logo">
         <a href="<?php echo base_url(); ?>">
         <img src="<?php echo base_url(); ?>assets/images/logo_scnd.png">
         </a>
      </div>
      <!-- /.login-logo -->
      <div class="login-box-body">
         <p class="login-box-msg"></p>
         <?php if(validation_errors()) { ?>
         <div class="alert alert-danger">
            <?php echo validation_errors(); ?>
         </div>
         <?php } ?>
         <form action="" method="post" data-parsley-validate="" class="validate">
            <div class="form-group has-feedback">
               <input type="text" class="form-control required" name="username" data-parsley-trigger="change" required="" placeholder="Username/Email ID">
               <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               <div class="error_div"  style="color:red"></div>
            </div>
            <div class="form-group has-feedback">
               <input type="password" class="form-control required" data-parsley-trigger="change" name="password" required="" placeholder="Password">
               <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
               <!-- /.col -->
               <div class="col-xs-12 right">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
               </div>
         </form>
         </div>
      </div>



   <script>
   base_url = "<?php echo base_url(); ?>";
   config_url = "<?php echo base_url(); ?>";
   </script>
   <!-- jQuery 2.1.4 -->
   <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
   <!-- Bootstrap 3.3.5 -->
   <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
   <!--[validation js]-->
   <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/parsley.min.js"></script>
    <!--[validation js]-->
   </body>
</html>