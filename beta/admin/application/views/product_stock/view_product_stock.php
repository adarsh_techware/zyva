<div class="content-wrapper">

 <!-- Content Header (Page header) -->

 

  <section class="content-header">

      <h1>

        Product Stock

      </h1><br>

      <div>

        <a href="<?php echo base_url(); ?>Product_stock/add_productstock"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a>

      </div>

      

      <ol class="breadcrumb">

         <li><a href="<?php echo base_url();?>"><i class="fa fa-male"></i>Home</a></li>

         <li><a href="<?php echo base_url();?>Product_stock/view_product_stock">Product Stock</a></li>

         <li class="active">View Product Stock</li>

      </ol>







  

</section>





 <!-- Main content -->

 <section class="content">

    <div class="row">

       <div class="col-xs-12">

          <?php

             if($this->session->flashdata('message')) {

                      $message = $this->session->flashdata('message');



                   ?>

          <div class="alert alert-<?php echo $message['class']; ?>">

             <button class="close" data-dismiss="alert" type="button">×</button>

             <?php echo $message['message']; ?>

          </div>

          <?php

             }

             ?>

       </div>

       <div class="col-xs-12">

          <!-- /.box -->

          <div class="box">

             <div class="box-header">

                <h3 class="box-title">View Product Stock</h3>

             </div>

             <!-- /.box-header -->

             <div class="box-body">

                <table id="" class="table table-bordered table-striped datatable">

                   <thead>

                      <tr>

                          <th class="hidden">ID</th>

                          <th>Name</th>

                          <th>Size</th>

                          <th>Color</th>

                          <th>Quantity</th>

                          <th>Status</th>

                          <th width="200px;">Action</th> 

                      </tr>

                   </thead>

                    <tbody>

                    <?php foreach($data as $stock) { ?>

                      <tr>

                        <td class="hidden"><?php echo $stock->id; ?></td>

                        <td class="center"><?php echo $stock->product_name; ?></td>

                        <td class="center"><?php echo $stock->size_type; ?></td>

                        <td class="center"><?php echo $stock->color_name; ?></td>

                        <td class="center"><?php echo $stock->quantity; ?></td>



                        <td>

                          <span class="center label <?php 

                            if($stock->status == '1')

                            {

                            echo "label-success";

                            }

                            else

                            {

                            echo "label-warning";

                            }

                            ?>">

            

                              <?php if($stock->status == '1')

                              {

                              echo "enable";

                              }

                              else

                              {

                              echo "disable";

                              }

                              ?>

                          </span>

                      </td>



                      <td class="center">

                        <a class="btn btn-sm btn-primary" href="<?php echo site_url('Product_stock/editpro_stock/'.$stock->id.'/'.$stock->color_id); ?>">

                        <i class="fa fa-fw fa-edit"></i>Edit</a>



                        <?php 

                        if( $stock->status){ ?>

                          <a class="btn btn-sm label-warning" href="<?php echo base_url();?>color_ctrl/color_status/<?php echo $stock->id; ?>">

                          <i class="fa fa-folder-open"></i> Disable </a>

                        <?php

                        }



                        else { ?>

                          <a class="btn btn-sm label-success" href="<?php echo base_url();?>color_ctrl/Color_active/<?php echo $stock->id; ?>">

                          <i class="fa fa-folder-o"></i> Enable </a>

                        <?php

                        }

                        ?>

                      </td>



                       



                    </tr>

                    <?php

                    }

                    ?>

                    </tbody>

                        

                   <tfoot>

                      <tr>

                           <th class="hidden">ID</th>

                           <th>Name</th>

                           <td>Size</td>

                           <td>Color</td>

                           <td>Quantity</td>

                           <td>Status</td>

                           <td width="200px;">Action</td>

                      </tr>

                   </tfoot>

                </table>

             </div>

             <!-- /.box-body -->

          </div>

          <!-- /.box -->

       </div>

       <!-- /.col -->

    </div>

    <!-- /.row -->

 </section>

 <!-- /.content -->

</div>

<div class="modal fade modal-wide" id="popup-subcatgetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"

 aria-hidden="true">

 <div class="modal-dialog">

    <div class="modal-content">

       <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

          <h4 class="modal-title">View stock  Details</h4>

       </div>

       <div class="modal-subcatbody">

       </div>

       <div class="business_info">

       </div>

       <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

       </div>

    </div>

    <!-- /.modal-content -->

 </div>

 <!-- /.modal-dialog -->

</div>

