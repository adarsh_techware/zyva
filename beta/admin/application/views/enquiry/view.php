<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <h1>
      Enquiry Details
    </h1>
    <ol class="breadcrumb">
       <li><a href="<?php echo base_url(); ?>"><i class=""></i>Home</a></li>
       <li class="active"><a href="<?php echo base_url(); ?>enquiry">Enquiry Details</a></li>
    </ol>
 </section>

 <!-- Main content -->
 <section class="content">
    <div class="row">
       <div class="col-xs-12">
          <?php
             if($this->session->flashdata('message')) {
                $message = $this->session->flashdata('message');?>
                <div class="alert alert-<?php echo $message['class']; ?>">
                   <button class="close" data-dismiss="alert" type="button">×</button>
                   <?php echo $message['message']; ?>
                </div>
          <?php
             }
             ?>
       </div>
       <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
             <div class="box-header">
                <h3 class="box-title">Enquiry Details</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body">
                <table id="" class="table table-bordered table-striped datatable">
                   <thead>
                      <tr>
                        <th class="hidden">ID</th>

                          <th>Name</th>
                          <th>Country</th>
                          <th>Email</th>
                          <th>Enquiry About</th>
                          <th>Product Code</th>
                          <th>Enquiry Message</th>
                          <th>Enquiry Date</th>
                      </tr>
                   </thead>
                   <tbody>
                     <?php
                         foreach($enquiry as $userdetail) {

                         ?>
                      <tr>
                         <td class="hidden"><?php echo $userdetail->id; ?></td> 
                         <td class="center"><?php echo $userdetail->first_name; ?> <?php echo $userdetail->last_name; ?></td>
                         <td class="center" ><?php echo $userdetail->country; ?></td>
                         <td class="center"><?php echo $userdetail->email; ?></td>
                         <td class="center"><?php echo $userdetail->name; ?></td>
                         <td class="center"><?php echo $userdetail->code; ?></td>
                         <td class="center"><?php echo $userdetail->enquiry; ?></td>
                         <td class="center" ><?php echo $userdetail->enquiry_date; ?></td>
                      </tr>
                      <?php
                         }
                         ?>
                   </tbody>
                   <tfoot>
                     <tr>
                        <th>Name</th>
                        <th>Country</th>
                        <th>Email</th>
                        <th>Enquiry About</th>
                        <th>Product Code</th>
                        <th>Enquiry Message</th>
                        <th>Enquiry Date</th>
                     </tr>
                   </tfoot>
                </table>
             </div>
             <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
       <!-- /.col -->
    </div>
    <!-- /.row -->
 </section>
 <!-- /.content -->
</div>
<div class="modal fade modal-wide" id="popup-customergetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
   aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">View customer Details</h4>
         </div>
         <div class="modal-customerbody">
         </div>
         <div class="business_info">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
