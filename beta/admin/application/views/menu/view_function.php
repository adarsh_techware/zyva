<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <h1>
      Sub function List</h1><br>
  <div>
  <a href="<?php echo base_url(); ?>main_menu/create_function"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a>
  </div>


    <ol class="breadcrumb">
       <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>
       <li><a href="<?php echo base_url();?>main_menu/view_function">Sub menu</a></li>
       <li class="active"><a href="<?php echo base_url();?>main_menu/view_function">View</a></li>
    </ol>
 </section>
 <!-- Main content -->
 <section class="content">
    <div class="row">
       <div class="col-xs-12">
          <?php
             if($this->session->flashdata('message')) {
                      $message = $this->session->flashdata('message');

                   ?>
          <div class="alert alert-<?php echo $message['class']; ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?php echo $message['message']; ?>
          </div>
          <?php
             }
             ?>
       </div>
       <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
             <div class="box-header">
                <h3 class="box-title">View Sub Menus</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body">
                <table id="" class="table table-bordered table-striped datatable">
                   <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Name</th>
                        <th>Main Menu</th>
                        <th>Path</th>
                        <th width="200px;">Action</th>
                      </tr>
                   </thead>
                   <tbody>
                      <?php
                         foreach($menus as $menu) {
                  //  $image=$cat->category_image;
                         ?>
                      <tr>
                         <td class="hidden"><?php echo $menu->id; ?></td>
                         <td class="center"><?php echo $menu->fun_name; ?></td>
                         <td class="center"><?php echo $menu->main_name;?></td>
                         <td><?php echo $menu->fun_path;?></td>
                            



                         <td class="center">
                            <!-- <a class="btn btn-sm bg-olive show-catgetdetails"  href="javascript:void(0);"  data-id="<?php echo $cat->id; ?>">
                            <i class="fa fa-fw fa-eye"></i> View </a> -->
                            <a class="btn btn-sm btn-primary" href="<?php echo site_url('main_menu/edit_function/'.$menu->id); ?>">
                            <i class="fa fa-fw fa-edit"></i>Edit</a>
                            <!-- <a class="btn btn-sm btn-danger" href="<?php echo site_url('Category/category_delete/'.$cat->id); ?>" onClick="return doconfirm()">
                            <i class="fa fa-fw fa-trash"></i>Delete</a> -->

                            
                            
                         </td>
                      </tr>
                      <?php
                         }
                         ?>
                   </tbody>
                   <tfoot>
                      <tr>
                         <th class="hidden">ID</th>
                         <th>Name</th>
                         <td>Main Menu</td>
                          <td>Path</td>
                         <td width="200px;">Action</td>
                      </tr>
                   </tfoot>
                </table>
             </div>
             <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
       <!-- /.col -->
    </div>
    <!-- /.row -->
 </section>
 <!-- /.content -->
</div>

