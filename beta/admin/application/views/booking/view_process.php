<style type="text/css">
  .color_pick_a {
    width: 25px;
    height: 25px;
    border-radius: 50%;
    text-align: center;
    left: 4px;
    top: 4px;
    cursor: pointer;
    margin-bottom: 10px;
    background-size: contain;
}
</style>
<div class="content-wrapper" style="font-size: 13px">

 <!-- Content Header (Page header) -->

 <section class="content-header">

    <h1>

      Process List

    </h1>

        <br><div>

<!-- <a href="<?php// echo base_url(); ?>user/add_sub_category"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a> -->

  </div>



    <ol class="breadcrumb">

       <li><a href="<?php echo base_url(); ?>"><i class=""></i>Home</a></li>

       <li class="active"><a href="<?php echo base_url(); ?>booking/process">Process</a></li>

    </ol>

 </section>

 <!-- Main content -->

 <section class="content">

    <div class="row">

       <div class="col-xs-12">

          <?php

             if($this->session->flashdata('message')) {

                      $message = $this->session->flashdata('message');



                   ?>

          <div class="alert alert-<?php echo $message['class']; ?>">

             <button class="close" data-dismiss="alert" type="button">×</button>

             <?php echo $message['message']; ?>

          </div>

          <?php

             }

             ?>

       </div>

       <div class="col-xs-12">

          <!-- /.box -->

          <div class="box">

             <div class="box-header">

                <h3 class="box-title">Processed Order List</h3>

             </div>

             <!-- /.box-header -->

          <div class="box-body">
            <table id="" class="table table-bordered table-striped datatable">
              <thead>
                  <tr>
                    <th class="hidden">ID</th>

                        <th>BookingID</th>

                        <th>Customer</th>

                        <th>Reseller</th>

                        <th>Date</th>

                        <th>DeliveryTo</th>

                        <th>PdtCode</th>

                        <th>Qty</th>

                        <th>Color</th>

                        <th>Size</th>

                        <th>Amout</th>

                        <th>Status</th>

                        <th>Payment</th>

                        <th>Action</th>

                      </tr>

                   </thead>

                   <tbody>

                     <?php

                         foreach($booking as $userdetail) { ?>

                      <tr>
                        <td class="hidden"><?php echo $userdetail->id; ?></td> 
                        <td class="center"><?php echo $userdetail->booking_no; ?></td>
                        <td class="center"><?php echo $userdetail->name; ?></td>
                        <td class="center"><?php echo get_reseller($userdetail->reseller_id); ?></td>
                        <td class="center"><?php echo date('d-m-Y h:i A',strtotime($userdetail->booked_date)); ?></td>
                        <td class="center"><?php echo get_address($userdetail->address_id); ?>
                        </td>
                        <td class="center" align="right"><?php echo $userdetail->product_code; ?>  </td>
                        <td class="center" align="right"><?php echo $userdetail->quantity; ?>  </td>
                        <td class="center" align="right"><div class="color_pick_a" style="background-image: url('<?php echo $userdetail->color; ?>')"></div></td>
                        <td class="center" align="right"><?php echo $userdetail->size; ?>  </td>
                        <td class="center" align="right">&#8377;<?php echo $userdetail->price; ?>  </td>

                        <td><?php echo $userdetail->pdt_status; ?></td>

                         <td class="center" align="right"><?php echo $userdetail->status; ?>  </td>
                        
                        

                       

                        

                        <td class="center">



                          <!-- <a href="<?php echo base_url('booking/view/'.$userdetail->id); ?>">view</a> -->

                          <a class="btn btn-sm bg-olive show-booking"  

                             id="booking_<?php echo $userdetail->id; ?>" href="javascript:void(0);"  

                             data-id="<?php echo $userdetail->id; ?>">

                            <i class="fa fa-fw fa-eye"></i> View 

                          </a>



                        </td>

                      </tr>

                      <?php

                         }

                         ?>

                   </tbody>

                   <tfoot>

                     <tr>

                        <th class="hidden">ID</th>

                        <th>BookingID</th>

                        <th>Customer</th>

                        <th>Reseller</th>

                        <th>Date</th>

                        <th>DeliveryTo</th>

                        <th>PdtCode</th>

                        <th>Qty</th>

                        <th>Color</th>

                        <th>Size</th>

                        <th>Amout</th>

                        <th>Status</th>

                        <th>Payment</th>

                        <th>Action</th>

                     </tr>

                   </tfoot>

                </table>

             </div>

             <!-- /.box-body -->

          </div>

          <!-- /.box -->

       </div>

       <!-- /.col -->

    </div>

    <!-- /.row -->

 </section>

 <!-- /.content -->

</div>







  <!-- Modal -->

 <div class="modal fade modal-wide" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"

   aria-hidden="true">

   <div class="modal-dialog">

      <div class="modal-content">

         <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">View Order Details</h4>

         </div>

         <div class="modal-productbody">

         </div>

         <div class="business_info">

         </div>

         <div class="modal-footer">

            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

         </div>

      </div>

      <!-- /.modal-content -->

   </div>

   <!-- /.modal-dialog -->

</div>

