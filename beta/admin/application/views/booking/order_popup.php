<style type="text/css">
  .color_pick_a {
    width: 25px;
    height: 25px;
    border-radius: 50%;
    text-align: center;
    left: 4px;
    top: 4px;
    cursor: pointer;
    margin-bottom: 10px;
    background-size: contain;
}
.clear{
  overflow: hidden;
  clear: both;
  height: 0px;
}
</style>

<div class="row">

  <div class="col-md-12">

    <div class="box box-primary">

      <div class="box-header with-border">

        <!-- <h5 class="box-title">View Order Details</h5> -->

        <div class="box-tools pull-right">

          <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">

            <i class="fa fa-minus"></i>

          </button>

        </div>

      </div>



    <div class="box-body">
    <div class="col-md-12">
      <div class="col-md-10">
      <h4>Order details</h4>
      <h5><span class="col-md-4">Booking ID:</span> <span class="col-md-6"><?php echo $order_info->booking_no; ?></span></h5><br/>

      <h5><span class="col-md-4">Order Date:</span><span class="col-md-6"><?php echo date('Y-m-d h:i A',strtotime($order_info->booked_date)); ?></span></h5><br/>

      <h5><span class="col-md-4">Customer:</span><span class="col-md-6"><?php echo $order_info->name; if($order_info->reseller_id!='') echo '&nbsp;&nbsp;('.get_reseller($order_info->reseller_id).')'; ?></span></h5><br/>

      <h5><span class="col-md-4">Amount:</span><span class="col-md-6">&#8377;<?php echo $order_info->price; ?></span></h5><br/> 

      <h5><span class="col-md-4">Status:</span><span class="col-md-6"><?php echo $order_info->pdt_status; ?></span></h5><br/>

      <h5><span class="col-md-4">Payment:</span><span class="col-md-6"><?php echo $order_info->status; ?></span></h5><br/>  

      <?php if($order_info->gift==1){?><h5><span class="col-md-4">Gift:</span><span class="col-md-6">Gift Packaging</span></h5><br/> <?php } ?>  

      <h5><span class="col-md-4">Medium:</span><span class="col-md-6"><?php echo $order_info->medium; ?></span></h5><br/>  

      </div>
      <br/>
      <div class="col-md-10">
        <h4>Product details</h4>
        <div class="col-md-4"><img src="<?php echo get_product_image($order_info->product_id); ?>" width="100px"></div>
        <div class="col-md-6">
          <h5><span class="col-md-6">Product Name:</span><span class="col-md-6"><?php echo $order_info->product_name;?></span></h5><br/>
          <h5><span class="col-md-6">Product Code:</span><span class="col-md-6"> <?php echo $order_info->product_code; ?></span></h5><br/>
          <h5><span class="col-md-6">Color:</span><span class="col-md-6"><div class="color_pick_a" style="background-image: url('<?php echo $order_info->color; ?>')"></div></span></h5><br/>
          <h5><span class="col-md-6">Size:</span><span class="col-md-6"> <?php echo $order_info->size; ?></span></h5><br/>
          <h5><span class="col-md-6">Quantity:</span><span class="col-md-6"> <?php echo $order_info->quantity; ?></span></h5><br/>
        </div>
      </div>
      <br/>
      <?php
      
      if($order_info->stitch_id>0){
        $stitching_info = get_stitch_info($order_info->stitch_id);
        
        $measurement = $stitching_info['measurement'];
      ?>
      <br/>
        <div class="col-md-10">
          <h4>Stitching details</h4>
            <h5><span class="col-md-4">Type:</span> <span class="col-md-6"><?php echo $stitching_info['meas_type'];?></span></h5><br/>
            <?php if($measurement){?>
            <h4 style="font-size: 16px;">Measurements</h4>
            <h5><b>Top Wear</b></h5>
            <h5><span class="col-md-4">Bust Size:</span> <span class="col-md-6"><?php echo $measurement->bustsize; ?></span></h5> <br/>
            <h5><span class="col-md-4">Waist Size:</span> <span class="col-md-6"> <?php echo $measurement->waistsize; ?></span></h5><br/>
            <h5><span class="col-md-4">Hip Size:</span> <span class="col-md-6"> <?php echo $measurement->waistsize; ?></span></h5><br/>

            <p><br/><b>Bottom Wear</b></p>
            <h5><span class="col-md-4">PantWaist:</span> <span class="col-md-6"> <?php echo $measurement->pantwaist; ?></span></h5><br/>
            <h5><span class="col-md-4">Hip:</span> <span class="col-md-6"> <?php echo $measurement->hip; ?></span></h5><br/>
            <h5><span class="col-md-4">Inseam:</span> <span class="col-md-6"> <?php echo $measurement->inseam; ?></span></h5><br/>
            <?php } ?>
 

            <h4 style="font-size: 16px;">Other Details</h4>
            <?php if($stitching_info['front']!=''){?><p class="clear"><h5><span class="col-md-4">Front:</span> <span class="col-md-6"><img src="<?php echo $stitching_info['front']->image;?>" width="50px" height="50px"></span></h5></p><?php } ?>
            <?php if($stitching_info['back']!=''){?><p class="clear"><h5><span class="col-md-4">Back:</span> <span class="col-md-6"><img src="<?php echo $stitching_info['back']->image;?>" width="50px" height="50px"></span></h5></p><?php } ?>
            <?php if($stitching_info['sleeve']!=''){?><p class="clear"><h5><span class="col-md-4">Sleeve:</span> <span class="col-md-6"><img src="<?php echo $stitching_info['sleeve']->image;?>" width="50px" height="50px"></span></h5></p><?php } ?>
           <?php if($stitching_info['hemline']!=''){?> <p class="clear"><h5><span class="col-md-4">Hemline:</span> <span class="col-md-6"><img src="<?php echo $stitching_info['hemline']->image;?>" width="50px" height="50px"></span></h5></p><?php } ?>
            <br/>
            <h4 style="font-size: 16px;" class="clear">Addon Details</h4>
            <?php if($stitching_info['add_ons']!=''){?><p class="clear"><h5><span class="col-md-4">Top Lining:</span> <span class="col-md-6"><img src="<?php echo $stitching_info['add_ons']->image;?>" width="50px" height="50px"></span></h5></p><?php } ?>
            <?php if($stitching_info['add_ons1']!=''){?><p class="clear"><h5><span class="col-md-4">Closing:</span> <span class="col-md-6"><img src="<?php echo $stitching_info['add_ons1']->image;?>" width="50px" height="50px"></span></h5></p><?php } ?>
            <?php if($stitching_info['add_ons2']!=''){?><p class="clear"><h5><span class="col-md-4">Placket:</span> <span class="col-md-6"><img src="<?php echo $stitching_info['add_ons2']->image;?>" width="50px" height="50px"></span></h5></p><?php } ?>
            <?php if($stitching_info['add_ons3']!=''){?><p class="clear"><h5><span class="col-md-4">Other:</span> <span class="col-md-6"><img src="<?php echo $stitching_info['add_ons3']->image;?>" width="50px" height="50px"></span></h5></p><br/><br/><?php } ?>
            <?php if($stitching_info['specialcase']!=''){?><h5><span class="col-md-4">Special Case:</span><span class="col-md-6"><?php echo $stitching_info['specialcase'];?></span></h5><?php } ?>
          </div>
        </div>
      <?php } ?>


    </div>
    <div class="box-footer" style="text-align: center;">
    
    <span class="message_info" style="color: red;display: none;text-align: center;"></span>
    <br/>
    <?php
    if($order_info->book_status=='1'){?>
      <button class="btn btn-info order_change" type="button" data-status="2">Process</button>&nbsp;&nbsp;<button class="btn btn-warning order_change" type="button" data-status="4">Cancel</button>
    <?php } else if($order_info->book_status=='2'){?>
      <button class="btn btn-info order_change" type="button" data-status="3">On Ship</button>
    <?php } else if($order_info->book_status=='4'){?>
      <button class="btn btn-info order_change" type="button" data-status="1">Re-Book</button>
    <?php } ?>
    <input type="hidden" id="order_id" name="order_id" value="<?php echo $order_info->id; ?>">
    </div>
    </div>

    </div><!-- /.box -->

  </div><!-- ./col -->

</div>  