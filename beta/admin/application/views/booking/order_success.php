<html>
	<head>
		<!-- <link href="http://192.168.138.31/TRAINEES/Nikhila/zyva_latest/assets/css/email_style.css" rel="stylesheet"> -->
	</head>
	<style>
		
	</style>
	<body>
		<div style="width:800px; margin:0  auto;">
			<div style=" width:100%; float:left; background-image: url('https://zyva.in/assets/images/bg_image.png'); background-repeat: no-repeat; background-position: top;">
				<div class="width_80" style="width:80%; margin: 0 auto;">
					<div class="logo" style="text-align:center">
						<img src="https://zyva.in/assets/images/logo.png">
					</div>
					<div class="email_container">
						<div class="menu_container" style="margin-top: 20px; width: 100%; border-top: 1px solid #dcdcdc; border-bottom: 1px solid #dcdcdc;">
							<!-- =================INCLUDE HEADER======================= -->
							<?php include 'header.php'; ?>
							<!-- =================INCLUDE HEADER======================= -->
						</div>

						<h2 style="color: #c25ba5; font-size: 32px; font-family: 'Roboto', sans-serif;font-weight: 400;">Order Booked Successfully</h2>
						<h4 style="color: #5b4180; font-size: 20px; font-family: 'Roboto', sans-serif;
						    font-weight: 100; font-style: italic; width: 100%;">
						    Your order has been placed successfully. Your order details 
						    <br>are as follows.
						</h4>
						<h3 style="color: #777; font-size: 19px; font-family: 'Roboto', sans-serif; 
						font-weight: 600; width: 100%;">
    						Order Id : <?php echo $order_id;?>
    					</h3>
						<div class="detail_cabin" style="width: 100%;float: left; 
							background-color: #f1f1f1; margin-bottom: 20px;">
							<div class="" style="padding:25px;">
								<h4 style="color: #5b4180; font-size: 20px;margin: 0px !important;
    								font-family: 'Roboto', sans-serif;font-weight: 100;width: 50% !important;
    								float: left; text-align: left;">
    								Total Items : <span style="font-weight:bold"><?php echo $total_item;?></span></h4>
								<h4 style="color: #5b4180; font-size: 20px;margin: 0px !important;
    								font-family: 'Roboto', sans-serif;font-weight: 100;width: 50% !important;
    								float: left; text-align: right;">
									Total  :<span style="font-weight:bold"> &#8377; <?php echo $total_rate;?></span></h4>
							</div>
							 <?php foreach($order as $order_info){?>	
							<div class="det_inner" style="width: 100%; padding: 35px 0px;float: left;">
								<div class="first_section" style="width: 40%; float: left; border-right: 1px solid #d6d6d6;">
									<div class="img_section" style="width: 50%; float: left;">
										<img src="https://zyva.in/<?php echo ('admin/'.$order_info->product_image.'_201x302.jpg');?>"
										style="width: 80px; height: 100px; border: 1px solid #e9e9e9; margin-left: 25px;">
									</div>
									<div class="det_section" style="width: 50%; float: left;">
										<h5 style="margin-top: 0px; padding-bottom: 6px; margin-bottom: 0px;
										    color: #808080; font-size: 18px; font-weight: 300;
										    font-family: 'Roboto', sans-serif; text-transform: inherit;">
										    Product Name
										</h5>
										<h6 style="font-size: 16px; font-weight: 600; color: #5a5a5a;
											font-family: 'Roboto', sans-serif;text-transform: uppercase; 
											margin-top: 15px;margin-bottom: 10px;">
											<?php echo $order_info->product_name;?>
										</h6>
										<h5 style="margin-top: 0px; padding-bottom: 6px; margin-bottom: 0px;
										    color: #808080; font-size: 18px; font-weight: 300;
										    font-family: 'Roboto', sans-serif; text-transform: inherit;">
										    <?php echo $order_info->product_code;?>
										</h5>
									</div>
								</div>
								<div class="first_section" style="width: 30%; float: left; border-right: 1px solid #d6d6d6;">
									<div class="qty_section" style="width: 50%; float: left;">
										<p style="margin-top:0px !important; font-size: 14px; font-weight: 300; 
											color: #5a5a5a; font-family: 'Roboto', sans-serif;margin-bottom: 25px;  padding-left: 40px;">
											Color
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px;  padding-left: 40px;">
											Size
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px;  padding-left: 40px;">
											Qty
										</p>
									</div>
									<div class="qty_section" style="width: 50%; float: left;">
										<p style="margin-top:0px !important">
											<img src="<?php echo $order_info->color_image;?>"
											style="width: 25px; height: 25px; object-fit: cover; border-radius: 50%;  margin-left: 33px;">
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px;padding-left: 10px;  padding-left: 40px;">
											<?php echo $order_info->size_type;?>
										</p>
										<p style="font-size: 14px; font-weight: 300; color: #5a5a5a;  
											font-family: 'Roboto', sans-serif;margin-bottom: 25px; padding-left: 10px;  padding-left: 40px;">
											<?php echo $order_info->quantity;?>
										</p>
									</div>
								</div>
								<div class="second_section" style="width: 29%; float: left">
									<div class="price_section" style="    font-size: 18px;
									    font-weight: 600; color: #5a5a5a; font-family: 'Roboto', sans-serif;
									    text-transform: uppercase; text-align: left; padding-left: 50px;">
									    &#8377; <?php echo $order_info->price;?>
									</div>
								</div>
							</div>
							<hr style="width: 96%; border-bottom: 1px solid #d6d6d6;border-top: none;">
							<?php } ?>
						</div>

						<div class="detail_cabin" style="width: 100%;float: left; 
							background-color: #f1f1f1; margin-bottom: 20px;">
							<div class="" style="padding:25px;">
								<h4  style="color: #5b4180;font-size: 22px; 
							    	font-family: 'Roboto', sans-serif;font-weight: 100; width:100%; float:left">
							    	Delivery Details
								</h4>
							</div>

							<h3 style="padding-left:25px; margin-top:0px; color: #777; font-size: 19px;
								font-family: 'Roboto', sans-serif; font-weight: 600;width: 100%; float: left;">
								<?php echo $delivery_name;?>
							</h3>
							<div class="address" style="width: 100%; padding: 25px;">
								<p style="margin-top: 0px; padding-bottom: 6px;margin-bottom: 0px;
								    color: #808080; font-size: 17px; font-weight: 100;line-height: 28px;
								    font-family: 'Roboto', sans-serif; text-transform: inherit;">
	    							<?php echo $address_1;?>,&nbsp;<?php echo $address_2;?> <br>
									<?php echo $state_id;?> &nbsp;-&nbsp;<?php echo $Zip;?><br>
									<?php echo $city_id;?></p>
									<p style="margin-top:15px; margin-bottom: 10px;
								    color: #808080; font-size: 17px; font-weight: 100;
								    font-family: 'Roboto', sans-serif; text-transform: inherit;">
								    Mobile: <?php echo $telephone;?></p>
							</div>
						</div>

						<div style="width:100%; float:left" >
							<h4 style="margin-bottom:10px; color: #5b4180; font-size: 20px; font-family: 'Roboto', sans-serif;font-weight: 100;font-style: italic;width: 100%;">Thank You!</h4>
							<h4 style="margin-top:0px; margin-bottom:50px; color: #5b4180; font-size:20px; font-family: 'Roboto', sans-serif;font-weight: 100;font-style: italic;width: 100%;">Zyva Team</h4>
						</div>
					</div>
				</div>
			</div>
			<!-- =================INCLUDE FOOTER======================= -->
			<?php include 'footer.php'; ?>
			<!-- =================INCLUDE FOOTER======================= -->
		</div>	
	</body>

<html>
