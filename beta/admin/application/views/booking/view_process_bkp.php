<div class="content-wrapper">

 <!-- Content Header (Page header) -->

 <section class="content-header">

    <h1>

      Order Processing

    </h1>

        <br><div>

<!-- <a href="<?php// echo base_url(); ?>user/add_sub_category"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a> -->

  </div>



    <ol class="breadcrumb">

       <li><a href="<?php echo base_url(); ?>"><i class=""></i>Home</a></li>

       <li class="active"><a href="<?php echo base_url(); ?>booking/process">Processing</a></li>

    </ol>

 </section>

 <!-- Main content -->

 <section class="content">

    <div class="row">

       <div class="col-xs-12">

          <?php

             if($this->session->flashdata('message')) {

                      $message = $this->session->flashdata('message');



                   ?>

          <div class="alert alert-<?php echo $message['class']; ?>">

             <button class="close" data-dismiss="alert" type="button">×</button>

             <?php echo $message['message']; ?>

          </div>

          <?php

             }

             ?>

       </div>

       <div class="col-xs-12">

          <!-- /.box -->

          <div class="box">

             <div class="box-header">

                <h3 class="box-title">Processed List</h3>

             </div>

             <!-- /.box-header -->

             <div class="box-body">

                <table id="" class="table table-bordered table-striped datatable">

                   <thead>

                      <tr>

                        <th class="hidden">ID</th>

                        <th>BookingID</th>

                        <th>Customer</th>

                        <th>Reseller</th>

                        <th>DeliveryTO</th>

                        <th>Total</th>

                        <th>Booked</th>

                        <th>Medium</th>

                        <th>Status</th>

                        <th>Payment</th>

                        <th>Action</th>

                      </tr>

                   </thead>

                   <tbody>

                     <?php

                         foreach($booking as $userdetail) {

                          //  print_r($user);die;



                         ?>

                      <tr>

                        <td class="hidden"><?php echo $userdetail->id; ?></td> 

                        <td class="center"><?php echo $userdetail->booking_no; ?></td>

                        <td class="center"><?php echo $userdetail->name; ?></td>

                        <td class="center"><?php echo get_reseller($userdetail->reseller_id); ?></td>

                        <td class="center">

                          

                          <?php echo $userdetail->address1; ?><br><?php echo $userdetail->address2; ?><br>

                          <?php echo $userdetail->telephone; ?><br>
                          <?php if($userdetail->email!= "") { echo $userdetail->email; ?><?php } ?>

                        </td>

                        <td class="center" align="right">&#8377;<?php echo $userdetail->total; ?>  </td>

                        <td class="center"><?php echo date('d-m-Y h:i A',strtotime($userdetail->date)); ?></td>

                        <td class="center"><?php echo $userdetail->medium; ?></td>

                         <td class="center">

                          <?php if($userdetail->status==1) {

                            echo "Booked";

                          }

                          else if($userdetail->status==2) {

                            echo "Processing";

                          }

                          else if($userdetail->status==3) {

                            echo "Delivered";

                          }

                          else {

                            echo "Cancelled";

                          }

                          ?>

                        </td>
                        <td class="center">

                          <?php if($userdetail->payment_status==1) {

                            echo "Paid";

                          } else {
                            echo "Not Paid";
                          }
                          

                          ?>

                        </td>

                        <td class="center">



                          <!-- <a href="<?php echo base_url('booking/view/'.$userdetail->id); ?>">view</a> -->

                          <a class="btn btn-sm bg-olive show-booking"  

                             id="booking_<?php echo $userdetail->id; ?>" href="javascript:void(0);"  

                             data-id="<?php echo $userdetail->id; ?>">

                            <i class="fa fa-fw fa-eye"></i> View 

                          </a>



                        </td>

                      </tr>

                      <?php

                         }

                         ?>

                   </tbody>

                   <tfoot>

                     <tr>

                        <th class="hidden">ID</th>

                        <th>BookingID</th>

                        <th>Customer</th>

                        <th>Reseller</th>

                        <th>DeliveryTo</th>

                        <th>Total</th>

                        <th>Booked</th>

                        <th>Medium</th>

                        <th>Status</th>

                        <th>Payment</th>

                        <th>Action</th>

                     </tr>

                   </tfoot>

                </table>

             </div>

             <!-- /.box-body -->

          </div>

          <!-- /.box -->

       </div>

       <!-- /.col -->

    </div>

    <!-- /.row -->

 </section>

 <!-- /.content -->

</div>







  <!-- Modal -->

 <div class="modal fade modal-wide" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"

   aria-hidden="true">

   <div class="modal-dialog">

      <div class="modal-content">

         <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <h4 class="modal-title">View Order Details</h4>

         </div>

         <div class="modal-productbody">

         </div>

         <div class="business_info">

         </div>

         <div class="modal-footer">

            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

         </div>

      </div>

      <!-- /.modal-content -->

   </div>

   <!-- /.modal-dialog -->

</div>

