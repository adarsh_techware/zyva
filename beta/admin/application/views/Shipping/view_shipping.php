



	<div class="content-wrapper" >

   <!-- Content Header (Page header) -->

   <section class="content-header">

      <h1>

        Charges

      </h1>

      <ol class="breadcrumb">

         <li><a href="<?php echo base_url();?>"><i class="fa fa-user-md"></i>Home</a></li>

         <li><a href="<?php echo base_url(); ?>shipping">Charges</a></li>

         <li class="active">View Charges</li>

      </ol>

   </section>

   <!-- Main content -->

   <section class="content">

        <div class="row">

         <div class="col-xs-12">

            <?php

               if($this->session->flashdata('message')) {

                        $message = $this->session->flashdata('message');



                     ?>

            <div class="alert alert-<?php echo $message['class']; ?>">

               <button class="close" data-dismiss="alert" type="button">×</button>

               <?php echo $message['message']; ?>

            </div>

            <?php

               }

               ?>

         </div>

         <div class="col-xs-12">



            <!-- /.box -->

            <div class="box">

	<div>

	<a href="<?php echo base_url(); ?>shipping/create"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a>

  </div>

               <div class="box-header">

                  <h3 class="box-title">Charges List</h3>

               </div>



               <!-- /.box-header -->

               <div class="box-body">

                  <table id="" class="table table-bordered table-striped datatable">

                     <thead>

                        <tr>

                           <th class="hidden">ID</th>

						         <th>Label</th>   

                           <th>Charge</th>

                           <th width="">Action</th>



                        </tr>

                     </thead>

                     <tbody>

                        <?php

						

                           foreach($data as $shipping) {



                           ?>

                        <tr>

                           <td class="hidden"><?php echo $shipping->id; ?></td>

                          <!--<td class="center" ><img width="100px" height="100px" src="<?php echo $product->product_image; ?>"></td> -->

                           <td class="center"><?php echo $shipping->label; ?></td>

                           <td class="center"><?php echo $shipping->charge; ?></td>





                          <!--  <td>

                              <span class="center label  <?php if($shipping->status == '1')

                        			{

                        			echo "label-success";

                        			}

                        			else

                        			{

                        			echo "label-warning";

                        			}

                        			?>"><?php if($shipping->status == '1')

                        			{

                        			echo "enable";

                        			}

                        			else

                        			{

                        			echo "disable";

                        			}

                        			?>

                              </span> 

                              </td>  -->

                              <td class="center"><a class="btn btn-sm btn-primary" href="<?php echo base_url();?>shipping/edit/<?php echo $shipping->id; ?>">

                                 <i class="fa fa-fw fa-edit"></i>Edit</a>

                              </td> 

                           </tr>

                        <?php

                           }

                           ?>

                     </tbody>

                     <tfoot>

                        <tr>

                           <th class="hidden">ID</th>

                           <th>Label</th>   

                           <th>Charge</th>

                           <th width="">Action</th>

                        </tr>

                     </tfoot>

                  </table>

               </div>

               <!-- /.box-body -->

            </div>

            <!-- /.box -->

         </div>

         <!-- /.col -->

      </div>

      <!-- /.row -->

   </section>

   <!-- /.content -->

</div>



