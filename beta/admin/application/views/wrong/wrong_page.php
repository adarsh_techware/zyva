<div class="content-wrapper">
  
   <section class="content">
      <div class="row">
         
         <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">Wrong Access</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <section class="content">
                    <div class="error-page">
                      <h2 class="headline text-yellow"> 404</h2>
                      <div class="error-content">
                        <h3><i class="fa fa-warning text-yellow"></i> Oops! Sorry, you don’t have access to this screen.</h3>
                        <p>
                          We could not find the page you were looking for.
                          Meanwhile, you may <a href="#" onclick="window.history.back();">return to Previous Page</a> or try using the search form.
                        </p>
                        <!-- <form class="search-form">
                          <div class="input-group">
                            <input type="text" name="search" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                              <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
                            </div>
                          </div>
                        </form> --><!-- /.input-group -->
                      </div><!-- /.error-content -->
                    </div><!-- /.error-page -->
                  </section><!-- /.content -->
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>