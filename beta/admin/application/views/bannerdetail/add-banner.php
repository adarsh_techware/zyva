<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Banner Details
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-male"></i>Home</a></li>
         <li><a href="#">Banner Details</a></li>
         <li class="active">Add Banner</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
               <!-- <div class="box-header with-border">
                  <h3 class="box-title">Add Banner Details</h3>
               </div> -->
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="" method="post"  data-parsley-validate="" class="validate" enctype="multipart/form-data">		
                  <div class="box-body">                 
                     <div class="col-md-6">
					 
					     
						  
						  <div class="form-group ">
									<label class="control-label" for="shopimage">Select Images</label>
									
									<input type="file"  name="banner_image" size="20" />
									<label >max size is 900kb </label>
                           </div>
						 
						<!--<div class="form-group has-feedback">
						   <label>Banner Image</label>
						   <input name="banner_image" accept="image/*" type="file" required="">
						
						   </div>	-->
						   
						   
						<div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Order</label>
                            <input type="text" class="form-control required" data-parsley-trigger="keyup" data-parsley-type="digits" 
							data-parsley-minlength="1" data-parsley-maxlength="15" data-parsley-pattern="^[0-9]+$" 	
                             required="" name="order"  placeholder="Order">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>
						  
						 
						  
											                 			   
					    
                  <!-- /.box-body -->
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
				   </div>
				   <div class="col-md-6">
				   

						  
						   

						
						
						
						
						  
						

                           			   
					    
				   </div>
				   
				  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>






		

