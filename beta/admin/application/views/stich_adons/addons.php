<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Stiching  adons
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class=""></i>Home</a></li>
         <li><a href="<?php echo base_url(); ?>Stich_adons/add_stiching_adons">Stiching adons detail</a></li>
         <li class="active">Add Stiching adons</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-warning">
               <div class="box-header with-border">
                  <h3 class="box-title">Add Stiching detail</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
             <form role="form" action="" method="post"  class="validate" enctype="multipart/form-data">

                  <div class="box-body">
                     <div class="col-md-6">
                        <!-- <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Stiching adons type</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="type"  placeholder="SubCategory Name">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div> -->

                <div class="form-group">
                          <label>Select type</label>
              <select class="form-control select2 required"  style="width: 100%;" id="bus_name" name="type">
              <option value="">Select Addons</option>
                   <?php
                    foreach($adons_type as $adonss){
                   ?>
                   <option value="<?php echo $adonss->id; ?>"><?php echo $adonss->adons_type; ?></option>
                   <?php
                   }
                   ?>
                            </select>
                        </div>


<script type="text/javascript">
function showDiv(select){
   if(select.value=='child'){
    document.getElementById('hidden_div').style.display = "block";
   } else{
    document.getElementById('hidden_div').style.display = "none";
   }
}
</script>


                    <div class="form-group ">
                           <label class="control-label" for="shopimage">Select Images</label>
                           <input type="file"  name="image" size="20" />
                                    </div>

                                     <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Enter discription</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="discription"  placeholder="discription">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                               <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Enter charge</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="charge"  placeholder="charge">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>


                   <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                        </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
