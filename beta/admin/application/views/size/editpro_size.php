<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Product Details
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-male"></i>Home</a></li>
         <li><a href="#">Product Details</a></li>
         <li class="active">Edit Product</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Edit Product Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="" method="post"  data-parsley-validate="" class="validate" enctype="multipart/form-data">
                  <div class="box-body">
                     <div class="col-md-12">



						  <div class="form-group">
                            <label>Select product</label>
                  <select class="form-control "  style="width: 100%;" name="product_id">

                   <?php
				  //print_r($product);
                  foreach($product as $pro){

                    //print_r($pro);die;

                   ?>
                <option value="<?php echo $pro->id;?>"<?php if ($pro->id == $data->product_id){ ?>
							selected = "selected" <?php } ?>><?php echo $pro->product_name;?></option>
                   <?php
                  }
                   ?>
                   </select>
            </div>


             <div class="form-group">
                          <label>Select size</label>
                <select class="form-control "  style="width: 100%;" name="size_id">

                 <?php
        //  print_r($size);die;
                 foreach($size as $pro1){

                ?>
          <option value="<?php echo $pro1->id;?>"<?php if ($pro1->id == $data->size_id){ ?>
            selected = "selected" <?php } ?>><?php echo $pro1->size_type;?></option>
                 <?php
               }
                 ?>
                 </select>
          </div>


<!-- /* multyselect   */ -->





<!-- /* multyselect   */ -->

                  <!-- /.box-body -->
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
				   </div>
				   <div class="col-md-6">






 </div>

				  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
