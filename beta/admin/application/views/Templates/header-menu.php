<?php
$userdata = $this->session->userdata('userdata');
?>
<header class="main-header">
   <a href="<?php echo base_url(); ?>" class="logo">
      <span class="logo-mini"><b>Z</b></span>
      <span class="hidden-xs">Zyva</span>
   </a>
   <!-- Header Navbar: style can be found in header.less -->
   <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only"> </span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
         <ul class="nav navbar-nav nav_bar_pad">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">                  
               <img src="<?php echo $userdata['profile_picture']; ?>" class="user-image profile_pic" alt="">
               <span class="hidden-xs">
               <?php
                  // echo $a['username'];?>
               </span>
               </a>
               <ul class="dropdown-menu">
                  <!-- User image -->
                  <?php
                     if($userdata) {
                     ?>
                  <li class="user-header">
                     <img src="<?php echo  $userdata['profile_picture']; ?>" class="img-circle" alt="">
                     <!-- <img src="<?php //echo base_url(); ?>assets/images/user2-160x160.jpg"  class="user-image" alt="User Image">-->
                  </li>
                  <?php } ?>
                  <!-- Menu Body -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                     <div class="pull-left">
                        <?php
                           $profile_link = ($userdata['user_type'] == 1) ? base_url()."admin_details/admin_profile" : base_url()."admin_details/user_profile";
                           ?>
                        <a href="<?php echo $profile_link; ?>" class="btn btn-default btn-flat">Profile</a>
                        <!--  <a href="<?php //echo base_url();?>Admin_detailsview/Admin_profile_view" class="btn btn-default btn-flat">Profile</a> -->
                     </div>
                     <div class="pull-right">
                        <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat">Sign out</a>
                     </div>
                  </li>
               </ul>
            </li>
         </ul>
      </div>
   </nav>
</header>