<?php
   $menu = $this->session->userdata('admin');
   
   ?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- <div class="user-panel">
         <div class="pull-left image">
         
         <img src="<?php echo  $this->session->userdata('profile_picture'); ?>" class="user-image" alt="">
         
         </div>
         
         <div class="pull-left info">
         
           <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
         
         
         
         </div>
         
         </div> -->
      <!-- search form -->
      <!-- <form method="post" class="sidebar-form">
         <div class="input-group">
         
           <input type="text" name="q" class="form-control" placeholder="Search...">
         
           <span class="input-group-btn">
         
             <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
         
           </span>
         
         </div>
         
         </form>-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
         <!-- <li class="header">MAIN NAVIGATION</li> -->
         <?php
            if( $menu==1  )
            
             {
            
            ?>
         <?php
            }
            
            ?>
         <li class="treeview">
            <a href="<?php echo base_url();?>user/create"><i class="fa fa-user" aria-hidden="true"></i><span>Users</span></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>user"><i class="fa fa-circle-o text-aqua"></i>View</a></li>
               <li><a href="<?php echo base_url();?>user/create"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
            </ul>
         </li>
         <li class="treeview">
         <li class="treeview">
            <a href="#"><i class="fa fa-user-md"></i> <span>Booking/Order</span><i class="fa fa-angle-left pull-right"></i></a>
            <!-- <a href="<?php echo base_url();?>booking"><i class="fa fa-user" aria-hidden="true"></i><span>Booking/Order</span></a> -->
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>booking"><i class="fa fa-circle-o text-aqua"></i>Booking</a></li>
               <li><a href="<?php echo base_url();?>booking/process"><i class="fa fa-circle-o text-aqua"></i>Processing</a></li>
               <li><a href="<?php echo base_url();?>booking/cancel"><i class="fa fa-circle-o text-aqua"></i>Cancelled</a></li>
               <li><a href="<?php echo base_url();?>booking/failed"><i class="fa fa-circle-o text-aqua"></i>Failed</a></li>
               <li><a href="<?php echo base_url();?>booking/all_order"><i class="fa fa-circle-o text-aqua"></i>All Order</a></li>
            </ul>
         </li>
         <!-- <li class="treeview"><a href="#"><i class="fa fa-user-md"></i> <span>Banner Details</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
            
            <li><a href="<?php echo base_url();?>Banner_ctrl/view_bannerdetails"><i class="fa fa-circle-o text-aqua"></i>View</a></li>
            
            <li><a href="<?php echo base_url();?>Banner_ctrl/add_banner"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
            
            
            
            </ul>
            
               </li> -->
         <li class="treeview">
            <a href="#"><i class="fa fa-user-md"></i> <span>Category</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>category"><i class="fa fa-circle-o text-aqua"></i>View</a></li>
               <li><a href="<?php echo base_url();?>Category/create"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class="fa fa-user-md"></i> <span>Sub Category</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>category/view_subcategory"><i class="fa fa-circle-o text-aqua"></i>View</a></li>
               <li><a href="<?php echo base_url();?>category/create_subcategory"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
            </ul>
         </li>
         <!-- <li class="treeview"><a href="#"><i class="fa fa-user-md"></i> <span>Brand</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
            
            <li><a href="<?php echo base_url();?>Brand_ctrl/view_brand"><i class="fa fa-circle-o text-aqua"></i>View Brand</a></li>
            
            <li><a href="<?php echo base_url();?>Brand_ctrl/add_brand"><i class="fa fa-circle-o text-aqua"></i>Add Brand</a></li>
            
            
            
            </ul>
            
            </li> -->
         <li class="treeview">
            <a href="#"><i class="fa fa-user-md"></i> <span>Products</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>product/view_product"><i class="fa fa-circle-o text-aqua"></i>View </a></li>
               <li><a href="<?php echo base_url();?>product/add_products"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
               <!--  <li><a href="<?php echo base_url();?>product/add_productgallery"><i class="fa fa-circle-o text-aqua"></i>Gallery</a></li> -->
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class="fa fa-user-md"></i> <span>Product Stock</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>Product_stock/add_productstock"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
               <li><a href="<?php echo base_url();?>Product_stock/view_product_stock"><i class="fa fa-circle-o text-aqua"></i>View </a></li>
            </ul>
         </li>
         <li class="treeview"><a href="<?php echo base_url();?>customer"><i class="fa fa-users" aria-hidden="true"></i> <span>Customers</span></a>
         </li>
         <li class="treeview">
            <a href="#"><i class="fa fa-user-md"></i> <span>Reseller</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>reseller"><i class="fa fa-circle-o text-aqua"></i>View </a></li>
               <li><a href="<?php echo base_url();?>reseller/create"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
               <li><a href="<?php echo base_url();?>reseller/booking"><i class="fa fa-circle-o text-aqua"></i>Booking</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class="fa fa-user-md"></i> <span>Delivery</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>Delevery/create"><i class="fa fa-circle-o text-aqua"></i>Create </a></li>
               <li><a href="<?php echo base_url();?>Delevery/view_location"><i class="fa fa-circle-o text-aqua"></i>View</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#"><i class="fa fa-user-md"></i> <span>Color</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>color_ctrl/add_color"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
               <li><a href="<?php echo base_url();?>color_ctrl/view_color"><i class="fa fa-circle-o text-aqua"></i>View</a></li>
            </ul>
         </li>
         <!-- <li class="treeview"><a href="#"><i class="fa fa-user-md"></i> <span>Manage Product Color</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
            
                 <li> <a href="<?php echo base_url();?>product/add_productcolor"><i class="fa fa-wrench" aria-hidden="true"></i><span>Add Product Color</span></a></li>
            
                 <li><a href="<?php echo base_url();?>product/view_color"><i class="fa fa-circle-o text-aqua"></i>view</a></li>
            
               </ul>
            
             </li> -->
         <li class="treeview">
            <a href="#"><i class="fa fa-user-md"></i> <span>Size</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>size_ctrl/add_size"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
               <li><a href="<?php echo base_url();?>size_ctrl/view_size"><i class="fa fa-circle-o text-aqua"></i>View </a></li>
            </ul>
         </li>
         <!-- <li class="treeview"><a href="#"><i class="fa fa-user-md"></i> <span>product size</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
            
            <li><a href="<?php echo base_url();?>size_ctrl/add_prosize"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
            
            <li><a href="<?php echo base_url();?>size_ctrl/view_prosize"><i class="fa fa-circle-o text-aqua"></i>view </a></li>
            
            
            
            </ul>
            
            </li> -->
         <li class="treeview">
            <a href="#"><i class="fa fa-user-md"></i> <span>Stiching</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>Stiching/add_stiching_types"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
               <li><a href="<?php echo base_url();?>Stiching/view_stich_types"><i class="fa fa-circle-o text-aqua"></i>View </a></li>
               <li><a href="<?php echo base_url();?>Stich_adons/add_stiching_adons"><i class="fa fa-circle-o text-aqua"></i>Create Ad Ones </a></li>
               <li><a href="<?php echo base_url();?>Stich_adons/view_stich_adons"><i class="fa fa-circle-o text-aqua"></i>View Ad Ones </a></li>
            </ul>
         </li>
         <!-- <li class="treeview"><a href="#"><i class="fa fa-user-md"></i> <span>promocode</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
            
            <li><a href="<?php echo base_url();?>promocode_ctrl/add_promocode"><i class="fa fa-circle-o text-aqua"></i>Add promocode</a></li>
            
            <li><a href="<?php echo base_url();?>promocode_ctrl/view_promocode"><i class="fa fa-circle-o text-aqua"></i>view promocode</a></li>
            
            
            
            </ul> -->
         <li class="treeview">
            <a href="#"><i class="fa fa-wrench"></i> <span>Charges</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
               <li><a href="<?php echo base_url();?>shipping/create"><i class="fa fa-circle-o text-aqua"></i>Create</a></li>
               <li><a href="<?php echo base_url();?>shipping"><i class="fa fa-circle-o text-aqua"></i>View </a></li>
            </ul>
         </li>
         <li class="treeview"><a href="<?php echo base_url();?>enquiry"><i class="fa fa-users" aria-hidden="true"></i> <span>Enquiry</span></a>
         </li>
         <!-- <li class="treeview"><a href="#"><i class="fa fa-user-md"></i> <span>stiching</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
            
            <li><a href="<?php echo base_url();?>promocode_ctrl/add_promocode"><i class="fa fa-circle-o text-aqua"></i>Add promocode</a></li>
            
            <li><a href="<?php echo base_url();?>Stiching/view_stich_types"><i class="fa fa-circle-o text-aqua"></i>view </a></li>
            
             <li><a href="<?php echo base_url();?>Stich_adons/add_stiching_adons"><i class="fa fa-circle-o text-aqua"></i>adons_create </a></li>
            
              <li><a href="<?php echo base_url();?>Stich_adons/view_stich_adons"><i class="fa fa-circle-o text-aqua"></i>ad_ons_view </a></li>
            
            </ul>
            
            </li> -->
         <li>
            <a href="<?php echo base_url(); ?>Settings_ctrl/view_settings"><i class="fa fa-wrench" aria-hidden="true"></i><span>Settings</span></a>
         </li>

         <li>
            <a href="<?php echo base_url(); ?>Transaction/view_settings"><i class="fa fa-wrench" aria-hidden="true"></i><span>Settings</span></a>
         </li>
      </ul>
   </section>
   <!-- /.sidebar -->
</aside>