<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <h1>
      View reseller </h1><br>
  <div>
  <a href="<?php echo base_url(); ?>Reseller/create"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a>
  </div>


    <ol class="breadcrumb">
       <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>
       <li><a href="<?php echo base_url();?>Reseller">Reseller</a></li>
       <li class="active"><a href="<?php echo base_url();?>Reseller">View reseller</a></li>
    </ol>
 </section>
 <!-- Main content -->
 <section class="content">
    <div class="row">
       <div class="col-xs-12">
          <?php
             if($this->session->flashdata('message')) {
                      $message = $this->session->flashdata('message');

                   ?>
          <div class="alert alert-<?php echo $message['class']; ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?php echo $message['message']; ?>
          </div>
          <?php
             }
             ?>
       </div>
       <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
             <div class="box-header">
                <h3 class="box-title">View Reseller</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body">
                <table id="" class="table table-bordered table-striped datatable">
                   <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Phone No</th>
                        <th>Address</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Status</th>
                        <th width="200px;">Action</th>
                      </tr>
                   </thead>
                   <tbody>
                      <?php
                         foreach($reseller as $res) {
                  //  $image=$cat->category_image;
                         ?>
                      <tr>
                         <td class="hidden"><?php echo $res->id; ?></td>
                         <td class="center"><?php echo $res->first_name; ?></td>
                         <td class="center"><?php echo $res->last_name; ?></td>
                         <td class="center"><?php echo $res->email; ?></td>

                         <td class="center"><?php echo $res->telephone; ?></td>
                         <td class="center"><?php echo $res->address; ?></td>

                         <td class="center"><?php echo $res->username; ?></td>
                          <td class="center"><?php echo $res->password; ?></td>


                         <!-- <td class="center"><img src="<?php echo $cat->category_image; ?>" width="200px"></td> -->
                         <td>
                          <span class="center label  <?php if($res->status == '1')
                                 {
                                 echo "label-success";
                                 }
                                 else
                                 {
                                 echo "label-warning";
                                 }
                                 ?>"><?php if($res->status == '1')
                                 {
                                 echo "enable";
                                 }
                                 else
                                 {
                                 echo "disable";
                                 }
                                 ?>
                            </span>
                            
                          </td>
                            



                         <td class="center">
                            <!-- <a class="btn btn-sm bg-olive show-catgetdetails"  href="javascript:void(0);"  data-id="<?php echo $cat->id; ?>">
                            <i class="fa fa-fw fa-eye"></i> View </a> -->
                            <a class="btn btn-sm btn-primary" href="<?php echo site_url('Reseller/edit/'.$res->id); ?>">
                            <i class="fa fa-fw fa-edit"></i>Edit</a>
                            <!-- <a class="btn btn-sm btn-danger" href="<?php //echo site_url('Category/category_delete/'.$cat->id); ?>" onClick="return doconfirm()">
                            <i class="fa fa-fw fa-trash"></i>Delete</a> -->

                            <?php if( $res->status){?>
                           <a class="btn btn-sm label-warning" href="<?php echo base_url();?>Reseller/Reseller_status/<?php echo $res->id; ?>">
                           <i class="fa fa-folder-open"></i> Disable </a>
                           <?php
                              }

                              else
                              {
                              ?>
                           <a class="btn btn-sm label-success" href="<?php echo base_url();?>Reseller/Reseller_active/<?php echo $res->id; ?>">
                           <i class="fa fa-folder-o"></i> Enable </a>
                           <?php
                              }
                              ?>
                            
                         </td>
                      </tr>
                      <?php
                         }
                         ?>
                   </tbody>
                   <tfoot>
                      <tr>
                         <th class="hidden">ID</th>
                         <th>First Name</th>
                         <th>Last Name</th>
                         <th>Email</th>
                         <th>Phone No</th>
                         <th>Address</th>
                         <th>Username</th>
                         <th>Password</th>
              <td>status</td>
             <td width="200px;">Action</td>
                      </tr>
                   </tfoot>
                </table>
             </div>
             <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
       <!-- /.col -->
    </div>
    <!-- /.row -->
 </section>
 <!-- /.content -->
</div>
<div class="modal fade modal-wide" id="popup-catModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
 aria-hidden="true">
 <div class="modal-dialog">
    <div class="modal-content">
       <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">View reseller Details</h4>
       </div>
       <div class="modal-catbody">
       </div>
       <div class="business_info">
       </div>
       <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
       </div>
    </div>
    <!-- /.modal-content -->
 </div>
 <!-- /.modal-dialog -->
</div>
