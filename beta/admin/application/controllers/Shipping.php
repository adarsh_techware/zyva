<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping extends CI_Controller {

	public function __construct() {
	parent::__construct();
		check_login();
		date_default_timezone_set("Asia/Kolkata");
		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);
		$this->load->model('Shipping_model');
		$this->load->library('upload');

    }

///////////////////////////////////
//////****PRODUCT DETAILS***///////
     public function index(){


			  $template['page'] = 'Shipping/view_shipping';
			  $template['page_title'] = "Shipping Cahrge";
			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
			  $template['data'] = $this->Shipping_model->get_details();
			  //var_dump($template['data']);
			  //die;
			  $this->load->view('template',$template);
	 }


//////////////////////////////////////////
//////*****ADD PRODUCT DETAILS****///////
	  public function create()
	  {
		
	    $template['page'] = 'Shipping/add-shipping';
		$template['page_title'] = 'Add Product';
		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
		$sessid=$this->session->userdata('logged_in');
		
		  if($_POST){		  
			  $data = $_POST;
			  
			       $result = $this->Shipping_model->add($data);
			
				if($result) {
					
					 $this->session->set_flashdata('message',array('message' => 'Added successfully','class' => 'success'));
				}
				else {
					
					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));  
				}
				}
			 
			  $this->load->view('template',$template);
		

    }

    //////////////////////////////////////////
	//////*****EDIT DETAILS****///////
	public function edit($id){
		$template['page'] = 'Shipping/edit_shipping';
		$template['page_title'] = "edit shipping";
		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
		$id = $this->uri->segment(3);

		$template['ship'] = $this->Shipping_model->edit_shipping($id);
					// print_r($template['Category1']);die;
		if(!empty($template['ship'])) {

			if($_POST){
		 		$data = $_POST;
			 	$result = $this->Shipping_model->update_shipping($id,$data);
				$this->session->set_flashdata('message',array('message' => 'Edit Shipping Details  Successfully','class' => 'success'));
			 	redirect(base_url().'shipping');
		 	}
		 }
		 else {

			$this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));
			redirect(base_url().'shipping');
		 }

		$this->load->view('template',$template);
	}


	

	
	


}
