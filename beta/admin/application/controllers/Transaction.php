<?php
class Transaction extends CI_Controller {
    public function __construct() {
        parent::__construct();
        check_login();
        
        $class      = $this->router->fetch_class();
        $method     = $this->router->fetch_method();
        $this->info = get_function($class, $method);
        
        $userdata = $this->session->userdata('userdata');
        
        $role_id = $userdata['user_type'];
        
        if (!privillage($class, $method, $role_id)) {
            redirect('wrong');
        }
        $this->perm = get_permit($role_id);
        $this->load->model('Transaction_model');
    }

    public function index() {
    	$template['page'] = 'Transaction/list_transaction';
    	$template['page_title'] = "Transaction";
		$template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
		$template['transaction_det'] = $this->Transaction_model->get_transaction();
		$this->load->view('template',$template);
    }
}
?>