<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Settings_ctrl extends CI_Controller {

	

	public function __construct(){

		parent:: __construct();	

			check_login();
		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

	      date_default_timezone_set("Asia/Kolkata");

		  

		  $this->load->model('Settings_model');		  

		  $this->load->library('image_lib');

		  

		  if(!$this->session->userdata('logged_in')) { 

			redirect(base_url());

		 }

		  

	}

	

	   

   //Add Settings 

    public function index()

	  {

		       $template['page'] = 'Settings/add-settings';

		       $template['page_title'] = 'Add Settings';

		       $template['perm'] = $this->perm;

        		$template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

			   $id = $this->session->userdata('logged_in')['id'];

			   

				if($_POST){

				$data = $_POST;

			    //array_walk($data, "remove_html");

				

			unset($data['submit']); 

			if(isset($_FILES['logo'])) {  

			$config = set_upload_logo('assets/uploads/logo');

			$this->load->library('upload');

			

			$new_name = time()."_".$_FILES["logo"]['name'];

			$config['file_name'] = $new_name;



			$this->upload->initialize($config);



			if ( ! $this->upload->do_upload('logo')) {

					unset($data['logo']);

				}

				else {

					$upload_data = $this->upload->data();

					//$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];

					$data['logo'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

				}

			}

			

			



			/* Save category details */

			$result = $this->Settings_model->update_settings($data);

			



			if($result) {

				/* Set success message */

				 $this->session->set_flashdata('message',array('message' => 'Add Settings Details Updated successfully','class' => 'success'));

			}

			else {

				/* Set error message */

				 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));  

			}

			}

					$resulttitles = $this->Settings_model->settings_viewing();

				

					$sessing_arrays = array(

					'title' => $resulttitles->title

					);

				  $this->session->set_userdata('title', $sessing_arrays);

				//redirect(base_url().'Business_information/add_Businessinformation');

				  $template['result'] = $this->Settings_model->settings_viewing(); 

				  $this->load->view('template',$template);

		    } 

		

}