<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Booking extends CI_Controller {
    public function __construct() {
        parent::__construct();
        check_login();
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        $this->info = get_function($class, $method);
        $userdata = $this->session->userdata('userdata');
        $role_id = $userdata['user_type'];
        if (!privillage($class, $method, $role_id)) {
            redirect('wrong');
        }
        $this->perm = get_permit($role_id);
        $this->load->model('Booking_model');
        $this->load->model('Reseller_model');
    }
    function index() {
        $template['page'] = 'booking/view_booking';
        $template['page_title'] = "Order List";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['booking'] = $this->Booking_model->booking_list();
        $this->load->view('template', $template);
    }
    function process() {
        $template['page'] = 'booking/view_process';
        $template['page_title'] = "Order Process";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['booking'] = $this->Booking_model->booking_process();
        $this->load->view('template', $template);
    }
    function cancel() {
        $template['page'] = 'booking/view_cancel';
        $template['page_title'] = "Cancel Orders";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['booking'] = $this->Booking_model->booking_cancel();
        $this->load->view('template', $template);
    }
    function failed() {
        $template['page'] = 'booking/view_failed';
        $template['page_title'] = "Failed Orders";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['booking'] = $this->Booking_model->view_failed();
        $this->load->view('template', $template);
    }
    function all_order() {
        $template['page'] = 'booking/view_all_order';
        $template['page_title'] = "All Orders";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['booking'] = $this->Booking_model->view_all_orders();
        $this->load->view('template', $template);
    }
    function view($bid) {
        $template['page'] = 'booking/view_order';
        $template['page_title'] = "View Order Details";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['booking'] = $this->Booking_model->view_orders($bid);
        $this->load->view('template', $template);
    }
    function order_popup() {
        $id = $_POST['booking_id'];
        $template['order_info'] = $this->Booking_model->order_info($id);
        //print_r($template['order_info']);
        $this->load->view('booking/order_popup', $template);
    }
    function failed_order_popup() {
        $id = $_POST['booking_id'];
        $template['order_info'] = $this->Booking_model->failed_order_popup($id);
        $this->load->view('booking/failed_order_popup', $template);
    }
    function order_change() {
        $order_id = $_POST['order_id'];
        $status = $_POST['status'];
        if ($status != 4) {
            $this->db->where('id', $order_id)->update('order_details', array('status' => $status));
        } else {
            $this->db->where('id', $order_id)->update('order_details', array('status' => $status, 'cancelled' => '1'));
        }
        $order_det = $this->Booking_model->get_order_info($order_id);
        if ($status == 2) {
            echo 'Order successfully Processed.';
            $message = "Processed: Your " . $order_det->product_code . " with booking id " . $order_det->booking_no . " has been processed and will be delivered soon.";
        } else if ($status == 3) {
            echo 'Order Delivered successfully.';
            $message = "Delivered: " . $order_det->product_code . " with booking id " . $order_det->booking_no . " from Zyva was delivered.";
        } else if ($status == 1) {
            echo 'Order re-booked successfully.';
            $message = "Order Received: We have received your order for " . $order_det->product_code . " with booking id " . $order_det->booking_no . " amounting to Rs." . $order_det->price . ".";
        } else {
            echo 'Order Cancelled successfully.';
            $message = "Cancelled: " . $order_det->product_code . " in your order with order id " . $order_det->booking_no . " has been cancelled.";
        }
        $mob_no = $order_det->telephone;
        $this->send_otp($message, $mob_no);
    }
    function book_change() {
        $booking_id = $_POST['booking_id'];
        $booking_no = $_POST['booking_no'];
        $status = $_POST['status'];
        $ref_code = $_POST['ref_code'];
        
        $this->db->where('id', $booking_id)->update('booking', array('status' => $status));
        $this->db->where('booking_no', $booking_no)->update('transaction', array('ref_code' => $ref_code, 'request_by' => 1));
        if ($status == 1) {
            //$this->db->where('booking_no',$booking_no)->update('transaction',array('status'=>'Success'));
            $template['Category'] = $this->Booking_model->category();
            $template['order'] = $this->Booking_model->order_details($booking_no);
            $template['booking_info'] = $this->Booking_model->booking_details($booking_no);
            $settings = get_icon();
            
            // print_r($template['booking_info']);die;
            $booking_info = $template['booking_info'];
            $template['order_id'] = $booking_info->booking_no;
            $template['total_item'] = $booking_info->total_item;
            $template['total_rate'] = $booking_info->total_rate;
            $template['delivery_name'] = $booking_info->delivery_name;
            $template['address_1'] = $booking_info->address_1;
            $template['address_2'] = $booking_info->address_2;
            $template['state_id'] = $booking_info->state_id;
            $template['city_id'] = $booking_info->city_id;
            $template['Zip'] = $booking_info->Zip;
            $template['telephone'] = $booking_info->telephone;

            $this->load->library('email');
            $config = Array('protocol' => 'smtp', 
		            'smtp_host' => $settings->smtp_host, //'smtp.techlabz.in',
		            'smtp_port' => 587, 
		            'smtp_user' => $settings->smtp_username, //'no-reply@techlabz.in', // change it to yours
		            'smtp_pass' => $settings->smtp_password, //'baAEanx7', // change it to yours
		            'smtp_timeout' => 20, 
		            'mailtype' => 'html', 
		            'charset' => 'iso-8859-1', 
		            'wordwrap' => TRUE);
            
            $this->email->initialize($config); // add this line
            $subject = $settings->sms_username . ' Order Success';
            $name = $settings->sms_username;
            $to = $settings->smtp_username;
            $mailTemplate = $this->load->view('booking/order_success', $template, true);
            //$this->email->set_newline("\r\n");
            $this->email->from($settings->admin_email, $name);
            //$this->email->to($to);
            $this->email->to('nikhila.techware@gmail.com');
            $this->email->subject($subject);
            $this->email->message($mailTemplate);
            $var = $this->email->send();
            if ($var == 1) {
                $result = array('status' => 'success', 'message' => 'Mail sent successfully');
            } else {
                $result = array('status' => 'error', 'message' => '');
            }
            echo 'Order Moved Booking.';
        } else {
            echo 'Booking Rejeced.';
        }
    }
    function send_otp($msg, $mob_no) {
        $sender_id = "TWSMSG"; // sender id
        $pwd = "968808"; //your SMS gatewayhub account password
        $str = trim(str_replace(" ", "%20", $msg));
        // to replace the space in message with  ‘%20’
        $url = "http://api.smsgatewayhub.com/smsapi/pushsms.aspx?user=nixon&pwd=" . $pwd . "&to=91" . $mob_no . "&sid=" . $sender_id . "&msg=" . $str . "&fl=0&gwid=2";
        // create a new cURL resource
        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // grab URL and pass it to the browser
        $result = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);
    }
}
?>
