<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Product_ctrl extends CI_Controller {



	public function __construct() {

	parent::__construct();

	check_login();

	$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);



		date_default_timezone_set("Asia/Kolkata");

		$this->load->model('Product_model');

		$this->load->library('upload');



    }



///////////////////////////////////

//////****PRODUCT DETAILS***///////

     public function view_product(){





			  $template['page'] = 'Productdetails/view-productdetails';

			  $template['page_title'] = "Product";

			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

			  $template['data'] = $this->Product_model->get_productdetails();

			  //var_dump($template['data']);

			  //die;

			  $this->load->view('template',$template);

	 }





//////////////////////////////////////////

//////*****ADD PRODUCT DETAILS****///////

	  public function add_products()

	  {

		

	    $template['page'] = 'Productdetails/add-product';

		$template['page_title'] = 'Add Product';

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$sessid=$this->session->userdata('logged_in');

		

		

		

		      if($_POST)

			{

		

		 $data = $_POST;

		

		

			   

		

			    $result = $this->Product_model->productdetails_add($data);

		

				if($result) 

				{

		

					 $this->session->set_flashdata('message',array('message' => 'Product added  successfully','class' => 'success'));

				}

				else

				{

		

					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

				}

			}

			  //}

		

			

				$template['category'] = $this->Product_model->gets_category();

		

				$template['sub_cat'] = $this->Product_model->gets_sub_category();

		

			    $this->load->view('template',$template);

     }



	//////////////////////////////////////////

//////***** EDIT PRODUCT DETAILS****///////



		 public function edit_product()

	{



		 $template['page'] = 'Productdetails/edit-product';

		      $template['page_title'] = 'Edit Product';

		      $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;



		      $id = $this->uri->segment(3);

		      $template['data'] = $this->Product_model->get_single_product($id);

			  //$template['data1'] = $this->Product_model->get_single_subimg($id);





			  if(!empty($template['data']))

			  {





		      if($_POST){

			  $data = $_POST;



			  

		$result = $this->Product_model->productdetails_edit($id,$data);



					if($result)

				{



					 $this->session->set_flashdata('message',array('message' => 'Edit product Details  successfully','class' => 'success'));

					redirect(base_url().'Product_ctrl/view_product');

				}

			  }

			  }

				else {



					 $this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));

					redirect(base_url().'Product_ctrl/view_product');

				}

		

		$template['category'] = $this->Product_model->gets_category();

		$template['sub_cat'] = $this->Product_model->gets_sub_category();

		//var_dump($template['sub_cat']);

		//die;

					  $this->load->view('template',$template);









	}



	



//////////////////////////////////////////

//////***** PRODUCT STATUS DETAILS****///////

	  public function product_status(){



				  $data1 = array(

				  "status" => '0'

							 );

				  $id = $this->uri->segment(3);

				  $s=$this->Product_model->update_product_status($id, $data1);

				  $this->session->set_flashdata('message', array('message' => 'Product Successfully Disabled','class' => 'warning'));

				  redirect(base_url().'Product_ctrl/view_product');

	    }

//////////////////////////////////////////

//////*****ACTIVE  PRODUCT DETAILS****///////

		public function product_active(){



				  $data1 = array(

				  "status" => '1'

							 );

				  $id = $this->uri->segment(3);

				  $s=$this->Product_model->update_product_status($id, $data1);

				  $this->session->set_flashdata('message', array('message' => 'Product Successfully Enabled','class' => 'success'));

				  redirect(base_url().'Product_ctrl/view_product');

	    }



	  //////////////////////////////////////////

//////*****DELETE PRODUCT DETAILS****///////

	     public function delete_product(){



		  $id = $this->uri->segment(3);

		  $result= $this->Product_model->product_delete($id);

		  $this->session->set_flashdata('message', array('message' => 'Deleted Successfully','class' => 'success'));

	      redirect(base_url().'Product_ctrl/view_product');

	    }

	 //////////////////////////////////////////

//////*****POPUP PRODUCT DETAILS****///////





     public function product_viewpopup()

	 {

		  $id=$_POST['productdetailsval'];

		  $template['data'] = $this->Product_model->view_popup_product($id);

		  $template['gallery'] = $this->Product_model->get_gallery($id);

		  //var_dump($template['data']);

		 // die;

		  $this->load->view('Productdetails/product-popup-view',$template);

	 }



	  public function category()

	{

		 $id=$_POST['id'];

	     $subcategory = $this->Product_model->gets_subcategory($id);

          

		 foreach($subcategory as $sub_category)

		 {

			 echo "<option value=".$sub_category->id.">".$sub_category->sub_category."</option>";

		 }

	}







	/*uplod*/







	/* public function add_product()

	{

		$template['page'] = 'Productdetails/add-product';

		$template['page_title'] = 'Add Product';

		$sessid=$this->session->userdata('logged_in');

		if($_POST){



			//$str_cat=explode(",",$_POST['category']);

			$upload_conf = set_upload_option();

			$this->upload->initialize( $upload_conf );

			$this->load->library('upload');

			if(!empty($_FILES['product_image']['name'])){

				if (!$this->upload->do_upload('product_image')){

				//var_dump($this->upload->display_errors());

				}

			$upload_data = $this->upload->data();

			$_POST['product_image']=base_url().$upload_conf['upload_path'].'/'.$upload_data['file_name'];

			}

			$this->db->insert('product',$_POST);







			$id= $this->db->insert_id();



			if(isset($_FILES['product_images'])){

			foreach($_FILES['product_images'] as $key=>$val)

			{

				$i = 1;

				foreach($val as $v)

				{

					$field_name = "file_".$i;

					$_FILES[$field_name][$key] = $v;

					$i++;

				}

			}

			// Unset the useless one

			unset($_FILES['product_images']);

			unset($_FILES['product_image']);

			// Put each errors and upload data to an array

			$error = array();

			$success = array();



			// main action to upload each file

			foreach($_FILES as $field_name => $file)

			{

				if ( ! $this->upload->do_upload($field_name))

				{

					// if upload fail, grab error

					$error['upload'][] = $this->upload->display_errors();

				}

				else

				{

					// otherwise, put the upload datas here.

					// if you want to use database, put insert query in this loop

					$upload_data = $this->upload->data();

					$path=base_url().$upload_conf['upload_path'].'/'.$upload_data['file_name'];

					$res= array('product_id'=>$id,'subimg'=>$path);

					$this->db->insert('subimg',$res);

					// otherwise, put each upload data to an array.

					$success[] = $upload_data;

				}

			}

			// see what we get

			if(count($error > 0))

			{

				$data['error'] = $error;

			}

			else

			{

				$data['success'] = $upload_data;

			}

		}

        $this->session->set_flashdata('item',array('message' => 'Updated Successfully','class' => 'success'));



	    // After that you need to used redirect function instead of load view such as

		}

		$template['brand'] = $this->Product_model->gets_brand();

		$template['category'] = $this->Product_model->gets_category();

		$template['color'] = $this->Product_model->get_color();

		$template['size'] = $this->Product_model->get_size();



      // var_dump($template['color']);die;

		 $this->load->view('template',$template);

	}*/

	//



  public function view_subimages()

  {

	    $template['page'] = 'Productdetails/view-subimages';

	    $template['page_title'] = "Product";

	    $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

	    $id = $this->uri->segment(3);

		//$template['data'] = $this->Product_model->get_single_subimg($id);

		 $this->load->view('template',$template);



  }

   public function add_productcolor()

	{

	   



	    $template['page'] = 'Productdetails/product-color';

		$template['page_title'] = 'Add Product';

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$sessid=$this->session->userdata('logged_in');

		

		

		

		      if($_POST)

			{

		

		 $data = $_POST;

		

		

			    $result = $this->Product_model->productcolor_add($data);

		

				if($result) 

				{

		

					 $this->session->set_flashdata('message',array('message' => 'Product added  successfully','class' => 'success'));

				}

				else

				{

		

					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

				}

			}

			

		

				$template['product'] = $this->Product_model->gets_product();

				$template['color'] = $this->Product_model->get_color();

			    $this->load->view('template',$template);

			

				

			

	}





	 public function view_color()

	{

		 $template['page'] = 'Productdetails/view-productcolor';

			  $template['page_title'] = "Product";

			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

			  $template['data'] = $this->Product_model->get_productcolordetails();

			  //var_dump($template['data']);

			 // die;

			  $this->load->view('template',$template);

	}

	

	

	

	

	

	//////////////////////////////////////////

//////***** PRODUCT COLOR STATUS DETAILS****///////

	  public function productcolor_status(){



				  $data1 = array(

				  "status" => '0'

							 );

				  $id = $this->uri->segment(3);

				  $s=$this->Product_model->update_productcolor_status($id, $data1);

				  $this->session->set_flashdata('message', array('message' => 'Product color Successfully Disabled','class' => 'warning'));

				  redirect(base_url().'Product_ctrl/view_color');

	    }

//////////////////////////////////////////

//////*****ACTIVE  PRODUCT color DETAILS****///////

		public function productcolor_active(){



				  $data1 = array(

				  "status" => '1'

							 );

				  $id = $this->uri->segment(3);

				  $s=$this->Product_model->update_productcolor_status($id, $data1);

				  $this->session->set_flashdata('message', array('message' => 'Product color Successfully Enabled','class' => 'success'));

				  redirect(base_url().'Product_ctrl/view_color');

	    }



	

//////////////////////////////////////////

//////*****EDIT  PRODUCT color DETAILS****///////	

	

	public function edit_productcolor()

	{

			   $template['page'] = 'Productdetails/edit-productcolor';

			   $template['page_title'] = "Product";

			   $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

			   

             

		      $id = $this->uri->segment(3); 

		   $template['data1'] = $this->Product_model->get_single_productcolor($id);

		    if($_POST){

			  $data = $_POST;

				

				

				

			  

				$result = $this->Product_model->productcolordetails_edit($data, $id);

				if($result) {

					

					 $this->session->set_flashdata('message',array('message' => 'Edit Product Color Details Updated successfully','class' => 'success'));

					  redirect(base_url().'Product_ctrl/view_color');

				}

				else {

					

					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error')); 

					 redirect(base_url().'Product_ctrl/view_color');

				}

				}	

				

				

						

			

			        $template['product'] = $this->Product_model->gets_productcolor($id);

					//print_r($template['product']);

					//die;

					$template['color'] = $this->Product_model->gets_color($id);

					$this->load->view('template',$template);



	}

	/////////////product gallery//////////

		public function add_productgallery()

		{



			   $template['page'] = 'Productdetails/add-productgallery';

			   $template['page_title'] = "Product Gallery"; 

			   $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

			   $template['product'] = $this->Product_model->gets_product();

			   

		 if($_POST){

			  $data = $_POST;





				// print_r($result);

				// die;

			$config = set_upload_product('./uploads/');

		 	$this->load->library('upload');



		 	$new_name = time()."_".$_FILES["product_image"]['name'];

		 	$config['file_name'] = $new_name;



		 	$this->upload->initialize($config);



		 	if ( ! $this->upload->do_upload('product_image')) {



		 		$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));



		 	}

		 	else

		 	{



		 	$upload_data = $this->upload->data();

		 	$data['product_image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];



		 			

					$result = $this->Product_model->add_productgallery($data);





		 		if($result) {



		 			 $this->session->set_flashdata('message',array('message' => 'Upload Product Images Details successfully','class' => 'success'));

		 		}

		 		else {

		 			 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

		 		}

		 		}

		 		}

			   

			   

			   

			   

			   

			   

			   

			   $this->load->view('template',$template);

	    }		 

	    

	

	public function view_gallery()

	{

					$id=$_POST['gallery'];



					$template['data'] = $this->Product_model->get_gallery($id);

					$this->load->view('Productdetails/productgallery-popup-view',$template);

					

	}



	public function remove_image(){

		$id = $_POST['id'];

		$this->Product_model->remove_image($id);

		print json_encode(array('status'=>'success'));

	}

	

	





}

