	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reseller extends CI_Controller {

	public function __construct() {
	parent::__construct();
	check_login();
		$this->load->model('Reseller_model');
		$this->load->library('upload');

    }

///////////////////////////////////
//////****PRODUCT DETAILS***///////
     public function index(){


			  $template['page'] = 'reseller/reseller_view';
			  $template['page_title'] = "Reseller";
			  $template['reseller'] = $this->Reseller_model->get_reseller();
			  
			  $this->load->view('template',$template);
	 }


//////////////////////////////////////////
//////*****ADD PRODUCT DETAILS****///////
	  public function create()
	  {
		
	    $template['page'] = 'reseller/reseller_create';
		$template['page_title'] = 'Create Reseller';
		   if($_POST){
			  $data = $_POST;

			   //print_r($data);die;

		     $result = $this->Reseller_model->reseller_add($data);
					


		 		if($result) {

		 			 $this->session->set_flashdata('message',array('message' => 'Add reseller Details successfully','class' => 'success'));
		 		}
		 		else {
		 			 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));
		 		}
		 		}
		
	   $this->load->view('template',$template);
     }




     public function Reseller_status(){

 				  $data1 = array(
 				  "status" => '0'
 							 );
 				  $id = $this->uri->segment(3);
 				  $s=$this->Reseller_model->update_reseller_status($id, $data1);
 				  $this->session->set_flashdata('message', array('message' => 'Reseller Disabled Successfully','class' => 'warning'));
 				  redirect(base_url().'Reseller');
 	    }

 		public function Reseller_active(){

 				  $data1 = array(
 				  "status" => '1'
 							 );
 				  $id = $this->uri->segment(3);
 				  $s=$this->Reseller_model->update_reseller_status($id, $data1);
 				  $this->session->set_flashdata('message', array('message' => 'Reseller Enabled Successfully','class' => 'success'));
 				  redirect(base_url().'Reseller');
 	    }



 	    public function edit(){
		
		$template['page'] = 'reseller/reseller_edit';
		$template['page_title'] = "edit reseller";
		$id = $this->uri->segment(3);
		$template['reseller1'] = $this->Reseller_model->edit_reseller($id);
		// print_r($template['Category1']);die;
		if(!empty($template['reseller1'])) {
			if($_POST){
				$data = $_POST;
								$result = $this->Reseller_model->update_reseller($id,$data);
				redirect(base_url().'Reseller');
			}
	 	}
	 	else
		{
			$this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));
			redirect(base_url().'Reseller');
	 	}
		$this->load->view('template',$template);
}



	
}
