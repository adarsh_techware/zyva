<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Wrong extends CI_Controller {
	public function __construct() {
	    parent::__construct();
	    $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);
	}

	public function index(){
		$template['page'] = 'wrong/wrong_page';
        
        $template['page_title'] = "Unauthorised Access";

        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
        
        $this->load->view('template', $template);
	}
}