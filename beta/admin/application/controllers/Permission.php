<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Permission extends CI_Controller {



	public function __construct() {

		parent::__construct();

		check_login();

		date_default_timezone_set("Asia/Kolkata");

		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);
        $userdata = $this->session->userdata('userdata');        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		//$this->load->model('Permission_model');
    }



    public function index($id=null){

    	if($id==null){
    		redirect(base_url('role'));
    	}

			$template['page'] = 'permission/permission_page';

			$template['page_title'] = "View Permission";

			$template['perm'] = $this->perm;

        	$template['main'] = $this->info->menu_name;

        	$template['sub'] = $this->info->fun_menu;

			$module = $this->db->order_by('main_name','ASC')->get('main_menu')->result();
			$function = array();
			foreach ($module as $rs) {
				$label = $rs->main_control;
				$function[$label] = $this->db->where('main_id',$rs->id)->order_by('fun_name','ASC')->get('function_menu')->result();
			}
			$template['module'] = $module;
			$template['function'] = $function;
			$count_module = count($module);
			$row = ceil($count_module/3);
			$template['row'] = $row;

			//$id = 1;

			$template['permission'] =  $this->db->select('function_id')->where('role_id',$id)->get('permissions')->result();

		$result = array();
			foreach ($template['permission'] as $rs) {

				$permission = explode(',', $rs->function_id);
				$result = array_merge($result, $permission);
			}

			$template['permission'] = $result;

			$template['role_id']=$id;

			$this->load->view('template',$template);



	}

	public function perm_assign(){
		$string='';
		foreach ($_POST as $key => $value) {
			if($key!='role_id'){
				
			$module = $this->db->where('main_control',$key)->get('main_menu')->row();

			$value_id = $module->id;

			if($string!=''){
				$string = $string.",".$value_id;
			} else {
				$string = $value_id;
			}

				$this->db->where('main_id',$module->id);
				$this->db->where('role_id',$_POST['role_id']);
				$query = $this->db->get('permissions');

				$value = implode(',',$_POST[$key]);

				$data = array('role_id'=>$_POST['role_id'],
							  'main_id'=>$module->id,
							  'function_id'=>$value);

				
				
				if($query->num_rows()==0){
					$this->db->insert('permissions',$data);					
				} else {
					$this->db->where('main_id',$module->id);
					$this->db->where('role_id',$_POST['role_id']);
					$this->db->update('permissions',$data);					
					
				}

				

				
			}
		}



		$this->db->query("DELETE FROM permissions WHERE main_id NOT IN ($string) AND role_id=".$_POST['role_id']);
		$this->session->set_flashdata('message',array('message' => 'Permission set successfully','class' => 'success'));
		redirect(base_url('role'));
		
	}



  }

  ?>

