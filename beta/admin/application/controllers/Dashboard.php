<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Dashboard_model');
        check_login();
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        $this->info = get_function($class, $method);
        $userdata = $this->session->userdata('userdata');
        $role_id = $userdata['user_type'];
        if (!privillage($class, $method, $role_id)) {
            redirect('wrong');
        }
        $this->perm = get_permit($role_id);
    }
    public function index() {
        $template['page'] = 'dashboard';
        $template['page_title'] = "Dashboard";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['earning'] = $this->Dashboard_model->get_total_earning();
        $template['product'] = $this->Dashboard_model->get_total_products();
        $template['sales'] = $this->Dashboard_model->get_total_sales();
        $template['booking'] = $this->Dashboard_model->get_bookings_count();
        $this->load->view('template', $template);
    }
    function bulk_update() {
        $result = $this->db->select('id')->get('product')->result();
        foreach ($result as $rs) {
            $array = array('product_id' => $rs->id, 'size_id' => '7', 'color_id' => '5', 'quantity' => '10');
            $result_array[] = $array;
        }
        $this->db->insert_batch('product_quantity', $result_array);
    }
    function image_generate() {
        ini_set("max_execution_time", 0);
        $this->load->helper('general');
        $dir_dest = 'assets/uploads/';
        $dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $dir_dest);
        upload_image();
        $result = $this->db->select('product_gallery.id,product_gallery.product_id,product_gallery.product_image,product.product_code')->from('product_gallery')->join('product', 'product.id = product_gallery.product_id')->get()->result();
        $i = 0;
        $product_id = 0;
        foreach ($result as $rs) {
            if ($product_id != $rs->product_id) {
                $product_id = $rs->product_id;
                $i = 0;
            }
            ++$i;
            $img_name = str_replace('http://beta.zyva.in/admin/', '', $rs->product_image);
            //$img_name = 'http://beta.zyva.in/admin/assets/uploads/1492427976_11491913172906-Moda-Rapido-Women-Blue-Embroidered-Straight-Kurta-4501491913172781-4.jpg';
            $img_name = str_replace('http://beta.zyva.in/admin/', '', $img_name);
            list($name, $ext) = explode('.', $img_name);
            $ext = '.' . $ext;
            ini_set("max_execution_time", 0);
            $dir_dest = 'assets/uploads/';
            $dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $dir_dest);
            $main_filename = $rs->product_code . '_' . $i;
            $sub_filename = $rs->product_code . '_' . $i . '_201x302';
            $similar_filename = $rs->product_code . '_' . $i . '_400x600';
            $small_filename = $rs->product_code . '_' . $i . '_95x143';
            $handle = new Upload($img_name);
            $handle->image_watermark = "assets/WATERMARK_vertical.png";
            if ($handle->uploaded) {
                $handle->Process($dir_dest, $main_filename);
                if ($handle->processed) {
                    echo $product_id;
                    $handle->image_resize = true;
                    $handle->image_y = 302;
                    $handle->image_x = 201;
                    $handle->image_watermark = "assets/WATERMARK_vertical_201x302.png";
                    $handle->Process($dir_dest, $sub_filename);
                    $handle->image_resize = true;
                    $handle->image_y = 600;
                    $handle->image_x = 400;
                    $handle->image_watermark = "assets/WATERMARK_vertical_400x600.png";
                    $handle->Process($dir_dest, $similar_filename);
                    $handle->image_resize = true;
                    $handle->image_y = 143;
                    $handle->image_x = 95;
                    $handle->Process($dir_dest, $small_filename);
                    $this->db->where('id', $rs->id)->update('product_gallery', array('product_image' => $dir_dest . $main_filename, 'format' => $ext));
                }
            } else {
                echo 'failed';
            }
        }
        /*$result = $this->db->get('product_gallery')->result();
        foreach ($result as $rs) {
        str_replace('http://beta.zyva.in/admin/', '', $rs->product_image);
        
        
        
        http://beta.zyva.in/admin/assets/uploads/1492427976_11491913172906-Moda-Rapido-Women-Blue-Embroidered-Straight-Kurta-4501491913172781-4.jpg
        }
        
        $img_name = 'http://beta.zyva.in/admin/assets/uploads/1494833005_url-m-d-s-collection-original-imaefyeknwd4v27q.jpeg';
        $img_name = str_replace('http://beta.zyva.in/admin/', '', $img_name);
        
        ini_set("max_execution_time",0);
        $dir_dest = 'assets/uploads/';
        $dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $dir_dest);
        $filename = 'zyv092';
        $this->load->helper('general');
        upload_image();
        
        ini_set("max_execution_time",0);
        $handle = new Upload($img_name);
        
           $handle->image_watermark       = "assets/WATERMARK.png";
        if ($handle->uploaded) {
         $handle->Process($dir_dest,$filename);
         if ($handle->processed) {
             echo '<p class="result">';
             echo '  <b>File uploaded with success</b><br />';
             echo '  File: <a href="'.base_url($dir_pics.'/' . $handle->file_dst_name) . '">' . $handle->file_dst_name . '</a>';
             echo '   (' . round(filesize($handle->file_dst_pathname)/256)/4 . 'KB)';
             echo '</p>';
             $filename = 'zyv092_300x201';
             $handle->image_resize          = true;
          $handle->image_y               = 300;
          $handle->image_x               = 201;
           $handle->image_watermark       = "assets/WATERMARK.png";
        	$handle->image_watermark_position = 'R';
          	$handle->Process($dir_dest,$filename);
        
          	$handle->image_resize          = true;
         	$handle->image_y               = 600;
          	$handle->image_x               = 400;
          	$filename = 'zyv092_400x600';
          	$handle->image_watermark       = "assets/WATERMARK.png";
        	$handle->image_watermark_position = 'R';
          	$handle->Process($dir_dest,$filename);
        
          	$handle->image_resize          = true;
          	$handle->image_y               = 95;
          	$handle->image_x               = 95;
          	$filename = 'zyv092_95x95';
          	$handle->image_watermark       = "assets/WATERMARK.png";
        	$handle->image_watermark_position = 'R';
         	$handle->Process($dir_dest,$filename);
        
          
        
        
         } else {
             // one error occured
             echo '<p class="result">';
             echo '  <b>File not uploaded to the wanted location</b><br />';
             echo '  Error: ' . $handle->error . '';
             echo '</p>';
         }
        
         // we delete the temporary files
         $handle-> Clean();
        
        } else {
         // if we're here, the upload file failed for some reasons
         // i.e. the server didn't receive the file
         echo '<p class="result">';
         echo '  <b>File not uploaded on the server</b><br />';
         echo '  Error: ' . $handle->error . '';
         echo '</p>';
        }*/
    }
}