<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reseller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        check_login();
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        $this->info = get_function($class, $method);
        $userdata = $this->session->userdata('userdata');
        //print_r($this->session->userdata());
        $role_id = $userdata['user_type'];
        if (!privillage($class, $method, $role_id)) {
            redirect('wrong');
        }
        $this->perm = get_permit($role_id);
        $this->load->model('Reseller_model');
        $this->load->library('upload');
    }

    // ***************************************VIEW RESELLER DETAILS
    public function index() {
        $template['page'] = 'reseller/reseller_view';
        $template['page_title'] = "Reseller";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['reseller'] = $this->Reseller_model->get_reseller();
        $template['sale_details'] = $this->Reseller_model->get_sale_details();
        //$template['cust_name'] = $this->Reseller_model->get_customername();
        $this->load->view('template', $template);
    }

    // ***************************************ADD RESELLER DETAILS
    public function create() {
        $template['page'] = 'reseller/reseller_create';
        $template['page_title'] = 'Create Reseller';
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        if ($_POST) {
            $data = $_POST;
            //print_r($data);die;
            $result = $this->Reseller_model->reseller_add($data);
            if ($result) {
                $this->session->set_flashdata('message', array('message' => 'Add reseller Details successfully', 'class' => 'success'));
            } else {
                $this->session->set_flashdata('message', array('message' => 'Error', 'class' => 'error'));
            }
        }
        $this->load->view('template', $template);
    }
    public function success_mail() {
        $email_content = array('username' => 'asdasdsa', 'password' => 'asdasdsad', 'email_id' => 'adarsh.techware@gmail.com', 'name' => 'sdfsdfsdfsdf');
        $result = $this->Reseller_model->success_mail($email_content);
    }
    public function Reseller_status() {
        $data1 = array("status" => '0');
        $id = $this->uri->segment(3);
        $s = $this->Reseller_model->update_reseller_status($id, $data1);
        $this->session->set_flashdata('message', array('message' => 'Reseller Disabled Successfully', 'class' => 'warning'));
        redirect(base_url() . 'Reseller');
    }
    public function Reseller_active() {
        $data1 = array("status" => '1');
        $id = $this->uri->segment(3);
        $s = $this->Reseller_model->update_reseller_status($id, $data1);
        $this->session->set_flashdata('message', array('message' => 'Reseller Enabled Successfully', 'class' => 'success'));
        redirect(base_url() . 'Reseller');
    }
    public function edit() {
        $template['page'] = 'reseller/reseller_edit';
        $template['page_title'] = "edit reseller";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $id = $this->uri->segment(3);
        $template['reseller1'] = $this->Reseller_model->edit_reseller($id);
        // print_r($template['Category1']);die;
        if (!empty($template['reseller1'])) {
            if ($_POST) {
                $data = $_POST;
                $result = $this->Reseller_model->update_reseller($id, $data);
                redirect(base_url() . 'Reseller');
            }
        } else {
            $this->session->set_flashdata('message', array('message' => "You don't have permission to access.", 'class' => 'danger'));
            redirect(base_url() . 'Reseller');
        }
        $this->load->view('template', $template);
    }
    public function booking() {
        $template['page'] = 'reseller/view_booking';
        $template['page_title'] = "View Booking Details";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['booking'] = $this->Reseller_model->view_booking();
        $this->load->view('template', $template);
    }
    function view($bid) {
        //print_r($bid);
        $template['page'] = 'reseller/view_order';
        $template['page_title'] = "View Order Details";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['booking'] = $this->Reseller_model->view_orders($bid);
        $this->load->view('template', $template);
    }
    function order_popup() {
        $id = $_POST['booking_id'];
        $template['order_info'] = $this->Reseller_model->view_order($id);
        //echo '<pre>';
        //print_r($template['order_info']);
        //die();
        // var_dump($template['userdetail']);
        // die;
        $this->load->view('reseller/order_popup', $template);
    }
    public function reseller_viewpopup() {
        $id = $_POST['reseller'];
        $template['reseller'] = $this->Reseller_model->edit_reseller($id);
        //print_r($template['reseller']);
        $this->load->view('reseller/reseller_viewpopup', $template);
    }
    public function reseller_approve() {
        $id = $_POST['reseller'];
        $rs = $this->Reseller_model->reseller_approve($id);
        if ($rs) {
            echo 1;
        } else {
            echo 0;
        }
    }
    public function reseller_reject() {
        $id = $_POST['reseller'];
        $rs = $this->Reseller_model->reseller_reject($id);
        if ($rs) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function reseller_paid() {
        $data= array(
            $reseller_id    => $_POST['reseller_id'],
            $reference_id   => $_POST['reference_id'],
            $booking_no     => $_POST['booking_no'],
            $date           => date('Y-m-d H:i:s'),
        );
        print_r($data);die;
        $rs= $Reseller_model->insert_reseller_paid($data);
        if($status==1){
            echo 'Order Moved Booking.';
        } else {
            echo 'Booking Rejeced.';
        }
    }
    public function payment() {
        $template['page'] = 'reseller/reseller_payment';
        $template['page_title'] = "Reseller Payment";
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $id = $this->uri->segment(3);
        if ($_POST) {
            $data = $_POST;
            $data['reseller_id'] = $id;
            $data['paid_date'] = date('Y-m-d H:i:s');
            // print_r($data);die;
            $result = $this->Reseller_model->add_payment($data);
            if ($result) {
                $this->session->set_flashdata('message', array('message' => 'Add Reseller Payment Details Successfully', 'class' => 'success'));
            } else {
                $this->session->set_flashdata('message', array('message' => 'Error', 'class' => 'error'));
            }
            redirect(base_url() . 'reseller');
        }
        $this->load->view('template', $template);
    }
}