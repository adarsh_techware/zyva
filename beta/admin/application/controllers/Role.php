<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Role extends CI_Controller
{

    
    public function __construct()
    {
        
        parent::__construct();
        
        check_login();
        
        $class      = $this->router->fetch_class();
        $method     = $this->router->fetch_method();
        $this->info = get_function($class, $method);
        
        $userdata = $this->session->userdata('userdata');
        
        $role_id = $userdata['user_type'];
        
        if (!privillage($class, $method, $role_id)) {
            redirect('wrong');
        }
        $this->perm = get_permit($role_id);
        
        $this->load->model('role_model');
        
        
    }
    
    
    
    public function index()
    {
        
        $template['page'] = 'Role/view_role';
        
        $template['page_title'] = "Roles";
        
        $template['perm'] = $this->perm;
        
        $template['main'] = $this->info->menu_name;
        
        $template['sub'] = $this->info->fun_menu;
        
        $template['data'] = $this->role_model->get_role();
        
        $this->load->view('template', $template);
        
    }
    
    
    
    public function create()
    {
        
        
        
        $template['page'] = 'Role/create-role';
        
        $template['page_title'] = 'Create Role';
        
        $template['perm'] = $this->perm;
        
        $template['main'] = $this->info->menu_name;
        
        $template['sub'] = $this->info->fun_menu;
        
        $sessid = $this->session->userdata('logged_in');
        
        if ($_POST) {
            
            $data = $_POST;
            
            /*$config = set_upload_category('../uploads/');
            
            $this->load->library('upload');
            
            $new_name = time() . "_" . $_FILES["profile_picture"]['name'];
            
            $config['file_name'] = $new_name;
            
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('profile_picture')) {
                
                $this->session->set_flashdata('message', array(
                    'message' => "Display Picture : " . $this->upload->display_errors(),
                    'title' => 'Error !',
                    'class' => 'danger'
                ));
                
            }
            
            else {
                
                $upload_data = $this->upload->data();
                
                $data['profile_picture'] = base_url() . $config['upload_path'] . "/" . $upload_data['file_name'];*/
                
                $result = $this->role_model->create_new($data);
                
                if ($result) {
                    
                    
                    
                    $this->session->set_flashdata('message', array(
                        'message' => 'New Role created successfully',
                        'class' => 'success'
                    ));
                    
                }
                
                else {
                    
                    $this->session->set_flashdata('message', array(
                        'message' => 'Error',
                        'class' => 'error'
                    ));
                    
                }
                
            //}
            
        }
        
        $template['role'] = $this->role_model->get_role();
        
        
        
        $this->load->view('template', $template);
        
        
        
    }

    public function edit_role($id=null)
    {
    	if($id==null){
    		redirect(base_url('role'));
    	}

    	$template['page'] = 'Role/role_edit';
        
        $template['page_title'] = 'Edit Role';
        
        $template['perm'] = $this->perm;

        $template['id'] = $id;
        
        $template['main'] = $this->info->menu_name;

        $template['role'] = $this->role_model->get_role();
        
        $template['sub'] = $this->info->fun_menu;

    	$rs = $this->db->where('id',$id)->get('role')->row();
    	
    	if(!empty($rs)){
    		$template['data'] = $rs;
    		$this->load->view('template',$template);
    	} else {
    		redirect(base_url('role'));
    	}
    }

    public function update_role($id=null){
    	$data = $_POST;
    	$this->db->where('id',$id)->update('role',$data);
    	$this->session->set_flashdata('message', array(
                        'message' => 'Role modified successfully',
                        'class' => 'success'
                    ));
    		redirect(base_url('role'));
    }
    
    
    
}