<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Admin_details extends CI_Controller {



    private $userdata = null;

    

    public function __construct() {

        parent::__construct();

        check_login();

        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

        $this->load->model('Admin_model');

        $this->load->model('Users_model');

        $this->userdata = $this->session->userdata('userdata');

    }

    

    //Change Admin Password	

    public function admin_password() {

        

        $template['page']       = 'Admindetails/View-admin-profile';

        $template['page_title'] = "View Admin profile";

        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

        $id = $this->userdata['id'];

        //$change_admin=$this->session->userdata('logged_in_admin');

        // echo $change_admin['id'];

        

        if (isset($_POST) and !empty($_POST)) {

            if (isset($_POST['reset_pwd'])) {

                $data = $_POST;

                

                array_walk($data, "remove_html");

                

                if ($data['n_password'] !== $data['c_password']) {

                    $this->session->set_flashdata('message', array(

                        'message' => 'Password doesn\'t match',

                        'title' => 'Error !',

                        'class' => 'danger'

                    ));

                    redirect(base_url() . 'Admin_detailsview/Admin_profile_view');

                } else {

                    unset($data['c_password']);

                    $result = $this->Admin_model->update_admin_passwords($data, $id);

                    if ($result) {

                        if ($result === "notexist") {

                            $this->session->set_flashdata('message', array(

                                'message' => 'Invalid Password',

                                'title' => 'Warning !',

                                'class' => 'warning'

                            ));

                            redirect(base_url() . 'Admin_detailsview/Admin_profile_view');

                        } else {

                            $this->session->set_flashdata('message', array(

                                'message' => 'Password updated successfully',

                                'title' => 'Success !',

                                'class' => 'success'

                            ));

                            redirect(base_url() . 'Admin_detailsview/Admin_profile_view');

                        }

                    } else {

                        $this->session->set_flashdata('message', array(

                            'message' => 'Sorry, Error Occurred',

                            'title' => 'Error !',

                            'class' => 'danger'

                        ));

                        redirect(base_url() . 'Admin_detailsview/Admin_profile_view');

                    }

                    

                }

            }

        }

        $this->load->view('template', $template);

    }

    

    //PROFILE VIEW

    public function admin_profile()

    {

        

        $template['page']       = 'Admindetails/View-admin-profile';

        $template['page_title'] = "View Admin profile";

        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

       

        $id = $this->userdata['id'];

        

        

        if (isset($_FILES['profile_pic'])) {

            $config = set_upload_profilepic('assets/uploads/profile_pic/admin');

            

            $this->load->library('upload');

            

            $new_name            = time() . "_" . $_FILES["profile_pic"]['name'];

            $config['file_name'] = $new_name;

            

            $this->upload->initialize($config);

            

            if (!$this->upload->do_upload('profile_pic')) {

                unset($data['profile_pic']);

            } else {

                $upload_data             = $this->upload->data();

                $data['username'] = $this->userdata['username'];

                $data['profile_picture'] = base_url() . $config['upload_path'] . "/" . $upload_data['file_name'];

                if($id == $this->userdata['id']) {

                $this->session->set_userdata('profile_pic',$data['profile_picture']);

                

                }

            }

            

            

            $result = $this->Admin_model->update_admin_profile($data, $id);

            

        }

        

        

        $template['data'] = $this->Admin_model->get_admin_profile_details($id);

        

        

        //$template['datas'] = $this->Admin_model->get_title($id);

        $this->load->view('template', $template);

    }

    

    

    

    

    

    ////view other users//

    public function user_profile()

    {

        

        $template['page']       = 'Users/view_user_profile';

        $template['page_title'] = "View user profile";

        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

        $id = $this->userdata['id'];

        // $id = $this->session->userdata('user_id');

        //print_r($id);exit;

        

        

        if (isset($_FILES['profile_pic'])) {

            $config = set_upload_profilepic('assets/uploads/profile_pic/admin');

            

            $this->load->library('upload');

            

            $new_name            = time() . "_" . $_FILES["profile_pic"]['name'];

            $config['file_name'] = $new_name;

            

            $this->upload->initialize($config);

            

            if (!$this->upload->do_upload('profile_pic')) {

                unset($data['profile_pic']);

            } else {

                $upload_data             = $this->upload->data();

                $data['username'] = $this->userdata['username'];

                $data['profile_picture'] = base_url() . $config['upload_path'] . "/" . $upload_data['file_name'];

                if($id == $this->userdata['id']) {

                $this->session->set_userdata('profile_pic',$data['profile_picture']);

                

                }

            }

            

            

            $result = $this->Users_model->update_admin_profile($data, $id);

            

        }

        

        

        $template['data'] = $this->Users_model->get_admin_profile_details($id);

        

        

        //$template['datas'] = $this->Admin_model->get_title($id);

        $this->load->view('template', $template);

    }

    

    

    

    

    ////change user password//

    //Change Admin Password	

    public function user_password()

    {

        

        $template['page']       = 'Users/view_user_profile';

        $template['page_title'] = "View user profile";

        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

        $id = $this->userdata['id'];

        //$change_admin=$this->session->userdata('logged_in_admin');

        // echo $change_admin['id'];

        

        if (isset($_POST) and !empty($_POST)) {

            if (isset($_POST['reset_pwd'])) {

                $data = $_POST;

                

                array_walk($data, "remove_html");

                

                if ($data['n_password'] !== $data['c_password']) {

                    $this->session->set_flashdata('message', array(

                        'message' => 'Password doesn\'t match',

                        'title' => 'Error !',

                        'class' => 'danger'

                    ));

                    redirect(base_url() . 'Admin_detailsview/profile_view_users');

                } else {

                    unset($data['c_password']);

                    $result = $this->Users_model->update_admin_passwords($data, $id);

                    if ($result) {

                        if ($result === "notexist") {

                            $this->session->set_flashdata('message', array(

                                'message' => 'Invalid Password',

                                'title' => 'Warning !',

                                'class' => 'warning'

                            ));

                            redirect(base_url() . 'Admin_detailsview/profile_view_users');

                        } else {

                            $this->session->set_flashdata('message', array(

                                'message' => 'Password updated successfully',

                                'title' => 'Success !',

                                'class' => 'success'

                            ));

                            redirect(base_url() . 'Admin_detailsview/profile_view_users');

                        }

                    } else {

                        $this->session->set_flashdata('message', array(

                            'message' => 'Sorry, Error Occurred',

                            'title' => 'Error !',

                            'class' => 'danger'

                        ));

                        redirect(base_url() . 'Admin_detailsview/profile_view_users');

                    }

                    

                }

            }

        }

        $this->load->view('template', $template);

    }

    

    

}