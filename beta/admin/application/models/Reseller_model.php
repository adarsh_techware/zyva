<?php
class Reseller_model extends CI_Model {
    public function _consruct() {
        parent::_construct();
    }
    function reseller_add($data) {
        $data['image'] = base_url('assets/uploads/aaaa.png');
        $password = $this->randomPassword();
        $database = $this->db->database;
        $rs = $this->db->query("SELECT `AUTO_INCREMENT` AS auto_id FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$database' AND TABLE_NAME  = 'reseller'")->row();
        $auto_id = $rs->auto_id;
        $auto_id = sprintf('%05d', $auto_id);
        $username = 'ZVAR' . $auto_id;
        $data['username'] = $username;
        $data['status'] = 1;
        $name_reseller = $data['first_name'] . ' ' . $data['last_name'];
        $data['password'] = md5($password);
        //print_r($data);exit;
        $result = $this->db->insert('reseller', $data);
        $email_content = array('username' => $username, 'password' => $password, 'email_id' => $data['email'], 'name' => $name_reseller);
        if ($result) {
            $this->success_mail($email_content);
        }
        //var_dump($result);die;
        // $insert_id = $this->db->insert_id();
        //$this->session->set_userdata('user_id',$insert_id);
        return $result;
    }
    function success_mail($rs) {
        $settings = get_icon();
        $email_content = ' Username: ' . $rs['username'] . ' and Password: ' . $rs['password'];
        $this->load->library('email');
        $config = Array('protocol' => 'smtp', 'smtp_host' => 'mail.zyva.in', 'smtp_port' => 587, 'smtp_user' => 'info@zyva.in', // change it to yours
        'smtp_pass' => 'Golden_123', // change it to yours
        'smtp_timeout' => 20, 'mailtype' => 'html', 'charset' => 'iso-8859-1', 'wordwrap' => TRUE);
        $this->email->initialize($config); // add this line
        $subject = 'Zyva Reseller';
        $name = 'Zyva Reseller';
        //$reg_name = $myArray->first_name.' '.$myArray->last_name;
        $mailTemplate = 'Hi ' . $rs['name'] . ', <br/>Your Zyva reseller account has been successfully created. Please use the below information for future login.';
        $mailTemplate.= $email_content;
        //$this->email->set_newline("\r\n");
        $this->email->from('info@zyva.in', $name);
        //$this->email->to('adarsh.techware@gmail.com');
        $this->email->to($rs['email_id']);
        $this->email->subject($subject);
        $this->email->message($mailTemplate);
        $var = $this->email->send();
        return $var;
    }
    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < 8;$i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
        
    }
    function get_reseller() {
        return $this->db->where('status!=', 2)->order_by('id', 'DESC')->get('reseller')->result();
    }
    function get_customername() {
        $this->db->select('customer.id as cid,customer.first_name,customer.telephone,add_address1.id,add_address1.mobile,add_address1.customer_id');
        $this->db->from('customer');
        $this->db->join('add_address1', 'customer.telephone=add_address1.mobile');
        $qry = $this->db->get();
        $res = $qry->row();
        return $res;
    }
    function update_reseller_status($data, $data1) {
        $this->db->where('id', $data);
        $result = $this->db->update('reseller', $data1);
        return $result;
    }
    function edit_reseller($id) {
        $query = $this->db->where('id', $id);
        $query = $this->db->get('reseller');
        $result = $query->row();
        return $result;
    }
    function update_reseller($id, $data) {
        if ($data['password'] != '') {
            $data['password'] = md5($data['password']);
        } else {
            unset($data['password']);
        }
        $this->db->where('id', $id);
        $result = $this->db->update('reseller', $data);
        return $result;
    }
    function view_booking() {
        $this->db->select('booking.id as id,
					   booking.booking_no,
					   add_address1.address as address1,
					   add_address1.name as custname,
					   add_address1.mobile as mobile,
					   booking.total_rate as total,
					   booking.booked_date date,
				       booking.status,
				       CONCAT(reseller.first_name," ",reseller.last_name) AS name');
        $this->db->from('booking as booking');
        $this->db->join('reseller', 'reseller.id = booking.reseller_id', 'left');
        $this->db->join('add_address1', 'add_address1.id = booking.address_id', 'left');
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        //print_r($query);exit;
        $result = $query->result();
        //print_r($result);exit;
        return $result;
    }
    function view_orders() {
        $this->db->select('order.id as id,
					   order.booking_id as b_id,
					   order.quantity as qty,
					   order.price as price,
					   product.product_name as p_name,
					   product.product_code as p_code,
					   color.color_name as c_name,
					   size.size_type as size,
					   order.stitch_id as stitch');
        $this->db->from('order_details as order');
        $this->db->join('product', 'product.id = order.product_id');
        $this->db->join('color', 'color.id = order.color_id');
        $this->db->join('size', 'size.id = order.size_id');
        $this->db->where('order.id', $bid);
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        $result = $query->result();
        return $result;
    }
    function view_order($bid) {
        //print_r($bid);exit;
        $this->db->select('order.id as id,
					   order.booking_id as b_id,
					   order.quantity as qty,
					   product.price as price,
					   product.product_name as p_name,
					   product.product_code as p_code,
					   color.color_name as c_name,
					   size.size_type as size,
					   order.stitch_id as stitch');
        $this->db->from('order_details as order');
        $this->db->join('product', 'product.id = order.product_id');
        $this->db->join('color', 'color.id = order.color_id');
        $this->db->join('size', 'size.id = order.size_id');
        $this->db->where('order.booking_id', $bid);
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $result = $query->result();
        $new_result = array();
        foreach ($result as $rs) {
            if ($rs->stitch > 0) {
                $res = $this->db->where('id', $rs->stitch)->get('stitching')->row();
                //print_r($res);
                $front = $this->get_stitch($res->front_id);
                //print_r($front);
                //echo $res->back_id;
                $back = $this->get_stitch($res->back_id);
                //print_r($back);
                $sleeve = $this->get_stitch($res->sleeve_id);
                $neck = $this->get_stitch($res->neck_id);
                $add_ons = $this->get_addons($res->add_ons);
                $add_ons1 = $this->get_addons($res->add_ons1);
                $add_ons2 = $this->get_addons($res->add_ons2);
                $add_ons3 = $this->get_addons($res->add_ons3);
                $meas_type = $res->measure_id == 0 ? 'Free Stiching' : 'Measurment';
                $measurement = $this->get_measurement($res->measure_id);
                $stich_array = array('front' => $front, 'back' => $back, 'sleeve' => $sleeve, 'neck' => $neck, 'add_ons' => $add_ons, 'add_ons1' => $add_ons1, 'add_ons2' => $add_ons2, 'add_ons3' => $add_ons3, 'meas_type' => $meas_type, 'measure_id' => $res->measure_id, 'measurement' => $measurement);
                $rs->stith_meas = $stich_array;
            }
            $new_result[] = $rs;
        }
        return $new_result;
    }
    function get_stitch($id) {
        //echo $id;
        $new_rs = $this->db->where('id', $id)->get('stich_type')->row();
        return $new_rs;
    }
    function get_addons($id) {
        $new_rs = $this->db->where('id', $id)->get('add_ons')->row();
        return $new_rs;
    }
    function get_measurement($id) {
        $new_rs = $this->db->where('id', $id)->get('measurement')->row();
        return $new_rs;
    }
    public function reseller_approve($id) {
        $data['image'] = base_url('assets/uploads/aaaa.png');
        $res = $this->edit_reseller($id);
        $password = $this->randomPassword();
        $data['status'] = 1;
        $auto_id = $id;
        $auto_id = sprintf('%05d', $auto_id);
        $username = 'ZVAR' . $auto_id;
        $data['username'] = $username;
        $name_reseller = $res->first_name . ' ' . $res->last_name;
        $data['password'] = md5($password);
        $reseller_code = sprintf('%05d', $id);
        $reseller_code = 'ZVAR' . $reseller_code;
        $data['reseller_code'] = $reseller_code;
        $result = $this->db->where('id', $id)->update('reseller', $data);
        $email_content = array('username' => $username, 'password' => $password, 'email_id' => $res->email, 'name' => $name_reseller);
        if ($result) {
            $this->success_mail($email_content);
            $msg = "Zyva Reseller account Created. Username: " . $username . " and Password: " . $password . " download your app https://goo.gl/gRSTMP";
            $mob_no = $res->telephone;
            $this->send_otp($msg, $mob_no);
        }
        return $result;
    }
    public function reseller_reject($id) {
        $data['status'] = 2;
        $result = $this->db->where('id', $id)->update('reseller', $data);
        return $result;
    }
    function send_otp($msg, $mob_no) {
        $sender_id = "TWSMSG"; // sender id
        $pwd = "968808"; //your SMS gatewayhub account password
        $str = trim(str_replace(" ", "%20", $msg));
        // to replace the space in message with  ‘%20’
        $url = "http://api.smsgatewayhub.com/smsapi/pushsms.aspx?user=nixon&pwd=" . $pwd . "&to=91" . $mob_no . "&sid=" . $sender_id . "&msg=" . $str . "&fl=0&gwid=2";
        // create a new cURL resource
        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // grab URL and pass it to the browser
        $result = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);
    }

    function get_sale_details() {
        // $query= $this->db->get('reseller')->result()

        $this->db->select('reseller.id as id,
					    reseller.first_name,
					    reseller.last_name,
					    reseller.telephone,
						reseller.address,	
						reseller.username'			   
						);
        $this->db->from('reseller');
        $this->db->join('booking', 'booking.reseller_id = reseller.id');
        $this->db->where('reseller.status!=', 2);
        $this->db->order_by('reseller.id', 'DESC');
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
        
    }

    function add_payment($data) {
        $result = $this->db->insert('reseller_payments', $data);
        if($result) {
            return $result;
        }
        else {
            return "error";
        }
    }
   
}
?>
