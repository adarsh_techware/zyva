<?php
class Dashboard_model extends CI_Model {
    public function _consruct() {
        parent::_construct();
    }
    
	function get_total_earning() {
        $this->db->select_sum('total_rate');
    	$this->db->from('booking');
    	$this->db->where('status', '1');
    	$query = $this->db->get();
    	$total = $query->row()->total_rate;
    	//print_r($total);die;
    	return $total;
    }
    function get_total_products() {
    	$this->db->where('status', '1');
        $query = $this->db->get('product');
        $count = $query->num_rows();
        return $count;
    }
    function get_total_sales() {
    	$this->db->where('status !=', '4');
        $query = $this->db->get('order_details');
        $count = $query->num_rows();
        return $count;
    }
    function get_bookings_count() {
    	$this->db->where('status', '1');
        $query = $this->db->get('order_details');
        $count = $query->num_rows();
        return $count;
    }

    function get_shops_count() {
        $menu = $this->session->userdata('admin');
        if ($menu != '1') {
            $user = $this->session->userdata('id');
            $this->db->where('shop_details.created_user', $user);
        }
        $result = $this->db->count_all_results('shop_details');
        return $result;
    }

}
