<?php
class Booking_model extends CI_Model {
    public function _consruct() {
        parent::_construct();
    }
    function view_customer_booking($cid) {
        $this->db->select('booking.id as id,

					   booking.booking_no,

					   address_book.email1 as email,

					   address_book.address_1 as address1,

					   address_book.address_2 as address2,

					   address_book.telephone as telephone,

					   booking.total_rate as total,

					   booking.booked_date date,

				       booking.status,

				       booking.payment_status,

				       booking.medium,

				       CONCAT(customer.first_name," ",customer.last_name) AS name');
        $this->db->from('booking as booking');
        $this->db->where('customer_id', $cid);
        $this->db->join('customer', 'customer.id = booking.customer_id', 'left');
        $this->db->join('address_book', 'address_book.id = booking.address_id', 'left');
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function booking_list() {
        $rs = $this->db->query("SELECT booking.booking_no,booking.reseller_id,booking.booked_date,customer.first_name AS name,product.product_code,order_details.price,order_details.quantity,color.image AS color,booking.address_id,(CASE size.size_type WHEN 'Custom' THEN 'C' ELSE size.size_type END) AS size,(CASE booking.payment_status WHEN 1 THEN 'Paid' ELSE 'Not Paid' END) As status,order_details.id FROM `order_details` INNER JOIN booking ON booking.id = order_details.booking_id LEFT JOIN customer ON customer.id = booking.customer_id LEFT JOIN color ON order_details.color_id = color.id LEFT JOIN size ON size.id = order_details.size_id LEFT JOIN product ON product.id = order_details.product_id WHERE booking.status = 1 AND order_details.status = 1 ORDER BY id ASC")->result();
        return $rs;
    }
    function view_all_orders() {
        $rs = $this->db->query("SELECT booking.booking_no,booking.reseller_id,booking.booked_date,customer.first_name AS name,product.product_code,order_details.price,order_details.quantity,color.image AS color,booking.address_id,(CASE size.size_type WHEN 'Custom' THEN 'C' ELSE size.size_type END) AS size,(CASE booking.payment_status WHEN 1 THEN 'Paid' ELSE 'Not Paid' END) As status,(CASE order_details.status WHEN 1 THEN 'Booked' WHEN 2 THEN 'Processing' WHEN 3 THEN 'Delivered' ELSE 'Cancelled' END) AS pdt_status,order_details.id FROM `order_details` INNER JOIN booking ON booking.id = order_details.booking_id LEFT JOIN customer ON customer.id = booking.customer_id LEFT JOIN color ON order_details.color_id = color.id LEFT JOIN size ON size.id = order_details.size_id LEFT JOIN product ON product.id = order_details.product_id WHERE booking.status = 1 ORDER BY id ASC")->result();
        return $rs;
    }
    function booking_process() {
        $rs = $this->db->query("SELECT booking.booking_no,booking.reseller_id,booking.booked_date,customer.first_name AS name,product.product_code,order_details.price,order_details.quantity,color.image AS color,booking.address_id,(CASE size.size_type WHEN 'Custom' THEN 'C' ELSE size.size_type END) AS size,(CASE booking.payment_status WHEN 1 THEN 'Paid' ELSE 'Not Paid' END) As status,order_details.id,(CASE order_details.status WHEN 2 THEN 'Process' ELSE 'Delivered' END) AS pdt_status FROM `order_details` INNER JOIN booking ON booking.id = order_details.booking_id LEFT JOIN customer ON customer.id = booking.customer_id LEFT JOIN color ON order_details.color_id = color.id LEFT JOIN size ON size.id = order_details.size_id LEFT JOIN product ON product.id = order_details.product_id WHERE booking.status = 1 AND ( order_details.status = 2 OR order_details.status = 3) ORDER BY id ASC")->result();
        return $rs;
    }
    function booking_cancel() {
        $rs = $this->db->query("SELECT booking.booking_no,booking.reseller_id,booking.booked_date,customer.first_name AS name,product.product_code,order_details.price,order_details.quantity,color.image AS color,booking.address_id,(CASE size.size_type WHEN 'Custom' THEN 'C' ELSE size.size_type END) AS size,(CASE booking.payment_status WHEN 1 THEN 'Paid' ELSE 'Not Paid' END) As status,order_details.id,(CASE order_details.status WHEN 2 THEN 'Process' ELSE 'Delivered' END) AS pdt_status FROM `order_details` INNER JOIN booking ON booking.id = order_details.booking_id LEFT JOIN customer ON customer.id = booking.customer_id LEFT JOIN color ON order_details.color_id = color.id LEFT JOIN size ON size.id = order_details.size_id LEFT JOIN product ON product.id = order_details.product_id WHERE booking.status = 1 AND order_details.status = 4 ORDER BY id ASC")->result();
        return $rs;
    }
    function order_info($id) {
        $rs = $this->db->query("SELECT booking.booking_no,booking.reseller_id,booking.booked_date,customer.first_name AS name,product.product_code,product.id,product.product_name,order_details.price,order_details.quantity,color.image AS color,booking.address_id,size.size_type AS size,(CASE booking.payment_status WHEN 1 THEN 'Paid' ELSE 'Not Paid' END) As status,order_details.id,(CASE order_details.status WHEN 1 THEN 'Booked' WHEN 2 THEN 'Processing' WHEN 3 THEN 'Delivered' ELSE 'Cancelled' END) AS pdt_status,order_details.stitch_id,product.id AS product_id,order_details.status AS book_status,booking.gift,booking.medium FROM `order_details` INNER JOIN booking ON booking.id = order_details.booking_id LEFT JOIN customer ON customer.id = booking.customer_id LEFT JOIN color ON order_details.color_id = color.id LEFT JOIN size ON size.id = order_details.size_id LEFT JOIN product ON product.id = order_details.product_id WHERE order_details.id = $id")->row();
        return $rs;
    }
    function get_order_info($id) {
        $rs = $this->db->query("SELECT booking.booking_no,customer.telephone,product.product_code,order_details.price FROM `order_details` INNER JOIN booking ON booking.id = order_details.booking_id LEFT JOIN customer ON customer.id = booking.customer_id LEFT JOIN product ON product.id = order_details.product_id WHERE order_details.id = $id")->row();
        return $rs;
    }
    function view_booking() {
        $this->db->select('booking.id as id,

					   booking.booking_no,

					   booking.reseller_id,

					   address_book.email1 as email,

					   address_book.address_1 as address1,

					   address_book.address_2 as address2,

					   address_book.telephone as telephone,

					   booking.total_rate as total,

					   booking.booked_date date,

				       booking.status,

				       booking.payment_status,

				       booking.medium,

				       CONCAT(customer.first_name," ",customer.last_name) AS name');
        $this->db->from('booking');
        $this->db->join('customer', 'customer.id = booking.customer_id', 'left');
        $this->db->join('address_book', 'address_book.id = booking.address_id', 'left');
        $this->db->where('booking.status', 1);
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function view_process() {
        $this->db->select('booking.id as id,

					   booking.booking_no,

					   booking.reseller_id,

					   address_book.email1 as email,

					   address_book.address_1 as address1,

					   address_book.address_2 as address2,

					   address_book.telephone as telephone,

					   booking.total_rate as total,

					   booking.booked_date date,

				       booking.status,

				       booking.payment_status,

				       booking.medium,

				       CONCAT(customer.first_name," ",customer.last_name) AS name');
        $this->db->from('booking');
        $this->db->join('customer', 'customer.id = booking.customer_id', 'left');
        $this->db->join('address_book', 'address_book.id = booking.address_id', 'left');
        $this->db->where('booking.status', 3);
        $this->db->or_where('booking.status', 2);
        //$this->db->where_or('booking.status',3);
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function view_cancel() {
        $this->db->select('booking.id as id,

					   booking.booking_no,

					   booking.reseller_id,

					   address_book.email1 as email,

					   address_book.address_1 as address1,

					   address_book.address_2 as address2,

					   address_book.telephone as telephone,

					   booking.total_rate as total,

					   booking.booked_date date,

				       booking.status,

				       booking.payment_status,

				       (CASE booking.cancelled WHEN 1 THEN "Customer" ELSE "Admin" END) AS cancelled,

				       booking.medium,

				       CONCAT(customer.first_name," ",customer.last_name) AS name');
        $this->db->from('booking');
        $this->db->join('customer', 'customer.id = booking.customer_id', 'left');
        $this->db->join('address_book', 'address_book.id = booking.address_id', 'left');
        $this->db->where('booking.status', 4);
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function view_failed() {
        $rs = $this->db->query("SELECT booking.id,booking.booking_no,CONCAT(customer.first_name,' ',customer.last_name) As customer,booking.reseller_id,address_book.address_1,address_book.address_2,address_book.telephone,address_book.email1 As email,booking.medium,booking.booked_date,transaction.message,booking.total_rate FROM booking INNER JOIN transaction ON booking.booking_no = transaction.booking_no LEFT JOIN customer ON booking.customer_id = customer.id LEFT JOIN address_book ON address_book.id = booking.address_id WHERE transaction.status != 'Success' AND booking.status!=2 ORDER BY booking.id DESC")->result();
        return $rs;
    }
    function failed_order_popup($id) {
        $result = $this->db->query("SELECT booking.id,booking.booking_no,booking.total_rate AS price,booking.booked_date,customer.first_name AS name,booking.reseller_id,booking.medium,transaction.message,customer.telephone,customer.email FROM booking LEFT JOIN customer ON customer.id = booking.customer_id LEFT JOIN transaction ON transaction.booking_no = booking.booking_no WHERE booking.id =" . $id)->row();
        $rs = $this->db->query("SELECT product.product_code,product.id,product.product_name,order_details.price,order_details.quantity,color.image AS color,size.size_type AS size FROM `order_details` LEFT JOIN color ON order_details.color_id = color.id LEFT JOIN size ON size.id = order_details.size_id LEFT JOIN product ON product.id = order_details.product_id WHERE order_details.booking_id =" . $id)->result();
        $result->order = $rs;
        return $result;
    }
    function view_all_order() {
        $this->db->select('booking.id as id,

					   booking.booking_no,

					   booking.reseller_id,

					   address_book.email1 as email,

					   address_book.address_1 as address1,

					   address_book.address_2 as address2,

					   address_book.telephone as telephone,

					   booking.total_rate as total,

					   booking.booked_date date,

				       booking.status,

				       booking.payment_status,

				       booking.medium,

				       CONCAT(customer.first_name," ",customer.last_name) AS name');
        $this->db->from('booking');
        $this->db->join('customer', 'customer.id = booking.customer_id', 'left');
        $this->db->join('address_book', 'address_book.id = booking.address_id', 'left');
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function view_order($bid) {
        $this->db->select('order.id as id,

					   order.booking_id as b_id,

					   order.quantity as qty,

					   product.product_name as p_name,

					   product.product_code as p_code,

					   color.color_name as c_name,

					   size.size_type as size,

					   order.stitch_id as stitch');
        $this->db->from('order_details as order');
        $this->db->join('product', 'product.id = order.product_id');
        $this->db->join('color', 'color.id = order.color_id');
        $this->db->join('size', 'size.id = order.size_id');
        $this->db->where('order.booking_id', $bid);
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $result = $query->result();
        $new_result = array();
        foreach ($result as $rs) {
            if ($rs->stitch > 0) {
                $res = $this->db->where('id', $rs->stitch)->get('stitching')->row();
                //print_r($res);
                $front = $this->get_stitch($res->front_id);
                //print_r($front);
                $res->back_id;
                $back = $this->get_stitch($res->back_id);
                //print_r($back);
                $sleeve = $this->get_stitch($res->sleeve_id);
                $neck = $this->get_stitch($res->neck_id);
                $add_ons = $this->get_addons($res->add_ons);
                $add_ons1 = $this->get_addons($res->add_ons1);
                $add_ons2 = $this->get_addons($res->add_ons2);
                $add_ons3 = $this->get_addons($res->add_ons3);
                $meas_type = $res->measure_id == 0 ? 'Free Stiching' : 'Measurment';
                $measurement = $this->get_measurement($res->measure_id);
                $stich_array = array('front' => $front, 'back' => $back, 'sleeve' => $sleeve, 'neck' => $neck, 'add_ons' => $add_ons, 'add_ons1' => $add_ons1, 'add_ons2' => $add_ons2, 'add_ons3' => $add_ons3, 'meas_type' => $meas_type, 'measure_id' => $res->measure_id, 'specialcase' => $res->specialcase, 'measurement' => $measurement);
                $rs->stith_meas = $stich_array;
            }
            $new_result[] = $rs;
        }
        return $new_result;
    }
    function get_stitch($id) {
        //echo $id;
        $new_rs = $this->db->where('id', $id)->get('stich_type')->row();
        return $new_rs;
    }
    function get_addons($id) {
        $new_rs = $this->db->where('id', $id)->get('add_ons')->row();
        return $new_rs;
    }
    function get_measurement($id) {
        $new_rs = $this->db->where('id', $id)->get('new_measurement')->row();
        return $new_rs;
    }
    function view_orders($bid) {
        $this->db->select('order.id as id,

					   order.booking_id as b_id,

					   order.quantity as qty,

					   order.price as price,

					   product.product_name as p_name,

					   product.product_code as p_code,

					   color.color_name as c_name,

					   size.size_type as size,

					   order.stitch_id as stitch');
        $this->db->from('order_details as order');
        $this->db->join('product', 'product.id = order.product_id');
        $this->db->join('color', 'color.id = order.color_id');
        $this->db->join('size', 'size.id = order.size_id');
        $this->db->where('order.id', $bid);
        // $this->db->group_by('product.id');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        $result = $query->result();
        return $result;
    }
    function edit_user($id) {
        $query = $this->db->where('id', $id);
        $query = $this->db->get('users');
        $result = $query->row();
        return $result;
    }
    function update_user($id, $data) {
        $this->db->where('id', $id);
        $result = $this->db->update('users', $data);
        // print_r($result);die;
        return $result;
    }
    function user_delete($id) {
        $this->db->where('id', $id);
        $result = $this->db->delete('users');
        if ($result) {
            return "success";
        } else {
            return "error";
        }
    }
    function category() {
        $this->db->where('status', 1);
        $this->db->order_by('order_by', 'ASC');
        $query = $this->db->get('category');
        $result = $query->result();
        return $result;
    }
   

    function order_details($booked_id) {
		$this->db->select('product.product_name,
							product_gallery.product_image as product_image,
							color.image as color_image,
							size.size_type,
							order_details.stitch_id,
							order_details.product_id,
							product.product_code,
							order_details.quantity,
							product.price,
							order_details.id,
							order_details.status,
							booking.booking_no'
						);
 		$this->db->from('order_details');
 		$this->db->join('product','product.id = order_details.product_id','left');
 		$this->db->join('color','color.id = order_details.color_id','left');
 		$this->db->join('size','size.id = order_details.size_id','left');
 		$this->db->join('product_gallery','product_gallery.product_id = order_details.product_id','left');
 		$this->db->join('booking','booking.id = order_details.booking_id','left');
 		$this->db->where('booking.booking_no',$booked_id);
 		$this->db->group_by("order_details.product_id");
 		//$this->db->order_by('product_gallery.id', 'DESC')->limit(1);

 		$query = $this->db->get()->result();
 		//echo $this->db->last_query();die;
 		return $query;
    }

    function booking_details($booked_id) {
        $this->db->select('COUNT(order_details.id) AS total_item,
                            booking.booking_no,
                            booking.total_rate,
                            booking.payment_type,
                            address_book.telephone,
                            CONCAT(address_book.firstname) AS delivery_name,
                            address_book.address_1,
                            address_book.address_2,
                            address_book.state_id,
                            address_book.city_id,
                            address_book.Zip'
                        );
        $this->db->from('booking');
        $this->db->join('address_book','address_book.id = booking.address_id','left');
        $this->db->join('order_details','order_details.booking_id = booking.id','left');
        $this->db->where('booking.booking_no',$booked_id);
        $this->db->group_by("booking.id");
        //$this->db->group_by(['booking.id', 'order_details.color_id','order_details.size_id']);
        $query = $this->db->get()->row();
        return $query;
    }
   
    
}
?>

