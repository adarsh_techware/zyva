<?php
class Users_model extends CI_Model
{
    
    public function _consruct()
    {
        
        parent::_construct();
        
    }
    
    function get_registrationdetails()
    {
        
        
        
        $query = $this->db->get('users');
        
        $result = $query->result();
        
        return $result;
        
        
        
    }
    
    function registrationdetails_add($data)
    {
        
        $data['password'] = md5($data['password']);
        
        $result = $this->db->insert('users', $data);
        
        $insert_id = $this->db->insert_id();
        
        //$this->session->set_userdata('user_id',$insert_id);
        
        
        
        
        
        $a = array(
            'user_id' => $insert_id,
            'username' => $data['username'],
            'password' => $data['password'],
            'role_id' => $data['role_id']
        );
        
        $this->db->insert('login', $a);
        
        
        
        return $result;
        
        
        
        
        
    }
    
    function gets_role()
    {
        
        $this->db->where('rolename!=', 'superadmin');
        
        $query = $this->db->get('role');
        
        $result = $query->result();
        
        return $result;
        
    }
    
    
    
    
    
    
    
    function update_admin_profile($data, $id)
    {
        
        
        
        // $this->db->select("count(*) as count");
        
        $this->db->where("username", $data['username']);
        
        $this->db->where("id !=", $id);
        
        $this->db->from("users");
        
        $count = $this->db->get()->row();
        
        //var_dump($count);
        
        // if($count->count > 0) {
        
        // 	return "exist";
        
        // }
        
        // else {
        
        
        
        $this->db->where('id', $id);
        
        $result = $this->db->update('users', $data);
        
        
        
        if ($result) {
            
            return true;
            
        }
        
        else {
            
            return false;
            
        }
        
        // }
        
        //exit;
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    function get_admin_profile_details($id)
    {
        
        //$id = $this->session->userdata('logged_in')['id'];
        
        
        
        $this->db->select("users.*,");
        
        $this->db->where('users.id', $id);
        
        $this->db->from("users");
        
        $query = $this->db->get();
        
        $result = $query->row();
        
        //var_dump($result);exit;
        
        
        
        return $result;
        
    }
    
    
    
    
    
    
    
    
    
    ////Update Password	
    
    function update_admin_passwords($data, $id)
    {
        
        
        
        
        
        
        
        $this->db->where("password", ($data['password']));
        
        
        
        $this->db->where("id", $id);
        
        $this->db->from("users");
        
        $count = $this->db->get()->row();
        
        
        
        
        
        $update_data['password'] = md5($data['n_password']);
        
        $this->db->where('id', $id);
        
        $result = $this->db->update('users', $update_data);
        
        
        
        if ($result) {
            
            return true;
            
        }
        
        else {
            
            return false;
            
        }
        
        
        
    }
    
    
    
    function get_userdetails()
    {
        
        
        
        
        
        
        
        $this->db->select("users.id as id,

						   CONCAT(users.first_name,' ',users.last_name) AS name,

					       users.email,

					       role.rolename,

					       users.username,

					       users.telephone");
        
        
        
        $this->db->from('users as users');
        
        $this->db->join('role', 'role.id = users.role_id');
        
        $query = $this->db->get();
        
        $result = $query->result();
        
        return $result;
        
        
        
        
        
    }
    
}

?>