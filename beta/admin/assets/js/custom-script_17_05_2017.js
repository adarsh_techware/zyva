


$( "form.validate" ).submit(function( event ) {

	var access = true;
	$(this).find('.required').each(function() {
		var v = $(this).val();
		if((v.replace(/\s+/g, '')) == '') {
			access = false;
			$(this).parents(".form-group").addClass("has-error");
		}
		else {
			$(this).parents(".form-group").removeClass("has-error");
		}
	});
	if(access) {
		return;
	}
	else {
		$("body").animate({ scrollTop: $('.has-error').offset().top - 50 }, "slow");
	}
	event.preventDefault();

});

// View Shop Details
$(function() {});





   


 $('.view-customer').on("click", function() {
	var loader = '<p class="text-center"><img src="'+config_url+'/assets/images/ajax-loader-4.gif" /></p>';
	$('#myModal .modal-body').html(loader);
	$('#myModal').modal({show:true});
	var id = $(this).data('id');
	var data = {id:id};
	var url = config_url+'customer/view_single_customer';
	var result = post_ajax(url, data);
	$('#myModal .modal-body').html(result);

	reload_gallery();

});




 $('.view-user').on("click", function() {
	var loader = '<p class="text-center"><img src="'+config_url+'/assets/images/ajax-loader-4.gif" /></p>';
	$('#myModal .modal-body').html(loader);
	$('#myModal').modal({show:true});
	var id = $(this).data('id');
	var data = {id:id};
	var url = config_url+'user/view_single_user';
	var result = post_ajax(url, data);
	$('#myModal .modal-body').html(result);
	reload_gallery();

});

 

$('.view-slider').on("click", function() {
	var loader = '<p class="text-center"><img src="'+config_url+'/assets/images/ajax-loader-4.gif" /></p>';
	$('#myModal .modal-body').html(loader);
	$('#myModal').modal({show:true});
	var id = $(this).data('id');
	var data = {id:id};
	var url = config_url+'settings/view_single_slider';
	var result = post_ajax(url, data);
	$('#myModal .modal-body').html(result);
	reload_gallery();
});









function post_ajax(url, data) {
	var result = '';
	$.ajax({
        type: "POST",
        url: url,
		data: data,
		success: function(response) {
			result = response;
		},
		error: function(response) {
			result = 'error';
		},
		async: false
		});

		return result;
}





//////ADMIN IMG UPLOADING/////

/*$('#profile_pic').on("change", function() {
	readURL(this);
});


$('#profileimg-form').change(function() {
	$('form#profilepic-form-img').submit();
});*/


$('#profile_pic').on("change", function() {
	readURL(this);
});


$('#profileimg-form').change(function() {
	$('form#profilepic-form-img').submit();
});

///////state city////


   function selectStatedoctor(country_id){
	if(country_id!="-1"){
		loadDatadoctor('state',country_id);
		$("#city_dropdown").html("<option value='-1'>Select city</option>");
	}else{
		$("#state_dropdown").html("<option value='-1'>Select state</option>");
		$("#city_dropdown").html("<option value='-1'>Select city</option>");
	}
}

function selectCitydoctor(state_id){
	if(state_id!="-1"){
		loadDatadoctor('city',state_id);
	}else{
		$("#city_dropdown").html("<option value='-1'>Select city</option>");
	}
}

function loadDatadoctor(loadType,loadId){

	var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
	//$("#"+loadType+"_loader").show();
   // $("#"+loadType+"_loader").fadeIn(400).html('Please wait... <img src="image/loading.gif" />');
	$.ajax({
		type: "POST",
		  url: base_url+"Doctor_Ctrl/loadData",
		data: dataString,
		cache: false,
		success: function(result){

			//$("#"+loadType+"_loader").hide();
			$("#"+loadType+"_dropdown").html("<option value='-1'>Select "+loadType+"</option>");
			$("#"+loadType+"_dropdown").append(result);
		},
		error: function() {
			alert("error");
		}
	});
}
   








//Customer details view popup
$('.show-customergetdetails').on("click", function(){
	var customerdetails = $(this).attr("data-id");
	var loader = '<p class="text-center"><img src="'+base_url+'assets/images/ajax-loader-4.gif" /></p>';
    $('#popup-customergetModal .modal-customerbody').html(loader);
    $('#popup-customergetModal').modal({show:true});
	$.ajax({
				type: "POST",
				url: base_url+'User/view_customerpopup',

				data: {'customerdetails':customerdetails},
				cache: false,
				success: function(result)
				{
					$('#popup-customergetModal .modal-customerbody').html(result);
				}
	});
})
$('.show-productdetails').on("click", function(){
	var productdetailsval = $(this).attr("data-id");
	var loader = '<p class="text-center"><img src="'+base_url+'assets/images/ajax-loader-4.gif" /></p>';
    $('#popup-productModal .modal-productbody').html(loader);
    $('#popup-productModal').modal({show:true});
	$.ajax({
				type: "POST",
				url: base_url+'Product_ctrl/product_viewpopup',

				data: {'productdetailsval':productdetailsval},
				cache: false,
				success: function(result)
				{
					$('#popup-productModal .modal-productbody').html(result);
				}
	});
})

$('.show-booking').on("click", function(){
	var booking_id = $(this).attr("data-id");
	var loader = '<p class="text-center"><img src="'+base_url+'assets/images/ajax-loader-4.gif" /></p>';
    $('#orderModal .modal-productbody').html(loader);
    $('#orderModal').modal({show:true});
	$.ajax({
				type: "POST",
				url: base_url+'Booking/order_popup',

				data: {'booking_id':booking_id},
				cache: false,
				success: function(result)
				{
					$('#orderModal .modal-productbody').html(result);
					call_toggle();
				}
	});
})

function call_toggle(){
	$('.myButton').click(function() {
    	$(this).closest('tr').next('tr').find('.more').toggle();
	});
}

