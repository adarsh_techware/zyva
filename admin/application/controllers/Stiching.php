<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Stiching extends CI_Controller {



	public function __construct() {

	parent::__construct();

		check_login();
	$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		date_default_timezone_set("Asia/Kolkata");

		$this->load->model('Stiching_model');



    }





    public function add_stiching_types() {
		$template['page'] = 'stiching/stiching_type';

		$template['page_title'] = 'Add stiching';


		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$template['stype'] = $this->Stiching_model->view_type();

        if($_POST){

			$data = $_POST;

			$config = set_upload_category('./uploads/');

		 	$this->load->library('upload');

		 	$new_name = time()."_".$_FILES["image"]['name'];

		 	$config['file_name'] = $new_name;

		 	$this->upload->initialize($config);



		 	if ( ! $this->upload->do_upload('image')) {

		 		$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));
		 	}

		 	else {


			 	$upload_data = $this->upload->data();

			 	$data['image'] =base_url().$config['upload_path']."/".$upload_data['file_name'];


			 	$result = $this->Stiching_model->add_stiching_types($data);

			 	if($result) {

			 		$this->session->set_flashdata('message',array('message' => 'Subcategory Added Successfully','class' => 'success'));

			 	} else {

			 		$this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

			 	}

			}

		}



 		$this->load->view('template',$template);
	}

	function view_stich_types(){

		$template['page'] = 'stiching/view_stich_types';

		$template['page_title'] = "View stiching types";

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$template['stich_types'] = $this->Stiching_model->view_stich_types();

		$this->load->view('template',$template);



	}





/*stich status*/

	 public function stich_status(){



 				  $data1 = array(

 				  "status" => '0'

 							 );

 				  $id = $this->uri->segment(3);

 				  $s=$this->Stiching_model->update_stich_status($id, $data1);

 				  $this->session->set_flashdata('message', array('message' => 'Successfully Disabled','class' => 'warning'));

 				  redirect(base_url().'Stiching/view_stich_types');

 	    }



 		public function stich_active(){



 				  $data1 = array(

 				  "status" => '1'

 							 );

 				  $id = $this->uri->segment(3);

 				  $s=$this->Stiching_model->update_stich_status($id, $data1);

 				  $this->session->set_flashdata('message', array('message' => 'Successfully Enabled','class' => 'success'));

 				  redirect(base_url().'Stiching/view_stich_types');

 	    }













 	    public function edit_stich(){

		

		$template['page'] = 'stiching/edit_stich';

		$template['page_title'] = "edit stiching";

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		 $template['stype'] = $this->Stiching_model->view_type();

		$id = $this->uri->segment(3);

		$template['stich1'] = $this->Stiching_model->edit_stich($id);

		// print_r($template['stich1']);die;

		if(!empty($template['stich1'])) {

			if($_POST){

				$data = $_POST;

				//  var_dump($_FILES);die;

				//upload image

				if(isset($_FILES['image']) && $_FILES['image']['name']!=''){

				$config = set_upload_category('./uploads/');

				

				// $config = set_upload_subcategory('assets/uploads/subcategory');

				$this->load->library('upload');

				$new_name = time()."_".$_FILES['image'];

				$config['file_name'] = $new_name;

				

				$this->upload->initialize($config);

				$upload_data = $this->upload->data();

				$data['image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

				if (!$this->upload->do_upload('image'))

				{

					$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));

				}

				else

				{

					$upload_data = $this->upload->data();

					$data['image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

				}

			}

				$result = $this->Stiching_model->update_stich($id,$data);

				if($result){

					$this->session->set_flashdata('message', array('message' => "Stiching Details Updated successfully", 'title' => 'Error !', 'class' => 'success'));

				}

				redirect(base_url().'Stiching/view_stich_types');

			}

	 	}

	 	else

		{

			$this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));

			redirect(base_url().'Stiching/view_stich_types');

	 	}

		$this->load->view('template',$template);

}



 



}