<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_stock extends CI_Controller {

	public function __construct() {
	parent::__construct();
	check_login();

	$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		date_default_timezone_set("Asia/Kolkata");
		$this->load->model('Product_stock_model');
		$this->load->model('Product_model');
		$this->load->model('sizemodel');
		$this->load->model('Product_model');
		
		
		// $this->load->library('upload');

    }

///////////////////////////////////
//////****PRODUCT  stock DETAILS***///////
     public function view_product_stock(){


			  $template['page'] = 'product_stock/view_product_stock';
			  $template['page_title'] = "Product";
			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
			  $template['data'] = $this->Product_stock_model->get_product_stock();
			  // var_dump($template['data']);
			  // die;
			  $this->load->view('template',$template);
	 }


//////////////////////////////////////////
//////*****ADD PRODUCT stock DETAILS****///////
	  public function add_productstock()
		{

			   $template['page'] = 'product_stock/product_stock';
			   $template['page_title'] = "Product stock"; 
			   $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
			   $template['product'] = $this->Product_model->gets_product();
			   
		 		if($_POST){
			  	$data = $_POST;


				// print_r($data);
				// die;
			
		 	

		 			
					$result = $this->Product_stock_model->add_productstock($data);


		 		if($result) {

		 			 $this->session->set_flashdata('message',array('message' => 'Product Stock Added Successfully','class' => 'success'));
		 		}
		 		else {
		 			 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));
		 		}


		 		
		 		}
			   
			    $template['product'] = $this->Product_model->gets_product();
			   
			   $template['size'] = $this->sizemodel->view_size();
			   
			   $template['color'] = $this->Product_model->get_color();
			   
			   $this->load->view('template',$template);
	    }


	public function editpro_stock($id=null,$color_id=null) {
		$template['page'] = 'product_stock/edit_stock';
	    $template['page_title'] = 'Edit Product';
	    $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
		$id = $this->uri->segment(3);
		$color_id = $this->uri->segment(4);
		if($id==null || $color_id==null){
			redirect(base_url('product_stock/view_product_stock'));
		}
	    $template['data'] = $this->Product_stock_model->get_stock_info($id,$color_id);

	    $template['data']->stock = $this->Product_stock_model->stock_info($id,$color_id);
	    

		if($_POST){
			  $data = $_POST;

			  $quanty_array = $data['quantity'];
			  $stock_array = $data['stock_id'];

			  $num_size = count($stock_array);

			  $result = false;

			  for ($i=0; $i < $num_size; $i++) { 
			  	$this->db->where('id',$stock_array[$i])->update('product_quantity',array('quantity'=>$quanty_array[$i]));
			  	$result = true;
			  }

			  



				if($result) {
					 $this->session->set_flashdata('message',array('message' => 'Edit Product Size Details Updated successfully','class' => 'success'));
					  redirect(base_url().'Product_stock/view_product_stock');
				}
				else {

					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));
					 redirect(base_url().'Product_stock/view_product_stock');
				}
		}

				
			$this->load->view('template',$template);
}	

public function size_remove(){
	$data = $_POST;
	$id = $data['id'];
	$this->db->where('id',$id)->delete('product_quantity');
	echo true;
}



/*public function editpro_stock()
{
		$template['page'] = 'product_stock/edit_stock';

	    $template['page_title'] = 'Edit Product';
	    $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
		$id = $this->uri->segment(3);
	    $template['data1'] = $this->Product_stock_model->edit_stock($id);
	    // var_dump($template['data1']);die;
		if($_POST){
			  $data = $_POST;

				 $result =$this->Product_stock_model->update_stock($id, $data);

				if($result) {
					 $this->session->set_flashdata('message',array('message' => 'Edit Product Size Details Updated successfully','class' => 'success'));
					  redirect(base_url().'Product_stock/view_product_stock');
				}
				else {

					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));
					 redirect(base_url().'Product_stock/view_product_stock');
				}
				}

				 $template['product'] = $this->Product_model->gets_product();

				$template['size'] = $this->sizemodel->view_size();
			   
			   $template['color'] = $this->Product_model->get_color();
					$this->load->view('template',$template);
}*/




/*Category status*/
	 public function stock_status(){

 				  $data1 = array(
 				  "status" => '0'
 							 );
 				  $id = $this->uri->segment(3);
 				  $s=$this->Product_stock_model->update_stock_status($id, $data1);
 				  $this->session->set_flashdata('message', array('message' => 'stock Successfully Disabled','class' => 'warning'));
 				  redirect(base_url().'Product_stock/view_product_stock');
 	    }

 		public function stock_active(){

 				  $data1 = array(
 				  "status" => '1'
 							 );
 				  $id = $this->uri->segment(3);
 				  $s=$this->Product_stock_model->update_stock_status($id, $data1);
 				  $this->session->set_flashdata('message', array('message' => 'stock Successfully Enabled','class' => 'success'));
 				  redirect(base_url().'Product_stock/view_product_stock');
 	    }


	    } 

	    ?>