<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sub_category extends CI_Controller {
    public function __construct() {
        parent::__construct();
        check_login();
        date_default_timezone_set("Asia/Kolkata");
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        $this->info = get_function($class, $method);
        $userdata = $this->session->userdata('userdata');
        $role_id = $userdata['user_type'];
        if (!privillage($class, $method, $role_id)) {
            redirect('wrong');
        }
        $this->perm = get_permit($role_id);
        $this->load->model('Category_model');
    }
    function delete_subcat() {
        $id = $this->uri->segment(3);
        $result = $this->Category_model->delete_subcat($id);
        $this->session->set_flashdata('message', array('message' => 'Deleted Successfully', 'class' => 'success'));
        redirect(base_url() . 'Category/view_subcategory');
    }
    public function create() {
        $template['page'] = 'subcategory/add_subcategory';
        $template['page_title'] = 'Add Sub Category';
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $sessid = $this->session->userdata('logged_in');
        $template['Category'] = $this->Category_model->get_category();
        if ($_POST) {
            $data = $_POST;
            $result = $this->Category_model->add_subcategory($data);
            if ($result) {
                $this->session->set_flashdata('message', array('message' => 'Subcategory Added Successfully', 'class' => 'success'));
            } else {
                $this->session->set_flashdata('message', array('message' => 'Error', 'class' => 'error'));
            }
        }
        $this->load->view('template', $template);
    }
    function index() {
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['page'] = 'subcategory/view_subcategory';
        $template['page_title'] = "View Subcategory";
        $template['sub_category'] = $this->Category_model->view_subcategory();
        $this->load->view('template', $template);
    }
    function edit_subcategory() {
        $template['page'] = 'subcategory/editsub_cat';
        $template['page_title'] = "edit sub_category";
        $id = $this->uri->segment(3);
        $template['perm'] = $this->perm;
        $template['main'] = $this->info->menu_name;
        $template['sub'] = $this->info->fun_menu;
        $template['subcat'] = $this->Category_model->edit_subcategory($id);
        if (!empty($template['subcat'])) {
            if ($_POST) {
                $data = $_POST;
                $result = $this->Category_model->update_subcategory($id, $data);
                $this->session->set_flashdata('message', array('message' => 'Subcategory Updated Successfully', 'class' => 'success'));
                redirect(base_url() . 'sub_category');
            }
        } else {
            $this->session->set_flashdata('message', array('message' => "You don't have permission to access.", 'class' => 'danger'));
            redirect(base_url() . 'sub_category');
        }
        $template['Category'] = $this->Category_model->view_category();
        $this->load->view('template', $template);
    }
    public function subcategory_status() {
        $data1 = array("status" => '0');
        $id = $this->uri->segment(3);
        $s = $this->Category_model->update_category_status1($id, $data1);
        $this->session->set_flashdata('message', array('message' => 'Subcategory Disabled Successfully', 'class' => 'warning'));
        redirect(base_url() . 'sub_category');
    }
    public function subCategory_active() {
        $data1 = array("status" => '1');
        $id = $this->uri->segment(3);
        $s = $this->Category_model->update_category_status1($id, $data1);
        $this->session->set_flashdata('message', array('message' => 'Subcategory Enabled Successfully', 'class' => 'success'));
        redirect(base_url() . 'sub_category');
    }
}
?>