<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Size_ctrl extends CI_Controller {



	public function __construct() {

	parent::__construct();

	$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);



		date_default_timezone_set("Asia/Kolkata");

		$this->load->model('sizemodel');



    }



    public function add_size() {



			  $template['page'] = 'size/addsize';

			  $template['page_title'] = 'Add size';

			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

        //  $this->load->view('template',$template);

		      $sessid=$this->session->userdata('logged_in');

        //

		      if($_POST){

			  $data = $_POST;





      $result = $this->sizemodel->add_size($data);

				// print_r($result);

				// die;

				if($result) {



					 $this->session->set_flashdata('message',array('message' => 'Size Added Successfully','class' => 'success'));

				}

				else {

					/* Set error message */

					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

				}

				}







			  $this->load->view('template',$template);



}

function view_size(){



			$template['page'] = 'size/viewsize';

			$template['page_title'] = "View size";

			$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

			$template['size'] = $this->sizemodel->view_size();

			$this->load->view('template',$template);



}



function add_prosize(){

	$template['page'] = 'size/product_size';

	$template['page_title'] = 'Add size';

	$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

	//  $this->load->view('template',$template);

		$sessid=$this->session->userdata('logged_in');

	//

		if($_POST){

	$data = $_POST;





$result = $this->sizemodel->add_prosize($data);

	// print_r($result);

	// die;

	if($result) {



		 $this->session->set_flashdata('message',array('message' => 'Registered  Successfully','class' => 'success'));

	}

	else {

		/* Set error message */

		 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

	}

	}





	$template['prod'] = $this->sizemodel->get_product();

	$template['size'] = $this->sizemodel->view_size();





	$this->load->view('template',$template);

}



function view_prosize(){



		$template['page'] = 'size/viewproduct_size';

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;





		$template['viewpsize'] = $this->sizemodel->view_prosize();

		$this->load->view('template',$template);



                      }





// function editpro_size()

//  {

//  			 $template['page'] = 'size/editpro_size';

//  			 $id = $this->uri->segment(3);

//

//

// 			 $template['prosi'] = $this->sizemodel->edit_sizep($id);

// 						  // print_r($template['prosi']);die;

// 			 	if(!empty($template['prosi'])) {

//

// 						  if($_POST)

// 						  {

// 							 $data = $_POST;

//

// 				  $result = $this->sizemodel->update_size($id,$data);

// 				  redirect(base_url().'size_ctrl/view_prosize');

//

// 		   }

// 				  }

// 				  else{

// 					   $this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));

// 			           redirect(base_url().'size_ctrl/view_prosize');

// 				  }

//

// 					$template['prod'] = $this->sizemodel->get_product();

// 					$template['size'] = $this->sizemodel->view_size();

// 				 $this->load->view('template',$template);

//

//    }

//



//

public function editpro_size()

{

		$template['page'] = 'size/editpro_size';

	    $template['page_title'] = 'Edit Product';

	    $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$id = $this->uri->segment(3);

	    $template['data'] = $this->sizemodel->get_single_productsize($id);

		if($_POST){

			  $data = $_POST;



				$result = $this->sizemodel->productsizedetails_edit($data, $id);



				if($result) {

					 $this->session->set_flashdata('message',array('message' => 'Product Size Updated Successfully','class' => 'success'));

					  redirect(base_url().'size_ctrl/view_prosize');

				}

				else {



					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

					 redirect(base_url().'size_ctrl/view_prosize');

				}

				}

				    $template['product'] = $this->sizemodel->gets_productsize($id);

					$template['size'] = $this->sizemodel->gets_size($id);

					//print_r($template['size']);

					//die;

					

					

					

					$this->load->view('template',$template);

}





  }

  ?>

