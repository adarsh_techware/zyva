<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        check_login();
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);
        $this->load->model('Product_model');
        $this->load->library('upload');
        
    }
    
    ///////////////////////////////////
    //////****PRODUCT DETAILS***///////
    public function view_product()
    {
        
        
        $template['page']       = 'Productdetails/view-productdetails';
        $template['page_title'] = "Product";
        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
        $template['data']       = $this->Product_model->get_productdetails();
        //var_dump($template['data']);
        //die;
        $this->load->view('template', $template);
    }
    
    
    //////////////////////////////////////////
    //////*****ADD PRODUCT DETAILS****///////
    public function add_products()
    {
        
        $template['page']       = 'Productdetails/add-product';
        $template['page_title'] = 'Add Product';
        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
        $sessid                 = $this->session->userdata('logged_in');

        $this->load->helper('general');
        upload_image();
        
        if ($_POST) {
            
            $data = $_POST;
            $result = $this->Product_model->productdetails_add($data);
            
            if ($result) {
                $pdt_id = $this->db->insert_id();
                
                $filesCount = count($_FILES['product_image']['name']);
                for ($i = 0; $i < $filesCount; $i++) {
                    $_FILES['pdt_image']['name']     = time() . "_" . $_FILES['product_image']['name'][$i];
                    $_FILES['pdt_image']['type']     = $_FILES['product_image']['type'][$i];
                    $_FILES['pdt_image']['tmp_name'] = $_FILES['product_image']['tmp_name'][$i];
                    $_FILES['pdt_image']['error']    = $_FILES['product_image']['error'][$i];
                    $_FILES['pdt_image']['size']     = $_FILES['product_image']['size'][$i];
                    
                    $uploadPath              = 'assets/uploads/';
                    $config['upload_path']   = $uploadPath;
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';

                    $j = $i+1;
                    
                    
                    $this->upload->initialize($config);
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('pdt_image')) {
                        $fileData   = $this->upload->data();
                        $uploadData[$i]['product_id']    = $pdt_id;

                        list($name,$ext) = explode('.', $fileData['file_name']);
                        $ext = '.'.$ext;

                        $image_name = $this->image_manipulate($uploadPath.$fileData['file_name'],$j,$data['product_code']);

                        $uploadData[$i]['product_image'] = $uploadPath.$image_name;
                        $uploadData[$i]['format'] = $ext;
                        
                    }  
                }
                
                if (!empty($uploadData)) {
                    $insert = $this->Product_model->insert_gallery($uploadData);
                }
                
                $this->session->set_flashdata('message', array(
                    'message' => 'Product Added  Successfully',
                    'class' => 'success'
                ));
            } else {
                
                $this->session->set_flashdata('message', array(
                    'message' => 'Error',
                    'class' => 'error'
                ));
            }
        }
        //}
        
        
        $template['category'] = $this->Product_model->gets_category();
        
        $template['sub_cat'] = $this->Product_model->gets_sub_category();
        
        $this->load->view('template', $template);
    }

    function check_avaliable(){
        $pdt_name = $_POST['pdt_name'];
        $rs = $this->db->where('product_name',$pdt_name)->get('product');
        if($rs->num_rows()>0){
            echo 1;
        } else {
            echo 0;
        }
    }

    function check_edit_avaliable(){
        $pdt_name = $_POST['pdt_name'];
        $id = $_POST['id'];
        $rs = $this->db->where('product_name',$pdt_name)->where('id!=',$id)->get('product');
        if($rs->num_rows()>0){
            echo 1;
        } else {
            echo 0;
        } 
    }


    function image_manipulate($img_name,$i,$product_code){

        ini_set("max_execution_time",0);
        
        $dir_dest = 'assets/uploads/';
        $dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $dir_dest);
        
        
        
        
        
            
            
            list($name,$ext) = explode('.', $img_name);
            $ext = '.'.$ext;
            ini_set("max_execution_time",0);
            $dir_dest = 'assets/uploads/';
            $dir_pics = (isset($_GET['pics']) ? $_GET['pics'] : $dir_dest);
            $main_filename = $product_code.'_'.$i;
            $sub_filename = $product_code.'_'.$i.'_201x302';
            $similar_filename = $product_code.'_'.$i.'_400x600';
            $small_filename = $product_code.'_'.$i.'_95x143';
            $handle = new Upload($img_name);
            $handle->image_watermark       = "assets/WATERMARK_vertical.png";
            $handle->image_text            = $product_code;
            $handle->image_text_color      = '#ababab';
            $handle->image_text_size       = 28;
            $handle->image_text_position   = 'BL';
            $handle->image_text_padding_x  = 10;
            $handle->image_text_padding_y  = 2;
            if($handle->uploaded) {
                $handle->Process($dir_dest,$main_filename);
                if($handle->processed) {
                   
                    $handle->image_resize          = true;
                    $handle->image_y               = 302;
                    $handle->image_x               = 201;
                    $handle->image_watermark       = "assets/WATERMARK_vertical_201x302.png";
                    $handle->Process($dir_dest,$sub_filename);

                    $handle->image_resize          = true;
                    $handle->image_y               = 600;
                    $handle->image_x               = 400;
                    $handle->image_watermark       = "assets/WATERMARK_vertical_400x600.png";
                    $handle->Process($dir_dest,$similar_filename);

                    $handle->image_resize          = true;
                    $handle->image_y               = 143;
                    $handle->image_x               = 95;
                    $handle->Process($dir_dest,$small_filename);
                    return $main_filename;
                    
                }
            } else {
               return $product_code.'_'.$i;;
            }
  
        


    }
    
    //////////////////////////////////////////
    //////***** EDIT PRODUCT DETAILS****///////
    
    public function edit_product()
    {
        
        $template['page']       = 'Productdetails/edit-product';
        $template['page_title'] = 'Edit Product';
        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

        $this->load->helper('general');
        upload_image();
        
        $id               = $this->uri->segment(3);
        $template['data'] = $this->Product_model->get_single_product($id);
        
        $cat_id=$template['data']->category_id;
        
        //$template['data1'] = $this->Product_model->get_single_subimg($id);
        
        
        if (!empty($template['data'])) {
            
            
            if ($_POST) {
                $data = $_POST;
                
                
                $result = $this->Product_model->productdetails_edit($id, $data);
                
                if ($result) {

                    $pdt_id = $id;

                    $gallery = $this->db->where('product_id',$id)->order_by('id','DESC')->limit(1)->get('product_gallery')->row();
                    $image_path = $gallery->product_image;

                    list($label,$j) = explode('_', $image_path);

                    
                
                $filesCount = count($_FILES['product_image']['name']);
                for ($i = 0; $i < $filesCount; $i++) {
                    $_FILES['pdt_image']['name']     = time() . "_" . $_FILES['product_image']['name'][$i];
                    $_FILES['pdt_image']['type']     = $_FILES['product_image']['type'][$i];
                    $_FILES['pdt_image']['tmp_name'] = $_FILES['product_image']['tmp_name'][$i];
                    $_FILES['pdt_image']['error']    = $_FILES['product_image']['error'][$i];
                    $_FILES['pdt_image']['size']     = $_FILES['product_image']['size'][$i];
                    
                    $uploadPath              = 'assets/uploads/';
                    $config['upload_path']   = $uploadPath;
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    
                    $j = $j+$i+1;
                    
                    $this->upload->initialize($config);
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('pdt_image')) {
                        $fileData   = $this->upload->data();
                        $uploadData[$i]['product_id']    = $pdt_id;
                        list($name,$ext) = explode('.', $fileData['file_name']);
                        $ext = '.'.$ext;

                        $image_name = $this->image_manipulate($uploadPath.$fileData['file_name'],$j,$data['product_code']);

                        $uploadData[$i]['product_image'] = $uploadPath.$image_name;
                        $uploadData[$i]['format'] = $ext;
                        
                    }  
                }
                
                if (!empty($uploadData)) {
                    $insert = $this->Product_model->insert_gallery($uploadData);
                }


                $this->session->set_flashdata('message', array(
                        'message' => 'Product Updated Successfully',
                        'class' => 'success'
                    ));
                redirect(base_url() . 'product/view_product');
                }
            }
        } else {
            
            $this->session->set_flashdata('message', array(
                'message' => "You don't have permission to access.",
                'class' => 'danger'
            ));
            redirect(base_url() . 'product/view_product');
        }
        
        $template['category'] = $this->Product_model->gets_category();
        // $template['sub_cat']  = $this->Product_model->gets_sub_category();
         $template['sub_cat']  = $this->Product_model->gets_subcategory($cat_id);

        //var_dump($template['sub_cat']);
        //die;
        $this->load->view('template', $template);
        
        
        
        
    }
    
    
    
    //////////////////////////////////////////
    //////***** PRODUCT STATUS DETAILS****///////
    public function product_status()
    {
        
        $data1 = array(
            "status" => '0'
        );
        $id    = $this->uri->segment(3);
        $s     = $this->Product_model->update_product_status($id, $data1);
        $this->session->set_flashdata('message', array(
            'message' => 'Product Disabled Successfully',
            'class' => 'warning'
        ));
        redirect(base_url() . 'product/view_product');
    }
    //////////////////////////////////////////
    //////*****ACTIVE  PRODUCT DETAILS****///////
    public function product_active()
    {
        
        $data1 = array(
            "status" => '1'
        );
        $id    = $this->uri->segment(3);
        $s     = $this->Product_model->update_product_status($id, $data1);
        $this->session->set_flashdata('message', array(
            'message' => 'Product Enabled Successfully',
            'class' => 'success'
        ));
        redirect(base_url() . 'product/view_product');
    }
    
    //////////////////////////////////////////
    //////*****DELETE PRODUCT DETAILS****///////
    public function delete_product()
    {
        
        $id     = $this->uri->segment(3);
        $result = $this->Product_model->product_delete($id);
        $this->session->set_flashdata('message', array(
            'message' => 'Product Deleted Successfully',
            'class' => 'success'
        ));
        redirect(base_url() . 'product/view_product');
    }
    //////////////////////////////////////////
    //////*****POPUP PRODUCT DETAILS****///////
    
    
    public function product_viewpopup()
    {
        $id               = $_POST['productdetailsval'];
        $template['data'] = $this->Product_model->view_popup_product($id);
        //var_dump($template['data']);
        // die;
        $this->load->view('Productdetails/product-popup-view', $template);
    }
    
    public function category()
    {
        $id          = $_POST['id'];
        $subcategory = $this->Product_model->gets_subcategory($id);
        
        foreach ($subcategory as $sub_category) {
            echo "<option value=" . $sub_category->id . ">" . $sub_category->sub_category . "</option>";
        }
    }
    
    
    
    /*uplod*/
    
    
    
    /* public function add_product()
    {
    $template['page'] = 'Productdetails/add-product';
    $template['page_title'] = 'Add Product';
    $sessid=$this->session->userdata('logged_in');
    if($_POST){
    
    //$str_cat=explode(",",$_POST['category']);
    $upload_conf = set_upload_option();
    $this->upload->initialize( $upload_conf );
    $this->load->library('upload');
    if(!empty($_FILES['product_image']['name'])){
    if (!$this->upload->do_upload('product_image')){
    //var_dump($this->upload->display_errors());
    }
    $upload_data = $this->upload->data();
    $_POST['product_image']=base_url().$upload_conf['upload_path'].'/'.$upload_data['file_name'];
    }
    $this->db->insert('product',$_POST);
    
    
    
    $id= $this->db->insert_id();
    
    if(isset($_FILES['product_images'])){
    foreach($_FILES['product_images'] as $key=>$val)
    {
    $i = 1;
    foreach($val as $v)
    {
    $field_name = "file_".$i;
    $_FILES[$field_name][$key] = $v;
    $i++;
    }
    }
    // Unset the useless one
    unset($_FILES['product_images']);
    unset($_FILES['product_image']);
    // Put each errors and upload data to an array
    $error = array();
    $success = array();
    
    // main action to upload each file
    foreach($_FILES as $field_name => $file)
    {
    if ( ! $this->upload->do_upload($field_name))
    {
    // if upload fail, grab error
    $error['upload'][] = $this->upload->display_errors();
    }
    else
    {
    // otherwise, put the upload datas here.
    // if you want to use database, put insert query in this loop
    $upload_data = $this->upload->data();
    $path=base_url().$upload_conf['upload_path'].'/'.$upload_data['file_name'];
    $res= array('product_id'=>$id,'subimg'=>$path);
    $this->db->insert('subimg',$res);
    // otherwise, put each upload data to an array.
    $success[] = $upload_data;
    }
    }
    // see what we get
    if(count($error > 0))
    {
    $data['error'] = $error;
    }
    else
    {
    $data['success'] = $upload_data;
    }
    }
    $this->session->set_flashdata('item',array('message' => 'Updated Successfully','class' => 'success'));
    
    // After that you need to used redirect function instead of load view such as
    }
    $template['brand'] = $this->Product_model->gets_brand();
    $template['category'] = $this->Product_model->gets_category();
    $template['color'] = $this->Product_model->get_color();
    $template['size'] = $this->Product_model->get_size();
    
    // var_dump($template['color']);die;
    $this->load->view('template',$template);
    }*/
    //
    
    public function view_subimages()
    {
        $template['page']       = 'Productdetails/view-subimages';
        $template['page_title'] = "Product";
        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
        $id                     = $this->uri->segment(3);
        //$template['data'] = $this->Product_model->get_single_subimg($id);
        $this->load->view('template', $template);
        
    }
    public function add_productcolor()
    {
        
        
        $template['page']       = 'Productdetails/product-color';
        $template['page_title'] = 'Add Product';
        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
        $sessid                 = $this->session->userdata('logged_in');
        
        
        
        if ($_POST) {
            
            $data = $_POST;
            
            
            $result = $this->Product_model->productcolor_add($data);
            
            if ($result) {
                
                $this->session->set_flashdata('message', array(
                    'message' => 'Product Added successfully',
                    'class' => 'success'
                ));
            } else {
                
                $this->session->set_flashdata('message', array(
                    'message' => 'Error',
                    'class' => 'error'
                ));
            }
        }
        
        
        $template['product'] = $this->Product_model->gets_product();
        $template['color']   = $this->Product_model->get_color();
        $this->load->view('template', $template);
        
        
        
    }
    
    
    public function view_color()
    {
        $template['page']       = 'Productdetails/view-productcolor';
        $template['page_title'] = "Product";
        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
        $template['data']       = $this->Product_model->get_productcolordetails();
        //var_dump($template['data']);
        // die;
        $this->load->view('template', $template);
    }
    
    
    
    
    
    //////////////////////////////////////////
    //////***** PRODUCT COLOR STATUS DETAILS****///////
    public function productcolor_status()
    {
        
        $data1 = array(
            "status" => '0'
        );
        $id    = $this->uri->segment(3);
        $s     = $this->Product_model->update_productcolor_status($id, $data1);
        $this->session->set_flashdata('message', array(
            'message' => 'Product Color Disabled Successfully',
            'class' => 'warning'
        ));
        redirect(base_url() . 'product/view_color');
    }
    //////////////////////////////////////////
    //////*****ACTIVE  PRODUCT color DETAILS****///////
    public function productcolor_active()
    {
        
        $data1 = array(
            "status" => '1'
        );
        $id    = $this->uri->segment(3);
        $s     = $this->Product_model->update_productcolor_status($id, $data1);
        $this->session->set_flashdata('message', array(
            'message' => 'Product color Enabled Successfully',
            'class' => 'success'
        ));
        redirect(base_url() . 'product/view_color');
    }
    
    
    //////////////////////////////////////////
    //////*****EDIT  PRODUCT color DETAILS****///////	
    
    public function edit_productcolor()
    {
        $template['page']       = 'Productdetails/edit-productcolor';
        $template['page_title'] = "Product";
        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
        
        
        $id                = $this->uri->segment(3);
        $template['data1'] = $this->Product_model->get_single_productcolor($id);
        if ($_POST) {
            $data = $_POST;
            
            
            
            
            $result = $this->Product_model->productcolordetails_edit($data, $id);
            if ($result) {
                
                $this->session->set_flashdata('message', array(
                    'message' => 'Product Color Updated successfully',
                    'class' => 'success'
                ));
                redirect(base_url() . 'product/view_color');
            } else {
                
                $this->session->set_flashdata('message', array(
                    'message' => 'Error',
                    'class' => 'error'
                ));
                redirect(base_url() . 'product/view_color');
            }
        }
        
        
        
        
        $template['product'] = $this->Product_model->gets_productcolor($id);
        //print_r($template['product']);
        //die;
        $template['color']   = $this->Product_model->gets_color($id);
        $this->load->view('template', $template);
        
    }
    /////////////product gallery//////////
    public function add_productgallery()
    {
        
        $template['page']       = 'Productdetails/add-productgallery';
        $template['page_title'] = "Product Gallery";
        $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
        $template['product']    = $this->Product_model->gets_product();
        
        if ($_POST) {
            $data = $_POST;
            
            
            // print_r($result);
            // die;
            $config = set_upload_product('./uploads/');
            $this->load->library('upload');
            
            $new_name            = time() . "_" . $_FILES["product_image"]['name'];
            $config['file_name'] = $new_name;
            
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('product_image')) {
                
                $this->session->set_flashdata('message', array(
                    'message' => "Display Picture : " . $this->upload->display_errors(),
                    'title' => 'Error !',
                    'class' => 'danger'
                ));
                
            } else {
                
                $upload_data           = $this->upload->data();
                $data['product_image'] = base_url() . $config['upload_path'] . "/" . $upload_data['file_name'];
                
                
                $result = $this->Product_model->add_productgallery($data);
                
                
                if ($result) {
                    
                    $this->session->set_flashdata('message', array(
                        'message' => 'Product Images Added Successfully',
                        'class' => 'success'
                    ));
                } else {
                    $this->session->set_flashdata('message', array(
                        'message' => 'Error',
                        'class' => 'error'
                    ));
                }
            }
        }
        
        
        
        
        
        
        
        $this->load->view('template', $template);
    }
    
    
    public function view_gallery()
    {
        $id = $_POST['gallery'];
        
        $template['data'] = $this->Product_model->get_gallery($id);
        $this->load->view('Productdetails/productgallery-popup-view', $template);
        
    }
    
    
    
    
}
