<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Stich_adons extends CI_Controller {



	public function __construct() {

		parent::__construct();

		check_login();

		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		date_default_timezone_set("Asia/Kolkata");

		$this->load->model('Stich_adons_model');



    }





    public function add_stiching_adons() {

    	

			$template['page'] = 'stich_adons/addons';

			$template['perm'] = $this->perm;

        	$template['main'] = $this->info->menu_name;

        	$template['sub'] = $this->info->fun_menu;

			

			  $template['adons_type'] = $this->Stich_adons_model->adons();

			   // var_dump($template['adons_type']);die;



		        if($_POST){

			  $data = $_POST;





				// print_r($data);exit();

				// die;

			$config = set_upload_category('./uploads/');

		 	$this->load->library('upload');



		 	$new_name = time()."_".$_FILES["image"]['name'];

		 	$config['file_name'] = $new_name;



		 	$this->upload->initialize($config);



		 	if ( ! $this->upload->do_upload('image')) {



		 		$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));



		 	}

		 	else

		 	{



		 		$upload_data = $this->upload->data();

		 	$data['image'] =base_url().$config['upload_path']."/".$upload_data['file_name'];



		 			//$data['created_by']=$sessid['created_user'];

					$result = $this->Stich_adons_model->add_stiching_adons($data);

					// print_r($data);die;





		 		if($result) {



		 			 $this->session->set_flashdata('message',array('message' => 'Subcategory Added Successfully','class' => 'success'));

		 		}

		 		else {

		 			 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

		 		}

		 		}

		 		}



 $this->load->view('template',$template);

			  



}

function view_stich_adons(){





			$template['page'] = 'stich_adons/view_adons';

			$template['page_title'] = "View stiching types";

			$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

			$template['stich_adons'] = $this->Stich_adons_model->view_stich_adons();



			// print_r($template['stich_adons']);die;



			$this->load->view('template',$template);



}





/*stich status*/

	 public function stich_status(){



 				  $data1 = array(

 				  "status" => '0'

 							 );

 				  $id = $this->uri->segment(3);

 				  $s=$this->Stich_adons_model->update_adons_status($id, $data1);

 				  $this->session->set_flashdata('message', array('message' => 'Banner Disabled Successfully','class' => 'warning'));

 				  redirect(base_url().'Stich_adons/view_stich_adons');

 	    }



 		public function stich_active(){



 				  $data1 = array(

 				  "status" => '1'

 							 );

 				  $id = $this->uri->segment(3);

 				  $s=$this->Stich_adons_model->update_adons_status($id, $data1);

 				  $this->session->set_flashdata('message', array('message' => 'Banner Enabled Successfully','class' => 'success'));

 				  redirect(base_url().'Stich_adons/view_stich_adons');

 	    }













 	    public function edit_adons(){

		

		$template['page'] = 'stich_adons/editadons';

		$template['page_title'] = "edit stiching adons";

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;



		$template['adons_type'] = $this->Stich_adons_model->adons();

		// print_r(expression)

		$id = $this->uri->segment(3);

		$template['stich1'] = $this->Stich_adons_model->edit_adons($id);

		// print_r($template['stich1']);die;

		if(!empty($template['stich1'])) {

			if($_POST){

				$data = $_POST;

				//  var_dump($_FILES);die;

				//upload image

				$config = set_upload_category('./uploads/');

				

				// $config = set_upload_subcategory('assets/uploads/subcategory');

				$this->load->library('upload');

				$new_name = time()."_".$_FILES['image'];

				$config['file_name'] = $new_name;

				

				$this->upload->initialize($config);

				$upload_data = $this->upload->data();

				$data['image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

				if (!$this->upload->do_upload('image'))

				{

					$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));

				}

				else

				{

					$upload_data = $this->upload->data();

					$data['image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

				}

				$result = $this->Stich_adons_model->update_adons_status($id,$data);

				redirect(base_url().'Stich_adons/view_stich_adons');

			}

	 	}

	 	else

		{

			$this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));

			redirect(base_url().'Stich_adons/view_stich_adons');

	 	}

		$this->load->view('template',$template);

}



 



}