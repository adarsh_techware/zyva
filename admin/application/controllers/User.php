<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller
{

    
    public function __construct()
    {
        
        parent::__construct();
        
        check_login();
        
        $class      = $this->router->fetch_class();
        $method     = $this->router->fetch_method();
        $this->info = get_function($class, $method);
        
        $userdata = $this->session->userdata('userdata');
        
        $role_id = $userdata['user_type'];
        
        if (!privillage($class, $method, $role_id)) {
            redirect('wrong');
        }
        $this->perm = get_permit($role_id);
        
        $this->load->model('Users_model');
        
        
    }
    
    
    
    public function index()
    {
        
        $template['page'] = 'Users/view_users';
        
        $template['page_title'] = "User";
        
        $template['perm'] = $this->perm;
        
        $template['main'] = $this->info->menu_name;
        
        $template['sub'] = $this->info->fun_menu;
        
        $template['data'] = $this->Users_model->get_userdetails();
        
        $this->load->view('template', $template);
        
    }
    
    
    
    public function create()
    {
        
        
        
        $template['page'] = 'Users/add-users';
        
        $template['page_title'] = 'Add Users';
        
        $template['perm'] = $this->perm;
        
        $template['main'] = $this->info->menu_name;
        
        $template['sub'] = $this->info->fun_menu;
        
        $sessid = $this->session->userdata('logged_in');
        
        if ($_POST) {
            
            $data = $_POST;
            
            /*$config = set_upload_category('../uploads/');
            
            $this->load->library('upload');
            
            $new_name = time() . "_" . $_FILES["profile_picture"]['name'];
            
            $config['file_name'] = $new_name;
            
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('profile_picture')) {
                
                $this->session->set_flashdata('message', array(
                    'message' => "Display Picture : " . $this->upload->display_errors(),
                    'title' => 'Error !',
                    'class' => 'danger'
                ));
                
            }
            
            else {
                
                $upload_data = $this->upload->data();
                
                $data['profile_picture'] = base_url() . $config['upload_path'] . "/" . $upload_data['file_name'];*/
                
                $result = $this->Users_model->registrationdetails_add($data);
                
                if ($result) {
                    
                    
                    
                    $this->session->set_flashdata('message', array(
                        'message' => 'Add users Details successfully',
                        'class' => 'success'
                    ));
                    
                }
                
                else {
                    
                    $this->session->set_flashdata('message', array(
                        'message' => 'Error',
                        'class' => 'error'
                    ));
                    
                }
                
            //}
            
        }
        
        $template['role'] = $this->Users_model->gets_role();
        
        
        
        $this->load->view('template', $template);
        
        
        
    }

    public function edit_user($id=null)
    {
    	if($id==null){
    		redirect(base_url('user'));
    	}

    	$template['page'] = 'Users/user_edit';
        
        $template['page_title'] = 'Edit Users';
        
        $template['perm'] = $this->perm;

        $template['id'] = $id;
        
        $template['main'] = $this->info->menu_name;

        $template['role'] = $this->Users_model->gets_role();
        
        $template['sub'] = $this->info->fun_menu;

    	$rs = $this->db->where('id',$id)->get('users')->row();
    	
    	if(!empty($rs)){
    		$template['data'] = $rs;
    		$this->load->view('template',$template);
    	} else {
    		redirect(base_url('user'));
    	}
    }

    public function update_user($id=null){
    	$data = $_POST;
    		if(isset($data['password']) && $data['password']!=''){
    			$data['password'] = md5($data['password']);
    		} else {
    			unset($data['password']);
    		}

    		$this->db->where('id',$id)->update('users',$data);
    		$this->session->set_flashdata('message', array(
                        'message' => 'modify users details successfully',
                        'class' => 'success'
                    ));
    		redirect(base_url('user'));
    }
    
    
    
}