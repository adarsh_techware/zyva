<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class image extends CI_Controller {

	public function __construct() {
	parent::__construct();
		check_login();

		$this->load->model('image_model');

    }

    function view(){
      $template['page'] = 'imguploading/img-upload';
      $template['page_title'] = 'Add image';
        $this->load->view('template',$template);
    }

    public function add_image() {

			  $template['page'] = 'image/image';
			  $template['page_title'] = 'Add image';
        //  $this->load->view('template',$template);
		      $sessid=$this->session->userdata('logged_in');
        //
		      if($_POST){
			  $data = $_POST;


      $result = $this->image_model->addimage($data);
				// print_r($result);
				// die;
				if($result) {

					 $this->session->set_flashdata('message',array('message' => '  successfully','class' => 'success'));
				}
				else {
					/* Set error message */
					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));
				}
				}



			  $this->load->view('template',$template);

}
  }
  ?>
