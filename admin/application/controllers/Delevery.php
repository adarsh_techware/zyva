<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delevery extends CI_Controller {

	public function __construct() {
	parent::__construct();
	check_login();
	$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);
		$this->load->model('Delevery_model');
		

    }

    //////*****ADD reseller DETAILS****///////
	  public function create()
	  {
		
	    $template['page'] = 'delevery/create';
		$template['page_title'] = 'Create delevery location';
		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
		   if($_POST){
			  $data = $_POST;

			   //print_r($data);die;

		     $result = $this->Delevery_model->delevery_add($data);
					


		 		if($result) {

		 			 $this->session->set_flashdata('message',array('message' => 'Add delevery location successfully','class' => 'success'));
		 		}
		 		else {
		 			 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));
		 		}
		 		}
		
	   $this->load->view('template',$template);
     }


     public function view_location(){


			  $template['page'] = 'delevery/view_location';
			  $template['page_title'] = "Delevery Location";
			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
			  $template['location'] = $this->Delevery_model->get_location();
			  
			  $this->load->view('template',$template);
	 }


	 public function location_status(){

 				  $data1 = array(
 				  "status" => '0'
 							 );
 				  $id = $this->uri->segment(3);
 				  $s=$this->Delevery_model->update_location_status($id, $data1);
 				  $this->session->set_flashdata('message', array('message' => 'Reseller Disabled Successfully','class' => 'warning'));
 				  redirect(base_url().'Delevery/view_location');
 	    }

 		public function location_active(){

 				  $data1 = array(
 				  "status" => '1'
 							 );
 				  $id = $this->uri->segment(3);
 				  $s=$this->Delevery_model->update_location_status($id, $data1);
 				  $this->session->set_flashdata('message', array('message' => 'Reseller Enabled Successfully','class' => 'success'));
 				  redirect(base_url().'Delevery/view_location');
 	    }



public function edit(){
		
		$template['page'] = 'Delevery/edit';
		$template['page_title'] = "edit location";
		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;
		$id = $this->uri->segment(3);
		$template['location'] = $this->Delevery_model->edit_location($id);
		// print_r($template['Category1']);die;
		if(!empty($template['location'])) {
			if($_POST){
				$data = $_POST;
								$result = $this->Delevery_model->update_location($id,$data);
				redirect(base_url().'Delevery/view_location');
			}
	 	}
	 	else
		{
			$this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));
			redirect(base_url().'Delevery/view_location');
	 	}
		$this->load->view('template',$template);
}




}
?>