<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Promocode_ctrl extends CI_Controller {



	public function __construct() {

	parent::__construct();

	$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		check_login();

		date_default_timezone_set("Asia/Kolkata");

		$this->load->model('promocode_model');



    }



    



//to add Promocode

  public function add_promocode() {



			  $template['page'] = 'promocode/zyva_promocode';

			  $template['page_title'] = 'Add promocode';

			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

        //  $this->load->view('template',$template);

		      $sessid=$this->session->userdata('logged_in');

    //     //

		      if($_POST){

			  $data = $_POST;





      $result = $this->promocode_model->add_promocode($data);

				// print_r($result);

				// die;

				if($result) {



					 $this->session->set_flashdata('message',array('message' => 'Registered  successfully','class' => 'success'));

				}

				else {

					/* Set error message */

					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

				}

				}







			  $this->load->view('template',$template);



}

//  //view all Promocode

  function view_promocode(){



		  $template['page'] = 'promocode/view_promocode';

          $template['page_title'] = 'promocode';

          $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		  $template['data'] = $this->Promocode_model->get_promocode_details();

		  $this->load->view('template',$template);



	  }



// // delete Promocode

// function delete_promocode(){



// 		  $data1 = array(

// 				  "promocode_status" => '1'

// 							 );

// 		  $id = $this->uri->segment(3);

// 		  $result = $this->Promocode_model->delete_promocode($id,$data1);

// 		  $this->session->set_flashdata('message', array('message' => 'Deleted Successfully','class' => 'success'));

// 		  redirect(base_url().'Promocode_ctrl/view_promocode');

// 	  }

// // edit Promocode



//  function edit_promocode(){

// 		  $template['page'] = 'promocode/editpromocode_view';

// 		  $template['title'] = 'Edit promocode';





// 		   $id = $this->uri->segment(3);

// 		   $template['data'] = $this->Promocode_model->editget_promocode_id($id);

// 		   if(!empty($template['data'])){



// 		  if($_POST){

// 			  $data = $_POST;







// 			      $result = $this->Promocode_model->edit_promocode($data, $id);

// 				  redirect(base_url().'Promocode_ctrl/view_promocode');



// 		         }

// 		    }

// 			else{

// 				 $this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));

// 			     redirect(base_url().'Promocode_ctrl/view_promocode');

// 			}





// 			 $this->load->view('template',$template);







//                    }

//  //popup Promocode

// 	  public function promodetails_view(){



// 	           $id=$_POST['promodetails'];

// 	           $template['data'] = $this->Promocode_model->get_promopopupdetails($id);

// 	           $this->load->view('promocode/promocode-popup',$template);



//          }

}

