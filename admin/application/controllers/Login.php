<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('login_model');
        if ($this->session->userdata('logged_in')) {
            redirect(base_url() . 'dashboard');
        }
        
    }
    public function index() {
        
        $template['page_title'] = "Login";
        if (isset($_POST)) {
            $this->load->library('form_validation');
            
            
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
            
            if ($this->form_validation->run() == TRUE) {
                redirect(base_url() . 'dashboard');
            }
        }
        
        $this->load->view('login-form');
    }
    
    function check_database($password) {
        
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $result   = $this->login_model->login($username, $password);
        
        if ($result) {
            $session_array = array(
                'id' => $result->id,
                'username' => $result->username,
                'profile_picture' => $result->profile_picture
            );
            if (isset($result->role_id)) {
                $session_array['user_type'] = $result->role_id;
            } else {
                $session_array['user_type'] = 1;
            }
            $this->session->set_userdata('logged_in', true);
            $this->session->set_userdata('userdata', $session_array);
            return TRUE;
            
        } else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
        
    }
    
    
    
}
