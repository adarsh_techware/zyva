<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Color_ctrl extends CI_Controller {



	public function __construct() {

	parent::__construct();

check_login();

		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		date_default_timezone_set("Asia/Kolkata");

		$this->load->model('color_model');



    }



    public function add_color() {



			  $template['page'] = 'color/color';

			  $template['page_title'] = 'Add Color';

			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

        //  $this->load->view('template',$template);

		      $sessid=$this->session->userdata('logged_in');

        //

		      if($_POST){

			  $data = $_POST;





			//$config = set_upload_color('assets/uploads/color');

			   $config = set_upload_category('./uploads/');

		$this->load->library('upload');



		$new_name = time()."_".$_FILES["image"]['name'];

		$config['file_name'] = $new_name;



		$this->upload->initialize($config);



		if ( ! $this->upload->do_upload('image')) {



			$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));



		}

		else

		{



			$upload_data = $this->upload->data();

		//$data['image'] =$config['upload_path']."/".$upload_data['file_name'];

		$data['image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];



				//$data['created_by']=$sessid['created_user'];

				$result = $this->color_model->add_color($data);







			if($result) {

				 //array_walk($data, "remove_html");

				/* Set success message */

				 $this->session->set_flashdata('message',array('message' => 'Add color Details successfully','class' => 'success'));

			}

			else {

				/* Set error message */

				 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

			}

			}

			}



			$this->load->view('template',$template);

}

  function view_color(){



			$template['page'] = 'color/view_color';

			$template['page_title'] = "View color";

			$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

			$template['color'] = $this->color_model->view_color();

			$this->load->view('template',$template);



}





 function delete_color(){



 $id = $this->uri->segment(3);

 $result= $this->color_model->color_delete($id);

 $this->session->set_flashdata('message', array('message' => 'Deleted Successfully','class' => 'success'));

 redirect(base_url().'color_ctrl/view_color');

 }



function edit_color()

 {

 			 $template['page'] = 'color/edit_color';

 			 $template['page_title'] = "edit color";

 			 $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

 			 $id = $this->uri->segment(3);



 			  $template['color'] = $this->color_model->view_color();

			 $template['col'] = $this->color_model->edit_color($id);

			 //print_r($template['col']);die;

			 // $result = $this->color_model->update_color( $id,$data);

				//  redirect(base_url().'color_ctrl/view_color');





			 if(!empty($template['col'])) {

			if($_POST){

				$data = $_POST;

				//  var_dump($_FILES);die;

				//upload image



				if(isset($_FILES['image']) && $_FILES['image']['name']!=''){

					$config = set_upload_category('./uploads/');

				

				//$config = set_upload_color('assets/uploads/color');

					$this->load->library('upload');

					$new_name = time()."_".$_FILES['image'];

					$config['file_name'] = $new_name;

					

					$this->upload->initialize($config);

					$upload_data = $this->upload->data();

					//$data['image'] =$config['upload_path']."/".$upload_data['file_name'];

					$data['image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

					if (!$this->upload->do_upload('image'))

					{

						$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));

					} else {

						$upload_data = $this->upload->data();

						//$data['image'] =$config['upload_path']."/".$upload_data['file_name'];

						$data['image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

					}

				}

				

				

				$result = $this->color_model->update_color( $id,$data);

				if($result){

					$this->session->set_flashdata('message', array('message' => "Product Color Details Updated successfully", 'title' => 'Error !', 'class' => 'success'));

				}

				redirect(base_url().'color_ctrl/view_color');

			}

	 	}

	 	else

		{

			$this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));

			redirect(base_url().'color_ctrl/view_color');

	 	}

						

			

				



				 $this->load->view('template',$template);



   }



	 /*color status*/

	public function color_status(){



					 $data1 = array(

					 "status" => '0'

								);

					 $id = $this->uri->segment(3);

					 $s=$this->color_model->update_color_status($id, $data1);

					 $this->session->set_flashdata('message', array('message' => 'Color Successfully Disabled','class' => 'warning'));

					 redirect(base_url().'color_ctrl/view_color');

			 }



		 public function Color_active(){



					 $data1 = array(

					 "status" => '1'

								);

					 $id = $this->uri->segment(3);

					 $s=$this->color_model->update_color_status($id, $data1);

					 $this->session->set_flashdata('message', array('message' => 'Color Successfully Enabled','class' => 'success'));

					 redirect(base_url().'color_ctrl/view_color');

			 }





  }

  ?>

