<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Brand_ctrl extends CI_Controller {



	public function __construct() {

	parent::__construct();

		check_login();

		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		date_default_timezone_set("Asia/Kolkata");

		$this->load->model('Brand_model');

		

		if(!$this->session->userdata('logged_in')) { 

			redirect(base_url());

		}



    }

	

/////////////////////////////////////////////////

/////*******ADD LANGUAGES DETAILS******/////////

	 public function add_brand(){

			

			  $template['page'] = "branddetail/add-brand";

			  $template['page_title'] = "Add Brand";

			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		  

		      if($_POST){		  

			  $data = $_POST;

			  

			 //print_r($data);

			//die;

			    

			    $result = $this->Brand_model->add_branddetails($data);

				if($result) {

					/* Set success message */

					 $this->session->set_flashdata('message',array('message' => 'Add Brand Details successfully','class' => 'success'));

				}

				else {

					/* Set error message */

					 $this->session->set_flashdata('message', array('message' => 'Brand Detail already exist','class' => 'error'));  

				}

				}

			  $template['data'] = $this->Brand_model->get_branddetails();

			  $this->load->view('template',$template);



	 }

	 



/////////////////////////////////////////////////

/////*******DELETE LANGUAGES DETAILS******//////

	 

	 public function delete_brand()

	 {

		      $id = $this->uri->segment(3);

		      $result= $this->Brand_model->brand_delete($id);

		      $this->session->set_flashdata('message', array('message' => 'Deleted Successfully','class' => 'success'));

	          redirect(base_url().'Brand_ctrl/add_brand');

	 }

	 

///////////////////////////////////////////

////*******EDIT LANGUAGES DETAILS******////

	 public function edit_branddetailsdval(){

		

		      $template['page'] = "branddetail/edit-brand-details";

			  $template['page_title'] = "Edit Brand";

			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;



		      $id = $this->uri->segment(3); 

		      $template['data'] = $this->Brand_model->get_single_brand($id);

			 

		      if($_POST){

			  $data = $_POST;

				

				//array_walk($data, "remove_html");

				

			  	$result = $this->Brand_model->branddetails_edit($data, $id);

				if($result) {

					/* Set success message */

					 $this->session->set_flashdata('message',array('message' => 'Edit Brand Details Updated successfully','class' => 'success'));

					  redirect(base_url().'Brand_ctrl/add_brand');

				}

				else {

					/* Set error message */

					 $this->session->set_flashdata('message', array('message' => 'Brand Detail already exist','class' => 'error')); 

					 redirect(base_url().'Brand_ctrl/add_brand');

				}

				}	

	      $this->load->view('template',$template); 	

	 }

	 	

	

}