<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Banner_ctrl extends CI_Controller {



	public function __construct() {

	parent::__construct();

		check_login();

		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		date_default_timezone_set("Asia/Kolkata");

		$this->load->model('Banner_model');

		

    }



///////////////////////////////////

//////****REGISTRATION DETAILS***///////

     public function view_bannerdetails(){

		 

	  

			    $template['page'] = 'bannerdetail/view-bannerdetails';

			  $template['page_title'] = "";

			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

			  $template['data'] = $this->Banner_model->get_bannerdetails();

			  $this->load->view('template',$template);

	 }

	



//////////////////////////////////////////

//////*****ADD BANNER DETAILS****///////		 

	  public function add_banner() { 

	 

			  $template['page'] = 'bannerdetail/add-banner';

			  $template['page_title'] = 'Add Registration';	

			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;		  

		      $sessid=$this->session->userdata('logged_in');		  

			  

		      if($_POST){		  

			  $data = $_POST;

			  

				

				

		   $config = set_upload_banner('./uploads/');

			$this->load->library('upload');

			

			$new_name = time()."_".$_FILES["banner_image"]['name'];

			$config['file_name'] = $new_name;



			$this->upload->initialize($config);



			if ( ! $this->upload->do_upload('banner_image')) {

				

				$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));

				

			}

			else 

			{

				

		    $upload_data = $this->upload->data();

			$data['banner_image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];	

				//var_dump($data['banner_image']);

				//die;

			    

			    $result = $this->Banner_model->registrationdetails_add($data);

			

				if($result) {

					

					 $this->session->set_flashdata('message',array('message' => 'Add Banner Details successfully','class' => 'success'));

				}

				else {

					

					 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));  

				}

			}

			  }	 

             	  

			  $this->load->view('template',$template);

	 

	 	 

	  }

	//////////////////////////////////////////

//////*****DELETE DETAILS****///////

	  public function delete_banner(){

			      

		  $id = $this->uri->segment(3);

		  $result= $this->Banner_model->banner_delete($id);

		  $this->session->set_flashdata('message', array('message' => 'Deleted Successfully','class' => 'success'));

	      redirect(base_url().'Banner_ctrl/view_bannerdetails');

	    }

  public function banner_status(){

		 

				  $data1 = array(

				  "status" => '0'

							 );

				  $id = $this->uri->segment(3);		   

				  $s=$this->Banner_model->update_banner_status($id, $data1);

				  $this->session->set_flashdata('message', array('message' => 'Banner Successfully Disabled','class' => 'warning'));

				  redirect(base_url().'Banner_ctrl/view_bannerdetails');

	    }



		public function banner_active(){

		 

				  $data1 = array(

				  "status" => '1'

							 );

				  $id = $this->uri->segment(3);		   

				  $s=$this->Banner_model->update_banner_status($id, $data1);

				  $this->session->set_flashdata('message', array('message' => 'Banner Successfully Enabled','class' => 'success'));

				  redirect(base_url().'Banner_ctrl/view_bannerdetails');

	    }

//////////////////////////////////////////

//////*****EDIT DETAILS****///////

     public function edit_banner(){

		

		      /*$template['page'] = 'bannerdetail/edit-banner-details';

		      $template['page_title'] = 'Edit Patient';



		      $id = $this->uri->segment(3); 

		      $template['data'] = $this->Banner_model->get_single_banner($id);

			  //var_dump($template['data']);

			  //die;

	    if(!empty($template['data'])) 

		{ 

				

				  //echo "hi";

				 // exit;

		      if($_POST)

			{

		

			  $data = $_POST;

			

			   

 

			if(isset($_FILES['banner_image'])) 

		{  

			$config = set_banner_image('assets/uploads/banner');

			$this->load->library('upload');

			

			$new_name = time()."_".$_FILES["banner_image"]['name'];

			$config['file_name'] = $new_name;



			$this->upload->initialize($config);



			if ( ! $this->upload->do_upload('banner_image')) {

					unset($data['banner_image']);

				}

				else {

					$upload_data = $this->upload->data();

					$data['banner_image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

				}

		}



			  	$result = $this->Banner_model->bannerdetails_edit($data, $id);

					

					if($result) 

				{

					

					 $this->session->set_flashdata('message',array('message' => 'Edit Banner Details  successfully','class' => 'success'));

					redirect(base_url().'Banner_ctrl/view_bannerdetails');

				}

			}

			

			

				else 

				{

					

					 $this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));	

					redirect(base_url().'Banner_ctrl/view_bannerdetails');

				}

				

					

					  $this->load->view('template',$template); 	

	    }



	}*/

	    $template['page'] = 'bannerdetail/edit-banner-details';

		$template['page_title'] = 'Edit Banner';

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$id = $this->uri->segment(3);

		 $template['data'] = $this->Banner_model->get_single_banner($id);

		// print_r($template['Category1']);die;

		 if(!empty($template['data'])) 

		 {

			if($_POST){

				$data = $_POST;

				

				$config = set_upload_banner('./uploads/');

				$this->load->library('upload');

				$new_name = time()."_".$_FILES['banner_image'];

				$config['file_name'] = $new_name;

				

				$this->upload->initialize($config);

				$upload_data = $this->upload->data();

				$data['banner_image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];	

				if (!$this->upload->do_upload('banner_image'))

				{

					$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));

				}

				else

				{

					$upload_data = $this->upload->data();

					$data['banner_image'] = $config['upload_path']."/".$upload_data['file_name'];

				}

				$result = $this->Banner_model->bannerdetails_edit($data, $id);

				redirect(base_url().'Banner_ctrl/view_bannerdetails');

			}

	 	}

	 	else

		{

			$this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));

			redirect(base_url().'Banner_ctrl/view_bannerdetails');

	 	}

		$this->load->view('template',$template);

	

}

	

	

	

	

	

}