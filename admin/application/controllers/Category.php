<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Category extends CI_Controller {



	public function __construct() {

	parent::__construct();

check_login();

		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		date_default_timezone_set("Asia/Kolkata");

		$this->load->model('Category_model');



    }



    public function index(){



			$template['page'] = 'category/view_category';

			$template['page_title'] = "View category";

			$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

			$template['Category'] = $this->Category_model->view_category();

			$this->load->view('template',$template);



	}



    public function create() {



			  $template['page'] = 'category/add_category';

			  $template['page_title'] = 'Add Category';

			  $template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

        //  $this->load->view('template',$template);

		      $sessid=$this->session->userdata('logged_in');

        //

		      if($_POST){

			  $data = $_POST;





				// print_r($data);exit();

				// die;



			  // /===================================no image needed



			// $config = set_upload_category('./uploads/');

		 // 	$this->load->library('upload');



		 // 	$new_name = time()."_".$_FILES["category_image"]['name'];

		 // 	$config['file_name'] = $new_name;



		 // 	$this->upload->initialize($config);



		 // 	if ( ! $this->upload->do_upload('category_image')) {



		 // 		$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));



		 // 	}

		 // 	else

		 // 	{



		 // 		$upload_data = $this->upload->data();

		 // 	$data['category_image'] =base_url().$config['upload_path']."/".$upload_data['file_name'];



			   // /===================================no image needed

		 



		 			//$data['created_by']=$sessid['created_user'];

					$result = $this->Category_model->add_category($data);

					// print_r($data);die;





			 		if($result) {



			 			 $this->session->set_flashdata('message',array('message' => 'Category Added Successfully','class' => 'success'));

			 		}

			 		else {

			 			 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

			 		}

		 		}

		 		// }



		 		$this->load->view('template',$template);



}





	public function edit(){

		

		$template['page'] = 'category/edit_category';

		$template['page_title'] = "edit category";

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$id = $this->uri->segment(3);

		$template['Category1'] = $this->Category_model->edit_category($id);

		// print_r($template['Category1']);die;

		if(!empty($template['Category1'])) {

			if($_POST){

				$data = $_POST;

				//  var_dump($_FILES);die;

				//upload image

				/*$config = set_upload_category('./uploads/');

				

				// $config = set_upload_subcategory('assets/uploads/subcategory');

				$this->load->library('upload');

				$new_name = time()."_".$_FILES['category_image'];

				$config['file_name'] = $new_name;

				

				$this->upload->initialize($config);

				$upload_data = $this->upload->data();

				$data['category_image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

				if (!$this->upload->do_upload('category_image'))

				{

					$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));

				}

				else

				{

					$upload_data = $this->upload->data();

					$data['category_image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

				}*/

				$result = $this->Category_model->update_category($id,$data);

				if($result) {



			 			 $this->session->set_flashdata('message',array('message' => 'Subategory Updated Successfully','class' => 'success'));

			 		}

			 		else {

			 			 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

			 		}

				redirect(base_url().'category');

			}

	 	}

	 	else

		{

			$this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));

			redirect(base_url().'category');

	 	}

		$this->load->view('template',$template);

}



 function category_delete(){



 $id = $this->uri->segment(3);

 $result= $this->Category_model->category_delete($id);

 $this->session->set_flashdata('message', array('message' => 'Deleted Successfully','class' => 'success'));

 redirect(base_url().'category');

 }







 function delete_subcat(){



 $id = $this->uri->segment(3);

 $result= $this->Category_model->delete_subcat($id);

 $this->session->set_flashdata('message', array('message' => 'Deleted Successfully','class' => 'success'));

 redirect(base_url().'Category/view_subcategory');

 }







public function create_subcategory() {



	 $template['page'] = 'subcategory/add_subcategory';

	 $template['page_title'] = 'Add Sub Category';

	 //  $this->load->view('template',$template);

		 $sessid=$this->session->userdata('logged_in');

		 // $template['Category'] = $this->Category_model->view_category();

		 $template['Category'] = $this->Category_model->get_category();



			if($_POST){

		$data = $_POST;





/*

	$config = set_upload_category('./uploads/');

	$this->load->library('upload');



	$new_name = time()."_".$_FILES["sub_image"]['name'];

	$config['file_name'] = $new_name;



	$this->upload->initialize($config);



	if ( ! $this->upload->do_upload('sub_image')) {



		$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));



	}

	else

	{



		$upload_data = $this->upload->data();

	$data['sub_image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];



			//$data['created_by']=$sessid['created_user'];*/

			 $result = $this->Category_model->add_subcategory($data);





		if($result) {

			 //array_walk($data, "remove_html");

			/* Set success message */

			 $this->session->set_flashdata('message',array('message' => 'Subcategory Added Successfully','class' => 'success'));

		}

		else {

			/* Set error message */

			 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

		}

		//}

		}



		$this->load->view('template',$template);





}

function view_subcategory(){



			$template['page'] = 'subcategory/view_subcategory';

			$template['page_title'] = "View Subcategory";

			$template['sub_category'] = $this->Category_model->view_subcategory();

			$this->load->view('template',$template);



}







function edit_subcategory()

 {

 			 $template['page'] = 'subcategory/editsub_cat';

 			 $template['page_title'] = "edit sub_category";

 			 $id = $this->uri->segment(3);





			 $template['subcat'] = $this->Category_model->edit_subcategory($id);

			 // 			 // print_r($template['Category1']);die;

			 	if(!empty($template['subcat'])) {



						  if($_POST)

						  {

							 $data = $_POST;

							//  $data['user_id']=$userid;

						  //    $data['user_id']=$userid;

							//

					//upload image

					/*$config = set_upload_category('./uploads/');



							// $config = set_upload_subcategory('assets/uploads/subcategory');

							      $this->load->library('upload');



						    $new_name = time()."_".$_FILES["sub_image"]['name'];

							$config['file_name'] = $new_name;

						    $this->upload->initialize($config);



								 if ( ! $this->upload->do_upload('sub_image'))

								  {



									$this->session->set_flashdata('message', array('message' => "Display Picture : ".$this->upload->display_errors(), 'title' => 'Error !', 'class' => 'danger'));



								  }

								  else

								   {



								  $upload_data = $this->upload->data();

								  $data['sub_image'] = base_url().$config['upload_path']."/".$upload_data['file_name'];

								   }*/





			 	  $result = $this->Category_model->update_subcategory($id,$data);

			 	  $this->session->set_flashdata('message', array('message' => 'Subcategory Updated Successfully','class' => 'success'));

				  redirect(base_url().'category/view_subcategory');



		   }

				  }

				  else{

					   $this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));

			           redirect(base_url().'Category/view_subcategory');

				  }



				//  $template['result'] = $this->Category_model->view_category();

				 $template['Category'] = $this->Category_model->view_category();

	             // $template['result1'] = $this->Category_model->get_sub_category();

			    //  $template['sub_catresult'] = $this->Category_model->get_sub_categorydetails();

				 $this->load->view('template',$template);



   }







	 /*Category status*/

	 public function category_status(){



 				  $data1 = array(

 				  "status" => '0'

 							 );

 				  $id = $this->uri->segment(3);

 				  $s=$this->Category_model->update_category_status($id, $data1);

 				  $this->session->set_flashdata('message', array('message' => 'Category Disabled Successfully','class' => 'warning'));

 				  redirect(base_url().'category');

 	    }



 		public function Category_active(){



 				  $data1 = array(

 				  "status" => '1'

 							 );

 				  $id = $this->uri->segment(3);

 				  $s=$this->Category_model->update_category_status($id, $data1);

 				  $this->session->set_flashdata('message', array('message' => 'Category Enabled Successfully','class' => 'success'));

 				  redirect(base_url().'category');

 	    }



			public function subcategory_status(){



	  				  $data1 = array(

	  				  "status" => '0'

	  							 );

	  				  $id = $this->uri->segment(3);

	  				  $s=$this->Category_model->update_category_status1($id, $data1);

	  				  $this->session->set_flashdata('message', array('message' => 'Subcategory Disabled Successfully','class' => 'warning'));

	  				  redirect(base_url().'category/view_subcategory');

	  	    }



	  		public function subCategory_active(){



	  				  $data1 = array(

	  				  "status" => '1'

	  							 );

	  				  $id = $this->uri->segment(3);

	  				  $s=$this->Category_model->update_category_status1($id, $data1);

	  				  $this->session->set_flashdata('message', array('message' => 'Subcategory Enabled Successfully','class' => 'success'));

	  				  redirect(base_url().'category/view_subcategory');

	  	    }





  }

  ?>

