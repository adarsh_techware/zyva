<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Main_menu extends CI_Controller {



	public function __construct() {

		parent::__construct();

		check_login();

		date_default_timezone_set("Asia/Kolkata");

		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		$this->load->model('Menu_model');
    }



    public function index(){



			$template['page'] = 'menu/view_menu';

			$template['page_title'] = "View Main Menu";

			$template['perm'] = $this->perm;

        	$template['main'] = $this->info->menu_name;

        	$template['sub'] = $this->info->fun_menu;

			$template['menus'] = $this->Menu_model->view_main();

			$this->load->view('template',$template);



	}



    public function create_main() {
		$template['page'] = 'menu/create_menu';

		$template['page_title'] = 'Create Menu';

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

        $sessid=$this->session->userdata('logged_in');

        if($_POST){

			$data = $_POST;


			$result = $this->Menu_model->create_main($data);
			if($result) {

			 	$this->session->set_flashdata('message',array('message' => 'New Menu created Successfully','class' => 'success'));

			}else {

			 	$this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

			}

		}

		$this->load->view('template',$template);


	}





	public function edit_main($id=null){

		$template['page'] = 'menu/edit_menu';

		$template['page_title'] = "Change Menu";

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$id = $this->uri->segment(3);

		if($id==null){
			redirect(base_url('main_menu'));
		}

		$template['menu'] = $this->Menu_model->edit_menu($id);

		if(empty($template['menu'])){
			redirect(base_url('main_menu'));
		}

		// print_r($template['Category1']);die;

			if($_POST){

				$data = $_POST;

				$result = $this->Menu_model->update_menu($id,$data);

				if($result) {



			 			 $this->session->set_flashdata('message',array('message' => 'Menu Updated Successfully','class' => 'success'));

			 		}

			 		else {

			 			 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

			 		}

				redirect(base_url().'main_menu');

			}

	 	

		$this->load->view('template',$template);

}



public function function_view(){



			$template['page'] = 'menu/view_function';

			$template['page_title'] = "View Sub Menus";

			$template['perm'] = $this->perm;

        	$template['main'] = $this->info->menu_name;

        	$template['sub'] = $this->info->fun_menu;

			$template['menus'] = $this->Menu_model->view_function();

			$this->load->view('template',$template);



	}



    public function create_function() {
		$template['page'] = 'menu/create_function';

		$template['page_title'] = 'Create Sub Menu';

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$template['main_menu'] = $this->db->get('main_menu')->result();

        $sessid=$this->session->userdata('logged_in');

        if($_POST){

			$data = $_POST;


			$result = $this->Menu_model->create_function($data);
			if($result) {

			 	$this->session->set_flashdata('message',array('message' => 'New Sub Menu created Successfully','class' => 'success'));

			}else {

			 	$this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

			}

		}

		$this->load->view('template',$template);


	}





	public function edit_function(){

		$template['page'] = 'menu/edit_function';

		$template['page_title'] = "Change Sub Menu";

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$id = $this->uri->segment(3);

		if($id==null){
			redirect(base_url().'main_menu/function_view');
		}

		$template['function'] = $this->Menu_model->edit_function($id);

		$template['main_menu'] = $this->db->get('main_menu')->result();

		// print_r($template['Category1']);die;

			if($_POST){

				$data = $_POST;

				$result = $this->Menu_model->update_function($id,$data);

				if($result) {



			 			 $this->session->set_flashdata('message',array('message' => 'function Updated Successfully','class' => 'success'));

			 		}

			 		else {

			 			 $this->session->set_flashdata('message', array('message' => 'Error','class' => 'error'));

			 		}

				redirect(base_url().'main_menu/function_view');

			}

	 	

		$this->load->view('template',$template);

	}







}

  ?>

