<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class enquiry extends CI_Controller {



	public function __construct() {

		parent::__construct();

		check_login();

		$class = $this->router->fetch_class();
        $method = $this->router->fetch_method();            
        $this->info = get_function($class,$method);

        $userdata = $this->session->userdata('userdata');

        //print_r($this->session->userdata());
        
        $role_id = $userdata['user_type'];
        
        if(!privillage($class,$method,$role_id)){
            redirect('wrong');
        }   
        $this->perm = get_permit($role_id);

		$this->load->model('Enquiry_model');



    }





	function index(){



		$template['page'] = 'enquiry/view';

		$template['page_title'] = "View Enquiry";

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$template['enquiry'] = $this->Enquiry_model->view_enquiry();

		$this->load->view('template',$template);



	}





	public function edit_customer(){

		$template['page'] = 'customer/edit_user';

		$template['page_title'] = "Edit Userdetail";

		$template['perm'] = $this->perm;

        $template['main'] = $this->info->menu_name;

        $template['sub'] = $this->info->fun_menu;

		$id = $this->uri->segment(3);



		$template['user1'] = $this->customer_model->edit_user($id);

		// print_r($template['Category1']);die;

		if(!empty($template['user1'])) {





			if($_POST){

			 	$data = $_POST;

				$result = $this->customer_model->update_user($id,$data);

				$this->session->set_flashdata('message',array('message' => 'Edit user Details  successfully','class' => 'success'));

				redirect(base_url().'customer');

			}

		}

		else {



			$this->session->set_flashdata('message', array('message' => "You don't have permission to access.",'class' => 'danger'));

			 redirect(base_url().'customer');

		}



		$this->load->view('template',$template);

}



 function delete_customer(){



 $id = $this->uri->segment(3);

 $result= $this->customer_model->user_delete($id);

 $this->session->set_flashdata('message', array('message' => 'Deleted Successfully','class' => 'success'));

 redirect(base_url().'customer');

 }



 public function view_customerpopup(){



  $id=$_POST['customerdetails'];

  $template['data'] = $this->customer_model->view_popup_customerdetails($id);

  $this->load->view('customer/user_popup',$template);

}



  }

  ?>

