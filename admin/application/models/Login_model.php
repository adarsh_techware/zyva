<?php

class Login_model extends CI_Model {
    
    public function _consruct() {
        parent::_construct();
    }
    
    
    function login($username, $password) {
        
        if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
            
            $rs = $this->db->where('username', $username)->where('password', $password)->get('admin')->row();
            
            if (count($rs) == 0) {
                $rs = $this->db->where('username', $username)->where('password', $password)->get('users')->row();
            }
            
        } else {
            $rs = $this->db->where('email', $username)->where('password', $password)->get('users')->row();
        }
        
        if (count($rs) > 0) {
            return $rs;
        } else {
            return false;
        }
        
    }
}
