<?php 

class Brand_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
	
	function add_branddetails($data)
	{
		
		$this->db->where('brand_name', $data['brand_name']);
		$query = $this->db->get('brand');
		if( $query->num_rows() > 0 )
		{ 
			return false;
		} 
	else 
		{ 
			$result = $this->db->insert('brand',$data);
			return $result;
		}
		
		
		
	}
	
	function get_branddetails()
	{
		$query = $this->db->get('brand');
		$result = $query->result();
		return $result;
	}
	
	function brand_delete($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('brand');
		if($result)
		{
			return "Success";
		}
		else
		{
			return "Error";
		}
	}
	
	function get_single_brand($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('brand');
		$result = $query->row();
		return $result;
	}
	
	function branddetails_edit($data, $id)
	{
		$this->db->where('brand_name', $data['brand_name']);
		$query = $this->db->get('brand');
		if( $query->num_rows() > 0 )
		{ 
			return false;
		} 
	else 
		{ 
		$this->db->where('id', $id);
		$result = $this->db->update('brand',$data);
		return $result;
		}
	}
}