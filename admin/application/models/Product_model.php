<?php

class Product_model extends CI_Model {

	public function _consruct(){
		parent::_construct();
 	}


	 function get_productdetails(){



		 $this->db->select('product.id as id,
				   product.product_name,
				   product.product_code,
			       category.category_name,
				   subcategory.sub_category,
				   product.short_description,
				   product.description,product.status'
				   );
		
		$this->db->from('product as product');
		
		$this->db->join('category', 'category.id = product.category_id','left');
		$this->db->join('subcategory', 'subcategory.id = product.subcategory_id','left');

	   	$this->db->group_by('product.id');
		$query = $this->db->get();
		$result = $query->result();
		return $result;


     }
      function productdetails_add($data)
	 {

 			$prod_name = trim($data['product_name']);
 			$prod_string_trim = strtolower($prod_name); 
 			$data['prod_display'] = preg_replace('/[^A-Za-z0-9\-]/', '_', $prod_string_trim);

 			// print_r($data['prod_display']);die;
		   	$result = $this->db->insert('product',$data);
			return $result;


	 }
  function productcolor_add($data)
	 {
		          $array = $data['color_id'];
				  $comma_separated = implode(",", $array);
				  $data['color_id']=$comma_separated;
				  
				  
		   	$result = $this->db->insert('product_color',$data);
			return $result;


	 }

	 function get_color(){
		$query= $this->db->get('color');

		 return $query->result();
	 }

	 function get_size(){
		$query= $this->db->get('size');

		 return $query->result();
	 }

	 function gets_product()
	 {
		    $query = $this->db->get('product');
			     $result = $query->result();
			     return $result;
	 }
	 
	 
	 function gets_productcolor($id)
	 {
		    $query = $this->db->get('product');
			     $result = $query->result();
			     return $result;
	 }
	 
	 
	 function gets_color($id)
	 {
		    $query = $this->db->get('color');
			     $result = $query->result();
			     return $result;
	 }
	 function gets_category()
	 {
		    $query = $this->db->where('status','1')->get('category');
			     $result = $query->result();
			     return $result;
	 }
	 function gets_subcategory($id)
	 {
		 $this->db->where('category_id',$id);
		 $query = $this->db->get('subcategory');
		 $result = $query->result();
		 return $result;
	 }
	 function gets_sub_category()
	 {
		 $this->db->select('id,sub_category');
		 $query = $this->db->get('subcategory');
		 $result = $query->result();
		 return $result;
	 }

///get product status//
	 function update_product_status($data,$data1){

			$this->db->where('id',$data);
			$result = $this->db->update('product',$data1);
			return $result;
	 }






	 /////////product status/////////
		function update_productcolor_status($data,$data1)
		{

				 $this->db->where('id',$data);
				 $result = $this->db->update('product_color',$data1);
				 return $result;
	    }

		//////////delete////////
	  public function product_delete($id){

		 $this->db->where('id',$id);
		 $result = $this->db->delete('product');
		 if($result)
		 {
			return "success";
		 }
		 else
		 {
			 return "error";
		 }
	 }
	 function branddetails_edit($data, $id)
	{

		$this->db->where('id', $id);
		$result = $this->db->update('product',$data);
		return $result;

	}

	////////edit/////////


	    public function get_single_product($id){

			   $query = $this->db->where('id',$id);
			   $query = $this->db->get('product');
			   $result = $query->row();
			   return $result;

	   }
	   //////////edit product  color//////////
	public function get_single_productcolor($id){

			   $query = $this->db->where('id',$id);
			   $query = $this->db->get('product_color');
			   $result = $query->row();
			   return $result;

	   }
		 function get_single_subimg($id){

			 $query = $this->db->where('product_id',$id);
			 $query = $this->db->get('subimg');
			 $result = $query->row();
			 return $result;
		 }

	   function productdetails_edit($id,$data){
	   		$data['prod_display'] = preg_replace('/[^A-Za-z0-9\-]/', '_', $data['product_name']);
	   		if(!isset($data['stitching_charge'])){
	   			$data['stitching_charge'] = 0;
	   		}

			   $this->db->where('id', $id);
			   $result = $this->db->update('product', $data);
			   /*echo $this->db->last_query();
			   die();*/
			   return $result;
	 }
function productcolordetails_edit($data, $id){

			   $this->db->where('id', $id);
			   $result = $this->db->update('product_color', $data);
			   return $result;
	 }


	 function view_popup_product($id){

		           $this->db->select('product.id as id,
						   product.product_name,
						   product.product_code,
					       category.category_name,
						   subcategory.sub_category,
						   product.short_description,
						   product.description,
						   product.status,
						   product.price,
						   product.stitching_charge,
						   product.actual_price,
						   product.product_offer,
						   product.dp,
						   product.feature,
						    product.mrp

							');
		//$this->db->select('*');

		$this->db->from('product as product');
		
		$this->db->join('category', 'category.id = product.category_id');
		$this->db->join('subcategory', 'subcategory.id = product.subcategory_id');

		 $this->db->group_by("product.id");
				 $this->db->where('product.id',$id);


				 $query = $this->db->get();
			     $result = $query->row();
			     return $result;

     }
	 //////////////product color details ////////
	
	 
	 
	   function get_productcolordetails()
	  {

	 

					$this->db->select('product_color.id as id,product_name,color_name,product_color.*');
	   
							$this->db->from('product_color');
							$this->db->join('product','product.id = product_color.product_id','left');
							$this->db->join('color','color.id = product_color.color_id','left');
							$this->db->group_by('product_color.id'); 
							
							$query = $this->db->get();
							//echo $this->db->last_query();
							//die;
							$result = $query->result();
							return $result;		
							
							
							
							
     }
	 
	 ////////////add product  gallery////////
	 function add_productgallery($data)
	  {

		$result = $this->db->insert('product_gallery',$data);
			return $result;

     }

     function insert_gallery($data){
     	$insert = $this->db->insert_batch('product_gallery',$data);
     	// echo $this->db->last_query(); die;
        return $insert?true:false;
     }

     function update_gallery($data, $id){
     	$this->db->where('product_id',$id);
     	//$this->db->set('product_image',);
     	$update = $this->db->insert('product_gallery',$data);
     	echo $this->db->last_query();
        return $update?true:false;
      
     }


	 function get_gallery($id)
	{
			 
				$this->db->select('product_gallery.id as id, product_gallery.*'); 
                $this->db->from('product_gallery');
               				               
                $this->db->join('product','product.id = product_gallery.product_id','left');
                $this->db->where('product_id',$id); 
                $query = $this->db->get();			
			    $result = $query->result();		
			    return $result;				
	}
	function remove_image($id){
		$rs = $this->db->where('id',$id)->delete('product_gallery');
	}
}
?>
