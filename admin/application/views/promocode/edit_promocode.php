<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Promocode Details
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-bus"></i>Home</a></li>
         <li><a href="#">Promocode Details</a></li>
         <li class="active">Edit Promocode</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-warning">
               <div class="box-header with-border">
                  <h3 class="box-title">Edit Promocode Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
			    <form role="form" action="" method="post"  class="validate" enctype="multipart/form-data">

                  <div class="box-body">
                     <div class="col-md-6">
                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Promocode</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="promocode"  value="<?php echo $data->promocode; ?>">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>
                        <div class="form-group">
                         <label>Promocode Type</label>
                         <select class="form-control select2"  style="width: 100%;" id="test" name="promocode_type" onchange="showDiv(this)">
                             <option value="fixed"<?php if ($data->promocode_type=="fixed") {echo "selected"; }?>>Fixed</option>
                             <option value="percentage" <?php if ($data->promocode_type=="percentage") {echo "selected"; }?>>Percentage</option>
                         </select>
                        </div>
						            <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Amount</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="promocode_amount"  value="<?php echo $data->promocode_amount; ?>">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>
                      <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </div>

					 	 <div class="col-md-6">
		         <div class="form-group">
                <label>Start Date:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" name="start_date" value="<?php echo $data->start_date; ?>">
                </div>
                <!-- /.input group -->
              </div>
			       <div class="form-group">
                <label>Valid Till:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1" name="end_date" value="<?php echo $data->end_date; ?>">
                </div>
                <!-- /.input group -->
              </div>
			        <div class="form-group">
                        <label>Status</label><br>
                        <select class="form-control select2 required"  style="width: 100%;" id="" name="status">
                             <option value="active"<?php if ($data->status=="active") {echo "selected"; }?>>Active</option>
                             <option value="inactive" <?php if ($data->status=="inactive") {echo "selected"; }?>>Inactive</option>

                        </select>
                      </div>
          </div>
			    </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
