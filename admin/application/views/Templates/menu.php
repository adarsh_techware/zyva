<style type="text/css">
  .sidebar-menu .treeview-menu > li {
    margin: 0;
    padding-left: 10px;
}
</style>
<aside class="main-sidebar">        
        <section class="sidebar">
          <!-- <div class="user-panel">
            <div class="pull-left image">
              <?php 
                if(!isset($face)){
                  $face = "avatar5.png";
                }
              ?>
              <img src="<?php echo base_url('assets/dist/img/'.$face);?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php if(isset($name)) echo $name; else if($this->session->userdata('username')) echo $this->session->userdata('username'); ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div> -->

          <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->


          <ul class="sidebar-menu">
            <li class="header">LEFT NAVIGATION</li>

            <?php
              foreach ($perm as $rs) {               
                $num = count($rs->sub);
                if($num>1){ ?>

                   <li class="treeview <?php echo side_nav($rs->menu_name,$main);?>">
                      <a href="#">
                        <i class="fa <?php echo $rs->main_class; ?>"></i> <span><?php echo $rs->main_name; ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                      </a>
                      <ul class="treeview-menu">
                      <?php foreach($rs->sub as $row) {?>
                        <li class="<?php echo sub_nav($row->fun_menu,$sub);?>">
                        <?php if($row->fun_path!='index'){
                          $control_path = $rs->main_control."/".$row->fun_path;
                        } else {
                            $control_path = $rs->main_control;
                        } ?>

                          <a href="<?php echo base_url($control_path); ?>">
                            <i class="fa <?php echo $row->fun_class; ?>"></i> <span><?php echo $row->fun_name; ?></span>
                          </a>
                        </li>
                        <?php } ?>                         
                           
                      </ul>
                    </li>

                <?php  } else {
                  if($rs->sub[0]->parent==1){ ?>
                  <li class="treeview">
                    <li class="<?php echo side_nav($rs->menu_name,$main);?>"><a href="<?php echo base_url($rs->main_control); ?>">
                      <i class="fa <?php echo $rs->main_class; ?>" ></i> <span><?php echo $rs->main_name; ?></span> 
                    </a></li>
                  </li> 

                  <?php } else { ?>

                    <li class="treeview <?php echo side_nav($rs->menu_name,$main);?>">
                      <a href="#">
                        <i class="fa <?php echo $rs->main_class; ?>"></i> <span><?php echo $rs->main_name; ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                      </a>
                      <ul class="treeview-menu">
                      <?php foreach($rs->sub as $row) {?>
                        <li class="<?php echo sub_nav($row->fun_menu,$sub);?>">
                        <?php if($row->fun_path!='index'){
                          $control_path = $rs->main_control."/".$row->fun_path;
                        } else {
                            $control_path = $rs->main_control;
                        } ?>

                          <a href="<?php echo base_url($control_path); ?>">
                            <i class="fa <?php echo $row->fun_class; ?>"></i> <span><?php echo $row->fun_name; ?></span>
                          </a>
                        </li>
                        <?php } ?>                         
                           
                      </ul>
                    </li>

                 <?php  }
                }

              }
              
            ?>

                       
          </ul>
        </section>
      </aside>