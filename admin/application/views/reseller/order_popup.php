<style type="text/css">
  .more {
        display: none;
    }
    .first_td {
      width:75% !important;
    }
    .td_width {
      text-align: center !important;
      width:25% !important;
      margin-top:20px !important;
      margin-bottom:25px !important;
    }
    .inner_table {
      width: 100%;
      height: 150px;
      margin-top: 50px;
      margin-bottom: 50px;
    }
    .outer_table {
      width: 100%;
    }
    .no_margin_bot {
      margin-bottom: 0px !important;
    }
    .border_bot {
      border-bottom :1px solid #7d7d7d;
    }
    .free_margin {
      margin-top: 20px;
      margin-bottom: 20px;
    }
    .row_height {
      height:50px;
    }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <!-- <h3 class="box-title">View Order Details</h3> -->
        <div class="box-tools pull-right">
          <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>

    <div class="box-body">
                <table id="" class="table table-bordered table-striped datatable">
                   <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>color</th>
                        <th>Size</th>
                        <th>Quantity</th>
                        <th>Stich</th>
                        

                          <!-- <th>image</th> -->
                          <!-- <th>terms</th> -->
                      </tr>
                   </thead>
                   <tbody>
                    <?php
                      foreach ($order_info as $userdetail) {
                    ?>
                      <tr>
                        <td class="hidden"><?php echo $userdetail->id; ?></td> 
                        <td class="center">Name:<?php echo $userdetail->p_name; ?><br>Code: <?php echo $userdetail->p_code; ?></td>
                        <td class="center"><?php echo $userdetail->price; ?>&#8377</td>
                        <td class="center"> <?php echo $userdetail->c_name; ?></td>
                        <td class="center"><?php echo $userdetail->size; ?></td>
                        <td class="center"><?php echo $userdetail->qty; ?></td>
                        <td class="center">
                          <?php 
                            if($userdetail->stitch==0){ 
                              //echo "No"; 
                            } 
                            else { ?>
                            <a class="btn btn-sm bg-olive myButton1">
                              <i class="fa fa-fw fa-eye"></i> View 
                            </a> 
                              <!-- <button class="myButton">View</button> -->
                            <?php }?>
                        </td>
                        
                      </tr>
                      <?php 
                            if($userdetail->stitch>0){
                            $stith_meas = $userdetail->stith_meas;
                             ?>
                        <tr>
                            <td colspan="6" class="more">
                              
                                <table class="outer_table">
                                  <tr class="border_bot">
                                    <td>Type</td>
                                    <td class="first_td">

                                      <table border=1px class="inner_table">
                                        <tr>
                                          <?php 
                                          if(!empty($stith_meas['front'])){?>
                                          <td class="td_width">Front<br/><img src="<?php echo $stith_meas['front']->image; ?>" /><br/><?php echo $stith_meas['front']->discription; ?></td>
                                          <?php } if(!empty($stith_meas['back'])){?>
                                          <td class="td_width">Back<br/><img src="<?php echo $stith_meas['back']->image; ?>" /><br/><?php echo $stith_meas['back']->discription; ?></td>
                                          <?php } if(!empty($stith_meas['sleeve'])){?>
                                          <td class="td_width">Sleeve<br/><img src="<?php echo $stith_meas['sleeve']->image; ?>" /><br/><?php echo $stith_meas['sleeve']->discription; ?></td>
                                          <?php } if(!empty($stith_meas['neck'])){?>
                                          <td class="td_width">Neck<br/><img src="<?php echo $stith_meas['neck']->image; ?>" /><br/><?php echo $stith_meas['neck']->discription; ?></td>
                                          <?php } ?>
                                        </tr>
                                      </table>

                                    </td>
                                  </tr>

                                  <tr class="border_bot">
                                    <td>Addons</td>
                                    <td class="first_td">
                                      <table border=1 class="inner_table">
                                        <tr>
                                          <?php if(!empty($stith_meas['add_ons'])){?>
                                            <td class="td_width">TOP LINING<br/><img src="<?php echo $stith_meas['add_ons']->image; ?>" /><br/><?php echo $stith_meas['add_ons']->discription; ?></td>
                                            <?php } if(!empty($stith_meas['add_ons1'])){ ?>
                                            <td class="td_width">CLOSING<br/><img src="<?php echo $stith_meas['add_ons1']->image; ?>" /><br/><?php echo $stith_meas['add_ons1']->discription; ?></td>
                                            <?php } if(!empty($stith_meas['add_ons2'])){ ?>
                                            <td class="td_width">PLACKET<br/><img src="<?php echo $stith_meas['add_ons2']->image; ?>" /><br/><?php echo $stith_meas['add_ons2']->discription; ?></td>
                                            <?php } if(!empty($stith_meas['add_ons3'])){ ?>
                                            <td class="td_width">OTHERS<br/><img src="<?php echo $stith_meas['add_ons3']->image; ?>" /><br/><?php echo $stith_meas['add_ons3']->discription; ?></td>
                                          <?php } ?>
                                        </tr>
                                      </table>
                                    </tr>

                                    <tr class="row_height">
                                      <td>Measurment Type: </td>
                                      <td><?php echo $stith_meas['meas_type']; ?></td>
                                    </tr>

                                    <?php if($stith_meas['measure_id']>0){?>
                                    <tr>
                                      <td>Measurement:</td>
                                      <td>
                                      <table class="inner_table">
                                        <tr>
                                          <td>Bust Size: <?php echo $stith_meas['measurement']->bustsize; ?></td>
                                          <td>Waist Size: <?php echo $stith_meas['measurement']->waistsize; ?></td>
                                          <td>Hip Size: <?php echo $stith_meas['measurement']->hipsize; ?></td>
                                        </tr>
                                        <tr>
                                          <td>Pant Waist: <?php echo $stith_meas['measurement']->pantwaist; ?></td>
                                          <td>Hip: <?php echo $stith_meas['measurement']->hip; ?></td>
                                          <td>Inseam: <?php echo $stith_meas['measurement']->inseam; ?></td>
                                        </tr>
                                      </table>
                                      </td>
                                  </tr>
                                  <?php } ?>
                                </table>

                            </td> 
                        
                        
                      </tr>
                      <?php  } ?>



                      <?php }  ?>
                   </tbody>
                   <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>color</th>
                        <th>Size</th>
                        <th>Quantity</th>
                        <th>Stich</th>
                        

                          <!-- <th>image</th> -->
                          <!-- <th>terms</th> -->
                      </tr>
                   </tfoot>
                </table>
             </div>
    </div><!-- /.box -->
  </div><!-- ./col -->
</div>  


<!-- <div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">View Order Details</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>

    <div class="box-body">
      <dl>

        <dt>Poduct Name</dt>
        <dd>Name:<?php echo $userdetail->p_name; ?><br>Code: <?php echo $userdetail->p_code; ?></dd>  

        <dt>Price</dt>
        <dd><?php echo $userdetail->price; ?>&#8377</dd>  

        <dt>Color</dt>
        <dd><?php echo $userdetail->c_name; ?></dd> 

        <dt>Size</dt>
        <dd><?php echo $userdetail->size; ?></dd> 

        <dt>Stich</dt>
        <dd>
          <?php if($userdetail->stitch==0){ 
            echo "No"; 
            } 
            else {
              echo "Yes"; 
            }?>
        </dd> 

        <dt>Quantity</dt>
        <dd><?php echo $userdetail->qty; ?></dd> 

        <dt>Status</dt>
        <dd>1</dd> 


      </dl>
    </div>
    </div>
  </div>
</div> -->  