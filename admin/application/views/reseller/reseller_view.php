
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         View reseller 
      </h1>
      <br>
      <div>
         <a href="<?php echo base_url(); ?>Reseller/create"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a>
      </div>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>
         <li><a href="<?php echo base_url();?>Reseller">Reseller</a></li>
         <li class="active"><a href="<?php echo base_url();?>Reseller">View reseller</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <?php
               if($this->session->flashdata('message')) {
                     $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">View Reseller</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <table id="" class="table table-bordered table-striped datatable">
                     <thead>
                        <tr>
                           <th>Reseller Name</th>
                           <th>Phone No</th>
                           <th>Address</th>
                           <th>Username</th>
                           <th>Total Sale</th>
                           <th>Earnings</th>
                           <th>Balance</th>
                           <th>Status</th>
                           <th width="200px;">Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           foreach($reseller as $res) {
                           //  $image=$cat->category_image;
                           ?>
                        <tr>
                           <td class="center"><?php echo $res->first_name.' '.$res->last_name; ?></td>
                           <td class="center"><?php echo $res->telephone; ?></td>
                           <td class="center"><?php echo $res->address; ?></td>
                           <td class="center"><?php echo $res->username; ?></td>
                           <td class="center"><?php echo get_reseller_sale($res->id); ?></td>
                           <td class="center">&#8377; <?php echo get_reseller_earnings($res->id); ?></td>
                           <td class="center">&#8377; <?php echo get_reseller_earnings($res->id) - get_total_paid($res->id); ?></td>

                           <td>
                              <span class="center label  <?php if($res->status == '1')
                                 {
                                    echo "label-success";
                                 }
                                 else {
                                    echo "label-warning";
                                 }
                                 ?>">
                                 <?php if($res->status == '1') {
                                    echo "enable";
                                 }
                                 else {
                                    echo "disable";
                                 } ?>
                              </span>
                           </td>
                           <td class="center">
                              <a class="btn btn-sm btn-primary" href="<?php echo site_url('reseller/edit/'.$res->id); ?>">
                              <i class="fa fa-fw fa-edit"></i>Edit</a>
                              <?php if( $res->status){?>
                                 <a class="btn btn-sm label-warning" href="<?php echo base_url();?>reseller/Reseller_status/<?php echo $res->id; ?>">
                                 <i class="fa fa-folder-open"></i> Disable </a>
                              <?php }
                              else{?>
                                 <a class="btn btn-sm label-success show_reseller" href="javascript:void(0);"  
                                 data-id="<?php echo $res->id; ?>">
                                 <i class="fa fa-folder-o"></i> Enable </a>
                              <?php } ?>
                              <!-- <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#paid_modal">
                              <i class="fa fa-fw fa-edit"></i>Pay</a> -->
                              <a class="btn btn-sm btn-primary" href="<?php echo site_url('reseller/payment/'.$res->id); ?>">
                              <i class="fa fa-fw fa-edit"></i>Pay</a>
                           </td>
                        </tr>
                        <?php
                           }
                           ?>
                     </tbody>
                     <tfoot>
                        <tr>
                           <th class="hidden">ID</th>
                           <th>Reseller</th>
                           <th>Phone No</th>
                           <th>Address</th>
                           <th>Username</th>
                           <th>Total Sale</th>
                           <th>Earnings</th>
                           <th>Balance</th>
                           <td>status</td>
                           <td width="200px;">Action</td>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<div class="modal fade modal-wide" id="popup_reseller" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
   aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">View reseller Details</h4>
         </div>
         <div class="modal-catbody">
         </div>
         <div class="business_info message_info">
         </div>
         <div class="modal-footer">
            <input type="hidden" name="reseller_id" id="reseller_id">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-default pull-left" id="approve">Approve</button>&nbsp;<button type="button" class="btn btn-default pull-left" id="reject">Reject</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>




<!-- <div class="modal fade in" id="paid_modal"  role="dialog" >
   <div class="modal-dialog  modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Paid Details</h4>
         </div>
         <div class="modal-productbody">
            <div class="row">
               <div class="col-md-12">
                  <div class="box box-primary">
                     <div class="box-header with-border">
                        <div class="box-tools pull-right">
                           <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
                           <i class="fa fa-minus"></i>
                           </button>
                        </div>
                     </div>
                      <form role="form" action="" method="post"  data-parsley-validate="" class="validate">
                        <div class="box-body">
                         <div class="col-md-2"></div> 
                         <div class="col-md-8">
                             <div class="form-group has-feedback">
                                <label for="exampleInputEmail1">Reference Number</label>
                                <textarea class="form-control required" id="refer_code" required="" 
                                name="reference_id"  placeholder="Reference Number"></textarea>
                              </div>

                              <div class="form-group has-feedback">
                                <label for="exampleInputEmail1">Paid Amount</label>
                                <input type="text" class="form-control required"   required="" 
                                name="pay_amount"  placeholder="Paid Amount" id="pay_amount">
                                <span class="glyphicon  form-control-feedback"></span>
                              </div>
                          </div>
                          <div class="col-md-2"></div>
                          <div class="col-md-12">
                            <div class="box-footer text-center">
                              <button class="btn btn-info pay_submit" id="" type="">Submit</button>
                            </div>
                          </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="business_info"></div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div> -->

<script>
function insert_amount() {
   var ref_code = $('#refer_code').val();
   var pay_amount = $('#pay_amount').val();
   var reference_id=
   alert(ref_code); alert(pay_amount);
   $.ajax({         
            type: "POST",
            url: base_url+'reseller/reseller_paid',
            data: 'reference_no='+ref_code+'&pay_amount='+pay_amount,+'&reseller_id='+reseller_id.
            cache: false,
            success: function(result) {
               $('.message_paid').css('display','block');
               $('.message_paid').html(result);
               setTimeout(function(){
                  location.reload();
               },3000);
            }
         });
}
</script>
<script>

    $('.pay_submit').on('click',function(){
        alert('hai');
    });
</script>