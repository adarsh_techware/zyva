<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Menu
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>
         <li><a href="<?php echo base_url();?>main_menu">Menu</a></li>
         <li class="active"><a href="<?php echo base_url();?>main_menu/create_main">Create Menu</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-warning">
               <div class="box-header with-border">
                  <h3 class="box-title">Create Main Menu</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
			    <form role="form" action="" method="post"  class="validate">

                  <div class="box-body">
                     <div class="col-md-6">
                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="main_name"  placeholder="Menu Name">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Controller</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="main_control"  placeholder="Controller">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Menu id</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="menu_name"  placeholder="Menu Unique Identifier">
                           <span class="glyphicon  form-control-feedback"></span>(Unique name)
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Menu Icon</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="main_class"  placeholder="Menu Class Icon">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Order</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="main_priority"  placeholder="Menu Order no">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>



							

						  <!-- <div class="form-group ">
									<label class="control-label" for="shopimage">Images</label>
									<input type="file"  name="category_image" size="20" />
                           </div>
 -->



					    <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                        </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
