<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Sub Menu
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>
         <li><a href="<?php echo base_url();?>main_menu/function_view">Sub Menu</a></li>
         <li class="active"><a href="<?php echo base_url();?>main_menu/edit_function">Modify Sub Menu</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-warning">
               <div class="box-header with-border">
                  <h3 class="box-title">Modify Sub Menu</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
          <form role="form" action="" method="post"  class="validate">

                  <div class="box-body">
                     <div class="col-md-6">
                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="fun_name"  placeholder="Menu Name" value="<?php echo $function->fun_name;?>">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Main Menu</label>
                            <select class="form-control required" required="" name="main_id">
                              <?php
                                foreach ($main_menu as $rs) {?>
                                  <option value="<?php echo $rs->id;?>" <?php if($rs->id==$function->main_id) echo 'selected="SELECTED"';?>><?php echo $rs->main_name;?></option>
                                <?php }
                              ?>
                            </select>
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Path</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="fun_path"  placeholder="Function Path" value="<?php echo $function->fun_path;?>">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Function id</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="fun_menu"  placeholder="Function Unique Identifier" value="<?php echo $function->fun_menu;?>">
                           <span class="glyphicon  form-control-feedback"></span>(Unique name)
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Function Icon</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="fun_class"  placeholder="Function Class Icon" value="<?php echo $function->fun_class;?>">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Parent Type</label>
                            <select class="form-control required"  required="" name="parent">
                              <option value="0" <?php if($function->parent=='0') echo 'selected="SELECTED"';?>>Sub</option>
                              <option value="1" <?php if($function->parent=='1') echo 'selected="SELECTED"';?>>Main</option>
                            </select>
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>



              

              <!-- <div class="form-group ">
                  <label class="control-label" for="shopimage">Images</label>
                  <input type="file"  name="category_image" size="20" />
                           </div>
 -->



              <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                        </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
