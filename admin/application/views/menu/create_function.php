<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Sub Menu
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>
         <li><a href="<?php echo base_url();?>main_menu/function_view">Sub Menu</a></li>
         <li class="active"><a href="<?php echo base_url();?>main_menu/create_function">Create Sub Menu</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-warning">
               <div class="box-header with-border">
                  <h3 class="box-title">Create Sub Menu</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
          <form role="form" action="" method="post"  class="validate">

                  <div class="box-body">
                     <div class="col-md-6">
                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="fun_name"  placeholder="Menu Name">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Main Menu</label>
                            <select class="form-control required" required="" name="main_id">
                              <option value="" selected="selected">Select Main menu</option>
                              <?php
                                foreach ($main_menu as $rs) {?>
                                  <option value="<?php echo $rs->id;?>"><?php echo $rs->main_name;?></option>
                                <?php }
                              ?>
                            </select>
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Path</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="fun_path"  placeholder="Function Path">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Function id</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="fun_menu"  placeholder="Function Unique Identifier">
                           <span class="glyphicon  form-control-feedback"></span>(Unique name)
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Function Icon</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" required="" name="fun_class"  placeholder="Function Class Icon">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">Parent Type</label>
                            <select class="form-control required"  required="" name="parent">
                              <option value="1" selected="SELECTED">Main</option>
                              <option value="0">Sub</option>
                            </select>
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>



              

              <!-- <div class="form-group ">
                  <label class="control-label" for="shopimage">Images</label>
                  <input type="file"  name="category_image" size="20" />
                           </div>
 -->



              <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                        </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
