<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
           Add Delevery location
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url();?>"><i class="fa fa-male"></i>Home</a></li>
         <li><a href="<?php echo base_url();?>Delevery/create"></a></li>
         <li class="active"><a href="#">Create</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Create</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="" method="post"  data-parsley-validate="" class="validate">    
                  <div class="box-body">

                     <div class="col-md-6">
           
                         <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Delevery location</label>
                            <input type="text" class="form-control required input_length" data-parsley-trigger="change" 
                            data-parsley-minlength="2"  data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="location"  placeholder="Location">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div> 
              
                          <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Zip code</label>
                            <input type="text" class="form-control required input_length"  data-parsley-trigger="change"  
                            data-parsley-minlength="2" data-parsley-maxlength="15"  required="" name="zip_code"  placeholder="zip">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div> 
               
                          
                         
                    
                    
                     </div>
                     
                       <div class="col-md-12">
                          <div class="box-footer">
                             <button type="submit" class="btn btn-primary">Submit</button>
                          </div>
                        </div>
                       
                      
                       
                    </div><!-- Box body -->

               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>



  

    

