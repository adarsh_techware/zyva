<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <h1>
      Booking Details
    </h1>
        <br><div>
<!-- <a href="<?php// echo base_url(); ?>user/add_sub_category"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a> -->
  </div>

    <ol class="breadcrumb">
       <li><a href="<?php echo base_url(); ?>"><i class=""></i>Home</a></li>
       <li class="active"><a href="<?php echo base_url(); ?>booking">Booking</a></li>
       <li class="active"><a href="<?php echo base_url(); ?>booking">Booking</a></li>
    </ol>
 </section>
 <!-- Main content -->
 <section class="content">
    <div class="row">
       <div class="col-xs-12">
          <?php
             if($this->session->flashdata('message')) {
                      $message = $this->session->flashdata('message');

                   ?>
          <div class="alert alert-<?php echo $message['class']; ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?php echo $message['message']; ?>
          </div>
          <?php
             }
             ?>
       </div>
       <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
             <div class="box-header">
                <h3 class="box-title">View Order Details</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body">
                <table id="" class="table table-bordered table-striped datatable">
                   <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>color</th>
                        <th>Size</th>
                        <th>Stich</th>
                        <th>Quantity</th>

                          <!-- <th>image</th> -->
                          <!-- <th>terms</th> -->
                      </tr>
                   </thead>
                   <tbody>
                     <?php
                         foreach($booking as $userdetail) {
                          //  print_r($user);die;

                         ?>
                      <tr>
                        <td class="hidden"><?php echo $userdetail->id; ?></td> 
                        <td class="center">Name:<?php echo $userdetail->p_name; ?><br>Code: <?php echo $userdetail->p_code; ?></td>
                        <td class="center"><?php echo $userdetail->price; ?>&#8377</td>
                        <td class="center"> <?php echo $userdetail->c_name; ?></td>
                        <td class="center"><?php echo $userdetail->size; ?></td>
                        <td class="center">
                          <?php if($userdetail->stitch==0){ 
                            echo "No"; 
                          } 
                            else {
                              echo "Yes"; 
                            }?>
                          </td>
                        <td class="center"><?php echo $userdetail->qty; ?></td>
                 <!-- <td><img width="100px" height="100px" src="<?php //echo $userdetail->profile_picture; ?>"></td> -->
                   <!-- <!-- <td class="center"><?php //echo $userdetail->password; ?></td> -->
                   <!-- <td class="center"><?php //echo $userdetail->terms; ?></td> -->

                   <!-- <td class="center"> -->
                      <!-- <a class="btn btn-sm bg-olive show-customergetdetails" href="javascript:void(0);" data-id="<?php echo $userdetail->id; ?>">
                        <i class="fa fa-fw fa-eye"></i> View </a> -->
                       <!--  <a class="btn btn-sm btn-primary" href="<?php echo site_url('customer/edit_customer/'.$userdetail->id); ?>">
                              <i class="fa fa-fw fa-edit"></i>Edit</a>
                        <a class="btn btn-sm btn-danger" href="<?php echo site_url('customer/delete_user/'.$userdetail->id); ?>" onClick="return doconfirm()">
                        <i class="fa fa-fw fa-trash"></i>Delete</a>
                     </td> -->
                      </tr>
                      <?php
                         }
                         ?>
                   </tbody>
                   <tfoot>
                     <tr>
                        <th class="hidden">ID</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>color</th>
                        <th>size</th>
                        <th>stich</th>
                        <th>Quantity</th>
                         <!-- <th>password</th> -->
                         <!-- <th>terms</th> -->
                     </tr>
                   </tfoot>
                </table>
             </div>
             <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
       <!-- /.col -->
    </div>
    <!-- /.row -->
 </section>
 <!-- /.content -->
</div>
