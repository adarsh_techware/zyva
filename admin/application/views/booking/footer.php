<div class="banner_fth" style="float: left; width: 100%; background-color: #eeeff1;
   padding-bottom: 18px;">

   <div class="full_width" style="width:100%; float:left">
      <div class="download_android" style="width: 100%;float: left; text-align: center;">
         <button style="width: 300px; background-color: #c25ba5; color: #fff; font-size: 15px;
            font-family: 'Roboto', sans-serif; font-weight: 400; border: 0px; padding: 15px; border-radius: 25px;
            margin: 30px auto; outline: none; background-image: url('https://zyva.in/assets/images/android.png') !important;
            background-repeat: no-repeat; background-position: 45px;background-size: 23px;"> 
            Download Android App 
         </button>
      </div>

      <div class="width_33" style="width: 33.33%; float: left; min-height: 1px">
         <div class="foot_img_outer" style="width: 100%; padding-top: 14px; float: left;">
            <div class="foot_img" style="width: 100%;float: left; text-align: center;">
               <img src="https://zyva.in/assets/images/phones.png">
               <!-- <h4 style="color: #5b4180; font-size: 18px; font-weight: 600;margin-bottom: 0px;">  
                  Contact Us</h4> -->
            </div>
         </div>
         <div class="contnt">
            <ul class="ft_link" style=" list-style: outside none none; padding: 10px; float: left; 
               width: 100%; margin: 0px;">
               <li style= "color: #7e7d7e; font-size: 15px; line-height: 21px; font-style: normal;
                  text-align: center; padding: 3px; padding-left: 0px; font-family: 'Roboto', sans-serif; margin-left:0px !important;">
                  <a href="tel:+919859548" style="color: #7e7d7e;text-decoration: none;">Tel:+91 952 6039555</a>
               </li>
               <li style= "color: #7e7d7e; font-size: 15px; line-height: 21px; font-style: normal;
                  text-align: center; padding: 3px; padding-left: 0px; font-family: 'Roboto', sans-serif; margin-left:0px !important;">
                  <a href="tel:+919859548" style="color: #7e7d7e;text-decoration: none;">Tel:+91 956 2481145</a>
               </li>
               <li style= "color: #7e7d7e; font-size: 15px; line-height: 21px; font-style: normal;
                  text-align: center; padding: 3px; padding-left: 0px; font-family: 'Roboto', sans-serif; margin-left:0px !important;">
                  <a href="mailto:name@email.com" style="color: #7e7d7e;text-decoration: none;">sales@zyva.in</a>
               </li>
            </ul>
         </div>
      </div>

      <div class="width_33" style="width: 33.33%; float: left; min-height: 1px">
         <div class="foot_img_outer" style="width: 100%; padding-top: 14px; float: left;">
            <div class="foot_img" style="width: 100%;float: left; text-align: center;">
               <img src="https://zyva.in/assets/images/location.png">
                <!-- <h4 style="color: #5b4180; font-size: 18px; font-weight: 600;margin-bottom: 0px;">
                  Visit Us</h4> -->
            </div>
         </div>
         <div class="foot_b" style="width:100%; float:left; background-image:url('http://192.168.138.31/TRAINEES/Nikhila/zyva_latest/assets//images/stripss.png'); 
            background-repeat: no-repeat; background-position: left 0px top 0px; background-size: 1px;">
            <ul class="ft_link" style=" list-style: outside none none; padding: 10px; float: left; 
               width: 100%; margin: 0px;">
               <li style= "color: #7e7d7e; font-size: 15px; line-height: 21px; font-style: normal;
                  text-align: center; padding: 3px; padding-left: 0px; font-family: 'Roboto', sans-serif; margin-left:0px !important;">
                  Zyva Fashions
               </li>
               <li style= "color: #7e7d7e; font-size: 15px; line-height: 21px; font-style: normal;
                  text-align: center; padding: 3px; padding-left: 0px; font-family: 'Roboto', sans-serif; margin-left:0px !important;">
                  DOOR NO. 66/6500
               </li>
               <li style= "color: #7e7d7e; font-size: 15px; line-height: 21px; font-style: normal;
                  text-align: center; padding: 3px; padding-left: 0px; font-family: 'Roboto', sans-serif; margin-left:0px !important;">
                  2nd Floor, Matsun Towers,
               </li>
               <li style= "color: #7e7d7e; font-size: 15px; line-height: 21px; font-style: normal;
                  text-align: center; padding: 3px; padding-left: 0px; font-family: 'Roboto', sans-serif; margin-left:0px !important;">
                  A K Seshadn Road,
               </li>
               <li style= "color: #7e7d7e; font-size: 15px; line-height: 21px; font-style: normal;
                  text-align: center; padding: 3px; padding-left: 0px; font-family: 'Roboto', sans-serif; margin-left:0px !important;">
                  Ernakulam, Kochi - 682 011 
               </li>
            </ul>
         </div>
      </div>

      <div class="width_33" style="width: 33.33%; float: left; min-height: 1px">
         <div class="foot_img_outer" style="width: 100%; padding-top: 14px; float: left;">
            <div class="foot_img" style="width: 100%;float: left; text-align: center;">
               <img class="acnt" src="https://zyva.in/assets/images/account.png">
               <!-- <h4  style="color: #5b4180; font-size: 18px; font-weight: 600;margin-bottom: 0px;">
                   Follow Us</h4> -->
            </div>
         </div>
         <div class="foot_b" style="width:100%; float:left; background-image:url('http://192.168.138.31/TRAINEES/Nikhila/zyva_latest/assets//images/stripss.png'); 
            background-repeat: no-repeat; background-position: left 0px top 0px; background-size: 1px;">
            <ul class="ft_link" style="width:100%;float:left;list-style: none; padding:0px;">
               <li style= "color: #7e7d7e; font-size: 15px; line-height: 21px; font-style: normal;
                  text-align: center; padding: 3px; padding-left: 0px; font-family: 'Roboto', sans-serif; margin-left:0px !important;">
                  www.zyva.in</li>
               <li style= "color: #7e7d7e; font-size: 15px; line-height: 21px; font-style: normal;
                  text-align: center; padding: 3px; padding-left: 0px; font-family: 'Roboto', sans-serif; margin-left:0px !important;">
                  <div class="social_icon" style="width:100%; float:left">
                     <ul class="social" style="list-style: none; padding: 0px;">
                        <li style=" display: inline;padding: 8px; margin-left:0px !important;">
                           <a style="text-decoration: none !important;">
                              <img src="https://zyva.in/assets/images/social/fb.png">
                           </a>
                        </li>

                        <li style=" display: inline;padding: 8px; margin-left:0px !important;">
                           <a  style="text-decoration: none !important;">
                              <img src="https://zyva.in/assets/images/social/twit.png">
                           </a>
                        </li>

                        <li style=" display: inline;padding: 8px; margin-left:0px !important;">
                           <img src="https://zyva.in/assets/images/social/in.png">
                        </li>
                     </ul>
                  </div>
               </li>
            </ul>
         </div>
      </div>

   </div>



   <div class="full_width" style="width: 100%; float: left;min-height: 1px;">
      <div class="footer_navs foot_rht" style="width:95%; margin: 0 auto;min-height: 1px;">
         <ul style="width: 100%; float: left;min-height: 1px; text-align: center; padding: 0px; margin-top:30px">
            <li class="foot_about" style="float: left;list-style: none; padding: 0px 30px;  margin-left:0px !important;">
               <a style="color: #7e7d7e !important;text-decoration: none;" href="<?php echo base_url('About/about') ?>">About Us</a>
            </li>
            <li class="foot_contact" style="float: left;list-style: none; 
               padding: 0px 25px; background-image:url('https://zyva.in/assets/images/dot_blck.png');
               background-repeat: no-repeat; background-position: left 5px; background-size: 8px; margin-left:0px !important;">
               <a style="color: #7e7d7e !important;text-decoration: none;" href="<?php echo base_url('About/contact'); ?>">Contact Us</a>
            </li>
            <li class="foot_terms" style="float: left;list-style: none; 
               padding: 0px 25px; background-image:url('https://zyva.in/assets/images/dot_blck.png');
               background-repeat: no-repeat; background-position: left 5px; background-size: 8px; margin-left:0px !important;">
               <a style="color: #7e7d7e !important;text-decoration: none;" href="<?php echo base_url('terms'); ?>">Terms</a>
            </li>
            <li class="foot_policy" style="float: left;list-style: none; 
               padding: 0px 25px; background-image:url('https://zyva.in/assets/images/dot_blck.png');
               background-repeat: no-repeat; background-position: left 5px; background-size: 8px; margin-left:0px !important;">
               <a style="color: #7e7d7e !important;text-decoration: none;" href="<?php echo base_url('terms/policy'); ?>"> Policies</a>
            </li>
            <li class="foot_contact" style="float: left;list-style: none; 
               padding: 0px 25px; background-image:url('https://zyva.in/assets/images/dot_blck.png');
               background-repeat: no-repeat; background-position: left 5px; background-size: 8px; margin-left:0px !important;">
               <a style="color: #7e7d7e !important;text-decoration: none;" href="<?php echo base_url('terms/disclaimer'); ?>"> Disclaimer</a>
            </li>
            <li class="foot_form" style="float: left;list-style: none; 
               padding: 0px 25px; background-image:url('https://zyva.in/assets/images/dot_blck.png');
               background-repeat: no-repeat; background-position: left 5px; background-size: 8px; margin-left:0px !important;">
               <a style="color: #7e7d7e !important;text-decoration: none;" href="https://zyva.in/assets/images/size_guide.jpg" download=""> Download Measurement Form</a>
            </li>
         </ul>
      </div>

      <div class="cards" style="width: 100%; float: left;min-height: 1px; text-align:center">
         <div class="footer_cards">
            <ul style="width: 100%; float: left;min-height: 1px;list-style:none; padding:0px">
               <li style="display: inline;padding: 4px;">
                  <img src="https://zyva.in/assets/images/payment_a.png">
               </li>
               <li style="display: inline;padding: 4px;">
                  <img src="https://zyva.in/assets/images/payment_b.png">
               </li>
               <li style="display: inline;padding: 4px;">
                  <img src="https://zyva.in/assets/images/payment_c.png">
               </li>
            </ul>
         </div>
      </div>

      <div class="cards" style="width: 100%; float: left;min-height: 1px; text-align: center;">
         <p style="font-size: 15px; font-family: 'Roboto', sans-serif;color: #696969 !important;">
            zyva.in 2016.All Rights Reserved
         </p>
      </div>
   </div>
</div>
