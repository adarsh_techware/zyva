<style type="text/css">
   .color_pick_a {
   width: 25px;
   height: 25px;
   border-radius: 50%;
   text-align: center;
   left: 4px;
   top: 4px;
   cursor: pointer;
   margin-bottom: 10px;
   background-size: contain;
   }
   .clear{
   overflow: hidden;
   clear: both;
   height: 0px;
   }
   .lr_pad {
      padding-left:30px;
      padding-right:30px;
   }
</style>
<div class="row">
   <div class="col-md-12">
      <div class="box box-primary">
         <div class="box-header with-border">
            <!-- <h5 class="box-title">View Order Details</h5> -->
            <div class="box-tools pull-right">
               <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
               <i class="fa fa-minus"></i>
               </button>
            </div>
         </div>
         <div class="box-body">
            <div class="row">
               <div class="col-md-12">
                  <h4>Booking details</h4>
                  <h5>
                     <div class="col-md-4">Booking ID:</div> 
                     <div class="col-md-8 lr_pad"><?php echo $order_info->booking_no; ?></div>
                  </h5>
                  <br/>
                  <h5>
                     <div class="col-md-4">Order Date:</div>
                     <div class="col-md-8 lr_pad"><?php echo date('Y-m-d h:i A',strtotime($order_info->booked_date)); ?></div></h5>
                  <br/>
                  <h5>
                     <div class="col-md-4">Customer:</div>
                     <div class="col-md-8 lr_pad"><?php echo $order_info->name; if($order_info->reseller_id!='') echo '&nbsp;&nbsp;('.get_reseller($order_info->reseller_id).')'; ?></div>
                  </h5>
                  <br/>
                  <h5>
                     <div class="col-md-4">Contact No:</div>
                     <div class="col-md-8 lr_pad"><?php echo 'Mob: '.$order_info->telephone.'<br/> Email ID: '.$order_info->email; ?></div>
                  </h5>
                  <br/><br/>
                  <h5>
                     <div class="col-md-4">Total:</div>
                     <div class="col-md-8 lr_pad">&#8377;<?php echo $order_info->price; ?></div>
                  </h5>
                  <br/> 
                  <h5>
                     <div class="col-md-4">Reason:</div>
                     <div class="col-md-8 lr_pad"><?php echo $order_info->message; ?></div>
                  </h5>
                  <br/>
                  <!-- <h5><div class="col-md-4">Payment:</div><div class="col-md-6"><?php echo $order_info->status; ?></div></h5><br/> -->    
               </div>
               <div class="col-md-12" class="clear">
                  <h4>Order details</h4>
                  <?php
                     foreach ($order_info->order as $rs) {?>
                     <div class="col-md-4"><img src="<?php echo get_product_image($rs->id); ?>" width="100px"></div>
                     <div class="col-md-8">
                        <h5><div class="col-md-6">Product Name:</div><div class="col-md-6"><?php echo $rs->product_name;?></div></h5>
                        <br/>
                        <h5><div class="col-md-6">Product Code:</div><div class="col-md-6"> <?php echo $rs->product_code; ?></div></h5>
                        <br/>
                        <h5><div class="col-md-6">Color:</div><div class="col-md-6">
                              <div class="color_pick_a" style="background-image: url('<?php echo $rs->color; ?>')"></div>
                           </div>
                        </h5>
                        <br/>
                        <h5><div class="col-md-6">Size:</div><div class="col-md-6"> <?php echo $rs->size; ?></div></h5>
                        <br/>
                        <h5><div class="col-md-6">Quantity:</div><div class="col-md-6"> <?php echo $rs->quantity; ?></div></h5>
                        <br/>
                     </div>
                  <hr class="clear"/>
                  <?php } ?>
               </div>
               <br/><br/>
            </div> 
         </div>
         <div class="box-footer" style="text-align: center;">
            <span class="message_info" style="color: red;display: none;text-align: center;"></span>
            <br/>
            <div class="row">
               <div class="col-md-2"></div>
               <div class="col-md-8" style="text-align:center;">
                  <textarea style="width:100%" placeholder="Reference Number" id="ref_code"></textarea>
                  <br><div class="ref_error" style="color:red; display:none;">Please enter reference number</div></br></br>
               </div>
               <div class="col-md-2"></div>
               <br/>
            </div>
            <div class="col-md-12" style="text-align:center;">
               <button class="btn btn-info book_change" type="button" data-status="1">Place Order</button>&nbsp;&nbsp;<button class="btn btn-warning book_change" type="button" data-status="2">Reject</button>
               <input type="hidden" id="booking_id" name="booking_id" value="<?php echo $order_info->id; ?>">
               <input type="hidden" id="booking_no" name="booking_no" value="<?php echo $order_info->booking_no; ?>">
            </div>
         </div>
        
      </div>
      <!-- /.box -->
   </div>
   <!-- ./col -->
</div>