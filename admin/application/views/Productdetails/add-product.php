<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        Product
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url();?>"><i class="fa fa-user-md"></i></i>Home</a></li>
         <li><a href="<?php echo base_url();?>product/view_product">Product</a></li>
         <li  class="active"><a href="<?php echo base_url(); ?>product/add_product">Create Product</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Create Product</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="" method="post"  data-parsley-validate="" class="validate" enctype="multipart/form-data">
                  <div class="box-body">

                     <div class="col-md-6">

        				        <div class="form-group has-feedback">
                          <label for="exampleInputEmail1">Product Name</label>
                          <input type="text" class="form-control required"  required="" name="product_name" id="product_name"  placeholder="Product Name">
                          <span class="glyphicon  form-control-feedback"></span>
                        </div>



			                   <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Code</label>
                            <input type="text" class="form-control required"   required="" name="product_code"  placeholder="Product Code">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>


  			                 <div class="form-group" id="" >
                            <label>Select Category</label>
                            <select class="form-control "  style="width: 100%;" name="category_id" id="category_id">
                              <option>Select Category</option>
                             <?php
          				             //print_r($category);
                              foreach($category as $categorys) { ?>

                              <option value="<?php echo $categorys->id;?>"><?php echo $categorys->category_name;?></option>
                              <?php
                              }
                              ?>
                             </select>
                          </div>


			                     <div class="form-group has-feedback">
                            <label>Select Subcategory</label>
                              <select class="form-control get_cat_sub"  style="width: 100%;" name="subcategory_id" id="subcategory_id">

                              <option>Select Subcategory</option>

                               <?php foreach($sub_cat as $sub_categorys){ ?>

                                <!-- <option value="<?php echo $sub_categorys->id;?>"><?php echo $sub_categorys->sub_category;?></option> -->
                               <?php
                                }
                               ?>
                               </select>
                            </div>


			                   <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Short Description</label>
                            <textarea class="form-control required"
                            required="" name="short_description"  placeholder="Short Description"></textarea>
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>




				                <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Description</label>
                            <textarea class="form-control required"  required="" name="description"  placeholder="Product Description"></textarea>
                            <span class="glyphicon  form-control-feedback"></span>
                        </div>


                          <div class="form-group has-feedback">
                              <label for="exampleInputEmail1">Product features</label>
                              <textarea class="form-control required"  required="" name="feature"  placeholder="Product Features"></textarea>
                              <span class="glyphicon  form-control-feedback"></span>
                          </div>

                        </div>
                        <div class="col-md-6">

				                  <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Selling Price</label>
                            <input type="number" class="form-control required" data-parsley-trigger="change"
                             required="" name="price"  placeholder="Product Price">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>

          				        <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Market Price</label>
                            <input type="number" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" required="" name="mrp"  placeholder="Market Price">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>


          				        <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Dealer Price</label>
                            <input type="number" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" required="" name="dp"  placeholder="Dealer Price">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>


          				        <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Actual Price</label>
                            <input type="number" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" required="" name="actual_price"  placeholder="Actual Price">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>

                        <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Product Offer</label>
                            <input type="text" class="form-control" name="product_offer"  placeholder="Product Offer">
                            <span class="glyphicon  form-control-feedback"></span>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Gallery</label>
                            <input type="file" class="form-control" name="product_image[]" multiple required />
                        </div>

                         <!-- <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Size Guide</label>
                            <textarea class="form-control" 
                            name="size_guide"  placeholder="Size Guide" style="height:35px"></textarea>
                            <span class="glyphicon  form-control-feedback"></span>
                        </div> -->

                        <!-- <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Stitching Enable</label>
                             <input type="checkbox" id="myCheck" name="stitching_charge" value="1" onclick="enable()">
                            <input type="number" class="form-control required" data-parsley-trigger="change"

                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" required="" name="stitching_charge"  placeholder="Stitching Charge">
                            <span class="glyphicon  form-control-feedback"></span>
                        </div>  -->

                        <div class="form-group" id="" >
                            <label>Specification</label>
                            <select class="form-control "  style="width: 100%;" name="spec">
                              <option value="1">Color and Size</option>
                              <option value="2">Only Color</option>
                              <option value="3">Only Size</option>
                              <option value="4">No Size and No Color</option>
                            </select>
                          </div>


                        <div class="form-group has-feedback">
                             <div class="check_lftt">
                                <input type="checkbox" id="myCheck"  name="stitching_charge" value="1" onclick="enable()" class="checkbox-custom">
                                <label for="myCheck" class="checkbox-custom-label">Stitching Enable</label>
                             </div>
                          </div>

                          




                      </div>
                      <div class="col-md-12">
                        <div class="message_info" style="color: red"></div>
                        <div class="box-footer">
                           <button type="submit" class="btn btn-primary" id="pdt_btn">Submit</button>
                        </div>
				              </div>


				  </div>
               </form>
            </div>
            <!-- /.box -->
            <script>

            function enable() {
              document.getElementById("myCheck").enabled = true;
                              }

                
            </script>
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
