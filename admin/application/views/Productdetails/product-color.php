<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add Prodcut Color Details
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class=""></i>Home</a></li>
         <!-- <li><a href="<?php echo base_url(); ?>Home_ctrl/view_category">Category Details</a></li> -->
         <li class="active">Add Prodcut Color </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-warning">
               <div class="box-header with-border">
                  <h3 class="box-title">Add Color Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
			    <form role="form" action="" method="post"  class="validate" enctype="multipart/form-data">

                  <div class="box-body">
                     <div class="col-md-12">
					 
					 
            <div class="form-group" id="" >
                            <label>Select Product</label>
                  <select class="form-control "  style="width: 100%;" name="product_id" id="product_id">

                   <?php
				  // print_r($product);
				  // die;
                  foreach($product as $pro){

                   ?>
                <option value="<?php echo $pro->id;?>"><?php echo $pro->product_name;?></option>
                   <?php
                  }
                   ?>
                   </select>
            </div>
			
			
			<!--<div class="form-group" id="" >
                            <label>Select Color</label>
                  <select class="form-control "  style="width: 100%;" name="color_id" id="color_id">

                   <?php
				 
                  //foreach($color as $color){

                   ?>
                <option value="<?php echo $color->id;?>"><?php echo $color->color_name;?></option>
                   <?php
                  //}
                   ?>
                   </select>
            </div>-->
			<div class="form-group">
                        <label>Product Color</label>
							<select class="form-control select2 js-example-basic-multiple"  multiple="multiple" style="width: 100%;" name="color_id[]"  required="">
								   <?php
									  $arry_select = explode(",", $color->color_name);
									 foreach($color as $color){
								   ?>
			            <option value="<?php echo $color->id;?>"<?php if (in_array($color->id, $arry_select))
					    echo 'selected';  ?> ><?php echo $color->color_name;?></option>			  
								   <?php
								   }
								   ?>
                            </select>
            </div>	




						  




					    <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                        </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
