<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Product Color Details
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-male"></i>Home</a></li>
         <li><a href="#">Product Color Details</a></li>
         <li class="active">Edit Product Color</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-warning">
               <div class="box-header with-border">
                  <h3 class="box-title">Add Color Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
			    <form role="form" action="" method="post"  class="validate" enctype="multipart/form-data">

                  <div class="box-body">
                     <div class="col-md-12">
					 
					 
            <div class="form-group" id="" >
                            <label>Select Product</label>
                  <select class="form-control "  style="width: 100%;" name="product_id" >

                  <?php
				   //print_r($product);

                  foreach($product as $product){

                   ?>
                <option value="<?php echo $product->id;?>"<?php if ($product->id == $data1->product_id){ ?>
							selected = "selected" <?php } ?>><?php echo $product->product_name;?></option>
                   <?php
                  }
                   ?>
                   </select>
            </div>
			<div class="form-group" id="" >
                            <label>Select Color</label>
                  <select class="form-control "  style="width: 100%;" name="color_id" >

                   <?php
				 
                  foreach($color as $color){

                   ?>
        		 <option value="<?php echo $color->id;?>"<?php if ($color->id == $data1->color_id){ ?>
				  selected = "selected" <?php } ?>><?php echo $color->color_name;?></option>
                   <?php
                  }
                   ?>
                   </select>
            </div>



					    <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                        </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
