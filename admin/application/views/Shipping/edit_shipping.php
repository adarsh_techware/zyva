<div class="content-wrapper">

   <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>

        Charges

      </h1>

      <ol class="breadcrumb">

         <li><a href="<?php echo base_url();?>"><i class="fa fa-user-md"></i>Home</a></li>

         <li><a href="<?php echo base_url(); ?>shipping">Charges</a></li>

         <li class="active">Edit Charges</li>

      </ol>

   </section>

   <!-- Main content -->

   <section class="content">

      <div class="row">

         <!-- left column -->

         <div class="col-md-12">

            <?php

               if($this->session->flashdata('message')) {

               $message = $this->session->flashdata('message');

               ?>

            <div class="alert alert-<?php echo $message['class']; ?>">

               <button class="close" data-dismiss="alert" type="button">×</button>

               <?php echo $message['message']; ?>

            </div>

            <?php

               }

               ?>

         </div>

         <div class="col-md-12">

            <!-- general form elements -->

            <div class="box">

               <div class="box-header with-border">

                  <h3 class="box-title">Edit Charges</h3>

               </div>

               <!-- /.box-header -->

               <!-- form start -->

               <form role="form" action="" method="post"  data-parsley-validate="" class="validate" enctype="multipart/form-data">

                  <div class="box-body">

                     <div class="col-md-6">



                        <div class="form-group has-feedback">

                            <label for="exampleInputEmail1">Label</label>

                            <input type="text" class="form-control required"  required="" name="label"  value="<?php echo $ship->label; ?>" placeholder="Label" readonly>

                            <span class="glyphicon  form-control-feedback"></span>

                        </div>



                        <div class="form-group has-feedback">

                            <label for="exampleInputEmail1">Shipping Charge</label>

                            <input type="text" class="form-control"    name="charge" value="<?php echo $ship->charge; ?>" placeholder="Shipping Charge">

                            <span class="glyphicon  form-control-feedback"></span>

                        </div>







                </div>

                <div class="col-md-12">

       

                  <!-- /.box-body -->

                  <div class="box-footer">

                     <button type="submit" class="btn btn-primary">Update</button>

                  </div>

           </div>





          </div>

               </form>

            </div>

            <!-- /.box -->

            <script>



            function enable() {

              document.getElementById("myCheck").enabled = true;

                              }

            </script>

         </div>

      </div>

      <!-- /.row -->

   </section>

   <!-- /.content -->

</div>

