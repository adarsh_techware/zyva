<div class="content-wrapper">

   <!-- Content Header (Page header) -->

   <section class="content-header">

      <h1>

          Manage Users

      </h1>

      <ol class="breadcrumb">

         <li><a href="<?php echo base_url();?>"><i class="fa fa-male"></i>Home</a></li>

         <li><a href="<?php echo base_url();?>user">Users</a></li>

         <li class="active"><a href="#">Edit</a></li>

      </ol>

   </section>

   <!-- Main content -->

   <section class="content">

      <div class="row">

         <!-- left column -->

         <div class="col-md-12">

            <?php

               if($this->session->flashdata('message')) {

               $message = $this->session->flashdata('message');

               ?>

            <div class="alert alert-<?php echo $message['class']; ?>">

               <button class="close" data-dismiss="alert" type="button">×</button>

               <?php echo $message['message']; ?>

            </div>

            <?php

               }

               ?>

         </div>

         <div class="col-md-12">

            <!-- general form elements -->

            <div class="box">

               <div class="box-header with-border">

                  <h3 class="box-title">Edit</h3>

               </div>

               <!-- /.box-header -->

               <!-- form start -->

               <form role="form" action="<?php echo base_url('role/update_role/'.$id) ;?>" method="post"  data-parsley-validate="" class="validate">		

                  <div class="box-body">



                     <div class="col-md-6">

					 

					               <div class="form-group has-feedback">

                            <label for="exampleInputEmail1">Role</label>

                            <input type="text" class="form-control required input_length" data-parsley-trigger="change"	

                            data-parsley-minlength="2" data-parsley-maxlength="15"  required="" name="rolename"  placeholder="Role" value="<?php echo $data->rolename;?>">

                            <span class="glyphicon  form-control-feedback"></span>

                          </div> 

						  

						              

            						  

            						

            						  <!-- <div class="form-group ">

                            <label class="control-label" for="shopimage">Images</label>

                            <input type="file"  name="profile_picture" size="20" class="pic_file" />

                          </div> -->

            					</div>



            					 <div class="col-md-12">

                          <div class="box-footer">

                             <button type="submit" class="btn btn-primary">Submit</button>

                          </div>

                        </div>

            				   

            				  

            				   

				            </div><!-- Box body -->



               </form>

            </div>

            <!-- /.box -->

         </div>

      </div>

      <!-- /.row -->

   </section>

   <!-- /.content -->

</div>







  



		



