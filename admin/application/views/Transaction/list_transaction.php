<div class="content-wrapper" >
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Transaction
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url();?>"><i class="fa fa-user-md"></i></i>Home</a></li>
         <li><a href="<?php echo base_url();?>transaction">Transaction</a></li>
         <li  class="active"><a href="<?php echo base_url(); ?>transaction/list_transaction">List Transaction</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <?php
               if($this->session->flashdata('message')) {
                        $message = $this->session->flashdata('message');
               
                     ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
               <div class="box-header">
                  <h3 class="box-title">View transaction</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <table id="" class="table table-bordered table-striped datatable">
                     <thead>
                        <tr>
                           <th class="hidden">ID</th>
                           <th>Booking Number</th>
                           <th>Transaction Id</th>
                           <th>Amount</th>
                           <th>Message</th>
                           <th>Status</th>
                           <th>Reference Code</th>
                           <th>Processed By</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           foreach($transaction_det as $transaction) {  
                           //var_dump($transaction);
                           //die;                     
                           ?>
                        <tr>
                           <td class="hidden"><?php echo $transaction->id; ?></td>
                           <td class="center"><?php echo $transaction->booking_no; ?></td>
                           <td class="center"><?php echo $transaction->trans_id; ?></td>
                           <td class="center"><?php echo $transaction->amount; ?></td>
                           <td class="center"><?php echo $transaction->message; ?></td>
                           <td class="center"><?php echo $transaction->status; ?></td>
                           <td class="center"><?php echo $transaction->ref_code; ?></td>
                           <td class="center"><?php if($transaction->request_by == 1){echo "Admin";} else {echo "Customer";} ?></td>
                        </tr>
                        <?php
                           }
                           ?>
                     </tbody>
                     <tfoot>
                        <tr>
                           <th class="hidden">ID</th>
                           <th>Booking Number</th>
                           <th>Transaction Id</th>
                           <th>Amount</th>
                           <th>Message</th>
                           <th>Status</th>
                           <th>Reference Code</th>
                           <th>Processed By</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.box-body -->
            </div>
            <!-- /.box -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
