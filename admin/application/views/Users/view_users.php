	

	

	<div class="content-wrapper" >

   <!-- Content Header (Page header) -->

   <section class="content-header">

      <h1>

        Manage User

      </h1>

      <ol class="breadcrumb">

         <li><a href="<?php echo base_url();?>"><i class="fa fa-user-md"></i></i>Home</a></li>

         <li><a href="<?php echo base_url();?>user">User</a></li>

         <li  class="active"><a href="<?php echo base_url(); ?>user">View User</a></li>

      </ol>

   </section>

   <!-- Main content -->

   <section class="content">

      <div class="row">

         <div class="col-xs-12">

            <?php

               if($this->session->flashdata('message')) {

                        $message = $this->session->flashdata('message');

               

                     ?>

            <div class="alert alert-<?php echo $message['class']; ?>">

               <button class="close" data-dismiss="alert" type="button">×</button>

               <?php echo $message['message']; ?>

            </div>

            <?php

               }

               ?>

         </div>

         <div class="col-xs-12">

            <!-- /.box -->

            <div class="box">

               <div class="box-header">

                  <h3 class="box-title">View User</h3>

               </div>

               <!-- /.box-header -->

               <div class="box-body">

                  <table id="" class="table table-bordered table-striped datatable">

                     <thead>

                        <tr>

                           <th class="hidden">ID</th>

						         <th>Name</th>                                            

                           <th>Email</th>  

						         <th>Role</th>

						         <th>Username</th>  						   

                           <th width="">Action</th>

                        </tr>

                     </thead> 

                     <tbody>

                        <?php

                        $i = 0;

                           foreach($data as $users) {	

//var_dump($product);

//die;						   

                           ?>

                        <tr>

                     <td class="hidden"><?php echo $users->id; ?></td>                 

                     <td class="center"><?php echo $users->name; ?></td>

						   <td class="center"><?php echo $users->email; ?></td>

                     <td class="center"><?php echo $users->rolename; ?></td>

                     <td class="center"><?php echo $users->username; ?></td>

                     <td class="center"><a class="btn btn-sm btn-primary" href="<?php echo site_url('user/edit_user/'.$users->id); ?>">

                            <i class="fa fa-fw fa-edit"></i>Edit</a></td>

                        </tr>

                        <?php

                           }

                           ?>

                     </tbody>

                     <tfoot>

                        <tr>


                            <th class="hidden">ID</th>  

                           <th>Name</th>                                                           

                           <th>Email</th>  

                           <th>Role</th>

                           <th>Username</th>

                           <th>Action</th>

                        </tr>

                     </tfoot>

                  </table>

               </div>

               <!-- /.box-body -->

            </div>

            <!-- /.box -->

         </div>

         <!-- /.col -->

      </div>

      <!-- /.row -->

   </section>

   <!-- /.content -->

</div>

















 