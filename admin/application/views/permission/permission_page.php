<style type="text/css">

  .checkbox{
    margin-top: 0;
    margin-bottom: 5px;
  }
  
</style>


<div class="content-wrapper">
    <section class="content-header">
      <h1>Permission Page</h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo base_url('role');?>">Manage Role</a></li>
            <li class="active">View</li>
          </ol>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-xs-12">   

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title" style="float: left;"><button class="form-control fa fa-user-plus" style="background-color: #2774ff;color: #fff" onclick="window.location.href='<?php echo base_url("role/create_role"); ?>'">&nbsp;CREATE</button></h3> 
                </div><!-- /.box-header -->
                <form id="permission_form" method="post" action="<?php echo base_url('permission/perm_assign');?>">
                    <table class="table table-bordered table-striped">
                      <tbody>
                        <?php
                        $i=0;
                        $k=0;
                        foreach ($module as $rs) {
                          $control = $rs->main_control;
                          if($i%3==0){?>
                            <tr>                        
                          <?php  } ?>
                          
                          <td>
                            <div class="box-body" style="padding-left: 100px">
                              <label><input type="checkbox" id="<?php echo $rs->main_control; ?>" onclick="check_fun($(this));"> <?php echo $rs->main_name; ?></label>
                                <?php 
                                  foreach ($function[$control] as $row) {
                                    ++$k;
                                    ?>

                                    <div class="form-group">
                                      <div class="col-sm-10">
                                        <div class="checkbox">
                                          <label>
                                            <input type="checkbox" id="<?php echo $rs->main_control.$k; ?>" class="<?php echo $rs->main_control; ?>" name="<?php echo $rs->main_control; ?>[]" value="<?php echo $row->id; ?>" onclick="check_sub($(this));" > <?php echo $row->fun_name; ?>
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                    
                                 <?php  } 
                                 ++$i;
                                ?>
                            </div>  
                          </td>

                          <?php if($i%3==0){
                           $rem = $i%3;
                           /*for($j=$rem;$j<3;$j++){?>
                            <td>&nbsp;</td>
                           <?php }*/
                            ?>
                            </tr>                        
                          <?php  } 

                        }

                        if($i%3!=0){?>
                          </tr> 
                        <?php } ?>
                         
                        
                      </tbody>
                    </table>
                <input type="hidden" value="<?php echo $role_id; ?>" id="role_id" name="role_id" />

                <div class="box-footer col-sm-offset-1">
                  <button type="submit" class="btn btn-info" style="background-color: #3333ff;color: #fff;">Assign</button>
                    <button type="reset" class="btn btn-default" onclick="window.history.back();">Cancel</button>
                    
                  </div><!-- /.box-footer -->
                </form>
                    
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
      
      <script type="text/javascript">
      //alert('zxczxc');
        var permision = new Array();
        <?php foreach($permission as $key => $val){ ?>
            permision.push('<?php echo $val; ?>');
        <?php } ?>
//alert(JSON.stringify(permision));
        $.each(permision, function(i, val){

          $("input[value='" + val + "']").prop('checked', true);
          
        });
        verify();

        function check_fun(control){
            if(control.prop("checked") == true){               
              var id = control.attr("id");
              $('.'+id).prop("checked", true);
            }

            else if(control.prop("checked") == false){

                var id = control.attr("id");
              $('.'+id).prop("checked", false);

            }
        }

        function check_sub(sub){
          var all = 0;

          if(sub.prop("checked") == true){

              var id = sub.attr("class");
              $('.'+id).each(function() {
                  if($(this).prop("checked") == false){
                    all = 1;
                  }

              });

              if(all==0){
                $('#'+id).prop("checked", true);
              }
              
          } else {
              var id = sub.attr("class");
              $('#'+id).prop("checked", false);
          }
          

        }

        function verify(){
$('input[type=checkbox]').each(function () {
  all = 0;
  var id =  $(this).attr("class");
              $('.'+id).each(function() {
                  if($(this).prop("checked") == false){
                    all = 1;
                  }

              });

              if(all==0){
                $('#'+id).prop("checked", true);
              }
});
}


        $('#permission_form').on('submit',function(e){
            var data = $(this).serialize();
            $.ajax({
              method: "POST",
              url: site_url+"permission/perm_assign",
              data: data
            })
            .done(function( msg ) {
              alert("Permission Assigned");
                  /*var data = JSON.parse(msg);
                  duplicate =  data['duplicate'];
                    $('.modal-body').html('<p>'+data['message']+'</p>');
                    $('#myModal').modal('toggle');
                    $('#myModal').modal('show');   */
            });

    
           e.preventDefault(); 
        });



        
        
       
      </script>