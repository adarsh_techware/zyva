<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Category
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class=""></i>Home</a></li>
        <li><a href="<?php echo base_url();?>category">Category</a></li>
        <li class="active"><a href="<?php echo base_url();?>category/edit">Edit Category</a></li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Edit Category</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="" method="post"  data-parsley-validate="" class="validate" enctype="multipart/form-data">
                  <div class="box-body">
                     <div class="col-md-6">


							<div class="form-group has-feedback">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" value="<?php echo $Category1->category_name; ?>"class="form-control required" data-parsley-trigger="change"
                             required="" name="category_name"  placeholder="Order">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>



                          <!-- <img src="<?php echo $Category1->category_image; ?>" width="200px"></a>
               					 <div class="form-group ">
               									<label class="control-label" for="shopimage">Select Images</label>
               									<input type="file"  name="category_image"  />
												 
                          </div> -->

                  <!-- /.box-body -->
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">update</button>
                  </div>
				   </div>
				   <div class="col-md-6">



</div>

				  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
