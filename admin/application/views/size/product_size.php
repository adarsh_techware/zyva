<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Add product size Details
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class=""></i>Home</a></li>
         <!-- <li><a href="<?php echo base_url(); ?>Home_ctrl/view_category">Category Details</a></li> -->
         <li class="active">Add product size</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-warning">
               <div class="box-header with-border">
                  <h3 class="box-title">Add product size Details</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
			    <form role="form" action="" method="post"  class="validate">

                  <div class="box-body">
                     <div class="col-md-12">
                        <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">product</label>
                           <select class="form-control select2 required"  style="width: 100%;"  name="product_id">
             								   <?php
             									  foreach($prod as $pro){
             								   ?>
                               <!-- <option>select</option> -->
             								   <option value="<?php echo $pro->id;?>"><?php echo $pro->product_name;?></option>
             								   <?php
             								   }
             								   ?>
                              </select>

                        </div>

                        <!-- <div class="form-group has-feedback">
                           <label for="exampleInputEmail1">size</label>
                           <select class="form-control select2 required"  style="width: 100%;"  name="size_id">
                               <?php
                                //foreach($size as $pro){
                               ?>
                               <!-- <option>select</option> -->
                               <!-- <option value="<?php //echo $pro->id;?>"><?php //echo $pro->size_type;?></option> -->
                               <?php
                              // }
                               ?>
                              <!-- </select> -->
                        <!-- </div> -->


                        <div class="form-group">
                                          <label>size type</label>
                                <select class="form-control select2 js-example-basic-multiple"  multiple="multiple" style="width: 100%;" name="size_id[]"  required="">
                                     <?php
                                      $arry_select = explode(",", $pro->size_type);
                                     foreach($size as $pro){
                                     ?>
                                    <option value="<?php echo $pro->id;?>"<?php if (in_array($pro->id, $arry_select))
                                echo 'selected';  ?> ><?php echo $pro->size_type;?></option>
                                     <?php
                                     }
                                     ?>
                                              </select>
                        </div>
						  <!-- <div class="form-group ">
									<label class="control-label" for="shopimage">Select Images</label>
									<input type="file"  name="image" size="20" />
                                    </div> -->




					    <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                        </div>

               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
