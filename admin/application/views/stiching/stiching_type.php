<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Stiching 
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class=""></i>Home</a></li>
         <li><a href="<?php echo base_url(); ?>Stiching/view_stich_types">Stiching</a></li>
         <li class="active">Create Stiching</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-warning">
               <div class="box-header with-border">
                  <h3 class="box-title">Create Stiching</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
			    <form role="form" action="" method="post"  class="validate" enctype="multipart/form-data">

                  <div class="box-body">
                     <div class="col-md-6">
                        <!-- <div class="form-group has-feedback"> 
                           <label for="exampleInputEmail1">Stiching Types</label>
                            <input type="text" class="form-control required" data-parsley-trigger="change"
                            data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="type">
                           <span class="glyphicon  form-control-feedback"></span>
                        </div>-->



                    <div class="form-group">
                          <label>Select stiching type</label>
                     <select class="form-control select2 required"  style="width: 100%;" id="bus_name" name="type_id">
                           <?php
                             foreach($stype as $stypes){
                           ?>
                           <option value="<?php echo $stypes->id;?>"><?php echo $stypes->stichtype;?></option>
                           <?php
                           }
                           ?>
                            </select>
                        </div>
					

                      <script type="text/javascript">
                      function showDiv(select){
                         if(select.value=='child'){
                          document.getElementById('hidden_div').style.display = "block";
                         } else{
                          document.getElementById('hidden_div').style.display = "none";
                         }
                      }
                      </script>


      						  <div class="form-group ">
      									<label class="control-label" for="shopimage">Select Images</label>
      									<input type="file"  name="image" size="20" />
                                          </div>




                                           <div class="form-group ">
                        <label class="control-label" for="shopimage">discription</label>
                        <textarea  name="discription" size="20" style="width: 100%;"></textarea>
                                          </div>
                     </div>
                     </div class="col-md-12">                     
      					    <div class="box-footer">
                       <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
                </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
