<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
    <h1>
      Customers
    </h1>
        <br><div>
<!-- <a href="<?php// echo base_url(); ?>user/add_sub_category"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a> -->
  </div>

    <ol class="breadcrumb">
       <li><a href="<?php echo base_url(); ?>"><i class=""></i>Home</a></li>
       <li><a href="<?php echo base_url(); ?>customer">Customers</a></li>
       <li class="active">Edit Customers</li>
    </ol>
 </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <?php
               if($this->session->flashdata('message')) {
               $message = $this->session->flashdata('message');
               ?>
            <div class="alert alert-<?php echo $message['class']; ?>">
               <button class="close" data-dismiss="alert" type="button">×</button>
               <?php echo $message['message']; ?>
            </div>
            <?php
               }
               ?>
         </div>
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Edit Customers</h3>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="" method="post"  data-parsley-validate="" class="validate">
                  <div class="box-body">
                     <div class="col-md-6">


							       <div class="form-group has-feedback">
                            <label for="exampleInputEmail1">First name</label>
                            <input type="text" value="<?php echo $user1->first_name; ?>"class="form-control required" data-parsley-trigger="change"
                             required="" name="first_name"  placeholder="Order">
                            <span class="glyphicon  form-control-feedback"></span>
                          </div>



                                      <div class="form-group has-feedback">
                                                        <label for="exampleInputEmail1">Last name</label>
                                                        <input type="text" value="<?php echo $user1->last_name; ?>"class="form-control required" data-parsley-trigger="change"
                                                        required="" name="last_name"  placeholder="Order">
                                                        <span class="glyphicon  form-control-feedback"></span>
                                          </div>

                                        </div>
                                        <div class="col-md-6">


                                              <div class="form-group has-feedback">
                                                    <label for="exampleInputEmail1">Telephone</label>
                                                    <input type="text" value="<?php echo $user1->telephone; ?>"class="form-control required" data-parsley-trigger="change"
                                                    required="" name="telephone"  placeholder="Order">
                                                    <span class="glyphicon  form-control-feedback"></span>
                                                </div>
                                                <div class="form-group has-feedback">
                                                              <label for="exampleInputEmail1">email</label>
                                                              <input type="text" value="<?php echo $user1->email; ?>"class="form-control required" data-parsley-trigger="change"
                                                              required="" name="email"  placeholder="Order">
                                                              <span class="glyphicon  form-control-feedback"></span>
                                                  </div>
                                                  <!-- <div class="form-group has-feedback">
                                                                <label for="exampleInputEmail1">Terms</label>
                                                                <input type="text" value="<?php //echo $user1->terms; ?>"class="form-control required" data-parsley-trigger="change"
                                                                required="" name="terms"  placeholder="Order">
                                                                <span class="glyphicon  form-control-feedback"></span>
                                                    </div> -->


                  <!-- /.box-body -->
                  
				   </div>
				   <div class="col-md-6">
              <div class="box-footer">
                 <button type="submit" class="btn btn-primary">update</button>
              </div>
          </div>

				  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
