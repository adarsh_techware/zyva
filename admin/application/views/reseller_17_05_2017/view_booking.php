<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <h1>
      Booking Details
    </h1>
        <br><div>
<!-- <a href="<?php// echo base_url(); ?>user/add_sub_category"><button class="btn add-new" type="button"><b><i class="fa fa-fw fa-plus"></i> Add New</b></button></a> -->
  </div>

    <ol class="breadcrumb">
       <li><a href="<?php echo base_url(); ?>"><i class=""></i>Home</a></li>
       <li class="active"><a href="<?php echo base_url(); ?>booking">Booking</a></li>
    </ol>
 </section>
 <!-- Main content -->
 <section class="content">
    <div class="row">
       <div class="col-xs-12">
          <?php
             if($this->session->flashdata('message')) {
                      $message = $this->session->flashdata('message');

                   ?>
          <div class="alert alert-<?php echo $message['class']; ?>">
             <button class="close" data-dismiss="alert" type="button">×</button>
             <?php echo $message['message']; ?>
          </div>
          <?php
             }
             ?>
       </div>
       <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
             <div class="box-header">
                <h3 class="box-title">View Booking Details</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body">
                <table id="" class="table table-bordered table-striped datatable">
                   <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Booking No:</th>
                        <th>Customer Name</th>
                        <th>Address</th>
                        <th>Total Rate</th>
                        <th>Booked Date</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                   </thead>
                   <tbody>
                     <?php
                         foreach($booking as $resellerdetail) {
                          //  print_r($user);die;

                         ?>
                      <tr>
                        <td class="hidden"><?php echo $resellerdetail->id; ?></td> 
                        <td class="center"><?php echo $resellerdetail->booking_no; ?></td>
                        <td class="center"><?php echo $resellerdetail->name; ?></td>
                        <td class="center">
                         
                          <?php echo $resellerdetail->address1; ?><br>
                          <?php echo $resellerdetail->mobile; ?><br>
                        </td>
                        <td class="center" align="right">&#8377;<?php echo $resellerdetail->total; ?>  </td>
                        <td class="center"><?php echo $resellerdetail->date; ?></td>
                        <td class="center">
                          <?php if($resellerdetail->status==1) {
                            echo "Booked";
                          }
                          else if($resellerdetail->status==2) {
                            echo "Processing";
                          }
                          else if($resellerdetail->status==3) {
                            echo "Delivered";
                          }
                          else {
                            echo "Cancelled";
                          }
                          ?>
                        </td>


                        <!-- <td>
               <span class="center label  <?php if($resellerdetail->status == '1')
      {
     // echo "label-success";
      }
      //else
      { 
     // echo "label-warning"; 
      }
      ?>"><?php //if($resellerdetail->status == '1')
      {
      //echo "enable";
      }
      //else
      { 
      //echo "disable"; 
      }
      ?></span>                                                                                                     
                    </td> -->
                        <td class="center">
                         

                          <!-- <a href="<?php echo base_url('Reseller/view/'.$userdetail->id); ?>">view</a> -->
                          <a class="btn btn-sm bg-olive show-booking1"  
                             id="booking_<?php echo $resellerdetail->id; ?>" href="javascript:void(0);"  
                             data-id="<?php echo $resellerdetail->id; ?>">
                            <i class="fa fa-fw fa-eye"></i> View 
                          </a>

                          <?php /*if( $resellerdetail->status){?>
                              <a class="btn btn-sm label-warning" href="<?php echo base_url();?>Reseller/Reseller_status/<?php echo $resellerdetail->id; ?>"> 
                              <i class="fa fa-folder-open"></i> Disable </a>           
                              <?php
                                 }
                                 
                                 else
                                 {
                                 ?>
                              <a class="btn btn-sm label-success" href="<?php echo base_url();?>Reseller/Reseller_active/<?php echo $resellerdetail->id; ?>"> 
                              <i class="fa fa-folder-o"></i> Enable </a>
                              <?php
                                 }*/
                                 ?>
                 
                      

                        </td>
                      </tr>
                      <?php
                         }
                         ?>
                   </tbody>
                   <tfoot>
                     <tr>
                        <th class="hidden">ID</th>
                        <th>Booking No:</th>
                        <th>Reseller Name</th>
                        <th>Address</th>
                        <th>Total Rtae</th>
                        <th>Booked Date</th>
                        <th>Status</th>
                        <th>Action</th>
                     </tr>
                   </tfoot>
                </table>
             </div>
             <!-- /.box-body -->
          </div>
          <!-- /.box -->
       </div>
       <!-- /.col -->
    </div>
    <!-- /.row -->
 </section>
 <!-- /.content -->
</div>



  <!-- Modal -->
 <div class="modal fade modal-wide" id="orderModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
   aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">View Order Details</h4>
         </div>
         <div class="modal-productbody">
         </div>
         <div class="business_info">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
