<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
 
 
<meta name="description" content="Online stylish Kurtis Shopping Store for Ladies and Girls. Cash on Delivery, Finest Collection, Best Prices">
<meta name="author" content="">
<meta name="theme-color" content="#9c2c80">
<title>Zyva Reseller Form</title>
<link rel="stylesheet" href="bootstrap.min.css">
<link rel="stylesheet" href="parsley.css">
<style type="text/css">
  input.parsley-error, select.parsley-error, textarea.parsley-error{
    width: 100% !important;
  }
  .loader {
    text-align: center;
    border: 4px solid #ccc;
    border-radius: 50%;
    border-top: 4px solid #472b73;
    width: 50px;
    height: 50px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }
    @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
  .tr {
    width:100% !important;
  }

  .title_field {
    width:25% !important;
  }
  .input_field {
    width:100% !important;
  }
</style>
</head>
<body>
<div style="background:url(https://beta.zyva.in/assets/images/bg_image.png);background-repeat: repeat; background-size:cover;">
<div class="container">
  <div class="form_container">
  <div class="first_heading">
    <h3 style="color:#5b4180; text-align:center; margin:50px auto; font-size: 30px; font-family: 'Open Sans',sans-serif;">Reseller Registration</h3>
  </div> 
    <form action="index.php" method="post" name="reseller" data-parsley-validate="" class="validate" id="reseller">
    <div class="col-md-12">
    <div class="error_msg" style="color: red;text-align: center;font-size: 14px"></div>
    <table class="table table-bordered">
      <!-- <thead>
        <tr>
          <th colspan="2" style="text-align:center">Reseller Registration</th>
        </tr>
      </thead>
      <tbody> -->
        <tr>
          <td class="title_field">First Name</td>
          <td class="input_field">
            <input type="text" class="form-control required input_length" data-parsley-trigger="change" data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="first_name"  placeholder="First name" >
            <span class="glyphicon  form-control-feedback"></span>
        </td>
        </tr>
        <tr>
          <td class="title_field">Last Name</td>
          <td class="input_field">
            <input type="text" class="form-control required input_length"  data-parsley-trigger="change" data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="last_name"  placeholder="Last name">
            <span class="glyphicon  form-control-feedback"></span>
          </td>
        </tr>
        <tr>
          <td class="title_field">Mobile</td>
          <td class="input_field">
            <input type="text" class="form-control required input_length" name="telephone" data-parsley-trigger="keyup" data-parsley-type="digits" data-parsley-minlength="10" data-parsley-maxlength="15" data-parsley-pattern="^[0-9]+$" required="" placeholder="Mobile">
            <span class="glyphicon  form-control-feedback"></span>
          </td>
        </tr>
        <tr>
          <td class="title_field">Email Id</td>
          <td class="input_field"> 
            <input type="email" class="form-control required input_length" name="email" data-parsley-trigger="keyup"   required="" placeholder="Email">
            <span class="glyphicon  form-control-feedback"></span>
          </td>
        </tr>
        <tr>
          <td class="title_field">Address</td>
          <td class="input_field">
            <textarea class="form-control required input_length" name="address" data-parsley-trigger="change" required="" placeholder="Address" rows="5"></textarea>
            <span class="glyphicon  form-control-feedback"></span>
          </td>
        </tr>
        <tr>
          <td colspan="2"><b>Bank Information</b></td>
        </tr>
        <tr>
          <td class="title_field">Account No</td>
          <td class="input_field">
            <input type="text" class="form-control input_length required" name="account_no" data-parsley-trigger="keyup" data-parsley-pattern="^[0-9]+$" placeholder="Bank Account No" required="">
            <span class="glyphicon  form-control-feedback"></span>
          </td>
        </tr>
        <tr>
          <td class="title_field">Account Holder</td>
          <td class="input_field">
            <input type="text" class="form-control input_length required" name="account_holder" data-parsley-trigger="keyup"  placeholder="Holder Name" required="">
          <span class="glyphicon  form-control-feedback"></span>
          </td>
        </tr>
        <tr>
          <td class="title_field">IFSC Code</td>
          <td class="input_field">
            <input type="text" class="form-control input_length required" name="ifcs_code" data-parsley-trigger="keyup"  placeholder="IFSC Code" required="">
            <span class="glyphicon  form-control-feedback"></span>
          </td>
        </tr>
        <tr>
          <td class="title_field">Bank name</td>
          <td class="input_field">
            <input type="text" class="form-control input_length required" name="bank_name" data-parsley-trigger="keyup"  placeholder="Bank name" required="">
            <span class="glyphicon  form-control-feedback"></span>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <div class="button_message" style="text-align:center; width:100px;">
              <div class="loader" style="display:none;"></div>
                <button id="submit" name="submit" type="submit" style="width:100px; height:35px; background-color: #5b4180; border:1px solid #5b4180; border-radius:20px; color: #fff; outline: none;">Register</button>
              </div>
          </td>
        </tr>
      </tbody>

    </table>
    </div>
    </form>
</div>
<!-- ============================MODAL -->
<div class="modal fade" id="resellerModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="remove_id">&times;</button>
          <h4 class="modal-title">OTP Verification</h4>
        </div>
        <div class="modal-body">
          <p>Enter OTP</p>
          <span style="color: green" id="msg_info"></span>
          <input type="password" name="otp" id="otp" style="background-color: transparent; width:100%">

        <div class="modal-footer">
          <div class="" style="float:left">
              <button type="button" class="btn btn-default" id="verify">Verify</button>
            </div>
            <div class="" style="float:right">
              <button type="button" class="btn btn-default" id="resend">Resend</button>
            </div>
            <input type="hidden" name="last_id" id="last_id">
        </div>
      </div>
    </div>
  </div>

</div>

<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="parsley.min.js"></script>
<script>
/*$(document).ready(function(){
        $("#resellerModal").modal('show');
});*/
</script>
<script type="text/javascript">
  $('#submit').on('click',function(){
    $('.error_msg').fadeIn();
    if($("#reseller").parsley().isValid()){
      $('#submit').hide();
      $('.loader').show();
      var result = $('#reseller').serialize();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'type=register&'+result,
        success: function(result){
            $('.loader').hide();
            $('#submit').show();
            var obj = JSON.parse(result);
            if(obj.status==1){
              $('#last_id').val(obj.last_id);
              $("#resellerModal").modal('show');
            } else if(obj.status==2){
              $('.error_msg').html('Email Id or phone no are already exists');
              setTimeout(function(){ $('.error_msg').fadeOut(); }, 3000);
                
            } else {
                $('.error_msg').html('Some error occured in Registration');
                setTimeout(function(){ $('.error_msg').fadeOut(); }, 3000);
            }
        }
          
      });
    }
  })

    $('#verify').on('click',function(){
      $('#msg_info').html('');
      var last_id = $('#last_id').val();
      var otp = $('#otp').val();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'type=verify&otp='+otp+'&id='+last_id,
        success: function(result){
           var obj = JSON.parse(result);
           if(obj.status==1){
            $('#msg_info').html('Thank You. OTP Verification successfully. Your registration under approval stage');
            setTimeout(function(){ window.location.reload(); }, 3000);
           } else if(obj.status==2){
            $('#msg_info').html('Sorry, Invalid OTP Please try again');
           } else {
            $('#msg_info').html('Some error occured in Verification please co-operate');
           }

        }
          
      });
    })

    $('#resend').on('click',function(){
      var result = "id="+$('#last_id').val();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'type=resend&'+result,
        success: function(result){
            var obj = JSON.parse(result);
           if(obj.status==1){
            $('#msg_info').html('OTP Resend successfully');
           } else {
            $('#msg_info').html('Some error occured in OTP sending');
           }
        }
          
      });
    })

    $("#remove_id").on('click',function(){
      var result = "id="+$('#last_id').val();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'type=remove&'+result,
        success: function(result){
           
        }
          
      });
    })



</script>
</div>
</body>

</html>
