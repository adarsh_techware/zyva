<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
 
 
<meta name="description" content="Online stylish Kurtis Shopping Store for Ladies and Girls. Cash on Delivery, Finest Collection, Best Prices">
<meta name="author" content="">
<meta name="theme-color" content="#9c2c80">
<title>Zyva Reseller Form</title>
<link rel="stylesheet" href="bootstrap.min.css">
<link rel="stylesheet" href="parsley.css">
<style type="text/css">
  input.parsley-error, select.parsley-error, textarea.parsley-error{
    width: 100% !important;
  }
</style>
</head>
<body>
<form action="index.php" method="post" name="reseller" data-parsley-validate="" class="validate" id="reseller">
<div class="col-md-6">
<table class="table table-bordered">
  <thead>
    <tr>
      <th colspan="2" style="text-align:center">Reseller Registration</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>First Name</td>
      <td><input type="text" class="form-control required input_length" data-parsley-trigger="change" data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="first_name"  placeholder="First name" >
      <span class="glyphicon  form-control-feedback"></span></td>
    </tr>
    <tr>
      <td>Last Name</td>
      <td><input type="text" class="form-control required input_length"  data-parsley-trigger="change" data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="last_name"  placeholder="Last name">
                            <span class="glyphicon  form-control-feedback"></span></td>
    </tr>
    <tr>
      <td>Mobile</td>
      <td> <input type="text" class="form-control required input_length" name="telephone" data-parsley-trigger="keyup" data-parsley-type="digits" data-parsley-minlength="10" data-parsley-maxlength="15" data-parsley-pattern="^[0-9]+$" required="" placeholder="Mobile">
      <span class="glyphicon  form-control-feedback"></span></td>
    </tr>
    <tr>
      <td>Email Id</td>
      <td> <input type="email" class="form-control required input_length" name="email" data-parsley-trigger="keyup"   required="" placeholder="Email">
                          <span class="glyphicon  form-control-feedback"></span></td>
    </tr>
    <tr>
      <td>Address</td>
      <td><textarea class="form-control required input_length" name="address" data-parsley-trigger="change" required="" placeholder="Address" rows="5"></textarea>
      <span class="glyphicon  form-control-feedback"></span></td>
    </tr>
    <tr>
      <td colspan="2"><b>Bank Information</b></td>
    </tr>
    <tr>
      <td>Account No</td>
      <td><input type="text" class="form-control input_length" name="account_no" data-parsley-trigger="keyup" data-parsley-pattern="^[0-9]+$" placeholder="Bank Account No">
      <span class="glyphicon  form-control-feedback"></span></td>
    </tr>
    <tr>
      <td>Account Holder</td>
      <td><input type="text" class="form-control input_length" name="account_holder" data-parsley-trigger="keyup"  placeholder="Holder Name">
      <span class="glyphicon  form-control-feedback"></span></td>
    </tr>
    <tr>
      <td>IFSC Code</td>
      <td><input type="text" class="form-control input_length" name="ifcs_code" data-parsley-trigger="keyup"  placeholder="IFSC Code">
      <span class="glyphicon  form-control-feedback"></span></td>
    </tr>
    <tr>
      <td>Bank name</td>
      <td><input type="text" class="form-control input_length" name="bank_name" data-parsley-trigger="keyup"  placeholder="Bank name">
       <span class="glyphicon  form-control-feedback"></span></td>
    </tr>
    <tr>
      <td colspan="2" align="center"><button id="submit" name="submit" type="submit" id="submit">Register</button></td>
    </tr>
  </tbody>

</table>
</div>
</form>
<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="parsley.min.js"></script>
<script type="text/javascript">
  $('#submit').on('click',function(){
    
    if($("#reseller").parsley().isValid()){
      var result = $('#reseller').serialize();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'type=register&'+result,
        success: function(result){
            console.log(result);
        }
          
      });
    }
  })
</script>
</body>

</html>
