
/////////state city///////
function selectCity(state_id){
	
	
	if(state_id!="-1"){
		//alert("hi");
		loadDatadoctor('city',state_id);
		$("#city_dropdown").html("<option value='-1'>Select city</option>");	
	}
}



function loadDatadoctor(loadType,loadId){
	
	var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
	
	$.ajax({
		type: "POST",
		  url: base_url+"Account/loadData",
		data: dataString,
		cache: false,
		success: function(result){
		
			$("#"+loadType+"_dropdown").html("<option value='-1'>Select "+loadType+"</option>");  
			$("#"+loadType+"_dropdown").append(result);  
		},
		error: function() {
			alert("error");
		}
	});
}


///send mail from contact//
$('#sene_mails').on('click', function (e) {
	if ($('#contact_form').parsley().validate()) {
		var value=$('#contact_form').serialize();
			
			//alert(value);
			
			var data = {value:value};
			var url = base_url+'About/send_mail';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			// alert(result);
			console.log(obj.status);
			if(obj.status=="success"){
				$(".contact_msg").show();
				$(".contact_msg").html('');
				$(".contact_msg").html('<p class="">Your Message Sent Sucessfully</p>');
				$('#contact_form')[0].reset();
				setTimeout(function(){$(".contact_msg").hide();}, 5000);
				
			}
			else{
				$(".contact_msg").show();
				$(".contact_msg").html('');
				$(".contact_msg").html('<p class="">Error Occuring In Sending Message</p>');
				setTimeout(function(){$(".contact_msg").hide();}, 5000);
			}
		}
				
});



/////////////add to temp/////////

$('#stitches').on('click', function (e) {

		cart = $('#cart').serialize();
		stitch = $('#stitching_cat').serialize();
		var data = {value:cart,stitch:stitch};
		var url = base_url+'cart/addtotemp';
		var result = post_ajax(url, data);
		obj = JSON.parse(result);

		if(obj.status=="success"){
				$(".errormsg").html('');
				$(".errormsg").html('<p class="alert-success alert_pad">'+obj.message+'</p>');
				// $(".errormsg").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg").hide();
					location.reload();}, 3000);
				//$('#stichingModal').trigger();
				$('#stichingModal').modal('toggle');				

			}
			else{
				
				$(".errormsg").html('');

				$(".errormsg").html('<p class="alert-danger alert_pad">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg").hide();}, 3000);
			}

			return false;
			
			if($('#front_id').val()!=='' || $('#back_id').val()!=='' || $('#sleeve_id').val()!==''|| $('#hemline_id').val()!==''){
            //alert('f-'+$('#front_id').val()+'b'+$('#back_id').val()+'s'+$('#sleeve_id').val()+'k'+$('#hemline_id').val());
   
			var value=$('#cart').serialize();
			var data = {value:value,};
			var url = base_url+'cart/addtotemp';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			// $('.rev').html('');
			
			console.log(obj.status);
			if(obj.status=="success"){
				$(".errormsg").html('');
				$(".errormsg").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg").hide();}, 3000);
				$('#stichingModal').modal('hide');
				$('#order_template').html(obj.template);
				stich_add();
			}
			else{
				$(".errormsg").html('');
				$(".errormsg").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg").hide();}, 3000);
			}
			
			  }
			   else{
			   	      //alert("select  stitching measure mandatory");

			   	  $(".front_styles").html('Select stitching. One basic measurement mandatory');
			   }
			   e.preventDefault();

			
    });




////add to cart when stiching enabled///
function stich_add(){

$('#add_cart').on('click', function (e) {

			var value=$('#cart').serialize();
			var data = {value:value};
			var url = base_url+'cart/addtocart';
			var result = post_ajax(url, data);
			// alert(result);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){
				//alert(success);
				$(".errormsg3").html('');
				$(".errormsg3").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg3").hide();}, 3000);				
			}
			else{
				
				$(".errormsg3").html('');
				$(".errormsg3").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg3").hide();}, 3000);
			}
			
  });		
 
}


function get_size_list(product_id,color_id){
	var data = {'product_id':product_id,'color_id':color_id};
	var url = base_url+'ajax/size_list';
	$.ajax({
	    type: "POST",
	    url: url,
		data: data,
		success: function(response) {
			$('.size_list').html(response);
			size_list();
			
		}
	});

	function size_list(){
    $('.size_new_opt').on('click',function(){
    	$("#id").attr('disabled','disabled');
    	$('.add_bag').removeAttr("disabled");
    	$('#chkPassport').prop('checked', false);
        $('#chkPassport').attr('checked', false);
        $(".sizes").removeClass('act_size');
        $(this).toggleClass('act_size');
        var size_id = $(this).data('id');
        $('#size_id').val(size_id);
        $("#addtocart1").show();
        $("#addtocart").hide();
    })
}

	
}
/////////////add to bag//

	
$('#addtocart1').on('click', function (e) {
		e.preventDefault();
		//alert($('#size_id').val());
		if($('#size_id').val()!='' && $('#color_id').val()!=''){
       
   
		var value=$('#cart').serialize();
			
			// alert(value);
			
			var data = {value:value};
			var url = base_url+'cart/addtocart1';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);

			if(obj.status=="success"){
				$(".add_cart_success").html('');
				$(".add_cart_success").html('<p class="alert-success alert_pad">'+obj.message+'</p>');
				setTimeout(function(){$(".add_cart_success").hide();}, 3000);
				location.reload();
			}
			else{
				
				$(".add_cart_success").html('');
				$(".add_cart_success").html('<p class="alert-danger alert_pad">'+obj.message+'</p>');
				setTimeout(function(){$(".add_cart_success").hide();}, 3000);
			}
			
		}	
		else { 
			// $(".front_style").html('');
	  		$(".front_style").html('<p class="alert-danger alert_pad">Please Select Color and Size</p>');
	  		// setTimeout(function(){$(".front_style").hide();}, 3000);
		}	
			
    });
	
	
	
	
/////enquiry///////
$('#enquires').on('click', function (e) {
	e.preventDefault();
	if ($('#enquiry1').parsley().validate()) {
		var value=$('#enquiry1').serialize();
			
			// alert(value);
			
			var data = {value:value};
			var url = base_url+'Stitching/addtoenquiry';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){
				$(".enq_status").show();
				$(".enq_status").html('');
				$(".enq_status").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){
					$("#enqireModal").modal('hide');
				}, 3000);
				location.reload();
			}
			else{
				$(".enq_status").show();
				$(".enq_status").html('');
				$(".enq_status").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".enq_status").hide();}, 3000);
			}
		}
			
			
			
    });



	////update to wishlist////////
	$('#updatetowishlist').on('click', function (e) {
		e.preventDefault();
		
		
		
		var value=$('#cart').serialize();
			
			//alert(value);
			
			var data = {value:value};
			var url = base_url+'wishlist/updatetowishlist';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){
				$(".errormsg").html('');
				$(".errormsg").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg").hide();}, 3000);
			}
			else{
				
				$(".errormsg").html('');
				$(".errormsg").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg").hide();}, 3000);
			}
			
			
			
    });
	//////////add to wishlist////////

	/*$('#addtowishlist').on('click', function (e) {
		e.preventDefault();
		
		
		var value=$('#cart').serialize();
			
			//alert(value);
			
			var data = {value:value};
			var url = base_url+'wishlist/addtowishlist';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){
				$(".errormsg").html('');
				$(".errormsg").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".error_div").hide();}, 3000);
			}
			else{
				
				$(".errormsg").html('');
				$(".errormsg").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".error_div").hide();}, 3000);
			}
			
		
			
    });*/
	

	
	
	
	$('#addtowishlist').on('click', function (e) {
		e.preventDefault();
		if($('#size_id').val()!=''){
		
		var value=$('#cart').serialize();
			
			//alert(value);
			
			var data = {value:value};
			var url = base_url+'wishlist/addtowishlist';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			console.log(obj.status);
			if(obj.status=="success"){
				$(".errormsg").show();
				$(".errormsg").html('');
				$(".errormsg").html('<p class="alert-success alert_pad">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg").hide();}, 3000);
				

			}
			else{
				$(".errormsg").show();
				$(".errormsg").html('');
				$(".errormsg").html('<p class="alert-danger alert_pad">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg").hide();}, 3000);
			}
			}
else 
{ 

	$(".front_style").html('select size and color');
}	
		
			
    });
	
$('#wishlisttocart').on('click', function (e) {
		e.preventDefault();
		//alert("hi");
		var value=$('#cart').serialize();
			
			// alert(value);
			
			var data = {value:value};
			var url = base_url+'wishlist/addtocart';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){
				$(".errormsg1").html('');
				$(".errormsg1").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg1").hide();}, 3000);
				location.reload();
			}
			else{
				
				$(".errormsg1").html('');
				$(".errormsg1").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg1").hide();}, 3000);
			}
			
			
			
    });
    
	
	//////add all to cart///////
	$('#addall').on('click', function (e) {
		e.preventDefault();
		
	var url = base_url+'Wishlist/countwishlist';
	var result = post_ajax(url);
	obj = JSON.parse(result);
	
	if(obj.count=='0')
	{
		$(".errormsg2").html('Your Wishlist is empty');
		setTimeout(function(){$(".errormsg2").hide();}, 2000);
	}
	else
	{
	     addall();
	}
		
    });
	
	
	function addall()
	{
		                var data = $('#cart').serialize();		
						var url = base_url+'cart/addalltocart';
						var result = post_ajax(url, data);
						obj = JSON.parse(result);
						console.log(obj.status);
						if(obj.status=="success")
						{
							
							$(".errormsg1").html('');
							$(".errormsg1").html('<p class="error">'+obj.message+'</p>');
							setTimeout(function(){$(".errormsg1").hide();}, 3000);
							location.reload();
						
						}
						else if(obj.status=="exist")
						{
							$(".errormsg1").html('');
							$(".errormsg1").html('<p class="error">'+obj.message+'</p>');
							setTimeout(function(){$(".errormsg1").hide();}, 3000);
							location.reload();
							
						}
	              	else 
						{
							$(".errormsg1").html('');
							$(".errormsg1").html('<p class="error">'+obj.message+'</p>');
							setTimeout(function(){$(".errormsg1").hide();}, 3000);
							location.reload();
							
						}
		
		
		
		
		
	}
	
	
	
	
	///////////add to checkout////////
	$('#addtocheckout').on('click', function (e) {
		window.location.href = base_url+'checkout';
		e.preventDefault();
	     
		 //alert("hi");
	
		
					
			
			
			
    });
	/////////checkout////////
	$('#checkout').on('click', function (e) {
			     
			var url = base_url+'Checkout/countcart';
			var result = post_ajax(url);
			obj = JSON.parse(result);	
			if(obj.count=='0')
			{
				
				$(".errormsg2").html('Your cart is empty');
				setTimeout(function(){$(".errormsg2").hide();}, 2000);
			}
			else
			{
				 window.location.href= base_url+'Checkout/';
			}
			
			
    });
	
	//////remove from cart///////
	$('.remove_pdt').on('click', function (e) {
		e.preventDefault();
			var id = $(this).data('id');
			var data = {id:id};
			var url = base_url+'cart/remove_cart';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success")
			{

				if(obj.count==0)
				{
					// window.location.href = base_url;
					location.reload();
				} 
				else
					{
					location.reload();
				}
				
				
				
				
				
			}
			
			
			
    });
	//////remove from wishlist///////
	$('.remove_btn_order').on('click', function (e) {
		var a = confirm('Are you sure that you want to delete this item?');
		if(a==false){
			return false;
		} else {

			var id = $(this).data('id');
			//alert(id);
			var data = 'id='+id;
			
			var url = base_url+'Myorder/remove_order';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){
					location.reload();
			}
		}
			
			
			
    });


    $('.remove_btn').on('click', function (e) {
		var a = confirm('Are you sure that you want to delete this item?');
		if(a==false){
			return false;
		} else {


		e.preventDefault();
		
		
		//alert("remove");
		var value=$('#cart').serialize();
			
			//alert(value);
			 var id = $(this).data('id');
			//alert(id);
			var data = {'id':id};
			
			var url = base_url+'Wishlist/removewishlist';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){
				window.location.reload();
			} else {
				$('#error_message').html(obj.message);
			}


		}
			
			
			
    });
	
	
	
	
	
	
	
	
	/////remove myorder////////////
	$('.remove1').on('click', function (e) {
		e.preventDefault();
		
		
		//alert("remove");
		var value=$('#order').serialize();
			
			//alert(value);
			 var id = $(this).data('id');
			// alert(id);
			var data = {id:id};
			var url = base_url+'cart/remove_cart';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){

				if(obj.count==0){
					window.location.href =base_url+'Women/index/women';
				} else {
					location.reload();
				}
				
				
			}
			
    });
	
	////remove orfder//////
	/*$('.remove2').on('click', function (e) {
		e.preventDefault();
		
		
		//alert("remove");
		//var value=$('#order').serialize();
			
			//alert(value);
			 var id = $(this).data('id');
			// alert(id);
			var data = {id:id};
			var url = base_url+'Myorder/removeorder';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){

				if(obj.count==0){
					window.location.href =base_url+'Women/index/women';
				} else {
					location.reload();
				}
				
				
			}
			
    });*/
	
	
/////////////edit cart in cart///////////
/*$('.edit').on('click', function (e) {
		e.preventDefault();
		
			var id = $(this).data('id');
			 alert(id);
			 var data = {id:id};
			var url = base_url+'checkout/edit';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			
		   window.location.href=base_url+"Women/index/Women";
		
			
			
			
    });*/


	$('#continue').on('click', function (e) {
		e.preventDefault();
		
					
			
		   window.location.href=base_url;
		
			
			
			
    });
	
	
	
	
	
	
	
	
	
	
	
	
	
	
///end///////


	/////////update or continue wishlist/////
	$('#update').on('click', function (e) {
		e.preventDefault();
		//alert("hi");
		 //window.location.href= 'Women/index/Women';
		  window.location.href=base_url+"Women/index/Women";
		
    });
	
	
	///// size button/////
	
/*	// jQuery("#btn .sizes").addClass('act_size');
	jQuery("#btn .sizes").click(function(){
		//alert($("#size_id").val());
        jQuery("#btn .sizes").removeClass('act_size');
        jQuery(this).toggleClass('act_size'); 
		 var id = $(this).data('id');
		 //alert(id);
		     $("#size_id").val(id);
		  $("#size").val(id);



		  ////sizeserch//

		   // var value=$('.search_dress').val();
			
			// alert(value);
			
			var data = {value:id};
			var url = base_url+'Women/size_search';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			 

			if(obj){
				
				
				$('#search_content').html(obj.template);
			}
			else{
				
				$(".errormsg1").html('');
				$(".errormsg1").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg1").hide();}, 3000);
			}



});*/
	
	



	/////// image neck/////////


// jQuery("#imgg").addClass('round_bg');
// 	jQuery("#imgg .click_round").click(function(){
// 		//alert($("#size_id").val());
//         jQuery("#imgg .click_round").removeClass('stich_images');
//         jQuery(this).toggleClass('stich_images'); 
// 		 var id = $(this).data('id');
// 		 //alert(id);
// 		     // $("#size_id").val(id);
		
// });



	////////////insert stitching /////

	/*-------------ASHA----------------*/
	/*$('#stitches').on('click', function (e) {
		e.preventDefault();
	
		
		 //alert('hai');
					
		var data = $('stitching1').serialize();
		//alert(data);
		
			var url = base_url+'cart/add_stitching';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){
				$(".errormsg").html('');
				$(".errormsg").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".error_div").hide();}, 3000);
			}
			else{
				
				$(".errormsg").html('');
				$(".errormsg").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".error_div").hide();}, 3000);
			}
			
			
			
    });*/



    /**********************************************/
	
	////////pending order////
$('#pendingorder').on('click', function (e) {
		e.preventDefault();
		
 window.location.href= 'Checkout/';
			
			
    });
	
	
	
	
	
	
	
	//add to measurement///
	
	
	$('.save').on('click', function (e) {
		e.preventDefault();
		//alert("hi");
		if ($('.measurement').parsley().validate()){
		
		
		var value=$('.measurement').serialize();
			
			//alert(value);
			
			var data = {value:value};
			var url = base_url+'Measurement/add_measurement';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){
				//alert("sucess");
				$(".measure_status").show();
				$(".measure_status").html('');
				$(".measure_status").html('<p class="alert-success alert_pad">'+obj.message+'</p>');
				setTimeout(function(){$(".measure_status").hide();}, 3000);
                 // window.location.href=base_url+"Women/index/Women";
			}
			else{
				$(".measure_status").show();
				$(".measure_status").html('');
				$(".measure_status").html('<p class="alert-danger alert_pad">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsgs").hide();}, 3000);
			}
			
		}	
			
    });

	
	
	
	$('#checkbox-3').on('click', function (e) {
		// e.preventDefault();
		//alert("hi");
		var p= $("#checkbox-3").val();
		// alert(p);
		 $("#check1").val(p);
		
			
			
    });
	$('#checkbox-4').on('click', function (e) {
		// e.preventDefault();
		//alert("hi");
		var p= $("#checkbox-4").val();
		// alert(p);
		 $("#check1").val(p);
		
			
			
    });
	$('#checkbox-5').on('click', function (e) {
		// e.preventDefault();
		//alert("hi");
		var p= $("#checkbox-5").val();
		// alert(p);
		 $("#check2").val(p);
		
			
			
    });

	
	
	$('#pay').on('click', function (e) {

		if ($("#checkbox-7").is(':checked')) {
		
			var data=$('#payment_sec').serialize();
		
			var url = base_url+'cart/payment';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			
			console.log(obj.status);
			if(obj.status=="success"){
				window.location.href = base_url+'checkout/success/'+obj.booked_id;
				
			}
			else{
				
				$(".errormsg1").html('');
				$(".errormsg1").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg1").hide();}, 3000);
			}
				
	    }
	    else {
	    	$(".place_order_alert").html('');
			$(".place_order_alert").html('<p class="alert alert-danger alert_pad">Please Select Place Order</p>');
			// setTimeout(function(){$(".errormsg1").hide();}, 3000);
	  		return false;
	  	}
		
			
			
    });
	
	
		
           ///search///

           $('#search').on('click', function (e) {
		e.preventDefault();
		//alert("hi");
		
		
       
   var value=$('.search_dress').val();
			
			// alert(value);
			
			var data = {value:value};
			var url = base_url+'Women/search';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			 

			if(obj){
				
				//$(".errormsg1").html('');
				//$(".errormsg1").html('<p class="error">'+obj.message+'</p>');
				//setTimeout(function(){$(".error_div").hide();}, 3000);
				$('#search_content').html(obj.template);
			}
			else{
				
				$(".errormsg1").html('');
				$(".errormsg1").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg1").hide();}, 3000);
			}
			
			
			
    });       


    //*********new measurement********





    $('.save1').on('click', function (e) {
    	if ($('.measurement').parsley().validate()) {
	    	// alert('asdasdas');
	    	var value=$('.measurement').serialize();
	    	var data = {value:value};
			var url = base_url+'Measurement/add_newmeasurement';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);
			if(obj.status=="success"){
				$('#stitch_id').val(obj.stitch_id);
				$('#measurement').modal('hide');
			} else {
				$(".errormsgs").html('');
				$(".errormsgs").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsgs").html('');}, 3000);
			}
		}
			
    });
	
	       
////////move from cart to wishlist///////
	
	
	$('.move').on('click', function (e) {
		
			
			 var id = $(this).data('id');
			var data = {id:id};
			var url = base_url+'Wishlist/move';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);	
			
			if(obj.status=="success"){
				
				$(".errormsgs").html('');
				$(".errormsgs").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsgs").hide();}, 3000);
                location.reload();
			}
			else{
				
				$(".errormsgs").html('');
				$(".errormsgs").html('<p class="error">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsgs").hide();}, 3000);
			}
		    
		
	
			
			
    });
	
		
			
	$('#can').click(function(){
		
        $("#addressbook").trigger('reset');
  });		
$('#can1').click(function(){
		
        $("#accountdetails").trigger('reset');
  });	
	
	
	
	
	
	//footer script//////
	function post_ajax(url, data) {
	var result = '';
	$.ajax({
	    type: "POST",
	    url: url,
		data: data,
		success: function(response) {
			result = response;
			
		},
		error: function(response) {
			result = 'error';
		},
		async: false
	});
	return result;
}


$('#signup_button').on('click', function (e) {	
	$(this).hide();
///alert("hi");			
	if ($('#signup_form').parsley().validate()) {
		var value=$('#signup_form').serialize();
		//alert(value);
		var data = {value:value};
		
		var url = base_url+'Home/sign_up';
		var result = post_ajax(url, data);		
		console.log(result.status);
		obj = JSON.parse(result);
		console.log(obj.status);		
		if(obj.status=="password") {
			$(".error_div2").show()			
			$(".error_div2").html('');
			$(".error_div2").html('<p class="error">'+obj.message+'</p>');
			setTimeout(function(){$(".error_div2").hide();}, 6000);	
			$('#signup_button').show();
		}

		else if(obj.status=="email") {
			$(".error_div1").show()
			$(".error_div1").html('');
			$(".error_div1").html('<p class="error">'+obj.message+'</p>');
			 setTimeout(function(){$(".error_div1").hide();}, 6000);
			 $('#signup_button').show();	
		}
		else if(obj.status=="success"){
			$("#myModal_reg").modal('hide');			
			$("#myModal").modal('show');
			$('body').css('overflow-x', 'hidden');
            $('.home_bg').addClass('no_index');

			
   			//   $('#myModal_reg').on('hidden', function () {
			//   $('#myModal').modal('show')
			// })

			$(".error_div").html('');
			$(".error_div").html('<p class="error">'+obj.result+'</p>');
			  setTimeout(function(){$(".error_div").hide();}, 6000);			
		}		
	}
	e.preventDefault();
});
	
	//
	$('#signup_button1').on('click', function (e) {	
///alert("hi");			
	if ($('#signup_form1').parsley().validate()) {
		var value=$('#signup_form1').serialize();
		//alert(value);
		var data = {value:value};
		
		var url = base_url+'Home/sign_up';
		var result = post_ajax(url, data);		
		console.log(result.status);
		obj = JSON.parse(result);
		console.log(obj.status);		
		if(obj.status=="password") {			
			$(".error_div2").html('');
			$(".error_div2").html('<p class="error">'+obj.message+'</p>');
			 setTimeout(function(){$(".error_div2").hide();}, 3000);	
		}

		else if(obj.status=="email") {
			$(".error_div1").html('');
			$(".error_div1").html('<p class="error">'+obj.message+'</p>');
			 setTimeout(function(){$(".error_div1").hide();}, 3000);	
		}
		else if(obj.status=="success"){	

			$("#myModal1").modal('show');				
			$("#myModal_reg1").modal('hide');
			$(".error_div").html('');
			$(".error_div").html('<p class="error">'+obj.result+'</p>');
			  setTimeout(function(){$(".error_div").hide();}, 3000);			
		}		
	}
	e.preventDefault();
});
	

/*$('#login_button').on('click', function (e) {	
	e.preventDefault();
	
	if ($('#login_form').parsley().validate()){
		//alert("hi");
		var value=$('#login_form').serialize();
		//alert(value);
		var data = {value:value};
		var url = base_url+'Home/login';
		var result = post_ajax(url, data);
		//console.log(result);
		obj = JSON.parse(result);
		
		console.log(obj.status);

		if(obj.status=="incorrect"){
			$(".error_div").show()
			$(".error_div").html('');
			$(".error_div").html('<p class="error">'+obj.message+'</p>');
			// setTimeout(function(){
				$(".error_div").hide();
			// }, 3000);
		}
		else if(obj.status=="success"){
			location.reload();
		}
	}
});

	
	
$('#login_button1').on('click', function (e) {	
	if ($('#login_form1').parsley().validate()){
		var value=$('#login_form1').serialize();
		var data = {value:value};
		var url = base_url+'Home/login';
		var result = post_ajax(url, data);
		obj = JSON.parse(result);		
		console.log(obj.status);
		if(obj.status=="incorrect"){
			$(".error_div").html('');
			$(".error_div").html('<p class="error">'+obj.message+'</p>');
			setTimeout(function(){$(".error_div").hide();}, 3000);
		} else if(obj.status=="success"){			
			 location.reload();		 
		}
	}
	e.preventDefault();
});*/


  
	
	
	






	
   
	
	
//////////edit account////////	

$('.chg_pwd').on('click', function (e) {

	// if($("#accountdetails").parsley().isValid()){


	var value=$('#accountdetails').serialize();
	var data = {value:value};
	var url = base_url+'Account/edit_account';
	var result = post_ajax(url, data);
	obj = JSON.parse(result);
	console.log(obj.status);
	if(obj.status=="error"){
		$(".errormsg").show();
		$(".errormsg").html('');
		$(".errormsg").html('<p class="alert alert-danger">'+obj.message+'</p>');
		setTimeout(function(){$(".errormsg").hide();}, 3000);
	} else if(obj.status=="cpasswordmissmatch"){
		$(".errormsg").show();		 
		$(".errormsg").html('');
		$(".errormsg").html('<p class="alert alert-danger">'+obj.message+'</p>');		
	} else if(obj.status=="passwordmismatch"){
		$(".errormsg").show();		 
		$(".errormsg").html('');
		$(".errormsg").html('<p class="alert alert-danger">'+obj.message+'</p>');
	} else {
		$(".errormsg").show();
		$(".errormsg").html('');
		$(".errormsg").html('<p class="alert alert-success">'+obj.message+'</p>');
		setTimeout(function(){$(".errormsg").hide();}, 3000);
	}
	e.preventDefault();
// }
			
});


////////edit address book////////
	
	$('#edit1').on('click', function (e) {
		// alert('abc');
		 if($("#addressbook").parsley().isValid()){
		// alert('123');	
			var value=$('#addressbook').serialize();
			var data = {value:value};
			var url = base_url+'Account/add_addressbook';
			var result = post_ajax(url, data);
			obj = JSON.parse(result);

			if(obj.status=="success"){
						
				$(".errormsg3").html('');
				$(".errormsg3").html('<p class="alert alert-success">'+obj.message+'</p>');
				$('#addressbook')[0].reset();
				setTimeout(function(){$(".errormsg3").hide();}, 3000);
               location.reload();
			}
			else if(obj.status=="error"){
				
				$(".errormsg3").html('');
				$(".errormsg3").html('<p class="alert alert-danger">'+obj.message+'</p>');
				setTimeout(function(){$(".errormsg1").hide();}, 3000);
			}
			// else if(obj.status=="info"){
				
			// 	$(".errormsg3").html('');
			// 	$(".errormsg3").html('<p class="alert alert-info">'+obj.message+'</p>');
			// 	setTimeout(function(){$(".errormsg1").hide();}, 3000);

			return false;
			
		 }
			
		
	});


$('.check_tab_bill').show();
$('.check_tab_ship').hide();
$('.check_tab_order').hide();
$('.tab_head_one').addClass('tab_active_head');
$('.count_one').addClass('tab_active_round');

$('.bill_next').on('click',function(){
   // if ($('.tabform').parsley().validate()) {
    $('.tab_head_one').removeClass('tab_active_head');
    $('.count_one').removeClass('tab_active_round');
    $('.tab_head_two').addClass('tab_active_head');
    $('.count_two').addClass('tab_active_round');
    $('.tab_head_three').removeClass('tab_active_head');
    $('.count_three').removeClass('tab_active_round');

    $('.check_tab_bill').hide();
    $('.check_tab_ship').show();
    $('.check_tab_order').hide();
  // }
})

$('.ship_next').on('click',function(){
	if($('#shipping_charge').val()!==''){
	    $('.tab_head_one').removeClass('tab_active_head');
	    $('.count_one').removeClass('tab_active_round');
	    $('.tab_head_two').removeClass('tab_active_head');
	    $('.count_two').removeClass('tab_active_round');
	    $('.tab_head_three').addClass('tab_active_head');
	    $('.count_three').addClass('tab_active_round');

	    $('.check_tab_bill').hide();
	    $('.check_tab_ship').hide();
	    $('.check_tab_order').show();
  	} 
  	else {
    	$(".ship_method_alert").html('');
		$(".ship_method_alert").html('<p class="alert alert-danger alert_pad">Please Select Shipping Method</p>');
		// setTimeout(function(){$(".errormsg1").hide();}, 3000);
  		return false;
  }
}) 

$('.bills').on('click',function(){
  // if ($('.tabform').parsley().validate()) {
    $('.tab_head_one').addClass('tab_active_head');
    $('.count_one').addClass('tab_active_round');
    $('.tab_head_two').removeClass('tab_active_head');
    $('.count_two').removeClass('tab_active_round');
    $('.tab_head_three').removeClass('tab_active_head');
    $('.count_three').removeClass('tab_active_round');

    $('.check_tab_bill').show();
    $('.check_tab_ship').hide();
    $('.check_tab_order').hide();
  // }
})  
$(document).ready(function(){
	$('.address_class').on('click',function(){
		var address_id = $(this).val();
		var data = {address_id:address_id};
		var url = base_url+'Account/change_default';
		var result = post_ajax(url, data);
		if(result=='success'){
			window.location.reload();
		} else {
			$('#address_error').html('Some error occured in set default address');
		}
	})
})

$('#new_address').on('click',function(){
	if ($('#address_new').parsley().validate()) {
		var data = $('#address_new').serialize();
		var url = base_url+'Account/add_new_default';
		var result = post_ajax(url, data);
		if(result=='success'){
			$('#address_error').html('New Address added Successfully');
			setTimeout(function(){
				window.location.reload();
			}, 3000);
			
		} else {
			$('#address_error').html('Some error occured in set default address');
		}
	}

})

$('.state_id').on('change',function(){
	var state_id = $(this).val();
	if(state_id!="-1"){
		//alert("hi");
		var dataString = 'loadType='+ city +'&loadId='+ state_id;
	
	$.ajax({
		type: "POST",
		  url: base_url+"Account/loadData",
		data: dataString,
		cache: false,
		success: function(result){
		
			
			$("#addr_city_dropdown").html("<option value='-1'>Select City</option>");  
			$("#addr_city_dropdown").append(result);  
		},
		error: function() {
			$('#address_error').html('Some error occured');
		}
	});
			
	} else {
		$("#addr_city_dropdown").html("<option value='-1'>Select city</option>");
	}
})

$('.search_icon').on('click',function(){
	var search = $('#search_item').val();

	$.ajax({
		type: "POST",
		url: base_url+"ajax/search_item",
		data: {'search':search},
		cache: false,
		success: function(result){
			console.log(result);
			var obj = JSON.parse(result);
			var length = Object.keys(obj).length;
			//return false;
			if(length>0){
				window.location.href = base_url+obj.prod_display;
			} else {
				//window.location.href = base_url+'product/'+search;
			}
		}
	});
})

$('#reset_btn').on('click',function(){
	
	var password = $('#password').val();
	var conf_password = $('#conf_password').val();
	//alert(password+conf_password)
	var reset_key = $('#reset_key').val();
	if(password==conf_password){
		$.ajax({
		type: "POST",
		url: base_url+"home/reset_password",
		data: 'password='+password+'&reset_key='+reset_key,
		cache: false,
		success: function(result){
			var obj = JSON.parse(result);
			if(obj.status=='success'){
				// $('#error_password').html('Password Updated Successfully');
				 $('#error_password').html('<p class="alert alert-success">Password Updated Successfully</p>');
			} else {
				// $('#error_password').html(obj.message);
				$('#error_password').html('<p class="alert alert-danger">'+obj.message+'</p>');
			}
		}
	});
	} else {
		// alert('asdasdasd')
		$('#error_password').html('<p class="alert alert-danger">Password mismatch. Please input correctly</p>');
	}
})

$('.edit_cart').on('click',function(){
	var id = $(this).data('id');
	$('#qty_label'+id).css('display','none');
	$('#qty_select'+id).css('display','block');
	$('#quantity_div_'+id).css('display','block');
	$('#quantity_dis'+id).css('display','none');
})

$('.qty').on('change',function(){
	
	var id = $(this).data('id');
	var qty = $(this).val();
	var price = $(this).data('parent');
	var total = price * qty;
	$('#price_tag'+id).html('&#8377;'+total);
	$('#quantity_dis'+id).html('<p>'+qty+'</p>');

})

$('.save_cart').on('click',function(){
	var id = $(this).data('id');
	var qty = $('#qty_'+id).val();
	$('#qty_label'+id).css('display','block');
	$('#qty_select'+id).css('display','none');
	$('#quantity_div_'+id).css('display','none');
	$('#quantity_dis'+id).css('display','block');

	$.ajax({
		type: "POST",
		url: base_url+"ajax/update_cart",
		data: {'id':id,'qty':qty},
		cache: false,
		success: function(result){
			
		}
	});

})

$('#select_new').on('click',function(){
	alert('Select Delivery Address');
})

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	