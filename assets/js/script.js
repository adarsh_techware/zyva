function selectCity(a) {
    if ("-1" != a) {
        loadDatadoctor("city", a);
        $("#city_dropdown").html("<option value='-1'>Select city</option>");
    }
}

function loadDatadoctor(a, b) {
    var c = "loadType=" + a + "&loadId=" + b;
    $.ajax({
        type: "POST",
        url: base_url + "Account/loadData",
        data: c,
        cache: false,
        success: function(b) {
            $("#" + a + "_dropdown").html("<option value='-1'>Select " + a + "</option>");
            $("#" + a + "_dropdown").append(b);
        },
        error: function() {
            alert("error");
        }
    });
}

$("#sene_mails").on("click", function(a) {
    if ($("#contact_form").parsley().validate()) {
        var b = $("#contact_form").serialize();
        var c = {
            value: b
        };
        var d = base_url + "About/send_mail";
        var e = post_ajax(d, c);
        obj = JSON.parse(e);
        console.log(obj.status);
        if ("success" == obj.status) {
            $(".contact_msg").show();
            $(".contact_msg").html("");
            $(".contact_msg").html('<p class="">Your Message Sent Sucessfully</p>');
            $("#contact_form")[0].reset();
            setTimeout(function() {
                $(".contact_msg").hide();
            }, 5000);
        } else {
            $(".contact_msg").show();
            $(".contact_msg").html("");
            $(".contact_msg").html('<p class="">Error Occuring In Sending Message</p>');
            setTimeout(function() {
                $(".contact_msg").hide();
            }, 5000);
        }
    }
});

$("#stitches").on("click", function(a) {
    cart = $("#cart").serialize();
    stitch = $("#stitching_cat").serialize();
    var b = {
        value: cart,
        stitch: stitch
    };
    var c = base_url + "cart/addtotemp";
    var d = post_ajax(c, b);
    obj = JSON.parse(d);
    if ("success" == obj.status) {
        $(".errormsg").html("");
        $(".errormsg").html('<p class="alert-success alert_pad">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg").hide();
            location.reload();
        }, 5000);
        $("#stichingModal").modal("toggle");
    } else {
        $(".errormsg").html("");
        $(".errormsg").html('<p class="alert-danger alert_pad">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg").hide();
        }, 5000);
    }
    return false;
    if ("" !== $("#front_id").val() || "" !== $("#back_id").val() || "" !== $("#sleeve_id").val() || "" !== $("#hemline_id").val()) {
        var e = $("#cart").serialize();
        var b = {
            value: e
        };
        var c = base_url + "cart/addtotemp";
        var d = post_ajax(c, b);
        obj = JSON.parse(d);
        console.log(obj.status);
        if ("success" == obj.status) {
            $(".errormsg").html("");
            $(".errormsg").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".errormsg").hide();
            }, 5000);
            $("#stichingModal").modal("hide");
            $("#order_template").html(obj.template);
            stich_add();
        } else {
            $(".errormsg").html("");
            $(".errormsg").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".errormsg").hide();
            }, 5000);
        }
    } else $(".front_styles").html("Select stitching. One basic measurement mandatory");
    a.preventDefault();
});

function stich_add() {
    $("#add_cart").on("click", function(a) {
        var b = $("#cart").serialize();
        var c = {
            value: b
        };
        var d = base_url + "cart/addtocart";
        var e = post_ajax(d, c);
        obj = JSON.parse(e);
        console.log(obj.status);
        if ("success" == obj.status) {
            $(".errormsg3").html("");
            $(".errormsg3").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".errormsg3").hide();
            }, 5000);
        } else {
            $(".errormsg3").html("");
            $(".errormsg3").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".errormsg3").hide();
            }, 5000);
        }
    });
}

function get_size_list(a, b) {
    var c = {
        product_id: a,
        color_id: b
    };
    var d = base_url + "ajax/size_list";
    $.ajax({
        type: "POST",
        url: d,
        data: c,
        success: function(a) {
            $(".size_list").html(a);
            e();
        }
    });
    function e() {
        $(".size_new_opt").on("click", function() {
            $("#id").attr("disabled", "disabled");
            $(".add_bag").removeAttr("disabled");
            $("#chkPassport").prop("checked", false);
            $("#chkPassport").attr("checked", false);
            $(".sizes").removeClass("act_size");
            $(this).toggleClass("act_size");
            var a = $(this).data("id");
            $("#size_id").val(a);
            $("#addtocart1").show();
            $("#addtocart").hide();
            var c = {
                product_id: $('#product_id').val(),
                color_id: $('#color_id').val(),
                size_id:a
            };
            var d = base_url + "cart/get_quntity";
            var e = post_ajax(d, c);
            $('#quantity').html(e);
        });
    }
}


// **************************ADD TO BAG WITHOUT CUSTOM SIZE. START
$("#addtocart1").on("click", function(a) {
    a.preventDefault();
    if ("" != $("#size_id").val() && "" != $("#color_id").val()) {
        var b = $("#cart").serialize();
        var c = {
            value: b
        };
        var d = base_url + "cart/addtocart1";
        var e = post_ajax(d, c);
        obj = JSON.parse(e);
        console.log(obj.status);
        if ("success" == obj.status) {
            $(".add_cart_success").html("");
            $(".add_cart_success").html('<p class="alert-success alert_pad">' + obj.message + "</p>");
            setTimeout(function() {
                $(".add_cart_success").hide();
            }, 5000);
            location.reload();
        } else {
            $(".add_cart_success").html("");
            $(".add_cart_success").html('<p class="alert-danger alert_pad">' + obj.message + "</p>");
            setTimeout(function() {
                $(".add_cart_success").hide();
            }, 5000);
        }
    } else $(".front_style").html('<p class="alert-danger alert_pad">Please Select Color and Size</p>');
});
// **************************ADD TO BAG WITHOUT CUSTOM SIZE END


$("#enquires").on("click", function(a) {
    a.preventDefault();
    if ($("#enquiry1").parsley().validate()) {
        var b = $("#enquiry1").serialize();
        var c = {
            value: b
        };
        var d = base_url + "Stitching/addtoenquiry";
        var e = post_ajax(d, c);
        obj = JSON.parse(e);
        console.log(obj.status);
        if ("success" == obj.status) {
            $(".enq_status").show();
            $(".enq_status").html("");
            $(".enq_status").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $("#enqireModal").modal("hide");
            }, 5000);
            location.reload();
        } else {
            $(".enq_status").show();
            $(".enq_status").html("");
            $(".enq_status").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".enq_status").hide();
            }, 5000);
        }
    }
});

$("#updatetowishlist").on("click", function(a) {
    a.preventDefault();
    var b = $("#cart").serialize();
    var c = {
        value: b
    };
    var d = base_url + "wishlist/updatetowishlist";
    var e = post_ajax(d, c);
    obj = JSON.parse(e);
    console.log(obj.status);
    if ("success" == obj.status) {
        $(".errormsg").html("");
        $(".errormsg").html('<p class="error">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg").hide();
        }, 5000);
    } else {
        $(".errormsg").html("");
        $(".errormsg").html('<p class="error">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg").hide();
        }, 5000);
    }
});

$("#addtowishlist").on("click", function(a) {
    a.preventDefault();
    if ("" != $("#size_id").val()) {
        var b = $("#cart").serialize();
        var c = {
            value: b
        };
        var d = base_url + "wishlist/addtowishlist";
        var e = post_ajax(d, c);
        obj = JSON.parse(e);
        console.log(obj.status);
        if ("success" == obj.status) {
            $(".errormsg").show();
            $(".errormsg").html("");
            $(".errormsg").html('<p class="alert-success alert_pad">' + obj.message + "</p>");
            setTimeout(function() {
                $(".errormsg").hide();
            }, 5000);
        } else {
            $(".errormsg").show();
            $(".errormsg").html("");
            $(".errormsg").html('<p class="alert-danger alert_pad">' + obj.message + "</p>");
            setTimeout(function() {
                $(".errormsg").hide();
            }, 5000);
        }
    } else $(".front_style").html("select size and color");
});

$(".wishlisttocart").on("click", function(a) {
    a.preventDefault();
    var b = $(this).data("id");
    var c = {
        value: b
    };
    var d = base_url + "wishlist/addtocart";
    var e = post_ajax(d, c);
    obj = JSON.parse(e);
    console.log(obj.status);
    if ("success" == obj.status) {
        $(".errormsg1").html("");
        $(".errormsg1").html('<p class="error">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg1").hide();
        }, 5000);
        location.reload();
    } else {
        $(".errormsg1").html("");
        $(".errormsg1").html('<p class="error">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg1").hide();
        }, 5000);
    }
});

$("#addall").on("click", function(a) {
    a.preventDefault();
    var b = base_url + "Wishlist/countwishlist";
    var c = post_ajax(b);
    obj = JSON.parse(c);
    if ("0" == obj.count) {
        $(".errormsg2").html("Your Wishlist is empty");
        setTimeout(function() {
            $(".errormsg2").hide();
        }, 5000);
    } else addall();
});

function addall() {
    var a = $("#cart").serialize();
    var b = base_url + "cart/addalltocart";
    var c = post_ajax(b, a);
    obj = JSON.parse(c);
    console.log(obj.status);
    if ("success" == obj.status) {
        $(".errormsg1").html("");
        $(".errormsg1").html('<p class="error">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg1").hide();
        }, 5000);
        location.reload();
    } else if ("exist" == obj.status) {
        $(".errormsg1").html("");
        $(".errormsg1").html('<p class="error">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg1").hide();
        }, 5000);
        location.reload();
    } else {
        $(".errormsg1").html("");
        $(".errormsg1").html('<p class="error">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg1").hide();
        }, 5000);
        location.reload();
    }
}

$("#addtocheckout").on("click", function(a) {
    window.location.href = base_url + "checkout";
    a.preventDefault();
});

$("#checkout").on("click", function(a) {
    var b = base_url + "Checkout/countcart";
    var c = post_ajax(b);
    obj = JSON.parse(c);
    if ("0" == obj.count) {
        $(".errormsg2").html("Your cart is empty");
        setTimeout(function() {
            $(".errormsg2").hide();
        }, 5000);
    } else window.location.href = base_url + "Checkout/";
});

$(".remove_pdt").on("click", function(a) {
    a.preventDefault();
    var b = $(this).data("id");
    var c = {
        id: b
    };
    var d = base_url + "cart/remove_cart";
    var e = post_ajax(d, c);
    obj = JSON.parse(e);
    console.log(obj.status);
    if ("success" == obj.status) if (0 == obj.count) location.reload(); else location.reload();
});

$(".remove_btn_order").on("click", function(a) {
    var b = confirm("Are you sure that you want to delete this item?");
    if (false == b) return false; else {
        var c = $(this).data("id");
        var d = "id=" + c;
        var e = base_url + "Myorder/remove_order";
        var f = post_ajax(e, d);
        obj = JSON.parse(f);
        console.log(obj.status);
        if ("success" == obj.status) location.reload();
    }
});

$(".remove_btn").on("click", function(a) {
    var b = confirm("Are you sure that you want to delete this item?");
    if (false == b) return false; else {
        a.preventDefault();
        var c = $("#cart").serialize();
        var d = $(this).data("id");
        var e = {
            id: d
        };
        var f = base_url + "Wishlist/removewishlist";
        var g = post_ajax(f, e);
        obj = JSON.parse(g);
        console.log(obj.status);
        if ("success" == obj.status) window.location.reload(); else $("#error_message").html(obj.message);
    }
});

$(".remove1").on("click", function(a) {
    a.preventDefault();
    var b = $("#order").serialize();
    var c = $(this).data("id");
    var d = {
        id: c
    };
    var e = base_url + "cart/remove_cart";
    var f = post_ajax(e, d);
    obj = JSON.parse(f);
    console.log(obj.status);
    if ("success" == obj.status) if (0 == obj.count) window.location.href = base_url + "Women/index/women"; else location.reload();
});

$("#continue").on("click", function(a) {
    a.preventDefault();
    window.location.href = base_url;
});

$("#update").on("click", function(a) {
    a.preventDefault();
    window.location.href = base_url + "Women/index/Women";
});

$("#pendingorder").on("click", function(a) {
    a.preventDefault();
    window.location.href = "Checkout/";
});

$(".save").on("click", function(a) {
    a.preventDefault();
    if ($(".measurement").parsley().validate()) {
        var b = $(".measurement").serialize();
        var c = {
            value: b
        };
        var d = base_url + "Measurement/add_measurement";
        var e = post_ajax(d, c);
        obj = JSON.parse(e);
        console.log(obj.status);
        if ("success" == obj.status) {
            $(".measure_status").show();
            $(".measure_status").html("");
            $(".measure_status").html('<p class="alert-success alert_pad">' + obj.message + "</p>");
            setTimeout(function() {
                $(".measure_status").hide();
            }, 5000);
        } else {
            $(".measure_status").show();
            $(".measure_status").html("");
            $(".measure_status").html('<p class="alert-danger alert_pad">' + obj.message + "</p>");
            setTimeout(function() {
                $(".errormsgs").hide();
            }, 5000);
        }
    }
});

$("#checkbox-3").on("click", function(a) {
    var b = $("#checkbox-3").val();
    $("#check1").val(b);
});

$("#checkbox-4").on("click", function(a) {
    var b = $("#checkbox-4").val();
    $("#check1").val(b);
});

$("#checkbox-5").on("click", function(a) {
    var b = $("#checkbox-5").val();
    $("#check2").val(b);
});

$("#pay").on("click", function(a) {
    if ($("#checkbox-7").is(":checked")) {
        var b = $("#payment_sec").serialize();
        var c = base_url + "cart/payment";
        var d = post_ajax(c, b);
        obj = JSON.parse(d);
        console.log(obj.status);
        if ("success" == obj.status) window.location.href = base_url + "payment/pgRedirect/" + obj.booked_id; else {
            $(".errormsg1").html("");
            $(".errormsg1").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".errormsg1").hide();
            }, 5000);
        }
    } else {
        $(".place_order_alert").html("");
        $(".place_order_alert").html('<p class="alert alert-danger alert_pad">Please Select Place Order</p>');
        return false;
    }
});



$("#search").on("click", function(a) {
    a.preventDefault();
    var b = $(".search_dress").val();
    var c = {
        value: b
    };
    var d = base_url + "Women/search";
    var e = post_ajax(d, c);
    obj = JSON.parse(e);
    if (obj) $("#search_content").html(obj.template); else {
        $(".errormsg1").html("");
        $(".errormsg1").html('<p class="error">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg1").hide();
        }, 5000);
    }
});



$(".save1").on("click", function(a) {
    if ($(".measurement").parsley().validate()) {
        var b = $(".measurement").serialize();
        var c = {
            value: b
        };
        var d = base_url + "Measurement/add_newmeasurement";
        var e = post_ajax(d, c);
        obj = JSON.parse(e);
        if ("success" == obj.status) {
            $("#stitch_id").val(obj.stitch_id);
            $("#measurement").modal("hide");
        } else {
            $(".errormsgs").html("");
            $(".errormsgs").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".errormsgs").html("");
            }, 5000);
        }
    }
});



$(".move").on("click", function(a) {
    var b = $(this).data("id");
    var c = {
        id: b
    };
    var d = base_url + "Wishlist/move";
    var e = post_ajax(d, c);
    obj = JSON.parse(e);
    if ("success" == obj.status) {
        $(".errormsgs").html("");
        $(".errormsgs").html('<p class="error">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsgs").hide();
        }, 5000);
        location.reload();
    } else {
        $(".errormsgs").html("");
        $(".errormsgs").html('<p class="error">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsgs").hide();
        }, 5000);
    }
});

$("#can").click(function() {
    $("#addressbook").trigger("reset");
});

$("#can1").click(function() {
    $("#accountdetails").trigger("reset");
});

function post_ajax(a, b) {
    var c = "";
    $.ajax({
        type: "POST",
        url: a,
        data: b,
        success: function(a) {
            c = a;
        },
        error: function(a) {
            c = "error";
        },
        async: false
    });
    return c;
}


$(".chg_pwd").on("click", function(a) {
    var b = $("#accountdetails").serialize();
    var c = {
        value: b
    };
    var d = base_url + "Account/edit_account";
    var e = post_ajax(d, c);
    obj = JSON.parse(e);
    console.log(obj.status);
    if ("error" == obj.status) {
        $(".errormsg").show();
        $(".errormsg").html("");
        $(".errormsg").html('<p class="alert alert-danger">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg").hide();
        }, 5000);
    } else if ("cpasswordmissmatch" == obj.status) {
        $(".errormsg").show();
        $(".errormsg").html("");
        $(".errormsg").html('<p class="alert alert-danger">' + obj.message + "</p>");
    } else if ("passwordmismatch" == obj.status) {
        $(".errormsg").show();
        $(".errormsg").html("");
        $(".errormsg").html('<p class="alert alert-danger">' + obj.message + "</p>");
    } else {
        $(".errormsg").show();
        $(".errormsg").html("");
        $(".errormsg").html('<p class="alert alert-success">' + obj.message + "</p>");
        setTimeout(function() {
            $(".errormsg").hide();
        }, 5000);
    }
    a.preventDefault();
});



$("#edit1").on("click", function(a) {
    if ($("#addressbook").parsley().isValid()) {
        var b = $("#addressbook").serialize();
        var c = {
            value: b
        };
        var d = base_url + "Account/add_addressbook";
        var e = post_ajax(d, c);
        obj = JSON.parse(e);
        if ("success" == obj.status) {
            $(".errormsg3").html("");
            $(".errormsg3").html('<p class="alert alert-success">' + obj.message + "</p>");
            $("#addressbook")[0].reset();
            setTimeout(function() {
                $(".errormsg3").hide();
            }, 5000);
            location.reload();
        } else if ("error" == obj.status) {
            $(".errormsg3").html("");
            $(".errormsg3").html('<p class="alert alert-danger">' + obj.message + "</p>");
            setTimeout(function() {
                $(".errormsg1").hide();
            }, 5000);
        }
        return false;
    }
});



$(".check_tab_bill").show();

$(".check_tab_ship").hide();

$(".check_tab_order").hide();

$(".tab_head_one").addClass("tab_active_head");

$(".count_one").addClass("tab_active_round");

$(".bill_next").on("click", function() {
    $(".tab_head_one").removeClass("tab_active_head");
    $(".count_one").removeClass("tab_active_round");
    $(".tab_head_two").addClass("tab_active_head");
    $(".count_two").addClass("tab_active_round");
    $(".tab_head_three").removeClass("tab_active_head");
    $(".count_three").removeClass("tab_active_round");
    $(".check_tab_bill").hide();
    $(".check_tab_ship").show();
    $(".check_tab_order").hide();
});



$(".ship_next").on("click", function() {
    if ("" !== $("#shipping_charge").val()) {
        $(".tab_head_one").removeClass("tab_active_head");
        $(".count_one").removeClass("tab_active_round");
        $(".tab_head_two").removeClass("tab_active_head");
        $(".count_two").removeClass("tab_active_round");
        $(".tab_head_three").addClass("tab_active_head");
        $(".count_three").addClass("tab_active_round");
        $(".check_tab_bill").hide();
        $(".check_tab_ship").hide();
        $(".check_tab_order").show();
    } else {
        $(".ship_method_alert").html("");
        $(".ship_method_alert").html('<p class="alert alert-danger alert_pad">Please Select Shipping Method</p>');
        return false;
    }
});



$(".bills").on("click", function() {
    $(".tab_head_one").addClass("tab_active_head");
    $(".count_one").addClass("tab_active_round");
    $(".tab_head_two").removeClass("tab_active_head");
    $(".count_two").removeClass("tab_active_round");
    $(".tab_head_three").removeClass("tab_active_head");
    $(".count_three").removeClass("tab_active_round");
    $(".check_tab_bill").show();
    $(".check_tab_ship").hide();
    $(".check_tab_order").hide();
});



$(document).ready(function() {
    $(".address_class").on("click", function() {
        var a = $(this).val();
        var b = {
            address_id: a
        };
        var c = base_url + "Account/change_default";
        var d = post_ajax(c, b);
        if ("success" == d) window.location.reload(); else $("#address_error").html("Some error occured in set default address");
    });
});



$("#new_address").on("click", function() {
    if ($("#address_new").parsley().validate()) {
        var a = $("#address_new").serialize();
        var b = base_url + "Account/add_new_default";
        var c = post_ajax(b, a);
        if ("success" == c) {
            $("#address_error").html("New Address added Successfully");
            setTimeout(function() {
                window.location.reload();
            }, 5000);
        } else $("#address_error").html("Some error occured in set default address");
    }
});



$(".state_id").on("change", function() {
    var a = $(this).val();
    if ("-1" != a) {
        var b = "loadType=" + city + "&loadId=" + a;
        $.ajax({
            type: "POST",
            url: base_url + "Account/loadData",
            data: b,
            cache: false,
            success: function(a) {
                $("#addr_city_dropdown").html("<option value='-1'>Select City</option>");
                $("#addr_city_dropdown").append(a);
            },
            error: function() {
                $("#address_error").html("Some error occured");
            }
        });
    } else $("#addr_city_dropdown").html("<option value='-1'>Select city</option>");
});



$(".search_icon").on("click", function() {
    var a = $("#search_item").val();
    $.ajax({
        type: "POST",
        url: base_url + "ajax/search_item",
        data: {
            search: a
        },
        cache: false,
        success: function(a) {
            console.log(a);
            var b = JSON.parse(a);
            var c = Object.keys(b).length;
            if (c > 0) window.location.href = base_url + b.prod_display; else ;
        }
    });
});



$("#reset_btn").on("click", function() {
    var a = $("#password").val();
    var b = $("#conf_password").val();
    var c = $("#reset_key").val();
    if (a == b) $.ajax({
        type: "POST",
        url: base_url + "home/reset_password",
        data: "password=" + a + "&reset_key=" + c,
        cache: false,
        success: function(a) {
            var b = JSON.parse(a);
            if ("success" == b.status) $("#error_password").html('<p class="alert alert-success">Password Updated Successfully</p>'); else $("#error_password").html('<p class="alert alert-danger">' + b.message + "</p>");
        }
    }); else $("#error_password").html('<p class="alert alert-danger">Password mismatch. Please input correctly</p>');
});



$(".edit_cart").on("click", function() {
    var a = $(this).data("id");
    $("#qty_label" + a).css("display", "none");
    $("#qty_select" + a).css("display", "block");
    $("#quantity_div_" + a).css("display", "block");
    $("#quantity_dis" + a).css("display", "none");
});



$(".qty").on("change", function() {
    var a = $(this).data("id");
    var b = $(this).val();
    var c = $(this).data("parent");
    var d = c * b;
    $("#price_tag" + a).html("&#8377;" + d);
    $("#quantity_dis" + a).html("<p>" + b + "</p>");
});

$(".save_cart").on("click", function() {
    var a = $(this).data("id");
    var b = $("#qty_" + a).val();
    $("#qty_label" + a).css("display", "block");
    $("#qty_select" + a).css("display", "none");
    $("#quantity_div_" + a).css("display", "none");
    $("#quantity_dis" + a).css("display", "block");
    $.ajax({
        type: "POST",
        url: base_url + "ajax/update_cart",
        data: {
            id: a,
            qty: b
        },
        cache: false,
        success: function(a) {}
    });
});

$("#select_new").on("click", function() {
    alert("Select Delivery Address");
});

