$('.carousel[data-type="multi"] .item').each(function() {
    var a = $(this).next();
    if (!a.length) a = $(this).siblings(":first");
    a.children(":first-child").clone().appendTo($(this));
    for (var b = 0; b < 4; b++) {
        a = a.next();
        if (!a.length) a = $(this).siblings(":first");
        a.children(":first-child").clone().appendTo($(this));
    }
});

function post_ajax(a, b) {
    var c = "";
    $.ajax({
        type: "POST",
        url: a,
        data: b,
        success: function(a) {
            c = a;
        },
        error: function(a) {
            c = "error";
        },
        async: false
    });
    return c;
}

function change_range() {
    $(".result_div").html("");
    var a = $("#filter_search").val();
    var b = $("#cat_id").val();
    var c = $("#size_id").val();
    var d = $("#sub_id").val();
    var e = $("#min_range").val();
    var f = $("#max_range").val();
    var g = "cat_id=" + b + "&order_by=" + a + "&size_id=" + c + "&sub_id=" + d + "&min_range=" + e + "&max_range=" + f;
    
    $.ajax({
        type: "POST",
        url: base_url + "ajax/filter_product",
        data: g,
        success: function(a) {
            var b = JSON.parse(a);
            $("#port").html(b.data);
            $("#last_limit").val(b.last_limit);
            $("#total_products").val(b.total_products);
            if (0 == b.total_products) $(".result_div").html("<p>No Result Found...</p>");
            processing = false;
        },
        error: function(a) {
            result = "error";
        },
        async: false
    });
}

$("#filter_search").on("change", function() {
    $(".result_div").html("");
    var a = $(this).val();
    var b = $("#cat_id").val();
    var c = $("#size_id").val();
    var d = $("#sub_id").val();
    var e = $("#min_range").val();
    var f = $("#max_range").val();
    var g = "cat_id=" + b + "&order_by=" + a + "&size_id=" + c + "&sub_id=" + d + "&min_range=" + e + "&max_range=" + f;
    // alert(a);alert(b);alert(c);alert(d);alert(e);alert(f);alert(g);
    $.ajax({
        type: "POST",
        url: base_url + "ajax/filter_product",
        data: g,
        success: function(a) {
            var b = JSON.parse(a);
            $("#port").html(b.data);
            $("#last_limit").val(b.last_limit);
            $("#total_products").val(b.total_products);
            if (0 == b.total_products) $(".result_div").html("<p>No Result Found...</p>");
            processing = false;
        },
        error: function(a) {
            result = "error";
        },
        async: false
    });
});

$(".size_opt").on("click", function() {
    $(".result_div").html("");
    var a = $(this).data("id");
    $("#size_id").val(a);
    var b = $("#filter_search").val();
    var c = $("#cat_id").val();
    var d = $("#sub_id").val();
    var e = $("#min_range").val();
    var f = $("#max_range").val();
    $(".sizes").removeClass("act_size");
    $(this).toggleClass("act_size");
    var g = "cat_id=" + c + "&order_by=" + b + "&size_id=" + a + "&sub_id=" + d + "&min_range=" + e + "&max_range=" + f;
    // alert(a);alert(b);alert(c);alert(d);alert(e);alert(f);alert(g);
    $.ajax({
        type: "POST",
        url: base_url + "ajax/filter_product",
        data: g,
        success: function(a) {
            var b = JSON.parse(a);
            $("#port").html(b.data);
            $("#last_limit").val(b.last_limit);
            $("#total_products").val(b.total_products);
            if (0 == b.total_products) $(".result_div").html("<p>No Result Found....</p>");
            processing = false;
        },
        error: function(a) {
            result = "error";
        },
        async: false
    });
});

//******************** SIGNUP FROM SIGNUP PAGE START
$("#signup_button").on("click", function(a) {
    if ($("#signup_form").parsley().validate()) {
        // $("#signup_button").hide();
        var b = $("#signup_form").serialize();
        var c = {
            value: b
        };
        var d = base_url + "Home/sign_up";
        var e = post_ajax(d, c);
        console.log(e.status);
        obj = JSON.parse(e);
        console.log(obj.status);
        if ("password" == obj.status) {
            $(".signup_error").show();
            $(".signup_error").html("");
            $(".signup_error").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".signup_error").hide();
            }, 5000);
            $("#signup_error").show();
        } else if ("email" == obj.status) {
            $(".signup_error").show();
            $(".signup_error").html("");
            $(".signup_error").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".signup_error").hide();
            }, 5000);
            $("#signup_button").show();
        } else if ("success" == obj.status) {
            // $("#myModal_reg").modal("hide");
            // $("#myModal").modal("show");
            // $("body").css("overflow-x", "hidden");
            // $(".home_bg").addClass("no_index");

            $(".error_div").html("");
            $(".error_div").html('<p class="error">' + obj.result + "</p>");
            setTimeout(function() {
                $(".error_div").hide();
            }, 9000);
            window.location.href = base_url + "home/signin";
        }
    }
    a.preventDefault();
});
// ***********************************************

//******************** LOGIN FROM LOGIN PAGE START
$("#login_button").on("click", function(a) {
    a.preventDefault();
    if ($("#login_form").parsley().validate()) {
        var b = $("#login_form").serialize();
        var c = {
            value: b
        };
        var d = base_url + "Home/login";
        var e = post_ajax(d, c);
        obj = JSON.parse(e);
        console.log(obj.status);
        if ("incorrect" == obj.status) {
            $(".error_div").show();
            $(".error_div").html("");
            $(".error_div").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".error_div").hide();
            }, 5000);
        } else if ("success" == obj.status) {
            var referrer =  document.referrer;
            var pathname = window.location.pathname;
            if(referrer!=pathname && referrer!=''){
                
               /* if (window.location.href.indexOf("?added-to-cart=555") > -1) {
                alert("found it");
                }*/

                if (referrer.match("https://zyva.in/").length > 0) {
                    window.location.href = referrer;
                } 
                else {
                   window.location.href = base_url; 
                }
                
                /*if(window.location.href.indexOf("https://zyva.in/") > -1) {
                }*/
            } 
            else {
                window.location.href = base_url;
            }           
            
        }
    }
});
// **********************************************************

//******************** FORGOT PASSWORD FROM FORGOT PASSWORD PAGE START
$("#land_logforget").on("click", function(a) {
    $(".view_loader").append('<div class="loader"></div>');
    $(".log_fogot").hide();
    $(".renew_pass").html("");
    setTimeout(function() {
        var a = $("#femail").val();
        if ("" == a) {
            $(".renew_pass").html('<p class="error">Please enter your Email Id</p>');
            $(".view_loader").html("");
            $(".log_fogot").show();
            return false;
        } else $(".renew_pass").html("");
        var b = base_url + "Home/Forget_Password";
        var c = {
            email: $("#femail").val()
        };
        var d = post_ajax(b, c);
        obj = JSON.parse(d);
        console.log(obj.status);
        if ("No" == obj.status) {
            $(".renew_pass").show("");
            $(".renew_pass").html("");
            $(".renew_pass").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".renew_pass").hide();
            }, 5000);
        } else if ("loggedIn" == obj.status) {
            $(".log_fogot").hide();
            $(".renew_pass").html('<p class="error">' + obj.message + "</p>");
            setTimeout(function() {
                $(".renew_pass").hide();
                $(".view_loader").html("");
            }, 5000);
        }
    }, 5000);
});
// *********************************************************
// ******************************* ADD TO WISHLIST FROM HOVER
$(".add_wish").on("click", function() {
    var a = $(this).attr("id");
    var b = {
        prod_id: a
    };
    var c = base_url + "wishlist/add_wish";
    var d = post_ajax(c, b);
    obj = JSON.parse(d);
    console.log(obj.status);
    if ("success" == obj.status) {
        $(".add_wish_message" + a).show();
        $(".add_wish_message" + a).html("");
        $(".add_wish_message" + a).html('<p class="alert-success alert_pad">' + obj.message + "</p>");
        setTimeout(function() {
            $(".add_wish_message" + a).hide();
        }, 2000);
        location.reload();
    } else {
        $(".add_wish_message" + a).show();
        $(".add_wish_message" + a).html("");
        $(".add_wish_message" + a).html('<p class="alert-danger alert_pad">' + obj.message + "</p>");
        setTimeout(function() {
            $(".add_wish_message" + a).hide();
        }, 2000);
    }
});
// *********************************************************

// ****************************************** CALL LOGIN PAGE
$(".call_signin").on('click',function(){
  window.location.href = base_url+"home/signin";
})
// **********************************************************

// ************************FOR TOGGLE THE ITEMS IN MY BAG IN HEADER PAGE
// var searchBox = $("#search .search-box");

// var searchTrigger = $("#search-trigger");

// searchTrigger.on("click", function(e) {
//     e.preventDefault();
//     e.stopPropagation();
//     searchBox.toggle();
// });

// $(document).click(function(event) {
//     if (!searchBox.is(event.target) && 0 === searchBox.has(event.target).length) {
//         searchBox.hide();
//     };
// });
// **********************************************************************


// ******************************************* ONLY FOR MOBILE VIEW START

// ------------------FOR SORT AND FILTER FOR MOBILE VIEW
$('#sort_button').on('click',function(){
  $('.sort_section').toggle();
  $('.sort_btn').toggleClass('btn_color');
  $('.sort_btn').toggleClass('sort_image');
  
  $('.filter_section').hide();
  $('.filter_btn').removeClass('filter_image');
  $('.filter_btn').removeClass('btn_color');
});
$('#filter_button').on('click',function(){
  $('.filter_section').toggle();
  $('.filter_btn').toggleClass('btn_color');
  $('.filter_btn').toggleClass('filter_image');
  
  $('.sort_section').hide();
  $('.sort_btn').removeClass('sort_image');
  $('.sort_btn').removeClass('btn_color');
});

$('#sort_close').on('click',function(){
    $('.sort_section').hide();
    $('.sort_btn').removeClass('sort_image');
    $('.sort_btn').removeClass('btn_color');
});
$('#filter_close').on('click',function(){
    $('.filter_section').hide();
    $('.filter_btn').removeClass('btn_color');
    $('.filter_btn').removeClass('filter_image');
});
// ---------------------------------------------------

//------------------------------------- FOR TAB IN FILTER FOR MOBILE VIEW
function filter_open(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
// ---------------------------------------------------


// ------------------------------------- FOR PRICE RANGER FOR MOBILE VIEW
var keypressSlider = document.getElementById('mob_slider');
var input1 = document.getElementById('mob_input-with-keypress-1');
var input0 = document.getElementById('mob_input-with-keypress-0');
var inputs = [input0, input1];

noUiSlider.create(keypressSlider, {
  start: [500, 10000],
  connect: true,
   direction: 'ltr',
   tooltips: [true, wNumb({ decimals: 0 })],
  range: {
    'min': [500],
    'max': [10000]
  }
});

keypressSlider.noUiSlider.on('end', function(values, handle){
  //addClassFor(lEnd, 'tShow', 450);
  var array = values.toString().split(",");
  $('#mini_range').val(array[0]);
  $('#maxi_range').val(array[1]);
  change_range();
});

keypressSlider.noUiSlider.on('update', function( values, handle, unencoded) {
  
  inputs[handle].value = values[handle];
});

function setSliderHandle(i, value) {
  var r = [null,null];
  r[i] = value;
  keypressSlider.noUiSlider.set(r);
}

// Listen to keydown events on the input field.
inputs.forEach(function(input, handle) {

  input.addEventListener('change', function(){
    setSliderHandle(handle, this.value);
  });

  input.addEventListener('keydown', function( e ) {

    var values = keypressSlider.noUiSlider.get();
    var value = Number(values[handle]);

    // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
    var steps = keypressSlider.noUiSlider.steps();

    // [down, up]
    var step = steps[handle];

    var position;

    // 13 is enter,
    // 38 is key up,
    // 40 is key down.
    switch ( e.which ) {

      case 13:
        setSliderHandle(handle, this.value);
        break;

      case 38:

        // Get step to go increase slider value (up)
        position = step[1];

        // false = no step is set
        if ( position === false ) {
          position = 1;
        }

        // null = edge of slider
        if ( position !== null ) {
          setSliderHandle(handle, value + position);
        }

        break;

      case 40:

        position = step[0];

        if ( position === false ) {
          position = 1;
        }

        if ( position !== null ) {
          setSliderHandle(handle, value - position);
        }

        break;
    }
  });
});
// --------------------------------------------------------------
// *********************************** ONLY FOR MOBILE VIEW END



