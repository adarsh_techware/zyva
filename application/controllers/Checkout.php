<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Checkout extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Home_model');
        $this->load->model('Checkout_model');
        $this->load->model('Women_model');
        $this->load->model('Cart_model');
        $this->load->model('Account_model');
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
    }
    function index() {
        $template['page'] = 'checkout/checkout';
        $this->Home_model->cart_update();
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        if (count((array)$template['cartlist']) == 0) {
            redirect(base_url());
        }
        $template['data'] = $this->Women_model->userinfo();
        $template['Category'] = $this->Women_model->category();
        //$template['image']       = $this->Women_model->viewwomen();
        //$template['sub_Category'] = $this->Women_model->sub_category_own();
        // $template['size']         = $this->Women_model->getsize();
        $template['address_list'] = $this->Checkout_model->view_address_list();
        $template['address'] = $this->Checkout_model->view_address();
        $template['shipping'] = $this->Checkout_model->view_shipping();
        $template['shipping1'] = $this->Checkout_model->view_shipping1();
        // $template['psize']        = $this->Women_model->viewproductsize($id);
        $template['shipping2'] = $this->Checkout_model->view_shipping2();
        $template['statedetails'] = $this->Account_model->get_state();
        $template['citydetails'] = $this->Account_model->get_city();
        $template['data1'] = $this->Account_model->view_addressbook();
        // echo '<pre>';
        // print_r($template);
        // die();
        $this->load->view('template', $template);
    }
    public function edit() {
        if ($_POST) {
            $data = $_POST;
            $myArray = array();
            parse_str($_POST['value'], $myArray);
            $res = $this->Checkout_model->edit($myArray);
            if ($res == "success") {
                $result = array('status' => 'success', 'message' => 'Sucessfully Updated to Bag');
            } else {
                $result = array('status' => 'error', 'message' => 'Error');
            }
            print json_encode($result);
        }
    }
    function countcart() {
        $res1 = $this->Cart_model->count_cart();
        $result = array('count' => $res1);
        print json_encode($result);
    }
    function success($booked_id = null) {
        if ($booked_id == null) {
            redirect(base_url());
        }
        $template['page'] = 'checkout/pay_success';
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        $template['data'] = $this->Women_model->userinfo();
        $template['Category'] = $this->Women_model->category();
        $template['booked'] = $this->Checkout_model->booked_info($booked_id);
        $template['order'] = $this->Checkout_model->order_info($booked_id);
        
        $count = count($template['booked']);
        if ($count == 0) {
            $template['page'] = 'checkout/pay_failure';
        }
        else {
            //$order_info = ($template['order']);
            //echo"<prev>";print_r($order_info);die;
            $booking_info = $template['booked'];
            $settings = get_icon();

            $template['order_id']       = $booking_info->booking_no;
            $template['total_item']     = $booking_info->total_item;
            $template['total_rate']     = $booking_info->total_rate;
            $template['delivery_name']  = $booking_info->delivery_name;
            $template['address_1']      = $booking_info->address_1;  
            $template['address_2']      = $booking_info->address_2;
            $template['state_id']       = $booking_info->state_id;
            $template['city_id']        = $booking_info->city_id;
            $template['Zip']            = $booking_info->Zip;
            $template['telephone']      = $booking_info->telephone;

            $this->load->library('email');
            $config = Array(
                        'protocol'  => 'smtp', 
                        'smtp_host' => $settings->smtp_host, //'smtp.techlabz.in',
                        'smtp_port' => 587, 
                        'smtp_user' => $settings->smtp_username, //'no-reply@techlabz.in', // change it to yours
                        'smtp_pass' => $settings->smtp_password, //'baAEanx7', // change it to yours
                        'smtp_timeout' => 20, 
                        'mailtype'  => 'html', 
                        'charset'   => 'iso-8859-1', 
                        'wordwrap'  => TRUE
                        );
            $this->email->initialize($config); // add this line
            $subject        = $settings->sms_username.' Order Success';
            $name           = $settings->sms_username;
            $to             = $settings->smtp_username;
            $mailTemplate   = $this->load->view('email_templates/order_success',$template, true);

            //$this->email->set_newline("\r\n");
            $this->email->from($settings->admin_email, $name);
            $this->email->to($to);
            // $this->email->to('nikhila.techware@gmail.com');
            $this->email->subject($subject);
            $this->email->message($mailTemplate);
            $var = $this->email->send();
            if ($var == 1) {
                $result = array('status' => 'success', 'message' => 'Mail sent successfully');
            } else {
                $result = array('status' => 'error', 'message' => '');
            }
        }
        $this->load->view('template', $template);
    }
    function failure($booked_id = null) {
        if ($booked_id == null) {
            redirect(base_url());
        }
        $template['page'] = 'checkout/payment_failure';
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        $template['data'] = $this->Women_model->userinfo();
        $template['Category'] = $this->Women_model->category();
        $template['booked'] = $this->Checkout_model->booked_info($booked_id);
        $template['payment_det'] = $this->Checkout_model->payment_info($booked_id);
        $count = count($template['booked']);
        if ($count == 0) {
            $template['page'] = 'checkout/pay_failure';
        }
        $this->load->view('template', $template);
    }
}
?>