<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Women extends CI_Controller {

 public function __construct() {
 parent::__construct();
 $this->load->helper('general');
 
$this->load->model('Women1_model');
$this->load->model('Women_neck_model');
$this->load->model('Women_model');
$this->load->model('Women_neck_model');
$this->load->model('product_model');
$this->load->model('Search_model');
$this->load->model('measurement_model');

$this->load->model('Home_model');
  $this->load->library('pagination');
 // if(!$this->session->userdata('user_id')) {
 // 			redirect(base_url());
 // 		}
   }
   function index(){
     $name = $this->uri->segment(3);

     if($name==null){
      redirect(base_url());
     }
	

     if(isset($name)){
       $template['catimg']=$this->Women_model->categoryview($name);
	   //echo '<pre>';
	  // print_r($template['catimg']);
	  //die;
     }
     

     $data_rec = $template['catimg'];
	 
	 $template['data_rec']=$data_rec;

    /* $max = 0;
     $min = 1000000;*/
	 $max = 100000;
     $min = 0;
	 

  foreach($data_rec as $k=>$v) {
    if($v->price>$max) {
      $max = $v->price;
    }
    if($v->price<$min){
      $min = $v->price;
    }
  }

  $template['max'] = $max;
  $template['min'] = $min;





	   $config["base_url"] = base_url() . "Women/index/".$name;
        $config["total_rows"] = count($template['catimg']);
        $config["per_page"] = 6;
        $config["uri_segment"] = 4;


        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        
      

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        
        
	 
       
	   
	   if(isset($_POST['sort']))
	   {
      $template['catimg'] = $this->Women1_model->fetch_category_sort($config["per_page"], $page,$name,$_POST['sort']);
      $template['sort'] = $_POST['sort'];
	   } else {
        $template['catimg'] = $this->Women_model->fetch_category($config["per_page"], $page,$name);
        $template['sort'] = 'product';
     }
	
	 
	 
   
   $template['links'] = $this->pagination->create_links();

        

      $template['page'] = 'women/women_page';
      $template['cartlist']=$this->Home_model->cartlist();
      $template['result']=$this->Home_model->shoppingbag();
      $template['data'] = $this->Women_model->userinfo();
      $template['Category'] = $this->Women_model->category();
      $template['sub_Category'] = $this->Women_model->sub_category_own();
      $template['size'] = $this->Women_model->getsize();
      $this->load->view('template',$template);
   }

/// /Price///////
function price_range()
{
	   $config["per_page"] = 6;
	if(isset($_POST))
	   {
		   
		  $p= $_POST;
		 // print_r($p);
	//die;		  
       $template['price'] = $this->Women1_model->fetch_category_price($config["per_page"],$p);
	   }
	   $temp1 = $this->load->view('women/price',$template,true);
       print json_encode(array('template'=>$temp1));   
	 
}
   
  /// /end///////

   function view_category(){

   			$template['page'] = 'women/women_page';
   			$template['page_title'] = "View category";
   			$template['Category'] = $this->Women_model->category();
        $template['sub_category'] = $this->Women_model->sub_category();
        
   			$this->load->view('template',$template);

   }

   function view_subcategory(){
    $id=$_POST['id'];
    $sub_Category = $this->Women_model->sub_category($id);
    print json_encode( $sub_Category );
   }

   function view_sub_cat(){
     $template['page'] = 'women/women_product_detail';
    //  $template['page_title'] = "View ";
     $template['sub_Category1'] = $this->Women_model->view_subcat();
     //var_dump($template['Category']);die;
     // print_r($template['Category']);
     // die;
     $this->load->view('women/women_product_detail',$template);
     $this->load->view('template',$template);

   }
 

   function productdetail(){

     $template['page'] = 'women/women_product_detail';
     $id = $this->uri->segment(3);

      $template['result']=$this->Home_model->shoppingbag();
    $template['Category'] = $this->Women_model->category();
 		 $template['cartlist']=$this->Home_model->cartlist();
    $template['viewcat'] = $this->Women_model->categoryview($id);

     $template['img1'] = $this->Women_model->viewimg1($id);
  
    
     $template['psize'] = $this->Women_model->viewproductsize($id);
     $template['colo'] = $this->Women_model->viewproductcolor($id);
 //var_dump($template['colo']);
 //die;
     $template['data'] = $this->Women_model->userinfo();
	  $template['imgs'] = $this->Women_model->viewsubimg($id);
   $template['images'] = $this->Women_model->viewitem_img();
 
   
    $template['front'] = $this->Women_neck_model->front_neck();
    $template['back'] = $this->Women_neck_model->back_neck();
    $template['sleeve'] = $this->Women_neck_model->sleeve_neck();
      $template['hemline'] = $this->Women_neck_model->hemline_neck();
      $template['adons']=$this->Women_neck_model->viewadons();

       $template['adons_sub']=$this->Women_neck_model->viewsub_adons();
      
  $template['measher'] = $this->measurement_model->view();

  
  
     $this->load->view('template',$template);
   }







   function search(){

     //$template['page'] = 'women/search';
     if($_POST){
     $data=$_POST;
     //var_dump($data);exit;
  
     //  $name = $this->uri->segment(3);

// $config["base_url"] = base_url() . "Women/search/".$data['value'];
//         $config["per_page"] = 3;
//         $config["uri_segment"] = 4;

//         $this->pagination->initialize($config);

//         $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
     

     if(isset($data)){
      $template['search'] = $this->Search_model->getsearch($data);
     }

     
    
 // $config["total_rows"] = count($template['search']);
 //  $template['links'] = $this->pagination->create_links();

      $temp = $this->load->view('women/search',$template,true);
  
     print json_encode(array('template'=>$temp));   
     }
    
   
  }






function size_search(){

     //$template['page'] = 'women/search';
     if($_POST){
     $data=$_POST;
     //var_dump($data);exit;
  
      // $name = $this->uri->segment(3);
      // print_r($name);

// $config["base_url"] = base_url() . "Women/size_search/".$data['value'];
//         $config["per_page"] = 3;
//         $config["uri_segment"] = 4;

//         $this->pagination->initialize($config);

//         $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

     if(isset($data)){
      $template['size_search'] = $this->Women1_model->getsearch_size($data);
     }



     
     
        // $config["total_rows"] = count($template['size_search']);
        

    
        // $template['links'] = $this->pagination->create_links();
 

      $temp = $this->load->view('women/size_search',$template,true);
  
     print json_encode(array('template'=>$temp));   
     }
    
   
  }
  
  
  
  
  
  
  
  
  
  
  






 }
 ?>
