<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Wishlist extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Home_model');
        $this->load->model('Women_model');
        $this->load->model('Wishlist_model');
        $this->load->model('Women_neck_model');
        $this->load->library('pagination');
        // if(!$this->session->userdata('user_id')) {
        //          redirect(base_url());
        //      }
        
    }
    function index() {
        $template['page'] = 'wishlist/my_wishlist';
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        $template['data'] = $this->Women_model->userinfo();
        $template['Category'] = $this->Women_model->category();
        $template['wishlist'] = $this->Wishlist_model->wishlistdetails();
        /// var_dump($template['wishlist']);
        // die;
        $template['sub_Category'] = $this->Women_model->sub_category_own();
        $template['size'] = $this->Women_model->getsize();
        $this->load->view('template', $template);
    }
    function addtowishlist() {
        if ($_POST) {
            $data = $_POST;
            $myArray = array();
            parse_str($_POST['value'], $myArray);
            $res = $this->Wishlist_model->addtowishlist($myArray);
            if ($res == "success") {
                $result = array('status' => 'success', 'message' => 'Sucessfully Added to Wishlist');
            } else {
                $result = array('status' => 'error', 'message' => 'Product Already Exist in Wishlist');
            }
            print json_encode($result);
        }
    }
    function addtocart() {
        if ($_POST) {
            $data = $_POST;
            $myArray = array();
            $myArray = $this->db->where('id', $_POST['value'])->get('wishlist')->row_array();

            //print_r($myArray);
            //die();
            // parse_str($_POST['value'], $myArray);
            $res = $this->Wishlist_model->addtocart($myArray);
            if ($res == "success") {
                $result = array('status' => 'success', 'message' => 'Sucessfully Added to cart');
            } else {
                $result = array('status' => 'error', 'message' => 'Eroor');
            }
            print json_encode($result);
        }
    }
    function removewishlist() {
        $data = $_POST;
        if ($_POST) {
            $data = $_POST;
            $res = $this->Wishlist_model->wishlist_delete($data);
            if ($res == 'success') {
                $result = array('status' => 'success', 'message' => 'Sucessfully Removed from bag');
            } else {
                $result = array('status' => 'error', 'message' => 'Some error found in removing whishlist');
            }
            /*$res1 = $this->Wishlist_model->count_cart();
            // var_dump($res1);
            // die;
            if ($res1 > 1) {
                //redirect(base_url().'Women/');    
                $result = array(
                    'status' => 'success',
                    'message' => 'Sucessfully  Removed from bag',
                    'count' => $res1
                );
            }
            
            else {
            
                if ($res == "success") {
            
                    $result = array(
            
                        'status' => 'success',
            
                        'message' => 'Sucessfully  Removed from bag',
            
                        'count' => $res1
            
                    );  
            
                } else {
                    $result = array(
            
                        'status' => 'error',
            
                        'message' => 'Eroor'
            
                    );
                }
            }*/
            print json_encode($result);
        }
    }
    function editwishlist() {
        $id = $this->uri->segment(3);
        $template['page'] = 'wishlist/edit_wishlist';
        $id = $this->uri->segment(3);
        $template['result'] = $this->Home_model->shoppingbag();
        $template['Category'] = $this->Women_model->category();
        $template['viewcat'] = $this->Women_model->categoryview($id);
        $template['img1'] = $this->Women_model->viewimg1($id);
        // $result = $this->Women_model->viewimg1($id);
        $template['psize'] = $this->Women_model->viewproductsize($id);
        $template['colo'] = $this->Women_model->viewproductcolor($id);
        $template['data'] = $this->Women_model->userinfo();
        $template['imgs'] = $this->Women_model->viewsubimg($id);
        $template['images'] = $this->Women_model->viewitem_img();
        // var_dump($template['colo']);exit;
        $template['neck'] = $this->Women_neck_model->view_neck();
        $template['wishlist'] = $this->Wishlist_model->wishlistdetails();
        // var_dump($template['neck']);exit;
        $this->load->view('template', $template);
    }
    ////update//
    function updatetowishlist() {
        if ($_POST) {
            $data = $_POST;
            $myArray = array();
            parse_str($_POST['value'], $myArray);
            $res = $this->Wishlist_model->updatetowishlist($myArray);
            if ($res == "success") {
                $result = array('status' => 'success', 'message' => 'Sucessfully Updated to Wishlist');
            } else {
                $result = array('status' => 'error', 'message' => 'Eroor');
            }
            print json_encode($result);
        }
    }
    function move() {
        if ($_POST) {
            $data = $_POST;
            $res = $this->Wishlist_model->move($data);
            if ($res == "success") {
                $result = array('status' => 'success', 'message' => 'Sucessfully Updated to Wishlist');
            } else {
                $result = array('status' => 'error', 'message' => 'Eroor');
            }
            print json_encode($result);
        }
    }
    function countwishlist() {
        $res1 = $this->Wishlist_model->count_wishlist();
        $result = array('count' => $res1);
        print json_encode($result);
    }
    function add_wish() {
        $data = $_POST;
        $prod_id = $data['prod_id'];
        //echo $prod_id ; die;
        $get_product = $this->Wishlist_model->get_prod_details($prod_id);
        $get_prod = (array)$get_product;
        
        $get_products = array(
            'product_id'    => $get_prod['product_id'],    
            'price'         => $get_prod['price'],
            'color_id'      => $get_prod['color_id'],
            'size_id'       => $get_prod['size_id'],
            'quantity'      => '1',
            );
        $result = $this->Wishlist_model->addtowishlist($get_products);
        if ($result == "success") {
            $result = array('status' => 'success', 'message' => 'Sucessfully Added to Wishlist');
        } else {
            $result = array('status' => 'error', 'message' => "Product Already Exist in Wishlist");
        }
        print json_encode($result);
    }
    

}
?>