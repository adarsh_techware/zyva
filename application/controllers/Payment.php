<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");
class Payment extends CI_Controller {
    public function __construct() {
    	parent::__construct();
    	$this->load->helper('paytm');
    }

   /* public function index(){
    	$this->load->view('paytm/payment_form');
    }*/

    public function pgRedirect($book_id=null){

    	load_paytm();
    	$rs = $this->db->where('booking_no',$book_id)->get('booking')->row();
    	$billing = $this->db->where('booking_id',$rs->id)->get('billing_address')->row();
    	$address = $this->db->where('id',$this->session->userdata('user_id'))->get('customer')->row();
    	$checkSum = "";
		$paramList = array();
		$ORDER_ID = $book_id;
		$CUST_ID = $this->session->userdata('user_id');
		$INDUSTRY_TYPE_ID = "Retail109";
		$CHANNEL_ID = "WEB";
		$TXN_AMOUNT = $rs->total_rate;
		$MOBILE_NO = $billing->telephone;
		$EMAIL = $address->email;

		$paramList["MID"] = PAYTM_MERCHANT_MID;
		$paramList["ORDER_ID"] = $ORDER_ID;
		$paramList["CUST_ID"] = $CUST_ID;
		$paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
		$paramList["CHANNEL_ID"] = $CHANNEL_ID;
		$paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
		$paramList["MOBILE_NO"] = $MOBILE_NO;
		$paramList["EMAIL"] = $EMAIL;
		$paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
		$paramList["CALLBACK_URL"] = "https://zyva.in/payment/success";
		$checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
		$data['paramList'] = $paramList;
		$data['checkSum'] = $checkSum;
		$this->load->view('paytm/payment_redirect',$data);
    }


    public function AppRedirect($book_id=null){

    	load_paytm();
    	$rs = $this->db->where('booking_no',$book_id)->get('booking')->row();
    	//$billing = $this->db->where('booking_id',$rs->id)->get('billing_address')->row();
    	$address = $this->db->where('id',$rs->customer_id)->get('customer')->row();
    	$checkSum = "";
		$paramList = array();
		$ORDER_ID = $book_id;
		$CUST_ID = $rs->customer_id;
		$INDUSTRY_TYPE_ID = "Retail109";
		$CHANNEL_ID = "WEB";
		$TXN_AMOUNT = $rs->total_rate;
		$MOBILE_NO = '9400882745';
		$EMAIL = 'adarsh.techware@gmail.com';

		$paramList["MID"] = PAYTM_MERCHANT_MID;
		$paramList["ORDER_ID"] = $ORDER_ID;
		$paramList["CUST_ID"] = $CUST_ID;
		$paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
		$paramList["CHANNEL_ID"] = $CHANNEL_ID;
		$paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
		$paramList["MOBILE_NO"] = $MOBILE_NO;
		$paramList["EMAIL"] = $EMAIL;
		$paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
		$paramList["CALLBACK_URL"] = "https://zyva.in/payment/app_success";
		$checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
		$data['paramList'] = $paramList;
		$data['checkSum'] = $checkSum;
		$this->load->view('paytm/payment_redirect',$data);
    }

    public function success(){
    	load_paytm();
    	$paytmChecksum = "";
		$paramList = array();
		$isValidChecksum = "FALSE";

		$paramList = $_POST;
		$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; 
		$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum);
		if($isValidChecksum == "TRUE") {
			//echo "<b>Checksum matched and following are the transaction details:</b>" . "<br/>";
			//print_r($_POST);
			if ($_POST["STATUS"] == "TXN_SUCCESS") {
				$this->db->where('booking_no',$_POST['ORDERID'])->update('booking',array('status'=>1,'payment_status'=>1));

				$message = "You have received a new order from the customer " . $customer_name;
		        $mobile = "9526039555";
		        $this->send_otp($message, $mobile);
		        $message = "An order has been Successfully placed in Zyva. Order number: " . $_POST['ORDERID'];
		        $billing = $this->db->where('booking_id',$_POST['ORDERID'])->get('billing_address')->row();
		        $customer_no = $billing->telephone;
		        //$mobile = "9526039555";
		        $this->send_otp($message, $customer_no);

				$data = array('booking_no'=>$_POST['ORDERID'],
							  'trans_id'=>$_POST['TXNID'],
							  'amount'=>$_POST['TXNAMOUNT'],
							  'message'=>$_POST['RESPMSG'],
							  'status'=>'Success');
				$this->db->insert('transaction',$data);
				redirect(base_url('checkout/success/'.$_POST['ORDERID']));
				
			}
			else {
				//Array ( [RESPCODE] => 1101 [RESPMSG] => Timeout from Wallet [STATUS] => TXN_FAILURE [MID] => ZyvaSt97684637714768 [TXNAMOUNT] => 1390.00 [ORDERID] => ZYA215923273 [TXNID] => 6322873217 [CHECKSUMHASH] => a4tc6SEwx2nY+VuLw8csKSWEQgYVKNI++kAl+pXPdqjJCTUy8KcmfOsOXBjOdZzqC+JNlIaB83pW2tIVFoHYcnqjT5UYrimhkpX0eAnkhvU= ) 
				//base_url('checkout/success/'+obj.booked_id);

				//$this->db->where('booking_no',$_POST['ORDERID'])->update('booking',array('status'=>1));

			$rs = $this->db->where('booking_no',$_POST['ORDERID'])->get('booking')->row();	
			$cart = $this->db->where('booking_id',$rs->id)->get('order_details')->result();

			foreach ($cart as $rs) {
	            $this->db->set('quantity', "quantity + $rs->quantity", FALSE);
	            $this->db->where('product_id', $rs->product_id);
	            $this->db->where('color_id', $rs->color_id);
	            $this->db->where('size_id', $rs->size_id);
	            $this->db->update('product_quantity');            
        	}



				$data = array('booking_no'=>$_POST['ORDERID'],
							  'trans_id'=>$_POST['TXNID'],
							  'amount'=>$_POST['TXNAMOUNT'],
							  'message'=>$_POST['RESPMSG'],
							  'status'=>$_POST['STATUS']);
				$this->db->insert('transaction',$data);
				redirect(base_url('checkout/failure/'.$_POST['ORDERID']));

			}

			
			

		}
		else {
			echo "<b>Checksum mismatched.</b>";
			//Process transaction as suspicious.
		}
    }

    public function app_success(){
    	load_paytm();
    	$paytmChecksum = "";
		$paramList = array();
		$isValidChecksum = "FALSE";

		$paramList = $_POST;
		$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; 
		$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum);
		if($isValidChecksum == "TRUE") {
			//echo "<b>Checksum matched and following are the transaction details:</b>" . "<br/>";
			//print_r($_POST);
			if ($_POST["STATUS"] == "TXN_SUCCESS") {
				$this->db->where('booking_no',$_POST['ORDERID'])->update('booking',array('status'=>1,'payment_status'=>1));

				$message = "You have received a new order from the customer " . $customer_name;
		        $mobile = "9526039555";
		        $this->send_otp($message, $mobile);
		        $message = "An order has been Successfully placed in Zyva. Order number: " . $_POST['ORDERID'];
		        $billing = $this->db->where('booking_id',$_POST['ORDERID'])->get('billing_address')->row();
		        $customer_no = $billing->telephone;
		        //$mobile = "9526039555";
		        $this->send_otp($message, $customer_no);

				$data = array('booking_no'=>$_POST['ORDERID'],
							  'trans_id'=>$_POST['TXNID'],
							  'amount'=>$_POST['TXNAMOUNT'],
							  'message'=>$_POST['RESPMSG'],
							  'status'=>'Success');
				$this->db->insert('transaction',$data);
				redirect(base_url('payment/app_completed'));
				
			}
			else {
				//Array ( [RESPCODE] => 1101 [RESPMSG] => Timeout from Wallet [STATUS] => TXN_FAILURE [MID] => ZyvaSt97684637714768 [TXNAMOUNT] => 1390.00 [ORDERID] => ZYA215923273 [TXNID] => 6322873217 [CHECKSUMHASH] => a4tc6SEwx2nY+VuLw8csKSWEQgYVKNI++kAl+pXPdqjJCTUy8KcmfOsOXBjOdZzqC+JNlIaB83pW2tIVFoHYcnqjT5UYrimhkpX0eAnkhvU= ) 
				//base_url('checkout/success/'+obj.booked_id);

				//$this->db->where('booking_no',$_POST['ORDERID'])->update('booking',array('status'=>1));

			$rs = $this->db->where('booking_no',$_POST['ORDERID'])->get('booking')->row();	
			$cart = $this->db->where('booking_id',$rs->id)->get('order_details')->result();

			foreach ($cart as $rs) {
	            $this->db->set('quantity', "quantity + $rs->quantity", FALSE);
	            $this->db->where('product_id', $rs->product_id);
	            $this->db->where('color_id', $rs->color_id);
	            $this->db->where('size_id', $rs->size_id);
	            $this->db->update('product_quantity');            
        	}



				$data = array('booking_no'=>$_POST['ORDERID'],
							  'trans_id'=>$_POST['TXNID'],
							  'amount'=>$_POST['TXNAMOUNT'],
							  'message'=>$_POST['RESPMSG'],
							  'status'=>$_POST['STATUS']);
				$this->db->insert('transaction',$data);
				redirect(base_url('payment/app_failure'));

			}

			
			

		}
		else {
			echo "<b>Checksum mismatched.</b>";
			//Process transaction as suspicious.
		}
    }

    function app_completed(){

    }

    function app_failure(){
    	
    }

    function send_otp($msg, $mob_no) {
        $sender_id = "TWSMSG"; // sender id
        $pwd = "968808"; //your SMS gatewayhub account password
        $str = trim(str_replace(" ", "%20", $msg));
        // to replace the space in message with  ‘%20’
        $url = "http://api.smsgatewayhub.com/smsapi/pushsms.aspx?user=nixon&pwd=" . $pwd . "&to=91" . $mob_no . "&sid=" . $sender_id . "&msg=" . $str . "&fl=0&gwid=2";
        // create a new cURL resource
        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // grab URL and pass it to the browser
        $result = curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);
    }
}