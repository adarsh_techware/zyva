<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('string');
        $this->load->model('Cart_model');
        $this->load->model('Women_model');
        $this->load->model('Home_model');
        // if (!$this->session->userdata('user_id')) {
        //     redirect(base_url());
        // }
        
    }
    function index() {
        $template['page'] = 'cart/my_cart';
        $id = $this->uri->segment(3);
        $this->Home_model->cart_update();
        $template['results'] = $this->Women_model->get_cart_details();
        $template['data'] = $this->Women_model->userinfo();
        $template['cart'] = $this->Cart_model->cartdetails();
        // $result                   = $this->Home_model->shoppingbag();
        $template['Category'] = $this->Women_model->category();
        $template['result'] = $this->Home_model->shoppingbag();
        $template['cartlist'] = $this->Home_model->cartlist();
        $template['sub_Category'] = $this->Women_model->sub_category_own();
        $template['size'] = $this->Women_model->getsize();
        $template['psize'] = $this->Women_model->viewproductsize($id);
        $template['colo'] = $this->Women_model->viewproductcolor($id);
        $this->load->view('template', $template);
    }
    // //add to temp//
    function addtotemp() {
        /* print_r($_POST);
        Array
        (
        [value] => customer_id=3&product_id=1&price=&color_id=9&size_id=7&quantity=1&stitching_on=checked
        [stitch] => front_id=1&back_id=25&sleeve_id=49&neck_id=59&add_ons=1&add_ons1=1&add_ons2=4&add_ons3=8&specialcase=sdadasdasd&stitch_id=0
        )
        */
        parse_str($_POST['stitch'], $stitch);
        parse_str($_POST['value'], $cart);
        $addon_change = $this->Cart_model->addon_change($stitch['add_ons'], $stitch['add_ons1'], $stitch['add_ons1'], $stitch['add_ons2'], $stitch['add_ons3']);
        $stiching = $this->db->where('id', 4)->get('shipping_charge')->row();
        $stiching_charge = $stiching->charge + $addon_change;
        $cart['price'] = $cart['price'] + $stiching_charge;
        $stitch['measure_id'] = $stitch['stitch_id'];
        unset($stitch['stitch_id']);
        $stitch['stitch_price'] = $stiching_charge;
        $this->db->insert('stitching', $stitch);
        $stitch_id = $this->db->insert_id();
        $cart['stitching_on'] = 1;
        $cart['stitch_id'] = $stitch_id;
        $rs = $this->db->insert('cart', $cart);
        if ($rs) {
            $result = array('status' => 'success', 'message' => 'Sucessfully Added To Bag');
        } else {
            $result = array('status' => 'error', 'message' => 'Some error occured');
        }
        print json_encode($result);
        /* die();
        if ($_POST) {
        
        // var_dump($_POST);exit;
        
        $data = $_POST;
        $myArray = array();
        parse_str($_POST['value'], $myArray);
        $res = $this->Cart_model->addtotemp($myArray);
        
        //  if(empty($myArray['front_id']) && empty($myArray['back_id']) && empty($myArray['sleeve_id']) && empty($myArray['neck_id']) && empty($myArray['add_ons'])&&empty($myArray['add_ons1'])&&empty($myArray['add_ons2'])&&empty($myArray['add_ons3']) )
        //  {
        //  unset($myArray['front_id']);
        //  unset($data['back_id']);
        //  unset($data['sleeve_id']);
        //  unset($data['neck_id']);
        //  unset($data['add_ons']);
        //  unset($data['add_ons1']);
        //  unset($data['add_ons2']);
        //  unset($data['add_ons3']);
        //  }
        // else{
        // print_r($myArray);die;
        // $this->Cart_model->addtocart($myArray);
        
        $data['front'] = $this->Cart_model->view_front($myArray['front_id']);
        $data['back']    = $this->Cart_model->view_back($myArray['back_id']);
        $data['sleeve']  = $this->Cart_model->view_sleeve($myArray['sleeve_id']);
        $data['hemline'] = $this->Cart_model->view_hemline($myArray['neck_id']);
        $data['addons']  = $this->Cart_model->view_addons($myArray['add_ons']);
        $data['addons1'] = $this->Cart_model->view_addons1($myArray['add_ons1']);
        $data['addons2'] = $this->Cart_model->view_addons2($myArray['add_ons2']);
        $data['addons3'] = $this->Cart_model->view_addons3($myArray['add_ons3']);
        $data['charge']  = $this->Cart_model->stich_charge();
        $data['price']   = $myArray['price'];
        $data['insert_id'] = $res;
        
        // print_r($data['price']);die;
        
        $template = $this->load->view('women/review_report', $data, true);
        
        // die;
        
        if ($res) {
        $result = array(
        'status' => 'success',
        'message' => 'sucessfully Added to cart',
        'template' => $template,
        'temp_id' => $res
        );
        }
          else {
        $result = array(
        'status' => 'error',
        'message' => 'Eroor'
        );
        }
        
        print json_encode($result);
        
        // }*/
    }
    public function get_quntity() {
        $data = $_POST;
        $product_id = $data['product_id'];
        $color_id = $data['color_id'];
        $size_id = $data['size_id'];
        $rs = $this->db->query("SELECT quantity FROM product_quantity WHERE color_id='$color_id' AND size_id ='$size_id' AND product_id ='$product_id'")->row();
        $quantity = $rs->quantity > 15 ? 15 : $rs->quantity;
        $string = '';
        for ($i = 1;$i <= $quantity;$i++) {
            $string.= '<option value="' . $i . '">' . $i . '</option>';
        }
        echo $string;
    }

    // ******************************REVIEW REPORT VIEW
    function review() {
        $template['page'] = 'women/review_report';
        $a = $this->load->view('template', $template);
    }
    // **********************************************
    
    // ****************************ADD TO CART 
    function addtocart() {
        if ($_POST) {
            $data = $_POST;
            $myArray = array();
            parse_str($_POST['value'], $myArray);
            $res = $this->Cart_model->addtocart($myArray);
            // die;
            if ($res == "success") {
                $result = array('status' => 'success', 'message' => 'Sucessfully Added To Bag');
            } else {
                $result = array('status' => 'error', 'message' => 'Eroor');
            }
            print json_encode($result);
        }
    }
    // ******************************************
    
    function addtocart1() {
        if ($_POST) {
            $data = $_POST;
            $myArray = array();
            parse_str($_POST['value'], $myArray);
            $res = $this->Cart_model->addtocart1($myArray);
            // }
            if ($res == "success") {
                $result = array('status' => 'success', 'message' => 'Sucessfully Added To Bag');
            } else if ($res == "notfound") {
                $result = array('status' => 'notfound', 'message' => 'Out of Stock');
            } else {
                $result = array('status' => 'error', 'message' => 'Eroor');
            }
            print json_encode($result);
        }
    }
    function addalltocart() {
        if ($_POST) {
            $data = $_POST;
            $data1 = $this->Cart_model->checkcart($data);
            if ($data1 == "noexist") {
                $res = $this->Cart_model->addalltocart();
                if ($res) {
                    $result = array('status' => 'success', 'message' => 'Sucessfully Added To Bag');
                } else {
                    $result = array('status' => 'error', 'message' => 'Eroor');
                }
            } else if ($data1 == "exist")
            // else
            {
                $result = array('status' => 'exist', 'message' => 'Sucessfully Added To Bag');
            }
            print json_encode($result);
        }
    }
    function remove_cart() {
        if ($_POST) {
            $data = $_POST;
            //  print_r($data);
            // die;
            $res = $this->Cart_model->remove_cart($data);
            $res1 = $this->Cart_model->count_cart();
            if ($res == "success") {
                $result = array('status' => 'success', 'message' => 'Sucessfully  Removed From Bag', 'count' => $res1);
            } else {
                $result = array('status' => 'error', 'message' => 'Eroor');
            }
            // var_dump($res1);
            // die;
            print json_encode($result);
        }
    }
    function add_stitching() {
        if ($_POST) {
            $data = $_POST;
            $myArray = array();
            parse_str($_POST['value'], $myArray);
            $res = $this->Cart_model->add_stitching($myArray);
            if ($res == "success") {
                $result = array('status' => 'success', 'message' => 'Sucessfully Added To Bag');
            } else {
                $result = array('status' => 'error', 'message' => 'Eroor');
            }
            print json_encode($result);
        }
    }
    function payment() {
        if ($_POST) {
            $data = $_POST;
            $res = $this->Cart_model->payment($data);
            $result = array('status' => 'success', 'booked_id' => $res);
            print json_encode($result);
        }
    }
}
?>