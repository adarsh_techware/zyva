<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My404 extends CI_Controller {

 public function __construct() {
 	parent::__construct();
 	$this->load->model('Women_model');
 	$this->load->model('Home_model');
}
   function index(){
   	$template['Category'] = $this->Women_model->category();
   	$template['data'] = $this->Home_model->userinfo();
	$template['result']=$this->Home_model->shoppingbag();
	$template['cartlist']=$this->Home_model->cartlist();
   	$template['page'] = 'my404';
   	$this->load->view('template',$template);
   }
}

   	?>