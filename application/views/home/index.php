<div class="landing_page">
    <div class="top_banner">
        <div class="container top_ban_pad">
            <img src="<?php echo base_url();?>assets/images/home/top_banner.png" class="hidden-xs">
            <img src="<?php echo base_url();?>assets/images/home/top_banner_mobile.png" class="hidden-lg hidden-md hidden-sm">
        </div>
    </div>
   <!-- fashion_block  -->
   <div class="banner_scnd">
      <div class="container">
         <div class="col-lg-12 pd-cust-1 no_padng">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pd-zero no_padng">
               <div class="fshion_store_out">
                  <div class="fshion_store">
                     <div class="fasion_head">
                        <div class="circle_out_full">
                           <div class="circle_out">
                              <div class="circle">Our Deals </div>
                           </div>
                        </div>
                        <div class="circle_scnd">
                           <h1>THE TRAILBLAZER</h1>
                           <h3>Of Online Kurtis</h3>
                        </div>
                     </div>
                     <div class="fasion_descript">
                        <p>
                           Show off your unique tastes in fashion with the vibrant colors, trendy cuts and stylish
                           collections of various kurtis from Maryam Apparels. Browse through our selection of kurtis and
                           intensify your feminine allure.
                        </p>
                     </div>
                     <div class="fasion_button">
                        <a href="<?php echo base_url('product/women');?>">  <button class="blck_read red_mre foot">Shop Now</button></a>
                     </div>
                  </div>
                  <div class="dress_material">
                     <img  class=" derss_a img-responsive" src="<?php echo base_url();?>assets/images/home/dres_pat.png">
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pd-zero">
               <div class="fshion_stre">
                  <div class="women_acesseries">
                     <h4>TALES OF</h4>
                     <hr class="bord_bot">
                     </hr>
                     <p>Tradition</p>
                  </div>
                  <div class="dress_b">
                     <img src="<?php echo base_url();?>assets/images/home/dress_pat_a.png" >
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- fashion_block end -->

   <!-- sale_block  -->
   <div class="banner_thrd" id="myDiv">
      <div class="container">
         <div class="row">
            <div class=" col-lg-4 col-md-4 col-sm-12 col-xs-12 pad_rht" >
               <div class="dress_materials">
                  <img class="img-responsive " src="<?php echo base_url();?>assets/images/home/dress_c.png">
               </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="super_sale">
                     <div class="super_sale_inner">
                        <h3>CATCH<br>
                           A GLIMPSE OF THE
                        </h3>
                        <h4>wide range of</h4>
                        <p>Maryam Apparels Ethnic Kurtis</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="sale_img">
                     <div class="hover " style="padding:0px;">
                        <div class="hovereffect">
                           <img class="img-responsive"src="<?php echo base_url();?>assets/images/home/dress_d.png" alt="">
                           <div class="overlay">
                              <a class="info" href="<?php echo base_url('product/women');?>">link here</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="arrivals">
                     <div class="hover">
                        <div class="hovereffect">
                           <img class="img-responsive"src="<?php echo base_url();?>assets/images/home/dress_e.png" alt="">
                           <div class="overlay">
                              <a class="info" href="<?php echo base_url('product/women');?>">link here</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-12 padding_none">
                  <div class="arival_new ">
                     <div class="arival_new_inner">
                        <h5>NEW ARRIVALS</h5>
                        <h3>#outfit</h3>
                        <h4><span class="gray_of">of </span>today</h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- sale_block end -->

   <!-- derss_block  -->
   <div class="bottom_banner">
        <div class="container">
            <div class="col-lg-8 col-md-8 col-sm-8 bot_banner_pad">
                <img src="<?php echo base_url();?>assets/images/home/bottom_banner1.png" class="bot_banner_img1">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 bot_banner_2pad">
                <img src="<?php echo base_url();?>assets/images/home/bottom_banner2.png" class="bot_banner_img2">
            </div>
        </div>
    </div>

</div>
</body>