<style>
   
</style>
<div class="login_wrapper">
   <div class="container">
      <div class="col-md-12 no_padng">
         <div class="col-md-4"></div>
         <div class="col-md-4 no_padng">
            <form method="POST" id="login_form">
               <div class="login_box">
                  <div class="padng_outer">
                     <div class="login_to_zyva">
                        <h3>Login to zyva</h3><hr class="log_hr">
                     </div>
                     <input type="text"  name="email" required="" class="text_box_input" 
                        data-parsley-trigger="change" autocomplete="off" 
                        data-parsley-required-message="Email is required." 
                        data-parsley-errors-container="#uname"  placeholder="Email">
                     <div id="uname" class="uname_pass"></div>
                     <div class="pass_height">
                        <input type="password" name="password" required="" class="text_box_input"
                        data-parsley-trigger="change"  
                        autocomplete="off" data-parsley-required-message="Password is required." 
                        data-parsley-errors-container="#pass" placeholder="Password">
                     
                        <div id="pass" class="pass_error"></div>
                        <div class="error_div pass_error"></div>

                     </div>
                     <div class="log_fogot">
                        <button  type="button" class="log_btn" id="login_button" >Login</button>
                        <p class="forgot_pass"> 
                           <a href="<?php echo base_url('home/forgot_password');?>" >Forgot password ?</a>
                        </p>
                     </div>
                     <div class="fb_google">
                           <div class="fb">
                              <a href="<?php echo $authUrl;?>"><button type="button"><p>Facebook</p></button></a>
                           </div>
                           <div class="google">
                              <a href="<?php echo $googleUrl;?>"><button type="button"><p>Google</p></button></a>
                           </div>
                           <div class="clear"></div>
                     </div>

                  </div>
               </div>
            </form>   
         </div>
         <div class="col-md-4"></div>
      </div>
      <div class="col-md-12 no_padng">
         <div class="col-md-4"></div>
         <div class="col-md-4 no_padng">
            <div class="reg_now">
               <div class="cabin_padding">
                  <div class="login_new_user">
                     <p>New to Zyva?</p>
                     <a href="<?php echo base_url('home/signup');?>">register now</a>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4"></div>
      </div>
   </div>
</div>