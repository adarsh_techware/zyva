<div class="banner">
<div class="starter-template">
   <div class="home_first_cabin">
      <!-- slider  -->
      <div class="container">
         <div class="col-md-7 col-sm-4 col-xs-12 remove_pad">
            <div class="slide_section">
               <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators slider_carousel">
                     <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                     <li data-target="#myCarousel" data-slide-to="1"></li>
                     <li data-target="#myCarousel" data-slide-to="2"></li>
                     <li data-target="#myCarousel" data-slide-to="3"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                     <div class="item active">
                        <a href="#"><img class="bg frst_img" src="<?php echo base_url();?>assets/images/home/model_1.png" /></a>
                     </div>
                     <div class="item">
                        <!-- <a href="#"><img class="bg sec_img" src="<?php echo base_url();?>assets/images/home/model_2.png"/></a> -->
                        <a href="#"><img class="bg frst_img" src="<?php echo base_url();?>assets/images/home/model_2.png" /></a>
                     </div>
                     <div class="item">
                        <!-- <a href="#"><img class="bg" src="<?php echo base_url();?>assets/images/home/model_3.png" /></a> -->
                        <a href="#"><img class="bg frst_img" src="<?php echo base_url();?>assets/images/home/model_3.png" /></a>
                     </div>
                     <div class="item">
                        <a href="#"><img class="bg frst_img" src="<?php echo base_url();?>assets/images/home/model_4.png" /></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-5 col-sm-8 col-xs-12 remove_left_pad">
            <div class="col-md-12 remove_left_pad">
               <div class="outer_des">
                  <div class="text">
                     <h1>ZYVA KURTIS</h1>
                     <h5><span class="bord_span">BLEND OF QUALITY </span>AND STYLE</h5>
                     <p>Choose the elegant styles of kurtis collection from Zyva Fashions. We deliver customized
                        designs, quality handpicked material with affordable prices. We also have some bestselling
                        designs of embroidered, ethnic floral printed, contemporary printed kurtis for those who are
                        looking for new trends. We design whatever you need. The only thing you need to do is to
                        start shopping!
                     </p>
                     <a href="<?php echo base_url('product/women');?>"> <button class="zyva_but box foo">Shop Now</button></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- slider  end-->
   </div>
   <!-- fashion_block  -->
   <div class="banner_scnd">
      <div class="container">
         <div class="col-lg-12 pd-cust-1">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pd-zero">
               <div class="fshion_store_out">
                  <div class="fshion_store">
                     <div class="fasion_head">
                        <div class="circle_out_full">
                           <div class="circle_out">
                              <div class="circle">Zyva Deals </div>
                           </div>
                        </div>
                        <div class="circle_scnd">
                           <h1>THE TRAILBLAZER</h1>
                           <h3>Of Online Kurtis</h3>
                        </div>
                     </div>
                     <div class="fasion_descript">
                        <p>
                           Show off your unique tastes in fashion with the vibrant colors, trendy cuts and stylish
                           collections of various kurtis from Zyva Fashion. Browse through our selection of kurtis and
                           intensify your feminine allure.
                        </p>
                     </div>
                     <div class="fasion_button">
                        <a href="<?php echo base_url('product/women');?>">  <button class="blck_read red_mre foot">Shop Now</button></a>
                     </div>
                  </div>
                  <div class="dress_material">
                     <img  class=" derss_a img-responsive" src="<?php echo base_url();?>assets/images/home/dres_pat.png">
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pd-zero">
               <div class="fshion_stre">
                  <div class="women_acesseries">
                     <h4>TALES OF</h4>
                     <hr class="bord_bot">
                     </hr>
                     <p>Tradition</p>
                  </div>
                  <div class="dress_b">
                     <img src="<?php echo base_url();?>assets/images/home/dress_pat_a.png" >
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- fashion_block end -->
   <!-- sale_block  -->
   <div class="banner_thrd" id="myDiv">
      <div class="container">
         <div class="row">
            <div class=" col-lg-4 col-md-4 col-sm-12 col-xs-12 pad_rht" >
               <div class="dress_materials">
                  <img class="img-responsive " src="<?php echo base_url();?>assets/images/home/dress_c.png">
               </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="super_sale">
                     <div class="super_sale_inner">
                        <h3>CATCH<br>
                           A GLIMPSE OF THE
                        </h3>
                        <h4>wide range of</h4>
                        <p>Zyvas Ethnic Kurtis</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="sale_img">
                     <div class="hover " style="padding:0px;">
                        <div class="hovereffect">
                           <img class="img-responsive"src="<?php echo base_url();?>assets/images/home/dress_d.png" alt="">
                           <div class="overlay">
                              <a class="info" href="<?php echo base_url('product/women');?>">link here</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="arrivals">
                     <div class="hover">
                        <div class="hovereffect">
                           <img class="img-responsive"src="<?php echo base_url();?>assets/images/home/dress_e.png" alt="">
                           <div class="overlay">
                              <a class="info" href="<?php echo base_url('product/women');?>">link here</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-12 padding_none">
                  <div class="arival_new ">
                     <div class="arival_new_inner">
                        <h5>NEW ARRIVALS</h5>
                        <h3>#outfit</h3>
                        <h4><span class="gray_of">of </span>today</h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- sale_block end -->
   <!-- derss_block  -->
   <div class="banner_frth">
      <div class="container">
         <div class="row">
            <div id="owl-demo">
               <div class="item">
                  <div class="model_a">
                     <div class="wrapper">
                        <div class="plus_add first_plus"> +</div>
                        <div class="tooltip first_tool">
                           <h3>Kurthis</h3>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                           <p class="rd_mre">Read More</p>
                        </div>
                     </div>
                     <img src="<?php echo base_url();?>assets/images/home/model_a.png">
                  </div>
               </div>
               <div class="item">
                  <div class="model_a">
                     <div class="wrapper chge_loc">
                        <div class="plus_add"> +</div>
                        <div class="tooltip">
                           <h3>Kurthis</h3>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                           <p class="rd_mre">Read More</p>
                        </div>
                     </div>
                     <img src="<?php echo base_url();?>assets/images/home/model_b.png">
                  </div>
               </div>
               <div class="item">
                  <div class="model_a">
                     <div class="wrapper chge_lft">
                        <div class="plus_add"> +</div>
                        <div class="tooltip">
                           <h3>Kurthis</h3>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                           <p class="carousel-indicators slider_carouselrd_mre">Read More</p>
                        </div>
                     </div>
                     <img src="<?php echo base_url();?>assets/images/home/model_c.png">
                  </div>
               </div>
               <div class="item">
                  <div class="model_a">
                     <div class="wrapper chge_right">
                        <div class="plus_add"> +</div>
                        <div class="tooltip ">
                           <h3>Kurthis</h3>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                           <p class="carousel-indicators slider_carouselrd_mre">Read More</p>
                        </div>
                     </div>
                     <img src="<?php echo base_url();?>assets/images/home/model_d.png">
                  </div>
               </div>
               <div class="item">
                  <div class="model_a">
                     <div class="wrapper chge_lft">
                        <div class="plus_add"> +</div>
                        <div class="tooltip">
                           <h3>Kurthis</h3>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                           <p class="carousel-indicators slider_carouselrd_mre">Read More</p>
                        </div>
                     </div>
                     <img src="<?php echo base_url();?>assets/images/home/model_e.png">
                  </div>
               </div>
               <div class="item">
                  <div class="model_a">
                     <div class="wrapper chge_right">
                        <div class="plus_add"> +</div>
                        <div class="tooltip">
                           <h3>Kurthis</h3>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                           <p class="carousel-indicators slider_carouselrd_mre">Read More</p>
                        </div>
                     </div>
                     <img src="<?php echo base_url();?>assets/images/home/model_f.png">
                  </div>
               </div>
               <div class="item">
                  <div class="model_a">
                     <div class="wrapper chge_loc">
                        <div class="plus_add"> +</div>
                        <div class="tooltip">
                           <h3>Kurthis</h3>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                           <p class="carousel-indicators slider_carouselrd_mre">Read More</p>
                        </div>
                     </div>
                     <img src="<?php echo base_url();?>assets/images/home/model_h.png">
                  </div>
               </div>
               <div class="item">
                  <div class="model_a">
                     <div class="wrapper chge_lft">
                        <div class="plus_add"> +</div>
                        <div class="tooltip">
                           <h3>Kurthis</h3>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                           <p class="carousel-indicators slider_carouselrd_mre">Read More</p>
                        </div>
                     </div>
                     <img src="<?php echo base_url();?>assets/images/home/model_g.png">
                  </div>
               </div>
               <div class="item">
                  <div class="model_a">
                     <div class="wrapper chge_lft">
                        <div class="plus_add"> +</div>
                        <div class="tooltip">
                           <h3>Kurthis</h3>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                           <p class="carousel-indicators slider_carouselrd_mre">Read More</p>
                        </div>
                     </div>
                     <img src="<?php echo base_url();?>assets/images/home/model_c.png">
                  </div>
               </div> 
            </div>
         </div>
      </div>
   </div>
</div>
</body>
</html