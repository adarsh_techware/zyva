<?php //echo '<pre>'; 
//print_r($data_rec);?>
<body>
        <!-- nav bar -->

    <div class="starter-template">

        <!-- wrapper -->
        <div class="full_wdth_wrapper"></div>
        <!-- wrapper end -->

        <!-- tab bar  -->
		<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
            <div class="inner_tab ">
                <div class="container">
                    <div class="row">
                        <div class="navs_tabs">
                            <ul>
                                <li> <a>Home</a></li>
                                <li>/</li>
                                <li class="women_clr"><a>Women</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <div class="tab_out">
                                <p><span>category</span></p>
                                <div class="fancy-collapse-panel">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                      <?php
            						  $i=0;
            						  $act_cls = "in";
            						  $act_tab = "";
            						  $act_state = "true";
            						  foreach ($Category as $cat) {
            							  //print_r($Category);die;
            							  $i++;
            						 ?>
                                        <div class="panel panel-default  women_panel">
                                            <div class="panel-heading no_head" role="tab" id="headingOne<?php echo $i;?>">
                                                

                                                <div <?php if ( $this->uri->uri_string() == 'Women/index/'.$cat->category_name ) : ?> class="active_menu" <?php endif; ?> >
                                                <h4 class="panel-title fnt_sze">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $i; ?>" aria-expanded="<?php echo $act_state; ?>" aria-controls="collapseOne<?php echo $i;?>" class="<?php echo $act_tab; ?>">
                                                      <?php echo $cat->category_name;?>
            										  
                                                    </a>
                                                </h4>
                                            </div>
                                            </div>
                                            <div id="collapseOne<?php echo $i;?>" aria-expanded="<?php echo $act_state; ?>" class="panel-collapse collapse <?php echo $act_cls; ?>" role="tabpanel" aria-labelledby="headingOne<?php echo $i;?>">
                                                <div class="panel-body no_bord">
            									<input type="hidden"  id="name"  name="name" value="<?php echo $cat->category_name;?>">

                                                <?php
                                                foreach ($sub_Category as $sub) {
            										//  print_r($sub);die;
                              	                if(($sub->category_name)==($cat->category_name)){
                                                ?>
                                                    <ul class="tab_list">
                                                      <li class="active">
                                                        <a href="<?php echo site_url('Women/index/'.$sub->sub_category);?>"><?php echo $sub->sub_category;?></a></li>
            										   <input type="hidden"   id="subcategory" name="subcategory" value="<?php echo site_url('Women/index/'.$sub->sub_category);?>">

                                                        <!-- <li class="active"><a data-toggle="tab" href="#home">Sarees</a></li>
                                                        <li><a data-toggle="tab" href="#menu1">Dresses & Gowns</a></li>
                                                        <li><a data-toggle="tab" href="#menu1">Anarkali & Suits</a></li>
                                                        <li><a data-toggle="tab" href="#menu2">Kurtas & Suits Sets </a></li>
                                                        <li><a data-toggle="tab" href="#menu3">Lehengas </a></li>
                                                        <li><a data-toggle="tab" href="#menu5">Tops</a></li>
                                                        <li><a data-toggle="tab" href="#menu6">Bottoms</a></li>
                                                        <li><a data-toggle="tab" href="#menu7">Dupatta & Stoles</a></li> -->
            										<?php }} ?>    
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>


                                        <?php
            							$act_tab = "collapsed";
            							$act_cls = "";
            							$act_state = "false";
            							} ?>

                                    </div>
                                </div>
                            	<p><span>filter</span></p>
                                <div class="layout">
                                    <p>Price</p>
                                    <input type="hidden" name="max_range" id="max_range" value="<?php echo $max; ?>" >
                                    <input type="hidden" name="min_range" id="min_range" value="<?php echo $min; ?>" >
                               
                                    <div class="layout-slider" style="width: 100%;padding-left:15px;" >
                                        <span style="display: inline-block; width: 220px; padding: 0 5px;">
                                            <input id="Slider1" type="slider" name="price" class="selector" value="<?php echo $min.';'.$max; ?>" />
                                        </span>  
                                    </div>
                                </div>
      
                                <form action="" method="POST">

                                    <div class="price_lst">
                                        <p>Size</p>
                                        <div class="sizes_outs" id="btn">
                                          <?php
                                          foreach ($size as $ss) {
                                          ?>
                                            <div class="sizes_out">
                                                <div class="sizes" data-id="<?php echo $ss->id;?>"><?php echo $ss->size_type; ?></div>
                								 <input type="hidden"  id="size"  name="size" value="<?php echo $ss->size_type; ?>" >
                                            </div>
                                            <?php } ?>
                                            <!-- <div class="sizes_out">
                                                <div class="sizes">S</div>
                                            </div>
                                            <div class="sizes_out">
                                                <div class="sizes">m</div>
                                            </div>
                                            <div class="sizes_out">
                                                <div class="sizes">l</div>
                                            </div> -->
                                        </div>
                                        <!-- <div class="sizes_outs">
                                            <div class="sizes_out ince_outer">
                                                <div class="sizes ince_siz">xxl</div>
                                            </div>
                                            <div class="sizes_out ince_outer">
                                                <div class="sizes ince_siz">xxxl</div>
                                            </div>
                                        </div> -->
                                    </div>
            					
                                </form>
    					
                            </div>
                        </div>

                    <!--  <div id="search_content"> -->
                        <div class="col-md-9">
                            <div class="sort_by">
                                <p>Sort by</p>
                                <div class="select-style">
                                    <select name="sort" class="filter_search"  onchange="this.form.submit()";>
                                        <option value="product" <?php if($sort=="product") echo 'SELECTED="SELECTED"'; ?>>Newest Product  </option>
                                        <option value="high" <?php if($sort=="high") echo 'SELECTED="SELECTED"'; ?>>High Price</option>
                                        <option value="low" <?php if($sort=="low") echo 'SELECTED="SELECTED"'; ?>>Low Price</option>
                                    </select>
                                </div>

                                <!-- <ul class="pagination">

                                   <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw1.png"></a></li>
                                    <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw2.png"></a></li>

                                    <li><a class="active" href="<?php echo site_url('Women/index') ?>"><?php echo $links; ?></a></li>
                                    
                                    <li><a class="active" href="<?php echo site_url('Women/index') ?>"><?php echo $links; ?></a></li>
                                     <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw3.png"></a></li>
                                     <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw4.png"></a></li> 
                                </ul> -->

                                <ul class="pagination">
                                    <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw1.png"></a></li>
                                    <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw2.png"></a></li>
                                    <li><a class="active" href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">6</a></li>
                                    <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw3.png"></a></li>
                                    <li><a href="#"><img src="<?php echo base_url();?>assets/images/pag_arw4.png"></a></li>
                                </ul>

                            </div>
                            <div class="tab-content contnt_tabs">
                                <div id="search_content">
                					<div id="price_content1">
                                    <?php
                                    if($catimg) {   ?>
                                        <div id="home" class="tab-pane fade in active box_shdw pad_nill">
                                        <ul id="port" class="clearfix">
                                             <?php
                        					 //echo '<pre>';
                                             //print_r($catimg);die;

                                            foreach ($catimg as $img) {
                        						
                                                $id=$img->id;
                                                $d=$id;

                                                // base64_encode($d)

                                                // print_r($d);die;
                                                // $this->session->set_userdata('id',$id);
                                                // $d=$this->session->userdata('id');
                                                $this->db->select_max("quantity");
                        						$this->db->from("product_quantity");
                        						$this->db->where("product_id",$img->product_id);
                        						$query = $this->db->get();
                        						$result = $query->result();							
                        						//return $result;
                                                ?>
                                                
                                                    <li>

                                                        <a href="<?php echo base_url('Women/productdetail/'.$img->product_name);?>"><img src="<?php  echo $img->product_image;?>"></a>
                        								<input type="hidden"  id="category"  name="category" value="<?php echo base_url('Women/productdetail/'.$img->product_name);?>">
                                                        <!-- <a href="<?php echo base_url();?>assets/images/salwar_a.png"><img src="<?php echo base_url();?>assets/images/salwar_a.png"></a>
                                                        <!-- <a href="#"><img src="<?php //echo $img->category_image;?>"></a> -->

                                                        <h4><?php echo $img->product_name;?></h4>
                                                        <p> &#8377; <?php echo $img->price;?></p>

                        							    <?php
                        								foreach ($result as $q) 
                        									if($q->quantity=='0') { ?>
                        									
                        						                <div class="stock">Out of stock</div>
                        						
                        								    <?php } 

                                                            else { ?>
                        								        <div class="stock1"></div>
                        								    <?php } ?>
                        								
                                                        <div class="zyva_lg"><img class="zyva_blck" src="<?php echo base_url();?>assets/images/zyva_lg.png"></div>
                                                    </li>
                                                
                                            <?php } ?>
</ul>
                                        </div>

                                    <?php } 
                                    else { ?>
                                        <h1> No results found </h1>
                                    <?php } ?>


                                    </div>
                                </div>

                                <div id="menu1" class="tab-pane fade box_shdw">
                                    <h3>Menu 1</h3>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                </div>
                                <div id="menu2" class="tab-pane fade box_shdw">
                                    <h3>Menu 2</h3>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                </div>
                                <div id="menu3" class="tab-pane fade box_shdw">
                                    <h3>Menu 3</h3>
                                    <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </form>
    </div>
            <!-- tab bar end -->
            <!-- footer_block -->
           
            <!-- footer_block end -->
            <!-- Modal login -->
           
            <!-- Modal login end -->
            <!-- Modal register -->
           
            <!-- Modal register end -->
        
        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        
         <script type="text/javascript">
        // $(document).ready(function(){
        	// var value = $(".selector").slider("value");
        	// alert(value);
        	// $(".selector").slider(){
        	// 	alert('fghfgh');
        	// }

        	// $(".selector").slider('onstatechange',function(){
        	// 	alert('dfgdfgdf');
        	// })
        	

        // })

        	
        </script>
</body>

