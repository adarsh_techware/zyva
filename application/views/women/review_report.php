 <div class="modal fade" id="rateModel" role="dialog">
               <div class="modal-dialog modal-lg" id="rev">
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-body login_modal">
                        <div class="reg_modals">

                           <div class="col-md-12 no_padng">
                              <div class="stich_model">
                                  <div class="padng_outer pad_bot ">
                                    <!-- <div class="col-md-1 col-sm-11 col-xs-11 no_padng"></div> -->
                                      <div class="col-md-11 col-sm-11 col-xs-11 rate_head">
                                        <div class="rate_des">
                                          <img src="<?php echo base_url();?>assets/images/own_des.png">
                                            <h5>PREVIEW ORDER</h5>
                                            <p>Note: The tailors will improvise on your final product based on the fabric design. For instance, borders will be used on sleeves unless specified. We highly recommend you to pre-wash your cotton dress materials.</p>
                                          </div>
                                      </div>
                                      <div class="col-md-1 col-sm-1 col-xs-1 no_padng">
                                       <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url();?>assets/images/modal_close.png"></button>
                                    </div>
                                </div>
                             </div>
                          </div>

                           <div class="col-md-12 no_padng">
                              <div class="stich_model">

                                 <!-- multistep form -->
                                 <form>
                                   <div class="rate_model">
                                       <div class="col-md-12">
                                          <h6 class="placket">MAIN</h6>
                                       </div>

                                       <div class="col-md-1"></div>
                                       <div class="col-md-10">
                                          <ul class="rate_dot_border">
                                            <?php 

                                            if($front){

                                            ?>
                                            <li>
                                              <h5>FRONT</h5>
                                              <img src="<?php echo $front->image;?>">
                                              <p><?php echo $front->discription;?></p>
                                            </li>
                                            <?php 
                                          }

                                          else{
                                            ?>

                                            

                                             <li>
                                              <h5>FRONT</h5>
                                              <img src="">
                                              <p></p>
                                            </li>
                                            <?php } ?>

                                            <?php 

                                            if($back){

                                            ?>

                                            <li>
                                             <h5>BACK</h5>
                                              <img src="<?php echo $back->image;?>">
                                              <p><?php echo $back->discription;?></p>
                                            </li>

                                            <?php 
                                              }
                                              else{

                                            ?>

                                          <li>
                                             <h5>BACK</h5>
                                              <img src="">
                                              <p></p>
                                            </li>

                                            <?php } ?>

                                            <?php if($sleeve){  ?>
                                            <li class="sleeve_list">
                                             <h5>SLEEVE</h5>
                                              <img src="<?php echo $sleeve->image;?>">
                                              <p><?php echo $sleeve->discription;?> </p>
                                             <!-- <p class="cap_sleeve">Cap sleeve</p> -->
                                            </li>

                                            <?php } 

                                            else {
                                                ?>


                                            <li class="sleeve_list">
                                             <h5>SLEEVE</h5>
                                              <img src="">
                                              <p></p>
                                             <p class="cap_sleeve"></p>
                                            </li>
                                            <?php } ?>

                                            <?php  if($hemline){    ?>
                                            <li>
                                             <h5>HEMLINE</h5>
                                              <img src="<?php echo $hemline->image;?>">
                                              <p><?php echo $hemline->discription;?></p>
                                            </li>
                                            <?php } 
                                              else {
                                            ?>
                                            <li>
                                             <h5>HEMLINE</h5>
                                              <img src="">
                                              <p></p>
                                            </li>
                                            <?php } ?>

                                          </ul>
                                       </div>
                                       <div class="col-md-1"></div>

                                       <div class="col-md-12">
                                          <div class="col-md-2"></div>
                                          <div class="col-md-4 col-sm-6 col-xs-6">
                                             <div class="rate_extra">
                                                <h6 class="placket extra">Extras</h6>
                                                <?php // print_r($addons);die;?>

                                              <?php  
                                                $add_charge = 0;

                                               if($addons) { 
                                                $add_charge += $addons->charge;

                                                ?>
                                                <h3 class="no_pad_top"><?php echo $addons->adons_type;?></h3>
                                                <div class="rate_subhead"><?php echo $addons->discription;?></div>
                                                <div class="rate_amount">Rs.<?php echo $addons->charge;?>/-</div>
                                                <?php } 

                                                else {
                                                ?>
                                                <h3 class="no_pad_top"></h3>
                                                <div class="rate_subhead"></div>
                                                <div class="rate_amount"></div>
                                                <?php } ?>




                                                  <?php   if($addons1!='') { 
                                                    $add_charge += $addons1->charge;
                                                    ?>
                                                <h3><?php echo $addons1->adons_type;?></h3>
                                                <div class="rate_subhead"><?php echo $addons1->discription;?></div>
                                                <div class="rate_amount">Rs.<?php echo $addons1->charge;?>/-</div>
                                                  <?php } 
                                                    else {
                                                  ?>
                                                <h3></h3>
                                                <div class="rate_subhead"></div>
                                                <div class="rate_amount"></div> 
                                                <?php } ?>

                                                 <?php   if($addons2) { 
                                                      $add_charge += $addons2->charge;

                                                  ?>
                                                <h3><?php echo $addons2->adons_type;?></h3>
                                                <div class="rate_subhead"><?php echo $addons2->discription;?></div>
                                                <div class="rate_amount">Rs.<?php echo $addons2->charge;?>/-</div>
                                                  <?php } else { ?>

                                                     <h3></h3>
                                                <div class="rate_subhead"></div>
                                                <div class="rate_amount"></div>

                                               <?php  } ?>

                                               <?php   if($addons3) { 
                                                $add_charge += $addons3->charge;
                                                ?>
                                                <h3><?php echo $addons3->adons_type;?></h3>
                                                <div class="rate_subhead"><?php echo $addons3->discription;?></div>
                                                <div class="rate_amount">Rs.<?php echo $addons3->charge;?>/-</div>
                                                <?php }  else { ?>

                                                 <h3></h3>
                                                <div class="rate_subhead"></div>
                                                <div class="rate_amount"></div>
                                                <?php } ?>
                                             </div>
                                          </div>
                                          <!-- <div class="col-md-1 "></div> -->

                                          <!-- <div class="col-md-6"> -->
                                          <div class="col-md-1 rate_border_right"></div>


                                           
                                          <div class="col-md-4 col-sm-6 col-xs-6">

                                         <div class="rate_extra">
                                                <h6 class="placket extra">Price</h6>
                                                
                                                <div class="rate_subhead">product Price:</div>
                                                <div class="rate_amount">Rs.<?php echo $price;?>/- </div>


                                                <div class="rate_subhead">Basic Stiching:</div>
                                                <div class="rate_amount">Rs.<?php echo $charge->charge;?>/- </div>

                                                
                                                <div class="rate_subhead extra_pad">Add_ons:</div>
                                                <?php  

                                                      //$addons->charge=0;
                                                      $s=$add_charge;

                                                      //$a=$addons->charge + $addons1->charge + $addons2->charge + $addons3->charge;
                                                       $b=$charge->charge;
                                                       $c=$price;
                                                       $total=$s+$b+$c;

                                                       ?>

                                                       <div class="rate_amount extra_pad">Rs.<?php echo $s;?>/-</div>

                                                
                                                          <div class="rate_subhead rate_total extra_pad">Total</div>
                                                          <div class="rate_amount rate_total extra_pad">Rs.<?php echo $total;?>/-</div>
                                                


                                                  <!--   ?>  -->
                                                

                                                <?php  //$s=$addons->charge+$addons1->charge+$addons2->charge+$addons3->charge;?>
                                                <!-- <div class="rate_amount extra_pad">Rs.<?php echo $s;?>/-</div>

                                                
                                                <div class="rate_subhead rate_total extra_pad">Total</div> -->
                                                <?php 


                                                  // $a=$addons->charge + $addons1->charge + $addons2->charge + $addons3->charge;
                                                  // $b=$charge->charge;
                                                  // $c=$price;
                                                  // $total=$a+$b+$c;

                                                ?>
<!--                                                 <div class="rate_amount rate_total extra_pad">Rs.<?php echo $total;?>/-</div> -->
                                             </div>

                                          </div>
                                          
                                          






                                       
                                          <div class="col-md-1"></div>
                                       </div>
                                       <!-- </div> -->
                                       <input type="hidden" name="temp_id" id="temp_id" value="<?php echo $insert_id; ?>">
                                      
                                       <div class="btn_align wizard_measure padd_top ">
                                         
                                          <button  id="add_cart" class="btn btn-primary nextBtn btn-lg enq_but hg_own" type="button">checkout</button>
                                        <div class="errormsg3" style="color:red"></div>
                                       </div> 

                                    </div>
                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>