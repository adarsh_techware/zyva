

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8">
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="http://getbootstrap.com/favicon.ico">
      <title>Zyva</title>
      <!-- Bootstrap core CSS -->
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="css/animations.css" rel="stylesheet">
      <link href="css/animate.css" rel="stylesheet">
      <link href="css/animations.css" rel="stylesheet">
      <link href="css/hover-min.css" rel="stylesheet">
      <link href="css/owl.carousel.css" rel="stylesheet">
      <link href="css/owl.theme.css" rel="stylesheet">
      <!-- Custom css -->
      <link href="css/style.css" rel="stylesheet">
      <link href="css/magnific-popup.css" rel="stylesheet">
      <!-- Custom Fonts -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
   </head>
   <body>
      <!-- nav bar -->
      <nav class="navbar navbar-default navbar-fixed-top inner_nav home_bg">
         <div class="container">
            <div class="main_nav">
               <div class="col-md-2 col-sm-6 col-xs-12">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                     <a class="navbar-brand inner" href="index.html"><img class="hd_logo" src="images/logo.png"></a>
                  </div>
               </div>
               <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="col-md-10  col-sm-6 col-xs-12">
                  <div class="womes_add">
                     <div class="col-md-12  col-sm-12 col-xs-12">
                        <div class="col-md-6  col-sm-12 col-xs-12">
                           <input type="text" class="search_dress" placeholder="Search for Products,Brands and More">
                           <span class="search_icon"><img src="images/search_icon.png"></span>
                        </div>
                        <div class="col-md-3  col-sm-6 col-xs-12 pad_lft">
                         <div class="shoping_bag_out" data-toggle="modal" data-target="#myModal_pop">
                              <div class="shoping_bag">
                                 <img src="images/bag.png">
                                 <div class="item_shop">0</div>
                              </div>
                              <p>Shopping Bag</p>
                           </div>
                        </div>
                        <div class="col-md-3  col-sm-6 col-xs-12">
                           <div class="acnt_nam">Hi, Girinathan</div>
                           <div class="shoping_bag_out chge_rht">
                              
                              <div class="my_acount pad_lft">
                                 <div class="dropdown ">
                                    <button class="btn dropdown-toggle acoubt my_acnt" type="button" data-toggle="dropdown"><img src="images/pro_pic.png">
                                    <span class="carets pos_drop"><i class="fa fa-chevron-down low_font" aria-hidden="true"></i>
                                    </span></button>
                                    <ul class="dropdown-menu bg_dropcontent drop_ch_pos bg_img">
                                       <li><a href="#">My Account</a></li>
                                       <li><a href="#">My Wishlist</a></li>
                                       <li><a href="#">My Cart</a></li>
                                       <li><a href="#">Checkout</a></li>
                                       <li><a href="#">Logout</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="collapse navbar-collapse nav_link" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right nav_font">
                           <li><a class="cool-link" href="index.html">Home</a></li>
                           <li><a class="cool-link" href="women_page.html">Women</a></li>
                           <li><a class="cool-link" href="#">Celeb Style</a></li>
                           <li><a class="cool-link" href="#">New Look</a></li>
                           <li><a class="cool-link" href="#">Festive</a></li>
                           <li><a class="cool-link" href="#">Daily Wear</a></li>
                        </ul>
                     </div>
                  </div>
                  <!-- /.navbar-collapse -->
               </div>
               <!-- /.container-fluid -->
            </div>
         </div>
      </nav>
      <div class="starter-template">
      <div class="my_acount_tabs top_inr">
    <div class="col-md-12 pad_lft bg_full_cnt">
          
          <div class="col-md-6 pad_lft">
        
        <div class="map">
              
              
              </div>
        
        </div>
                  <div class="col-md-4">
        <div class="contact_form">
                      <h4>Contact us</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
            
            
            <div class="cont_form_out">
             <div class="form-group">
      <input type="text" class="form-control cnt_frm" id="usr" placeholder="Name">
    </div>
    <div class="form-group">
     
      <input type="password" class="form-control cnt_frm" id="pwd" placeholder="email">
    </div>
 <div class="form-group">
     
      <input type="password" class="form-control cnt_frm" id="pwd" placeholder="subject">
    </div>  
    
    <div class="form-group">
  <textarea class="form-control cnt_frm" rows="5" id="comment"  placeholder="your message here"></textarea>
</div>
   <button class="yelow_but yellow_clr yello_low">Send</button>
      </div>
                     
    </div>
        
        </div>
        
          <div class="col-md-2">  </div>
          </div>
          
          </div>
      <!-- footer_block -->
      <div class="banner_fth">
         <div class="container">
            <div class="row">
               <div class="col-md-2 col-sm-12 col-xs-12">
               </div>
               <div class="col-md-8 col-sm-12 col-xs-12">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="foot_img_outer">
                           <div class="foot_img">
                              <img src="images/phones.png">
                           </div>
                        </div>
                        <div class="foot_b contnt">
                           <h4>Contact Us</h4>
                           <ul class="ft_link">
                              <li>Tel:+91 985 9548</li>
                              <li>Tel:+91 985 9548</li>
                              <li>support@zyva.com</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="foot_img_outer">
                           <div class="foot_img">
                              <img src="images/location.png">
                           </div>
                        </div>
                        <div class="foot_b">
                           <h4>Visit Us</h4>
                           <ul class="ft_link">
                              <li>Zyva Fashions</li>
                              <li> Market Road,Kakanad</li>
                              <li>Kochi </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="foot_img_outer">
                           <div class="foot_img">
                              <img class="acnt" src="images/acount.png">
                           </div>
                        </div>
                        <div class="foot_b">
                           <h4> Follow Us</h4>
                           <ul class="ft_link">
                              <li>www.zyva.co.in</li>
                              <li>
                                 <div class="social_icon">
                                    <ul class="social">
                                       <li><i class="fa fa-facebook fa-lg lght_rse" aria-hidden="true"></i></li>
                                       <li><i class="fa fa-twitter fa-lg lght_rse" aria-hidden="true"></i>
                                       </li>
                                       <li><i class="fa fa-linkedin fa-lg lght_rse" aria-hidden="true"></i>
                                       </li>
                                    </ul>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-2 col-sm-12 col-xs-12">
               </div>
            </div>
            <div class="col-md-12 col-sm-6 col-xs-12">
               <div class="col-md-2 col-sm-6 col-xs-12"></div>
               <div class="col-md-8 col-sm-6 col-xs-12 no_padng">
                  <div class="footer_navs">
                     <ul class="nav navbar-nav navbar-right nav_font foot_rht">
                        <li><a class="cool-link" href="home.html">About Us</a></li>
                        <li><a class="cool-link" href="#">Contact Us</a></li>
                        <li><a class="cool-link" href="#">Terms </a></li>
                        <li><a class="cool-link" href="#"> Policies</a></li>
                        <li><a class="cool-link" href="#">Download Measurement Form</a></li>
                     </ul>
                  </div>
                  <div class="col-md-12 col-sm-6 col-xs-12">
                     <div class="col-md-4 col-sm-6 col-xs-12"></div>
                     <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="footer_navs_cards">
                           <ul>
                              <li><img src="images/payment_a.png"></li>
                              <li><img src="images/payment_b.png"></li>
                              <li><img src="images/payment_c.png"></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-6 col-xs-12"></div>
                  </div>
                  <div class="col-md-12 col-sm-6 col-xs-12">
                     <div class="col-md-3 col-sm-6 col-xs-12"></div>
                     <div class="col-md-6">
                        <div class="footer_navs_cards">
                           <p>zyva.co.in 2016.All Rights Reserved</p>
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12"></div>
                  </div>
               </div>
               <div class="col-md-2"></div>
            </div>
         </div>
      </div>
      <!-- footer_block end -->
      <!-- Modal login -->
      <div class="modal fade" id="myModal" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-body login_modal">
                  <div class="col-md-12 no_padng">
                     <div class="col-md-6 no_padng">
                        <div class="login_section1">
                           <div class="padng_outer">
                              <img class="log_zyva" src="images/zya_logo_blck.png">
                              <input type="text" class="text_box" placeholder="Email">
                              <input type="text" class="text_box" placeholder="password">
                              <button class="log_but">Login</button>
                              <p class="forgot_pass">Forgot password ?</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 no_padng">
                        <div class="login_section2">
                           <div class="padng_outer">
                              <button type="button" class="close" data-dismiss="modal"> <img src="images/modal_close.png"></button>
                              <div class="new_user">
                                 <p>New User</p>
                                 <h4>register now</h4>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal login end -->
          
          
          
           <!-- popover -->
      
<!-- Modal -->
<div class="modal fade" id="myModal_pop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Modal</h4>
      </div>
      <div id="calendar" class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->!--  popover end -->   
          
          
          
      <!-- Modal register -->
      <div class="modal fade" id="myModal_reg" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-body login_modal">
                  <div class="reg_modals">
                     <div class="col-md-12 no_padng">
                        <div class="lft_reg">
                           <div class="col-md-6 no_padng">
                              <div class="padng_outer pad_bot">
                                 <img class="log_zyva" src="images/zya_logo_blck.png">
                                 <input type="text" class="text_box" placeholder="First Name">
                                 <input type="text" class="text_box" placeholder="Telephone">
                                 <input type="text" class="text_box no_margn" placeholder="Password">
                              </div>
                           </div>
                           <div class="col-md-6 no_padng">
                              <div class="padng_outer">
                                 <button type="button" class="close" data-dismiss="modal"> <img src="images/modal_close.png"></button>
                                 <div class="reg_form">
                                    <input type="text" class="text_box" placeholder="Last Name">
                                    <input type="text" class="text_box" placeholder="Email">
                                    <input type="text" class="text_box" placeholder="Confirm Password">
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12 no_padng">
                              <div class="col-md-6">
                                 <div class="margn_out">
                                    <div class="check_outer">
                                       <div>
                                          <input id="checkbox-1" class="checkbox-custom" name="checkbox-1" type="checkbox" checked>
                                          <label for="checkbox-1" class="checkbox-custom-label"> I Accept the Terms and Conditions </label>
                                       </div>
                                       <div>
                                          <input id="checkbox-2" class="checkbox-custom" name="checkbox-2" type="checkbox">
                                          <label for="checkbox-2" class="checkbox-custom-label"> Subscribe to Newsletter</label>
                                       </div>
                                    </div>
                                    <div class="check_inner">
                                       <button class="log_but">Sign in</button>
                                       <p class="forgot_pass">Forgot password ?</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="acount">
                                    <p>Already have an Account</p>
                                    <h4>Log In here</h4>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal register end -->
      <!-- Bootstrap core JavaScript
         ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="js/jquery.js"></script>
      <script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
      <script src="js/bootstrap.js"></script>
      <!-- BackToTop Button -->
      <a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;"></a>
      <script src="js/custom.js"></script>
      <script src="js/jquery.magnific-popup.min.js"></script>
      <script src="js/owl.carousel.js"></script>
   </body>
</html>
