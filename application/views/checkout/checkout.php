<script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.js"></script>
<div class="check_out_outer">
   <div class="container">
      <div class="row checkout_page_start">
         <!-- ..............ORDER SUMMARY SECTION START.................. -->
         <div class="col-md-4 col-sm-12 col-xs-12">
            <form method="POST" id="myorder">
               <div class="order_summary_outer checkout_order" id="order_container">
                  <div class="order_summary_inner">
                     <h4 class="order_pad">order summary</h4>
                     <?php
                     $i=0;
                     $total=0;
                     foreach ($cartlist as $cart) {
                     ?>
                     <div class="orders" id="order">
                        <div class="row">
                           <div class="col-md-4 col-sm-6 col-xs-12">
                              <div class="order_dtl">
                                 <img src="<?php echo base_url('admin/'.$cart->product_image.'_201x302.jpg');?>">
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-6 col-xs-12" style="padding-left:0px;">
                              <div class="order_inf">
                                 <h4><?php echo $cart->product_name;?></h4>
                                 <input type="hidden"  name="product_id[]" value="<?php echo $cart->product_id;;?>" >
                                 <h4><?php echo $cart->quantity;?> x &#8377;<?php echo $cart->price;?></h4>
                                 <div class="dres_inf">
                                    <p class="cart_color">Color</p>
                                    <p class="clr_dre" style="background-image:url('<?php echo $cart->image;?>')"></p>
                                 </div>
                                 <div class="dres_inf">
                                    <p>Size</p>
                                    <p class="cart_size"><?php echo $cart->size_type;?></p>
                                 </div>
                                 <?php
                                    if($cart->stock_status>0){?>
                                 <p class=""><?php if($cart->stock_status=='1') echo 'Out of Stock'; else echo 'Stock Limited';?></p>
                                 <?php } ?>
                              </div>
                           </div>
                        </div>
                        <div class="remove_but">
                           <div class="row">
                              <!-- <div class="col-md-3 col-sm-6 col-xs-12 pad_rht">
                                 <div class="edit">
                                 
                                           <i class="fa fa-pencil gray_clr" aria-hidden="true"></i>
                                 
                                            <p>
                                 
                                              <a href="<?php echo base_url('product_info/'.$cart->prod_display);?>"><button  class="remove_btn1" type="button"> edit</button></a>
                                 
                                            </p>
                                 
                                         </div>
                                 
                                      </div> -->
                              <div class="col-md-3 col-sm-6 col-xs-12 pad_rht pad_lft">
                                 <div class="remove">
                                    <i class="fa fa-times gray_clr" aria-hidden="true"></i>
                                    <p class="remove_pdt" data-id="<?php echo $cart->id;?>"> &nbsp;remove</p>
                                 </div>
                              </div>
                              <div class="col-md-6 col-sm-6 col-xs-12"></div>
                           </div>
                        </div>
                     </div>
                     <?php 
                        if($cart->stock_status!=1){
                        
                        $q=$cart->quantity;
                        
                        $total+= $cart->price*$q;
                        
                        }
                        
                        $i++; 
                        
                        }
                        
                        ?>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="sub_tot order_pad">Sub Total</div>
                        </div>
                        <div class="col-md-6">
                           <div class="sub_tot total_rup">
                              &#8377; <?php echo $total;?>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <!-- ..............ORDER SUMMARY SECTION END.................. -->
         <!-- ..............CHECKOUT TAB  START.................. -->
         <div class="col-md-8 col-sm-12 col-xs-12">
            <!-- .....TAB HEADING..... -->
            <div class="checkout_tab">
               <div class="tab_heading">
                  <div class="billing_add bills">
                     <div class="count">
                        <p class="count_one">1</p>
                     </div>
                     <p class="tab_heads tab_head_one">Billing Address</p>
                  </div>
                  <div class="shipping_pay bill_next">
                     <div class="count">
                        <p class="count_two">2</p>
                     </div>
                     <p class="tab_heads tab_head_two">Shipping and Payment</p>
                  </div>
                  <div class="order_info ship_next">
                     <div class="count">
                        <p class="count_three">3</p>
                     </div>
                     <p class="tab_heads tab_head_three">Order Information</p>
                  </div>
               </div>
               <!-- ............TAB DETAILS............ -->
               <!-- *********.....BILLING ADDRES START........********** -->
               <form method="post" name="payment_sec" id="payment_sec" data-parsley-validate="" class="tabform validate adredd_val">
                  <fieldset class="check_tab_bill">
                     <?php
                        if(count($address_list)==0){?>
                     <input type="button" data-toggle="modal" data-target="#delevery_add_modal" value="Add new Address">
                     <?php } else {?>
                     <div class="row">
                        <div class="col-md-6">
                           <h2 class="tab_cont_title align_left">Billing Address</h2>
                        </div>
                        <div class="col-md-6" >
                           <h2 class="tab_cont_title align_right" data-toggle="modal" 
                              data-target="#delevery_add_modal">My Address</h2>
                        </div>
                     </div>
                     <div class="rows">
                        <div class="checkout_billing">
                           <div class="text_boxs">
                              <input type="text" name="firstname"  value="<?php echo $address->firstname; ?>" placeholder="First name" 
                                 data-parsley-trigger="change" 
                                 data-parsley-pattern="^[a-zA-Z\  \/]+$" 
                                 required="" data-parsley-required-message="Please insert your first name."
                                 data-parsley-errors-container="#first_name"/>
                              <div id="first_name" class="custom_parse_error"></div>
                           </div>
                           <div class="text_boxs">
                              <input type="text" name="lastname" value="<?php echo $address->lastname; ?>" placeholder="Last name" 
                                 data-parsley-trigger="change" 
                                 data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                 required="" data-parsley-required-message="Please insert your last name."
                                 data-parsley-errors-container="#last_name"/>
                              <div id="last_name" class="custom_parse_error"></div>
                           </div>
                           <div class="text_boxs">
                              <input type="text" name="address_1"  value="<?php echo $address->address_1; ?>" placeholder="Address Line 1" 
                                 data-parsley-trigger="change" required="" 
                                 data-parsley-required-message="Please insert address line 1."
                                 data-parsley-errors-container="#add_line1"/>
                              <div id="add_line1" class="custom_parse_error"></div>
                           </div>
                           <div class="text_boxs">
                              <input type="text" name="address_2"  value="<?php echo $address->address_2; ?>" placeholder="Address Line 2" 
                                 data-parsley-trigger="change" required="" 
                                 data-parsley-required-message="Please insert address line 2."
                                 data-parsley-errors-container="#add_line2"/>
                              <div id="add_line2" class="custom_parse_error"></div>
                           </div>
                        </div>
                        <div class="checkout_billing">
                           <?php //if($statedetails > 0) { ?>
                           <div class="text_boxs">
                              <input type="text" name="state_id" id="state_id"
                                 value="<?php echo $address->state_id;?>" placeholder="State" 
                                 data-parsley-trigger="change" required="" 
                                 data-parsley-required-message="Please insert state."
                                 data-parsley-errors-container="#states"/>
                              <div id="states" class="custom_parse_error"></div>
                           </div>
                           <div class="text_boxs">
                              <input type="text" name="city_id" id="city_dropdown"
                                 value="<?php echo $address->city_id;?>" placeholder="City" 
                                 data-parsley-trigger="change" required="" 
                                 data-parsley-required-message="Please insert city."
                                 data-parsley-errors-container="#city"/>
                              <div id="city" class="custom_parse_error"></div>
                           </div>
                           <?php //} ?>
                           <div class="text_boxs">
                              <input type="text" name="Zip"  value="<?php echo $address->Zip; ?>" placeholder="Zip Code" 
                                 data-parsley-trigger="change" 
                                 data-parsley-minlength="5" data-parsley-maxlength="8" data-parsley-pattern="^[0-9\  \/]+$"  
                                 required="" data-parsley-required-message="Please insert zip code."
                                 data-parsley-errors-container="#zip"/>
                              <div id="zip" class="custom_parse_error"></div>
                           </div>
                           <div class="text_boxs">
                              <input type="text" name="telephone"  value="<?php echo $address->telephone; ?>" 
                                 placeholder=" Telephone" data-parsley-trigger="change" 
                                 data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$"
                                 required="" data-parsley-required-message="Please insert phone number."
                                 data-parsley-errors-container="#phone"/>
                              <div id="phone" class="custom_parse_error"></div>
                           </div>
                        </div>
                     </div>
                     <!-- <div class="checkout_billing_btn"> -->
                     <h2 class="tab_cont_title pad_tops">Delivery Address</h2>
                     <div class="tab-pane active" id="already">
                        <?php
                           $i = 0;
                           
                           foreach ($address_list as $rs) {
                           
                           $class = $i%2==1?'no_margins_right':'';
                           
                           $checked = $rs->default_id==1?'checked="checked"':'';
                           
                           ++$i;
                           
                           ?>
                        <div class="address_exist <?php echo $class; ?>">
                           <div class="address_exist_check">
                              <input type="radio" name="address_id" 
                                 id="delivery_address_id_<?php echo $rs->address_id; ?>" 
                                 value="<?php echo $rs->address_id; ?>" 
                                 class="delivery_address_class" <?php echo $checked; ?> 
                                 class="parsley-validated" data-required="true" />
                              <label for="address_id_<?php echo $rs->address_id; ?>"></label>
                           </div>
                           <div class="exist_pad">
                              <p><?php echo ucfirst($rs->firstname).' '.$rs->lastname; ?></p>
                              <?php if($rs->company!=''){?>
                              <p><?php echo $rs->company; ?></p>
                              <?php } ?>
                              <p><?php echo $rs->address_1; ?></p>
                              <?php if($rs->address_2!=''){?>
                              <p><?php echo $rs->address_2; ?></p>
                              <?php } ?>
                              <p><?php echo $rs->city_name; ?></p>
                              <p><?php echo $rs->state_name; ?>-<?php echo $rs->Zip; ?></p>
                           </div>
                        </div>
                        <?php } 
                           if(count($address_list)==0){?>
                        <h2 class="tab_cont_title align_right" data-toggle="modal" 
                           data-target="#delevery_add_modal">Add New Address</h2>
                        <?php } ?>
                        <?php if(count($address_list)>0){?>
                        <div id="newsletter" class="custom_check_parse_error"></div>
                        <input type="button" name="next" class="next action-button bill_next" value="Next">
                        <?php } else {?>
                        <input type="button" name="next" class="action-button" value="Next" id="select_new">
                        <?php } ?>
                     </div>
                     <?php } ?>
                  </fieldset>
                  <!-- *********.....BILLING ADDRES END........********** -->
                  <!-- *********.....SHIPPING PAYMENT START........********** -->
                  <fieldset  class="check_tab_ship">
                     <h2 class="tab_cont_title">Shipping Method</h2>
                     <div class=" check_box" id="check1">
                        <div class="check_lft" >
                           <input id="checkbox-3" class="checkbox-custom shipping" required="" name="checkbox-2" type="radio" value="<?php echo $shipping->charge; ?>" >
                           <label for="checkbox-3" class="checkbox-custom-label"><?php echo $shipping->label; ?></label>
                        </div>
                        <div class="price" id="price"> &#8377; <?php echo $shipping->charge; ?></div>
                     </div>
                     <div class="check_box" >
                        <div class="check_lft">
                           <input id="checkbox-4" class="checkbox-custom shipping" name="checkbox-2" type="radio" value="<?php echo $shipping1->charge; ?>" >
                           <label for="checkbox-4" class="checkbox-custom-label"><?php echo $shipping1->label; ?></label>
                        </div>
                        <div class="price"> &#8377; <?php echo $shipping1->charge; ?></div>
                     </div>
                     <h2 class="tab_cont_title pad_tops">Is it a Gift ?</h2>
                     <div class="check_box">
                        <div class="check_lft"> 
                           <input id="checkbox-5" class="checkbox-custom gift" name=""  type="checkbox" value="<?php echo $shipping2->charge; ?>"  >
                           <label for="checkbox-5" class="checkbox-custom-label">Yes, Include a Gift Packaging</label>
                        </div>
                        <div class="price"> &#8377; <?php echo $shipping2->charge; ?></div>
                     </div>
                     <!-- <h2 class="tab_cont_title pad_tops">Payment Method</h2>
                        <div class="check_box">
                        
                          <div class="check_lft">
                        
                            <input id="checkbox-6" class="checkbox-custom" required="" name="payment_type" type="checkbox" checked="checked" readonly value="COD">
                        
                            <label for="checkbox-6" class="checkbox-custom-label"> Cash on Delivery</label>
                        
                          </div>
                        
                        </div> -->
                     <input type="hidden" id="shipping_charge" name="shipping_charge" />
                     <input type="hidden" id="enable_gift" name="enable_gift" value="0" />
                     <input type="hidden" id="gift_charge" name="gift_charge" value="0" />
                     <input type="hidden" name="total_rate" id="total_rate" value="<?php echo $total; ?>">
                     <div class="tab_sub_btns">
                        <div class="ship_method_alert"></div>
                        <input type="button" name="previous" class="previous action-button bills" value="Previous" />
                        <input id="next " type="button" name="next"  class="action-button next ship_next ship_next_id"  value="Next"/>
                     </div>
                  </fieldset>
                  <!-- *********.....SHIPPING PAYMENT END........********** -->
                  <!-- *********.....ORDER INFORMATION START........********** -->
                  <fieldset  class="check_tab_order">
                     <h2 class="tab_cont_title">Order Information</h2>
                     <div class="row">
                        <div class="col-md-2">
                           <div class="sub_tot">Sub Total</div>
                        </div>
                        <div class="col-md-3">
                           <div class="sub_tot total_rup">
                              &#8377; <?php echo $total;?>.00  
                           </div>
                        </div>
                        <div class="col-md-7"></div>
                     </div>
                     <div class="row">
                        <div class="col-md-2">
                           <div class="sub_tot">Grand Total</div>
                        </div>
                        <div class="col-md-3">
                           <div class="sub_tot total_rup">
                              <span>&#8377;</span><span id="abc"> 0</span>.00
                           </div>
                        </div>
                        <div class="col-md-7"></div>
                     </div>
                     <div class="check_box">
                        <div class="check_lft full_check">
                           <input id="checkbox-7" class="checkbox-custom place_check" name="" type="checkbox" >
                           <label for="checkbox-7"  class="checkbox-custom-label"> By placing an order you agree our Terms & Conditions & Privacy Policy </label>
                        </div>
                     </div>
                     <!-- showing error message when not select place order -->
                     <div class="place_order_alert"></div>
                     <input type="button" name="previous" class="previous action-button bill_next" value="Previous" />
                     <input id="pay" type="submit" name="submit"  class="submit action-button place_order place_btn" value="Place Order" />
                  </fieldset>
               </form>
               <!-- *********.....ORDER INFORMATION START........********** -->
            </div>
         </div>
         <!-- ..............CHECKOUT TAB  START.................. -->
      </div>
   </div>
</div>
<!-- MODAL FOR DELIVERY ADDRESS START -->
<div class="modal fade" id="delevery_add_modal" role="dialog">
   <div class="modal-dialog reg_mod_width">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body login_modal">
            <div class="reg_modals">
               <div class="col-md-12 no_padng">
                  <!-- <form method="POST" id="signup_form" autocomplete="off"> -->
                  <div class="lft_reg">
                     <div class="col-md-6 no_padng">
                        <div class="padng_outer_reg pad_bot">
                           <img class="" src="<?php echo base_url();?>assets/images/zya_logo_blck.png">
                        </div>
                     </div>
                     <div class="col-md-6 no_padng">
                        <div class="padng_outer_reg pad_bot">
                           <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url();?>assets/images/modal_close.png"></button>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="delivery_address">
                           <h3>ADDRESS LIST</h3>
                           <h4 id="address_error"></h4>
                           <div class="tab_cabin">
                              <ul class="tabrow">
                                 <li class="tab_one selected"><a href="#">Add New</a></li>
                                 <li class="tab_two"><a href="#">Already Exist</a></li>
                              </ul>
                              <!-- =============Tab 1- for Add new Address Start==========-->
                              <div class="add_new">
                                 <form name="address_new" id="address_new" method="post">
                                    <div class="rows">
                                       <div class="checkout_billing">
                                          <div class="text_boxs">
                                             <input type="text" name="firstname" placeholder="First name" 
                                                data-parsley-trigger="change" 
                                                data-parsley-pattern="^[a-zA-Z\  \/]+$" 
                                                required="" data-parsley-required-message="Please insert your first name."
                                                data-parsley-errors-container="#firstname"/>
                                             <div id="firstname" class="custom_parse_error"></div>
                                          </div>
                                          <div class="text_boxs">
                                             <input type="text" name="lastname" placeholder="Last name" 
                                                data-parsley-trigger="change" 
                                                data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                                required="" data-parsley-required-message="Please insert your last name."
                                                data-parsley-errors-container="#lastname"/>
                                             <div id="lastname" class="custom_parse_error"></div>
                                          </div>
                                          <div class="text_boxs">
                                             <input type="text" name="address_1" placeholder="Address Line 1" 
                                                data-parsley-trigger="change" 
                                                data-parsley-minlength="2" 
                                                required="" data-parsley-required-message="Please insert address line 1."
                                                data-parsley-errors-container="#addline1"/>
                                             <div id="addline1" class="custom_parse_error"></div>
                                          </div>
                                          <div class="text_boxs">
                                             <input type="text" name="address_2"  value="<?php echo $address->address_2; ?>" placeholder="Address Line 2" 
                                                data-parsley-trigger="change" 
                                                data-parsley-minlength="2" required="" data-parsley-required-message="Please insert address line 2."
                                                data-parsley-errors-container="#addline2"/>
                                             <div id="addline2" class="custom_parse_error"></div>
                                          </div>
                                       </div>
                                       <div class="checkout_billing">
                                          <?php //if($statedetails > 0) { ?>
                                          <div class="text_boxs">
                                             <input type="text" name="state_id" id="state_id"
                                                value="<?php echo $address->state_name;?>" placeholder="State" 
                                                data-parsley-trigger="change" required="" 
                                                data-parsley-required-message="Please insert state."
                                                data-parsley-errors-container="#state"/>
                                             <div id="state" class="custom_parse_error"></div>
                                          </div>
                                          <div class="text_boxs">
                                             <input type="text" name="city_id" id="city_dropdown"
                                                value="<?php echo $address->city_name;?>" placeholder="City" 
                                                data-parsley-trigger="change" required="" 
                                                data-parsley-required-message="Please insert city."
                                                data-parsley-errors-container="#cities"/>
                                             <div id="cities" class="custom_parse_error"></div>
                                          </div>
                                          <?php //} ?>
                                          <div class="text_boxs">
                                             <input type="text" name="Zip" placeholder="Zip Code" 
                                                data-parsley-trigger="change" 
                                                data-parsley-minlength="2" data-parsley-maxlength="8" data-parsley-pattern="^[0-9\  \/]+$"  
                                                required="" data-parsley-required-message="Please insert zip code."
                                                data-parsley-errors-container="#zipcode"/>
                                             <div id="zipcode" class="custom_parse_error"></div>
                                          </div>
                                          <div class="text_boxs">
                                             <input type="text" name="telephone" placeholder=" Telephone" 
                                                data-parsley-trigger="change" 
                                                data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$"
                                                required="" data-parsley-required-message="Please insert phone number."
                                                data-parsley-errors-container="#phoneno"/>
                                             <div id="phoneno" class="custom_parse_error"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="delivery_sub">
                                       <input type="button" class="delivery_btn" value="Submit" id="new_address" />
                                    </div>
                                 </form>
                              </div>
                              <!-- =============Tab 1- for Add new Address End==========-->
                              <!-- =============Tab 2- for Add Already Exist Start==========-->
                              <div class="already" style="min-height:330px;">
                                 <?php
                                    $i = 0;
                                    foreach ($address_list as $rs) {
                                    $class = $i%2==1?'no_margin_right':'';
                                    ++$i;
                                    ?>
                                 <div class="address_exists <?php echo $class; ?>">
                                    <div class="address_exist_check">
                                       <input type="radio" name="address_id" 
                                          id="address_id_<?php echo $rs->address_id; ?>" 
                                          value="<?php echo $rs->address_id; ?>" 
                                          class="address_class" />
                                       <label for="address_id_<?php echo $rs->address_id; ?>"></label>
                                    </div>
                                    <div class="exist_pad">
                                       <p><?php echo ucfirst($rs->firstname).' '.$rs->lastname; ?></p>
                                       <?php if($rs->company!=''){?>
                                       <p><?php echo $rs->company; ?></p>
                                       <?php } ?>
                                       <p><?php echo $rs->address_1; ?></p>
                                       <?php if($rs->address_2!=''){?>
                                       <p><?php echo $rs->address_2; ?></p>
                                       <?php } ?>
                                       <p><?php echo $rs->city_name; ?></p>
                                       <p><?php echo $rs->state_name; ?>-<?php echo $rs->Zip; ?></p>
                                    </div>
                                 </div>
                                 <?php } ?>
                              </div>
                              <!-- =============Tab 1- for Already Exist End==========-->
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- </form> -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- MODAL FOR DELIVERY ADDRESS END -->
     
          
          
          <script>
          $('.shipping').on('click',function(){
            var total = '<?php echo $total; ?>';
            var charge = $(this).val();
            $('#shipping_charge').val(charge);
            if($("#checkbox-5").is(':checked')){
              var gift = $("#checkbox-5").val();
              var sum = parseInt(total)+parseInt(charge)+parseInt(gift);
              $('#total_rate').val(sum);
              $("#abc").html(sum);
              $('#abc1').val(sum);
            } else {
               var sum = parseInt(total)+parseInt(charge);
              $('#total_rate').val(sum);
              $("#abc").html(sum);
              $('#abc1').val(sum);
            }

          })

  $('#checkbox-5').on('click',function(){
    if($("#checkbox-5").is(':checked')){
      $('#enable_gift').val(1);
      var total = '<?php echo $total; ?>';
      var shipping = $('#shipping_charge').val(); 
      var gift = $(this).val();
      $('#gift_charge').val(gift);
      var sum = parseInt(total)+parseInt(shipping)+parseInt(gift);
      $('#total_rate').val(sum);
      $("#abc").html(sum);
      $('#abc1').val(sum);
    } else {
      $('#enable_gift').val(1);
      var total = '<?php echo $total; ?>';
      var shipping = $('#shipping_charge').val();
      var gift = $(this).val();
      $('#gift_charge').val(0);
      var sum = parseInt(total)+parseInt(shipping);
      $('#total_rate').val(sum);
      $("#abc").html(sum);
      $('#abc1').val(sum);
    }

  })

  $('#checkbox-6').on('click',function(){
    $(this).attr('checked','checked');
  })


$(".add_new").show();
$(".already").hide();
$(".tab_one").addClass('selected');
$(".tab_two").removeClass('selected');
$(".tab_one").click(function(e) {
   $(".already").hide();
   $(".add_new").show();
   $(".tab_one").addClass('selected');
   $(".tab_two").removeClass('selected');
});
$(".tab_two").click(function(e) {
   $(".already").show();
   $(".add_new").hide();
   $(".tab_two").addClass('selected');
   $(".tab_one").removeClass('selected');
});

var order_container = $('#order_container');
$('#a').mCustomScrollbar({        
});

$('#order_container').mCustomScrollbar({ 
      theme:"dark"        
});
</script>
