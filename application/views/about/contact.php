


<body onload="initialize()">
<div class="my_acount_tabs top_inr">

  <div class="col-md-12 pad_lft bg_full_cnt">



  <div class="col-md-6 pad_lft">
    <!-- <div class="map" id="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3929.0349565032434!2d76.36118095069051!3d10.01397127550521!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b080d40109a4bd3%3A0xd2508d8c0feb4555!2sTechware+Solution!5e0!3m2!1sen!2sin!4v1491393191105" width="100%" height="530" frameborder="0" style="border:0" allowfullscreen></iframe>
        <div id="marker-tooltip"></div>
    </div> -->

  <div class="map" id="googleMap" style="width: 100%; height:530px"></div>
  </div>

  

  <form method="post" id="contact_form" data-parsley-validate="true" class="validate">

    <div class="col-md-4">

      <div class="contact_form">

      <h4>Contact us</h4>

      <p>we are happy to answer any questions you have or provide you with an estimate. Just send 

        us a message in the form below with any questions you may have.</p>



        <div class="cont_form_out">



          <div class="form-group">

            <input type="text" class="form-control cnt_frm" id="usr" placeholder="Name" 

            name="name" data-parsley-trigger="change" required=""

            data-parsley-required-message="Please insert name." 

            data-parsley-errors-container="#name">



            <div class="acc_errors" id="name"></div>

          </div>



          <div class="form-group">

              <input type="email" class="form-control cnt_frm" id="pwd" placeholder="email" 

              name="email" data-parsley-trigger="change" required=""

              data-parsley-required-message="Please insert email." 

              data-parsley-errors-container="#email">



              <div class="acc_errors" id="email"></div>

          </div>



          <div class="form-group">

              <input type="text" class="form-control cnt_frm" id="pwd" placeholder="subject" 

              name="subject" data-parsley-trigger="change" required=""

              data-parsley-required-message="Please insert subject." 

              data-parsley-errors-container="#sub">

              <div class="acc_errors" id="sub"></div>

          </div>  



          <div class="form-group">

              <textarea class="form-control cnt_frm" rows="5" id="comment"  placeholder="your message here" 

              name="msg"data-parsley-trigger="change" required=""

              data-parsley-required-message="Please insert message." 

              data-parsley-errors-container="#msg"></textarea>

              <div class="acc_errors" id="msg"></div>

          </div>



         <div class="button_carrier"> 

            <button class="yelow_but yellow_clr yello_low" id="sene_mails" type="button">Send</button>

            <div class="contact_msg"></div>

          </div>



        



        </div>





      </div>

    </div>

  </form>



  <div class="col-md-2">  </div>

  </div>



</div>


<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyB1br9lwKFyEpCnS5elLan_90CCsYeak6I"></script>

<script>

function initialize() {


//replace this variable with the json you generate in the google maps api wizard tool
//Styles Start
 var styles = [ { "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [ { "color": "#444444" } ] }, { "featureType": "landscape", "elementType": "all", "stylers": [ { "color": "#f2f2f2" } ] }, { "featureType": "poi", "elementType": "all", "stylers": [ { "visibility": "on" } ] }, { "featureType": "poi", "elementType": "geometry.fill", "stylers": [ { "saturation": "-100" }, { "lightness": "57" } ] }, { "featureType": "poi", "elementType": "geometry.stroke", "stylers": [ { "lightness": "1" } ] }, { "featureType": "poi", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road", "elementType": "all", "stylers": [ { "saturation": -100 }, { "lightness": 45 } ] }, { "featureType": "road.highway", "elementType": "all", "stylers": [ { "visibility": "simplified" } ] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "featureType": "transit", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "transit.station.bus", "elementType": "all", "stylers": [ { "visibility": "on" } ] }, { "featureType": "transit.station.bus", "elementType": "labels.text.fill", "stylers": [ { "saturation": "0" }, { "lightness": "0" }, { "gamma": "1.00" }, { "weight": "1" } ] }, { "featureType": "transit.station.bus", "elementType": "labels.icon", "stylers": [ { "saturation": "-100" }, { "weight": "1" }, { "lightness": "0" } ] }, { "featureType": "transit.station.rail", "elementType": "all", "stylers": [ { "visibility": "on" } ] }, { "featureType": "transit.station.rail", "elementType": "labels.text.fill", "stylers": [ { "gamma": "1" }, { "lightness": "40" } ] }, { "featureType": "transit.station.rail", "elementType": "labels.icon", "stylers": [ { "saturation": "-100" }, { "lightness": "30" } ] }, { "featureType": "water", "elementType": "all", "stylers": [ { "color": "#d2d2d2" }, { "visibility": "on" } ] } ];

//Styles End
   //Create a styled map using the above styles
   var styledMap = new google.maps.StyledMapType(styles,{name: "Styled Map"}); 

   var mapProp = { 
      center:new google.maps.LatLng(10.1151,76.3282),//set the centre of the map. In my case it is the same as the position of the map pin.
      zoom:14,
      mapTypeId:google.maps.MapTypeId.ROADMAP
   };

   var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

   //Set the map to use the styled map
   map.mapTypes.set('map_style', styledMap);
   map.setMapTypeId('map_style');
   // var contentString = '<div id="google-popup">'+
   //          '<p>Kadungalloor Road</p>' +
   //          '<p>Muppathadom</p>' +
   //          '<p>Edayar</p>' +
   //          '<p>Kerala 683110</p>' +
   //          '<p>+91 952 603 9555</p>' +
   //          '<p>sales@zyva.in</p>' +

   //          '</div>';

   var contentString = '<div id="google-popup">'+
            '<p>DOOR NO. 66/6500,</p>' +
            '<p>2nd Floor, Matsun Towers,</p>' +
            '<p>A K Seshadn Road</p>' +
            '<p>Ernakulam, Kochi - 682 011</p>' +
            '<p>+91 952 603 9555</p>' +
            '<p>sales@zyva.in</p>' +

            '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

   //Create a marker pin to add to the map
   var image = 'https://zyva.in/assets/images/marker.png';
   var marker;
   marker = new google.maps.Marker({
      position: new google.maps.LatLng(10.1151,76.3282),//set the position of the pin
      map: map,
      title: "Derby",
      icon: image, //if you comment this out or delete it you will get the default pin icon.
      animation:google.maps.Animation.DROP
   });

    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });

    if ($map.find('.gm-style').length === 0) {
      $map.find('.gm-style-iw').parent().addClass('gm-style-iw-container');
    }

}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
</body>
