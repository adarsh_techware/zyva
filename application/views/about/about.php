<div class="starter-template">

    <div class="my_acount_tabs top_inr">

        <div class="about_page about_min_height">

            <div class="container">

                <div class="col-md-6">

                    <div class="abut_img">

                      <img src="<?php echo base_url();?>assets/images/abt.png">

                    </div>

                </div>

                <div class="col-md-6">   

                    <div class="flr-wrap">

                        <a class="buttons active" data-rel="#content-a" href="#">

                            <img src="<?php echo base_url();?>assets/images/quality.png">

                            <p>quality</p>

                        </a>

                        <a class="buttons" data-rel="#content-b" href="#">

                            <img src="<?php echo base_url();?>assets/images/perfection.png">

                            <p>perfection</p>

                        </a>

                        <div class="flr-inner">

                            <div class="cont" id="content-a">



                              <h5>We always use highly durable fabrics, unmatched quality 
                                of customization, versatile designs.</h5>

                              <p>Maryam Apparels is a women’s online fashion Kurtis store carrying the hottest trends and unique

                                fashion finds at unbeatable prices. So whatever your style Maryam Apparels is the final destination

                                for your fashion needs as you can stay up to date with all the latest trends of Kurtis in just a click

                                of button. Maryam Apparels helps you to find attire for casual occasions, weddings and engagements, home

                                and office wear, festival attire and glamorous party outfits. Our design philosophy has been

                                moulded to deliver customized apparel to our customers, at their doorsteps, across the globe.

                                Maryam Apparels has an experienced in-house team of designers, tailors and pattern makers to get your

                                Kurtis personalized. We are distinguished by our sense of design, excellent fit and finish.

                                Meticulous observation to keep up standardization of sizes and material quality is the hallmark

                                of our brand.</p>

                              <!-- <p class="know_more">want to know more</p> -->
                              <p></p>


                            </div>

                            <div class="cont" id="content-b">



                                <h5>We offer extraordinary designs with absolute perfection and unparalleled custom tailoring
                                    service.</h5>



                                 <p>We design and craft the kurtis with superior quality fabrics under the guidance of our skilled

                                    team of designers who work towards stitching perfection. This requires high degree of agility

                                    and quick precision. We specialize in stunning ethnic kurtis for all women’s who love to

                                    sparkle. All our kurtis are designed to provide the best value, and we are specialists in the

                                    area of larger size kurtis collection. Don't waste your time to browse through tons of kurtis

                                    for just a handful of good stuff. Maryam Apparels handpicked kurtis collection will make you to buy it all,

                                    in half the time. Which is useful for both the individuals and resellers.</p>

                                <p></p>

                            </div>

                        </div>

                    </div> 



                </div>



            </div>

        </div>   

    </div>

</div>



