

    <body>
        <!-- nav bar -->
        <!--     <div class="main"> -->
        <div class="banner">

        <div class="starter-template">


            <!-- slider  -->
            <div class="container">
                <div class="col-md-5">
                    <div class="slide_section">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators slider_carousel">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <a href="#"><img class="bg frst_img" src="<?php echo base_url();?>assets/images/model_1.png" /></a>
                                </div>
                                <div class="item">
                                    <a href="#"><img class="bg" src="<?php echo base_url();?>assets/images/modalss_b.png"/></a>
                                </div>
                                <div class="item">
                                    <a href="#"><img class="bg" src="<?php echo base_url();?>assets/images/modalss_c.png" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="outer_des">
                        <div class="text">
                            <h1>FOR WOMEN</h1>
                            <h5><span class="bord_span">ALL THE TRENDS </span> YOU NEED THE SEASON</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has</p>
                            <a href="#"> <button class="zyva_but box foo">read more</button></a>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-12"></div>
                </div>
            </div>
            <!-- slider  end-->


            <!-- fashion_block  -->
            <div class="banner_scnd">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                            <div class="fshion_store_out">
                                <div class="fshion_store">
                                    <div class="circle_out_full">
                                        <div class="circle_out">
                                            <div class="circle">20% </div>
                                        </div>
                                    </div>
                                    <div class="circle_scnd">
                                        <h1>FASHIONS</h1>
                                        <H3>For Women</H3>
                                    </div>
                                    <p>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                    </p>
                                    <a href="#"  <button class="blck_read red_mre foot">read more</button></a>
                                </div>
                                <div class="dress_material">
                                    <img  class=" derss_a img-responsive" src="<?php echo base_url();?>assets/images/dres_pat.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="fshion_stre">
                                <div class="women_acesseries">
                                    <h4>Women accessories</h4>
                                    <hr class="bord_bot">
                                    </hr>
                                    <p>100 modal items</p>
                                </div>
                                <div class="dress_b">
                                    <img src="<?php echo base_url();?>assets/images/dress_pat_a.png" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fashion_block end -->



            <!-- sale_block  -->
            <div class="banner_thrd" id="myDiv">
                <div class="container">
                    <div class="row">
                        <div class=" col-lg-4 col-md-4 col-sm-12 col-xs-12 pad_rht" >
                            <div class="dress_materials">
                                <img class="img-responsive " src="<?php echo base_url();?>assets/images/dress_c.png">
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <div class="col-md-6">
                                <div class="super_sale">
                                    <div class="super_sale_inner">
                                        <h3>SUPER<br>
                                            SALE
                                        </h3>
                                        <h4>enjoy 20% off</h4>
                                        <p>with code # sample</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="sale_img">
                                    <div class="hover " style="padding:0px;">
                                        <div class="hovereffect">
                                            <img class="img-responsive"src="<?php echo base_url();?>assets/images/dress_d.png" alt="">
                                            <div class="overlay">
                                                <a class="info" href="inner.html">link here</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="arrivals">
                                    <div class="hover">
                                        <div class="hovereffect">
                                            <img class="img-responsive"src="<?php echo base_url();?>assets/images/dress_e.png" alt="">
                                            <div class="overlay">
                                                <a class="info" href="inner.html">link here</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 padding_none">
                                <div class="arival_new ">
                                    <div class="arival_new_inner">
                                        <h5>NEW ARRIVALS</h5>
                                        <h3>#outfit</h3>
                                        <h4><span class="gray_of">of </span>today</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- sale_block end -->



            <!-- derss_block  -->
            <div class="banner_frth">
                <div class="container">
                    <div class="row">
                        <div id="owl-demo">
                            <div class="item">
                                <div class="model_a">
                                    <div class="wrapper">
                                        <div class="plus_add"> +</div>
                                        <div class="tooltip">
                                            <h3>Kurthis</h3>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                            <p class="rd_mre">Read More</p>
                                        </div>
                                    </div>
                                    <img src="<?php echo base_url();?>assets/images/model_a.png">
                                </div>
                            </div>
                            <div class="item">
                                <div class="model_a">
                                    <div class="wrapper chge_loc">
                                        <div class="plus_add"> +</div>
                                        <div class="tooltip">
                                            <h3>Kurthis</h3>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                            <p class="rd_mre">Read More</p>
                                        </div>
                                    </div>
                                    <img src="<?php echo base_url();?>assets/images/model_b.png">
                                </div>
                            </div>
                            <div class="item">
                                <div class="model_a">
                                    <div class="wrapper chge_lft">
                                        <div class="plus_add"> +</div>
                                        <div class="tooltip">
                                            <h3>Kurthis</h3>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                            <p class="rd_mre">Read More</p>
                                        </div>
                                    </div>
                                    <img src="<?php echo base_url();?>assets/images/model_c.png">
                                </div>
                            </div>
                            <div class="item">
                                <div class="model_a">
                                    <div class="wrapper">
                                        <div class="plus_add"> +</div>
                                        <div class="tooltip">
                                            <h3>Kurthis</h3>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                            <p class="rd_mre">Read More</p>
                                        </div>
                                    </div>
                                    <img src="<?php echo base_url();?>assets/images/model_a.png">
                                </div>
                            </div>
                            <div class="item">
                                <div class="model_a">
                                    <div class="wrapper chge_loc">
                                        <div class="plus_add"> +</div>
                                        <div class="tooltip">
                                            <h3>Kurthis</h3>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                            <p class="rd_mre">Read More</p>
                                        </div>
                                    </div>
                                    <img src="<?php echo base_url();?>assets/images/model_b.png">
                                </div>
                            </div>
                            <div class="item">
                                <div class="model_a">
                                    <div class="wrapper chge_lft">
                                        <div class="plus_add"> +</div>
                                        <div class="tooltip">
                                            <h3>Kurthis</h3>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                                            <p class="rd_mre">Read More</p>
                                        </div>
                                    </div>
                                    <img src="<?php echo base_url();?>assets/images/model_c.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- derss_block end -->



            <!-- footer_block -->
            <div class="banner_fth">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 col-sm-12 col-xs-12">
                        </div>
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="foot_img_outer">
                                        <div class="foot_img">
                                            <img src="<?php echo base_url();?>assets/images/phones.png">
                                        </div>
                                    </div>
                                    <div class="foot_b contnt">
                                        <h4>Contact Us</h4>
                                        <ul class="ft_link">
                                            <li>Tel:+91 985 9548</li>
                                            <li>Tel:+91 985 9548</li>
                                            <li>support@zyva.com</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="foot_img_outer">
                                        <div class="foot_img">
                                            <img src="<?php echo base_url();?>assets/images/location.png">
                                        </div>
                                    </div>
                                    <div class="foot_b">
                                        <h4>Visit Us</h4>
                                        <ul class="ft_link">
                                            <li>Zyva Fashions</li>
                                            <li> Market Road,Kakanad</li>
                                            <li>Kochi </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="foot_img_outer">
                                        <div class="foot_img">
                                            <img class="acnt" src="<?php echo base_url();?>assets/images/acount.png">
                                        </div>
                                    </div>
                                    <div class="foot_b">
                                        <h4> Follow Us</h4>
                                        <ul class="ft_link">
                                            <li>www.zyva.co.in</li>
                                            <li>
                                                <div class="social_icon">
                                                    <ul class="social">
                                                        <li><i class="fa fa-facebook fa-lg lght_rse" aria-hidden="true"></i></li>
                                                        <li><i class="fa fa-twitter fa-lg lght_rse" aria-hidden="true"></i>
                                                        </li>
                                                        <li><i class="fa fa-linkedin fa-lg lght_rse" aria-hidden="true"></i>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="col-md-2 col-sm-6 col-xs-12"></div>
                        <div class="col-md-8 col-sm-6 col-xs-12 no_padng">
                            <div class="footer_navs">
                                <ul class="nav navbar-nav navbar-right nav_font foot_rht">
                                    <li><a class="cool-link" href="home.html">About Us</a></li>
                                    <li><a class="cool-link" href="#">Contact Us</a></li>
                                    <li><a class="cool-link" href="#">Terms </a></li>
                                    <li><a class="cool-link" href="#"> Policies</a></li>
                                    <li><a class="cool-link" href="#">Download Measurement Form</a></li>
                                </ul>
                            </div>
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="col-md-4 col-sm-6 col-xs-12"></div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="footer_navs_cards">
                                        <ul>
                                            <li><img src="<?php echo base_url();?>assets/images/payment_a.png"></li>
                                            <li><img src="<?php echo base_url();?>assets/images/payment_b.png"></li>
                                            <li><img src="<?php echo base_url();?>assets/images/payment_c.png"></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12"></div>
                            </div>
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="col-md-3 col-sm-6 col-xs-12"></div>
                                <div class="col-md-6">
                                    <div class="footer_navs_cards">
                                        <p>zyva.co.in 2016.All Rights Reserved</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12"></div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
<!-- footer_block end -->


            <!-- Modal login -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body login_modal">
                            <div class="col-md-12 no_padng">
                                <div class="col-md-6 no_padng">
                                    <div class="login_section1">
                                        <div class="padng_outer">
                                            <img class="log_zyva" src="<?php echo base_url();?>assets/images/zya_logo_blck.png">
                                            <input type="text" class="text_box" placeholder="Email">
                                            <input type="text" class="text_box" placeholder="password">
                                            <button class="log_but">Login</button>
                                            <p class="forgot_pass">Forgot password ?</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 no_padng">
                                    <div class="login_section2">
                                        <div class="padng_outer">
                                            <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url();?>assets/images/modal_close.png"></button>
                                            <div class="new_user">
                                                <p>New User?</p>
                                                <h4>register now</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal login end -->



            <!-- Modal register end -->

        </div>
        <!-- Bootstrap core JavaScript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

    </body>
</html>
