<!-- my cart -->
<div class="my_cart_bg">
   <div class="container">
      <div class="my_bag">
         <div class="col-md-12 ">
            <div class="col-md-4"></div>
            <div class="col-md-4">
               <div class="bag_icon_full">
                  <div class="bag_icon_outer">
                     <div class="bag_icon">
                        <img src="<?php echo base_url();?>assets/images/my_bag.png">
                     </div>
                     <span> My cart</span>
                  </div>
               </div>
            </div>
            <div class="col-md-4"></div>
         </div>
      </div>
      <!-- table -->
      <?php
         if(count($cart['result'])){
         
         ?>
      <div class="col-md-12">
         <div class="mycart_table">
            <table>
               <thead>
                  <tr>
                     <th class="lft_moves">Product Name</th>
                     <th class="lft_moves">Unit Price</th>
                     <th class="lft_moves"> qty</th>
                     <th class="lft_moves">sub total </th>
                     <th>&nbsp;  </th>
                     <th>&nbsp;  </th>
                  </tr>
               </thead>
               <?php
                  $i=0;
                  
                  $t=0;
                  
                         // print_r($cart['result']);
                  
                          foreach ($cart['result'] as $cart_info) { ?>
               <?php 
                  // echo $cart_info->image;
                  
                  // print_r($cart['result']); die;
                  
                  ?> 
               <tbody class="bord_bots">
                  <tr>
                     <td class="">
                        <div class="orders border_nill">
                           <div class="row">
                              <div class="col-md-3 col-sm-6 col-xs-12">
                                 <div class="order_dtl">
                                    <img src="<?php echo get_image_path($cart_info->product_id); ?>">
                                 </div>
                              </div>
                              <div class="col-md-8 col-sm-6 col-xs-12" style="padding-left:0px;">
                                 <div class="order_inf table_fit ">
                                    <h4><?php echo $cart_info->product_name;?></h4>
                                    <h5><?php echo $cart_info->product_code;?></h5>
                                    <div class="dres_inf">
                                       <p>Color</p>
                                       <p class="clr_dre" style="background-image: url('<?php echo $cart_info->image;?>'); box-shadow: 2px 2px 5px 0px #888888;"></p>
                                    </div>
                                    <div class="dres_inf">
                                       <p>Size</p>
                                       <p><?php echo $cart_info->size_type;?></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </td>
                     <td class="pos_abs wdth_inrs">
                        <div class="pas_iner">
                           <p>  &#8377; <?php echo $cart_info->price;?> </p>
                        </div>
                     </td>
                     <td class="pos_abs low_wdths">
                        <div class="pas_iner" id="quantity_dis<?php echo $cart_info->id; ?>">
                           <p><?php echo $cart_info->quantity;?></p>
                        </div>
                        <?php
                           //echo $cart_info->product_id.','.$cart_info->color_id.','.$cart_info->size_id.','.$cart_info->quantity;
                                    $actual_qty = get_actual_qty($cart_info->product_id,$cart_info->color_id,$cart_info->size_id,$cart_info->quantity);
                                    ?>
                        <div id="quantity_div_<?php echo $cart_info->id; ?>" style="display: none">
                           <select id="qty_<?php echo $cart_info->id; ?>" class="qty form-control" data-parent="<?php echo $cart_info->price;?>" data-id="<?php echo $cart_info->id; ?>" data-val="<?php echo $cart_info->total;?>">
                              <?php 
                                 for($i=1;$i<=$actual_qty;$i++) {?>
                              <option value="<?php echo $i; ?>" <?php if($i==$cart_info->quantity) echo 'selected="SELECTED"'; ?>><?php echo $i; ?></option>
                              <?php } ?>
                              ?>
                           </select>
                        </div>
                     </td>
                     <?php 
                        /*$price=$cart->price;
                        $quantity=$cart->quantity;
                        $total=$price * $quantity;
                        //$t+= $total;
                        //$i++;*/
                        ?>
                     <td class="pos_abs sub_tot">
                        <div class="pas_iner" id="price_tag<?php echo $cart_info->id; ?>">
                           <p>  &#8377; <?php echo $cart_info->total;?></p>
                        </div>
                     </td>
                     <td class="pos_abs low_wdths">
                        <div class="pas_iner">
                           <!-- <p> <a href="<?php echo base_url('product_info/'.$cart_info->prod_display);?>"><button  class="remove_btn1 abc" type="button"><i class="fa fa-pencil" aria-hidden="true">  edit</i></button></a></p> -->
                           <p> <a href="javascript:void(0)"><button  class="remove_btn1 abc edit_cart" type="button" data-id="<?php echo $cart_info->id; ?>" id="qty_label<?php echo $cart_info->id; ?>"><i class="fa fa-pencil" aria-hidden="true">  edit</i></button><button  class="remove_btn1 abc save_cart" type="button" data-id="<?php echo $cart_info->id; ?>" style="display: none" id="qty_select<?php echo $cart_info->id; ?>"><i class="fa fa-pencil" aria-hidden="true">  Save</i></button></a></p>
                           <p class="move" data-id="<?php echo $cart_info->id; ?>"><i class="fa fa-heart-o" aria-hidden="true"></i>
                              move
                           </p>
                        </div>
                     </td>
                     <td class="pos_abs wdth_inrsful" >
                        <div  class="remove_pdt pas_iner cross_btn" data-id="<?php echo $cart_info->id; ?>" > <img src="<?php echo base_url();?>assets/images/cross.png"></div>
                     </td>
                  </tr>
               </tbody>
               <?php } ?>
            </table>
         </div>
      </div>
      <!-- table -end->
         </div>
         
         <!--  my cart -->
      <div class="check_proceed">
         <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
               <div class="grand_total">
                  <div class="col-md-12">
                     <div class="col-md-6"></div>
                     <div class="col-md-6">
                        <p>grand total</p>
                        <p class="fnt_wght cart_price">&#8377; <?php echo $cart['total'];?> </p>
                     </div>
                     <div class="col-md-1"></div>
                     <div class="col-md-11" style="text-align: right;">
                        <div class="errormsg2" style="color:red"></div>
                        <button id="continue" class="yelow_but">Continue Shopping</button>
                        <button  id="checkout" class="yelow_but yellow_clr">Proceed to Checkout</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php } else { ?>
      <div class="empty no_cart">No product found in cart.</div>
      <?php } ?>
   </div>
</div>