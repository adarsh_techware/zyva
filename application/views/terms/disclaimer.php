<div class="starter-template">

    <div class="my_acount_tabs top_inr">

        <div class="terms_condition">

            <div class="container">

                <div class="col-md-12">   

                    <div class="terms_cabin">
                        <!-- <h2>Terms And  Conditions</h2> -->

                            <div class="zyva_terms">
                                <h3>Disclaimer and limitation of liability</h3>

                                  <p>To the extent permitted by law, we exclude all warranties, conditions and representations whether

                                    express, implied, statutory or otherwise, relating in any way to this web site, your use of this web site

                                    and the information, products and services supplied, offered to be supplied or advertised or accessed

                                    through this web site; and liability You acknowledge and undertake that you are accessing the

                                    Services and purchasing the Kurtis at your own risk and that you are using prudent judgment before

                                    placing an order for a Product or availing any Services through the Platforms. Maryam Apparels shall, at no point, is

                                    held liable or responsible for any representations or warranties in relation to the kurtis. Refund of the

                                    price paid for the purchase of kurtis or replacement thereof shall be governed by the 
                                    <b><a href="<?php echo base_url();?>terms" style="text-decoration:none;">Return and

                                    Refund Policy</a></b>. The information, images, photographs, trademarks, products and in general the

                                    elements and material that you will find on this website are disclosed only for promotional and

                                    advertising purposes; you may download or copy any of such elements and material only for private,

                                    personal and not commercial use. Maryam Apparels makes no warranties of the absence of malicious

                                    programs (such as viruses, bugs or Trojan horses). Our liability ends once the end product has been

                                    delivered to you.</p>

                                    <h3>Intellectual Property Right</h3>
                                    <p>All the intellectual property used on the Platforms by Maryam Apparels, including the Maryam Apparels Content, shall

                                    remain the property of Maryam Apparels, its parent company, group companies, subsidiaries, associates, affiliates,

                                    suppliers, vendors, sister companies or of any third party hosting such intellectual property on the

                                    Platforms. Except as provided in the Agreement, the materials may not be modified, copied,

                                    reproduced, distributed, republished, downloaded, displayed, sold, compiled, posted or transmitted

                                    in any form by any means, including but not limited to, electronic, mechanical, photocopying,

                                    recording or other means, without the prior express written permission of Maryam Apparels.
                                    </p>

                            </div>
                        </div>
                    </div> 
                </div>

            </div>

        </div>   

    </div>

</div>



