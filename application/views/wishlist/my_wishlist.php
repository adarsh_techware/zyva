<!-- nav bar -->
<div class="starter-template">
<div class="my_acount_tabs top_inr">
   <!-- Starting my order Page here -->
   <div class="col-md-12 pad_lft bg_full_cnt">
      <div class="container">
         <div class="col-md-2"></div>
         <div class="col-md-8 order_padbot">
            <div class="my_ordr">
               <span class="mywish_img"><img src="<?php echo base_url();?>assets/images/my_wish.png"></span>
               MY WISHLIST
            </div>
            <!--  Sectio1 srarts here  -->
            <div class="row row_pad_bot">
               <h3 id="error_message"></h3>

               <?php foreach ($wishlist as $cart) { ?>
               <form method="POST" id="cart" >
                  <div class="row" id="len">
                     <div class="col-md-6 col-sm-6 col-sm-12">
                        <!-- <div class="row"> -->
                        <div class="border_right  left_padding">
                           <div class="col-md-4 col-sm-6 col-xs-6">
                              <div class="order_dtl">
                                 <!-- <img src="images/dress_c.png">-->
                                 <img src="<?php echo base_url('admin/'.$cart->product_image.$cart->format);?>">
                              </div>
                           </div>
                           <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left:0px;">
                              <div class="order_inf table_fit">
                                 <input type="hidden"  name="customer_id" value="<?php echo $this->session->userdata('user_id')?>" >
                                 <input type="hidden"  id="product" name="product_id" value="<?php echo $cart->product_id;?>" >
                                 <h4><?php echo $cart->product_name;?></h4>
                                 <input type="hidden"  id="product_name" name="product_name" value="<?php echo $cart->product_name;?>" >
                                 <h5><?php echo $cart->product_code;?></h5>
                                 <h5 class="order_price padng_lows"> &#8377; <?php echo $cart->price;?></h5>
                                 <input type="hidden"  name="price" value="<?php echo $cart->price;?>" >
                              </div>
                           </div>
                           <div class="col-md-2"></div>
                        </div>
                        <!-- </div> -->
                     </div>
                     <div class="col-md-3 col-sm-3 col-sm-12 details_pad">
                        <div class="border_right">
                           <div class="my_order_list">
                              <div class="frst_head">Color</div>
                              <div class="scnd_head">
                                 <p class="clr_dre" style="background-image:url(<?php echo $cart->image;?>)"></p>
                                 <input type="hidden"  name="color_id" value="<?php echo $cart->color_id;?>" >
                              </div>
                           </div>
                           <div class="my_order_list bot_pad">
                              <div class="frst_head">Size</div>
                              <div class="scnd_head"><?php echo $cart->size_type;?></div>
                              <input type="hidden"  name="size_id" value="<?php echo $cart->size_id;?>" >
                           </div>
                           <div class="my_order_list">
                              <div class="frst_head">QTY</div>
                              <!-- <div class="scnd_head"><?php echo $cart->quantity;?></div> -->
                               <div class="scnd_head">1</div>
                              <input type="hidden"  name="quantity" value="<?php echo $cart->quantity;?>" >
                           </div>
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-3 col-sm-12">
                        <div class="order_buttons">
                           <a href="" class="remove_a">
                              <!-- <a href="<?php echo base_url('Women/productdetail/'.$cart->product_name);?>">
                                 <button  class="remove_btn1" type="button"> 
                                 
                                   <span class="close_btn">
                                 
                                     <img src="<?php echo base_url();?>assets/images/edit.png">
                                 
                                   </span> edit
                                 
                                 </button>
                                 
                                 </a> -->
                              <button  class="remove_btn" data-id="<?php echo $cart->id; ?>" type="button">
                              <span class="close_btn">
                              <img src="<?php echo base_url();?>assets/images/close.png">
                              </span> Remove
                              </button>
                           </a>
                        </div>
                        <div class="errormsg"></div>
                        <div class="order_buttons">
                           <button class="add_cart_btn wishlisttocart" data-id="<?php echo $cart->id; ?>" type="button">Add to Cart</button>
                        </div>
                     </div>
                  </div>
                  <hr class="hr_bord">
                  <?php
                     }
                     
                     ?>
               </form>
            </div>
            <!--  Sectio1 End here  -->
            <!-- <div class="section2"><hr class="myorder_hr row_pad_bot"></div> -->
            <!--  Sectio2 Starts here  -->
            <div class="row">
               <div class="col-md-6 col-sm-6 col-sm-12">
               </div>
               <?php
                  if(count($wishlist)>0){?>
               <div class="col-md-12 col-sm-12 col-xs-12 cart_wish_button">
                  <div class="msg"></div>
                  <div class="errormsg1"></div>
                  <div class="errormsg2" style="color:red"></div>
                  <button id="addall" class="add_all_btn">Add all to Cart</button>
                  <!-- <button id="update" class="update_wish_btn">Update Wishlist </button> -->
               </div>
               <?php } else { ?>
               <div class="col-md-12 col-sm-12 col-xs-12 cart_wish_button">
                  <div class="empty msg">No wishlist found.</div>
               </div>
               <?php } ?>
            </div>
            <!-- section 2 ends here -->
         </div>
      </div>
      <div class="col-md-2"></div>
   </div>
</div>
<!-- End My order Page Here -->