<!DOCTYPE html>

<html ng-app='myFrontend'>

	<?php
		$this->load->view('Templates/header-script.php');
	?>

	<body class="ng-cloak">

	<?php

		$this->load->view('Templates/header-home');

		$this->load->view($page);

		$this->load->view('Templates/footer');

		$this->load->view('Templates/footer-script');

	?>

	</body>

</html>

