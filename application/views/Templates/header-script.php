<head>
   <meta http-equiv="content-type" content="text/html; charset=UTF-8">
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="apple-mobile-web-app-capable" content="yes">
   <!-- <meta name="viewport" content="user-scalable=no"> -->
   <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <?php if(isset($description)){ ?>
   <meta name="description" content="<?php echo $description;?>">
   <?php } else { ?> 
   <meta name="description" content="">
   <?php } ?>
   <meta name="author" content="">
   <meta name="theme-color" content="#9c2c80">
   <?php if(!isset($title)){ ?>
   <title>Zyva Fashions</title>
   <?php } else { ?> 
   <title><?php echo $title;?></title>
   <?php } ?>
   <link rel="icon" href="<?php echo base_url('assets/images/zyva_ fav icon.png'); ?>">
   <!-- for slick slider    -->
   <link href="<?php echo base_url(); ?>assets/css/slick/slick.css" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url(); ?>assets/css/slick/slick-theme.css" rel="stylesheet" type="text/css" />
   <!-- Bootstrap core CSS -->
   <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/animations.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/animations.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/hover-min.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/owl.carousel.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/owl.theme.css" rel="stylesheet">
   <!-- Custom css -->
   <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
   <!-- <link href="<?php echo base_url();?>assets/css/magnific-popup.css" rel="stylesheet"> -->
   <link href="<?php echo base_url();?>assets/css/magiczoomplus.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/parsley/parsley.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/jslider.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/jquery-ui.css" rel="stylesheet">
   <link href="<?php echo base_url();?>/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
   <!-- Custom Fonts -->
   <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"> -->
   <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
   <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
   <script src="<?php echo base_url();?>assets/js/wNumb.js"></script>
   <script src="<?php echo base_url();?>assets/js/nouislider.min.js"></script>
   <script src="<?php echo base_url();?>/assets/js/jquery.sidebarFix.js"></script>
   <link href="<?php echo base_url();?>assets/css/nouislider.min.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/sticky-sidebar.css" rel="stylesheet">
</head>



