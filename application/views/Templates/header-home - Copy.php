<!-- nav bar -->
<?php
// $uri=$this->uri->uri_string();
//  $uri;
?>


<nav class="navbar navbar-default navbar-fixed-top inner_nav home_bg">
   <div class="container">
      <div class="main_nav">
         <div class="col-md-2 col-sm-2 col-xs-3 remove_media_pad">
            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header">
<!--               <button type="button" class="navbar-toggle collapsed small_nav" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">-->
<!--               <span class="sr-only">Toggle navigation</span>-->
<!--               <span class="icon-bar"></span>-->
<!--               <span class="icon-bar"></span>-->
<!--               <span class="icon-bar"></span>-->
<!--               </button>-->

                <!--new-header-menu-->

                <div class="dropdown">
                    <button class="btn dropdown-toggle navbar-toggle mn-tog-menu" type="button" data-toggle="dropdown">
                        <img class="mn-tog" src="<?php echo base_url();?>assets/images/mn-tog.png">
<!--                        <span class="caret"></span>-->
                    </button>
                    <ul class="dropdown-menu dp-menu-new">
                        <li class="act-hm-mn"><a href="#">Home</a></li>
                        <li><a href="#">Women</a></li>
                        <li><a href="#">Celeb Style</a></li>
                        <li><a href="#">New Look</a></li>
                        <li><a href="#">Festive</a></li>
                        <li><a href="#">Daily Wear</a></li>
                    </ul>
                </div>
                <!-- new-header-menu-->


               <a class="navbar-brand inner" href="<?php echo base_url()?>">
                  <img class="hd_logo" src="<?php echo base_url();?>assets/images/logo.png">
                  <div class="zyva_logo"></div>
              </a>
            </div>

         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="col-md-10  col-sm-10 col-xs-9 remove_media_pad">
            <div class="womes_add">
               <div class="col-md-12  col-sm-12 col-xs-12 remove_small_media">
                  <div class="col-md-6 col-sm-12 col-xs-7 media_pad_bottom ui-widget remove_small_media">
                     <input type="text" class="search_dress lookup hidden-xs"  placeholder="Search for Products, Category and More" name="items" id="search_item">
                      <!--  mobile-->
                      <input type="text" class="search_dress lookup hidden-lg hidden-md hidden-sm"  placeholder="Search" name="items" id="search_item">
                      <!--  mobile-->
                     <span class="search_icon small_search"><img src="<?php echo base_url();?>assets/images/search_icon.png" id="search" ></span>
                     <div class="matches"></div>
                  </div>


				   <?php   if(!$this->session->userdata('user_id')) { ?>
                  <div class="col-md-2  col-sm-6 pad_lft">
                                    <div class="logs">
                                        <ul>
                                            <li><a data-toggle="modal" data-target="#myModal">Login</a></li>
                                            <li>
                                                <div class="bar_hght"></div>
                                            </li>
                                            <li><a  data-toggle="modal" data-target="#myModal_reg">Register</a></li>
                                        </ul>
                                    </div>
                        </div>
				   <?php } ?>
				      <div class="col-md-3  col-sm-6 col-xs-2 remove_shop_pad">
				  
                     <div class="shoping_bag_out" id="search">
					         <?php  if($this->session->userdata('user_id')) { ?>
                           <div class="shoping_bag search-link" href="#search" class="search-nav-button" id="search-trigger">
                              <img src="<?php echo base_url();?>assets/images/bag.png">
                              <div class="item_shop"><?php echo $result;?></div>
                           </div>
					         <?php }else { ?>
      					      <div class="shoping_bag search-link">
                                 <img src="<?php echo base_url();?>assets/images/bag.png">
                                 <div class="item_shop">0</div>
                           </div>
   					      <?php } ?>
   					 
   					 
                          <!-- <p>Shopping Bag</p>-->
   					   
         						<p  class="search-nav-button my_shop_bag" id="search-trigger">Shopping Bag</p>
         						
                           <div class="search-box">
         						   <form method="POST" id="removecart" >
                              
         						   <?php if($result!==0){?>
                                    
                                 <div class="hh content mCustomScrollbar _mCS_4" data-mcs-theme="dark">
               						   <?php
               						   if($this->session->userdata('user_id')) { 
               							$i=0;
               							$total=0;
                                       foreach ($cartlist as $cart) {
                                        ?>
               							 
                                          <div class="row">
                                             <div class="col-md-12 bord_botsfull">
                                                <div class="col-md-4">
                                                   <img src="<?php echo $cart->product_image;?>">
                                                </div>
                                                <div class="col-md-8">
                                                   <div class="order_inf poptext ">
                                                      <h4><?php echo $cart->product_name;?></h4>
                     										   <input type="hidden"   name="product_id" value="<?php echo $cart->product_id;?>" >
                     										   <input type="hidden"   name="product_name" value="<?php echo $cart->product_name;?>" >
                     										   <input type="hidden"   name="color_id" value="<?php echo $cart->color_id;?>" >
                     										   <input type="hidden"   name="size_id" value="<?php echo $cart->size_id;?>" >
               									   
               								
                                                      <h5>Price:&nbsp;<?php echo $cart->quantity;?> x &#8377;<?php echo $cart->price;?></h5>
               								  
               								               <div class="check_clrs">
                                                         <div class="check_clr_size">Color</div>
                                                         <div class="check_clr_b"><div class="clrs_check" style="background-image:url(<?php echo $cart->image;?>);"></div></div>
                                                      </div>
               							                  <div class="check_clrs">
                                                         <div class="check_clr_size">Size</div>
                                                         <div class="check_clr_b"><?php echo $cart->size_type;?></div>
                                                      </div>
                                                      <p class="remove_pdt" data-id="<?php echo $cart->id;?>" style="width:51% !important"><img src="<?php echo base_url();?>assets/images/close_icon.png"> &nbsp; Remove</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
               							
               							   <?php 
               							   $q=$cart->quantity;
               							      $total+= $cart->price*$q;
               							      $i++; 
               								}
               								?>
                                       <!-- <hr class="gray_bar"> -->
                                       <div class="col-md-12 check_margin">
                                          <div class="col-md-6" style="padding-left:0px;">
                                             <button id="addtocheckout" class="checks_out">check out</button>
                                          </div>
                                          <div class="col-md-6" style="padding:0px;">
                                             <div class="total_price">
                                                <p>total :  <b>&#8377;<?php echo $total;?></b> </p>
                                             </div>
                                          </div>
                                       </div>
                  						   <?php } ?>
                                 </div>
            						  <?php } ?>

            						</form>
                           </div>
						
						
						
                     </div>
                  </div>
				  
				  
				  <?php 
				   if(!$this->session->userdata('user_id')) { ?>
				  
               <div class="col-md-1  col-sm-6 col-xs-3">
                     <div class="shoping_bag_out chge_rht">
                           <div class="my_acount pad_lft">
                              <div class="dropdown ">

                                 <a data-toggle="modal" data-target="#myModal">
                                    <button class="btn my_acnt nw-acc-new" type="button">
                                       <img src="<?php echo base_url();?>assets/images/z-menu.png">
                                       <span class="carets pos_drop"><i class="fa fa-chevron-down low_font" aria-hidden="true"></i></span>
                                    </button>
                                 </a>
                                
                              </div>
                           </div>
                           <!-- <div class="my_acount">
                              <div class="dropdown ">
                                 <button class="btn dropdown-toggle acoubt my_acnt" type="button" data-toggle="dropdown"><img src="<?php echo base_url();?>assets/images/pro_pic.png">
                                 <span class="carets pos_drop"><i class="fa fa-chevron-down low_font" aria-hidden="true"></i>
                                 </span></button>
                                 <ul class="dropdown-menu bg_dropcontent drop_ch_pos bg_img">
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">My Wishlist</a></li>
                                    <li><a href="#">My Cart</a></li>
                                    <li><a href="#">Checkout</a></li>
                                    <li><a href="#">Logout</a></li>
                                 </ul>
                              </div>
                              </div>-->
                     </div>
				   <?php } else {?>

				   <div class="col-md-3  col-sm-6 col-xs-3">

					   <div class="acnt_nam">Hi,&nbsp;<?php echo $data->first_name;?>&nbsp;!</div>
				   
				   
				   
								  
				
					
                     <div class="shoping_bag_out chge_rht">
                        <div class="my_acount pad_lft">
                           <div class="dropdown ">
                              <button class="btn dropdown-toggle acoubt my_acnt" type="button" data-toggle="dropdown"><img src="<?php echo base_url();?>assets/images/pro_pic.png">
                              <span class="carets pos_drop"><i class="fa fa-chevron-down low_font" aria-hidden="true"></i>
                              </span></button>
                              <ul class="dropdown-menu bg_dropcontent drop_ch_pos bg_img">
                                 <li><a href="<?php echo base_url();?>account">My Account</a></li>
                                 <li><a href="<?php echo base_url();?>wishlist">My Wishlist</a></li>
                                 <li><a href="<?php echo base_url();?>cart">My Cart</a></li>
                                 <li><a href="<?php echo base_url();?>measurement">My Measurement</a></li>
                                 <li><a href="<?php echo base_url();?>myorder">My Order</a></li>
                                 <li><a href="<?php echo base_url();?>checkout">Checkout</a></li>
                                 <li><a href="<?php echo base_url();?>logout">Logout</a></li>
                              </ul>
                           </div>
                        </div>
                        <!-- <div class="my_acount">
                           <div class="dropdown ">
                              <button class="btn dropdown-toggle acoubt my_acnt" type="button" data-toggle="dropdown"><img src="<?php echo base_url();?>assets/images/pro_pic.png">
                              <span class="carets pos_drop"><i class="fa fa-chevron-down low_font" aria-hidden="true"></i>
                              </span></button>
                              <ul class="dropdown-menu bg_dropcontent drop_ch_pos bg_img">
                                 <li><a href="#">My Account</a></li>
                                 <li><a href="#">My Wishlist</a></li>
                                 <li><a href="#">My Cart</a></li>
                                 <li><a href="#">Checkout</a></li>
                                 <li><a href="#">Logout</a></li>
                              </ul>
                           </div>
                           </div>-->
                     </div>
                     <?php } ?>
                
               </div>

               <?php
                  //print_r($category);
               ?>

               <div class="collapse navbar-collapse nav_link" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right nav_font">
                        <li <?php if (  $this->uri->segment(2) == "" ) : ?> class="active_menu" <?php endif; ?> >
                              <a class="cool-link abc" href="<?php echo base_url();?>">Home</a>
                        </li>
                        <?php
                        foreach ($Category as $ro) {
                          // print_r($ro);die;
                        
                        ?>
                        <li <?php if ( $this->uri->segment(2) == $ro->display_name): ?> class="active_menu"<?php endif; ?> >
                           <a class="cool-link" href="<?php echo base_url();?>product/<?php echo  $ro->display_name;?>"><?php echo  $ro->category_name;?></a>
                        </li> 
                     <!-- <li><a class="cool-link" href="<?php echo base_url();?>Women/index">Celeb Style</a></li>
                        <li><a class="cool-link" href="<?php echo base_url();?>Women/index">New Look</a></li>
                        <li><a class="cool-link" href="<?php echo base_url();?>Women/index">Festive</a></li>
                        <li><a class="cool-link" href="<?php echo base_url();?>Women/index">Daily Wear</a></li> -->
                     <?php } ?>
                  </ul>
               </div>
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </div>
   </div>
</nav>

<script>



// (function($) {
  // $.fn.autocomplete = function(words, output) {
    // var startsWith = function(letters) {
      // return function(word) {
        // return word.indexOf(letters) === 0;
      // }
    // }

    // var matches = function(letters) {
      // return letters ?
        // $.grep(words, startsWith(letters)) : [];
    // }

    // this.keyup(function() {
      // var letters = $(this).val();
      // output(letters, matches(letters));
    // });
  // };
// })(jQuery);

// var words = [
  // 'random', 'list', 'of', 'words',
  // 'draisine', 'swithe', 'overdiversification', 'bitingness',
  // 'misestimation', 'mugger', 'fissirostral', 'oceanophyte',
  // 'septic', 'angletwitch', 'brachiopod', 'autosome','autosome','autosome','autosome','autosome','autosome','autosome',
  // 'uncredibility', 'epicyclical', 'causticize', 'tylotic',
  // 'robustic', 'chawk', 'mortific', 'histotomy',
  // 'slice', 'enjambment', 'mercaptids', 'oppositipetalous',
  // 'impious', 'pollinivorous', 'poulaine', 'wholesaler'
// ];

// var render = function($output) {
  // return function(letters, matches) {
    // $output.empty()

    // if(matches.length) {
      // var $highlight = $('<span/>')
        // .text(letters)
        // .addClass('highlight');

      // $.each(matches, function(index, match) {
        // var remaining = match.replace(letters, '');
        // $match = $('<li/>')
          // .append($highlight.clone(), remaining)
          // .addClass('match');
        // $output.append($match);
      // });
    // }
  // }
// }

// $(document).ready(function() {
  // $('input').autocomplete(words.sort(), render($('.matches')));
// });


 $(document).ready(function(){

 /* $( "#search_item" ).on('keyup',function(){
    var dataString = $(this).val();

    $.ajax({
      type: "POST",
        url: base_url+"ajax/get_keyword",
      data: 'key='+dataString,
      success: function(result){

      }
        
    });*/

    //function new_fun(result){
      //$(document).ready(function(){

    $( "#search_item" ).on('keyup',function(){    
      var length = $( "#search_item" ).val().length;
      if(length>0){
        //alert(length);
      $( "#search_item" ).autocomplete({
               minLength: 0,
               source: base_url+"ajax/get_keyword/?key="+$( "#search_item" ).val(),
               focus: function( event, ui ) {
                  $( "#search_item" ).val( ui.item.product_name );
                     return false;
               },
               select: function( event, ui ) {
                  $( "#search_item" ).val( ui.item.product_name );
                  return false;
               }
            })

        
            .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
               return $( "<li>" )
               .append( "<a href='"+base_url+item.prod_display+"'>"+item.product_name+"</a>" )
               .appendTo( ul );
            };
          }

            });
    });
    //}
    //alert('sdasdasd');
    

            
  //})

 $(".small_search").click(function(){
  $(".search_dress").show();
});
            

$(".small_nav").click(function(){
    $(".home_bg").addClass("active_small_nav ");
});
</script>



