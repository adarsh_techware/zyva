<!-- nav bar -->
<nav class="navbar navbar-default navbar-fixed-top inner_nav home_bg">
  <div class="container">
    <div class="main_nav">
       <div class="col-md-2 col-sm-1 col-xs-3 remove_media_pad">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
             <div class="dropdown">
                <button class="btn dropdown-toggle navbar-toggle mn-tog-menu" type="button" data-toggle="dropdown">
                <img class="mn-tog" src="<?php echo base_url();?>assets/images/mn-tog.png">
                </button>
                <ul class="dropdown-menu dp-menu-new">
                   <li <?php if ($this->uri->segment(2) == "") : ?> class="act-hm-mn" <?php endif; ?>><a href="<?php echo base_url();?>">Home</a></li>
                   <?php foreach ($Category as $ro) { ?>
                   <li <?php if ( $this->uri->segment(2) == $ro->display_name): ?> class="act-hm-mn" <?php endif; ?> >
                      <a href="<?php echo base_url();?>product/<?php echo  $ro->display_name;?>"><?php echo  $ro->category_name;?></a>
                   </li>
                   <?php } ?>
                </ul>
             </div>
             <!-- new-header-menu-->
             <a class="navbar-brand inner" href="<?php echo base_url()?>">
                <img class="hd_logo" src="<?php echo base_url();?>assets/images/logo.png">
                <div class="zyva_logo"></div>
             </a>
          </div>
       </div>
       <!-- Collect the nav links, forms, and other content for toggling -->
       <div class="col-md-10  col-sm-11 col-xs-9 remove_media_pad">
          <div class="womes_add">
             <div class="col-md-12  col-sm-12 col-xs-12 remove_small_media">
                <div class="col-md-6 col-sm-4 col-xs-7 media_pad_bottom ui-widget remove_small_media">
                   <input type="text" class="search_dress lookup hidden-xs"  placeholder="Search for Products, Category and More" name="items" id="search_item">
                   <!--  mobile-->
                   <input type="text" class="search_dress lookup hidden-lg hidden-md hidden-sm"  placeholder="Search" name="items" id="search_item">
                   <!--  mobile-->
                   <span class="search_icon small_search"><img src="<?php echo base_url();?>assets/images/search_icon.png" id="search" ></span>
                   <div class="matches"></div>
                </div>
                <?php   if(!$this->session->userdata('user_id')) { ?>
                <div class="col-md-2  col-sm-3 pad_lft">
                   <div class="logs">
                      <ul>
                         <li><a href="<?php echo base_url('home/signin');?>">Login</a></li>
                         <li>
                            <div class="bar_hght"></div>
                         </li>
                         <li><a href="<?php echo base_url('home/signup');?>">Register</a></li>
                      </ul>
                   </div>
                </div>
                <?php } ?>
                <div class="col-md-3  col-sm-3 col-xs-2 remove_shop_pad">
                   <div class="shoping_bag_out" id="search">
                      <div class="shoping_bag search-link search-nav-button" href="#search"  id="search-trigger">
                         <img src="<?php echo base_url();?>assets/images/bag.png">
                         <div class="item_shop"><?php echo $result;?></div>
                      </div>
                      <!-- <p>Shopping Bag</p>-->
                      <p  class="search-nav-button my_shop_bag" id="search-trigger">Shopping Bag</p>
                      <div class="search-box">
                         <form method="POST" id="removecart" >
                            <?php if($result!==0){?>
                            <div class="hh content mCustomScrollbar _mCS_4" data-mcs-theme="dark">
                               <?php
                                  $i=0;
                                  $total=0;
                                  foreach ($cartlist as $cart) {
                                ?>
                               <div class="row">
                                  <div class="col-md-12 bord_botsfull">
                                     <div class="col-md-4">
                                        <img src="<?php echo base_url('admin/'.$cart->product_image.'_201x302.jpg');?>">
                                     </div>
                                     <div class="col-md-8">
                                        <div class="order_inf poptext ">
                                           <h4><?php echo $cart->product_name;?></h4>
                                           <input type="hidden"   name="product_id" value="<?php echo $cart->product_id;?>" >
                                           <input type="hidden"   name="product_name" value="<?php echo $cart->product_name;?>" >
                                           <input type="hidden"   name="color_id" value="<?php echo $cart->color_id;?>" >
                                           <input type="hidden"   name="size_id" value="<?php echo $cart->size_id;?>" >
                                           <h5>Price:&nbsp;<?php echo $cart->quantity;?> x &#8377;<?php echo $cart->price;?></h5>
                                           <div class="check_clrs">
                                              <div class="check_clr_size">Color</div>
                                              <div class="check_clr_b">
                                                 <div class="clrs_check" style="background-image:url(<?php echo $cart->image;?>);"></div>
                                              </div>
                                           </div>
                                           <div class="check_clrs">
                                              <div class="check_clr_size">Size</div>
                                              <div class="check_clr_b"><?php echo $cart->size_type;?></div>
                                           </div>
                                           <p class="remove_pdt" data-id="<?php echo $cart->id;?>" style="width:51% !important"><img src="<?php echo base_url();?>assets/images/close_icon.png"> &nbsp; Remove</p>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                               <?php 
                                  $q=$cart->quantity;
                                     $total+= $cart->price*$q;
                                     $i++; 
                                  }
                                  ?>
                               <!-- <hr class="gray_bar"> -->
                               <div class="col-md-12 check_margin">
                                  <div class="col-md-6" style="padding-left:0px;">
                                    <?php if($this->session->userdata('user_id')) { ?>
                                      <button id="addtocheckout" class="checks_out">check out</button>
                                   <?php }
                                   else { ?>
                                      <button class="checks_out call_signin" type="button">check out</button>
                                   <?php } ?>
                                  </div>
                                  <div class="col-md-6" style="padding:0px;">
                                     <div class="total_price">
                                        <p>total :  <b>&#8377;<?php echo $total;?></b> </p>
                                     </div>
                                  </div>
                               </div>
                            </div>
                            <?php } ?>
                         </form>
                      </div>
                   </div>
                </div>
                <?php 
                   if(!$this->session->userdata('user_id')) { ?>
                <div class="col-md-1  col-sm-3 col-xs-3">
                   <div class="shoping_bag_out chge_rht shoping_bag_out-user">
                      <div class="my_acount pad_lft">
                         <div class="dropdown ">
                            <a href="<?php echo base_url('home/signin');?>">
                            <button class="btn my_acnt nw-acc-new" type="button">
                            <img src="<?php echo base_url();?>assets/images/z-menu.png">
                            <span class="carets pos_drop"><i class="fa fa-chevron-down low_font" aria-hidden="true"></i></span>
                            </button>
                            </a>
                         </div>
                      </div>
                   </div>
                   <?php } else {?>
                   <div class="col-md-3  col-sm-6 col-xs-3">
                      <div class="acnt_nam">Hi,&nbsp;<?php echo $data->first_name;?>&nbsp;!</div>
                      <div class="shoping_bag_out chge_rht">
                         <div class="my_acount pad_lft">
                            <div class="dropdown ">
                               <button class="btn dropdown-toggle acoubt my_acnt" type="button" data-toggle="dropdown"><img src="<?php echo base_url();?>assets/images/z-menu.png">
                               <span class="carets pos_drop"><i class="fa fa-chevron-down low_font" aria-hidden="true"></i>
                               </span></button>
                               <ul class="dropdown-menu bg_dropcontent drop_ch_pos bg_img">
                                  <li><a href="<?php echo base_url();?>account">My Account</a></li>
                                  <li><a href="<?php echo base_url();?>wishlist">My Wishlist</a></li>
                                  <li><a href="<?php echo base_url();?>cart">My Cart</a></li>
                                  <li><a href="<?php echo base_url();?>measurement">My Measurement</a></li>
                                  <li><a href="<?php echo base_url();?>myorder">My Order</a></li>
                                  <li><a href="<?php echo base_url();?>checkout">Checkout</a></li>
                                  <li><a href="<?php echo base_url();?>logout">Logout</a></li>
                               </ul>
                            </div>
                         </div>
                      </div>
                      <?php } ?>
                   </div>
                   <div class="collapse navbar-collapse nav_link" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right nav_font">
                         <li <?php if (  $this->uri->segment(2) == "" ) : ?> class="active_menu" <?php endif; ?> >
                            <a class="cool-link abc" href="<?php echo base_url();?>">Home</a>
                         </li>
                         <?php
                            foreach ($Category as $ro) {
                            ?>
                         <li <?php if ( $this->uri->segment(2) == $ro->display_name): ?> class="active_menu"<?php endif; ?> >
                            <a class="cool-link" href="<?php echo base_url();?>product/<?php echo  $ro->display_name;?>"><?php echo  $ro->category_name;?></a>
                         </li>
                         <?php } ?>
                      </ul>
                   </div>
                </div>
                <!-- /.navbar-collapse -->
             </div>
             <!-- /.container-fluid -->
          </div>
       </div>
    </div>
  </div>
</nav>
<script>
   $(document).ready(function(){
   
      $( "#search_item" ).on('keyup',function(){    
        var length = $( "#search_item" ).val().length;
        if(length>0){
          //alert(length);
        $( "#search_item" ).autocomplete({
                 minLength: 0,
                 source: base_url+"ajax/get_keyword/?key="+$( "#search_item" ).val(),
                 focus: function( event, ui ) {
                    $( "#search_item" ).val( ui.item.product_name );
                       return false;
                 },
                 select: function( event, ui ) {
                    $( "#search_item" ).val( ui.item.product_name );
                    return false;
                 }
              })
   
          
              .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                 return $( "<li>" )
                 .append( "<a href='"+base_url+item.prod_display+"'>"+item.product_name+"</a>" )
                 .appendTo( ul );
              };
            }
   
              });
      });
   $(".small_search").click(function(){
    $(".search_dress").show();
   
       $(".small_search").addClass("sm-srim");
   
   });
              
   
   $(".small_nav").click(function(){
      $(".home_bg").addClass("active_small_nav ");
   });

   // ************************FOR TOGGLE THE ITEMS IN MY BAG IN HEADER PAGE

var searchBox = $(".search-box");

var searchTrigger = $("#search-trigger");

searchTrigger.on("click", function(e) {
  $('.search-box').toggle();
    e.preventDefault();
    e.stopPropagation();
    // searchBox.toggle();
});

$(document).click(function(event) {
    if (!searchBox.is(event.target) && 0 === searchBox.has(event.target).length) {
        //searchBox.hide();
        $('.search-box').hide();
    };
});
// **********************************************************************
</script>