<?php
	$i = 1;
	//$count = count($product);
	foreach ($product as $rs) {
		$percentage= floor((($rs->mrp - $rs->price)*100)/$rs->mrp) ;
	?>

	<li>

	        <?php if(max_quantity($rs->id)>0) { ?>
        <div class="hover_div">
         <div class="prod_image">
          <a href="<?php echo base_url('product_info/'.$rs->prod_display);?>">
            <img data-original="<?php echo base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);?>">
          </a>
        </div>
        <div class="add_save hidden-xs">
          <?php if($this->session->userdata('user_id')) { ?>
          <a  id="<?php echo $rs->id;?>" class="prod_save add_wish">SAVE</a>
          <input type="hidden" id="prod_id" value="<?php echo $rs->id;?>">
          <?php } 
          else {?>
          <a id="call_signin" class="prod_save call_signin">SAVE</a>
          <?php } ?>
          <a href="<?php echo base_url('product_info/'.$rs->prod_display);?>" class="prod_add">ADD TO BAG</a>
        </div>
        <div class="prod_name">
          <h4><?php echo $rs->product_name;?></h4>
          <!-- <p class="prod_subname">Vaamsi Women Beige & Black</p> -->
        </div>
          <?php if($rs->mrp != $rs->price) { ?>
            <div class="prod_price hidden-xs">
              <p class="prod_actprice third_width"> &#8377; <?php echo $rs->price;?></p>
              <p class="prod_cutprice third_width"><strike>&#8377; <?php echo $rs->mrp;?></strike></p>
              <p class="prod_off third_width"> (<?php echo $percentage; ?>% off)</p>
            </div>
            <div class="prod_price hidden-lg hidden-md- hidden-sm">
              <p class="prod_actprice"> &#8377; <?php echo $rs->reseller_amount;?></p>
            </div>
             <?php } 
            else {?>
             <div class="prod_price">
              <p class="prod_actprice"> &#8377; <?php echo $rs->reseller_amount;?></p>
            </div>
           <?php }?>
              <div class="prod_size hidden-xs"><?php echo get_size($rs->id);?></div>
              <div class="add_wish_message<?php echo $rs->id;?> wish_msg"></div>
        </div>
           <?php } 
          else {?>
          <div class="product_hover">
            <div class="prod_image">
              <a href="<?php echo base_url('product_info/'.$rs->prod_display);?>">
                <img data-original="<?php echo base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);?>">
              </a>
            </div>
             <h4><?php echo $rs->product_name;?></h4>
              <?php if($rs->mrp != $rs->price) { ?>
                <div class="prod_price hidden-xs">
                  <p class="prod_actprice third_width"> &#8377; <?php echo $rs->reseller_amount;?></p>
                  <p class="prod_cutprice third_width"><strike>&#8377; <?php echo $rs->mrp;?></strike></p>
                  <p class="prod_off third_width"> (<?php echo $percentage; ?>% off)</p>
                </div>
                <div class="prod_price hidden-lg hidden-md hidden-sm">
                  <p class="prod_actprice"> &#8377; <?php echo $rs->reseller_amount;?></p>
                </div>
               <?php } 
              else {?>
               <div class="prod_price">
                <p class="prod_actprice"> &#8377; <?php echo $rs->reseller_amount;?></p>
                </div>
             <?php }?>
              <div class="stock">Out of stock</div>
            </div>
         <?php }  ?>

	</li>

	<?php }

?>
<script src="<?php echo base_url();?>assets/js/lazyload.transpiled.min.js"></script>
<script type="text/javascript">
	new LazyLoad();
</script>