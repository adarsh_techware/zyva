<head>
   <?php  foreach ($prod_info->gallery as $rs) {}?>
   <meta property="og:type" content="product">
   <meta property="og:title" content="<?php echo $prod_info->product_name;?>">
   <meta property="og:description" content="<?php echo $prod_info->description;?>">
   <meta property="og:url" content="https://zyva.in/product_info/<?php echo $prod_info->product_name;?>">
   <meta property="og:image" content="<?php echo base_url('admin/'.$prod_info->gallery[0]->product_image.$rs->format); ?>">
   <meta property="fb:app_id" content="1481833365364178">
   <meta property="product:price:amount" content="<?php echo $prod_info->price;?>">
   <meta property="product:price:currency" content="Rs">
</head>


<?php
$userid = $this->session->userdata('user_id');
$sess_id= $this->session->userdata('session_id');
/*echo '<pre>';
print_r($prod_info);
die();*/
?>
<head>
 <!-- for slick slider    -->
   <!-- <link href="<?php echo base_url(); ?>assets/css/slick/slick.css" rel="stylesheet" type="text/css" /> -->
   <!-- <link href="<?php echo base_url(); ?>assets/css/slick/slick-theme.css" rel="stylesheet" type="text/css" /> -->
</head>

<style type="text/css">
   .checkbox-custom, .checkbox-custom-label, .radio-custom, .radio-custom-label{
   margin: 0px !important;
   padding-top: 0px;
   }
</style>
<body>

   <form method="POST" id="cart"  data-parsley-validate="" class="validate adredd_val">
      <div class="starter-template">
         <div class="container">
            <div class="col-md-12 product_outer zoom_page" style="margin-top:120px;">
               <div class="col-md-6  col-sm-6 col-xs-12 remove_left_pad">
                  
                  <div class="zoom_pic_section">
                     <div  class="small_pic_list">
                        <!-- Small round pic items -->
                        <?php
                           foreach ($prod_info->gallery as $rs) { ?>
                        <div class="small_pic_item active">
                           <ul>
                              <li>
                                 <a href="#" class="small-thumbnail"><img data-image="<?php echo base_url('admin/'.$rs->product_image.$rs->format);?>" src="<?php echo base_url('admin/'.$rs->product_image.'_95x143'.$rs->format);?>" alt="Image"></a>
                              </li>
                           </ul>
                        </div>
                        <?php } ?>
                        <!-- Small round pic items -->
                     </div>
                     <div class="product_image">
                        <div class="thumb-image">
                        
                           <img src="<?php echo base_url('admin/'.$prod_info->gallery[0]->product_image.$rs->format); ?>" 
                              data-imagezoom="true" id="DataDisplay"> 
                        </div>
                        <div class="share_outer">
                           <div class="share">
                              <img class="share_via" src="<?php echo base_url();?>assets/images/share_via.png">
                           </div>
                           <div class="share_icons">
                              <div class="close_share_outedr">
                                 <div class="social_icon social_pos">
                                    <ul class="social">
                                       <li>
                                        <a href="http://www.facebook.com/sharer.php?u=<?php echo base_url($this->uri->uri_string()); ?>" target="_blank">
                                          <i class="fa fa-facebook fa-lg wht_clr" aria-hidden="true"></i>
                                        </a>
                                      </li>

                                       <li>
                                        <a href="http://twitter.com/share?text=<?php echo $prod_info->product_name; ?>&url=<?php echo base_url($this->uri->uri_string()); ?>">
                                          <i class="fa fa-twitter fa-lg wht_clr" aria-hidden="true"></i>
                                        </a>
                                      </li>
                                      
                                       <!-- <li><i class="fa fa-linkedin fa-lg wht_clr" aria-hidden="true"></i></li> -->
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="close_share">  
                           <img class="into" src="<?php echo base_url();?>assets/images/into.png">
                        </div>
                     </div>
                  </div>
                  <!-- .............................. ZOOMING SECTION END..................... -->
               </div>
               <div class="col-md-6  col-sm-6 col-xs-12">
                  <div class="dress_detail">
                     <input type="hidden"  name="customer_id" value="<?php echo $this->session->userdata('user_id')?>" >
                     <input type="hidden" id="product_id"  name="product_id" value="<?php echo $prod_info->id;?>" >
                     <h4 class="top_mar_zero"><?php echo $prod_info->product_name;?></h4>
                     <p>Product Code <b><?php echo $prod_info->product_code;?></b></p>
                     <h4 class="women_rupee"> &#8377; <?php echo $prod_info->price;?></h4>
                     <input type="hidden"  name="price" value="<?php echo $prod_info->price;?>" >
                     <p>Inclusive of all local taxes </p>
                  </div>
                  <br>
                  <!--out of stock -->
                  <?php 
                  if($prod_info->quantity->quantity=='0' || $prod_info->quantity->quantity=='') { ?>
                  <div class="stock_out">Out of stock</div>
                  <?php } else { ?>     
                  <!--out of stock -->
                  <div class="col-md-12 bord_btm">
                     <div class="col-md-6 pad_left">
                        <div class="bord_ft">
                           <p class="colr">Colour</p>
                           <div class="color_round">
                              <div class="color_rounda">
                                 <?php
                                    
                                      foreach($prod_info->color as $color)  {
                                      ?>
                                 <div class="color_pick_outer" id="btn1" data-id="<?php echo $color->id;?>">
                                    <div class="color_pick_a" style="background-image: url('<?php echo $color->image;?>')" ></div>
                                 </div>
                                 <?php } ?>
                                 <input type="hidden"  name="color_id" value="<?php echo $prod_info->color[0]->id;?>" id="color_id" /> 
                                 
                                      
                              </div>
                           </div>
                           
                           <p class="colr">size <span class="siz_guide" data-toggle="modal" data-target="#size_guide_modal">view size guide</span></p>
                           <div class="sizes_outs size_list">
                              <?php 
                                   foreach($prod_info->size as $size)  { 
                                    if($size->size_type!='custom'){?>
                              <div class="sizes_out pad_left">
                                 <div  class="sizes size_new_opt" id="btn" data-id="<?php echo $size->size_id;?>"> <?php echo $size->size_type; ?> </div>
                              </div>
                              <?php } } ?> 
                           </div>
                           <input type="hidden" id="size_id"  name="size_id" value=""/>
                           <p class="colr">quantity</p>
                           <div class="quantity_dress">
                              <select class="quant_dress" name="quantity" id="quantity" >
                                <?php for($i=1;$i<=10;$i++) { ?>
                                  <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php } ?>
                                 <!-- <option>1</option>
                                 <option>2</option>
                                 <option>3</option> -->
                                
                              </select>
                           </div>
                           &nbsp
                        </div>
                     </div>
                     <div class="col-md-6 remove_right_pad">
                     <div class="cartmsg_height">
                        <div class="add_cart_success front_style errormsg"></div>
                      </div>
                        <div class="button_list">

                           <!-- For stiching checkbox -->
                           <div class="check_outer check_box stiching_box" <?php if($prod_info->stitching_charge=='0') {?> style="display: none;" <?php } ?>>
                              <div class="check_lft">
                                 <!-- //<input type="hidden" name="stitching_on" id="checkbox-2" > -->
                                 <!-- <input id="checkbox-2" class="checkbox-custom" name="" type="checkbox" > -->
                                 <input type="checkbox" id="chkPassport"  name="stitching_on" class="checkbox-custom" data-id="<?php echo $prod_info->custom->id; ?>">
                                 <label for="chkPassport" class="checkbox-custom-label stich_color" data-toggle="modal" style="">Stitching</label>
                              </div>
                           </div>
                           <!-- stiching checkbox ends -->

                           
                           <!--  <div class="front_style"></div> -->
                           <button id="addtocart" class="add_bag add_bag_clr aaa" style="display:none" type="button">
                              <span class="icon_but">
                                 <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                              </span> 
                              add to bag
                           </button>

                           <button id="addtocart1" class="add_bag add_bag_clr bbb"  >
                               <span class="icon_but">
                                 <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                              </span> 
                              add to bag
                           </button>

                           <?php if($this->session->userdata('user_id')) { ?>

                           <button id="addtowishlist" class="add_bag">
                              <span class="icon_but"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                              add to wishlist
                           </button>

                           <?php } 

                           else { ?>
                              <!-- <a class="call_signin">
                                 <button class="add_bag add_bag_clr bbb">
                                    <span class="icon_but"><i class="fa fa-shopping-bag" aria-hidden="true"></i></span> 
                                    add to bag
                                 </button>
                              </a> -->
                              <a class="call_signin">
                                 <button class="add_bag" type="button">
                                    <span class="icon_but"><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                                    add to wishlist
                                 </button>
                              </a>
                           <?php } ?>
                           <button class="add_bag_enquiry" data-toggle="modal" data-target="#enqireModal" type="button">
                                 <span class="icon_but"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                              enquire now
                           </button>
                        </div>
                     </div>
                  </div>
                  <?php }   ?>       
                  <div class="col-md-12 pad_lft_more">
                     <div class="tabs_des">
                        <ul class="nav nav-tabs caps_tab">
                           <li class="active"><a data-toggle="tab" href="#home">description</a></li>
                           <li><a data-toggle="tab" href="#menu1">features</a></li>
                           <li><a data-toggle="tab" href="#menu2">payment modes</a></li>
                        </ul>
                        <div class="tab-content txt_clr">
                           <div id="home" class="tab-pane fade in active">
                              <p><?php echo $prod_info->description;?></p>
                           </div>
                           <div id="menu1" class="tab-pane fade">
                              <p><?php echo $prod_info->feature; ?></p>
                           </div>
                           <div id="menu2" class="tab-pane fade">
                              <h5>COD / Pick from Store / Pick from Re-seller</h5>
                              <!--<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--  </form> -->
               <div class="preview col">
                  <div class="col-md-12">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </form>
   
<!-- ======================== SLIDER START================================= -->
   <div class="dress_slider">
      <button class="slider_head">you may also like</button>
      <div class="container">
         <div class="col-md-12">
            <div class="span12">
               <div class="slider">
               <?php
                  foreach ($cate_gallery as $rs) { ?>
                  <div class="slide">
                    <a href="<?php echo base_url('product_info/'.$rs->prod_display); ?>">
                     <div class="slide">
                        <img src="<?php echo base_url('admin/'.$rs->product_image.'_400x600'.$rs->format);?>" class="img-responsive center-block full_img">
                        <div class="slider_cap"><?php echo $rs->product_name; ?></div>
                        <div class="slider_rs">&#8377;<?php echo $rs->price; ?></div>
                     </div>
                   </a>
                  </div>
                  <?php } ?>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--////////////////////// New salwar  slider ends ////////////////////////////////
   <!-- Modal for enquiry starts -->
   
   <div class="modal fade" id="enqireModal" role="dialog">
      <div class="modal-dialog enqire_modal">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-body login_modal">
               <div class="reg_modals">
                  <div class="col-md-12 no_padng">
                     <div class="lft_reg">
                        <div class="padng_outer pad_bot">
                           <div class="col-md-2 col-sm-12 col-xs-12 no_padng enq_logo">
                              <img class="" src="<?php echo base_url();?>assets/images/zya_logo_blck.png">
                              <!-- <img class="" src="images/enq_border.png"> -->
                           </div>
                           <div class="col-md-8 col-sm-11 col-xs-11 enq_logo_pad">
                              <p>Please provide as much information as possible for us to help you with your enquiry.</p>
                           </div>
                           <div class="col-md-2 col-sm-1 col-xs-1 no_padng">
                              <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url();?>assets/images/modal_close.png"></button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12 no_padng">
                     <form method="POST" id="enquiry1" data-parsley-validate="">
                        <div class="lft_reg">
                           <div class="col-md-6 no_padng">
                              <div class="padng_outer pad_bot">
                                 <input type="hidden"   name="product_id" value="<?php echo $prod_info->id;?>">


                                 <div class="enq_width">
                                      <input type="text" name="first_name" class="text_box no_margn" placeholder="First Name" 
                                       value="<?php echo $data->name; ?>"  
                                       data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"  
                                       required="" data-parsley-required-message="Please insert first name."
                                       data-parsley-errors-container="#fname" />
                                       <div id="fname" class="enq_error"></div>
                                  </div>
                                  

                                  <div class="enq_width">
                                      <input type="text" name="country" class="text_box no_margn" placeholder="Country"  
                                       data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"  
                                       required="" data-parsley-required-message="Please insert country."
                                       data-parsley-errors-container="#ctry" />
                                       <div id="ctry" class="enq_error"></div>
                                  </div>
                                  

                                  <div class="enq_width">
                                      <input type="email" name="email" class="text_box no_margn" placeholder="Email"  
                                       required="" data-parsley-required-message="Please insert email."
                                       tabindex="4" data-parsley-errors-container="#email" />
                                       <div id="email" class="enq_error"></div>
                                  </div>
                                  

                                 <!-- <input type="text" name="first_name" class="text_box" placeholder="First Name" value="<?php echo $data->name; ?>"> -->
                                 <!-- <input type="text" name="country" class="text_box" placeholder="Country"> -->
                                 <!-- <input type="email" name="email" class="text_box no_margn" placeholder="Email" value="<?php echo $data->email; ?>"> -->
                              </div>
                           </div>
                           <div class="col-md-6 no_padng">
                              <div class="padng_outer">
                                 <div class="accc">
                                    <div class="name_width">
                                      <input type="text" name="last_name" class="text_box no_margn" placeholder="Last Name"  
                                       value="<?php echo $data->last_name; ?>" required="" 
                                       data-parsley-required-message="Please insert last name."
                                       data-parsley-errors-container="#lname" />
                                       <div id="lname" class="enq_error"></div>
                                     </div>

                                     <div class="enq_text">
                                        <textarea  name="enquiry" class="enq_textarea" required="" placeholder="Enquiry here"
                                        data-parsley-required-message="Please insert enquiry." 
                                        data-parsley-errors-container="#eng_text"></textarea>
                                        <div id="eng_text"></div>
                                    </div>

                                    <!-- <input type="text" name="last_name" class="text_box last_name_margin" placeholder="Last Name" value="<?php echo $data->last_name; ?>"> -->
                                    <!-- <textarea  name="enquiry" placeholder="Enquiry here" class="enq_textarea"></textarea>   -->
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12">
                            <div class="col-md-3"></div>
                             <div class="col-md-6">
                                <div class="enq_status"></div>
                              </div>
                             <div class="col-md-3 enq_submit">
                                <button id="enquires" class="enq_but">Submit</button>
                             </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Modal for enquiry end -->
   <!-- Modal for stiching start here -->

   <div class="modal fade" id="stichingModal" role="dialog" data-dismiss="modal" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg modal_stich">

         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-body login_modal">
               <div class="stich_modal_content">
                  <!-- <form method="POST" id="stitching1" > -->
                  <div class="col-md-12 no_padng">
                     <div class="stich_model">
                        <div class="padng_outer pad_bot ">
                           <div class="col-md-1 col-sm-11 col-xs-11 no_padng">
                           </div>
                           <div class="col-md-10 col-sm-11 col-xs-11 no_padng">
                              <div class="own_des">
                                 <img src="<?php echo base_url();?>assets/images/own_des.png">
                                 <h5>create your own design</h5>
                                 <p>Personalise your product by adding necklines, sleeves and other add-ons of your choice from 100's of styles.</p>
                              </div>
                           </div>
                           <div class="col-md-1 col-sm-1 col-xs-1 no_padng">
                              <button type="button" class="close" data-dismiss="modal"> <img src="<?php echo base_url();?>assets/images/modal_close.png"></button>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="col-md-12 no_padng">
                    <form action="" method="post" name="stitching_cat" id="stitching_cat">
                     <div class="stich_model">
                        <!-- multistep form -->
                        <div class="stepwizard col-md-offset-3">
                           <div class="stepwizard-row setup-panel">
                              <div class="stepwizard-step">
                                 <a href="#step-1" type="button" class="btn btn-default btn-circle active  btn-primary"></a>
                                 <p>Front</p>
                              </div>
                              <div class="stepwizard-step">
                                 <a href="#step-2" type="button" class="btn btn-default btn-circle active" disabled="disabled"></a>
                                 <p>Back</p>
                              </div>
                              <div class="stepwizard-step">
                                 <a href="#step-3" type="button" class="btn btn-default btn-circle active" disabled="disabled"></a>
                                 <p>Sleeve</p>
                              </div>
                              <div class="stepwizard-step">
                                 <a href="#step-4" type="button" class="btn btn-default btn-circle active" disabled="disabled"></a>
                                 <p>Hemline</p>
                              </div>
                              <div class="stepwizard-step">
                                 <div class="add_child"></div>
                                 <a href="#step-5" type="button" class="btn btn-default btn-circle active" disabled="disabled"></a>
                                 <p>Add-Ons</p>
                              </div>
                           </div>
                        </div>
                        <div class="row setup-content" id="step-1">
                           <div class="col-md-12">
                              <!-- <div class=""> -->
                              <div class="front_stich" >
                                 <ul>
                                    <?php 
                                    foreach ($front as $r1) { ?>
                                    <li>
                                       <img src="<?php echo $r1->image?>" class="my_class"  data-id="<?php echo $r1->id; ?>">
                                       <div class="stich_images_front" id="round_front_<?php echo $r1->id; ?>" style="display: none" ><img src="<?php echo base_url();?>assets/images/round_bg.png" class="round_bg"></div>
                                       <p><?php echo $r1->discription?></p>
                                    </li>
                                    <?php } ?>
                                    <input type="hidden"  id="front_id" name="front_id" value="" >
                                 </ul>
                              </div>
                              <div class="btn_align wizard_measure">
                                 <p class="wiz_back">  
                                    <i class="fa fa-angle-left fa-2x fnt_inr" aria-hidden="true">
                                    </i> &nbsp; Back
                                 </p>
                                 <div class="next_buts"> 
                                    <button class="btn btn-primary nextBtn btn-lg enq_but hg_own" type="button"  id="btn1">Next</button>
                                    <span id="front_style"></span>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row setup-content" id="step-2">
                           <div class="col-md-12">
                              <div class="front_stich">
                                 <ul>
                                    <?php 
                                       foreach ($back as $r2) {
                                       
                                       ?>
                                    <li>
                                       <img src="<?php echo $r2->image?>" class="my_class1"  data-id="<?php echo $r2->id; ?>">
                                       <div class="stich_images_back" id="round_back_<?php echo $r2->id; ?>" style="display: none" ><img src="<?php echo base_url();?>assets/images/round_bg.png" class="round_bg"></div>
                                       <p><?php echo $r2->discription?></p>
                                    </li>
                                    <?php } ?>
                                    <input type="hidden"  id="back_id" name="back_id" value="" >
                                 </ul>
                              </div>
                              <div class="btn_align wizard_measure">
                                 <p class="wiz_back">  <i class="fa fa-angle-left fa-2x fnt_inr" aria-hidden="true"></i> &nbsp; Back</p>
                                 <div class="next_buts"> 
                                    <button class="btn btn-primary nextBtn btn-lg enq_but hg_own" type="button" >Next</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--/////////////// step3 start ////////////// -->
                        <div class="row setup-content" id="step-3">
                           <div class="col-md-12">
                              <!-- start  sleeve page start -->
                              <div class="front_stich">
                                 <ul>
                                    <?php 
                                       foreach ($sleeve as $r3) {
                                        
                                       
                                       ?>
                                    <li class="sleeve_list">
                                       <img src="<?php echo $r3->image?>" class="my_class2"  data-id="<?php echo $r3->id; ?>">
                                       <div class="stich_images_sleeve" id="round_sleeve_<?php echo $r3->id; ?>" style="display: none" ><img src="<?php echo base_url();?>assets/images/round_bg.png" class="round_bg"></div>
                                       <p><?php echo $r3->discription?></p>
                                    </li>
                                    <?php } ?>
                                    <input type="hidden"  id="sleeve_id" name="sleeve_id" value="" >
                                 </ul>
                              </div>
                              <!-- start  sleeve page end -->
                              <div class="btn_align wizard_measure">
                                 <p class="wiz_back">  <i class="fa fa-angle-left fa-2x fnt_inr" aria-hidden="true"></i> &nbsp; Back</p>
                                 <div class="next_buts"> 
                                    <button class="btn btn-primary nextBtn btn-lg enq_but hg_own" type="button" >Next</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--/////////////// step3 end ////////////// -->
                        <div class="row setup-content" id="step-4">
                           <div class="col-md-12">
                              <div class="front_stich">
                                 <ul>
                                    <?php  
                                       foreach ($hemline as $r4) {
                                         
                                       
                                       ?>
                                    <li>
                                       <img src="<?php echo $r4->image?>" class="my_class3"  data-id="<?php echo $r4->id; ?>">
                                       <div class="stich_images_hemline" id="round_hemline_<?php echo $r4->id; ?>" style="display: none" ><img src="<?php echo base_url();?>assets/images/round_bg.png" class="round_bg"></div>
                                       <p><?php echo $r4->discription?></p>
                                    </li>
                                    <?php } ?>
                                    <input type="hidden"  id="hemline_id" name="neck_id" value="" >
                                 </ul>
                              </div>
                              <div class="btn_align wizard_measure">
                                 <p class="wiz_back">  <i class="fa fa-angle-left fa-2x fnt_inr" aria-hidden="true"></i> &nbsp; Back</p>
                                 <div class="next_buts"> 
                                    <button class="btn btn-primary nextBtn btn-lg enq_but hg_own" type="button" >Next</button>
                                 </div>
                              </div>
                           </div>
                        </div>

                       
                        <div class="row setup-content" id="step-5">
                           <div class="col-md-12 pad">
                              <!-- <div class="frst_sect_adds"> -->

                              
                              
                              


                              <div class="front_stich ">
                                 <div class="col-md-3"></div>

                                 <div class="col-md-3">
                                    <div class="add_ons">
                                       <h5 class="prewash_head lining"><?php echo $adons[0]->adons_type;?></h5>
                                       <ol class="stich_ol">
                                          <?php 
                                           $row1=get_addons_list($adons[0]->id);
                                          ?>
                                          <li>
                                             <?php  foreach ($row1 as $a1) {
                                                ?>
                                             <img src="<?php echo $a1->image?>" class="add_a"  data-id="<?php echo $a1->id; ?>">
                                             <div class="stich_images addon_class_one" id="round_addon_<?php echo $a1->id; ?>" style="display: none" ><img src="<?php echo base_url();?>assets/images/round_bg.png" class="round_bg"></div>
                                             <p><?php echo $a1->discription; ?></p>
                                             <h6>Rs.<?php echo $a1->charge; ?>/-</h6>
                                             <?php } ?>
                                          </li>
                                          <input type="hidden"   id="addon_id"  name="add_ons" value="" > 
                                       </ol>
                                    </div>
                                 </div>
                                 <div class="col-md-3">
                                    <div class="add_ons">
                                       <h5 class="prewash_head lining"><?php echo $adons[1]->adons_type;?></h5>
                                       <ol class="stich_ol">
                                          <?php 
                                             $row2=get_addons_list($adons[1]->id);
                                           
                                             
                                             ?>
                                          <li>
                                             <?php  foreach ($row1 as $a2) {
                                                ?>
                                             <img src="<?php echo $a2->image?>" class="add_a1"  data-id="<?php echo $a2->id; ?>">
                                             <div class="stich_images addon_class_two" id="round_addon1_<?php echo $a2->id; ?>" style="display: none" ><img src="<?php echo base_url();?>assets/images/round_bg.png" class="round_bg"></div>
                                             <p><?php echo $a2->discription; ?></p>
                                             <h6>Rs. <?php echo $a2->charge; ?>/-</h6>
                                             <?php } ?>
                                          </li>
                                          <input type="hidden"   id="addon1_id"  name="add_ons1" value="" > 
                                       </ol>
                                    </div>
                                 </div>
                                 <div class="col-md-3"></div>
                                 <div class="col-md-12">
                                    <h5 class="placket"><?php echo $adons[2]->adons_type;?></h5>
                                 </div>
                                 <div class="col-md-2"></div>
                                 <div class="col-md-8 add_width">
                                    <ol class="stich_ol">
                                       <?php 
                                          $row3=get_addons_list($adons[2]->id);
                                          // print_r($row1);
                                          foreach ($row3 as $a3) 
                                          {
                                          
                                          ?>
                                       <li class="placket_list">
                                          <?php  
                                             ?>
                                          <img src="<?php echo $a3->image?>" class="add_a2"  data-id="<?php echo $a3->id; ?>">
                                          <div class="stich_images addon_class_three" id="round_addon2_<?php echo $a3->id; ?>" style="display: none" ><img src="<?php echo base_url();?>assets/images/round_bg.png" class="round_bg"></div>
                                          <p><?php echo $a3->discription; ?></p>
                                          <h6>Rs. <?php echo $a3->charge; ?>/-</h6>
                                       </li>
                                       <?php } ?>
                                       <input type="hidden"   id="addon2_id"  name="add_ons2" value="" > 
                                    </ol>
                                 </div>
                                 <div class="col-md-2"></div>
                                 <div class="col-md-12">
                                    <h5 class="placket"><?php echo $adons[3]->adons_type;?></h5>
                                 </div>
                                 <div class="col-md-12">
                                    <ol class="stich_ol">
                                       <?php 
                                          $row3=get_addons_list($adons[3]->id);
                                          // print_r($row1);
                                          foreach ($row3 as $a4) 
                                          {
                                          
                                          ?>
                                       <li>
                                          <img src="<?php echo $a4->image?>" class="add_a3"  data-id="<?php echo $a4->id; ?>">
                                          <div class="stich_images addon_class_four" id="round_addon3_<?php echo $a4->id; ?>" style="display: none" >
                                             <img src="<?php echo base_url();?>assets/images/round_bg.png" class="round_bg">
                                          </div>
                                          <p><?php echo $a4->discription; ?></p>
                                          <h6>Rs. <?php echo $a4->charge; ?>/-</h6>
                                       </li>
                                       <?php } ?>
                                       <input type="hidden"   id="addon3_id"  name="add_ons3" value="" > 
                                    </ol>
                                 </div>
                                 <div class="col-md-12">
                                    <h5 class="placket padd_bott">SPECIAL INSTRUCTIONS</h5>
                                    <h4 class="spcl_ins">Tell us if you have any changes in measurement / special design requirement in the box below.</h4>
                                 </div>
                                 <div class="col-md-2"></div>
                                 <div class="col-md-7">
                                    <textarea rows="3" name="specialcase"></textarea>
                                 </div>
                                 <div class="row">
                                  <div class="col-md-6" style="text-align:center"><button class="btn btn-primary" id="chkPassport1" name="stitch_id" type="button">Free Stitching</button></div>
                                  <div class="col-md-6" style="text-align:center"><button class="btn" data-toggle="modal" data-target="#measurement" type="button">My Measurement</button></div>
                                 </div>
                                 <!-- <div class="stichmesh" data-toggle="modal" data-target="#measurement">Add Stiching Measurement</div>
                                 <div class="stichmesh">Free Stitching
                                    <input type="checkbox" id="chkPassport1"  name="stitch_id" >
                                 </div> -->
                                 <div class="col-md-2"></div>
                              </div> 

                              <input type="hidden" value="0" name="stitch_id" id="stitch_id">


                              <!-- </div> -->
                              <div class="btn_align wizard_measure padd_top">
                                 <p class="wiz_back">  <i class="fa fa-angle-left fa-2x fnt_inr" aria-hidden="true"></i> &nbsp; Back</p>
                                 <!-- <span class="front_style"></span> -->
                                 <div class="next_buts">
                                    <!-- <button id="stitches" class="btn btn-primary nextBtn btn-lg enq_but hg_own" data-dismiss="modal" type="button" data-toggle="modal" data-target="#rateModel">Submit</button> -->
                                    <button id="stitches" class="btn btn-primary nextBtn btn-lg enq_but hg_own" data-dismiss="" type="button" data-toggle="modal" data-target="#rateModel">Submit</button>
                                 </div>
                                 <span class="front_styles"  style="color:red"></span>
                              </div>
                           </div>
                        </div> 
                        <!--- end others -->
                     </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div> 

   <!-- ********************************************************* -->
   <!-- Modal -->
 
   <div class="modal fade" id="measurement" role="dialog"  data-dismiss="modal" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Measurement </h4>
            </div>
            <form method="post" class="measurement">
               <div class="modal-body">
                  
                  <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 bord_rhts">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="women_sizes">
                            <img src="<?php echo base_url();?>assets/images/women_top.png" class="top_img">
                            </div>

                        </div>
                         <div class="col-md-9  col-sm-12 col-xs-12">
                            <div class="women_sizes_detail">
                              <h5>Women’s sizes</h5>
                              <h5 class="top_wear">Top wear</h5>
                              <input type="hidden"  name="customer_id" value="<?php echo $this->session->userdata('user_id')?>" >
                              <ol class="custom-counter">

                                <li>
                                  <div class="prev_size"><p>1</p></div>
                                  <div class="size_name">Bust Size</div> 
                                  <div class="size_input">
                                      <input type="text" class="measure_input"  name="bustsize" placeholder="eg:30 cm"  
                                       data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"  
                                       required="" data-parsley-required-message="Please insert bust size."
                                       data-parsley-errors-container="#bust_size1" value="<?php echo $measurement->bustsize; ?>"/>
                                  </div>
                                  <div id="bust_size1" class="custom_measure_parse_error"></div>
                                </li>

                                <li>
                                  <div class="prev_size"><p>2</p></div>
                                  <div class="size_name">Waist Size</div>
                                  <div class="size_input">
                                    <input type="text" class="measure_input"  name="waistsize"   placeholder="eg:30 cm"  
                                    data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"  
                                    required="" data-parsley-required-message="Please insert waist size."
                                    data-parsley-errors-container="#waist_size1" value="<?php echo $measurement->waistsize; ?>"/>
                                  </div>
                                  <div id="waist_size1" class="custom_measure_parse_error"></div>
                                </li>

                                <li>
                                  <div class="prev_size"><p>3</p></div>
                                  <div class="size_name">Hip Size</div>
                                  <div class="size_input">
                                    <input type="text" class="measure_input"  name="hipsize"   placeholder="eg:30 cm"  
                                    data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"  
                                    required="" data-parsley-required-message="Please insert hip size."
                                       data-parsley-errors-container="#hip_size1" value="<?php echo $measurement->hipsize; ?>"/>
                                  </div>
                                  <div id="hip_size1" class="custom_measure_parse_error"></div>
                                </li> 

                              </ol>

                            </div>

                        </div>


                    </div>
                </div>

                <div class="col-md-6 col-sm-3 col-sm-12 details_pad">
                     <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="women_sizes">
                            <img src="<?php echo base_url();?>assets/images/women_bot.png">

                            </div>

                        </div>

                        <div class="col-md-9  col-sm-12 col-xs-12">
                          <div class="women_sizes_detail">
                            <h5>Women’s sizes</h5>
                            <h5 class="top_wear">bottom wear</h5>

                            <ol class="custom-counter">
                            <li>
                              <div class="prev_size"><p>1</p></div>
                              <div class="size_name">Pant Waist</div> 
                              <div class="size_input">
                                <input type="text" class="measure_input"  name="pantwaist"   placeholder="eg:30 cm" 
                                data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"
                                required="" data-parsley-required-message="Please insert pant waist size."
                                data-parsley-errors-container="#pant_waist1" value="<?php echo $measurement->pantwaist; ?>"/>
                              </div>
                              <div id="pant_waist1" class="custom_measure_parse_error"></div>
                            </li>  

                            <li>
                              <div class="prev_size"><p>2</p></div>
                              <div class="size_name">Hip</div>
                              <div class="size_input">
                                <input type="text" class="measure_input"   name="hip"  placeholder="eg:30 cm"   
                                data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"
                                required="" data-parsley-required-message="Please insert hip size."
                                data-parsley-errors-container="#pant_hip1" value="<?php echo $measurement->hip; ?>"/>
                              </div>
                              <div id="pant_hip1" class="custom_measure_parse_error"></div>
                            </li> 

                            <li>
                              <div class="prev_size"><p>3</p></div>
                              <div class="size_name">Inseam</div>
                              <div class="size_input">
                                <input type="text" class="measure_input"  name="inseam"  placeholder="eg:30 cm" 
                                data-parsley-pattern="^[a-zA-Z\ 0-9  \/]+$"
                                required="" data-parsley-required-message="Please insert inseam size." 
                                data-parsley-errors-container="#inseam1" value="<?php echo $measurement->inseam; ?>"/>
                              </div>
                              <div id="inseam1" class="custom_measure_parse_error"></div>
                            </li>
                            </ol>
                          </div>

                        </div>
                    </div>
                </div>

            </div>
                  <div class="modal-footer">
                     <div class="errormsgs" style="color:red"></div>
                     <input type="checkbox" id="chkPassport2" value="1" name="stitch_id">Update measurement
                     <button class="btn btn-primary save1 measure" id="savemesh1" type="button">Save</button>
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
               </div>
               
               
         </form>
      </div>
   </div>
   </div>
   <!-- **************************************************************** -->
   
   <!-- Modal for Stiching end -->
   <!-- model for rate start-->
   <div id="order_template">
   </div>
   
   
   <script>
   $('.add_bag').attr("disabled", "disabled").off('click');
      
       $('#chkPassport').click(function () {
       
         var chkstich = document.getElementById("chkPassport");
         $("#chkPassport").val("checked");                
         $(".aaa").hide();
         $(".bbb").show();
       });
      // });

      $('.sizes').click(function () { 
         $("#chkPassport").attr('disabled','disabled');
            
       });

            
      /*custom scrollbar plugin */
         
         $("#stichingModal .modal-body").mCustomScrollbar({
            setHeight:640,
            theme:"dark-3"
         });
      
      //*******Free stitching********//
      
      $('#chkPassport1').click(function () {
         var chkstich = document.getElementById("chkPassport1");
         var a=$("#chkPassport1").val("checked"); 
      });



      
                     
   </script>
   