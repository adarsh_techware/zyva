
<div class="starter-template">
   <!-- wrapper -->
   <div class="full_wdth_wrapper hidden-xs"></div>
   <!-- wrapper end -->
   <!-- tab bar  -->
   <div class="inner_tab margin_top_60">
      <div class="container">
         <div class="row">
            <div class="navs_tabs">
               <ul>
                  <li> <a>Home</a></li>
                  <li>/</li>
                  <li class="women_clr"><a href="<?php echo base_url('product/'.$cat_id); ?>"><?php echo ucfirst($cat_id); ?></a></li>
                  <?php if($sub_id!=''){?><li>/</li>
                  <li class="women_clr"><a href="<?php echo base_url('product/'.$cat_id.'/'.$sub_id); ?>"><?php echo ucfirst($sub_id); ?></a></li>
                  <?php } ?>
               </ul>
            </div>
            <div class="outline">
              <div class="middle">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">
                  <div class="jq_sidebar_fix">
                     <div class="tab_out">
                        <p><span>category</span></p>

                        <div class="fancy-collapse-panel">
                              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                              <?php
                              foreach ($Category as $rs) { ?>
                              

                                  <div class="panel panel-default  women_panel">
                                      <div class="panel-heading no_head" role="tab" id="heading_<?php echo $rs->id; ?>">
                                        <div <?php if ( $this->uri->uri_string() == 'product/'.$rs->display_name ) : ?> class="active_menu" <?php endif; ?> >
                                          <h4 class="panel-title fnt_sze">
                                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $rs->id; ?>" <?php if($cat_id==$rs->display_name){?> aria-expanded="true" <?php } else {?> aria-expanded="true" <?php } ?> aria-controls="collapse_<?php echo $rs->id; ?>"><?php echo $rs->category_name; ?>
                                              </a>
                                          </h4>
                                        </div>
                                      </div>
                                      <div id="collapse_<?php echo $rs->id; ?>" class="panel-collapse collapse <?php if($cat_id==$rs->display_name){?> in <?php } ?>" role="tabpanel" aria-labelledby="heading<?php echo $rs->id; ?>">
                                          <div class="panel-body no_bord">
                                              <ul class="tab_list">
                                              <?php foreach ($rs->sub as $row) { ?>
                                                 
                                                  <li><a href="<?php echo base_url('product/'.$rs->display_name.'/'.$row->sub_display); ?>"><?php echo $row->sub_category; ?></a></li>
                                                  <?php } ?>
                                                 
                                              </ul>
                                          </div>
                                      </div>
                                  </div>
                                  <?php } ?>
                                  
                              </div>
                          </div>


                        
                        <p><span>filter</span></p>
                        <div class="layout">
                           <p>Price</p>
                          <div class="price_range">
                            <div id="slider"  class="ranger"></div>
                            <input type="text" class="keypress_0" name="input-with-keypress-0" id="input-with-keypress-0">
                            <input type="text" class="keypress_1" name="input-with-keypress-1" id="input-with-keypress-1">
                          </div>

                        </div>
                        <form action="" method="POST">
                           <div class="price_lst">
                              <p>Size</p>
                              <div class="sizes_outs" id="btn">
                                 <?php
                                    foreach ($size as $rs) {
                                    ?>
                                 <div class="sizes_out">
                                    <div class="sizes size_opt" data-id="<?php echo $rs->id;?>"><?php echo $rs->size_type; ?></div>
                                    <input type="hidden"  id="size"  name="size" value="<?php echo $rs->size_type; ?>" >
                                 </div>
                                 <?php } ?>
                                 
                              </div>
                              
                           </div>
                        </form>
                     </div>
                   </div>
                </div>
                <!--  <div id="search_content"> -->
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                  <div id="content" class="contents">
                     <div class="sort_by hidden-xs">
                        <p>Sort by</p>
                        <div class="select-style">
                           <select name="sort" id="filter_search">
                              <option value="product">Newest Product</option>
                              <option value="high">High Price</option>
                              <option value="low">Low Price</option>
                           </select>
                        </div>
                     </div>

                     <div class="tab-content contnt_tabs">
                        <div id="search_content">
                          <div id="price_content1">
                              <?php
                                 if($product) {   ?>
                              <div id="home" class="tab-pane fade in active box_shdw pad_nill">
                              <ul id="port" class="clearfix">
                                 <?php
                                    $i = 1;
                                    foreach ($product as $rs) {
                                      $percentage= floor((($rs->mrp - $rs->price)*100)/$rs->mrp) ;
                                    ?>
                                 
                                    <li>
                                    <?php if($rs->quantity!=0) { ?>
                                    <div class="hover_div">
                                     <div class="prod_image">
                                      <a href="<?php echo base_url('product_info/'.$rs->prod_display);?>">
                                        <img data-original="<?php echo base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);?>">
                                      </a>
                                      </div>
                                    <div class="add_save hidden-xs">
                                      <?php if($this->session->userdata('user_id')) { ?>
                                      <a  id="<?php echo $rs->id;?>" class="prod_save add_wish">SAVE</a>
                                      <input type="hidden" id="prod_id" value="<?php echo $rs->id;?>">
                                      <?php } 
                                      else {?>
                                      <a id="call_signin" class="prod_save call_signin">SAVE</a>
                                      <?php } ?>
                                      <a href="<?php echo base_url('product_info/'.$rs->prod_display);?>" class="prod_add">ADD TO BAG</a>
                                    </div>
                                    <div class="prod_name">
                                      <h4><?php echo $rs->product_name;?></h4>
                                      <!-- <p class="prod_subname">Vaamsi Women Beige & Black</p> -->
                                    </div>
                                      <?php if($rs->mrp != $rs->price) { ?>
                                        <div class="prod_price hidden-xs">
                                          <p class="prod_actprice third_width"> &#8377; <?php echo $rs->price;?></p>
                                          <p class="prod_cutprice third_width"><strike>&#8377; <?php echo $rs->mrp;?></strike></p>
                                          <p class="prod_off third_width"> (<?php echo $percentage; ?>% off)</p>
                                        </div>
                                        <div class="prod_price hidden-lg hidden-md- hidden-sm">
                                          <p class="prod_actprice"> &#8377; <?php echo $rs->price;?></p>
                                        </div>
                                         <?php } 
                                        else {?>
                                         <div class="prod_price">
                                          <p class="prod_actprice"> &#8377; <?php echo $rs->price;?></p>
                                        </div>
                                       <?php }?>
                                          <div class="prod_size hidden-xs"><?php echo get_size($rs->id);?></div>
                                          <div class="add_wish_message<?php echo $rs->id;?> wish_msg"></div>
                                    </div>
                                       <?php } 
                                      else {?>
                                      <div class="product_hover">
                                        <div class="prod_image">
                                          <a href="<?php echo base_url('product_info/'.$rs->prod_display);?>">
                                            <img data-original="<?php echo base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);?>">
                                          </a>
                                        </div>
                                         <h4><?php echo $rs->product_name;?></h4>
                                          <?php if($rs->mrp != $rs->price) { ?>
                                            <div class="prod_price hidden-xs">
                                              <p class="prod_actprice third_width"> &#8377; <?php echo $rs->price;?></p>
                                              <p class="prod_cutprice third_width"><strike>&#8377; <?php echo $rs->mrp;?></strike></p>
                                              <p class="prod_off third_width"> (<?php echo $percentage; ?>% off)</p>
                                            </div>
                                            <div class="prod_price hidden-lg hidden-md hidden-sm">
                                            <p class="prod_actprice"> &#8377; <?php echo $rs->price;?></p>
                                            </div>
                                           <?php } 
                                          else {?>
                                           <div class="prod_price">
                                            <p class="prod_actprice"> &#8377; <?php echo $rs->price;?></p>
                                            </div>
                                         <?php }?>
                                          <div class="stock">Out of stock</div>
                                        </div>
                                     <?php }  ?>

                                    </li>

                                 
                                 <?php } ?>
                                 </ul>
                                 
                                  <div class="show_loader">
                                    <img src="<?php echo base_url();?>assets/images/loader2.gif">
                                  </div>
                                  <div class="sm_button">
                                    <div class="view_more_prod" id="view_more_product">SHOW MORE PRODUCTS</div>
                                  </div>
                                  
                                 <div class="result_div"></div>
                              </div>
                              <?php } 
                                 else { ?>
                                 <div class="noresult">
                                    <h1> No Results Found</h1>
                                    <img src="<?php echo base_url();?>assets/images/404.png">
                                    <h4>Please checkout our new arrivals <a href="<?php echo base_url('product/women'); ?>">Click here</a></h4>
                                  </div>
                                <?php } ?>
                           </div>
                        </div>


                        <div id="menu1" class="tab-pane fade box_shdw">
                           <h3>Menu 1</h3>
                           <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                        </div>
                        <div id="menu2" class="tab-pane fade box_shdw">
                           <h3>Menu 2</h3>
                           <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                        </div>
                        <div id="menu3" class="tab-pane fade box_shdw">
                           <h3>Menu 3</h3>
                           <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                     </div>
                   </div>
                </div>
                <!-- </div> -->
              </div>
            </div>
         </div>
      </div>
   </div>
   <input type="hidden" name="total_products" id="total_products" value="<?php echo $total_products; ?>">
   <input type="hidden" name="last_limit" id="last_limit" value="<?php echo $last_limit; ?>">
   <input type="hidden" name="cat_id" id="cat_id" value="<?php echo $cat_id; ?>">
   <input type="hidden" name="size_id" id="size_id" value="">
   <input type="hidden" name="sub_id" id="sub_id" value="<?php echo $sub_id; ?>">
   <input type="hidden" name="min_range" id="min_range">
   <input type="hidden" name="max_range" id="max_range">
   </form>
</div>

<!--................... SORT AND FILTER IN MOBILE....................-->
<div class="product_footer hidden-sm hidden-md hidden-lg">
  <!-- ******************** SORT SECTION START *************** --> 
  <div class="sort_section" style="display: none;">
    <div class="close_div"><button type="button" id="sort_close">x</button></div>
    <div class="sortby">
        <h3>Sort by</h3>
        <ul>
          <li>
            <input class="fil_sort" id="high" type="radio" name="sort_by" onclick="set_size('high')">
            <label for="high">Price- high to low</label>
          </li>
          <li>
            <input class="fil_sort" id="low" type="radio" name="sort_by" onclick="set_size('low')">
            <label for="low">Price- low to high</label>
          </li>
          <li>
            <input class="fil_sort" id="popularity" type="radio" name="sort_by" onclick="set_size('popular')">
            <label for="popularity">Popularity</label>
          </li>
          <li>
            <input class="fil_sort" id="discount" type="radio" name="sort_by" onclick="set_size('discount')">
            <label for="discount">Discount</label>
          </li>
          <li>
            <input class="fil_sort" id="whats_new" type="radio" name="sort_by" onclick="set_size('product')">
            <label for="whats_new">Whats New</label>
          </li>
        </ul>
        <div class="sort_apply"><button type="button" id="mob_sort" class="apply_filter">Apply</button></div>
    </div>
  </div>
  <!-- ********************* SORT SECTION END **************** -->

  <!-- ********************FILTER SECTION START*************** -->
  <div class="filter_section" style="display:none">
    <div class="close_div"><button type="button" id="filter_close">x</button></div>
    <div class="filterby">
      <h3>Filter by</h3> 
      <div class="tab">
        <button class="tablinks" onclick="filter_open(event, 'Category')" id="defaultOpen">Category</button>
        <button class="tablinks" onclick="filter_open(event, 'Size')">Size</button>
        <button class="tablinks" onclick="filter_open(event, 'Color')">Color</button>
        <button class="tablinks" onclick="filter_open(event, 'Price')">Price</button>
      </div>
      <!--============== FILTER BY CATEGORY ==================-->
      <div id="Category" class="tabcontent">
        <ul class="tab_list">
        <?php  foreach ($sub_category as $row) { ?>
          <li class="cat_check">
            <input class="fil_subcat" id="<?php echo $row->id;?>" type="radio" name="sub_category" onclick="set_sub(<?php echo $row->id;?>)">
            <label for="<?php echo $row->id;?>">
              <a><?php echo $row->sub_category; ?></a>
            </label>
          </li>
          <?php } ?>
        </ul>
      </div>
      <!--================================================-->
      <!--============== FILTER BY SIZE ==================-->
      <div id="Size" class="tabcontent">
        <form action="" method="POST">
           <div class="price_lst">
              <div class="sizes_outs_filter">
                 <?php foreach ($size as $rs) { ?>
                 <div class="sizes_out_filter">
                    <div class="sizes click_size" onclick="set_size_type('<?php echo $rs->id?>')"><?php echo $rs->size_type; ?></div>
                 </div>
                 <?php } ?>
              </div>
           </div>
        </form>
      </div>
      <!--==================================================-->
      <!--============== FILTER BY COLORS ==================-->
      <div id="Color" class="tabcontent">
          <div class="color_round_fil">
          <?php foreach($color as $colors)  { ?>
            <div class="prod_color"  data-id="<?php echo $colors->id;?>">
              <div class="color_pick_a" style="background-image: url('<?php echo $colors->image;?>')" onclick="set_color('<?php echo $colors->id;?>')"></div>
            </div>
          <?php } ?>
          </div>
      </div>
      <!--=================================================-->
      <!--============== FILTER BY PRICE ==================-->
      <div id="Price" class="tabcontent">
        <div class="mob_price_range">
          <div id="mob_slider"  class="ranger"></div>
          <input type="text" class="keypress_0" name="input-with-keypress-0" id="mob_input-with-keypress-0" readonly>
          <input type="text" class="keypress_1" name="input-with-keypress-1" id="mob_input-with-keypress-1" readonly>
          <!-- <input type="hidden" name="min_range" id="mini_range">
          <input type="hidden" name="max_range" id="maxi_range"> -->

        </div>
        <ul class="tab_list filter_price">
          <li class="cat_check">
            <input id="under_1000" type="radio" name="filter_by_price" onclick="set_price(0,1000)">
            <label for="under_1000"><a>Under &#8377;1000</a></label> 
          </li>
          <li class="cat_check">
            <input id="1000_2000" type="radio" name="filter_by_price" onclick="set_price(1000,2000)">
            <label for="1000_2000"><a>&#8377;1000 - &#8377;2000</a></label> 
          </li>
          <li class="cat_check">
            <input id="above_2000" type="radio" name="filter_by_price" onclick="set_price(2000,100000)">
            <label for="above_2000"><a>Above &#8377;2000</a></label> 
          </li>
        </ul>
      </div>
      <!-- ===================================================== -->
      <div class="sort_apply"><button type="button" id="mob_filter" class="apply_filter">Apply</button></div>
    </div>
  </div>
  <!-- ********************FILTER SECTION END***************** -->

  <div class="prod_sort">
    <button class="sort_btn" id="sort_button">SORT</button>
  </div>
  <div class="prod_filter">
     <button class="filter_btn" id="filter_button">FILTER</button>
  </div>
</div>
<!--...............................................................-->

<script type="text/javascript">
var processing;

$(document).ready(function(){

    $("#view_more_product").click(function(e){
        if (processing)
            return false;

        var last_limit = $('#last_limit').val();
        var total_products = $('#total_products').val();
        var order_by = $('#filter_search').val();
        var size_id = $('#size_id').val();
        var min_range = $('#min_range').val();
        var max_range = $('#max_range').val();



        if(total_products>last_limit){


        if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
           //alert(last_limit+' - '+total_products);
            processing = true;
            var cat_id = '<?php echo $cat_id; ?>';
            var sub_id = '<?php echo $sub_id; ?>';
            var data = 'cat_id='+cat_id+'&last_limit='+last_limit+'&order_by='+order_by+'&size_id='+size_id+'&sub_id='+sub_id+'&min_range='+min_range+'&max_range='+max_range;

            $.ajax({
            type: "POST",
            url: base_url+'ajax/scroll_product',
            data: data,
            success: function(response) {
              console.log(response);
                var obj = JSON.parse(response);
                $('#port').append(obj.data);
                $('#last_limit').val(obj.last_limit);
                $('#total_products').val(obj.total_products);
                if(obj.total_products==0){
                    $('.sm_button').hide();
                    //$('.result_div').html('<p>No Result Found....</p>'); 
                }
                processing = false;

            },
            error: function(response) {
                result = 'error';
            },
            async: false
            });
            

            //var result = call_scroll_products();
            //console.log(result);
            //processing = false;
            /*$.post('/echo/html/', 'html=<div class="loadedcontent">new div</div>', function(data){
                $('#container').append(data);
                processing = false;
            });
            alert('sdfsdfsd');*/
        }
    }
    });
});


var keypressSlider = document.getElementById('slider');
var input1 = document.getElementById('input-with-keypress-1');
var input0 = document.getElementById('input-with-keypress-0');
var inputs = [input0, input1];

noUiSlider.create(keypressSlider, {
  start: [500, 10000],
  connect: true,
   direction: 'ltr',
   tooltips: [true, wNumb({ decimals: 0 })],
  range: {
    'min': [500],
    'max': [10000]
  }
});

keypressSlider.noUiSlider.on('end', function(values, handle){
  //addClassFor(lEnd, 'tShow', 450);

  
  var array = values.toString().split(",");
  $('#min_range').val(array[0]);
  $('#max_range').val(array[1]);
  change_range();
});

keypressSlider.noUiSlider.on('update', function( values, handle, unencoded) {
  
  inputs[handle].value = values[handle];
});

function setSliderHandle(i, value) {
  var r = [null,null];
  r[i] = value;
  keypressSlider.noUiSlider.set(r);
}

// Listen to keydown events on the input field.
inputs.forEach(function(input, handle) {

  input.addEventListener('change', function(){
    setSliderHandle(handle, this.value);
  });

  input.addEventListener('keydown', function( e ) {

    var values = keypressSlider.noUiSlider.get();
    var value = Number(values[handle]);

    // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
    var steps = keypressSlider.noUiSlider.steps();

    // [down, up]
    var step = steps[handle];

    var position;

    // 13 is enter,
    // 38 is key up,
    // 40 is key down.
    switch ( e.which ) {

      case 13:
        setSliderHandle(handle, this.value);
        break;

      case 38:

        // Get step to go increase slider value (up)
        position = step[1];

        // false = no step is set
        if ( position === false ) {
          position = 1;
        }

        // null = edge of slider
        if ( position !== null ) {
          setSliderHandle(handle, value + position);
        }

        break;

      case 40:

        position = step[0];

        if ( position === false ) {
          position = 1;
        }

        if ( position !== null ) {
          setSliderHandle(handle, value - position);
        }

        break;
    }
  });
});
     
</script>
<script type="text/javascript">
  $(window).load(function(){
    var width = $(window).width();
    if(width > 500) {
    $('.jq_sidebar_fix').sidebarFix({
      frame: $('.middle')
    });
  }
  });
</script>

<script>

var order_type  = '';
var sub_type    = '';
var price_min   = '';
var price_max   = '';
var size_type   = new Array();
var color_type  = new Array();

function set_size(type){
  order_type = type;
}

function set_sub(sub_id){
  sub_type = sub_id;
}

function set_price(min,max){
  price_min = min;
  price_max = max;
}

// function set_size_type(size_id){
//   size_type = size_id;
// }
function set_size_type(size_id) {
    
    var found = jQuery.inArray(size_id, size_type);
    if (found >= 0) {
        // Element was found, remove it.
        size_type.splice(found, 1);
    } else {
        // Element was not found, add it.
        size_type.push(size_id);
    }

    console.log(size_type);
}

function set_color(color_id) {
    
    var found = jQuery.inArray(color_id, color_type);
    if (found >= 0) {
        // Element was found, remove it.
        color_type.splice(found, 1);
    } else {
        // Element was not found, add it.
        color_type.push(color_id);
    }

    console.log(color_type);
  }

$('.apply_filter').on('click',function(){
  $('.filter_section').hide();
  $('.filter_btn').removeClass('filter_image');
  $('.filter_btn').removeClass('btn_color');
  $('.sort_section').hide();
  $('.sort_btn').removeClass('sort_image');
  $('.sort_btn').removeClass('btn_color');
  var data = 'order_type='+order_type+'&sub_type='+sub_type+'&price_min='+price_min+'&price_max='+price_max+'&size_type='+size_type+'&color_type='+color_type;
  $.ajax({
  type: "POST",
  url: base_url+'ajax/filter_search_pdt',
  data: data,
  success: function(response) {
    console.log(response);
      var obj = JSON.parse(response);
      $('#port').html(obj.data);                
      if(obj.total_products==0){
         $('.result_div').html('<p>No Result Found....</p>'); 
      }
      processing = false;

  },
  error: function(response) {
      result = 'error';
  },
  async: false
  });
})


$('.prod_color').on('click', function () {       
    $(this).toggleClass('highlight');
});
$('.click_size').on('click', function () {       
    $(this).toggleClass("act_size");
});
</script>


