<?php
class Product_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function prod_by_cat($data){
    	$start = $data['start'];
    	$end = $data['end'];
    	$cat_id = $data['cat_id'];
    	$order_by = $data['order_by'];
    	if($order_by=='product'){
    		$order_by = 'ORDER BY product.id DESC'; 
    	} else if($order_by=='high'){
    		$order_by = 'ORDER BY product.price DESC';
    	} else if($order_by=='low'){
    		$order_by = 'ORDER BY product.price ASC';
    	} else {
    		$order_by = 'ORDER BY product.product_name ASC';
    	}

    	if($data['size_id']!=''){
    		$size_id = $data['size_id'];
    		$size_where = "AND product.id IN(SELECT DISTINCT product_id FROM `product_quantity` WHERE size_id =".$size_id.")";
    	} else {
    		$size_where = '';
    	}
    	
    	if(!isset($data['min_range'])){
    	    $data['min_range'] = '';
    	}
    	
    	if(!isset($data['max_range'])){
    	    $data['max_range'] = '';
    	}

        if($data['min_range']!='' && $data['max_range']!=''){
            $min_range = $data['min_range'];
            $max_range = $data['max_range'];
            $range_where = "AND product.price BETWEEN '$min_range' AND '$max_range'";
        } else {
            $range_where = '';
        }

        if(isset($data['sub_id']) && $data['sub_id']!=''){
            $sub_id = $data['sub_id'];
            $sub_where = "INNER JOIN subcategory ON product.subcategory_id = subcategory.id AND subcategory.sub_display='$sub_id'";
        } else {
            $sub_where = '';
        }


    	$product_array = array();
    	$result = $this->db->query("SELECT product.id,product.product_name,product.prod_display,product.price, product.mrp FROM product INNER JOIN category ON product.category_id = category.id $sub_where WHERE product.status = 1 AND category.display_name='$cat_id' $size_where $range_where $order_by LIMIT $start,$end")->result();
        $last_query = $this->db->last_query();
    	foreach ($result as $rs) {
    		$row = $this->db->query('SELECT MAX(quantity) as quantity FROM `product_quantity` WHERE product_id ='.$rs->id)->row();
    		$rs->quantity = $row->quantity;
    		$row = $this->db->query('SELECT product_image,format FROM `product_gallery` WHERE product_id ='.$rs->id.' ORDER BY product_gallery.default DESC LIMIT 0,1')->row();
            if(count($row)>0){
               $rs->product_image = $row->product_image; 
               $rs->format = $row->format; 
           } else {
             $rs->product_image = '';
             $rs->format = '';
           }

           //$rs->last = $last_query;
    		
    		$product_array[] = $rs;
    	}

    	return $product_array;

    }

    public function prod_by_cat_all($id,$sub_id=null){

        if($sub_id!=''){

            $result = $this->db->query("SELECT product.id,product.product_name,product.prod_display,product.price FROM product INNER JOIN category ON product.category_id = category.id INNER JOIN subcategory ON product.subcategory_id = subcategory.id AND subcategory.sub_display='$sub_id' WHERE product.status = 1 AND category.display_name='$id' ORDER BY product.id DESC");
            return $result->num_rows();

        } else {
            $result = $this->db->query("SELECT product.id,product.product_name,product.prod_display,product.price FROM product INNER JOIN category ON product.category_id = category.id WHERE product.status = 1 AND category.display_name='$id' ORDER BY product.id DESC");
            return $result->num_rows();
        }
    	

    }


    public function prod_by_filter_all($id,$size_id,$sub_id,$min_range,$max_range){
    	if($size_id!=''){
    		$size_where = "AND product.id IN(SELECT DISTINCT product_id FROM `product_quantity` WHERE size_id =".$size_id.")";
    	} else {
    		$size_where = '';
    	}

        if($min_range!='' && $max_range!=''){
            $range_where = "AND product.price BETWEEN '$min_range' AND '$max_range'";
        } else {
            $range_where = '';
        }

        if($sub_id!=''){

            $result = $this->db->query("SELECT product.id,product.product_name,product.prod_display,product.price FROM product INNER JOIN category ON product.category_id = category.id INNER JOIN subcategory ON product.subcategory_id = subcategory.id AND subcategory.sub_display='$sub_id' WHERE product.status = 1 AND category.display_name='$id' $size_where $range_where ORDER BY product.id DESC");

        } else {
    	       $result = $this->db->query("SELECT product.id,product.product_name,product.prod_display,product.price FROM product INNER JOIN category ON product.category_id = category.id WHERE product.status = 1 AND category.display_name='$id' $size_where $range_where ORDER BY product.id DESC");
           }
    	return $result->num_rows();

    }

    public function category_list(){
    	$rs = $this->db->where('status','1')->order_by('order_by','ASC')->get('category')->result();

    	foreach ($rs as $key) {
    		$row = $this->db->where('category_id',$key->id)->where('status','1')->get('subcategory')->result();
    		$key->sub = $row;
    		$result[] = $key;
    	}

    	return $result;

    }

    public function size_list(){
    	return $this->db->where('size_type!=','custom')->get('size')->result();
    }

    public function product_info($id){
        $product = $this->db->where('prod_display',$id)->get('product')->row();

        $color = $this->db->distinct()->select('color.id,color.image')->from('product_quantity')->join('color','product_quantity.color_id=color.id')->join('product','product_quantity.product_id=product.id')->where('product.prod_display',$id)->get()->result();

        $size = $this->db->distinct()->select('size_id,size.size_type')->from('product_quantity')->join('size','product_quantity.size_id=size.id')->join('product','product_quantity.product_id=product.id')->where('product.prod_display',$id)->where('size.size_type !=','Custom')->get()->result();

        if($product->stitching_charge==1){
            $custom =  $this->db->where('size_type','custom')->get('size')->row();
        } else {
            $custom = array();
        }
        
        $gallery = $this->db->select('product_image,format')->from('product_gallery')->join('product','product_gallery.product_id=product.id')->where('product.prod_display',$id)->limit(5)->get()->result();

        $quantity = $this->db->query("SELECT MAX(quantity) as quantity FROM `product_quantity` INNER JOIN product ON product_quantity.product_id = product.id WHERE product.prod_display ='$id'")->row();

        $product->size = $size;
        $product->color = $color;
        $product->gallery = $gallery;
        $product->quantity = $quantity;
        $product->custom = $custom;
        return $product;
    }

    public function same_category($id){
        $prod_cat = $this->db->select('category_id')->where('prod_display',$id)->get('product')->row();
        $cat_id = $prod_cat->category_id;
        $rs = $this->db->select('product.id,product.prod_display,product.product_name,product.price,product_gallery.product_image,product_gallery.format')->from('product')->join('product_gallery','product_gallery.product_id=product.id')->where('product.category_id',$cat_id)->where('product.prod_display!=',$id)->where('product.status','1')->group_by('product.id')->limit(10)->get()->result();
        return $rs;
        
    }

    public function get_measurement(){        
        $id=$this->session->userdata('user_id');
        $this->db->where('customer_id',$id);
        $result = $this->db->get('measurement')->row();        
        return $result;
    }

    public function get_size_list($data){

        return $this->db->select('size.id,size.size_type,product_quantity.quantity')->where('product_id',$data['product_id'])->where('color_id',$data['color_id'])->where('size_type!=','custom')->from('product_quantity')->join('size','size.id=product_quantity.size_id')->get()->result();
    }

    public function get_search_product($key){
        $result = $this->db->select('product_name,prod_display')->where("product_name LIKE '%$key%'")->where('status','1')->limit(10)->get('product')->result();
        $new_array = array();
        foreach ($result as $rs) {
            $new_array[] = array('product_name'=>$rs->product_name,
                                 'prod_display'=>'product_info/'.$rs->prod_display);
        }

        $result = $this->db->select('category_name,display_name')->where("category_name LIKE '%$key%'")->limit(10)->get('category')->result();
        foreach ($result as $rs) {
            $new_array[] = array('product_name'=>$rs->category_name,
                                 'prod_display'=>'product/'.$rs->display_name);
        }

        $result = $this->db->select('sub_category,sub_display,display_name')->join('category','category.id = subcategory.category_id')->where("sub_category LIKE '%$key%'")->where('subcategory.status','1')->limit(10)->get('subcategory')->result();
        foreach ($result as $rs) {
            $new_array[] = array('product_name'=>$rs->sub_category,
                                 'prod_display'=>'product/'.$rs->display_name.'/'.$rs->sub_display);
        }



        return $new_array;
    }

    public function search_item($item){
        $new_array = array();
        $result = $this->db->select('product_name,prod_display')->where('product_name', $item)->where('status','1')->get('product');
        //echo $this->db->last_query();
        if($result->num_rows()>0){
            $rs = $result->row();
             $new_array = array('product_name'=>$rs->product_name,
                                 'prod_display'=>'product_info/'.$rs->prod_display);
             return $new_array;
        } else {
            $result = $this->db->select('category_name,display_name')->where('category_name', $item)->where('status','1')->get('category');
            if($result->num_rows()>0){
                $rs = $result->row();
                 $new_array = array('product_name'=>$rs->category_name,
                                 'prod_display'=>'product/'.$rs->display_name);
                 return $new_array;
                } else {
                    $result = $this->db->select('sub_category,sub_display,display_name')->join('category','category.id = subcategory.category_id')->where('sub_category',$item)->where('subcategory.status','1')->get('subcategory');
                    if($result->num_rows()>0){
                        $rs = $result->row();
                         $new_array = array('product_name'=>$rs->sub_category,
                                         'prod_display'=>'product/'.$rs->display_name.'/'.$rs->sub_display);
                         return $new_array;
                        } else {
                            return $new_array;
                        }

                }
        }
        
        
        return $new_array;
        

       

        //search_item
    }

    public function update_cart($data){
        return $this->db->where('id',$data['id'])->update('cart',array('quantity'=>$data['qty']));
    }

    public function get_custom_size_list($data){
       $product_id = $data['product_id'];
       $color_id = $data['color_id'];
       $query = $this->db->query("SELECT * FROM product_quantity WHERE product_quantity.product_id = $product_id AND product_quantity.color_id = $color_id AND product_quantity.size_id= 7 AND product_quantity.quantity > 0");

       return $query->num_rows();
    }

    public function sub_category_list($cat_id){
        $this->db->select('subcategory.id as id,subcategory.sub_category,subcategory.category_id');
        $this->db->from('subcategory');
        $this->db->join('category', 'subcategory.category_id=category.id');
        $this->db->where('category.display_name', $cat_id);
        $result = $this->db->get();
        //echo $this->db->last_query();die;
        return $result->result();

    }
    public function color_list(){
        return $this->db->get('color')->result();
    }

    public function search_filter($data){
        $sub_id     = $data['sub_type'];
        $order_by   = $data['order_type'];
        $min_range  = $data['price_min'];
        $max_range  = $data['price_max'];
        $color_id   = $data['color_type'];
        $size_id    = $data['size_type'];
        $reseller   = null;

        $id = $this->session->userdata('user_id');
        if($id){
            $reseller = get_reseller($id);
        }

        $this->db->select('product.id as id,product.*,
                            product_gallery.product_image,
                            product_gallery.format,
                            product_gallery.product_id,
                            product_quantity.quantity,
                            product_quantity.size_id,
                            product_quantity.color_id,
                            product.status
                        ');
        $this->db->from('product');        
        $this->db->join('product_gallery','product_gallery.product_id= product.id');
        $this->db->join("product_quantity",'product_quantity.product_id=product.id');
        
        if($reseller){
            $this->db->select('IFNULL(reseller_price.price,product.price) AS reseller_amount,(product.mrp-IFNULL(reseller_price.price,product.price)) AS discount');
            $this->db->join("reseller_price", "reseller_price.product_id = product.id AND reseller_id = '$reseller'",'LEFT');
        } else {
            $this->db->select('product.price AS reseller_amount,(product.mrp-product.price) AS discount');
        }
        
        $this->db->group_by('product.id');

        if($order_by!=''){
            if($order_by=='high'){
                $this->db->order_by("reseller_amount","DESC");
            } else if($order_by=='low'){
                $this->db->order_by("reseller_amount","ASC");
            } else if($order_by=='popular'){
                $this->db->select('SUM(order_details.quantity) AS max_pdt');
                $this->db->join("order_details",'product.id = order_details.product_id','left');
                $this->db->order_by("max_pdt","desc");
            } else if($order_by=='discount'){
                $this->db->order_by("discount","desc");
            } else {
                $this->db->order_by("product.id","desc");
            }               
        }

        // if(isset($data->colors) && !empty($data->colors)){
        //     $this->db->where_in('product_quantity.color_id',$data->colors);
        // }

        if($min_range!='' && $max_range!=''){
            //list($start,$end) = explode('-', $data->ranges);
            //$this->db->where('reseller_amount >=', $start);
            //$this->db->where('reseller_amount <=', $end);
            $this->db->having("reseller_amount >=$min_range AND reseller_amount <=$max_range");
        }

        /*if(isset($data->discounts)){
            //$this->db->select('((product.mrp-reseller_amount)/product.mrp)*100 AS new_discount');
            if($data->discounts==0){
                //$this->db->having("reseller_amount >=$start AND reseller_amount <=$end");
                $this->db->having('((product.mrp-reseller_amount)/product.mrp)*100 >', '50',false);
            } else {
                $this->db->having('((product.mrp-reseller_amount)/product.mrp)*100 <=', $data->discounts,false);                   
            }
            $this->db->order_by('((product.mrp-reseller_amount)/product.mrp)*100',"desc");
        }*/
        if($sub_id!=''){
            $this->db->where('product.subcategory_id',$sub_id);
        }

        if($color_id!=''){
            $this->db->where_in('product_quantity.color_id', $color_id);
        }

        if($size_id!=''){
            $this->db->where_in('product_quantity.size_id',$size_id);
        }

        $this->db->where('product.status',1);
        $result = $this->db->get()->result();

        return $result;
    }
        
}

?>
