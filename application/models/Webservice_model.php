<?php

class Webservice_model extends CI_Model
{
    
    public function _consruct()
    {
        parent::_construct();
        
    }
    
    
    function viewproduct($id)
    {
        
        
        $this->db->select("product.category_id as id,product.id as product_id,product.status,product.product_name,product.product_code,product.short_description,product.description,product.price,product.feature,product.mrp,product_gallery.product_image,product_quantity.quantity,product_gallery.format,product.prod_display");
        $this->db->from("product");
        $this->db->join("product_gallery", 'product_gallery.product_id= product.id');
        $this->db->join("product_quantity", 'product_quantity.product_id=product.id');
        // $this->db->where('product.status',1);
        $this->db->where('product.id', $id);
        $query = $this->db->get()->row();
         //$new_result = array();
         $query->product_image = base_url('admin/'.$query->product_image.'_201x302'.$query->format);
            //$new_result[] = $rs;
       
        return $query;
        
    }
    
    
    function stock_qty($id)
    {
        
        $this->db->select("product_quantity.quantity");
        $this->db->from("product_quantity");
        $query = $this->db->where('product_quantity.product_id', $id);
        $query = $this->db->get();
        return $query->row();
        
        
    }
    
    
    function stock_qty1($id)
    {
        
        $this->db->select("product_quantity.id,product_quantity.product_id,product_quantity.quantity");
        $this->db->from("product_quantity");
        $query = $this->db->where('product_quantity.product_id', $id);
        $query = $this->db->get();
        return $query->row();
        
        
    }


    function get_customer($id){
        $rs = $this->db->select("telephone AS customer_no,CONCAT(firstname,' ',lastname) AS customer_name")->where('id',$id)->get('address_book')->row();
        return $rs;
      }
    
    function get_color_id($img)
    {
        
        $this->db->select('color.id');
        $this->db->from('color');
        $this->db->where('color.image', $img);
        $query = $this->db->get();
        return $query->row()->id;
        
    }
    
    
    function get_size_id($type)
    {
        
        $this->db->select('size.id');
        $this->db->from('size');
        $this->db->where('size.size_type', $type);
        $query = $this->db->get();
        return $query->row()->id;
    }
    
    
    
    
    
    
    function viewproduct1($id)
    {
        
        
        $this->db->select('product.id as id,product.*,
                       product_gallery.product_image,
                        product_gallery.product_id,product_gallery.format
                          
               ');
        $this->db->from('product');
        
        $this->db->join('product_gallery', 'product_gallery.product_id= product.id');
        
        $query = $this->db->where('product.id', $id);
        $query = $this->db->get()->row();
        $query->product_image = base_url('admin/'.$query->product_image.'_201x302'.$query->format);

        return $query;
        
    }
    
    function viewcolor($id)
    {
        $this->db->distinct();
        $this->db->select('color.id as id,color.color_name,color.image,product_quantity.product_id,product_quantity.color_id,product.product_name');
        $this->db->from('color');
        $this->db->join('product_quantity', 'FIND_IN_SET(color.id, product_quantity.color_id) > 0');
       $this->db->join('product','product.id=product_quantity.product_id');
       $this->db->where('product.id',$id);
       $query=$this->db->get();
       return $query->result();
    }
    
    
    function get_filter_color()
    {
        $this->db->select('*');
        $this->db->from('color');
        $query = $this->db->get();
        return $query->result();
        
    }
    function get_gallery1()
    {
        $this->db->select('*');
        $this->db->from('product_gallery');
        $this->db->order_by('id', 'desc');
        $this->db->limit(10);
        $qry = $this->db->get()->result();
        $result = array();
        foreach ($qry as $rs) {
           $rs->product_image = base_url('admin/'.$rs->product_image.$rs->format);
           $result[] = $rs;
        }

        return $result;
    }
    
    function get_gallery()
    {
        
        $this->db->select('product_gallery.product_id,product.status,product_gallery.product_image,product_gallery.format');
        $this->db->from('product');
        $this->db->join('product_gallery', 'product.id=product_gallery.product_id');
        $this->db->join("product_quantity", 'product_quantity.product_id=product.id');
        $this->db->where('product.status',1);
        $this->db->where('product_quantity.quantity >', '0');
        $this->db->order_by('product.id', 'desc');
        $this->db->group_by('product.id');
        $this->db->limit(10);
        $qry = $this->db->get()->result();
        $result = array();
        foreach ($qry as $rs) {
           $rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);
           $result[] = $rs;
        }

        return $result;
    }
    
    
    
    function viewproductsize($id)
    {
        /*$this->db->distinct();
        $this->db->select('size.id as id,size.size_type,product_quantity.product_id,
        product_quantity.size_id,product.product_name');
        $this->db->from('size');
        $this->db->join('product_quantity', 'FIND_IN_SET(size.id, product_quantity.size_id) > 0');
        $this->db->join('product', 'product.id=product_quantity.product_id');
        $this->db->where('product.id', $id);
        $this->db->where('size_type!=', 'custom');
        $query = $this->db->get();
        return $query->result();*/
        return $size = $this->db->distinct()->select('size_id AS id,size.size_type')->from('product_quantity')->join('size','product_quantity.size_id=size.id')->join('product','product_quantity.product_id=product.id')->where('product.id',$id)->where('size.size_type !=','Custom')->get()->result();
    }
    
    
    function viewitem_imgslider($cat_id,$id)
    {
         $this->db->select('product_gallery.id as id,product_gallery.product_id,product_gallery.product_image,product_gallery.format,product.product_name,product.price');
           $this->db->from('product_gallery');
           $this->db->join('product','product.id=product_gallery.product_id');
           $this->db->group_by('product.id');
           $this->db->where('product.id!=',$id);
           $this->db->where('product.status','1');
          $this->db->limit(9,0);
          $query=$this->db->get();
          $result = $query->result();
          $new_result = array();
          foreach ($result as $rs) {
           $rs->product_image = base_url('admin/'.$rs->product_image.'_400x600'.$rs->format);
           $new_result[] = $rs;
          }
          //echo $this->db->last_query();
          return $new_result;
         //return $rs;
    }


    function get_charge($topline_id,$closing_id,$placket_id,$other_id){
        if($topline_id!=0)
            $topline = $this->addon_charge($topline_id);
        else
            $topline = 0;

        if($closing_id!=0)
            $closing = $this->addon_charge($closing_id);
        else
            $closing = 0;

        if($placket_id!=0)
            $placket = $this->addon_charge($placket_id);
        else
            $placket = 0;

        if($other_id!=0)
            $other = $this->addon_charge($other_id);
        else
            $other = 0;

        $addon_rate = $topline + $closing + $placket + $other;

        return $addon_rate;
    }

    function addon_charge($id){
        $rs = $this->db->where('id',$id)->select('charge')->get('add_ons')->row();
        return $rs->charge;
    }

    function addon_cart($id){
        $rs = $this->db->where('id',$id)->select('stitch_price')->get('stitching')->row();
        if(count($rs)>0)
            return $rs->stitch_price;
        else
            return 0;
    }
    
    
    
    function viewsubimg($id)
    {
        $this->db->select('product_gallery.id as id,product_gallery.product_id,product_gallery.product_image,product_gallery.format,product.product_name');
           $this->db->from('product_gallery');
           $this->db->join('product','product.id=product_gallery.product_id');
          $this->db->where('product.id',$id);
          $query=$this->db->get();
          $result = $query->result();
          $new_result = array();
          foreach ($result as $rs) {
           $rs->product_image = base_url('admin/'.$rs->product_image.'_400x600'.$rs->format);
           $new_result[] = $rs;
          }
            return $new_result;

        
    }
    
    
    ///stitching neck//
    
    function front_neck()
    {
        
        $this->db->select("stich_type.id as id,stich_type.image,stich_type.discription");
        $this->db->from("stich_type");
        $this->db->join("stichtype", 'stichtype.id =stich_type.type_id');
        $this->db->where("stichtype.stichtype", 'FRONT');
        $query = $this->db->get();

        return $query->result();
        
    }
    
    function back_neck()
    {
        
        $this->db->select("stich_type.id as id,stichtype.stichtype,stich_type.type_id,stich_type.image,stich_type.discription");
        $this->db->from("stich_type");
        $this->db->join("stichtype", 'stichtype.id =stich_type.type_id');
        $this->db->where("stichtype.stichtype", 'BACK');
        $query = $this->db->get();
        return $query->result();
        
    }
    
    function sleeve_neck()
    {
        
        $this->db->select("stich_type.id as id,stichtype.stichtype,stich_type.type_id,stich_type.image,stich_type.discription");
        $this->db->from("stich_type");
        $this->db->join("stichtype", 'stichtype.id =stich_type.type_id');
        $this->db->where("stichtype.stichtype", 'SLEEVE');
        $query = $this->db->get();
        return $query->result();
        
    }
    
    function hemline_neck()
    {
        
        $this->db->select("stich_type.id as id,stichtype.stichtype,stich_type.type_id,stich_type.image,stich_type.discription");
        $this->db->from("stich_type");
        $this->db->join("stichtype", 'stichtype.id =stich_type.type_id');
        $this->db->where("stichtype.stichtype", 'HEMLINE');
        $query = $this->db->get();
        return $query->result();
        
    }
    
    
    
    function addons1()
    {
        $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
        $this->db->from("adons_type");
        $this->db->join("add_ons", 'add_ons.type =adons_type.id');
        $this->db->where('add_ons.type', 1);
        $result = $this->db->get()->result();
        return $result;
        
    }
    
    
    
    function addons2()
    {
        $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
        $this->db->from("adons_type");
        $this->db->join("add_ons", 'add_ons.type =adons_type.id');
        $this->db->where('add_ons.type', 2);
        $result = $this->db->get()->result();
        return $result;
        
    }
    
    
    
    function addons3()
    {
        $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
        $this->db->from("adons_type");
        $this->db->join("add_ons", 'add_ons.type =adons_type.id');
        $this->db->where('add_ons.type', 3);
        $result = $this->db->get()->result();
        return $result;
        
    }
    
    function addons4()
    {
        $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
        $this->db->from("adons_type");
        $this->db->join("add_ons", 'add_ons.type =adons_type.id');
        $this->db->where('add_ons.type', 4);
        $this->db->limit(2, 0);
        $result = $this->db->get()->result();
        return $result;
        
    }
    
    
    
    function addons5()
    {
        
        $this->db->select('add_ons.id,adons_type.adons_type,add_ons.type,add_ons.discription,add_ons.image,add_ons.charge');
        $this->db->from("adons_type");
        $this->db->join("add_ons", 'add_ons.type =adons_type.id');
        $this->db->where('add_ons.type', 4);
        $this->db->order_by('id', "desc");
        $this->db->limit(2, 0);
        $result = $this->db->get()->result();
        
        return $result;
        
    }


    function stitch_charge(){
     $this->db->select('charge');
     $this->db->from('shipping_charge');
     $this->db->where('label','stiching_charge');
    $qry= $this->db->get();
    //print_r($qry);exit;

      return $qry->row();

    }


   function delivery_charge(){

      $this->db->select('charge');
      $this->db->from('shipping_charge');
      //$this->db->where('label',' Free Shipping (Delivered with in 3 to 7 Working D...');
      $this->db->where('id',1);
      $qry=$this->db->get();
     //print_r($qry);exit;
      return $qry->row();

     }

    
    
    
    //view measurement
    
    function view($id)
    {
        
        $this->db->where('customer_id', $id);
        $query  = $this->db->get('measurement');
        $result = $query->row();
        return $result;
        
    }
    
    
    function items()
    {
        
        $this->db->select('*');
        $this->db->from('product');
        $res    = $this->db->get();
        $result = $res->result();
        return $result;
        
        
    }
    
    
    function sub_category_own()
    {
        $this->db->select('subcategory.id as id,category.category_name,subcategory.sub_category');
        $this->db->from('subcategory');
        $this->db->join('category', 'category.id = subcategory.category_id', 'left');
        $query = $this->db->get();
        return $query->result();
    }
    
    //get shipping address
    function get_addrs($id)
    {
        $this->db->select('address');
        $this->db->from('add_address1');
        $this->db->where('customer_id', $id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
        
    }
    
    
    ///get search item
    function get_search_item($id)
    {
        $this->db->select('*');
        $this->db->from('recent_search');
        $this->db->where('customer_id', $id);
        $this->db->distinct('category_id');
        $query  = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    
    
    function shoppingbag_count($id)
    {
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('customer_id', $id);
        $query  = $this->db->get();
        $result = $query->num_rows();
        return $result;
        
    }
    
    
    function notification_count($id)
    {
        $this->db->select('*');
        $this->db->from('notification');
        $this->db->where('customer_id', $id);
        $query  = $this->db->get();
        $result = $query->num_rows();
        return $result;
        
        
        
    }
    
    function get_address_count($id)
    {
        
        $this->db->select('*');
        $this->db->from('add_address1');
        $this->db->where('customer_id', $id);
        $query  = $this->db->get();
        $result = $query->num_rows();
        return $result;
        
        
    }
    
    
    function get_profilepic($id)
    {
        
        $this->db->select('photo');
        $this->db->from('customer');
        $this->db->where('id', $id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
    }
    
    function get_detail2($id)
    {
        
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('id', $id);
        $query  = $this->db->get();
        $result = $query->row();
        return $result;
        
    }


    function custom_check($id){
      $query = $this->db->query("SELECT * FROM product JOIN product_quantity ON product_quantity.product_id = product.id WHERE product_quantity.size_id='7'  AND product_quantity.quantity > 0 AND product.stitching_charge = 1 AND product.id=".$id);
      return $query->num_rows();
     }


    function stock_qty_org($id){

       /*$this->db->select("product_quantity.id,product_quantity.product_id,product_quantity.quantity");
        $this->db->from("product_quantity");
        $query=$this->db->where('product_quantity.product_id',$id);
        $query=$this->db->get();
        return $query->row();*/
        $row = $this->db->query('SELECT MAX(quantity) as quantity FROM `product_quantity` WHERE product_id ='.$id)->row();
        return $row;


}
    
    
    // function outofstock(){
    
    
    
    // }
    
    
    
    
    ///////////forget password//////
    
    
    
    
    function forgetpassword($email)
    {
        $this->db->where('email', $email);
        $query    = $this->db->get('customer');
        $query    = $query->row();
        $settings = get_icon();
        if ($query) {
            $username   = $query->first_name;
            $from_email = $settings->admin_email;
            $this->load->helper('string');
            $rand_pwd = random_string('alnum', 8);
            $password = array(
                'password' => md5($rand_pwd)
            );
            $this->db->where('email', $email);
            $query = $this->db->update('customer', $password);
            if ($query) {

                $data->name = $username;
                $data->message = 'New Password is '.$rand_pwd;
                $data->email = $email;              
                
                $status = $this->check_mail($data);


                
                //return $result;
                /*$configs = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $settings->smtp_host,
                    'smtp_user' => $settings->smtp_username,
                    'smtp_pass' => $settings->smtp_password,
                    'smtp_port' => '587'
                );
                $this->load->library('email');
                $this->email->initialize($configs);
                $this->email->from($from_email, $username);
                $this->email->to($email);
                
                $this->email->send();*/
                return $status==1?"EmailSend":"EmailNotExist";
            }
        } else {
            return "EmailNotExist";
        }
    }


    public function check_mail($data) {
        
        

        $this->load->library('email');
        $config = Array('protocol' => 'smtp', 'smtp_host' => 'mail.zyva.in', 'smtp_port' => 587, 'smtp_user' => 'info@zyva.in', // change it to yours
        'smtp_pass' => '&1M5ZI&xLO@y', // change it to yours
        'smtp_timeout' => 20, 'mailtype' => 'html', 'charset' => 'iso-8859-1', 'wordwrap' => TRUE);
        $this->email->initialize($config); // add this line
        $subject = 'Forget Password';
        $name = $data->name;
        $mailTemplate = $data->message;
        //$this->email->set_newline("\r\n");
        $this->email->from('info@zyva.in', $name);
        $this->email->to($data->email);
        $this->email->subject($subject);
        $this->email->message($mailTemplate);
        return $this->email->send();
    }
    
    public function get_wishlist($id)
    {
        $rs = $this->db->query("SELECT wishlist.id,wishlist.quantity,product.id AS product_id,product.product_name,product.product_code,wishlist.price,product.mrp,color.image,wishlist.size_id,size.size_type FROM `wishlist` LEFT JOIN product ON product.id = wishlist.product_id LEFT JOIN color ON color.id = wishlist.color_id LEFT JOIN size ON size.id = wishlist.size_id WHERE wishlist.customer_id =" . $id)->result();
        return $rs;
    }
    
    public function get_product_image($id)
    {
        $rs = $this->db->query("SELECT product_image,format FROM `product_gallery` WHERE product_id = $id ORDER BY `default` DESC LIMIT 0,1")->row();
        $rs->product_image = base_url('admin/'.$rs->product_image.'_201x302'.$rs->format);

        return $rs->product_image;
    }
    
    public function view_move($cid)
    {
        $this->db->distinct();
        $this->db->select('product_id');
        $this->db->from('wishlist');
        //$this->db->where('product_id',$id);
        $this->db->where('customer_id', $cid);
        $qry = $this->db->get();
        return $qry->result();
        
    }


    public function view_price($id,$query){

       $this->db->select('reseller_code');
          $this->db->from('customer');
          $this->db->where('id',$id);
          $qry=$this->db->get();
          $res=$qry->row();
        
           //print_r($res);exit;
          

         if(!empty($res)){
              $resellercode=$res->reseller_code;

          $this->db->select('id');
          $this->db->from('reseller');
          $this->db->where('reseller_code',$resellercode);
          $rr=$this->db->get();
          $re=$rr->row();
             //print_r($re);exit;
            if($re){

                 $reseler_id=$re->id;

                  $price_id = array();

                 foreach ($query as $rs) {
                    //print_r($query);exit;
                
                  $this->db->select('price');
                  $this->db->from('reseller_price');
                  $this->db->where('product_id',$rs->product_id);
                  $this->db->where('reseller_id',$reseler_id);
                  $query=$this->db->get()->result();
                   //$price=$query->price;
                  //print_r($query);exit;
                    $price_id[]=$query;

                    return $price_id;
                    
              }

               
            }

            }

        //}

    }
    
    public function get_cart($id)
    {

      
    $result = $this->db->query("SELECT cart.id,cart.quantity,cart.price,cart.stock_status,cart.stitch_id,product.id AS product_id,cart.color_id,cart.size_id,product.product_name,cart.stock_status,
    product.product_code,product.price as selling,product.mrp,color.image,size.size_type,cart.size_id FROM `cart` LEFT JOIN 
    product ON product.id = cart.product_id LEFT JOIN
     color ON color.id = cart.color_id  LEFT JOIN
     
     size ON size.id = cart.size_id WHERE cart.customer_id =" . $id)->result();
        //echo $this->db->last_query();exit;
    $res = array();
    foreach ($result as $val) {
    
            $this->db->select('*');
            $this->db->from('product_quantity');
            $this->db->where('color_id', $val->color_id);
            $this->db->where('size_id', $val->size_id);
            $this->db->where('product_id', $val->product_id);
            $rs   = $this->db->get()->row();

            $this->db->last_query();
            
            if ($rs->quantity >= $val->quantity) {                
                if($val->stock_status!=0){
                    $this->db->where('id', $val->id);
                    $val->stock_status = '0';
                    $this->db->set('stock_status', $val->stock_status);
                    $result = $this->db->update('cart');
                }    
                
            } else if($rs->quantity == 0){
                $val->stock_status = 1;
                $this->db->where('id', $val->id);
                $this->db->set('stock_status', $val->stock_status);
                $result = $this->db->update('cart'); 
            } else {
                $qty = $val1->quantity;
                $this->db->where('id', $val->id);
                $this->db->set('quantity', $qty);
                $val->stock_status = '2';
                $this->db->set('stock_status',$val->stock_status);
                $result = $this->db->update('cart');
            }


        $res[] = $val;


        
    }
            
    return $res;

          //}
    }


    public function get_addonscharge($id){

     $this->db->select('stitching.add_ons,add_ons.charge');
     $this->db->from('stitching');
     $this->db->join('add_ons','add_ons.type=stitching.add_ons');
     $this->db->where('cart_id',$id);
     $qry1=$this->db->get()->row();

      if($qry1){

        $addons1=$qry1->charge;
      }
      else{
           $addons1=0;
      }
    //print_r($addons1);exit;

     $this->db->select('stitching.add_ons1,add_ons.charge');
     $this->db->from('stitching');
     $this->db->join('add_ons','add_ons.type=stitching.add_ons1');
     $this->db->where('cart_id',$id);
     $qry2=$this->db->get()->row();

     if($qry2){

        $addons2=$qry2->charge;
      }
      else{
           $addons2=0;
      }
      //print_r($addons2);

     $this->db->select('stitching.add_ons2,add_ons.charge');
     $this->db->from('stitching');
     $this->db->join('add_ons','add_ons.type=stitching.add_ons2');
     $this->db->where('cart_id',$id);
     $qry3=$this->db->get()->row();
     if($qry3){

        $addons3=$qry3->charge;
      }
      else{
           $addons3=0;
      }
      //print_r($addons3);
     $this->db->select('stitching.add_ons3,add_ons.charge');
     $this->db->from('stitching');
     $this->db->join('add_ons','add_ons.type=stitching.add_ons3');
     $this->db->where('cart_id',$id);
     $qry4=$this->db->get()->row();

     if($qry4){

        $addons4=$qry4->charge;
      }
      else{
           $addons4=0;
      }
      //print_r($addons4);
      $addons_charge=$addons1+$addons2+$addons3+$addons4;
      //print_r($addons_charge);exit;

      return $addons_charge;

    }


    function custom_color_check($id,$color_id){
      $query = $this->db->query("SELECT * FROM product JOIN product_quantity ON product_quantity.product_id = product.id WHERE product_quantity.size_id='7' AND product_quantity.color_id='$color_id' AND product_quantity.quantity > 0 AND product.stitching_charge = 1 AND product.id=".$id);
      return $query->num_rows();
     }



    /*function viewmainimage($id){


       $this->db->select('product.id as id,product.*,
                       product_gallery.product_image,
                         product_gallery.format,
                        product_gallery.product_id
               
               ');
        $this->db->from('product');
       
        $this->db->join('product_gallery','product_gallery.product_id= product.id','left');
         
         $query=$this->db->where('product.id',$id);
        $query=$this->db->get();
        $rs = $query->row();
        $rs->product_image = base_url('admin/'.$rs->product_image.$rs->format);
       //echo $this->db->last_query();
        //print_r($rs->product_image );
        return $rs;

   }*/

   function viewmainimage($id){


       $this->db->select('product.id as id,product.*,
                       product_gallery.product_image,
                         product_gallery.format,
                        product_gallery.product_id
               
               ');
        $this->db->from('product');
       
        $this->db->join('product_gallery','product_gallery.product_id= product.id','left');
         
         $query=$this->db->where('product.id',$id);
        $query=$this->db->get();
        $rs = $query->row();
        $rs->share_image = base_url('admin/'.$rs->product_image.$rs->format);
        $rs->product_image = base_url('admin/'.$rs->product_image.$rs->format);
        
       //echo $this->db->last_query();
        //print_r($rs->product_image );
        return $rs;

   }
    
    
    function cart_update($id)
    {
        
        
        $this->db->where('customer_id', $id);
        $query = $this->db->get("cart")->result();
        
        foreach ($query as $rs) {
            $qty_array = $this->db->where('product_id', $rs->product_id)->where('size_id', $rs->size_id)->where('color_id', $rs->color_id)->get('product_quantity')->row();
            
            if ($rs->quantity > $qty_array->quantity) {
                if ($qty_array->quantity == 0) {
                    $this->db->where('id', $rs->id);
                    $this->db->update("cart", array(
                        'stock_status' => '1'
                    ));
                } else {
                    $this->db->where('id', $rs->id);
                    $this->db->update("cart", array(
                        'quantity' => $qty_array->quantity,
                        'stock_status' => '2'
                    ));
                }
                
            }
            
        }
    }

    function get_order_info($id){
        $rs = $this->db->query('SELECT `order_details`.`price`,`order_details`.`quantity`,`order_details`.`stitch_id`,`booking`.`booking_no`,`booking`.address_id,`booking`.reseller_id,`booking`.payment_type,`booking`.booked_date,`booking`.status,`product`.product_name,`product`.product_code,`size`.size_type,`color`.image,`order_details`.product_id FROM `order_details` INNER JOIN `booking` ON booking.id = order_details.booking_id LEFT JOIN product ON order_details.product_id = product.id LEFT JOIN size ON size.id = `order_details`.`size_id` LEFT JOIN color ON color.id = order_details.color_id WHERE order_details.id ='.$id)->row();
        return $rs;
    }

    function get_reseller($id){
        if($id!=0){
            return $this->db->where('id',$id)->get('reseller')->row();
        } else {
            return false;
        }
        
    }

    function get_address($id){
        return $this->db->where('id',$id)->get('address_book')->row();
    }

    function get_stitch_info($id){
        $res = $this->db->where('id',$id)->get('stitching')->row();
        $front = $this->get_stitch($res->front_id);
        $back = $this->get_stitch($res->back_id);
        $sleeve = $this->get_stitch($res->sleeve_id);
        $neck = $this->get_stitch($res->neck_id);
        $add_ons = $this->get_addons($res->add_ons);
        $add_ons1 = $this->get_addons($res->add_ons1);
        $add_ons2 = $this->get_addons($res->add_ons2);
        $add_ons3 = $this->get_addons($res->add_ons3);
        $meas_type =  $res->measure_id==0?'Free Stiching':'Use Measurment';

        $measurement = $this->get_measurement($res->measure_id);

            $stich_array = array('front'=>$front,
                                 'back'=>$back,
                                 'sleeve'=>$sleeve,
                                 'neck'=>$neck,
                                 'add_ons'=>$add_ons,
                                 'add_ons1'=>$add_ons1,
                                 'add_ons2'=>$add_ons2,
                                 'add_ons3'=>$add_ons3,
                                 'meas_type'=>$meas_type,
                                 'measure_id'=>$res->measure_id,
                                 'specialcase'=>$res->specialcase,
                                 'measurement'=>$measurement);

            return $stich_array;
    }

    function get_stitch($id){
        $new_rs = $this->db->where('id',$id)->get('stich_type')->row();
        return $new_rs;
    }

    function get_addons($id){
        $new_rs = $this->db->where('id',$id)->get('add_ons')->row();
        return $new_rs;
    }

    function get_measurement($id){ 
        $new_rs = $this->db->where('id',$id)->get('new_measurement')->row();
        return $new_rs;
    }


    
    
    
    
    
    
    
}
?>