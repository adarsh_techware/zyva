<?php
class Home_model extends CI_Model {
    public function _consruct() {
        parent::_construct();
    }
    function cart_update() {
        $customer_id = $this->session->userdata('user_id');
        $this->db->where('customer_id', $customer_id);
        $query = $this->db->get("cart")->result();
        foreach ($query as $rs) {
            $qty_array = $this->db->where('product_id', $rs->product_id)->where('size_id', $rs->size_id)->where('color_id', $rs->color_id)->get('product_quantity')->row();
            if ($rs->quantity > $qty_array->quantity) {
                if ($qty_array->quantity == 0) {
                    $this->db->where('id', $rs->id);
                    $this->db->update("cart", array('stock_status' => '1'));
                } else {
                    $this->db->where('id', $rs->id);
                    $this->db->update("cart", array('quantity' => $qty_array->quantity, 'stock_status' => '2'));
                }
            }
        }
    }
    function sign_up($data) {
        $this->db->where('email', $data['email']);
        $query1 = $this->db->get('customer');
        if ($query1->num_rows() > 0) {
            return "email";
        }
        else  {
            $data['password'] = md5($data['password']);
            $this->db->insert('customer', $data);
            $insert_id = $this->db->insert_id();
            $this->session->set_userdata('email', $data['email']);
            $user = $this->session->userdata('email');
            return $user;
        } 
        // else {
        //     if ($data['password'] == $data['c_password']) {
        //         $data['password'] = md5($data['password']);
        //         $data['created_date'] = date('Y-m-d');
        //         unset($data['c_password']);

        //         if ($this->db->insert('customer', $data)) {
        //             $insert_id = $this->db->insert_id();
        //             $this->session->set_userdata('email', $data['email']);
        //             $user = $this->session->userdata('email');
        //             return $user;
        //         }

        //     } 
        //     else {
        //         return "password";
        //     }
        // }
    }
    function view_address_list() {
        $customer_id = $this->session->userdata('user_id');
        return $this->db->query("SELECT address_book.id AS address_id,address_book.firstname,address_book.lastname,address_book.company,address_book.address_1,address_book.address_2,address_book.Zip,address_book.default_id,state.state_name,city.city_name FROM `address_book` LEFT JOIN state ON state.id = address_book.state_id LEFT JOIN city ON city.id = address_book.city_id WHERE address_book.customer_id = '$customer_id' AND address_book.status = 1 ORDER BY address_book.id DESC")->result();
    }
    //login
    function login($data) {
        $this->db->select('id,email,password');
        $this->db->where('email', $data['email']);
        $this->db->where('password', md5($data['password']));
        $query = $this->db->get('customer');
        //echo $this->db->last_query(); die();
        if ($query->num_rows() == 1) {
            $result = $query->row();
            $this->session->set_userdata('user_value', $result);
            $this->session->set_userdata('user_id', $result->id);
            $user = $this->session->userdata('user_value');
            $id = $this->session->userdata('user_id');
            return $user;
        } else {
            return "incorrect";
        }
    }
    ///////////forget password//////
    function forgetpassword($email) {
        $rs = $this->db->where('email', $email)->get('customer');
        if ($rs->num_rows() > 0) {
            $unqe = md5(uniqid(time() . mt_rand(), true));
            $this->db->where('email', $email)->update('customer', array('reset_key' => $unqe));
            $link       = base_url('home/reset/' . $unqe);
            $myArray    = $rs->row();
            $settings   = get_icon();
            $this->load->library('email');
            $config     = Array(
                            'protocol'  => 'smtp', 
                            'smtp_host' => $settings->smtp_host, //'smtp.techlabz.in',
                            'smtp_port' => 587, 
                            'smtp_user' => $settings->smtp_username, //'no-reply@techlabz.in', // change it to yours
                            'smtp_pass' => $settings->smtp_password, //'baAEanx7', // change it to yours
                            'smtp_timeout' => 20, 
                            'mailtype'  => 'html', 
                            'charset'   => 'iso-8859-1', 
                            'wordwrap'  => TRUE
                            );
            $this->email->initialize($config); // add this line
            $template['Category']   = $this->category();
            $subject                = $settings->sms_username.' Forgot password';
            $name                   = $settings->sms_username;
            $to                     = $myArray->email;

            $template['reg_name']   = $myArray->first_name;
            $template['link']       = $link;
            $mailTemplate           = $this->load->view('email_templates/forgot_password', $template, true);
            //$this->email->set_newline("\r\n");
            $this->email->from($settings->admin_email, $name);
            $this->email->to($to);
            // $this->email->to('nikhila.techware@gmail.com');
            $this->email->subject($subject);
            $this->email->message($mailTemplate);
            $this->email->send();
        } else {
            return "EmailNotExist";
        }
    }
    function send_notification($myArray) {
        $settings = get_icon();
        $myArray = (object)$myArray;
        $this->load->library('email');
        $config = Array(
                    'protocol'  => 'smtp', 
                    'smtp_host' => $settings->smtp_host, //'smtp.techlabz.in',
                    'smtp_port' => 587, 'smtp_user' => $settings->smtp_username, //'no-reply@techlabz.in', // change it to yours
                    'smtp_pass' => $settings->smtp_password, //'baAEanx7', // change it to yours
                    'smtp_timeout' => 20, 
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1', 
                    'wordwrap'  => TRUE
                    );
        $this->email->initialize($config); // add this line
        $template['Category']   = $this->category();
        $subject                = $settings->sms_username.' Registration';
        $name                   = $settings->sms_username;
        // $reg_name = $myArray->first_name . ' ' . $myArray->last_name;
        $template['reg_name']   = $myArray->first_name;
        $template['email']      = $myArray->email;
        $template['password']   = $myArray->password;
        $mailTemplate           = $this->load->view('email_templates/register_success', $template, true);

        //$this->email->set_newline("\r\n");
        $this->email->from($settings->admin_email, $name);
        $this->email->to($myArray->email);
        // $this->email->to('nikhila.techware@gmail.com');
        $this->email->subject($subject);
        $this->email->message($mailTemplate);
        $this->email->send();
    }
    function category() {
        $this->db->where('status', 1);
        $this->db->order_by('order_by', 'ASC');
        $query = $this->db->get('category');
        $result = $query->result();
        return $result;
    }
    ////////get banners///////////
    function getbanners() {
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->group_by('order');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function shoppingbag() {
        // $id = $this->session->userdata('user_id');
        // $this->db->select('*');
        // $this->db->from('cart');
        // $this->db->where('customer_id', $id);
        // $query = $this->db->get();
        // $result = $query->num_rows();
        // return $result;

        $id = $this->session->userdata('user_id');
        $sess_id = $this->session->userdata('session_id');
        if ($id) {
            $this->db->select('*');
            $this->db->from('cart');
            $this->db->where('customer_id', $id);
        } else {
            $this->db->select('*');
            $this->db->from('temporary_cart');
            $this->db->where('session_id', $sess_id);
        }
        $query = $this->db->get();
        $result = $query->num_rows();
        return $result;
    }
    function cartlist() {
        // $new_result = array();
        // $id = $this->session->userdata('user_id');
        // $this->db->select('*,cart.id as id,product.product_name,cart.price,color.color_name,color.image,size.size_type,product_gallery.product_image');
        // $this->db->from('cart');
        // $this->db->join('product', 'product.id = cart.product_id');
        // $this->db->join('color', 'color.id = cart.color_id');
        // $this->db->join('size', 'size.id = cart.size_id');
        // $this->db->join('product_gallery', 'product_gallery.product_id = cart.product_id', 'left');
        // $this->db->where('customer_id', $id);
        // $this->db->group_by("cart.id");
        // $query = $this->db->get()->result();
        // foreach ($query as $rs) {
        //     if ($rs->stitch_id > 0) {
        //         $row = $this->db->where('id', $rs->stitch_id)->get('stitching')->row();
        //         $rs->price = $rs->price + $row->stitch_price;
        //     }
        //     $new_result[] = $rs;
        // }
        // return (object)$new_result;

        $new_result = array();
        $id = $this->session->userdata('user_id');
        $sess_id = $this->session->userdata('session_id');
        if ($id) {
            $this->db->select('*,cart.id as id,product.product_name,cart.price,color.color_name,color.image,size.size_type,product_gallery.product_image');
            $this->db->from('cart');
            $this->db->join('product', 'product.id = cart.product_id');
            $this->db->join('color', 'color.id = cart.color_id');
            $this->db->join('size', 'size.id = cart.size_id');
            $this->db->join('product_gallery', 'product_gallery.product_id = cart.product_id', 'left');
            $this->db->where('customer_id', $id);
            $this->db->group_by("cart.id");
        } else {
            $this->db->select('*,temporary_cart.id as id,product.product_name,temporary_cart.price,color.color_name,color.image,size.size_type,product_gallery.product_image');
            $this->db->from('temporary_cart');
            $this->db->join('product', 'product.id = temporary_cart.product_id');
            $this->db->join('color', 'color.id = temporary_cart.color_id');
            $this->db->join('size', 'size.id = temporary_cart.size_id');
            $this->db->join('product_gallery', 'product_gallery.product_id = temporary_cart.product_id', 'left');
            $this->db->where('session_id', $sess_id);
            $this->db->group_by("temporary_cart.id");
        }
        $new_result = $this->db->get()->result();
        return (object)$new_result;

        // CUTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
        /*foreach ($query as $rs) {
            if ($rs->stitch_id > 0) {
                $row = $this->db->where('id', $rs->stitch_id)->get('stitching')->row();
                $rs->price = $rs->price + $row->stitch_price;
            }
            $new_result[] = $rs;
        }*/
        // CUTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
        
    }
    public function userinfo() {
        $id = $this->session->userdata('user_id');
        // $this->db->select("CONCAT(first_name,' ',last_name) AS first_name,email,first_name as name,last_name,reseller_id");
        $this->db->select("CONCAT(first_name) AS first_name,email,first_name as name,reseller_id");
        $this->db->from('customer');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    function reset_pass($data) {
        $res = $this->db->where('reset_key', $data['reset_key'])->update('customer', array('password' => md5($data['password']), 'reset_key' => ''));
        return $this->db->affected_rows();
    }

    function fb_login($data){
        $this->db->select('id,email,password');
        $this->db->where('social_id', $data['social_id']);
        $this->db->or_where('email', $data['email']);
        $query = $this->db->get('customer');
        //echo $this->db->last_query();//die();
        
        if($query->num_rows()>0) {
            $result = $query->row();
            $this->session->set_userdata('user_value',$result);
            $this->session->set_userdata('user_id',$result->id);
            $user = $this->session->userdata('user_value');
            $id = $this->session->userdata('user_id');
            return $user;
        } else {
            //echo $this->db->last_query();die();
            $this->db->insert('customer',$data);
            $user_id = $this->db->insert_id();
            $this->session->set_userdata('user_value',$data);
            $this->session->set_userdata('user_id',$user_id);
            $user = $this->session->userdata('user_value');
            $id = $this->session->userdata('user_id');
            return $user;
        }
    }

    function get_temp_cart() {
        $sess_id = $this->session->userdata('session_id');
        $this->db->where('session_id', $sess_id);
        $query = $this->db->get('temporary_cart');
        return $query->result();
    }
}
?>
