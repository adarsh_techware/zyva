<?php
class Checkout_model extends CI_Model {
    public function _consruct() {
        parent::_construct();
    }
    function view_address() {
        $id = $this->session->userdata('user_id');
        $this->db->select('address_book.id as id,address_book.*,
		                   state_name,city_name');
        $this->db->from('address_book');
        $this->db->where('customer_id', $id);
        $this->db->join('state', 'state.id = address_book.state_id', 'left');
        $this->db->join('city', 'city.id = address_book.city_id', 'left');
        $this->db->order_by('address_book.default_id', 'DESC')->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row();
        } else {
            $result = (object)array('id' => 0, 'customer_id' => '', 'firstname' => '', 'lastname' => '', 'company' => '', 'email1' => '', 'telephone' => '', 'address_1' => '', 'address_2' => '', 'state_id' => '', 'city_id' => '', 'Zip' => '', 'default_id' => '', 'status' => '', 'state_name' => '', 'city_name' => '');
        }
        return $result;
    }
    function view_address_list() {
        $customer_id = $this->session->userdata('user_id');
        return $this->db->query("SELECT address_book.id AS address_id,address_book.firstname,address_book.lastname,address_book.company,address_book.address_1,address_book.address_2,address_book.Zip,address_book.default_id,address_book.state_id,address_book.city_id,state.state_name,city.city_name FROM `address_book` LEFT JOIN state ON state.id = address_book.state_id LEFT JOIN city ON city.id = address_book.city_id WHERE address_book.customer_id = '$customer_id' AND address_book.status = 1 ORDER BY address_book.id DESC")->result();
    }
    function view_shipping() {
        $query = $this->db->where('id', '1');
        $query = $this->db->get('shipping_charge');
        $result = $query->row();
        //echo $this->db->last_query();
        //var_dump($result);
        //die;
        return $result;
    }
    function view_shipping1() {
        $query = $this->db->where('id', '2');
        $query = $this->db->get('shipping_charge');
        $result = $query->row();
        //echo $this->db->last_query();
        //var_dump($result);
        //die;
        return $result;
    }
    function view_shipping2() {
        $query = $this->db->where('id', '3');
        $query = $this->db->get('shipping_charge');
        $result = $query->row();
        //echo $this->db->last_query();
        //var_dump($result);
        //die;
        return $result;
    }
    function booked_info($booked_id) {
        return $this->db->query("SELECT COUNT(order_details.id) AS total_item,booking.booking_no,booking.total_rate,booking.payment_type,address_book.telephone,CONCAT(address_book.firstname,' ',address_book.lastname) AS delivery_name,address_book.address_1,address_book.address_2,address_book.state_id,address_book.city_id,address_book.Zip FROM booking LEFT JOIN address_book ON address_book.id = booking.address_id LEFT JOIN order_details ON order_details.booking_id = booking.id WHERE booking.booking_no = '$booked_id' AND booking.payment_status='1' GROUP BY booking.id")->row();
    }

    // function booked_info($booked_id) {
    //     return $this->db->query("SELECT COUNT(order_details.id) AS total_item,booking.booking_no,booking.total_rate,booking.payment_type,address_book.telephone,CONCAT(address_book.firstname,' ',address_book.lastname) AS delivery_name,address_book.address_1,address_book.address_2,address_book.state_id,address_book.city_id,address_book.Zip FROM booking LEFT JOIN address_book ON address_book.id = booking.address_id LEFT JOIN order_details ON order_details.booking_id = booking.id WHERE booking.booking_no = '$booked_id' GROUP BY booking.id")->row();
    // }

    function order_info($booked_id) {
		$this->db->select('product.product_name,
							product_gallery.product_image as product_image,
							color.image as color_image,
							size.size_type,
							order_details.stitch_id,
							order_details.product_id,
							product.product_code,
							order_details.quantity,
							product.price,
							order_details.id,
							order_details.status,
							booking.booking_no'
						);
 		$this->db->from('order_details');
 		$this->db->join('product','product.id = order_details.product_id','left');
 		$this->db->join('color','color.id = order_details.color_id','left');
 		$this->db->join('size','size.id = order_details.size_id','left');
 		$this->db->join('product_gallery','product_gallery.product_id = order_details.product_id','left');
 		$this->db->join('booking','booking.id = order_details.booking_id','left');
 		$this->db->where('booking.booking_no',$booked_id);
 		$this->db->group_by("order_details.product_id");
 		//$this->db->order_by('product_gallery.id', 'DESC')->limit(1);

 		$query = $this->db->get()->result();
 		//echo $this->db->last_query();die;
 		return $query;
    }

    function payment_info($booked_id) {
        $this->db->where('booking_no', $booked_id);
        $query = $this->db->get('transaction');
        $result = $query->row();
        //echo $this->db->last_query();
        //var_dump($result);
        //die;
        return $result;
    }
}
?>