<?php
class Women1_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
        }

     public function fetch_category_sort($limit, $start,$name,$sort) {
        $this->db->limit($limit, $start);
        

        $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
        $this->db->from("category");
        $this->db->join("product",'product.category_id = category.id');
        $this->db->join("product_gallery",'product_gallery.product_id= product.id');
        $this->db->where("category.category_name",$name);
        if($sort=='product'){
        	$this->db->order_by("product.id",'DESC');
        }
         else if($sort=='high')
        {
        	$this->db->order_by("product.price",'DESC');
        }
        else
        {
        	$this->db->order_by("product.price",'ASC');
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $final = $query->result();
            return $final;
        }        
       else{
        $this->db->limit($limit, $start);
        $this->db->select("subcategory.id as id,subcategory.sub_category,subcategory.sub_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
        $this->db->from("subcategory");
        $this->db->join("product",'product.subcategory_id =subcategory.id');
         $this->db->join("product_gallery",'product.id =product_gallery.product_id');
        $this->db->where("subcategory.sub_category",$name);
        if($sort=='product'){
        	$this->db->order_by("product.id",'DESC');
        } 
         else if($sort=='high')
        {
        	$this->db->order_by("product.price",'DESC');
        }
        else 
        {
        	$this->db->order_by("product.price",'ASC');
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $final = $query->result();
            return $final;
        }
      
        return false;

       }



   }
   
   
   
      public function fetch_category_price($limit,$price)
	  {
		  
				$list=explode(';',$price['range']);
				$min=$list[0];
				$max=$list[1];
			
        $this->db->limit($limit);
        
        $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
        $this->db->from("category");
        $this->db->join("product",'product.category_id = category.id');
        $this->db->join("product_gallery",'product_gallery.product_id= product.id');
        //$this->db->where("category.category_name",$name);
		
		$this->db->where('product.price >',$min);
		$this->db->where('product.price <',$max);
        $query = $this->db->get();
		//echo $this->db->last_query();
	//	die;
        if ($query->num_rows() > 0) {
            $final = $query->result();
            return $final;
		   
        }        
       else{
        $this->db->limit($limit);
        $this->db->select("subcategory.id as id,subcategory.sub_category,subcategory.sub_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
        $this->db->from("subcategory");
        $this->db->join("product",'product.subcategory_id =subcategory.id');
         $this->db->join("product_gallery",'product.id =product_gallery.product_id');
        //$this->db->where("subcategory.sub_category",$name);
		
		$this->db->where('product.price >',$min);
		$this->db->where('product.price <',$max);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $final = $query->result();
           return $final;
		    
        }
		
       }
                
           
     
   }
   
   
   
  //  ///////size/////
  //  public function fetch_category_size($name,$size)
	 //  {
		  
		//  //print_r($price);
		 
		 
		// 		//print_r($list);
			
		// 		//echo $range1;
		// 		//echo $range2;
		//  //die;
		 
		 
  //       // $this->db->limit($limit, $start);
        
  //       $this->db->select("category.id as id,category.category_name,category.category_image,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
  //       $this->db->from("category");
  //       $this->db->join("product",'product.category_id = category.id');
  //       $this->db->join("product_gallery",'product_gallery.product_id= product.id');
  //       $this->db->where("category.category_name",$name);
		// $this->db->where("product.size",$size);
		
       
  //       $query = $this->db->get();
		// //echo $this->db->last_query();
		// //die;
  //       if ($query->num_rows() > 0) {
  //           $final = $query->result();
  //           return $final;
		   
  //       }        
  //      else{
  //       // $this->db->limit($limit, $start);
  //       $this->db->select("subcategory.id as id,subcategory.sub_category,subcategory.sub_image,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image");
  //       $this->db->from("subcategory");
  //       $this->db->join("product",'product.subcategory_id =subcategory.id');
  //        $this->db->join("product_gallery",'product.id =product_gallery.product_id');
  //       $this->db->where("subcategory.sub_category",$name);
		// $this->db->where("product.size",$size);
       

  //       $query = $this->db->get();
  //       if ($query->num_rows() > 0) {
  //           $final = $query->result();
  //          return $final;
		    
  //       }
		
       
  //      //return false;

  //      }



  //  }
   
   
   
   
   
   
   
   
   
   
   
   
    function  getsearch_size($data)
      {
//var_dump($data);exit;
        // $this->db->limit($limit, $start);
        $this->db->select("category.id as id,category.category_name,category.category_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image,product_size.product_id,product_size.size_id");
        $this->db->from("category");
        $this->db->join("product",'product.category_id = category.id');
        $this->db->join("product_gallery",'product_gallery.product_id= product.id');
       
         $this->db->join("product_size",'product_size.product_id= product.id');
        $this->db->where("product_size.size_id",$data['value']);
        $query = $this->db->get();
        $final = $query->result();
        //var_dump($final);exit;
       if($final){
        return $final;

       }
       else{
        // $this->db->limit($limit, $start);
        $this->db->select("subcategory.id as id,subcategory.sub_category,subcategory.sub_image,product_gallery.product_id,product.product_name,product.product_code,product.description,product.price,product_gallery.product_image,product_size.product_id,product_size.size_id");
        $this->db->from("subcategory");
        $this->db->join("product",'product.subcategory_id =subcategory.id');
         $this->db->join("product_gallery",'product.id =product_gallery.product_id');
          $this->db->join("product_size",'product_size.product_id= product.id');
        $this->db->where("product_size.size_id",$data['value']);
       
        $query = $this->db->get();
        // print_r($query);die;
        // print_r($query);die;
        return $query->result();

       }

      }

   
   
   
   
   
   

       

}
?>
