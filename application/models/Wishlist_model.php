<?php
class Wishlist_model extends CI_Model {
    public function _consruct() {
        parent::_construct();
    }
    ////////////add to wishlist////
    public function addtowishlist($data) {
        $id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('wishlist');
        $this->db->where('customer_id', $id);
        $this->db->where('product_id', $data['product_id']);
        $this->db->where('color_id', $data['color_id']);
        $this->db->where('size_id', $data['size_id']);
        $query1 = $this->db->get();
        if ($query1->num_rows() == 0) {
            $data1 = array(
                'customer_id'   => $id, 
                'product_id'    => $data['product_id'], 
                'color_id'      => $data['color_id'], 
                'size_id'       => $data['size_id'], 
                'price'         => $data['price'], 
                'quantity'      => $data['quantity']
                );
            if ($this->db->insert('wishlist', $data1)) {
                return "success";
            } 
            else {
                return "error";
            }
            // $q=$query1->quantity+1;
            /*  $a=$data['quantity'];
            
            $q=$query1->quantity+$a;
            
            $a=array('quantity' => $q);
            
            $this->db->where('customer_id',$id);
            
            $this->db->where('product_id', $data['product_id']);
            
            $this->db->where('color_id', $data['color_id']);
            
            $this->db->where('size_id', $data['size_id']);
            if($this->db->update('wishlist', $a))
            
            {
            
            $this->db->where('customer_id',$id);
            
            $this->db->where('product_id', $data['product_id']);
            
            return "success";
            
            } */
        }
    }
    ///add to cart////
    public function addtocart($data) {
        $id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('customer_id', $id);
        $this->db->where('product_id', $data['product_id']);
        $this->db->where('color_id', $data['color_id']);
        $this->db->where('size_id', $data['size_id']);
        $query1 = $this->db->get()->row();
        if (count($query1) > 0) {
            // $q=$query1->quantity+1;
           /* $a = $data['quantity'];
            $q = $query1->quantity + $a;
            $a = array('quantity' => $q);
            $this->db->where('customer_id', $id);
            $this->db->where('product_id', $data['product_id']);
            $this->db->where('color_id', $data['color_id']);
            $this->db->where('size_id', $data['size_id']);*/
            //$this->db->update('cart', $a);
            //if ($this->db->update('cart', $a)) {
                $this->db->where('customer_id', $id);
                $this->db->where('product_id', $data['product_id']);
                $this->db->where('color_id', $data['color_id']);
                $this->db->where('size_id', $data['size_id']);
                $result = $this->db->delete('wishlist');
                //echo $this->db->last_query();
                return "success";
            //} else {
            //    return "error";
            //}
            //echo $this->db->last_query();
            //  return "success";
            
        } else {
            $data1 = array('customer_id' => $data['customer_id'], 'product_id' => $data['product_id'], 'price' => $data['price'], 'color_id' => $data['color_id'], 'size_id' => $data['size_id'], 'quantity' => 1);
            if ($this->db->insert('cart', $data1)) {
                //echo "delete";
                $this->db->where('customer_id', $id);
                $this->db->where('product_id', $data['product_id']);
                $this->db->where('color_id', $data['color_id']);
                $this->db->where('size_id', $data['size_id']);
                $result = $this->db->delete('wishlist');
                //echo $this->db->last_query();
                return "success";
            } else {
                return "error";
            }
        }
    }
    public function updatetowishlist($data) {
        $id = $this->session->userdata('user_id');
        $this->db->where('customer_id', $id);
        $this->db->where('product_id', $data['product_id']);
        if ($this->db->update('wishlist', $data)) {
            return "success";
        } else {
            return "error";
        }
    }
    public function wishlistdetails() {
        $id = $this->session->userdata('user_id');
        $this->db->select('wishlist.id as id,wishlist.*,

                           wishlist.product_id,

                            product.product_name,

                            product.product_code,

                            product.prod_display,

                            wishlist.price,

                            color.image,

                            size.size_type,

                            product_gallery.product_image,

                            product_gallery.format,

                          GROUP_CONCAT(distinct product_gallery.product_id) as product

                            ');
        $this->db->from('wishlist');
        $this->db->join('size', 'size.id = wishlist.size_id', 'left');
        $this->db->join('color', 'color.id = wishlist.color_id', 'left');
        $this->db->join('product', 'product.id = wishlist.product_id', 'left');
        $this->db->join('product_gallery', 'product_gallery.product_id = wishlist.product_id', 'left');
        $this->db->where('customer_id', $id);
        $this->db->group_by("wishlist.id");
        $query = $this->db->get();
        //echo $this->db->last_query();
        //die();
        $result = $query->result();
        return $result;
    }
    public function wishlist_delete($data) {
        $this->db->where('id', $data['id']);
        $result = $this->db->delete('wishlist');
        if ($result) {
            return "success";
        } else {
            return "error";
        }
    }
    public function move($data) {
        $this->db->select('*');
        $this->db->from('cart');
        $this->db->where('id', $data['id']);
        $query1 = $this->db->get()->row();
        $data1  = array('customer_id' => $query1->customer_id, 'product_id' => $query1->product_id, 'price' => $query1->price, 'color_id' => $query1->color_id, 'size_id' => $query1->size_id, 'quantity'=>'1');
        $exist  = $this->db->where('product_id', $query1->product_id)->where('color_id', $query1->color_id)->where('size_id', $query1->size_id)->get('wishlist');
        if ($exist->num_rows() == 0) {
            // echo "not_exist"; die;
            if ($this->db->insert('wishlist', $data1)) {
                $this->db->where('id', $data['id']);
                $result = $this->db->delete('cart');
                if ($result) {
                    return "success";
                } else {
                    return "error";
                }
            }
        }
        else {
            // echo "exist"; die;
            $this->db->where('id', $data['id']);
            $result = $this->db->delete('cart'); 
            if ($result) {
                return "success";
            } else {
                return "error";
            } 
        }
    }
    public function count_wishlist() {
        $id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('wishlist');
        $this->db->where("customer_id", $id);
        $query = $this->db->get();
        //return $query->result();
        //$qry=$query->count->num_rows();
        $s = $query->num_rows();
        return $s;
    }
    function get_prod_details($prod_id) {
        
        // $this->db->order_by("quantity", "desc");
        // $this->db->where('product_id', $prod_id) ;
        // $query= $this->db->get('product_quantity');
        // $this->db->limit(1);        
        // $result = $query->row();
        

        $result=$this->db->select('*')->from('product_quantity')->join('product','product_quantity.product_id=product.id')->where('product_id',$prod_id)->order_by("quantity", "desc")->limit(1)->get()->row();
        // echo $this->db->last_query();
        return $result;
    }
    
}
?>