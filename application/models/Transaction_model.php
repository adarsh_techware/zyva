<?php
class Transaction_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_transaction() {
        return $this->db->get('transaction')->result();
    }

}
?>