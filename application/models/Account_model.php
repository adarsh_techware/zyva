<?php
class Account_model extends CI_Model {
    public function _consruct() {
        parent::_construct();
    }
    function view() {
        $id = $this->session->userdata('user_id');
        /*$this->db->where('id',$id);
        
        $query = $this->db->get('user');
        
        $result = $query->result();
        
        return $result;*/
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('customer');
        //$this->db->join('address', 'address.customer_id = customer.id');
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    function view_address() {
        $id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->where('customer_id', $id);
        $this->db->from('address_book');
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    public function edit($data) {
        $id = $this->session->userdata('user_id');
        $result = array('first_name' => $data['first_name'], 'last_name' => $data['last_name']);
        //print_r($result);
        if ($data['password'] != '') {
            $query = $this->db->where('id', $id)->where('password', md5($data['password']))->get('customer');
            if ($query->num_rows() > 0) {
                if ($data['n_password'] != '') {
                    if ($data['n_password'] == $data['c_password']) {
                        $result['password'] = md5($data['n_password']);
                        $this->db->where('id', $id)->update('customer', $result);
                    } else {
                        return 'cpasswordmissmatch';
                    }
                } else {
                    return 'required';
                }
            } else {
                return "passwordmismatch";
            }
        } else {
            $this->db->where('id', $id)->update('customer', $result);
            return 'success';
        }
        /*$psw= md5($data['password']);
        
        $npsw= md5($data['n_password']);
        
        $cpsw= md5($data['c_password']);
        
           
        
        $datas=array('password'=>$npsw);
        
        
        
        $this->db->select('*');
        
               $this->db->where('id', $id);
        
        $query = $this->db->get('customer');
        
        $results = $query->row();
        
        
        
        if($results)
        
        {
        
        if ($psw == $results->password)
        
        {
        
        
        
        if($npsw == $cpsw) {
        
        $this->db->where('id', $id);
        
        if($this->db->update('customer',$datas))
        
        {
        
        
        
        return "success";
        
        }
        
        else
        
        {
        
        return "error";
        
        }
        
          } 
        
        else
        
        {
        
        return 'cpasswordmissmatch';
        
        }
        
        } 
        
        else
        
        { return 'passwordmismatch';
        
        }
        
        } */
    }
    public function addaddress($myArray) {
        $id = $this->session->userdata('user_id');
        $myArray['customer_id'] = $id;
        if ($this->db->insert('address_book', $myArray)) {
            return "success";
        } else {
            return "error";
        }
    }
    public function get_state() {
        $query = $this->db->get('state');
        $result = $query->result();
        return $result;
    }
    public function get_city() {
        $query = $this->db->get('city');
        $result = $query->result();
        return $result;
    }
    public function view_addressbook() {
        $id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->where('customer_id', $id);
        $this->db->from('address_book');
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    public function getData($loadType, $loadId) {
        if ($loadType == "state") {
            $fieldList = 'id,state_name as name';
            $table = 'state';
            $fieldName = 'country_id';
            $orderByField = 'state_name';
        } else {
            $fieldList = 'id,city_name as name';
            $table = 'city';
            $fieldName = 'state_id';
            $orderByField = 'city_name';
        }
        $this->db->select($fieldList);
        $this->db->from($table);
        $this->db->where($fieldName, $loadId);
        $this->db->order_by($orderByField, 'asc');
        $query = $this->db->get();
        return $query;
    }
    function editadd() {
        $id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->where('customer_id', $id);
        $this->db->from('address_book');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function update_addressbook($data) {
        unset($data['company']);
        unset($data['check1']);
        unset($data['check2']);
        unset($data['checkbox-2']);
        $data['customer_id'] = $this->session->userdata('user_id');
        $this->db->insert('address_book', $data);
        return $this->db->insert_id();
    }
    function update_default($id) {
        $customer_id = $this->session->userdata('user_id');
        $this->db->where('customer_id', $customer_id)->update('address_book', array('default_id' => '0'));
        return $this->db->where('id', $id)->where('customer_id', $customer_id)->update('address_book', array('default_id' => '1'));
    }
    function add_new_address($data) {
        $customer_id = $this->session->userdata('user_id');
        $this->db->where('customer_id', $customer_id)->update('address_book', array('default_id' => '0'));
        // echo $this->db->last_query();die;
        $data['default_id'] = 1;
        $data['customer_id'] = $customer_id;
        return $this->db->insert('address_book', $data);
    }
}
?>